var CACHE_VERSION = 42;
var CURRENT_CACHES = {
    prefetch: 'TvHay-v' + CACHE_VERSION,
    dynamic: 'TvHay-dynamic-v' + CACHE_VERSION
};
var assets = [
    '/offline.html',
    '/assets/css/multi-select.dist.css',
    '/assets/css/select2-bootstrap4.min.css',
    '/assets/css/style.css',
    '/assets/css/sweetalert.css',
    '/assets/img/logo/amazon.png',
    '/assets/img/logo/anonfile.png',
    '/assets/img/logo/archive.png',
    '/assets/img/logo/bayfiles.png',
    '/assets/img/logo/blogger.png',
    '/assets/img/logo/dailymotion.png',
    '/assets/img/logo/direct.png',
    '/assets/img/logo/dropbox.png',
    '/assets/img/logo/facebook.png',
    '/assets/img/logo/fembed.png',
    '/assets/img/logo/filecm.png',
    '/assets/img/logo/filerio.png',
    '/assets/img/logo/filesfm.png',
    '/assets/img/logo/filesim.png',
    '/assets/img/logo/fireload.png',
    '/assets/img/logo/gdrive.png',
    '/assets/img/logo/gofile.png',
    '/assets/img/logo/googlephotos.png',
    '/assets/img/logo/hexupload.png',
    '/assets/img/logo/hxfile.png',
    '/assets/img/logo/indishare.png',
    '/assets/img/logo/mediafire.png',
    '/assets/img/logo/mixdropto.png',
    '/assets/img/logo/mp4upload.png',
    '/assets/img/logo/mymailru.png',
    '/assets/img/logo/okru.png',
    '/assets/img/logo/onedrive.png',
    '/assets/img/logo/pandafiles.png',
    '/assets/img/logo/pcloud.png',
    '/assets/img/logo/racaty.png',
    '/assets/img/logo/rumble.png',
    '/assets/img/logo/sendvid.png',
    '/assets/img/logo/sibnet.png',
    '/assets/img/logo/solidfiles.png',
    '/assets/img/logo/soundcloud.png',
    '/assets/img/logo/streamable.png',
    '/assets/img/logo/streamff.png',
    '/assets/img/logo/streamlare.png',
    '/assets/img/logo/streamsb.png',
    '/assets/img/logo/streamtape.png',
    '/assets/img/logo/supervideo.png',
    '/assets/img/logo/tiktok.png',
    '/assets/img/logo/uploadbuzz.png',
    '/assets/img/logo/uploadsmobi.png',
    '/assets/img/logo/upstream.png',
    '/assets/img/logo/uptobox.png',
    '/assets/img/logo/uqload.png',
    '/assets/img/logo/userscloud.png',
    '/assets/img/logo/videobin.png',
    '/assets/img/logo/vidio.png',
    '/assets/img/logo/vidoza.png',
    '/assets/img/logo/vimeo.png',
    '/assets/img/logo/viu.png',
    '/assets/img/logo/voe.png',
    '/assets/img/logo/vudeo.png',
    '/assets/img/logo/vupto.png',
    '/assets/img/logo/yadisk.png',
    '/assets/img/logo/yourupload.png',
    '/assets/img/logo/youtube.png',
    '/assets/img/logo/zippyshare.png',
    '/assets/img/logo/zplayer.png',
    '/assets/img/plyr-custom.svg',
    '/assets/js/app.js',
    '/assets/js/detect-adblocker.min.js',
    '/assets/js/jquery-ui.min.js',
    '/assets/js/jquery.min.js',
    '/assets/js/jquery.multi-select.js',
    '/assets/js/js.cookie.min.js',
    '/assets/js/md5.js',
    '/assets/js/polyfill.js',
    '/assets/js/prebid-ads.js',
    '/assets/js/pwacompat.min.js',
    '/assets/js/sweetalert.min.js',
    '/assets/vendor/apexcharts/apexcharts.css',
    '/assets/vendor/apexcharts/apexcharts.min.js',
    '/assets/vendor/bootstrap/css/bootstrap.min.css',
    '/assets/vendor/bootstrap/js/bootstrap.bundle.min.js',
    '/assets/vendor/bs-custom-file-input/bs-custom-file-input.min.js',
    '/assets/vendor/crypto-js/aes.js',
    '/assets/vendor/crypto-js/crypto-js.js',
    '/assets/vendor/datatables/datatables.min.css',
    '/assets/vendor/datatables/datatables.min.js',
    '/assets/vendor/fontawesome6/css/all.min.css',
    '/assets/vendor/fontawesome6/webfonts/fa-brands-400.woff2',
    '/assets/vendor/fontawesome6/webfonts/fa-solid-900.woff2',
    '/assets/vendor/hls.js/latest/hls.min.js',
    '/assets/vendor/jquery-wheelcolorpicker/css/wheelcolorpicker.css',
    '/assets/vendor/jquery-wheelcolorpicker/jquery.wheelcolorpicker.min.js',
    '/assets/vendor/mux.js/mux.min.js',
    '/assets/vendor/plyr/plyr-custom.polyfilled.min.js',
    '/assets/vendor/plyr/plyr.css',
    '/assets/vendor/select2/css/select2.min.css',
    '/assets/vendor/select2/js/select2.min.js',
    '/assets/vendor/shaka-player/latest/shaka-player.compiled.js',
    '/assets/vendor/toastify-js/toastify.min.css',
    '/assets/vendor/toastify-js/toastify.min.js',
];

self.addEventListener('install', function (e) {
    e.waitUntil(
        caches.open(CURRENT_CACHES.prefetch).then(function (cache) {
            cache.addAll(assets);
        })
    );
});

self.addEventListener('activate', function (e) {
    e.waitUntil(
        caches.keys().then(function (keys) {
            return Promise.all(
                keys.filter(function (key) {
                    return key !== CURRENT_CACHES.prefetch && key !== CURRENT_CACHES.dynamic;
                }).map(function (key) {
                    return caches.delete(key);
                })
            );
        })
    );
});

self.addEventListener('fetch', function (e) {
    if (e.request.headers.get('range') || !useCache(e.request.url)) {
        return;
    } else {
        e.respondWith(
            caches.match(e.request).then(function (cacheRes) {
                return cacheRes || fetch(e.request).then(function (fetchRes) {
                    if (fetchRes.status === 200) {
                        return caches.open(CURRENT_CACHES.dynamic).then(function (cache) {
                            cache.put(e.request.url, fetchRes.clone());
                            return fetchRes;
                        });
                    }
                });
            }).catch(function () {
                if (!useCache(e.request.url)) {
                    return caches.match('/offline.html');
                }
            })
        );
    }
});

function useCache(url) {
    return url.indexOf('/assets/css') > -1 || url.indexOf('/assets/js') > -1 || url.indexOf('/assets/vendor') > -1 || url.indexOf('/assets/img') > -1 || url.indexOf('/poster/') > -1 || url.indexOf('/subtitle/') > -1 || url.indexOf('/poster.jpg') > -1 || url.indexOf('/subtitle.vtt') > -1 || url.indexOf('/uploads/subtitles') > -1 || url.indexOf('/uploads/images') > -1;
}