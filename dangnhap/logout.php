<?php
session_write_close();

require 'includes.php';

header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

$login = new \Login();
$logout = $login->logout();
if ($logout) create_alert('info', 'Logged out successfully!', BASE_URL . admin_dir() . '/login');
else create_alert('danger', 'Can not log out!');
