<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

$dmca_link = get_option('dmca_page_link');
$contact_link = get_option('contact_page_link');
?>
</main>
<footer id="footer" class="row mt-5 py-5 bg-dark rounded-bottom text-center text-white ">
    <div class="col-12">
        <ul class="nav justify-content-center mb-3">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo $baseURL; ?>">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo $baseURL; ?>changelog/">Change Log</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo $baseURL; ?>terms/">Terms</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo $baseURL; ?>privacy/">Privacy</a>
            </li>
            <?php if (!empty($dmca_link)) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $dmca_link; ?>" target="_blank" rel="noopener">DMCA</a>
                </li>
            <?php endif; ?>
            <?php if (!empty($contact_link)) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $contact_link; ?>" target="_blank" rel="noopener">Contact</a>
                </li>
            <?php endif; ?>
        </ul>
        <p>&copy; 2020 - <?php echo date('Y'); ?>. Made with <i class="fas fa-heart text-danger"></i> by <?php echo $sitename; ?>.</p>
    </div>
</footer>
</div>
<button id="gotoTop" class="bg-custom shadow">
    <span class="gotoContent">
        <i class="fas fa-chevron-up"></i>
    </span>
</button>
<script src="<?php echo $baseURL; ?>assets/js/pwacompat.min.js"></script>
<script src="<?php echo $baseURL; ?>assets/js/js.cookie.min.js" defer></script>
<script src="<?php echo $baseURL; ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js" defer></script>
<script src="<?php echo $baseURL; ?>assets/js/sweetalert.min.js" defer></script>
<script>
    $(document).ready(function($) {
        loadTooltip();

        $('#user').blur(function() {
            if ($(this).val() !== '' && typeof $(this).attr('disabled') === 'undefined') {
                $.ajax({
                    url: adminURL + 'ajax/public.ajax.php',
                    type: 'POST',
                    data: {
                        action: 'check_username',
                        username: $(this).val()
                    },
                    success: function(res) {
                        if (res.status === 'fail') swal('Warning!', res.message, 'warning');
                        else $(this).next('.input-group-append .btn').removeAttr('disabled');
                    },
                    error: function(xhr) {
                        swal('Error!', xhr.responseText, 'error');
                    }
                });
            }
        });

        $('#email').blur(function() {
            if ($(this).val() !== '' && typeof $(this).attr('disabled') === 'undefined') {
                $.ajax({
                    url: adminURL + 'ajax/public.ajax.php',
                    type: 'POST',
                    data: {
                        action: 'check_email',
                        email: $(this).val()
                    },
                    success: function(res) {
                        if (res.status === 'fail') swal('Warning!', res.message, 'warning');
                        else $(this).next('.input-group-append .btn').removeAttr('disabled');
                    },
                    error: function(xhr) {
                        swal('Error!', xhr.responseText, 'error');
                    }
                });
            }
        });

        $('.sweet-overlay').click(function(){
            console.log('test ajah');
            $(this).hide();
            $('.sweet-alert').removeClass('showSweetAlert visible').addClass('hideSweetAlert');
        });
    });

    function loadTooltip() {
        $('[data-toggle="tooltip"], [data-tooltip="true"]').tooltip({
            container: 'body'
        });
        $('[data-toggle="tooltip"], [data-tooltip="true"]').on('show.bs.tooltip', function() {
            $('body > [role="tooltip"]').remove();
        });
    }

    function require(url) {
        var e = document.createElement("script");
        e.src = url;
        e.type = "text/javascript";
        document.getElementsByTagName("head")[0].appendChild(e);
    }

    function $_GET(name) {
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(window.location.href);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    function showToast(text, type, opts, onClose) {
        var def = {
            text: text.replace(/\+/ig, ' '),
            duration: 5000,
            close: true,
            gravity: "top",
            position: "right",
            stopOnFocus: true,
            callback: onClose
        };
        var style = {
            info: {
                backgroundColor: 'linear-gradient(to right, #5477f5, #73a5ff)'
            },
            warning: {
                backgroundColor: 'linear-gradient(to right, #ff9800, #ffc107)',
                className: 'text-dark'
            },
            danger: {
                backgroundColor: 'linear-gradient(to right, #e91e63, #f44336)'
            },
            success: {
                backgroundColor: 'linear-gradient(to right, #009688, #4caf50)'
            },
            error: {
                backgroundColor: 'linear-gradient(to right, #e91e63, #f44336)'
            },
        };
        $('body div[role="tooltip"]').remove();
        Toastify(Object.assign(def, style[type], opts)).showToast();
    }

    function ajaxGET(url, sCallback, eCallback) {
        var c = url.indexOf('/api') > -1 ? false : true;
        $.ajax({
            url: url,
            type: 'GET',
            cache: c,
            success: sCallback,
            error: eCallback
        });
    }

    function ajaxPOST(url, data, sCallback, eCallback) {
        var c = url.indexOf('/api') > -1 ? false : true;
        $.ajax({
            url: url,
            type: 'POST',
            cache: c,
            data: data,
            success: sCallback,
            error: eCallback
        });
    }

    (function() {
        'use strict';
        window.addEventListener('load', function() {
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(e) {
                    if (form.checkValidity() === false) {
                        e.preventDefault();
                        e.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
<?php if ($userLogin) : ?>
    <script src="<?php echo $baseURL; ?>assets/vendor/bs-custom-file-input/bs-custom-file-input.min.js" defer></script>
    <script src="<?php echo $baseURL; ?>assets/vendor/datatables/datatables.min.js" defer></script>
    <script src="<?php echo $baseURL; ?>assets/vendor/select2/js/select2.min.js" defer></script>
    <script src="<?php echo $baseURL; ?>assets/vendor/jquery-wheelcolorpicker/jquery.wheelcolorpicker.min.js" defer></script>
    <script src="<?php echo $baseURL; ?>assets/vendor/toastify-js/toastify.min.js" defer></script>
    <script src="<?php echo $baseURL; ?>assets/vendor/apexcharts/apexcharts.min.js" defer></script>
    <script src="<?php echo $baseURL; ?>assets/js/jquery.multi-select.js" defer></script>
    <script src="<?php echo $baseURL; ?>assets/js/md5.js" defer></script>
    <script src="<?php echo $baseURL; ?>assets/js/jquery-ui.min.js" defer></script>
    <script>
        $(document).ready(function() {
            bsCustomFileInput.init();

            $.fn.DataTable.ext.pager.simple_numbers_no_ellipses = function(page, pages) {
                var numbers = [];
                var buttons = 4;
                var half = Math.floor(buttons / 2);

                var _range = function(len, start) {
                    var end;
                    var out = [];
                    if (typeof start === "undefined") {
                        start = 0;
                    } else {
                        end = start;
                        start = len;
                    }
                    for (var i = start; i < end; i++) {
                        out.push(i);
                    }
                    return out;
                };
                if (pages <= buttons) {
                    numbers = _range(0, pages);
                } else if (page <= half) {
                    numbers = _range(0, buttons);
                } else if (page >= pages - 1 - half) {
                    numbers = _range(pages - buttons, pages);
                } else {
                    numbers = _range(page - half, page + half + 1);
                }
                numbers.DT_el = 'span';
                return ['first', 'previous', numbers, 'next', 'last'];
            };

            $.extend(true, $.fn.dataTable.defaults, {
                destroy: true,
                stateSave: true,
                responsive: true,
                processing: true,
                paging: true,
                pagingType: 'simple_numbers_no_ellipses',
                deferRender: true,
                rowReorder: true,
                searchDelay: 1000,
                language: {
                    //"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Indonesian.json",
                    "decimal": "",
                    "emptyTable": "No data available in table",
                    "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                    "infoEmpty": "Showing 0 to 0 of 0 entries",
                    "infoFiltered": "(filtered from _MAX_ total entries)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Show _MENU_ entries",
                    "loadingRecords": "Loading...",
                    "processing": "Processing...",
                    "search": "Search:",
                    "zeroRecords": "No matching records found",
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "paginate": {
                        "first": '<i class="fas fa-step-backward"></i>',
                        "last": '<i class="fas fa-step-forward"></i>',
                        "next": '<i class="fas fa-chevron-right"></i>',
                        "previous": '<i class="fas fa-chevron-left"></i>'
                    }
                }
            });

            $('.select2').select2({
                theme: 'bootstrap4'
            });

            $('.multi-select').multiSelect();

            $(window).on('scroll', function() {
                var g = $("#gotoTop");
                if (document.body.scrollTop > 640 || document.documentElement.scrollTop > 640) {
                    g.fadeIn();
                } else {
                    g.fadeOut();
                }
            });

            $('#gotoTop').on('click', function() {
                $('html,body').animate({
                    scrollTop: 0
                }, 'slow');
            });
        });

        function copyText(text, name) {
            var textarea = document.createElement('textarea');
            textarea.textContent = text;
            document.body.appendChild(textarea);

            var range = document.createRange();
            range.selectNode(textarea);

            var selection = document.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);

            console.log('copy success', document.execCommand('copy'));
            selection.removeAllRanges();

            document.body.removeChild(textarea);

            name = typeof name !== 'undefined' && name !== '' && name !== null ? name : 'Embed code';
            showToast(name + ' has been copied.', 'success');
        }

        function getRandomInt(min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min) + min);
        }

        var vidHosts = <?php echo json_encode(\Hosting::supportedSites(), true); ?>;
        var subtitleHTML = '<?php echo subtitle_languages('lang[]'); ?>';
        ajaxPOST(adminURL + 'ajax/public.ajax.php', {
            action: 'get_load_balancer_list'
        }, function(res) {
            localStorage.setItem('lb', JSON.stringify(res.data));
        }, function(xhr) {
            localStorage.setItem('lb', '[]');
        });
    </script>
    <?php
    if (is_dir($adminDir . '/js/')) {
        open_resources_handler();
        $list = new \DirectoryIterator($adminDir . '/js/');
        foreach ($list as $file) {
            if (!$file->isDot() && $file->isFile()) {
                if (pathinfo($file->getFilename(), PATHINFO_EXTENSION) === 'js') {
                    echo '<script src=' . $adminURL . '/js/' . $file->getFilename() . '?v='.date('ymdhis') .' defer></script>';
                }
            }
        }
    }
    ?>
<?php endif; ?>
<?php
$class = new \Plugins();
$adminJS = $class->getAdminJS(true);
if ($adminJS) {
    echo $adminJS;
}
?>
<?php
$page = get_admin_page();
if ($page === 'login' || $page === 'register' || $page === 'reset-password') {
    include BASE_DIR . 'includes/recaptcha.php';
}
include BASE_DIR . 'includes/histats.php';
echo html_entity_decode(get_option('chat_widget'));
?>
<script>
    if ("serviceWorker" in navigator && '<?php echo $baseURL; ?>'.indexOf('localhost') === -1) {
        navigator.serviceWorker
            .register('<?php echo $baseURL; ?>sw.js')
            .then(function(res) {
                console.log("service worker registered")
            })
            .catch(function(err) {
                console.log("service worker not registered", err)
            })
    }
</script>
</body>

</html>
<?php
$class = new \Minify();
$output = $class->minify_html(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
