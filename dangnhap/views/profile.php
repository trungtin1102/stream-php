<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!$userLogin) {
    session_write_close();
    include $adminDir . '/views/402.php';
    exit();
} else {
    session_write_close();
    if (!empty($_POST)) {
        $class = new \Users();

        if (isset($_POST['user'])) {
            $class->setCriteria('id', $userLogin['id'], '<>');
            $class->setCriteria('user', $_POST['user'], '=', 'AND');
            $userExist = $class->getNumRows() > 0;
        } else {
            $userExist = false;
        }

        if (isset($_POST['email'])) {
            $class->setCriteria('id', $userLogin['id'], '<>');
            $class->setCriteria('email', $_POST['email'], '=', 'AND');
            $emailExist = $class->getNumRows() > 0;
        } else {
            $emailExist = false;
        }

        if ($userExist) {
            create_alert('danger', 'Username not available.', $_SERVER['REQUEST_URI']);
        } elseif ($emailExist) {
            create_alert('danger', 'Email not available.', $_SERVER['REQUEST_URI']);
        } else {
            unset($_POST['id']);
            $class->setCriteria('id', $userLogin['id']);
            if (!empty($_POST['password'])) {
                if (!empty($_POST['retype_password'])) {
                    if ($_POST['password'] === $_POST['retype_password']) {
                        unset($_POST['retype_password']);
                        $_POST['password'] = password_hash($_POST['password'], 1);
                        $updated = $class->update($_POST);
                        if ($updated) {
                            create_alert('success', 'Your account updated successfully.', $_SERVER['REQUEST_URI']);
                        } else {
                            create_alert('danger', $class->getLastError(), $_SERVER['REQUEST_URI']);
                        }
                    } else {
                        create_alert('danger', 'New password and retype password doesn\'t match.', $_SERVER['REQUEST_URI']);
                    }
                } else {
                    create_alert('danger', 'Insert retype password first.', $_SERVER['REQUEST_URI']);
                }
            } else {
                unset($_POST['password']);
                unset($_POST['retype_password']);
                $updated = $class->update($_POST);
                if ($updated) {
                    create_alert('success', 'Your account updated successfully.', $_SERVER['REQUEST_URI']);
                } else {
                    create_alert('danger', $class->getLastError(), $_SERVER['REQUEST_URI']);
                }
            }
        }
    } else {
        unset($userLogin['password']);
        $dataP = $userLogin;
    }
}

$title = 'My Account';
include $adminDir . '/header.php';
$badge = '';
if (is_admin()) {
    $badge = '<span class="badge badge-warning">Admin</span>';
}
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">My Account <?php echo $badge; ?></h1>
        <form action="<?php echo $adminURL; ?>/profile" method="post" class="needs-validation" autocomplete="off" novalidate>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" value="<?php if (!empty($dataP['name'])) echo htmlspecialchars($dataP['name']); ?>" class="form-control" placeholder="Your name" required>
                        <div class="invalid-feedback">Must be filled!</div>
                    </div>
                    <div class="form-group">
                        <label for="user">Username</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button class="btn btn-secondary" type="button" id="edit-user" data-toggle="tooltip" title="Edit Username" onclick="$('#user, #btnChangeUser').prop('disabled', false)">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </div>
                            <input type="text" name="user" id="user" value="<?php if (!empty($dataP['user'])) echo htmlspecialchars($dataP['user']); ?>" class="form-control" placeholder="Your username" required disabled>
                            <div class="input-group-append">
                                <button class="btn btn-info rounded-right" type="button" id="btnChangeUser" data-toggle="tooltip" title="Save" onclick="users.changeUsername($('#user').val())" disabled>
                                    <i class="fas fa-save"></i>
                                </button>
                            </div>
                            <div class="invalid-feedback">Must be filled!</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button class="btn btn-secondary" type="button" id="edit-email" data-toggle="tooltip" title="Change Email" onclick="$('#email, #btnChangeEmail').prop('disabled', false)">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </div>
                            <input type="email" name="email" id="email" value="<?php if (!empty($dataP['email'])) echo htmlspecialchars($dataP['email']); ?>" class="form-control" placeholder="Your email" required disabled>
                            <div class="input-group-append">
                                <button class="btn btn-info rounded-right" id="btnChangeEmail" type="button" data-toggle="tooltip" title="Save" onclick="users.changeEmail($('#email').val())" disabled>
                                    <i class="fas fa-save"></i>
                                </button>
                            </div>
                            <div class="invalid-feedback">Must be valid!</div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="password">New Password</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button class="btn btn-secondary" type="button" data-toggle="tooltip" title="Show/hide New Password" onclick="if($('#password').attr('type') === 'password'){ $('#password').attr('type', 'text');$(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash');} else{ $('#password').attr('type', 'password');$(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');}">
                                    <i class="fas fa-eye"></i>
                                </button>
                            </div>
                            <input type="password" name="password" id="password" class="form-control" autocomplete="off" placeholder="Your new password" onchange="if($(this).val() !== '') $('#retype_password').attr('required', 'true'); else $('#retype_password').removeAttr('required');">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">Retype New Password</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button class="btn btn-secondary" type="button" data-toggle="tooltip" title="Show/hide retype new password" onclick="if($('#retype_password').attr('type') === 'password'){ $('#retype_password').attr('type', 'text');$(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash'); }else{ $('#retype_password').attr('type', 'password');$(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');}">
                                    <i class="fas fa-eye"></i>
                                </button>
                            </div>
                            <input type="password" name="retype_password" id="retype_password" autocomplete="off" class="form-control" placeholder="Retype your new password" onchange="if($(this).val() !== $('#password').val()) $(this).removeClass('is-valid').addClass('is-invalid'); else $(this).removeClass('is-invalid').addClass('is-valid');">
                            <div class="invalid-feedback">Must match the new password!</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <button type="submit" class="btn btn-success shadow-sm">
                        <i class="fas fa-save mr-2"></i>
                        <span>Update</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
include BASE_DIR . 'administrator/footer.php';
