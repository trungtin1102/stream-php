<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!$userLogin) {
    include $adminDir . '/views/403.php';
    exit();
}

$title = 'Subtitle List';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">Subtitle List</h1>
        <div class="mb-3">
            <button type="button" class="btn shadow-sm btn-danger btn-sm mr-2" onclick="subtitles.delete.multi($(this))" title="Delete">
                <i class="fas fa-trash-alt"></i>
                <span class="ml-2 d-none d-md-inline">Delete</span>
            </button>
            <button type="button" class="btn shadow-sm btn-info btn-sm mr-2" onclick="subtitles.reload()">
                <i class="fas fa-sync-alt"></i>
                <span class="ml-2 d-none d-md-inline">Reload</span>
            </button>
            <button type="button" class="btn shadow-sm btn-warning btn-sm mr-2" onclick="subtitles.migrate()">
                <i class="fas fa-sync-alt"></i>
                <span class="ml-2 d-none d-md-inline">Migrate</span>
            </button>
        </div>
        <table id="tbSubtitles" class="table table-striped table-bordered table-hover table-sm m-0" style="width:100%">
            <thead>
                <tr>
                    <th>
                        <div class="custom-control custom-checkbox mx-auto">
                            <input type="checkbox" class="custom-control-input" id="ckAllSubtitles">
                            <label class="custom-control-label" for="ckAllSubtitles"></label>
                        </div>
                    </th>
                    <th>File Name</th>
                    <th>Language</th>
                    <th>User</th>
                    <th>Location</th>
                    <th>Added On</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>
                        <div class="custom-control custom-checkbox mx-auto">
                            <input type="checkbox" class="custom-control-input" id="ckAllSubtitles1">
                            <label class="custom-control-label" for="ckAllSubtitles1"></label>
                        </div>
                    </th>
                    <th>File Name</th>
                    <th>Language</th>
                    <th>User</th>
                    <th>Location</th>
                    <th>Added On</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<div class="modal fade" id="modalHostSub" tabindex="-1" role="dialog" aria-labelledby="modalHostSubLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <form method="POST" id="frmMigrateSub" class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalHostSubLabel">Subtitle Migration</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="subHosts">Old Location</label>
                    <select id="subHosts" name="subHosts" class="form-control" required></select>
                </div>
                <div class="form-group">
                    <label for="subNewLoc">New Location</label>
                    <input type="url" name="subNewLoc" id="subNewLoc" class="form-control" placeholder="Example: <?php echo $baseURL; ?>" required>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="action" value="migrate">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" onclick="subtitles.migrateNow()">Migrate Now</button>
            </div>
        </form>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
