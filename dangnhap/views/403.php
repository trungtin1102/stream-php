<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

$title = '403 Unauthorized!';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12 text-center">
        <div class="my-5">
            <h1 class="h3 text-danger"><strong>403</strong> Unauthorized!</h1>
            <h3 class="h4 text-secondary">This page can only be accessed by super administrators.</h3>
        </div>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
