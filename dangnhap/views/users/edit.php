<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!is_admin()) {
    session_write_close();
    include $adminDir . '/views/403.php';
    exit();
} else {
    session_write_close();
    // cek id
    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
    if (!empty($id)) {
        $class = new \Users();
        if (!empty($_POST)) {
            session_write_close();
            $_POST['updated'] = time();

            $class->setCriteria('id', $id, '<>');
            $class->setCriteria('user', $_POST['user'], '=', 'AND');
            $userExist = $class->getNumRows() > 0;

            $class->setCriteria('id', $id, '<>');
            $class->setCriteria('email', $_POST['email'], '=', 'AND');
            $emailExist = $class->getNumRows() > 0;
            $class->resetVar();

            if ($userExist) {
                create_alert('danger', 'Username not available.', $_SERVER['REQUEST_URI']);
            } elseif ($emailExist) {
                create_alert('danger', 'Email not available.', $_SERVER['REQUEST_URI']);
            } else {
                unset($_POST['id']);
                $class->setCriteria('id', $id);
                if (!empty($_POST['password'])) {
                    if (!empty($_POST['retype_password'])) {
                        if ($_POST['password'] === $_POST['retype_password']) {
                            unset($_POST['retype_password']);
                            $_POST['password'] = password_hash($_POST['password'], PASSWORD_BCRYPT);
                            $updated = $class->update($_POST);
                            if ($updated) {
                                create_alert('success', 'User details updated successfully.', $_SERVER['REQUEST_URI']);
                            } else {
                                create_alert('danger', $class->getLastError(), $_SERVER['REQUEST_URI']);
                            }
                        } else {
                            create_alert('danger', 'New password and retype password doesn\'t match.', $_SERVER['REQUEST_URI']);
                        }
                    } else {
                        create_alert('danger', 'Insert retype password first.', $_SERVER['REQUEST_URI']);
                    }
                } else {
                    unset($_POST['password']);
                    unset($_POST['retype_password']);
                    $updated = $class->update($_POST);
                    if ($updated) {
                        create_alert('success', 'User details updated successfully.', $_SERVER['REQUEST_URI']);
                    } else {
                        create_alert('danger', $class->getLastError(), $_SERVER['REQUEST_URI']);
                    }
                }
            }
            
            $class->setCriteria('id', $id);
            $dataU = $class->getOne();
            unset($dataU['password']);
        } else {
            session_write_close();
            $class->setCriteria('id', $id);
            $dataU = $class->getOne();
            unset($dataU['password']);
        }
    } else {
        session_write_close();
        include $adminDir . '/views/404.php';
        exit();
    }
}

$title = 'User Details';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">User Details</h1>
        <form action="<?php echo $adminURL; ?>/users/edit?id=<?php echo $id; ?>" method="post" class="needs-validation" autocomplete="off" novalidate>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" value="<?php if (!empty($dataU['name'])) echo htmlspecialchars($dataU['name']); ?>" class="form-control" placeholder="Your name" required>
                        <div class="invalid-feedback">Must be filled!</div>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" value="<?php if (!empty($dataU['email'])) echo htmlspecialchars($dataU['email']); ?>" class="form-control" placeholder="Your email address" required>
                        <div class="invalid-feedback">Must be valid!</div>
                    </div>
                    <div class="form-group">
                        <label for="user">Username</label>
                        <input type="text" name="user" id="user" value="<?php if (!empty($dataU['user'])) echo htmlspecialchars($dataU['user']); ?>" class="form-control" placeholder="Your username" required>
                        <div class="invalid-feedback">Must be filled!</div>
                    </div>
                    <div class="form-group">
                        <label for="name">User Role</label>
                        <select name="role" id="role" class="custom-select" required>
                            <option value="">-- Select User Role --</option>
                            <option value="0" <?php echo $dataU['role'] === '0' ? 'selected' : ''; ?>>Administrator</option>
                            <option value="1" <?php echo $dataU['role'] === '1' ? 'selected' : ''; ?>>User</option>
                        </select>
                        <div class="invalid-feedback">Must choose one!</div>
                    </div>
                    <div class="form-group">
                        <label for="name">Status</label>
                        <select name="status" id="status" class="custom-select" required>
                            <option value="">-- Select Status --</option>
                            <option value="0" <?php echo $dataU['status'] === '0' ? 'selected' : ''; ?>>Inactive</option>
                            <option value="1" <?php echo $dataU['status'] === '1' ? 'selected' : ''; ?>>Active</option>
                            <option value="2" <?php echo $dataU['status'] === '2' ? 'selected' : ''; ?>>Need Approval</option>
                        </select>
                        <div class="invalid-feedback">Must choose one!</div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="password">New Password</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button class="btn btn-secondary" type="button" data-toggle="tooltip" title="Show/hide New Password" onclick="if($('#password').attr('type') === 'password'){ $('#password').attr('type', 'text');$(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash');} else{ $('#password').attr('type', 'password');$(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');}">
                                    <i class="fas fa-eye"></i>
                                </button>
                            </div>
                            <input type="password" name="password" id="password" class="form-control" placeholder="Your new password" onchange="if($(this).val() !== '') $('#retype_password').attr('required', 'true'); else $('#retype_password').removeAttr('required');">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">Retype New Password</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button class="btn btn-secondary" type="button" data-toggle="tooltip" autocomplete="off" title="Show/hide retype new password" onclick="if($('#retype_password').attr('type') === 'password'){ $('#retype_password').attr('type', 'text');$(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash'); }else{ $('#retype_password').attr('type', 'password');$(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');}">
                                    <i class="fas fa-eye"></i>
                                </button>
                            </div>
                            <input type="password" name="retype_password" id="retype_password" autocomplete="off" class="form-control" placeholder="Retype your new password" onchange="if($(this).val() !== $('#password').val()) $(this).removeClass('is-valid').addClass('is-invalid'); else $(this).removeClass('is-invalid').addClass('is-valid');">
                            <div class="invalid-feedback">Must match the new password!</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <button type="submit" class="btn btn-success shadow-sm mr-2">
                        <i class="fas fa-save mr-2"></i>
                        <span>Update</span>
                    </button>
                    <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
                </div>
            </div>
        </form>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
