<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

$title = '404 Page not found!';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12 text-center">
        <h1 class="h3 text-danger"><strong>404</strong> Page not found!</h1>
        <h3 class="h4 text-secondary">The page you want to access does not exist.</h3>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
