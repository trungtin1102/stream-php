<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!is_admin()) {
    include $adminDir . '/views/403.php';
    exit();
}

$title = 'Session List';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">Session List</h1>
        <div class="mb-3">
            <button type="button" class="btn shadow-sm btn-danger btn-sm mr-2" onclick="sessions.deleteChecked()">
                <i class="fas fa-trash-alt"></i><span class="ml-2">Delete</span>
            </button>
            <button type="button" class="btn shadow-sm btn-info btn-sm mr-2" onclick="sessions.reload()">
                <i class="fas fa-sync-alt"></i>
                <span class="ml-2">Reload</span>
            </button>
        </div>
        <table id="tbSessions" class="table table-striped table-bordered table-hover table-sm m-0" style="width:100%">
            <thead>
                <tr>
                    <th>
                        <div class="custom-control custom-checkbox mx-auto">
                            <input type="checkbox" class="custom-control-input" id="ckAllSessions">
                            <label class="custom-control-label" for="ckAllSessions"></label>
                        </div>
                    </th>
                    <th>Username</th>
                    <th>IP Address</th>
                    <th>User Agent</th>
                    <th>Log On</th>
                    <th>Expires On</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>
                        <div class="custom-control custom-checkbox mx-auto">
                            <input type="checkbox" class="custom-control-input" id="ckAllSessions1">
                            <label class="custom-control-label" for="ckAllSessions1"></label>
                        </div>
                    </th>
                    <th>Username</th>
                    <th>IP Address</th>
                    <th>User Agent</th>
                    <th>Log On</th>
                    <th>Expires On</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
