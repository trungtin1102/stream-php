<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!$userLogin) {
    session_write_close();
    include $adminDir . '/views/402.php';
    exit();
} else {
    session_write_close();
    $supportedSites = \Hosting::supportedSites();
    $altLinks = [];
    $mainLink = '';
    $embedUrl = '';
    $dlUrl = '';
    $reqUrl = '';
    $jsonUrl = '';
    $slug = '';
    $poster = '';
    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
    if (!empty($id)) {
        if (!empty($_POST)) {
            // upload poster
            if (!empty($_FILES['poster-file']['tmp_name'])) {
                $posterURL = upload_poster(array(
                    'tmp_name' => $_FILES['poster-file']['tmp_name'],
                    'name' => $_FILES['poster-file']['name'],
                    'size' => $_FILES['poster-file']['size'],
                    'type' => $_FILES['poster-file']['type'],
                    'error' => $_FILES['poster-file']['error']
                ));
                if ($posterURL) {
                    $_POST['poster'] = basename($posterURL);
                }
            } elseif (validate_url($_POST['poster-url'])) {
                $_POST['poster'] = $_POST['poster-url'];
            }
            unset($_POST['poster-url']);
            unset($_POST['poster-file']);

            // upload subtitles
            $now = time();
            $subtitles = [];
            $subLabelFiles = [];
            $subLabelURLs = [];
            $oldSubtitles = isset($_POST['subs']) ? (array) $_POST['subs'] : [];

            foreach ($_POST['sub-type'] as $i => $dt) {
                if ($dt === 'file') $subLabelFiles[] = $_POST['lang'][$i];
                elseif ($dt === 'url') $subLabelURLs[] = $_POST['lang'][$i];
            }

            $subManager = new \SubtitleManager();
            if (!empty($_FILES['sub-file']['tmp_name']) && is_array($_FILES['sub-file']['tmp_name'])) {
                foreach ($_FILES['sub-file']['tmp_name'] as $i => $dt) {
                    $subtitleURL = upload_subtitle(array(
                        'tmp_name' => $_FILES['sub-file']['tmp_name'][$i],
                        'name' => $_FILES['sub-file']['name'][$i],
                        'size' => $_FILES['sub-file']['size'][$i],
                        'type' => $_FILES['sub-file']['type'][$i],
                        'error' => $_FILES['sub-file']['error'][$i]
                    ));
                    if ($subtitleURL) {
                        $subManager->insert(array(
                            'file_name' => basename($subtitleURL),
                            'file_size' => $_FILES['sub-file']['size'][$i],
                            'file_type' => $_FILES['sub-file']['type'][$i],
                            'language' => $subLabelFiles[$i],
                            'added' => $now,
                            'uid' => $userLogin['id'],
                            'host' => strtr(parse_url(BASE_URL, PHP_URL_HOST), ['www.' => ''])
                        ));
                        $subtitles[] = [
                            'file' => $subtitleURL,
                            'label' => $subLabelFiles[$i]
                        ];
                    }
                }
            }

            // save subtitle link
            if (!empty($_POST['sub-url'])) {
                $class = new \Subscene();
                foreach ($_POST['sub-url'] as $i => $dt) {
                    if (validate_url($dt)) {
                        if (strpos(parse_url($dt, PHP_URL_HOST), 'subscene') !== FALSE) {
                            $class->set_url($dt);
                            $class->download();
                            $data = $class->file();
                            if ($data) {
                                $subManager->insert(array(
                                    'file_name' => basename($data['url']),
                                    'file_size' => filesize($data['path']),
                                    'file_type' => mime_content_type($data['path']),
                                    'language' => $subLabelURLs[$i],
                                    'added' => $now,
                                    'uid' => $userLogin['id'],
                                    'host' => strtr(parse_url(BASE_URL, PHP_URL_HOST), ['www.' => ''])
                                ));
                                $subtitles[] = [
                                    'file' => $data['url'],
                                    'label' => $subLabelURLs[$i]
                                ];
                            }
                        } else {
                            $subtitles[] = [
                                'file' => $dt,
                                'label' => $subLabelURLs[$i]
                            ];
                        }
                    }
                }
            }
            unset($_POST['sub-type']);
            unset($_POST['sub-file']);
            unset($_POST['sub-url']);
            unset($_POST['lang']);
            unset($_POST['subs']);

            // custom slug
            $slug = slugify($_POST['slug']);
            unset($_POST['slug']);

            $alternatives = $_POST['altLinks'];
            $alternatives = array_filter($alternatives);
            $alternatives = array_values($alternatives);
            unset($_POST['altLinks']);

            $hosting = new \Hosting();
            $hosting->setURL($_POST['host_id']);
            $_POST['host'] = $hosting->getHost();
            $_POST['host_id'] = $hosting->getID();

            $now = time();
            $_POST['updated'] = $now;
            //$_POST['uid'] = $userLogin['id'];
            $_POST['ahost'] = '';
            $_POST['ahost_id'] = '';

            $class = new \Videos();
            $class->setCriteria('id', $id);
            $updated = $class->update($_POST);
            if ($updated) {
                $class = new \VideosAlternatives();
                $class->setCriteria('vid', $id);
                if (!empty($alternatives)) {
                    $oldLinks = $class->get(['id', 'host', 'host_id']);

                    foreach ($alternatives as $dt) {
                        $hosting->setURL($dt);
                        $altLinks[] = [
                            'host' => $hosting->getHost(),
                            'host_id' => $hosting->getID(),
                            'link' => $hosting->getDownloadLink()
                        ];
                    }

                    if ($oldLinks) {
                        foreach ($oldLinks as $dt) {
                            $khost = array_search($dt['host'], array_column($altLinks, 'host'));
                            $kid = array_search($dt['host_id'], array_column($altLinks, 'host_id'));
                            if ($khost === false || $kid === false) {
                                $class->setCriteria('vid', $id);
                                $class->setCriteria('host', $dt['host'], '=', 'AND');
                                $class->setCriteria('host_id', $dt['host_id'], '=', 'AND');
                                $class->delete();
                            }
                        }
                    }

                    foreach ($altLinks as $i => $dt) {
                        $class->setCriteria('vid', $id, '=');
                        $class->setCriteria('host', $dt['host'], '=', 'AND');
                        $class->setCriteria('host_id', $dt['host_id'], '=', 'AND');
                        if ($class->getNumRows() === 0) {
                            $class->insert(array(
                                'vid' => $id,
                                'host' => $dt['host'],
                                'host_id' => $dt['host_id'],
                                'order' => $i
                            ));
                        } else {
                            $class->setCriteria('vid', $id, '=');
                            $class->setCriteria('host', $dt['host'], '=', 'AND');
                            $class->setCriteria('host_id', $dt['host_id'], '=', 'AND');
                            $class->update(array(
                                'order' => $i
                            ));
                        }
                    }
                } else {
                    $class->delete();
                }

                $class = new \VideoShort();
                $class->setCriteria('vid', $id);
                $short = $class->getOne(['key']);
                if ($short) {
                    if (!empty($slug)) {
                        if ($slug !== $short['key']) {
                            $class->setCriteria('vid', $id);
                            $newKey = $class->update(array(
                                'vid' => $id,
                                'key' => $slug
                            ));
                        }
                    } else {
                        $uuid = new \UUID();
                        $slug = $uuid->v4();
                        $newKey = $class->insert(array(
                            'vid' => $id,
                            'key' => $slug
                        ));
                        if ($newKey === false) {
                            $slug = $uuid->v4();
                            $class->insert(array(
                                'vid' => $id,
                                'key' => $slug
                            ));
                        }
                    }
                } else {
                    if (!empty($slug)) {
                        $newKey = $class->insert(array(
                            'vid' => $id,
                            'key' => $slug
                        ));
                    } else {
                        $uuid = new \UUID();
                        $slug = $uuid->v4();
                        $newKey = $class->insert(array(
                            'vid' => $id,
                            'key' => $slug
                        ));
                        if ($newKey === false) {
                            $slug = $uuid->v4();
                            $class->insert(array(
                                'vid' => $id,
                                'key' => $slug
                            ));
                        }
                    }
                }

                if (!empty($subtitles)) {
                    $class = new \Subtitles();
                    foreach ($subtitles as $i => $dt) {
                        $class->insert(array(
                            'vid' => $id,
                            'language' => $dt['label'],
                            'link' => $dt['file'],
                            'added' => $now,
                            'uid' => $userLogin['id'],
                            'order' => $i
                        ));
                    }
                }

                if (!empty($oldSubtitles)) {
                    $class = new \Subtitles();
                    foreach ($oldSubtitles as $i => $dt) {
                        $class->setCriteria('id', $dt);
                        $class->update(array(
                            'order' => $i
                        ));
                    }
                }

                create_alert('success', 'Video updated successfully.', $_SERVER['REQUEST_URI']);
            } else {
                create_alert('danger', $class->getLastError(), $_SERVER['REQUEST_URI']);
            }
        } else {
            $class = new \Videos();
            $class->setCriteria('id', $id, '=');
            $dataV = $class->getOne();
            if ($dataV) {
                $hosting = new \Hosting();

                $hosting->setHost($dataV['host']);
                $hosting->setID($dataV['host_id']);
                $mainLink = $hosting->getDownloadLink();

                // get query
                $qry = array(
                    'id' => $mainLink,
                    'alt' => []
                );

                // get alternative hosts
                if (!empty($dataV['ahost_id'])) {
                    $hosting->setHost($dataV['ahost']);
                    $hosting->setID($dataV['ahost_id']);
                    $link = $hosting->getDownloadLink();
                    $altLinks[] = [
                        'host' => $dataV['ahost'],
                        'host_id' => $dataV['ahost_id'],
                        'link' => $link
                    ];
                    array_push($qry['alt'], $link);
                }

                $class = new \VideosAlternatives();
                $class->setCriteria('vid', $id);
                $class->setOrderBy('order', 'ASC');
                $list = $class->get(['host', 'host_id']);
                if ($list) {
                    foreach ($list as $dt) {
                        $hosting->setHost($dt['host']);
                        $hosting->setID($dt['host_id']);
                        $link = $hosting->getDownloadLink();
                        $altLinks[] = [
                            'host' => $dt['host'],
                            'host_id' => $dt['host_id'],
                            'link' => $link
                        ];
                        array_push($qry['alt'], $link);
                    }
                }

                // get poster
                $posterURL = validate_url($dataV['poster']) ? BASE_URL . 'poster/?url=' . encode($dataV['poster']) : BASE_URL . 'uploads/images/' . $dataV['poster'];
                $qry['poster'] = $poster;

                // get subtitles
                $class = new \Subtitles();
                $class->setCriteria('vid', $id);
                $class->setOrderBy('order', 'ASC');
                $list = $class->get(['id', 'link', 'language']);
                if ($list) {
                    $dataV['subtitle'] = $list;
                    $qry['sub'] = array_column($list, 'link');
                    $qry['lang'] = array_column($list, 'language');
                }

                $httpQuery = http_build_query($qry);

                // get short key
                $short = new \VideoShort();
                $short->setCriteria('vid', $id);
                $data = $short->getOne(['key']);
                if ($data) {
                    $slug = $data['key'];
                    $embedUrl = BASE_URL . 'embed/' . $data['key'];
                    $dlUrl = BASE_URL . 'download/' . $data['key'];
                } else {
                    $uuid = new \UUID();
                    $key = $uuid->v4();
                    $newKey = $short->insert(array(
                        'key' => $key,
                        'vid' => $id
                    ));
                    if ($newKey) {
                        $slug = $key;
                        $embedUrl = BASE_URL . 'embed/' . $key;
                        $dlUrl = BASE_URL . 'download/' . $key;
                    } else {
                        $encode = encode($httpQuery);
                        $embedUrl = BASE_URL . 'embed/?' . $encode;
                        $dlUrl = BASE_URL . 'download/?' . $encode;
                    }
                }
                $reqUrl = BASE_URL . 'embed2/?' . $httpQuery . '&onlylink=no';
                $jsonUrl = BASE_URL . 'api/?' . $httpQuery;

                $class = new \GDriveMirrors();
                $files = [];
                if ($dataV['host'] === 'gdrive') {
                    $class->setCriteria('gdrive_id', $dataV['host_id']);
                    $list = $class->get();
                    if ($list) $files = array_merge($files, $list);
                }
                if (!empty($altLinks)) {
                    foreach ($altLinks as $dt) {
                        if ($dt['host'] === 'gdrive') {
                            $class->setCriteria('gdrive_id', $dt['host_id']);
                            $list = $class->get();
                            if ($list) $files = array_merge($files, $list);
                        }
                    }
                }
            } else {
                session_write_close();
                include $adminDir . '/views/404.php';
                exit();
            }
        }
    } else {
        session_write_close();
        include $adminDir . '/views/404.php';
        exit();
    }
}

$title = 'Edit Video';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">Edit Video</h1>
        <form action="<?php echo $adminURL; ?>/videos/edit?id=<?php echo $id; ?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="form-group">
                        <label for="title">Video Title</label>
                        <input type="text" name="title" id="title" value="<?php if (!empty($dataV['title'])) echo strip_tags($dataV['title']); ?>" class="form-control" placeholder="Insert video title here" required>
                    </div>
                    <div class="form-group">
                        <label for="host_id">Main Video URL</label>
                        <div class="input-group">
                            <?php if (!empty($dataV['host'])) : ?>
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <img src="<?php echo BASE_URL . "assets/img/logo/{$dataV['host']}.png"; ?>" data-toggle="tooltip" title="<?php echo $supportedSites[$dataV['host']]; ?>">
                                    </div>
                                </div>
                            <?php endif; ?>
                            <input type="url" id="host_id" name="host_id" class="form-control" placeholder="Insert main video url here" value="<?php echo $mainLink; ?>" required>
                            <div class="input-group-append">
                                <button type="button" class="btn btn-info" onclick="videos.scrollToHosts()" data-toggle="tooltip" title="Example Link">
                                    <i class="fas fa-info-circle"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="altLink-0">Alternative Video URLs</label>
                        <div id="altWrapper">
                            <div class="form-group" data-index="0">
                                <div class="input-group">
                                    <?php if (!empty($altLinks[0])) : ?>
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <img src="<?php echo BASE_URL . "assets/img/logo/{$altLinks[0]['host']}.png"; ?>" data-toggle="tooltip" title="<?php echo strtr($supportedSites[$altLinks[0]['host']], ['|New' => '', '|Additional Host' => '']); ?>">
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <input type="url" id="altLink-0" name="altLinks[]" class="form-control" placeholder="Insert alternative video url here" value="<?php if (!empty($altLinks[0]['link'])) echo $altLinks[0]['link']; ?>">
                                    <div class="input-group-append">
                                        <button type="button" data-toggle="tooltip" title="Add Alternative Video URL" class="btn btn-custom" onclick="videos.addAlternativeHTML()">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                        <?php if (!empty($altLinks[0])) : ?>
                                            <button type="button" data-toggle="tooltip" title="Remove Alternative Video URL" class="btn btn-danger" onclick="videos.removeAlternativeHTML(0)">
                                                <i class="fas fa-minus"></i>
                                            </button>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            unset($altLinks[0]);
                            $altLinks = array_values($altLinks);
                            foreach ($altLinks as $i => $dt) :
                                $i++; ?>
                                <div class="form-group" data-index="<?php echo $i; ?>">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <img src="<?php echo BASE_URL . "assets/img/logo/{$dt['host']}.png"; ?>" data-toggle="tooltip" title="<?php echo strtr($supportedSites[$dt['host']], ['|New' => '', '|Additional Host' => '']); ?>">
                                            </div>
                                        </div>
                                        <input type="url" id="altLink-<?php echo $i; ?>" name="altLinks[]" class="form-control" placeholder="Insert alternative video url here" value="<?php echo $dt['link']; ?>">
                                        <div class="input-group-append">
                                            <button type="button" data-toggle="tooltip" title="Remove Alternative Video URL" class="btn btn-danger" onclick="videos.removeAlternativeHTML(<?php echo $i; ?>)">
                                                <i class="fas fa-minus"></i>
                                            </button>
                                            <a href="javascript:void(0)" data-toggle="tooltip" title="Move" class="btn btn-outline-secondary move"><i class="fas fa-expand-arrows-alt"></i></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sub-0">Subtitles</label>
                        <div id="subsWrapper">
                            <div class="form-group" data-index="0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <?php echo subtitle_languages('lang[]'); ?>
                                        <input type="hidden" id="sub-type-0" name="sub-type[]" value="url">
                                    </div>
                                    <input type="url" id="sub-0" name="sub-url[]" class="form-control subtitle" placeholder="Subtitle URL">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-outline-primary" data-toggle="tooltip" title="Upload Subtitle" aria-label="Upload Subtitle" onclick="videos.subtitles.upload($(this))" data-index="0">
                                            <i class="fas fa-upload"></i>
                                        </button>
                                        <button type="button" class="btn btn-outline-success" data-toggle="tooltip" title="Add Subtitle" aria-label="Add Subtitle" onclick="videos.subtitles.add()">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <small class="form-text text-muted">Subtitle formats: .srt, .vtt, .ass, .sub, .stl, .dfxp, .ttml, .sbv, .txt</small>
                    </div>
                    <?php
                    if (!empty($dataV['subtitle'])) {
                        echo '<div class="form-group"><ul id="subtitles" class="list-group list-group-flush">';
                        foreach ($dataV['subtitle'] as $i => $sub) {
                            echo '<li class="list-group-item p-2" data-sub="' . $sub['id'] . '"><input type="hidden" name="subs[]" value="' . $sub['id'] . '"><div class="float-left"><a href="' . $sub['link'] . '" target="_blank">' . $sub['language'] . '</a></div><div class="float-right"><a href="javascript:void(0)" class="ml-2 text-danger" data-toggle="tooltip" title="Remove Subtitle" onclick="videos.subtitles.delete(' . $sub['id'] . ')"><i class="fas fa-trash-alt"></i></a></div></li>';
                        }
                        echo '</ul><small class="form-text text-muted">Drag and drop to change the order of subtitles</small></div>';
                    }
                    ?>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="form-group">
                        <label for="slug">Custom Slug</label>
                        <input type="text" name="slug" id="slug" value="<?php echo $slug; ?>" class="form-control" placeholder="Insert custom slug here" maxlength="50">
                    </div>
                    <div class="form-group">
                        <label for="poster">Poster</label>
                        <div id="posterURL" class="input-group">
                            <input type="url" id="poster" name="poster-url" class="form-control" placeholder="Poster Link">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-primary btn-upload-poster" data-toggle="tooltip" title="Upload Poster">
                                    <i class="fas fa-upload"></i>
                                </button>
                            </div>
                        </div>
                        <div id="posterUpload" class="input-group d-none">
                            <div class="custom-file">
                                <input type="file" id="poster" name="poster-file" class="custom-file-input form-control" accept="image/jpeg, image/png, image/webp, image/gif">
                                <label class="custom-file-label" for="poster">Choose file</label>
                            </div>
                            <div class="input-group-append">
                                <button type="button" class="btn btn-primary btn-upload-poster" data-toggle="tooltip" title="Insert Poster Link">
                                    <i class="fas fa-link"></i>
                                </button>
                            </div>
                        </div>
                        <small class="form-text text-muted">Poster formats: .jpg, .jpeg, .png, .webp, .gif</small>
                    </div>
                    <?php if (!empty($dataV['poster'])) : ?>
                        <div class="form-group">
                            <a href="<?php echo $posterURL; ?>" target="_blank" rel="noopener" title="Poster Image">
                                <img src="<?php echo $posterURL; ?>" class="img-thumbnail w-100 mb-2" alt="Poster Image">
                            </a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-danger" onclick="videos.removePoster(<?php echo !empty($dataV['id']) ? intval($dataV['id']) : ''; ?>, $(this))" rel="noopener">
                                <i class="fas fa-trash-alt mr-2"></i>Remove
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12 form-group">
                    <input type="hidden" name="id" value="<?php echo !empty($dataV['id']) ? intval($dataV['id']) : ''; ?>">
                    <button type="submit" class="btn btn-success shadow-sm mr-2">
                        <i class="fas fa-save mr-2"></i>
                        <span>Update</span>
                    </button>
                    <button type="button" class="btn btn-danger shadow-sm mr-2" onclick="videos.cache.clear.single($(this))" data-id="<?php echo !empty($dataV['id']) ? intval($dataV['id']) : ''; ?>">
                        <i class="fas fa-eraser mr-2"></i>
                        <span>Clear Cache</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-12 form-group">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Google Drive Backup Links</strong>
            </div>
            <ul class="list-group list-group-flush">
                <?php if (!empty($files)) : ?>
                    <?php
                    $class = new \Hosting();
                    foreach ($files as $row) :
                        $class->setHost('gdrive');
                        $class->setID($row['mirror_id']);
                        $link = $class->getDownloadLink();
                    ?>
                        <a href="<?php echo $link; ?>" class="list-group-item list-group-item-action" target="_blank"><?php echo $link; ?></a>
                    <?php
                    endforeach;
                    ?>
                <?php else : ?>
                    <li class="list-group-item">Not found backup links</li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <ul class="nav nav-pills nav-justified mb-3" role="tablist" aria-owns="url-tab embed-tab dl-tab req-tab json-tab">
            <li class="nav-item">
                <a role="tab" class="nav-link active" id="url-tab" data-toggle="tab" href="#turl" aria-controls="turl">Embed Link</a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" id="embed-tab" data-toggle="tab" href="#tembed" aria-controls="tembed">Embed Code</a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" id="dl-tab" data-toggle="tab" href="#tdl" aria-controls="tdl">Download Link</a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" id="req-tab" data-toggle="tab" href="#treq" aria-controls="treq">Request Link</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="turl" role="tabpanel" aria-labelledby="url-tab">
                <label for="txtEmbed">Embed Link</label>
                <textarea onfocus="this.select()" id="txtEmbed" cols="30" rows="6" class="form-control" readonly><?php echo $embedUrl; ?></textarea>
            </div>
            <div class="tab-pane fade" id="tembed" role="tabpanel" aria-labelledby="embed-tab">
                <label for="txtEmbedCode">Embed Code</label>
                <textarea onfocus="this.select()" id="txtEmbedCode" cols="30" rows="6" class="form-control" readonly><?php echo htmlentities('<iframe src="' . strtr($embedUrl, ['https:' => '', 'http:' => '']) . '" frameborder="0" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" width="640" height="320"></iframe>'); ?></textarea>
            </div>
            <div class="tab-pane fade" id="tdl" role="tabpanel" aria-labelledby="dl-tab">
                <label for="txtDl">Download Link</label>
                <textarea onfocus="this.select()" id="txtDl" cols="30" rows="6" class="form-control" readonly><?php echo $dlUrl; ?></textarea>
            </div>
            <div class="tab-pane fade" id="treq" role="tabpanel" aria-labelledby="req-tab">
                <label for="txtReq">Request Link</label>
                <textarea onfocus="this.select()" id="txtReq" cols="30" rows="6" class="form-control" readonly><?php echo $reqUrl; ?></textarea>
            </div>
        </div>
    </div>
</div>
<?php
include BASE_DIR . 'includes/link_format.php';
include $adminDir . '/footer.php';
