<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!$userLogin) {
    session_write_close();
    include $adminDir . '/views/403.php';
    exit();
} else {
    session_write_close();
    if (!empty($_POST)) {
        // upload poster
        if (!empty($_FILES['poster-file']['tmp_name'])) {
            $posterURL = upload_poster(array(
                'tmp_name' => $_FILES['poster-file']['tmp_name'],
                'name' => $_FILES['poster-file']['name'],
                'size' => $_FILES['poster-file']['size'],
                'type' => $_FILES['poster-file']['type'],
                'error' => $_FILES['poster-file']['error']
            ));
            if ($posterURL) {
                $_POST['poster'] = basename($posterURL);
            } else {
                $_POST['poster'] = '';
            }
        } elseif (validate_url($_POST['poster-url'])) {
            $_POST['poster'] = $_POST['poster-url'];
        }
        unset($_POST['poster-url']);
        unset($_POST['poster-file']);

        // upload subtitles
        $now = time();
        $subtitles = [];
        $subLabelFiles = [];
        $subLabelURLs = [];

        foreach ($_POST['sub-type'] as $i => $dt) {
            if ($dt === 'file') $subLabelFiles[] = $_POST['lang'][$i];
            elseif ($dt === 'url') $subLabelURLs[] = $_POST['lang'][$i];
        }

        $subManager = new \SubtitleManager();
        if (!empty($_FILES['sub-file']['tmp_name']) && is_array($_FILES['sub-file']['tmp_name'])) {
            foreach ($_FILES['sub-file']['tmp_name'] as $i => $dt) {
                $subtitleURL = upload_subtitle(array(
                    'tmp_name' => $_FILES['sub-file']['tmp_name'][$i],
                    'name' => $_FILES['sub-file']['name'][$i],
                    'size' => $_FILES['sub-file']['size'][$i],
                    'type' => $_FILES['sub-file']['type'][$i],
                    'error' => $_FILES['sub-file']['error'][$i]
                ));
                if ($subtitleURL) {
                    $subManager->insert(array(
                        'file_name' => basename($subtitleURL),
                        'file_size' => $_FILES['sub-file']['size'][$i],
                        'file_type' => $_FILES['sub-file']['type'][$i],
                        'language' => $subLabelFiles[$i],
                        'added' => $now,
                        'uid' => $userLogin['id'],
                        'host' => strtr(parse_url(BASE_URL, PHP_URL_HOST), ['www.' => ''])
                    ));
                    $subtitles[] = [
                        'file' => $subtitleURL,
                        'label' => $subLabelFiles[$i]
                    ];
                }
            }
        }

        // save subtitle link
        if (!empty($_POST['sub-url'])) {
            $class = new \Subscene();
            foreach ($_POST['sub-url'] as $i => $dt) {
                if (validate_url($dt)) {
                    if (strpos(parse_url($dt, PHP_URL_HOST), 'subscene') !== FALSE) {
                        $class->set_url($dt);
                        $class->download();
                        $data = $class->file();
                        if ($data) {
                            $subManager->insert(array(
                                'file_name' => basename($data['url']),
                                'file_size' => filesize($data['path']),
                                'file_type' => mime_content_type($data['path']),
                                'language' => $subLabelURLs[$i],
                                'added' => $now,
                                'uid' => $userLogin['id'],
                                'host' => strtr(parse_url(BASE_URL, PHP_URL_HOST), ['www.' => ''])
                            ));
                            $subtitles[] = [
                                'file' => $data['url'],
                                'label' => $subLabelURLs[$i]
                            ];
                        }
                    } else {
                        $subtitles[] = [
                            'file' => $dt,
                            'label' => $subLabelURLs[$i]
                        ];
                    }
                }
            }
        }
        unset($_POST['sub-type']);
        unset($_POST['sub-file']);
        unset($_POST['sub-url']);
        unset($_POST['lang']);

        // custom slug
        $slug = slugify($_POST['slug']);
        unset($_POST['slug']);

        $alternatives = $_POST['altLinks'];
        $alternatives = array_filter($alternatives);
        $alternatives = array_values($alternatives);
        unset($_POST['altLinks']);

        $hosting = new \Hosting();
        $hosting->setURL($_POST['host_id']);
        $_POST['host'] = $hosting->getHost();
        $_POST['host_id'] = $hosting->getID();

        $_POST['added'] = $now;
        $_POST['updated'] = $now;
        $_POST['uid'] = $userLogin['id'];
        $_POST['ahost'] = '';
        $_POST['ahost_id'] = '';

        $class = new \Videos();
        $id = $class->insert($_POST);
        if ($id) {
            $uuid = new \UUID();
            $class = new \VideoShort();
            $class->setCriteria('vid', $id);
            $short = $class->getOne(['key']);
            if ($short) {
                if (!empty($slug)) {
                    if ($slug !== $short['key']) {
                        $class->setCriteria('vid', $id);
                        $newKey = $class->update(array(
                            'vid' => $id,
                            'key' => $slug
                        ));
                    }
                } else {
                    $newKey = $class->insert(array(
                        'vid' => $id,
                        'key' => $uuid->v4()
                    ));
                }
            } else {
                if (!empty($slug)) {
                    $newKey = $class->insert(array(
                        'vid' => $id,
                        'key' => $slug
                    ));
                } else {
                    $newKey = $class->insert(array(
                        'vid' => $id,
                        'key' => $uuid->v4()
                    ));
                }
            }

            if (!empty($alternatives)) {
                $class = new \VideosAlternatives();
                foreach ($alternatives as $i => $url) {
                    if (validate_url($url)) {
                        $hosting->setURL($url);
                        $class->insert(array(
                            'vid' => $id,
                            'host' => $hosting->getHost(),
                            'host_id' => $hosting->getID(),
                            'order' => $i
                        ));
                    }
                }
            }

            if (!empty($subtitles)) {
                $class = new \Subtitles();
                foreach ($subtitles as $i => $dt) {
                    $class->insert(array(
                        'vid' => $id,
                        'language' => $dt['label'],
                        'link' => $dt['file'],
                        'added' => $now,
                        'uid' => $userLogin['id'],
                        'order' => $i
                    ));
                }
            }
            create_alert('success', 'New video added successfully.', strtr(rtrim($_SERVER['REQUEST_URI'], '/'), ['/new' => '/edit/?id=' . $id]));
        } else {
            create_alert('danger', $class->getLastError(), $_SERVER['REQUEST_URI']);
        }
    }
}

$title = 'New Video';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">New Video</h1>
        <form action="<?php echo $adminURL; ?>/videos/new" method="post" class="needs-validation" enctype="multipart/form-data" novalidate>
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="form-group">
                        <label for="title"> Video Title</label>
                        <input type="text" name="title" id="title" class="form-control" placeholder="Insert video title here" required>
                    </div>
                    <div class="form-group">
                        <label for="host_id">Main Video URL</label>
                        <div class="input-group">
                            <input type="url" id="host_id" name="host_id" class="form-control" placeholder="Insert main video url here" required>
                            <div class="input-group-append">
                                <button type="button" class="btn btn-info" onclick="videos.scrollToHosts()" data-toggle="tooltip" title="Example Link">
                                    <i class="fas fa-info-circle"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="altLink-0">Alternative Video URLs</label>
                        <div id="altWrapper">
                            <div class="form-group" data-index="0">
                                <div class="input-group">
                                    <input type="url" id="altLink-0" name="altLinks[]" class="form-control" placeholder="Insert alternative video url here">
                                    <div class="input-group-append">
                                        <button type="button" data-toggle="tooltip" title="Add Alternative Video URL" class="btn btn-custom" onclick="videos.addAlternativeHTML()">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sub-0">Subtitles</label>
                        <div id="subsWrapper">
                            <div class="form-group" data-index="0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <?php echo subtitle_languages('lang[]'); ?>
                                        <input type="hidden" id="sub-type-0" name="sub-type[]" value="url">
                                    </div>
                                    <input type="url" id="sub-0" name="sub-url[]" class="form-control subtitle" placeholder="Subtitle URL">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-outline-primary" data-toggle="tooltip" title="Upload Subtitle" aria-label="Upload Subtitle" onclick="videos.subtitles.upload($(this))" data-index="0">
                                            <i class="fas fa-upload"></i>
                                        </button>
                                        <button type="button" class="btn btn-outline-success" data-toggle="tooltip" title="Add Subtitle" aria-label="Add Subtitle" onclick="videos.subtitles.add()">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <small class="form-text text-muted">Subtitle formats: .srt, .vtt, .ass, .sub, .stl, .dfxp, .ttml, .sbv, .txt</small>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="form-group">
                        <label for="slug">Custom Slug</label>
                        <input type="text" name="slug" id="slug" class="form-control" placeholder="Insert custom slug here" maxlength="50">
                    </div>
                    <div class="form-group">
                        <label for="poster">Poster</label>
                        <div id="posterURL" class="input-group">
                            <input type="url" id="poster" name="poster-url" class="form-control" placeholder="Poster Link">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-primary btn-upload-poster" data-toggle="tooltip" title="Upload Poster">
                                    <i class="fas fa-upload"></i>
                                </button>
                            </div>
                        </div>
                        <div id="posterUpload" class="input-group d-none">
                            <div class="custom-file">
                                <input type="file" id="poster" name="poster-file" class="custom-file-input form-control" accept=".jpg, .jpeg, .png, .webp, .gif">
                                <label class="custom-file-label" for="poster">Choose file</label>
                            </div>
                            <div class="input-group-append">
                                <button type="button" class="btn btn-primary btn-upload-poster" data-toggle="tooltip" title="Insert Poster URL">
                                    <i class="fas fa-link"></i>
                                </button>
                            </div>
                        </div>
                        <small class="form-text text-muted">Poster formats: .jpg, .jpeg, .png, .webp, .gif</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 form-group">
                    <button type="submit" class="btn btn-success shadow-sm mr-2">
                        <i class="fas fa-save mr-2"></i>
                        <span>Save</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
include BASE_DIR . 'includes/link_format.php';
include $adminDir . '/footer.php';
