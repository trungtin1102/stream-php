


<?php

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Streaming\Representation;

require $_SERVER['DOCUMENT_ROOT'].'/includes/config.php';


if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!$userLogin) {
    session_write_close();
    include $adminDir . '/views/403.php';
    exit();
} else {

    session_write_close();
    if (!empty($_POST)) {

        ini_set('display_errors', 'On');
        $googleDrive = "https://drive.google.com/file/d/";

        if(substr($_POST['host_id'], 0, 32) == $googleDrive){

            $title = str_slug($_POST['title']);
            $folder_embed = $_SERVER['DOCUMENT_ROOT'] .'/embed/'. $title;
            if(!file_exists($folder_embed)){
                mkdir("./embed/".$title, 0777, true);
            }

            $length = strpos( $_POST['host_id'], '/view');
            $id = substr($_POST['host_id'], 32, ($length - 32));
            $dir = 'uploads/' .$title. '.mp4';

            $cmd = "wget --load-cookies /tmp/cookies.txt " .'"https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate ' . "'https://docs.google.com/uc?export=download&id=".$id."'" . " -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=".$id."" . '" -O '.$dir;
            shell_exec($cmd);

            try {
                $config = [
                    'ffmpeg.binaries'  => '/usr/bin/ffmpeg', // the path to the FFMpeg binary
                    'ffprobe.binaries' => '/usr/bin/ffprobe', // the path to the FFProbe binary
                    'timeout'          => 3600, // the timeout for the underlying process
                    'ffmpeg.threads'   => 12,   // the number of threads that FFMpeg should use
                ];

                $output = $_SERVER['DOCUMENT_ROOT']. "/embed/". $title . '/' . $title .".m3u8";

                $log = new Logger($dir);
                $log->pushHandler(new StreamHandler('logs/convert-videos.log'));
                $ffmpeg = Streaming\FFMpeg::create($config, $log);

                $video = $ffmpeg->open($dir);

                $script = "/usr/bin/ffmpeg -i $dir -s 1280x720 -bsf:v h264_mp4toannexb -codec: copy -start_number 0 -hls_time 10 -hls_list_size 0 -f hls $output";

                shell_exec($script);

                unlink($_SERVER['DOCUMENT_ROOT'] .'/'.$dir);

                // convertVideo($script);

                $_POST['folder'] = $title;
                $_POST['embed_code'] = md5($title);

                $video = new \HLSVideos();
                $video->insert(array(
                    'title' => $_POST['title'],
                    'folder' => $title,
                    'embed_code' => md5($title)
                ));

                create_alert('success', 'New video added successfully.', BASE_URL. ADMIN_DIR . '/hls/new');

            }
            catch(Exception $e) {
                echo 'Message: ' .$e->getMessage(). ' | ' . $e->getLine(). ' | ' . $e->getFile(); die;
            }
            create_alert('success', 'New video added successfully.', BASE_URL. ADMIN_DIR . '/hls/new');
        }else{
            create_alert('danger', "Link doesn't supported !", BASE_URL. ADMIN_DIR . '/hls/new');
        }

        // upload subtitles
        $now = time();

        // if ($id) {
        //     $uuid = new \UUID();
        //     $class = new \VideoShort();
        //     $class->setCriteria('vid', $id);
        //     $short = $class->getOne(['key']);
        //     if ($short) {
        //         if (!empty($slug)) {
        //             if ($slug !== $short['key']) {
        //                 $class->setCriteria('vid', $id);
        //                 $newKey = $class->update(array(
        //                     'vid' => $id,
        //                     'key' => $slug
        //                 ));
        //             }
        //         } else {
        //             $newKey = $class->insert(array(
        //                 'vid' => $id,
        //                 'key' => $uuid->v4()
        //             ));
        //         }
        //     } else {
        //         if (!empty($slug)) {
        //             $newKey = $class->insert(array(
        //                 'vid' => $id,
        //                 'key' => $slug
        //             ));
        //         } else {
        //             $newKey = $class->insert(array(
        //                 'vid' => $id,
        //                 'key' => $uuid->v4()
        //             ));
        //         }
        //     }

        //     if (!empty($alternatives)) {
        //         $class = new \VideosAlternatives();
        //         foreach ($alternatives as $i => $url) {
        //             if (validate_url($url)) {
        //                 $hosting->setURL($url);
        //                 $class->insert(array(
        //                     'vid' => $id,
        //                     'host' => $hosting->getHost(),
        //                     'host_id' => $hosting->getID(),
        //                     'order' => $i
        //                 ));
        //             }
        //         }
        //     }

        //     if (!empty($subtitles)) {
        //         $class = new \Subtitles();
        //         foreach ($subtitles as $i => $dt) {
        //             $class->insert(array(
        //                 'vid' => $id,
        //                 'language' => $dt['label'],
        //                 'link' => $dt['file'],
        //                 'added' => $now,
        //                 'uid' => $userLogin['id'],
        //                 'order' => $i
        //             ));
        //         }
        //     }
        //     create_alert('success', 'New video added successfully.', strtr(rtrim($_SERVER['REQUEST_URI'], '/'), ['/new' => '/edit/?id=' . $id]));
        // } else {
        //     create_alert('danger', $class->getLastError(), $_SERVER['REQUEST_URI']);
        // }
    }
}

$title = 'New Video';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">New Video Remote</h1>
        <form action="<?php echo $adminURL; ?>/hls/new" method="post" class="needs-validation" enctype="multipart/form-data" novalidate>
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="form-group">
                        <label for="title"> Video Title</label>
                        <input type="text" name="title" id="title" class="form-control" placeholder="Insert video title here" required>
                    </div>
                    <div class="form-group">
                        <label for="host_id">Video URL Remote</label>
                        <div class="input-group">
                            <input type="url" id="host_id" name="host_id" class="form-control" placeholder="Insert main video url here" required>
                            <div class="input-group-append">
                                <button type="button" class="btn btn-info" onclick="videos.scrollToHosts()" data-toggle="tooltip" title="Example Link">
                                    <i class="fas fa-info-circle"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 form-group">
                    <button type="submit" class="btn btn-success shadow-sm mr-2">
                        <i class="fas fa-save mr-2"></i>
                        <span>Save</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php
include BASE_DIR . 'includes/link_format_remote.php';
include $adminDir . '/footer.php';
