<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!is_admin()) {
    session_write_close();
    include $adminDir . '/views/403.php';
    exit();
} else {
    session_write_close();
    if (!empty($_POST)) {
        $InstanceCache->deleteItem('gdrive_accounts');
        $userLogin = current_user();

        $time = time();
        $_POST['status'] = isset($_POST['status']) ? 1 : 0;
        $_POST['uid'] = $userLogin['id'];
        $_POST['created'] = $time;
        $_POST['modified'] = $time;

        $class  = new \GDriveAuth();
        $id = $class->insert($_POST);
        if ($id) {
            create_alert('success', 'New Google Account added successfully.', strtr(rtrim($_SERVER['REQUEST_URI'], '/'), ['/new' => '/edit/?id=' . $id]));
        } else {
            create_alert('danger', $class->getLastError(), $_SERVER['REQUEST_URI']);
        }
    }
}

$title = 'New Google Drive Account';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">New Google Drive Account</h1>
        <form action="<?php echo $adminURL; ?>/gdrive_accounts/new/" method="post" class="needs-validation" novalidate>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" class="form-control" maxlength="100" required>
                        <div class="invalid-feedback">Must be valid!</div>
                    </div>
                    <div class="form-group">
                        <label for="api_key">API Key</label>
                        <input type="text" name="api_key" id="api_key" class="form-control" maxlength="50" required>
                        <div class="invalid-feedback">Must be filled!</div>
                    </div>
                    <div class="form-group">
                        <label for="client_id">Client ID</label>
                        <input type="text" name="client_id" id="client_id" class="form-control" maxlength="100" required>
                        <div class="invalid-feedback">Must be filled!</div>
                    </div>
                    <div class="form-group">
                        <label for="client_secret">Client Secret</label>
                        <input type="text" name="client_secret" id="client_secret" class="form-control" maxlength="50" required>
                        <div class="invalid-feedback">Must be filled!</div>
                    </div>
                    <div class="form-group">
                        <label for="refresh_token">Refresh Token</label>
                        <input type="text" name="refresh_token" id="refresh_token" class="form-control" maxlength="150" required>
                        <div class="invalid-feedback">Must be filled!</div>
                    </div>
                    <div class="form-group row">
                        <label for="status" class="col-2">Status</label>
                        <div class="custom-control custom-checkbox col-10">
                            <input type="checkbox" class="custom-control-input" name="status" id="status">
                            <label class="custom-control-label" for="status">Enable</label>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <button type="submit" class="btn btn-success shadow-sm mr-2">
                        <i class="fas fa-save mr-2"></i>
                        <span>Save</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
