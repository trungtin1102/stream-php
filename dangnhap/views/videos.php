<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!$userLogin) {
    include $adminDir . '/views/402.php';
    exit();
}

$title = 'Video List';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">Video List</h1>
        <div id="toolbar">
            <a href="<?php echo $adminURL; ?>/videos/new" class="btn shadow-sm btn-success btn-sm mr-2 mb-3" data-toggle="tooltip" title="Add New">
                <i class="fas fa-plus-circle"></i>
                <span class="ml-2 d-none d-md-inline">Add New</span>
            </a>
            <button type="button" class="btn shadow-sm btn-primary btn-sm mr-2 mb-3" data-toggle="modal" data-target="#mdVideosBulkLink" data-tooltip="true" title="Add Bulk Link">
                <i class="fas fa-plus-square"></i>
                <span class="ml-2 d-none d-md-inline">Add Bulk Videos</span>
            </button>
            <button id="btnChecker" type="button" class="btn shadow-sm btn-warning btn-sm mr-2 mb-3 d-none" onclick="videos.checker.multi($(this))" data-toggle="tooltip" title="Video Checker">
                <i class="fas fa-check-circle"></i>
                <span class="ml-2 d-none d-md-inline">Video Checker</span>
            </button>
            <button id="btnClear" type="button" class="btn shadow-sm btn-secondary btn-sm mr-2 mb-3 d-none" onclick="videos.cache.clear.multi($(this))" data-toggle="tooltip" title="Clear Cache">
                <i class="fas fa-eraser"></i>
                <span class="ml-2 d-none d-md-inline">Clear Cache</span>
            </button>
            <button id="btnDelete" type="button" class="btn shadow-sm btn-danger btn-sm mr-2 mb-3 d-none" onclick="videos.delete.multi($(this))" data-toggle="tooltip" title="Delete">
                <i class="fas fa-trash-alt"></i>
                <span class="ml-2 d-none d-md-inline">Delete</span>
            </button>
            <button type="button" class="btn shadow-sm btn-info btn-sm mr-2 mb-3" onclick="videos.reload()" data-toggle="tooltip" title="Reload">
                <i class="fas fa-sync-alt"></i>
                <span class="ml-2 d-none d-md-inline">Reload</span>
            </button>
            <div class="dropdown d-inline-block">
                <button class="btn shadow-sm btn-custom btn-sm mr-2 mb-3 dropdown-toggle" class="btn shadow-sm btn-primary btn-sm" type="button" id="dropdownStatus" data-toggle="dropdown" aria-expanded="false" data-tooltip="true" title="Status">
                    <i class="fas fa-eye"></i>
                    <span class="ml-2 d-none d-md-inline">Status</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right shadow border-0" aria-labelledby="dropdownStatus">
                    <a class="dropdown-item" href="<?php echo $adminURL . '/videos/'; ?>">
                        <i class="fas fa-sync-alt mr-2"></i>All
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?php echo $adminURL . '/videos/?status=0'; ?>">
                        <i class="fas fa-check-circle text-success mr-2"></i>Good
                    </a>
                    <a class="dropdown-item" href="<?php echo $adminURL . '/videos/?status=2'; ?>">
                        <i class="fas fa-exclamation-circle text-warning mr-2"></i>Warning
                    </a>
                    <a class="dropdown-item" href="<?php echo $adminURL . '/videos/?status=1'; ?>">
                        <i class="fas fa-times-circle text-danger mr-2"></i>Broken
                    </a>
                </div>
            </div>
        </div>
        <table id="tbVideos" class="table table-striped table-bordered table-hover table-sm" style="width:100%">
            <thead>
                <tr>
                    <th style="max-width:36px!important">
                        <div class="custom-control custom-checkbox mx-auto">
                            <input type="checkbox" class="custom-control-input" id="ckAllVideos">
                            <label class="custom-control-label" for="ckAllVideos"></label>
                        </div>
                    </th>
                    <th>Title</th>
                    <th>Sources</th>
                    <th>Status</th>
                    <th>Subtitles</th>
                    <th>Views</th>
                    <th>User</th>
                    <th style="min-width:145px!important">Updated On</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th style="max-width:36px!important">
                        <div class="custom-control custom-checkbox mx-auto">
                            <input type="checkbox" class="custom-control-input" id="ckAllVideos1">
                            <label class="custom-control-label" for="ckAllVideos1"></label>
                        </div>
                    </th>
                    <th>Title</th>
                    <th>Sources</th>
                    <th>Status</th>
                    <th>Subtitles</th>
                    <th>Views</th>
                    <th>User</th>
                    <th style="min-width:145px!important">Updated On</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<div class="modal fade" id="mdVideosBulkLink" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdVideosBulkLinkLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mdVideosBulkLinkLabel">Add Bulk Videos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning mb-3">You can insert <strong>unlimited video links</strong> here but there will be a <strong>3 second delay</strong> for each link.</div>
                <form id="frmBulkVideo">
                    <div class="form-group">
                        <textarea name="links" id="links" cols="30" rows="8" class="form-control" placeholder="Insert video URL per line" required></textarea>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="useTitleAsSlug" name="useTitleAsSlug">
                            <label class="custom-control-label" for="useTitleAsSlug">Use Title As Slug</label>
                        </div>
                        <small class="form-text text-muted">If the url slug exists or the title exist/empty then the url slug will be generated randomly.</small>
                    </div>
                </form>
                <div class="form-group m-0">
                    <div class="table-responsive">
                        <table id="tbBulkVideos" class="table table-striped table-bordered table-hover table-sm m-0" style="width:100%">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="ckAllBulkVideos">
                                            <label class="custom-control-label" for="ckAllBulkVideos"></label>
                                        </div>
                                    </th>
                                    <th>Title</th>
                                    <th>Data</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="ckAllBulkVideos1">
                                            <label class="custom-control-label" for="ckAllBulkVideos1"></label>
                                        </div>
                                    </th>
                                    <th>Title</th>
                                    <th>Data</th>
                                    <th>Status</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnGetEmbedURLs" onclick="videos.bulk.get_embed()" class="btn btn-primary btn-sm mr-1" disabled>Get Embed URLs</button>
                <button type="button" id="btnBulkSave" onclick="videos.bulk.save($(this))" class="btn shadow-sm btn-success btn-sm mr-1" disabled>
                    <i class="fas fa-save"></i>
                    <span class="ml-2">Save</span>
                </button>
                <button type="reset" class="btn shadow-sm btn-danger btn-sm" data-dismiss="modal">
                    <i class="fas fa-times"></i>
                    <span class="ml-2">Close</span>
                </button>
            </div>
        </div>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
