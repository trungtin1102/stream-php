<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!is_admin()) {
    include $adminDir . '/views/403.php';
    exit();
}

$excludes = gdrive_rent_excludes();
$class = new \GDriveAuth();
$class->setCriteria('status', 1);
$class->setOrderBy('email', 'ASC');
$result = $class->get(['email']);
$gdrive_list = [];
if ($result) {
    if (file_exists(BASE_DIR . '.rent')) {
        foreach ($result as $dt) {
            if (!in_array($dt['email'], $excludes)) $gdrive_list[] = $dt;
        }
    } else {
        $gdrive_list = $result;
    }
}

$title = 'Google Drive Videos';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">Google Drive Videos</h1>
        <p>List of videos found in your Google Drive account. If you add video files to your Google Drive account directly and from the anti-limit feature they will be displayed here.</p>
        <div class="form-row">
            <div class="col-12 col-md-4 mb-3">
                <select name="email" id="email" class="custom-select custom-select-sm">
                    <option value="">-- Select Account --</option>
                    <?php
                    foreach ($gdrive_list as $dt) {
                        echo '<option value="' . $dt['email'] . '" ' . (isset($_GET['email']) && htmlspecialchars($_GET['email']) === $dt['email'] ? 'selected' : '') . '>' . $dt['email'] . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="col-12 col-md-8 mb-3">
                <button type="button" class="btn shadow-sm btn-primary btn-sm mr-2" onclick="gdrive_files.newFolder()">
                    <i class="fas fa-folder-plus"></i><span class="d-none d-md-inline-block ml-2">New Folder</span>
                </button>
                <div class="dropdown d-inline-block">
                    <button class="btn btn-secondary btn-sm mr-2 dropdown-toggle" type="button" id="changeStatus" data-toggle="dropdown" aria-expanded="false">
                        <i class="fas fa-eye"></i><span class="d-none d-md-inline-block ml-2">Change Status</span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="changeStatus">
                        <a class="dropdown-item" href="javascript:void(0)" onclick="gdrive_files.publicChecked()">
                            <i class="fas fa-eye"></i><span class="ml-2">Public</span>
                        </a>
                        <a class="dropdown-item" href="javascript:void(0)" onclick="gdrive_files.privateChecked()">
                            <i class="fas fa-eye-slash"></i><span class="ml-2">Private</span>
                        </a>
                    </div>
                </div>
                <button type="button" class="btn shadow-sm btn-danger btn-sm mr-2" onclick="gdrive_files.deleteChecked()">
                    <i class="fas fa-trash-alt"></i><span class="d-none d-md-inline-block ml-2">Delete</span>
                </button>
                <button type="button" class="btn shadow-sm btn-info btn-sm mr-2" onclick="gdrive_files.reload()">
                    <i class="fas fa-sync-alt"></i><span class="d-none d-md-inline-block ml-2">Reload</span>
                </button>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="onlyPrivate" value="true" onchange="gdrive_files.reload()">
                    <label class="custom-control-label" for="onlyPrivate">Show Private Files/Folders Only</label>
                </div>
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="onlyFolder" value="true" onchange="gdrive_files.reload()">
                    <label class="custom-control-label" for="onlyFolder">Show Folders Only</label>
                </div>
            </div>
        </div>
        <?php if (!empty($_GET['folder_id'])) : ?>
            <div class="row mb-3">
                <div class="col">
                    <a href="javascript:void(0)" onclick="history.back()" class="btn shadow-sm btn-secondary btn-sm mr-2">
                        <i class="fas fa-arrow-left mr-1"></i>Go Back
                    </a>
                </div>
            </div>
        <?php endif; ?>
        <table id="tbGDFiles" class="table table-striped table-bordered table-hover table-sm m-0" style="width:100%">
            <thead>
                <tr>
                    <th style="width:60px;max-width:60px">
                        <div class="custom-control custom-checkbox mx-auto">
                            <input type="checkbox" class="custom-control-input" id="ckAll">
                            <label class="custom-control-label" for="ckAll"></label>
                        </div>
                    </th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Modified On</th>
                    <th>Shared</th>
                    <th>Editable</th>
                    <th>Copyable</th>
                    <th style="width:100px">Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th style="width:60px;max-width:60px">
                        <div class="custom-control custom-checkbox mx-auto">
                            <input type="checkbox" class="custom-control-input" id="ckAll1">
                            <label class="custom-control-label" for="ckAll1"></label>
                        </div>
                    </th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Modified On</th>
                    <th>Shared</th>
                    <th>Editable</th>
                    <th>Copyable</th>
                    <th style="width:100px">Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
