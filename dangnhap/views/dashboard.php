<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!$userLogin) {
    include $adminDir . '/views/402.php';
    exit();
}

$isAdmin = is_admin();
$title = 'Dashboard';
include $adminDir . '/header.php';

$class = new \ServerInfo();
?>
<div class="row mb-3">
    <div class="col-12 col-md-4 col-lg-3">
        <?php if ($isAdmin) : ?>
            <div class="card shadow-sm mb-3">
                <div class="card-body py-2">
                    <div class="media">
                        <i class="fas fa-film fa-2x align-self-center mr-3 text-success"></i>
                        <div class="media-body">
                            <a href="<?php echo $adminURL . '/videos/'; ?>">
                                <h4 class="m-0"><?php echo $class->getNumVideos(); ?></h4>
                            </a>
                            <small class="text-muted">Total Links</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow-sm mb-3">
                <div class="card-body py-2">
                    <div class="media">
                        <i class="fas fa-film fa-2x align-self-center mr-3 text-danger"></i>
                        <div class="media-body">
                            <a href="<?php echo $adminURL . '/videos/?status=1'; ?>">
                                <h4 class="m-0"><?php echo $class->getNumVideos(null, 1); ?></h4>
                            </a>
                            <small class="text-muted">Broken Links</small>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($isAdmin && !file_exists(BASE_DIR . '.rent')) : ?>
                <div class="card shadow-sm mb-3">
                    <div class="card-body py-2">
                        <div class="media">
                            <i class="fas fa-server fa-2x align-self-center mr-3"></i>
                            <div class="media-body">
                                <a href="<?php echo $adminURL . '/load_balancers/'; ?>">
                                    <h4 class="m-0"><?php echo $class->getNumServers(); ?></h4>
                                </a>
                                <small class="text-muted">Total Servers</small>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="card shadow-sm mb-3">
                <div class="card-body py-2">
                    <div class="media">
                        <i class="fab fa-google-drive fa-2x text-primary align-self-center mr-3"></i>
                        <div class="media-body">
                            <a href="<?php echo $adminURL . '/gdrive_accounts/'; ?>">
                                <h4 class="m-0"><?php echo $class->getNumGDriveAccounts(); ?></h4>
                            </a>
                            <small class="text-muted">Google Drive Accounts</small>
                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <div class="card shadow-sm mb-3">
                <div class="card-body py-2">
                    <div class="media">
                        <i class="fas fa-film fa-2x align-self-center mr-3 text-success"></i>
                        <div class="media-body">
                            <a href="<?php echo $adminURL . '/videos/'; ?>">
                                <h4 class="m-0"><?php echo $class->getNumVideos($userLogin['id']); ?></h4>
                            </a>
                            <small class="text-muted">Total Links</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow-sm mb-3">
                <div class="card-body py-2">
                    <div class="media">
                        <i class="fas fa-film fa-2x align-self-center mr-3 text-danger"></i>
                        <div class="media-body">
                            <a href="<?php echo $adminURL . '/videos/?status=1'; ?>">
                                <h4 class="m-0"><?php echo $class->getNumVideos($userLogin['id'], 1); ?></h4>
                            </a>
                            <small class="text-muted">Broken Links</small>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="card shadow-sm mb-3">
            <div class="card-header">
                <h3 class="h5 float-left">Videos Status</h3>
                <button class="btn btn-outline-secondary btn-sm float-right" onclick="dashboard.chart.videoStatus('.video-status')" data-toggle="tooltip" title="Reload">
                    <i class="fas fa-sync-alt"></i>
                </button>
            </div>
            <div class="card-body p-2">
                <div class="video-status mx-auto text-center">
                    <i class="fas fa-spin fa-sync-alt text-primary" style="font-size:50px"></i>
                    <div class="chart"></div>
                </div>
            </div>
        </div>
        <?php if ($isAdmin && !file_exists(BASE_DIR . '.rent')) : ?>
            <div class="card shadow-sm mb-3">
                <div class="card-header">
                    <h3 class="h5 float-left">Servers Usage</h3>
                    <button class="btn btn-outline-secondary btn-sm float-right" onclick="dashboard.chart.serverStatus('.server-status')" data-toggle="tooltip" title="Reload">
                        <i class="fas fa-sync-alt"></i>
                    </button>
                </div>
                <div class="card-body p-2">
                    <div class="server-status mx-auto text-center">
                        <i class="fas fa-spin fa-sync-alt text-primary" style="font-size:50px"></i>
                        <div class="chart"></div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalExtApps" tabindex="-1" role="dialog" aria-labelledby="modalExtAppsLabel" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="modalExtAppsLabel">Support Checker</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span>&times;</span>
                            </button>
                        </div>
                        <div class="modal-body p-0">
                            <div class="alert alert-warning rounded-0">If you want to use <strong>Facebook</strong>, <strong>OneDrive</strong>, <strong>StreamSB</strong> and <strong>Vidio</strong> then you have to enable php functions and install the following applications.</div>
                            <table class="table-sm m-3">
                                <tbody>
                                    <tr>
                                        <td><strong>exec</strong></td>
                                        <td><span id="extExecChecker"><i class="fas fa-times-circle text-danger"></i> Disabled</span></td>
                                    </tr>
                                    <tr>
                                        <td><strong>shell_exec</strong></td>
                                        <td><span id="extShell_ExecChecker"><i class="fas fa-times-circle text-danger"></i> Disabled</span></td>
                                    </tr>
                                    <tr>
                                        <td><strong>popen</strong></td>
                                        <td><span id="extPopenChecker"><i class="fas fa-times-circle text-danger"></i> Disabled</span></td>
                                    </tr>
                                    <tr>
                                        <td><strong>proc_open</strong></td>
                                        <td><span id="extProc_OpenChecker"><i class="fas fa-times-circle text-danger"></i> Disabled</span></td>
                                    </tr>
                                    <tr>
                                        <td><strong>proc_get_status</strong></td>
                                        <td><span id="extProc_Get_StatusChecker"><i class="fas fa-times-circle text-danger"></i> Disabled</span></td>
                                    </tr>
                                    <tr>
                                        <td><strong>proc_close</strong></td>
                                        <td><span id="extProc_CloseChecker"><i class="fas fa-times-circle text-danger"></i> Disabled</span></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Google Chrome</strong><br><a href="https://linuxize.com/tags/chrome/" target="_blank">Install Now</a></td>
                                        <td><span id="appChromeChecker"><i class="fas fa-times-circle text-danger"></i> Disabled</span></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="form-group">
                                <div class="custom-checkbox"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" onclick="location.reload()">Re-check</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="col-12 col-md-8 col-lg-9">
        <div class="card shadow-sm mb-3">
            <div class="card-header">
                <h3 class="h5 float-left">Popular Videos</h3>
                <button class="btn btn-outline-secondary btn-sm float-right" onclick="$('#tbPopularVideos').DataTable().ajax.reload(null, false)" data-toggle="tooltip" title="Reload">
                    <i class="fas fa-sync-alt"></i>
                </button>
            </div>
            <div class="card-body p-0">
                <table id="tbPopularVideos" class="table table-striped table-hover table-sm m-0" style="width:100%">
                    <thead>
                        <tr>
                            <th style="max-width:300px!important">Title</th>
                            <th>Sources</th>
                            <th>Views</th>
                            <th>User</th>
                            <th style="min-width:145px!important">Added On</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="max-width:300px!important">Title</th>
                            <th>Sources</th>
                            <th>Views</th>
                            <th>User</th>
                            <th style="min-width:145px!important">Added On</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="h5 float-left">Recently Added Videos</h3>
                <button class="btn btn-outline-secondary btn-sm float-right" onclick="$('#tbRecentVideos').DataTable().ajax.reload(null, false)" data-toggle="tooltip" title="Reload">
                    <i class="fas fa-sync-alt"></i>
                </button>
            </div>
            <div class="card-body p-0">
                <table id="tbRecentVideos" class="table table-striped table-hover table-sm m-0" style="width:100%">
                    <thead>
                        <tr>
                            <th style="max-width:300px!important">Title</th>
                            <th>Sources</th>
                            <th>Views</th>
                            <th>User</th>
                            <th style="min-width:145px!important">Added On</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="max-width:300px!important">Title</th>
                            <th>Sources</th>
                            <th>Views</th>
                            <th>User</th>
                            <th style="min-width:145px!important">Added On</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="h5 float-left">Statistics</h3>
                <button class="btn btn-outline-secondary btn-sm float-right" onclick="dashboard.chart.views()" data-toggle="tooltip" title="Reload">
                    <i class="fas fa-sync-alt"></i>
                </button>
            </div>
            <div class="card-body p-2">
                <div id="views" class="mx-auto text-center">
                    <div class="toolbar mb-3">
                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-outline-custom btn-sm">
                                <input type="radio" name="options" value="last_year"> Last Year
                            </label>
                            <label class="btn btn-outline-custom btn-sm active">
                                <input type="radio" name="options" value="seven_days" checked> 7 Days
                            </label>
                            <label class="btn btn-outline-custom btn-sm">
                                <input type="radio" name="options" value="one_month"> 1 Month
                            </label>
                            <label class="btn btn-outline-custom btn-sm">
                                <input type="radio" name="options" value="six_months"> 6 Months
                            </label>
                            <label class="btn btn-outline-custom btn-sm">
                                <input type="radio" name="options" value="this_year"> This Year
                            </label>
                        </div>
                    </div>
                    <i class="fas fa-spin fa-sync-alt text-primary mb-3" style="font-size:50px"></i>
                    <div class="chart"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
