<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (current_user()) {
    session_write_close();
    header('location: ' . $adminURL . '/dashboard/');
    exit();
} elseif (isset($_POST['submit'])) {
    session_write_close();
    $login = new \Login();
    if ($login->cek_salah_login()) {
        //kalau user salah login melebihi batas yang ditentukan, maka proses langsung berhenti
        create_alert('warning', 'Sorry you can\'t log in again because you have logged in too many errors. Contact the Administrator for more information.', $adminURL . '/login/');
    } else {
        $captcha = isset($_POST['captcha-response']) ? $_POST['captcha-response'] : '';
        $recaptchaIsValid = recaptcha_validate($captcha);
        if ($recaptchaIsValid) {
            if (!empty($_POST['username']) && !empty($_POST['password'])) {
                $class = new \Users();
                $class->setCriteria('user', $_POST['username']);
                $class->setCriteria('email', $_POST['username'], '=', 'OR');
                $data = $class->getOne();
                if ($data) {
                    $status = intval($data['status']);
                    $verified = password_verify($_POST['password'], $data['password']);
                    if ($verified) {
                        if ($status === 2) {
                            //status pending
                            $login->salah_login_action($_POST['username']);
                            create_alert('warning', 'Your account is awaiting approval! Please contact admin for more information.', $adminURL . '/login/');
                        } elseif ($status === 0) {
                            //status nonaktif
                            $login->salah_login_action($_POST['username']);
                            create_alert('danger', 'Your account is currently inactive! Please contact admin for more information.', $adminURL . '/login/');
                        } else {
                            //password sudah cocok
                            $expired = isset($_POST['remember']) ? '+7 days' : '+1 day';
                            $login->true_login($_POST['username'], $expired);
                            create_alert('success', 'You have successfully logged in!', $adminURL . '/dashboard/');
                        }
                    } else {
                        //password tidak cocok
                        $login->salah_login_action($_POST['username']);
                        create_alert('danger', 'Incorrect username or password! Please try again later.', $adminURL . '/login/');
                    }
                } else {
                    $login->salah_login_action($_POST['username']);
                    create_alert('danger', 'The account is not registered!', $adminURL . '/login/');
                }
            } else {
                create_alert('danger', 'Username and password must be filled in!', $adminURL . '/login/');
            }
        } else {
            $login->salah_login_action($_POST['username']);
            create_alert('danger', 'The security code entered is incorrect!', $adminURL . '/login/');
        }
    }
}

$title = 'Login';
include $adminDir . '/header.php';
?>
<div class="row py-5">
    <div class="col-12 col-md-6 col-lg-4 mx-auto">
        <h1 class="h3 my-3 text-center">Login</h1>
        <?php echo show_alert(); ?>
        <form id="frmLogin" action="<?php echo $adminURL . '/login/'; ?>" method="post" class="needs-validation" novalidate>
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" id="username" name="username" class="form-control" placeholder="Enter your username / email address" required>
                <div class="invalid-feedback">Must be filled!</div>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" autocomplete="false" id="password" name="password" class="form-control" placeholder="Enter your password" required>
                <div class="invalid-feedback">Must be filled!</div>
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="rmb" name="remember" value="1">
                    <label class="custom-control-label" for="rmb">Remember Me</label>
                </div>
            </div>
            <div class="form-group text-center">
                <div class="row">
                    <div class="col">
                        <?php
                        $recaptcha_site_key = get_option('recaptcha_site_key');
                        if ($recaptcha_site_key) : ?>
                            <div id="g-recaptcha" class="g-recaptcha" data-sitekey="<?php echo $recaptcha_site_key; ?>" data-size="invisible" data-callback="gCallback"></div>
                            <input type="hidden" id="captcha-response" name="captcha-response" />
                        <?php endif; ?>
                        <button type="submit" name="submit" class="btn btn-custom btn-block shadow-sm">
                            <i class="fas fa-lock"></i><span class="ml-2">Login</span>
                        </button>
                    </div>
                    <?php if (!file_exists(BASE_DIR . '.rent') && !filter_var(get_option('disable_registration'), FILTER_VALIDATE_BOOLEAN)) : ?>
                        <div class="col">
                            <a href="<?php echo $adminURL; ?>/register/" class="btn btn-block btn-secondary shadow-sm">
                                <span>Register</span>
                                <i class="fas fa-arrow-right ml-2"></i>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
