<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!is_admin()) {
    session_write_close();
    include $adminDir . '/views/403.php';
    exit();
} else {
    session_write_close();
    $hosts = [];
    $supportedSites = [];
    $resolutions = [];
    $bypassed = [];
    if (!empty($_POST['opt'])) {
        session_write_close();
        $class = new \InstanceCache();
        $class->deleteItemsByTag('options');
        
        if (isset($_COOKIE['MiscSettings'])) {
            $MiscSettings = json_decode($_COOKIE['MiscSettings'], true);
            if (!empty($MiscSettings)) {
                foreach ($MiscSettings as $key) {
                    if (!empty($_POST['opt'][$key])) {
                        set_option($key, $_POST['opt'][$key]);
                    } elseif (!empty($key)) {
                        set_option($key, '');
                    }
                }
                create_alert('success', 'Miscellaneous settings updated successfully.', $_SERVER['REQUEST_URI']);
            } else {
                create_alert('danger', 'Failed to update miscellaneous settings.', $_SERVER['REQUEST_URI']);
            }
        } else {
            create_alert('danger', 'Cannot access the cookies from your browser!', $_SERVER['REQUEST_URI']);
        }
    } else {
        session_write_close();
        $opt = get_option();
        $supportedSites = \Hosting::supportedSites();
        ksort($supportedSites);
        if (!empty($opt['bypass_host'])) {
            $bypassed = json_decode($opt['bypass_host'], true);
        }
        if (!empty($opt['disable_host'])) {
            $hosts = json_decode($opt['disable_host'], true);
        }
        if (!empty($opt['disable_resolution'])) {
            $resolutions = json_decode($opt['disable_resolution'], true);
        }
    }
}

$title = 'Miscellaneous Settings';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">Miscellaneous Settings</h1>
        <form action="<?php echo $adminURL; ?>/settings/misc" id="frmSettings" data-cookie="MiscSettings" method="post" class="needs-validation" autocomplete="off" novalidate>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="bypass_host">Bypassed Hosts</label>
                        <select multiple="multiple" id="bypass_host" name="opt[bypass_host][]" class="multi-select">
                            <?php
                            if (!empty($supportedSites)) {
                                foreach ($supportedSites as $k => $v) {
                                    list($v, $badge) = array_pad(explode('|', $v), 2, '');
                                    echo '<option value="' . $k . '" ' . (in_array($k, $bypassed) ? 'selected' : '') . '>' . $v . '</option>';
                                }
                            }
                            ?>
                        </select>
                        <div class="mt-2">
                            <button type="button" class="btn btn-warning btn-sm mr-2" id="resetHost" onclick="settings.resetHost()">Reset Hosts</button>
                        </div>
                        <small class="form-text text-muted">The bypassed hosts will use your VPS bandwidth to stream the video. The bypassed hosts are in the right or bottom column.</small>
                    </div>
                    <div class="form-group">
                        <label for="disable_resolution">Disabled Video Resolutions</label>
                        <select multiple="multiple" id="disable_resolution" name="opt[disable_resolution][]" class="multi-select">
                            <option value="Default" <?php echo in_array('Default', $resolutions) ? 'selected' : ''; ?>>Default</option>
                            <?php for ($i = 1; $i < 11; $i++) : $val = $i * 100; ?>
                                <option value="<?php echo $val; ?>" <?php echo in_array($val, $resolutions) ? 'selected' : ''; ?>><?php echo $val; ?>p+</option>
                            <?php endfor; ?>
                            <option value="Original" <?php echo in_array('Original', $resolutions) ? 'selected' : ''; ?>>Original</option>
                        </select>
                        <small class="form-text text-muted">Does not apply to HLS/MPD videos. The disabled resolutions are in the right or bottom column.</small>
                    </div>
                    <div class="form-group">
                        <label for="disable_host">Disabled Hosts</label>
                        <select multiple="multiple" id="disable_host" name="opt[disable_host][]" class="multi-select">
                            <?php
                            if (!empty($supportedSites)) {
                                foreach ($supportedSites as $k => $v) {
                                    list($v, $badge) = array_pad(explode('|', $v), 2, '');
                                    echo '<option value="' . $k . '" ' . (in_array($k, $hosts) ? 'selected' : '') . '>' . $v . '</option>';
                                }
                            }
                            ?>
                        </select>
                        <small class="form-text text-muted">The disabled hosts are in the right or bottom column.</small>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="disable_proxy" name="opt[disable_proxy]" value="true" <?php echo !empty($opt['disable_proxy']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="disable_proxy">Disable Proxy</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="free_proxy" name="opt[free_proxy]" value="true" <?php echo !empty($opt['free_proxy']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="free_proxy">Disable Free Proxy</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="delete_unused_proxy" name="opt[delete_unused_proxy]" value="true" <?php echo !empty($opt['delete_unused_proxy']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="delete_unused_proxy">Delete Unused Proxies</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="proxy_list">Active Proxy List</label>
                        <textarea name="opt[proxy_list]" id="proxy_list" cols="30" rows="5" class="form-control" placeholder="Active proxy list"><?php echo !empty($opt['proxy_list']) ? trim(htmlspecialchars($opt['proxy_list'])) : ''; ?></textarea>
                        <small class="form-text text-muted">
                            <strong>Proxy Format:</strong><br>
                            IP:PORT<br>
                            IP:PORT,TYPE<br>
                            IP:PORT,USERNAME:PASSWORD<br>
                            IP:PORT,USERNAME:PASSWORD,TYPE<br>
                            <strong>Type:</strong> socks4, socks4a, socks5, https
                        </small>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary btn-sm" onclick="settings.checkProxy()" id="checkProxy">Proxy Checker</button>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="word_blacklisted">Blacklisted Words</label>
                        <textarea name="opt[word_blacklisted]" id="word_blacklisted" cols="30" rows="5" class="form-control" placeholder="Enter the words you blacklisted (per line). Example: porn"><?php echo !empty($opt['word_blacklisted']) ? trim(htmlspecialchars($opt['word_blacklisted'])) : ''; ?></textarea>
                        <p><small class="form-text text-muted">Videos will not play if the title has blacklisted words.</small></p>
                        <button type="button" class="btn btn-danger btn-sm" onclick="settings.deleteVideosWithWords()" id="deleteVideosBlacklisted">Delete Videos With Blacklisted Words</button>
                    </div>
                    <div class="form-group">
                        <label for="domain_whitelisted">Whitelisted Domains/IP Addesses</label>
                        <textarea name="opt[domain_whitelisted]" id="domain_whitelisted" cols="30" rows="5" class="form-control" placeholder="Enter a domain / ip you trust (per line). Example: userweb.com"><?php echo !empty($opt['domain_whitelisted']) ? trim(htmlspecialchars($opt['domain_whitelisted'])) : ''; ?></textarea>
                        <small class="form-text text-muted">Only whitelisted websites can access this tool. If the whitelist is left blank, all websites can access this tool.</small>
                    </div>
                    <div class="form-group">
                        <label for="domain_blacklisted">Blacklisted Domains/IP Addresses</label>
                        <textarea name="opt[domain_blacklisted]" id="domain_blacklisted" cols="30" rows="5" class="form-control" placeholder="Enter the domain / ip you blacklisted (per line). Example: userweb.com"><?php echo !empty($opt['domain_blacklisted']) ? trim(htmlspecialchars($opt['domain_blacklisted'])) : ''; ?></textarea>
                        <small class="form-text text-muted">Blacklisted websites cannot access this tool. If left blank, all websites can access this tool.</small>
                    </div>
                    <div class="form-group">
                        <label for="link_blacklisted">Blacklisted Referers</label>
                        <textarea name="opt[link_blacklisted]" id="link_blacklisted" cols="30" rows="5" class="form-control" placeholder="Enter the link you blacklisted (per line). Example: http://userweb.com/movie1/"><?php echo !empty($opt['link_blacklisted']) ? trim(htmlspecialchars($opt['link_blacklisted'])) : ''; ?></textarea>
                        <small class="form-text text-muted">Blacklisted referers cannot access this tool. If left blank, all referers can access this tool.</small>
                    </div>
                    <div class="form-group">
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <button type="submit" class="btn btn-success shadow-sm mr-2">
                        <i class="fas fa-save mr-2"></i>Update
                    </button>
                    <div class="btn-group dropup">
                        <button type="button" id="clearCache" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="fas fa-trash-alt mr-2"></i>Clear Cache
                        </button>
                        <div class="dropdown-menu shadow border-0">
                            <a class="dropdown-item" href="javascript:void(0)" onclick="settings.clearVideoHLSMPDCache()">Clear Static HLS/MPD File Cache</a>
                            <a class="dropdown-item" href="javascript:void(0)" onclick="settings.clearVideoCache()">Clear Video Cache</a>
                            <a class="dropdown-item" href="javascript:void(0)" onclick="settings.clearSettingsCache()">Clear Settings Cache</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0)" onclick="settings.clearCache()">Clear All</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
