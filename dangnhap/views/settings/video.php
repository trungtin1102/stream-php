<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!is_admin()) {
    session_write_close();
    include $adminDir . '/views/403.php';
    exit();
} else {
    session_write_close();
    $resolutions = [];
    $torrent = [
        'wss://tracker.openwebtorrent.com',
        'wss://spacetradersapi-chatbox.herokuapp.com:443/announce',
        'wss://tracker.btorrent.xyz/'
    ];
    if (!empty($_POST['opt'])) {
        session_write_close();
        $class = new \InstanceCache();
        $class->deleteItemsByTag('options');

        if (isset($_COOKIE['VideoSettings'])) {
            $VideoSettings = json_decode($_COOKIE['VideoSettings'], true);
            if (!empty($VideoSettings)) {
                foreach ($VideoSettings as $key) {
                    if (!empty($_POST['opt'][$key])) {
                        set_option($key, $_POST['opt'][$key]);
                    } elseif (!empty($key)) {
                        set_option($key, '');
                    }
                }
                create_alert('success', 'Video player settings updated successfully.', $_SERVER['REQUEST_URI']);
            } else {
                create_alert('danger', 'Failed to update video player settings.', $_SERVER['REQUEST_URI']);
            }
        } else {
            create_alert('danger', 'Cannot access the cookies from your browser!', $_SERVER['REQUEST_URI']);
        }
    } else {
        session_write_close();
        $opt = get_option();
        if (!empty($opt['disable_resolution'])) {
            $resolutions = json_decode($opt['disable_resolution'], true);
        }
    }
}

$title = 'Video Player Settings';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">Video Player Settings</h1>
        <form action="<?php echo $adminURL; ?>/settings/video" id="frmSettings" data-cookie="VideoSettings" method="post" class="needs-validation" autocomplete="off" novalidate>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="card form-group">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-12 col-md-6">
                                    <label for="player">Video Player</label>
                                    <select name="opt[player]" id="player" class="custom-select">
                                        <option value="jwplayer">JW Player 8.9.5</option>
                                        <option value="jwplayer_latest" <?php echo !empty($opt['player']) && 'jwplayer_latest' === $opt['player'] ? 'selected' : ''; ?>>JW Player Latest Version</option>
                                        <option value="plyr" <?php echo !empty($opt['player']) && 'plyr' === $opt['player'] ? 'selected' : ''; ?>>Plyr</option>
                                    </select>
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="player_skin">Video Player Skin</label>
                                    <select name="opt[player_skin]" id="player_skin" class="custom-select">
                                        <option value="">Default</option>
                                        <?php
                                        $skinDir = BASE_DIR . 'assets/css/skin/' . (!empty($opt['player']) ? strtr($opt['player'], ['_latest' => '']) : 'jwplayer');
                                        if (is_dir($skinDir)) {
                                            open_resources_handler();
                                            $list = new \DirectoryIterator($skinDir);
                                            foreach ($list as $file) {
                                                if (!$file->isDot() && $file->isFile()) {
                                                    if (pathinfo($file->getFilename(), PATHINFO_EXTENSION) === 'css') {
                                                        $skin = strtr($file->getFilename(), ['.min.css' => '', '.css' => '']);
                                                        echo '<option value="' . $skin . '" ' . (!empty($opt['player_skin']) && $skin === $opt['player_skin'] ? 'selected' : '') . '>' . ucfirst($skin) . '</option>';
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12 col-md-6">
                                    <label for="player_color">Video Player Color</label>
                                    <input type="text" name="opt[player_color]" id="player_color" class="form-control" placeholder="Video Player Color" value="<?php echo !empty($opt['player_color']) ? $opt['player_color'] : '673AB7'; ?>" data-wheelcolorpicker>
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <?php $stretching = !empty($opt['stretching']) ? $opt['stretching'] : ''; ?>
                                    <label for="stretching">Video Position</label>
                                    <select name="opt[stretching]" id="stretching" class="custom-select">
                                        <option value="uniform">Uniform</option>
                                        <option value="exactfit" <?php echo $stretching === 'exactfit' ? 'selected' : ''; ?>>Exact Fit</option>
                                        <option value="fill" <?php echo $stretching === 'fill' ? 'selected' : ''; ?>>Fill</option>
                                        <option value="none" <?php echo $stretching === 'none' ? 'selected' : ''; ?>>None</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12 col-md-6">
                                    <?php $preload = !empty($opt['preload']) ? $opt['preload'] : ''; ?>
                                    <label for="preload">Video Preload</label>
                                    <select name="opt[preload]" id="preload" class="custom-select">
                                        <option value="auto">Auto</option>
                                        <option value="metadata" <?php echo $preload === 'metadata' ? 'selected' : ''; ?>>Metadata</option>
                                        <option value="none" <?php echo $preload === 'none' ? 'selected' : ''; ?>>None</option>
                                    </select>
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="default_resolution">Default Video Resolution</label>
                                    <select id="default_resolution" name="opt[default_resolution]" class="custom-select">
                                        <?php for ($i = 1; $i < 11; $i++) : $val = $i * 100; ?>
                                            <?php if (!in_array($val, $resolutions)) : ?>
                                                <option value="<?php echo $val; ?>" <?php echo !empty($opt['default_resolution']) && $val == $opt['default_resolution'] ? 'selected' : '' ?>><?php echo $val; ?>p+</option>
                                            <?php endif; ?>
                                        <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12 col-md-6">
                                    <label for="default_subtitle">Default Subtitle</label>
                                    <?php echo subtitle_languages('opt[default_subtitle]', (!empty($opt['default_subtitle']) ? $opt['default_subtitle'] : 'Default')); ?>
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="subtitle_color">Subtitle Font Color</label>
                                    <input type="text" name="opt[subtitle_color]" id="subtitle_color" class="form-control" placeholder="Video Player Color" value="<?php echo !empty($opt['subtitle_color']) ? $opt['subtitle_color'] : 'ffff00'; ?>" data-wheelcolorpicker>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="autoplay" name="opt[autoplay]" value="true" <?php echo !empty($opt['autoplay']) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="autoplay">Autoplay</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="mute" name="opt[mute]" value="true" <?php echo !empty($opt['mute']) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="mute">Mute</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="repeat" name="opt[repeat]" value="true" <?php echo !empty($opt['repeat']) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="repeat">Repeat</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="display_title" name="opt[display_title]" value="true" <?php echo !empty($opt['display_title']) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="display_title">Show Title</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="playback_rate" name="opt[playback_rate]" value="true" <?php echo !empty($opt['playback_rate']) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="playback_rate">Show Playback Rate Controls</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="enable_share_button" name="opt[enable_share_button]" value="true" <?php echo !empty($opt['enable_share_button']) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="enable_share_button">Show Share Button</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="enable_download_button" name="opt[enable_download_button]" value="true" <?php echo !empty($opt['enable_download_button']) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="enable_download_button">Show Download Button</label>
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="continue_watching" name="opt[continue_watching]" value="true" <?php echo !empty($opt['continue_watching']) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="continue_watching">Enable Continue Watching Dialog</label>
                                </div>
                                <small class="form-text text-muted">If you enable this option, a continue watching video dialog will be displayed to remind you that you have watched the video for a certain period of time.</small>
                            </div>
                        </div>
                    </div>
                    <div class="card form-group">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="poster">Default Poster Image URL</label>
                                <input type="url" name="opt[poster]" id="poster" class="form-control" placeholder="Insert Poster image url (.jpg/.jpeg/.png/.webp)" value="<?php echo !empty($opt['poster']) ? $opt['poster'] : ''; ?>">
                            </div>
                            <div class="form-group">
                                <label for="small_logo_file">Small Logo URL</label>
                                <input type="url" name="opt[small_logo_file]" id="small_logo_file" class="form-control" placeholder="The small logo will be displayed on the video player control" value="<?php echo !empty($opt['small_logo_file']) ? $opt['small_logo_file'] : ''; ?>">
                            </div>
                            <div class="form-group mb-0">
                                <label for="small_logo_link">Visited URL</label>
                                <input type="url" name="opt[small_logo_link]" id="small_logo_link" class="form-control" placeholder="Insert a link that will be visited each time the small logo is clicked" value="<?php echo !empty($opt['small_logo_link']) ? $opt['small_logo_link'] : ''; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="card form-group">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="logo_file">Image URL</label>
                                <input type="url" name="opt[logo_file]" id="logo_file" class="form-control" placeholder="Insert the watermark image link" value="<?php echo !empty($opt['logo_file']) ? $opt['logo_file'] : ''; ?>">
                            </div>
                            <div class="form-group">
                                <label for="logo_open_link">Visited URL</label>
                                <input type="url" name="opt[logo_open_link]" id="logo_open_link" class="form-control" placeholder="Insert a link that will be visited each time the watermark image is clicked" value="<?php echo !empty($opt['logo_open_link']) ? $opt['logo_open_link'] : ''; ?>">
                            </div>
                            <div class="form-group">
                                <?php $logo_position = !empty($opt['logo_position']) ? $opt['logo_position'] : ''; ?>
                                <label for="logo_position">Position</label>
                                <select name="opt[logo_position]" id="logo_position" class="custom-select">
                                    <option value="top-right" <?php echo $logo_position === 'top-right' ? 'selected' : ''; ?>>Top-right</option>
                                    <option value="bottom-right" <?php echo $logo_position === 'bottom-right' ? 'selected' : ''; ?>>Bottom-right</option>
                                    <option value="bottom-left" <?php echo $logo_position === 'bottom-left' ? 'selected' : ''; ?>>Bottom-left</option>
                                    <option value="top-left" <?php echo $logo_position === 'top-left' ? 'selected' : ''; ?>>Top-left</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="logo_margin">Margin</label>
                                <input type="number" name="opt[logo_margin]" id="logo_margin" class="form-control" placeholder="Enter the distance between the logo and the edge of the screen (px)" value="<?php echo !empty($opt['logo_margin']) ? $opt['logo_margin'] : 8; ?>">
                            </div>
                            <div class="form-group mb-0">
                                <?php $logo_hide = !empty($opt['logo_hide']) ? $opt['logo_hide'] : ''; ?>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="logo_hide" name="opt[logo_hide]" value="true" <?php echo filter_var($logo_hide, FILTER_VALIDATE_BOOLEAN) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="logo_hide">Hide</label>
                                </div>
                                <small class="form-text text-muted">When this option is checked, the logo will automatically show and hide along with the other player controls </small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="card form-group">
                        <div class="card-body">
                            <div class="form-group">
                                <p class="alert alert-warning">Using the p2p feature in JW Player will remove some parts of the HLS/MPEG DASH, such as audio or subtitle options.</p>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="p2p" name="opt[p2p]" value="true" <?php echo !empty($opt['p2p']) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="p2p">Enable P2P</label>
                                </div>
                                <small class="form-text text-muted">If you enable this option, bandwidth usage will be reduced especially when many visitors are playing videos at the same time but may slow down video playback.</small>
                            </div>
                            <div class="form-group mb-0">
                                <label for="torrent_tracker">Torrent Trackers</label>
                                <textarea name="opt[torrent_tracker]" id="torrent_tracker" cols="30" rows="5" class="form-control mb-0"><?php echo !empty($opt['torrent_tracker']) ? trim(htmlspecialchars($opt['torrent_tracker'])) : implode("\n", $torrent); ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card form-group">
                        <div class="card-body">
                            <div class="form-group">
                                <small class="form-text text-muted">{title} = Your Video Title<br>{siteName} = Your Site Name</small>
                            </div>
                            <div class="form-group">
                                <label for="text_title">Embed Page Title Text</label>
                                <input type="text" name="opt[text_title]" id="text_title" class="form-control" placeholder="Watch {title} - {siteName}" value="<?php echo !empty($opt['text_title']) ? $opt['text_title'] : 'Watch {title} - {siteName}'; ?>">
                            </div>
                            <div class="form-group">
                                <label for="text_loading">Loading Text</label>
                                <input type="text" name="opt[text_loading]" id="text_loading" class="form-control" placeholder="Please wait..." value="<?php echo !empty($opt['text_loading']) ? $opt['text_loading'] : 'Please wait...'; ?>">
                            </div>
                            <div class="form-group">
                                <label for="text_download">Download Button Text</label>
                                <input type="text" name="opt[text_download]" id="text_download" class="form-control" placeholder="Download {title}" value="<?php echo !empty($opt['text_download']) ? $opt['text_download'] : 'Download {title}'; ?>">
                            </div>
                            <div class="form-group">
                                <label for="text_resume">Resume Dialog Text</label>
                                <textarea name="opt[text_resume]" id="text_resume" class="form-control"><?php echo !empty($opt['text_resume']) ? $opt['text_resume'] : htmlspecialchars('Welcome back! You left off at <span id="timez"></span>. Would you like to resume watching?'); ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="text_resume_yes">Resume Confirm Button Text</label>
                                <input type="text" name="opt[text_resume_yes]" id="text_resume_yes" class="form-control" placeholder="Yes, Please" value="<?php echo !empty($opt['text_resume_yes']) ? $opt['text_resume_yes'] : 'Yes, Please'; ?>">
                            </div>
                            <div class="form-group">
                                <label for="text_resume_no">Resume Cancel Button Text</label>
                                <input type="text" name="opt[text_resume_no]" id="text_resume_no" class="form-control" placeholder="No, Thanks" value="<?php echo !empty($opt['text_resume_no']) ? $opt['text_resume_no'] : 'No, Thanks'; ?>">
                            </div>
                            <div class="form-group">
                                <label for="text_rewind">JW Player Rewind Button Text</label>
                                <input type="text" name="opt[text_rewind]" id="text_rewind" class="form-control" placeholder="Rewind 10 Seconds" value="<?php echo !empty($opt['text_rewind']) ? $opt['text_rewind'] : 'Rewind 10 Seconds'; ?>">
                            </div>
                            <div class="form-group mb-0">
                                <label for="text_forward">JW Player Forward Button Text</label>
                                <input type="text" name="opt[text_forward]" id="text_forward" class="form-control" placeholder="Forward 10 Seconds" value="<?php echo !empty($opt['text_forward']) ? $opt['text_forward'] : 'Forward 10 Seconds'; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <button type="submit" class="btn btn-success shadow-sm">
                        <i class="fas fa-save mr-2"></i>
                        <span>Update</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
