<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!is_admin()) {
    session_write_close();
    include $adminDir . '/views/403.php';
    exit();
} else {
    session_write_close();
    if (!empty($_POST['opt'])) {
        session_write_close();
        $class = new \InstanceCache();
        $class->deleteItemsByTag('options');
        
        if (isset($_COOKIE['SmtpSettings'])) {
            $SmtpSettings = json_decode($_COOKIE['SmtpSettings'], true);
            if (!empty($SmtpSettings)) {
                foreach ($SmtpSettings as $key) {
                    if (!empty($_POST['opt'][$key])) {
                        set_option($key, $_POST['opt'][$key]);
                    } elseif (!empty($key))  {
                        set_option($key, '');
                    }
                }
                create_alert('success', 'SMTP settings updated successfully.', $_SERVER['REQUEST_URI']);
            } else {
                create_alert('danger', 'Failed to update SMTP settings.', $_SERVER['REQUEST_URI']);
            }
        } else {
            create_alert('danger', 'Cannot access the cookies from your browser!', $_SERVER['REQUEST_URI']);
        }
    } else {
        session_write_close();
        $opt = get_option();
    }
}

$title = 'SMTP Settings';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">SMTP Settings</h1>
        <form action="<?php echo $adminURL; ?>/settings/smtp" id="frmSettings" data-cookie="SmtpSettings" method="post" class="needs-validation" autocomplete="off" novalidate>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="disable_confirm" name="opt[disable_confirm]" value="true" <?php echo !empty($opt['disable_confirm']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="disable_confirm">Disable registration confirmation email / reset password</label>
                        </div>
                        <small class="form-text text-muted">Warning! If you tick this feature then the email entered by the user will not be validated.</small>
                    </div>
                    <div class="form-group">
                        <?php
                        $smtp_provider = !empty($opt['smtp_provider']) ? $opt['smtp_provider'] : '';
                        ?>
                        <label for="smtp_provider">Provider</label>
                        <select name="opt[smtp_provider]" id="smtp_provider" class="custom-select" onchange="settings.smtp()">
                            <option value="">-- Select Email Provider --</option>
                            <option value="gmail" <?php if ($smtp_provider === 'gmail') echo 'selected'; ?>>Gmail</option>
                            <option value="ymail" <?php if ($smtp_provider === 'ymail') echo 'selected'; ?>>Yahoo!</option>
                            <option value="outlook" <?php if ($smtp_provider === 'outlook') echo 'selected'; ?>>Outlook</option>
                            <option value="other" <?php if ($smtp_provider === 'other') echo 'selected'; ?>>Other</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="smtp_host">Host</label>
                        <input type="text" name="opt[smtp_host]" id="smtp_host" class="form-control" placeholder="SMTP Host" value="<?php echo !empty($opt['smtp_host']) ? $opt['smtp_host'] : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label for="smtp_port">Port</label>
                        <input type="number" name="opt[smtp_port]" id="smtp_port" class="form-control" placeholder="SMTP Port" value="<?php echo !empty($opt['smtp_port']) ? $opt['smtp_port'] : ''; ?>">
                    </div>
                    <?php
                    $smtp_tls = !empty($opt['smtp_tls']) ? filter_var($opt['smtp_tls'], FILTER_VALIDATE_BOOLEAN) : FALSE;
                    ?>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="smtp_tls" name="opt[smtp_tls]" <?php echo $smtp_tls ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="smtp_tls">Use TLS</label>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="smtp_email">Email</label>
                        <input type="email" name="opt[smtp_email]" id="smtp_email" class="form-control" placeholder="Email" value="<?php echo !empty($opt['smtp_email']) ? $opt['smtp_email'] : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label for="smtp_password">Password</label>
                        <input type="password" name="opt[smtp_password]" id="smtp_password" class="form-control" placeholder="Password" value="<?php echo !empty($opt['smtp_password']) ? $opt['smtp_password'] : ''; ?>" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="smtp_sender">Sender Name</label>
                        <input type="text" name="opt[smtp_sender]" id="smtp_sender" class="form-control" placeholder="Sender Name" value="<?php echo !empty($opt['smtp_sender']) ? $opt['smtp_sender'] : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label for="smtp_reply_email">Reply To Email</label>
                        <input type="email" name="opt[smtp_reply_email]" id="smtp_reply_email" class="form-control" placeholder="Reply To Email" value="<?php echo !empty($opt['smtp_reply_email']) ? $opt['smtp_reply_email'] : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label for="smtp_reply_name">Reply To Recipient</label>
                        <input type="text" name="opt[smtp_reply_name]" id="smtp_reply_name" class="form-control" placeholder="Reply To Recipient" value="<?php echo !empty($opt['smtp_reply_name']) ? $opt['smtp_reply_name'] : ''; ?>">
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <button type="submit" class="btn btn-success shadow-sm">
                        <i class="fas fa-save mr-2"></i>
                        <span>Update</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
