<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!is_admin()) {
    session_write_close();
    include $adminDir . '/views/403.php';
    exit();
} else {
    session_write_close();
    if (!empty($_POST['opt'])) {
        session_write_close();
        $class = new \InstanceCache();
        $class->deleteItemsByTag('options');
        
        if (isset($_COOKIE['AdsSettings'])) {
            $AdsSettings = json_decode($_COOKIE['AdsSettings'], true);
            if (!empty($AdsSettings)) {
                foreach ($AdsSettings as $key) {
                    if (!empty($_POST['opt'][$key])) {
                        set_option($key, $_POST['opt'][$key]);
                    } elseif (!empty($key)) {
                        set_option($key, '');
                    }
                }
                create_alert('success', 'Advertisement settings updated successfully.', $_SERVER['REQUEST_URI']);
            } else {
                create_alert('danger', 'Failed to update advertisement settings.', $_SERVER['REQUEST_URI']);
            }
        } else {
            create_alert('danger', 'Cannot access the cookies from your browser!', $_SERVER['REQUEST_URI']);
        }
    }
    $opt = get_option();
    $vast_xml = isset($opt['vast_xml']) ? @json_decode($opt['vast_xml'], true) : [];
    $vast_xml = is_array($vast_xml) ? array_filter($vast_xml) : [];
    $vast_offset = isset($opt['vast_offset']) ? @json_decode($opt['vast_offset'], true) : [];
    $vast_offset =is_array($vast_offset) ?  array_filter($vast_offset) : [];
}

$title = 'Advertisement Settings';
include BASE_DIR . admin_dir() . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">Advertisement Settings</h1>
        <form action="<?php echo $adminURL; ?>/settings/ads" id="frmSettings" data-cookie="AdsSettings" method="post" class="needs-validation" autocomplete="off" novalidate>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="card mb-3">
                        <div class="card-head">
                            <h6 class="card-header">VAST Ads</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="disable_vast_ads" name="opt[disable_vast_ads]" value="true" <?php echo !empty($opt['disable_vast_ads']) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="disable_vast_ads">Disable VAST Ads</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="block_adblocker" name="opt[block_adblocker]" value="true" <?php echo !empty($opt['block_adblocker']) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="block_adblocker">Forced To Disable AdBlocker</label>
                                </div>
                                <small class="form-text text-muted">Display a message to notify the visitor to disable the AdBlocker.</small>
                            </div>
                            <div class="form-group">
                                <label for="vast_client">Publisher</label>
                                <div class="input-group">
                                    <?php
                                    $adClient = !empty($opt['vast_client']) ? $opt['vast_client'] : '';
                                    ?>
                                    <select name="opt[vast_client]" id="vast_client" class="custom-select">
                                        <option value="vast" <?php echo $adClient === 'vast' ? 'selected' : ''; ?>>VAST</option>
                                        <option value="googima" <?php echo $adClient === 'googima' ? 'selected' : ''; ?>>Google IMA</option>
                                    </select>
                                    <div class="input-group-append"><button type="button" class="btn btn-outline-primary" onclick="$('#modalCustomVAST').modal('show')" data-toggle="tooltip" title="Create Custom VAST"><i class="fas fa-plus-circle"></i></button></div>
                                </div>
                            </div>
                            <div id="vastWrapper">
                                <div class=" form-group" data-index="0">
                                    <label for="vast_xml">VAST URLs</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend" style="max-width:110px">
                                            <input type="text" placeholder="Ad Position" id="vast_offset" name="opt[vast_offset][]" class="form-control" value="<?php echo isset($vast_offset[0]) ? $vast_offset[0] : ''; ?>">
                                        </div>
                                        <input type="url" id="vast_xml" name="opt[vast_xml][]" placeholder="VAST URL (.xml)" class="form-control" value="<?php echo isset($vast_xml[0]) ? $vast_xml[0] : ''; ?>">
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-success" onclick="settings.addVastHTML()" data-toggle="tooltip" title="Add VAST">
                                                <i class="fas fa-plus-circle"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <?php if (!empty($vast_xml)) :
                                    foreach ($vast_xml as $i => $val) :
                                        if ($i > 0) :
                                ?>
                                            <div class=" form-group" data-index="<?php echo $i; ?>">
                                                <div class="input-group">
                                                    <div class="input-group-prepend" style="max-width:110px">
                                                        <input type="text" placeholder="Ad Position" name="opt[vast_offset][]" id="vast_offset-<?php echo $i; ?>" class="form-control" value="<?php echo isset($vast_offset[$i]) ? $vast_offset[$i] : ''; ?>">
                                                    </div>
                                                    <input type="url" name="opt[vast_xml][]" id="vast_xml-<?php echo $i; ?>" placeholder="VAST URL (.xml)" class="form-control" value="<?php echo $val; ?>">
                                                    <div class="input-group-append">
                                                        <?php if ($i > 0) : ?>
                                                            <button type="button" class="btn btn-danger" onclick="settings.removeVastHTML(<?php echo $i; ?>)" data-toggle="tooltip" title="Remove VAST">
                                                                <i class="fas fa-minus-circle"></i>
                                                            </button>
                                                        <?php else : ?>
                                                            <button type="button" class="btn btn-success" onclick="settings.addVastHTML()" data-toggle="tooltip" title="Add VAST">
                                                                <i class="fas fa-plus-circle"></i>
                                                            </button>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                <?php
                                        endif;
                                    endforeach;
                                endif; ?>
                            </div>
                            <div class="form-group">
                                <small><strong>Ad Position Supported:</strong><br>
                                    <strong>preroll</strong>: Start of Video<br>
                                    <strong>postroll</strong>: End of Video<br>
                                    <strong>start</strong>: Start of Video<br>
                                    <strong>end</strong>: End of Video<br>
                                    <strong>50%</strong>: In the Middle of Video<br>
                                    <strong>00:10:00</strong>: At the 10th Minute<br>
                                    <strong>600</strong>: At the 600th Second</small>
                            </div>
                            <div class="form-group mb-0">
                                <label for="vast_skip">Skip Ads After</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" name="opt[vast_skip]" id="vast_skip" placeholder="Skip ads after (seconds)" value="<?php echo !empty($opt['vast_skip']) ? intval($opt['vast_skip']) : 0; ?>">
                                    <div class="input-group-append">
                                        <div class="input-group-text">Seconds</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mb-3">
                        <div class="card-head">
                            <h6 class="card-header">Banner Ads</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="disable_banner_ads" name="opt[disable_banner_ads]" value="true" <?php echo !empty($opt['disable_banner_ads']) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="disable_banner_ads">Disable Banner Ads</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dl_banner_top">Top Banner on Download Page</label>
                                <textarea name="opt[dl_banner_top]" id="dl_banner_top" cols="30" rows="3" class="form-control" placeholder="HTML/Javascript Here"><?php echo !empty($opt['dl_banner_top']) ? htmlspecialchars($opt['dl_banner_top']) : ''; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="dl_banner_bottom">Bottom Banner on Download Page</label>
                                <textarea name="opt[dl_banner_bottom]" id="dl_banner_bottom" cols="30" rows="3" class="form-control" placeholder="HTML/Javascript Here"><?php echo !empty($opt['dl_banner_bottom']) ? htmlspecialchars($opt['dl_banner_bottom']) : ''; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="sh_banner_top">Top Banner on Sharer Page</label>
                                <textarea name="opt[sh_banner_top]" id="sh_banner_top" cols="30" rows="3" class="form-control" placeholder="HTML/Javascript Here"><?php echo !empty($opt['sh_banner_top']) ? htmlspecialchars($opt['sh_banner_top']) : ''; ?></textarea>
                            </div>
                            <div class="form-group mb-0">
                                <label for="sh_banner_bottom">Bottom Banner on Sharer Page</label>
                                <textarea name="opt[sh_banner_bottom]" id="sh_banner_bottom" cols="30" rows="3" class="form-control" placeholder="HTML/Javascript Here"><?php echo !empty($opt['sh_banner_bottom']) ? htmlspecialchars($opt['sh_banner_bottom']) : ''; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="card mb-3">
                        <div class="card-head">
                            <h6 class="card-header">Direct Ads</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="disable_direct_ads" name="opt[disable_direct_ads]" value="true" <?php echo !empty($opt['disable_direct_ads']) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="disable_direct_ads">Disable Direct Ads</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="direct_ads_link">Direct URL Ads</label>
                                <input type="url" class="form-control" name="opt[direct_ads_link]" id="direct_ads_link" placeholder="Enter the direct link ads" value="<?php echo !empty($opt['direct_ads_link']) ? filter_var($opt['direct_ads_link'], FILTER_SANITIZE_URL) : ''; ?>">
                                <small class="form-text text-muted">The direct URL will open when the download button is clicked.</small>
                            </div>
                            <div class="form-group mb-0">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="visitads_onplay" name="opt[visitads_onplay]" value="true" <?php echo !empty($opt['visitads_onplay']) && filter_var($opt['visitads_onplay'], FILTER_VALIDATE_BOOLEAN) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="visitads_onplay">Visit Direct URL Ads on Play</label>
                                </div>
                                <small class="form-text text-muted">Visit the direct URL ad when the play button is clicked for the first time.</small>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-head">
                            <h6 class="card-header">Popup Ads</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="disable_popup_ads" name="opt[disable_popup_ads]" value="true" <?php echo !empty($opt['disable_popup_ads']) ? 'checked' : ''; ?>>
                                    <label class="custom-control-label" for="disable_popup_ads">Disable Popup Ads</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="popup_ads_link">Javascript URL</label>
                                <input type="url" class="form-control" name="opt[popup_ads_link]" id="popup_ads_link" placeholder="Enter the Popup Ads URL (.js)" value="<?php echo !empty($opt['popup_ads_link']) ? filter_var($opt['popup_ads_link'], FILTER_SANITIZE_URL) : ''; ?>">
                            </div>
                            <div class="form-group mb-0">
                                <label for="popup_ads_code">Javascript Codes</label>
                                <textarea name="opt[popup_ads_code]" id="popup_ads_code" cols="30" rows="10" class="form-control" placeholder="Enter the popup ad's javascript code (with the <script> tag)"><?php echo !empty($opt['popup_ads_code']) ? trim(htmlspecialchars($opt['popup_ads_code'])) : ''; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <button type="submit" class="btn btn-success shadow-sm">
                        <i class="fas fa-save mr-2"></i>
                        <span>Update</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="modalCustomVAST" tabindex="-1" role="dialog" aria-labelledby="modalCustomVASTLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <form method="post" id="frmCreateCustomVast" class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalCustomVASTLabel">Create Custom VAST</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>(*) Required</p>
                <div class="form-group">
                    <label for="adTitle">Title</label>
                    <input type="text" name="adTitle" id="adTitle" class="form-control" placeholder="Insert ad title">
                </div>
                <div class="form-group">
                    <label for="adClickThrough">Destination URL (*)</label>
                    <input type="url" name="adClickThrough" id="adClickThrough" class="form-control" placeholder="Insert destination url when ad is clicked" required>
                </div>
                <div class="form-group">
                    <label for="adMediaFile">Video File URL (.mp4) (*)</label>
                    <input type="url" name="adMediaFile" id="adMediaFile" class="form-control" placeholder="Insert ad video file url (.mp4)" required>
                </div>
                <div class="form-group">
                    <label for="adDuration">Video Duration (*)</label>
                    <div class="input-group">
                        <input type="number" min="0" step="1" name="adDuration" id="adDuration" class="form-control" placeholder="Insert ad video duration" aria-describedby="ad-addon1" required>
                        <div class="input-group-append"><span class="input-group-text" id="ad-addon1">Seconds</span></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="adSkipOffset">Skip Offset</label>
                    <div class="input-group">
                        <input type="number" min="0" step="1" name="adSkipOffset" id="adSkipOffset" class="form-control" placeholder="Insert ad skip offset" aria-describedby="ad-addon1">
                        <div class="input-group-append"><span class="input-group-text" id="ad-addon1">Seconds</span></div>
                    </div>
                    <small class="form-text text-muted">Show skip button after video plays after few seconds.</small>
                </div>
                <div class="form-group">
                    <label for="adFilename">Filename (*)</label>
                    <input type="text" name="adFilename" id="adFilename" class="form-control" placeholder="Insert VAST ad filename" required>
                </div>
                <div id="customVastResult" class="d-none">
                    <div class="form-group">
                        <label for="txtCustomVastResult">Custom VAST URL</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="txtCustomVastResult" value="" readonly>
                            <div class="input-group-append">
                                <button type="button" class="btn btn-outline-secondary" onclick="copyText($('#txtCustomVastResult').val(), 'Custom VAST URL')" data-toggle="tooltip" title="Copy Custom VAST URL">
                                    <i class="fas fa-copy"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="action" value="create_custom_vast">
                <button type="submit" class="btn btn-success" name="adSubmit">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
