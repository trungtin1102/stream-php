<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!is_admin()) {
    session_write_close();
    include $adminDir . '/views/403.php';
    exit();
} else {
    session_write_close();
    if ($_POST) {
        session_write_close();
        
        $iCache = new \InstanceCache();
        $iCache->deleteItemsByTag('options');

        $class = new \Settings();
        $deleted = $class->reset();
        if ($deleted) create_alert('success', 'Settings reset successfully.', $_SERVER['REQUEST_URI']);
        else create_alert('success', 'Failed to reset settings.', $_SERVER['REQUEST_URI']);
    }
}

$title = 'Reset Settings';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <form action="<?php echo $adminURL; ?>/settings/reset" method="post">
            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="h3 mb-3">Are you sure you want to reset the settings?</h1>
                    <button name="reset" type="submit" class="btn btn-danger btn-lg">Reset Now</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
