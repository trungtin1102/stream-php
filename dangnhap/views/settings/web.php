<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();
if (!is_admin()) {
    session_write_close();
    include $adminDir . '/views/403.php';
    exit();
} else {
    session_write_close();
    if (!empty($_POST['opt'])) {
        session_write_close();
        $class = new \InstanceCache();
        $class->deleteItemsByTag('options');

        if (isset($_COOKIE['WebSettings'])) {
            $WebSettings = json_decode($_COOKIE['WebSettings'], true);
            if (!empty($WebSettings)) {
                foreach ($WebSettings as $key) {
                    if (!empty($_POST['opt'][$key])) {
                        set_option($key, $_POST['opt'][$key]);
                    } elseif (!empty($key)) {
                        set_option($key, '');
                    }
                }
            }
        }

        // upload favicon
        if ($_FILES) {
            $dir = BASE_DIR . 'assets/img/';
            $ext = pathinfo($_FILES["favicon"]["name"], PATHINFO_EXTENSION);
            if ($ext === 'png') {
                $faviconFile = $dir . 'logo.png';
                @unlink($faviconFile);
                $isImage = getimagesize($_FILES["favicon"]["tmp_name"]);
                if ($isImage) {
                    $uploaded = move_uploaded_file($_FILES["favicon"]["tmp_name"], $faviconFile);
                    if ($uploaded) {
                        @unlink($dir . 'logo.ico');
                        @unlink($dir . 'logo.ico');

                        $image = new \SimpleImage();
                        $image->load($faviconFile);
                        $image->resize(57, 57);
                        $image->save($dir . 'apple-touch-icon.png', IMAGETYPE_PNG);
                        $image->save($dir . 'apple-touch-icon-precomposed.png', IMAGETYPE_PNG);

                        $files = ['icon-144x144.png', 'icon-192x192.png', 'icon-256x256.png', 'icon-384x384.png', 'icon-512x512.png', 'apple-touch-icon-152x152.png', 'apple-touch-icon-152x152-precomposed.png', 'apple-touch-icon-120x120.png', 'apple-touch-icon-120x120-precomposed.png', 'apple-touch-icon-114x114.png', 'apple-touch-icon-72x72.png', 'apple-touch-icon-57x57.png'];
                        foreach ($files as $file) {
                            @unlink($dir . $file);

                            $image->load($faviconFile);
                            list($w, $h) = explode('x', strtr($file, ['apple-touch-icon-' => '', '-precomposed.png' => '', 'icon-' => '', '.png' => '']), 2);
                            $image->resize(intval($w), intval($h));
                            $image->save($dir . $file, IMAGETYPE_PNG);
                        }

                        @unlink($dir . 'logo.ico');
                        usleep(100000);
                        $ico_lib = new \PHP_ICO($faviconFile, array([256, 256]));
                        $saved = $ico_lib->save_ico($dir . 'logo.ico');
                        if ($saved) {
                            @copy($dir . 'logo.ico', BASE_DIR . 'favicon.ico');
                        }
                    }
                } elseif (!file_exists(BASE_DIR . 'favicon.ico') && file_exists(BASE_DIR . 'assets/img/logo.ico')) {
                    @copy(BASE_DIR . 'assets/img/logo.ico', BASE_DIR . 'favicon.ico');
                }
            } elseif (!file_exists(BASE_DIR . 'favicon.ico') && file_exists(BASE_DIR . 'assets/img/logo.ico')) {
                @copy(BASE_DIR . 'assets/img/logo.ico', BASE_DIR . 'favicon.ico');
            }
        } elseif (!file_exists(BASE_DIR . 'favicon.ico') && file_exists(BASE_DIR . 'assets/img/logo.ico')) {
            @copy(BASE_DIR . 'assets/img/logo.ico', BASE_DIR . 'favicon.ico');
        }

        // create manifest json
        $manifest = json_encode(array(
            "name" => $_POST['opt']['site_name'],
            "description" => $_POST['opt']['site_description'],
            "icons" => array(
                array(
                    "src" => "/assets/img/icon-144x144.png",
                    "type" => "image/png",
                    "sizes" => "144x144"
                ), array(
                    "src" => "/assets/img/icon-192x192.png",
                    "type" => "image/png",
                    "sizes" => "192x192"
                ),
                array(
                    "src" => "/assets/img/icon-256x256.png",
                    "type" => "image/png",
                    "sizes" => "256x256"
                ),
                array(
                    "src" => "/assets/img/icon-384x384.png",
                    "type" => "image/png",
                    "sizes" => "384x384"
                ),
                array(
                    "src" => "/assets/img/icon-512x512.png",
                    "type" => "image/png",
                    "sizes" => "512x512"
                )
            ),
            "short_name" => $_POST['opt']['pwa_shortname'],
            "start_url" => "/?source=pwa",
            "scope" => "/",
            "display" => $_POST['opt']['pwa_display'],
            "background_color" => '#' . trim($_POST['opt']['pwa_backgroundcolor']),
            "theme_color" => '#' . trim($_POST['opt']['pwa_themecolor'])
        ));

        // delete old manifest
        @unlink(BASE_DIR . 'manifest.webmanifest');

        // save manifest file
        open_resources_handler();
        $fp = @fopen(BASE_DIR . 'manifest.webmanifest', 'w+');
        if ($fp) {
            fwrite($fp, $manifest, strlen($manifest));
            fclose($fp);
        }

        create_alert('success', 'Website settings updated successfully.', $_SERVER['REQUEST_URI']);
    } else {
        session_write_close();
        $opt = get_option();
    }
}

$title = 'Website Settings';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">Website Settings</h1>
        <form action="<?php echo $adminURL; ?>/settings/web" id="frmSettings" data-cookie="WebSettings" method="post" enctype="multipart/form-data" class="needs-validation" autocomplete="off" novalidate>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="site_name">Site Name</label>
                        <input type="text" name="opt[site_name]" id="site_name" placeholder="Insert site name" class="form-control" value="<?php echo !empty($opt['site_name']) ? $opt['site_name'] : 'TvHay'; ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="site_slogan">Slogan</label>
                        <input type="text" name="opt[site_slogan]" id="site_slogan" placeholder="Insert site slogan" class="form-control" value="<?php echo !empty($opt['site_slogan']) ? $opt['site_slogan'] : 'Google Drive Video Player'; ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="site_description">Site Description</label>
                        <textarea name="opt[site_description]" id="site_description" placeholder="Insert site description" class="form-control" required><?php echo !empty($opt['site_description']) ? $opt['site_description'] : 'Google Drive Video Player'; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="favicon">Favicon</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="favicon" name="favicon" accept=".png">
                            <label class="custom-file-label" for="favicon">Choose file</label>
                        </div>
                        <small class="form-text text-muted">Upload a favicon image as your website identity. Image format must be PNG. The dimensions should be 512x512px.</small>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="custom_color">Color 1</label>
                                <input type="text" name="opt[custom_color]" id="custom_color" placeholder="Example: 673ab7" class="form-control" value="<?php echo !empty($opt['custom_color']) ? $opt['custom_color'] : '673ab7'; ?>" data-wheelcolorpicker>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="custom_color2">Color 2</label>
                                <input type="text" name="opt[custom_color2]" id="custom_color2" placeholder="Example: 673ab7" class="form-control" value="<?php echo !empty($opt['custom_color2']) ? $opt['custom_color2'] : '3f51b5'; ?>" data-wheelcolorpicker>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <h2 class="h5">PWA</h2>
                    </div>
                    <div class="form-group">
                        <label for="pwa_shortname">Short Name</label>
                        <input type="text" name="opt[pwa_shortname]" id="pwa_shortname" placeholder="Insert short name" class="form-control" value="<?php echo !empty($opt['pwa_shortname']) ? $opt['pwa_shortname'] : 'TvHay'; ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="pwa_themecolor">Theme Color</label>
                        <input type="text" name="opt[pwa_themecolor]" id="pwa_themecolor" placeholder="Example: 673ab7" class="form-control" value="<?php echo !empty($opt['pwa_themecolor']) ? $opt['pwa_themecolor'] : '673ab7'; ?>" data-wheelcolorpicker>
                    </div>
                    <div class="form-group">
                        <label for="pwa_backgroundcolor">Background Color</label>
                        <input type="text" name="opt[pwa_backgroundcolor]" id="pwa_backgroundcolor" placeholder="Example: ffffff" class="form-control" value="<?php echo !empty($opt['pwa_backgroundcolor']) ? $opt['pwa_backgroundcolor'] : 'ffffff'; ?>" data-wheelcolorpicker>
                    </div>
                    <div class="form-group">
                        <label for="pwa_display">Display UI</label>
                        <select name="opt[pwa_display]" id="pwa_display" class="custom-select">
                            <option value="standalone">Standalone (Default)</option>
                            <option value="fullscreen" <?php echo !empty($opt['pwa_display']) && $opt['pwa_display'] === 'fullscreen' ? 'selected' : ''; ?>>Fullscreen</option>
                            <option value="minimal-ui" <?php echo !empty($opt['pwa_display']) && $opt['pwa_display'] === 'minimal-ui' ? 'selected' : ''; ?>>Minimal</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <button type="submit" class="btn btn-success shadow-sm">
                        <i class="fas fa-save mr-2"></i>
                        <span>Update</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
