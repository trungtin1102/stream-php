<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

$rent = file_exists(BASE_DIR . '.rent');
if (!is_admin()) {
    session_write_close();
    include $adminDir . '/views/403.php';
    exit();
} else {
    session_write_close();
    if (!empty($_POST['opt'])) {
        session_write_close();
        $class = new \InstanceCache();
        $class->deleteItemsByTag('options');

        if (isset($_COOKIE['GeneralSettings'])) {
            $GeneralSettings = json_decode($_COOKIE['GeneralSettings'], true);
            if (!empty($GeneralSettings)) {
                foreach ($GeneralSettings as $key) {
                    if (!empty($_POST['opt'][$key])) {
                        if ($key === 'main_site') set_option($key, rtrim($_POST['opt'][$key], '/') . '/');
                        else set_option($key, $_POST['opt'][$key]);
                    } elseif (!empty($key)) {
                        set_option($key, '');
                    }
                }
                if ($rent) {
                    set_option('cache_instance', 'redis');
                    set_option('disable_validation', '');
                    set_option('production_mode', 'true');
                }
                create_alert('success', 'General settings updated successfully.', $_SERVER['REQUEST_URI']);
            } else {
                create_alert('danger', 'Failed to update general settings.', $_SERVER['REQUEST_URI']);
            }
        } else {
            create_alert('danger', 'Cannot access the cookies from your browser!', $_SERVER['REQUEST_URI']);
        }
    } else {
        session_write_close();
        $opt = get_option();
    }
}

$timezone = [];
$class = new \InstanceCache();
$class->setKey('timezone');
$cache = $class->get();
if ($cache) {
    $timezone = $cache;
} else {
    open_resources_handler();
    $fp = @fopen(BASE_DIR . 'includes/timezone.json', 'rb');
    if ($fp) {
        stream_set_blocking($fp, false);
        $timezone = stream_get_contents($fp);
        fclose($fp);

        $timezone = json_decode($timezone, true);
        $class->save($timezone, 2592000);
    }
}
$timezone = is_array($timezone) ? $timezone : json_decode($timezone, true);
sort($timezone);

$title = 'General Settings';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">General Settings</h1>
        <form action="<?php echo $adminURL; ?>/settings/general" id="frmSettings" data-cookie="GeneralSettings" method="post" class="needs-validation" autocomplete="off" novalidate>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="main_site">Main Site Homepage URL</label>
                        <input type="url" name="opt[main_site]" id="main_site" placeholder="Example: http://mainsite.com/" class="form-control" value="<?php echo !empty($opt['main_site']) ? $opt['main_site'] : BASE_URL; ?>" required>
                        <small class="form-text text-muted">Main site as public video player generator. If someone accesses the video player generator from a load balancer site then they will be redirected to this main site.</small>
                    </div>
                    <div class="form-group">
                        <label for="timezone">Timezone</label>
                        <select name="opt[timezone]" id="timezone" class="custom-select select2">
                            <?php
                            foreach ($timezone as $dt) {
                                echo '<option value="' . $dt . '" ' . (isset($opt['timezone']) && $opt['timezone'] === $dt ? 'selected' : '') . '>' . $dt . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <?php if (!$rent) : ?>
                        <div class="form-group">
                            <label for="cache_instance">Cache Instance</label>
                            <select name="opt[cache_instance]" id="cache_instance" class="custom-select">
                                <option value="files">Files (Default)</option>
                                <option value="sqlite" <?php echo !empty($opt['cache_instance']) && $opt['cache_instance'] === 'sqlite' ? 'selected' : ''; ?>>SQLite</option>
                                <option value="redis" <?php echo !empty($opt['cache_instance']) && $opt['cache_instance'] === 'redis' ? 'selected' : ''; ?>>Redis</option>
                            </select>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="production_mode" name="opt[production_mode]" value="true" <?php echo !empty($opt['production_mode']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="production_mode">Production Mode</label>
                        </div>
                        <small class="form-text text-muted">If you enabled this option, we will encrypt the javascript code.</small>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="load_balancer_rand" name="opt[load_balancer_rand]" value="true" <?php echo !empty($opt['load_balancer_rand']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="load_balancer_rand">Video Stream Rand</label>
                        </div>
                        <small class="form-text text-muted">Checked = Direct URL (stream) video will be selected from alternative source randomly.<br>Unchecked = A list of alternative hosts will be displayed on the embed page.</small>
                    </div>
                    <?php if (!$rent) : ?>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="disable_validation" name="opt[disable_validation]" value="true" <?php echo !empty($opt['disable_validation']) ? 'checked' : ''; ?>>
                                <label class="custom-control-label" for="disable_validation">Disable Validation</label>
                            </div>
                            <small class="form-text text-muted">If you enable this option then CORS Policy and token validation are disabled. Be careful using this feature as your site is vulnerable to DDoS attacks.</small>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="gdrive_copy" name="opt[gdrive_copy]" value="true" <?php echo !empty($opt['gdrive_copy']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="gdrive_copy">Enable Always Copy Google Drive Files</label>
                        </div>
                        <small class="form-text text-muted">If you enable this option, Google Drive videos embedded by users or the public will be backed up to any Google Drive account.</small>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="gdrive_copy_all" name="opt[gdrive_copy_all]" value="true" <?php echo !empty($opt['gdrive_copy_all']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="gdrive_copy_all">Enable Always Copy Google Drive Files to All Accounts</label>
                        </div>
                        <small class="form-text text-muted">If you enable this option, Google Drive videos embedded by users or public will be backed up to all Google Drive accounts.</small>
                    </div>
                    <div class="form-group">
                        <label for="visit_counter">Visit Counter</label>
                        <input type="number" min="1" class="form-control" name="opt[visit_counter]" id="visit_counter" placeholder="Insert Max. Views Per Video/IP" value="<?php echo !empty($opt['visit_counter']) ? intval($opt['visit_counter']) : 1; ?>">
                        <small class="form-text text-muted">Maximum number of views per Video/IP within 24 hours.</small>
                    </div>
                    <div class="form-group">
                        <label for="visit_counter_runtime">Visit Counter Running Time</label>
                        <div class="input-group">
                            <input type="number" min="1" class="form-control" name="opt[visit_counter_runtime]" id="visit_counter_runtime" placeholder="Insert Max. Views Per Video/IP" value="<?php echo !empty($opt['visit_counter_runtime']) ? intval($opt['visit_counter_runtime']) : 60; ?>">
                            <div class="input-group-append">
                                <div class="input-group-text">Seconds</div>
                            </div>
                        </div>
                        <small class="form-text text-muted">The visit counter will run once the video is played equal to/more than that time.</small>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="maxmind_license_key">MaxMind License Key</label>
                        <input type="text" class="form-control" name="opt[maxmind_license_key]" id="maxmind_license_key" value="<?php echo !empty($opt['maxmind_license_key']) ? $opt['maxmind_license_key'] : ''; ?>" placeholder="Insert MaxMind License Key">
                        <small class="form-text text-muted">You need MaxMind License Key to update GeoIP information for validation system to work properly to avoid DDoS attacks.</small>
                    </div>
                    <?php if (class_exists('uptobox')) : ?>
                        <div class="form-group">
                            <label for="uptobox_api">Uptobox API Token</label>
                            <input type="text" class="form-control" name="opt[uptobox_api]" id="uptobox_api" value="<?php echo !empty($opt['uptobox_api']) ? $opt['uptobox_api'] : ''; ?>" placeholder="Insert Uptobox API Token">
                        </div>
                    <?php endif; ?>
                    <?php if (class_exists('Anti_Captcha')) : ?>
                        <div class="form-group">
                            <label for="anti_captcha"><a href="https://bit.ly/3rCQPtU" target="_blank" rel="noopener">Anti-Captcha</a> API Key</label>
                            <input type="text" class="form-control" name="opt[anti_captcha]" id="anti_captcha" value="<?php echo !empty($opt['anti_captcha']) ? $opt['anti_captcha'] : ''; ?>" placeholder="Insert Anti-Captcha API Key">
                            <small class="form-text text-muted">This Anti-Captcha API key will be used to fetch videos from various hostings. Ex. To get DoodStream sources, get Uptobox multi-resolution</small>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <label for="google_analytics_id"><a href="https://analytics.google.com/" target="_blank" rel="noopener">Google Analytics</a> ID</label>
                        <input type="text" class="form-control" name="opt[google_analytics_id]" id="google_analytics_id" placeholder="Example: UA-123456" value="<?php echo !empty($opt['google_analytics_id']) ? htmlspecialchars($opt['google_analytics_id']) : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label for="google_tag_manager"><a href="https://tagmanager.google.com/" target="_blank" rel="noopener">Google Tag Manager</a> ID</label>
                        <input type="text" class="form-control" name="opt[google_tag_manager]" id="google_tag_manager" placeholder="Example: GTM-123456" value="<?php echo !empty($opt['google_tag_manager']) ? htmlspecialchars($opt['google_tag_manager']) : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label for="histats_id"><a href="https://www.histats.com/" target="_blank" rel="noopener">Histats</a> SID</label>
                        <input type="text" class="form-control" name="opt[histats_id]" id="histats_id" placeholder="Insert Histats SID" value="<?php echo !empty($opt['histats_id']) ? htmlspecialchars($opt['histats_id']) : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recaptcha_site_key"><a href="https://www.google.com/recaptcha/admin" target="_blank" rel="noopener">Google ReCaptcha</a> Site Key</label>
                        <input type="text" class="form-control" name="opt[recaptcha_site_key]" id="recaptcha_site_key" placeholder="Insert Google ReCaptcha Site Key" value="<?php echo !empty($opt['recaptcha_site_key']) ? htmlspecialchars($opt['recaptcha_site_key']) : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recaptcha_secret_key">Google ReCaptcha Secret Key</label>
                        <input type="text" class="form-control" name="opt[recaptcha_secret_key]" id="recaptcha_secret_key" placeholder="Insert Google ReCaptcha Secret Key" value="<?php echo !empty($opt['recaptcha_secret_key']) ? htmlspecialchars($opt['recaptcha_secret_key']) : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label for="disqus_shortname">Disqus Shortname</label>
                        <div class="input-group">
                            <input type="text" name="opt[disqus_shortname]" id="disqus_shortname" placeholder="Insert Disqus Shortname" class="form-control" value="<?php echo !empty($opt['disqus_shortname']) ? $opt['disqus_shortname'] : ''; ?>">
                            <div class="input-group-append">
                                <div class="input-group-text">.disqus.com</div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="chat_widget">Chat Widget Script</label>
                        <textarea name="opt[chat_widget]" id="chat_widget" cols="30" rows="5" class="form-control" placeholder="Enter the Chat Widget javascript code (with the <script> tag)"><?php echo !empty($opt['chat_widget']) ? trim(htmlspecialchars($opt['chat_widget'])) : ''; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <button type="submit" class="btn btn-success shadow-sm">
                        <i class="fas fa-save mr-2"></i>
                        <span>Update</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
