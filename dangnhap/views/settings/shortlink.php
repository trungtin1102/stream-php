<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!is_admin()) {
    session_write_close();
    include $adminDir . '/views/403.php';
    exit();
} else {
    session_write_close();
    if (!empty($_POST['opt'])) {
        session_write_close();
        $class = new \InstanceCache();
        $class->deleteItemsByTag('options');
        
        if (isset($_COOKIE['SLinkSettings'])) {
            $SLinkSettings = json_decode($_COOKIE['SLinkSettings'], true);
            if (!empty($SLinkSettings)) {
                foreach ($SLinkSettings as $key) {
                    if (!empty($_POST['opt'][$key])) {
                        set_option($key, $_POST['opt'][$key]);
                    } elseif (!empty($key))  {
                        set_option($key, '');
                    }
                }
                create_alert('success', 'Shortener link settings updated successfully.', $_SERVER['REQUEST_URI']);
            } else {
                create_alert('danger', 'Failed to update shortener link settings.', $_SERVER['REQUEST_URI']);
            }
        } else {
            create_alert('danger', 'Cannot access the cookies from your browser!', $_SERVER['REQUEST_URI']);
        }
    } else {
        session_write_close();
        $opt = get_option();
    }
}

$title = 'Shortener Link Settings';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">Shortener Link Settings</h1>
        <form action="<?php echo $adminURL; ?>/settings/shortlink" id="frmSettings" data-cookie="SLinkSettings" method="post" class="needs-validation" autocomplete="off" novalidate>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="opt[disable_shortener_link]" id="disable_shortener_link" value="true" <?php echo !empty($opt['disable_shortener_link']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="disable_shortener_link">Disable Shortener Link</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12 col-md-6">
                            <label for="main_url_shortener">Main URL Shortener (bitly.com)</label>
                            <input type="text" name="opt[main_url_shortener]" id="main_url_shortener" class="form-control" placeholder="Enter the access token from bitly.com" value="<?php echo !empty($opt['main_url_shortener']) ? trim(htmlspecialchars($opt['main_url_shortener'])) : ''; ?>">
                            <small class="form-text text-muted">
                                You can find out how to get a bitly.com access token from the following link <a href="https://support.bitly.com/hc/en-us/articles/230647907-How-do-I-find-my-OAuth-access-token-" target="_blank">https://support.bitly.com/hc/en-us/articles/230647907-How-do-I-find-my-OAuth-access-token-</a>
                            </small>
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="additional_url_shortener">Select Additional URL Shortener</label>
                            <?php echo earnmoney_website('additional_url_shortener', (!empty($opt['additional_url_shortener']) ? trim(htmlspecialchars($opt['additional_url_shortener'])) : '')); ?>
                            <small class="form-text text-muted">Additional URL shorteners are used to earn money online and will be used on the download page.</small>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        $urlShortener = earnmoney_website('', '', false, true);
                        $urlShortener = json_decode($urlShortener, TRUE);
                        foreach ($urlShortener as $key => $value) {
                            if ($key !== 'random') {
                                $name = 'additional_url_shortener_' . $key;
                        ?>
                                <div class="form-group col-12 col-md-6">
                                    <label for="<?php echo $key; ?>">
                                        <a href="http://<?php echo $key; ?>" target="_blank"><?php echo $value; ?></a> API Key
                                    </label>
                                    <input type="text" name="opt[<?php echo $name; ?>]" id="<?php echo $name; ?>" class="form-control" placeholder="Enter <?php echo $key; ?> API Key" value="<?php echo !empty($opt[$name]) ? trim(htmlspecialchars($opt[$name])) : ''; ?>">
                                </div>
                        <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <button type="submit" class="btn btn-success shadow-sm">
                        <i class="fas fa-save mr-2"></i>
                        <span>Update</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
