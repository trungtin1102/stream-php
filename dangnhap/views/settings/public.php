<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

$rent = file_exists(BASE_DIR . '.rent');
if (!is_admin()) {
    session_write_close();
    include $adminDir . '/views/403.php';
    exit();
} else {
    session_write_close();
    if (!empty($_POST['opt'])) {
        session_write_close();
        $class = new \InstanceCache();
        $class->deleteItemsByTag('options');

        if (isset($_COOKIE['PublicSettings'])) {
            $PublicSettings = json_decode($_COOKIE['PublicSettings'], true);
            if (!empty($PublicSettings)) {
                foreach ($PublicSettings as $key) {
                    if (!empty($_POST['opt'][$key])) {
                        set_option($key, $_POST['opt'][$key]);
                    } elseif (!empty($key)) {
                        set_option($key, '');
                    }
                }
                if ($rent) {
                    set_option('anonymous_generator', 'true');
                }
                deleteDir(BASE_DIR . 'cache/js');
                create_alert('success', 'Public settings updated successfully.', $_SERVER['REQUEST_URI']);
            } else {
                create_alert('danger', 'Failed to update public settings.', $_SERVER['REQUEST_URI']);
            }
        } else {
            create_alert('danger', 'Cannot access the cookies from your browser!', $_SERVER['REQUEST_URI']);
        }
    } else {
        session_write_close();
        $opt = get_option();
    }
}

$title = 'Public Settings';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">Public Settings</h1>
        <form action="<?php echo $adminURL; ?>/settings/public" id="frmSettings" data-cookie="PublicSettings" method="post" class="needs-validation" autocomplete="off" novalidate>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="anonymous_generator" name="opt[anonymous_generator]" value="true" <?php echo !empty($opt['anonymous_generator']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="anonymous_generator">Enable Public Video Generator</label>
                        </div>
                        <small class="form-text text-muted">If you enable this option, people can use this website to play their videos.</small>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="embed_only" name="opt[embed_only]" value="true" <?php echo !empty($opt['embed_only']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="embed_only">Embed Only</label>
                        </div>
                        <small class="form-text text-muted">If you enable this option, the embed url must be accessed with an iframe.</small>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="enable_download_page" name="opt[enable_download_page]" value="true" <?php echo !empty($opt['enable_download_page']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="enable_download_page">Enable Download Page</label>
                        </div>
                        <small class="form-text text-muted">If you enable this option, people can download videos from the download page.</small>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="hide_sub_download" name="opt[hide_sub_download]" value="true" <?php echo !empty($opt['hide_sub_download']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="hide_sub_download">Hide Subtitles on Download Page</label>
                        </div>
                        <small class="form-text text-muted">If you enable this option, subtitles will not be displayed on the download page.</small>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="save_public_video" name="opt[save_public_video]" value="true" <?php echo !empty($opt['save_public_video']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="save_public_video">Save Public Video</label>
                        </div>
                        <small class="form-text text-muted">If you enable this option, any videos submitted by the public will be stored in the database.</small>
                    </div>
                    <div class="form-group">
                        <label for="public_video_user">Save Public Videos As User</label>
                        <select name="opt[public_video_user]" id="public_video_user" class="custom-select select2">
                            <?php
                            $selUser = !empty($opt['public_video_user']) ? $opt['public_video_user'] : '';
                            $class = new \Users();
                            $list = $class->get(['id', 'name']);
                            if ($list) {
                                foreach ($list as $row) {
                                    echo '<option value="' . $row['id'] . '" ' . (intval($selUser) === intval($row['id']) ? 'selected' : '') . '>' . $row['name'] . '</option>';
                                }
                            }
                            ?>
                        </select>
                        <small class="form-text text-muted">Any videos submitted by the public will be stored in the database using this user.</small>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="enable_gsharer" name="opt[enable_gsharer]" value="true" <?php echo !empty($opt['enable_gsharer']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="enable_gsharer">Enable Google Sharer Page</label>
                        </div>
                        <small class="form-text text-muted">If you enable this option, the public can bypass their Google Drive file limit via the Bypass Limit page.</small>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="enable_gdrive_downloader" name="opt[enable_gdrive_downloader]" value="true" <?php echo !empty($opt['enable_gdrive_downloader']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="enable_gdrive_downloader">Enable Google Drive Downloader via Ajax</label>
                        </div>
                        <small class="form-text text-muted">If you enable this option, the public can download the Google Drive bypass file via ajax via the Bypass Limit page.</small>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="disable_registration" name="opt[disable_registration]" value="true" <?php echo !empty($opt['disable_registration']) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="disable_registration">Disable Registration Page</label>
                        </div>
                        <small class="form-text text-muted">If you enable this option, the public cannot register.</small>
                    </div>
                    <div class="form-group">
                        <label for="dmca_page_link">DMCA Page URL</label>
                        <input type="url" name="opt[dmca_page_link]" id="dmca_page_link" class="form-control" placeholder="Insert DMCA page URL" value="<?php if (isset($opt['dmca_page_link'])) echo $opt['dmca_page_link']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="contact_page_link">Contact Page URL</label>
                        <input type="url" name="opt[contact_page_link]" id="contact_page_link" class="form-control" placeholder="Insert contact page URL" value="<?php if (isset($opt['dmca_page_link'])) echo $opt['contact_page_link']; ?>">
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <button type="submit" class="btn btn-success shadow-sm">
                        <i class="fas fa-save mr-2"></i>
                        <span>Update</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
