<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!is_admin()) {
    include $adminDir . '/views/403.php';
    exit();
}

$title = 'Google Drive Accounts';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">Google Drive Accounts</h1>
        <p>You can add a Google Drive account from this menu without creating a json file manually.</p>
        <div class="row mb-3">
            <div class="col-12 col-md-8">
                <a href="<?php echo $adminURL; ?>/gdrive_accounts/new" class="btn shadow-sm btn-success btn-sm mr-2">
                    <i class="fas fa-plus-circle"></i>
                    <span class="ml-2">Add New</span>
                </a>
                <button type="button" class="btn shadow-sm btn-danger btn-sm mr-2" onclick="gdrive_accounts.deleteChecked();">
                    <i class="fas fa-trash-alt mr-2"></i>Delete
                </button>
                <button type="button" class="btn shadow-sm btn-info btn-sm mr-2" onclick="gdrive_accounts.reload();">
                    <i class="fas fa-sync-alt mr-2"></i>Reload
                </button>
            </div>
        </div>
        <table id="tbGDAccounts" class="table table-striped table-bordered table-hover table-sm m-0" style="width:100%">
            <thead>
                <tr>
                    <th style="width:60px;max-width:60px">
                        <div class="custom-control custom-checkbox mx-auto">
                            <input type="checkbox" class="custom-control-input" id="ckAllGDA">
                            <label class="custom-control-label" for="ckAllGDA"></label>
                        </div>
                    </th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Added On</th>
                    <th>Updated On</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th style="width:60px;max-width:60px">
                        <div class="custom-control custom-checkbox mx-auto">
                            <input type="checkbox" class="custom-control-input" id="ckAllGDA1">
                            <label class="custom-control-label" for="ckAllGDA1"></label>
                        </div>
                    </th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Added On</th>
                    <th>Updated On</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>
        <script>
            
        </script>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
