<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

$rent = file_exists(BASE_DIR . '.rent');
$rent_slug = $rent ? SECURE_SALT : 'load_balancers';
if (!is_admin()) {
    session_write_close();
    include $adminDir . '/views/403.php';
    exit();
} else {
    session_write_close();
    if (!empty($_POST)) {
        if (!$rent || ($rent && !empty($_POST['password']) && $_POST['password'] === SECURE_SALT)) {
            $replace = ['https:' => '', 'http:' => '', 'www.'];
            $mainSite = strtr(rtrim(get_option('main_site'), '/'), $replace);
            $link = strtr(rtrim($_POST['link'], '/'), $replace);
            if ($mainSite === $link) {
                create_alert('danger', 'The main site should not be used as a load balancer.', $_SERVER['REQUEST_URI']);
            } else {
                unset($_POST['password']);
                $time = time();
                $_POST['added'] = $time;
                $_POST['updated'] = $time;
                $_POST['link'] = rtrim($_POST['link'], '/') . '/';
                $_POST['status'] = isset($_POST['status']) ? 1 : 0;
                $_POST['public'] = isset($_POST['public']) ? 1 : 0;
                $_POST['disallow_hosts'] = isset($_POST['disallow_hosts']) ? $_POST['disallow_hosts'] : [];

                $class = new \LoadBalancers();
                $id = $class->insert($_POST);
                if ($id) {
                    create_alert('success', 'New load balancer added successfully.', strtr(rtrim($_SERVER['REQUEST_URI'], '/'), ['/new' => '/edit/?id=' . $id]));
                } else {
                    create_alert('danger', $class->getLastError(), $_SERVER['REQUEST_URI']);
                }
            }
        } else {
            create_alert('danger', 'You are not authorized to access this feature!', $_SERVER['REQUEST_URI']);
        }
    }
    $supportedSites = \Hosting::supportedSites();
    ksort($supportedSites);
}

$title = 'New Load Balancer';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">New Load Balancer</h1>
        <form action="<?php echo $adminURL; ?>/<?php echo $rent_slug; ?>/new" method="post" class="needs-validation" novalidate>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Insert load balancer name" required>
                        <div class="invalid-feedback">Must be filled!</div>
                    </div>
                    <div class="form-group">
                        <label for="link">Homepage URL</label>
                        <input type="url" name="link" id="link" class="form-control" placeholder="Example: http://example.com/" required>
                        <div class="invalid-feedback">Must be valid!</div>
                    </div>
                    <div class="form-group row">
                        <label for="status" class="col-2">Status</label>
                        <div class="col-10">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="status" id="status" value="1">
                                <label class="custom-control-label" for="status">Active</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="public" class="col-2">Public</label>
                        <div class="col-10">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="public" id="public" value="1">
                                <label class="custom-control-label" for="public">Active</label>
                            </div>
                            <small class="form-text text-muted">If you enable this option, then the public can use the video generator freely.</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="disallow_hosts">Disabled Hosts</label>
                        <select multiple="multiple" id="disallow_hosts" name="disallow_hosts[]" class="multi-select">
                            <?php
                            if (!empty($supportedSites)) {
                                foreach ($supportedSites as $k => $v) {
                                    list($v, $badge) = array_pad(explode('|', $v), 2, '');
                                    echo '<option value="' . $k . '">' . $v . '</option>';
                                }
                            }
                            ?>
                        </select>
                        <small class="form-text text-muted">The disabled hosts are in the right or bottom column.</small>
                    </div>
                    <?php if ($rent) : ?>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" id="password" required>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-12 col-md-6">
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <button type="submit" class="btn btn-success shadow-sm mr-2">
                        <i class="fas fa-save mr-2"></i>
                        <span>Save</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
