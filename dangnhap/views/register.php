<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (current_user()) {
    session_write_close();
    header('location: ' . $adminURL . '/videos/');
    exit();
} else {
    session_write_close();
    if (file_exists(BASE_DIR . '.rent') || filter_var(get_option('disable_registration'), FILTER_VALIDATE_BOOLEAN)) {
        session_write_close();
        header('location: ' . $adminURL . '/login/');
        exit();
    } elseif (isset($_POST['submit'])) {
        session_write_close();
        $sitename = sitename();
        $recaptchValidation = recaptcha_validate($_POST['captcha-response']);
        if ($recaptchValidation) {
            $class = new \Users();

            $class->setCriteria('user', $_POST['user']);
            $userExist = $class->getNumRows() > 0;

            $class->setCriteria('email', $_POST['email']);
            $emailExist = $class->getNumRows() > 0;

            // validasi
            if ($emailExist) {
                create_alert('warning', 'The email is already registered, please use another one!', $adminURL . '/register');
            } elseif ($userExist) {
                create_alert('warning', 'The username is already registered, please use another one!', $adminURL . '/register');
            } elseif ($_POST['password'] !== $_POST['retype_password']) {
                create_alert('warning', 'Confirm password must be the same as the password!', $adminURL . '/register');
            } elseif (!empty($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                if ($_POST['password'] === $_POST['retype_password']) {
                    $expired = time() + 600;
                    $confirm = filter_var(get_option('disable_confirm'), FILTER_VALIDATE_BOOLEAN);
                    $smtp_email = get_option('smtp_email');
                    $smtp_password = get_option('smtp_password');

                    if (!$confirm && !empty($smtp_email) && !empty($smtp_password)) {
                        open_resources_handler();
                        $fp = @fopen($adminDir . '/templates/registration-email.html', 'rb');
                        if ($fp) {
                            stream_set_blocking($fp, false);
                            $message = stream_get_contents($fp);
                            fclose($fp);

                            $link = $adminURL . '/register/?token=' . encode($_POST['email'] . '|' . $expired);
                            $message = strtr($message, [
                                '{recepient_name}' => $_POST['name'],
                                '{email_confirmation_link}' => $link,
                                '{sitename}' => $sitename
                            ]);

                            $time = time();
                            $_POST['added'] = $time;
                            $_POST['updated'] = $time;
                            $_POST['role'] = 1;
                            $_POST['status'] = 2;
                            $_POST['password'] = password_hash($_POST['password'], PASSWORD_BCRYPT);

                            unset($_POST['submit']);
                            unset($_POST['action']);
                            unset($_POST['retype_password']);
                            unset($_POST['captcha-response']);
                            unset($_POST['g-recaptcha-response']);

                            try {
                                $mail = new \Mailer();
                                $send = $mail->send([
                                    'sendto' => [
                                        'name' => $_POST['name'],
                                        'email' => $_POST['email']
                                    ],
                                    'subject' => 'Confirmation email (' . $sitename . ') | ' . $_POST['name'],
                                    'message' => $message
                                ]);
                                if ($send) {
                                    $insert = $class->insert($_POST);
                                    if ($insert) {
                                        create_alert('success', 'Registration successful! Please check the confirmation email in the inbox / spam folder in your email.', $adminURL . '/register');
                                    } else {
                                        create_alert('danger', $class->getLastError(), $adminURL . '/register');
                                    }
                                } else {
                                    create_alert('warning', $class->getLastError(), $adminURL . '/register');
                                }
                            } catch (\Exception $e) {
                                create_alert('danger', $e->getMessage(), $adminURL . '/register');
                            }
                        } else {
                            create_alert('danger', 'Registration is disabled! Please contact admin.', $adminURL . '/reset-password');
                        }
                    } else {
                        $time = time();
                        $_POST['added'] = $time;
                        $_POST['updated'] = $time;
                        $_POST['status'] = 1;
                        $_POST['role']   = 1;
                        $_POST['password'] = password_hash($_POST['password'], PASSWORD_BCRYPT);

                        unset($_POST['submit']);
                        unset($_POST['action']);
                        unset($_POST['retype_password']);
                        unset($_POST['captcha-response']);
                        unset($_POST['g-recaptcha-response']);

                        $insert = $class->insert($_POST);
                        if ($insert) {
                            create_alert('success', 'Registration successful!', $adminURL . '/login');
                        } else {
                            create_alert('danger', $class->getLastError(), $adminURL . '/register');
                        }
                    }
                } else {
                    create_alert('danger', 'New password and retype password doesn\'t match.', $adminURL . '/register');
                }
            } else {
                create_alert('warning', 'Incorrect email format!', $adminURL . '/register');
            }
        } else {
            create_alert('danger', 'The security code entered is incorrect!', $adminURL . '/register');
        }
    } elseif (!empty($_GET['token'])) {
        session_write_close();
        $token = explode('|', decode($_GET['token']));
        $email = $token[0];
        $expired = (int) end($token);
        $now = time();
        if ($now < $expired) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $class = new \Users();
                $class->setCriteria('email', $email);
                $updated = $class->update(array(
                    'status' => 1,
                    'updated' => $now
                ));
                if ($updated) {
                    create_alert('success', 'Registration is successful! Now you can login.', $adminURL . '/login');
                } else {
                    create_alert('danger', $e->getMessage(), $adminURL . '/register');
                }
            } else {
                create_alert('warning', 'Your email is invalid! Please enter a valid email.', $adminURL . '/register');
            }
        } else {
            $newExp = $now + 600;
            $resendLink = $adminURL . '/register/?token=' . encode($email . '|' . $newExp);
            create_alert('warning', 'The email confirmation link has expired, <a href="' . $resendLink . '">resend the link</a>.', $adminURL . '/register');
        }
    }
}

$title = 'Register';
include $adminDir . '/header.php';
?>
<div class="row py-5">
    <div class="col-12 col-md-6 col-lg-4 mx-auto">
        <h1 class="h3 my-3 text-center">Register</h1>
        <?php echo show_alert(); ?>
        <form id="frm" action="<?php echo $adminURL . '/register'; ?>" method="post" class="needs-validation" novalidate>
            <div class="form-group">
                <label for="username">Name</label>
                <input type="text" id="name" name="name" class="form-control" placeholder="Your name" required>
                <div class="invalid-feedback">Must be filled!</div>
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" id="user" name="user" class="form-control" placeholder="Your username" required>
                <div class="invalid-feedback">Must be filled!</div>
            </div>
            <div class="form-group">
                <label for="username">Email</label>
                <input type="email" id="email" name="email" class="form-control" placeholder="Your email" required>
                <div class="invalid-feedback">Must be filled!</div>
            </div>
            <div class="form-group">
                <label for="password">New Password</label>
                <input type="password" autocomplete="false" id="password" name="password" class="form-control" placeholder="New Password" onchange="if($(this).val() === $('#retype_password').val()) $('#retype_password').removeClass('is-invalid').addClass('is-valid'); else $('#retype_password').removeClass('is-valid').addClass('is-invalid');" required>
                <div class="invalid-feedback">Must be filled!</div>
            </div>
            <div class="form-group">
                <label for="password">Confirm New Password</label>
                <input type="password" autocomplete="false" id="retype_password" name="retype_password" class="form-control" placeholder="Confirm New Password" onchange="if($(this).val() !== $('#password').val()) $(this).removeClass('is-valid').addClass('is-invalid'); else $(this).removeClass('is-invalid').addClass('is-valid');" required>
                <div class="invalid-feedback">The new password confirmation must be the same as the new password!</div>
            </div>
            <div class="form-group text-center">
                <div class="row">
                    <div class="col-6">
                        <a href="<?php echo $adminURL . '/'; ?>" class="btn btn-block btn-secondary shadow-sm">
                            <i class="fas fa-arrow-left"></i><span class="ml-2">Login</span>
                        </a>
                    </div>
                    <div class="col-6">
                        <?php
                        $recaptcha_site_key = get_option('recaptcha_site_key');
                        if ($recaptcha_site_key) :
                        ?>
                            <div id="g-recaptcha" class="g-recaptcha" data-sitekey="<?php echo $recaptcha_site_key; ?>" data-size="invisible" data-callback="gCallback"></div>
                        <?php
                        endif;
                        ?>
                        <input type="hidden" id="captcha-response" name="captcha-response" />
                        <button type="submit" name="submit" class="btn btn-block btn-custom shadow-sm">
                            <i class="fas fa-check"></i><span class="ml-2">Register</span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <div class="form-group">
            <div class="row">
                <div class="col-12 col-md-9 mx-auto">
                    <div id="btn-google-login"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
