<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (current_user()) {
    session_write_close();
    header('location: ' . $adminURL . '/videos/');
    exit();
} elseif (isset($_POST['submit'])) {
    session_write_close();
    $sitename = sitename();
    $recaptchValidation = recaptcha_validate($_POST['captcha-response']);
    $token = !empty($_POST['token']) ? $_POST['token'] : '';
    if ($recaptchValidation) {
        if (!isset($_GET['save']) && !empty($_POST['username'])) {
            session_write_close();
            $class = new \Users();

            $class->setCriteria('user', $_POST['username']);
            $userExist = $class->getOne();

            $class->setCriteria('email', $_POST['username']);
            $emailExist = $class->getOne();

            // validasi
            if ($emailExist || $userExist) {
                $data = $userExist ? $userExist : $emailExist;
                $exp = time() + 600;
                $disableConfirm = filter_var(get_option('disable_confirm'), FILTER_VALIDATE_BOOLEAN);
                if (!$disableConfirm) {
                    open_resources_handler();
                    $fp = @fopen($adminDir . '/templates/reset-password-email.html', 'rb');
                    if ($fp) {
                        stream_set_blocking($fp, false);
                        $message = stream_get_contents($fp);
                        fclose($fp);

                        $link = $adminURL . '/reset-password/?token=' . encode($data['email'] . '|' . $exp);
                        $message = strtr($message, [
                            '{recepient_name}' => $data['name'],
                            '{reset_password_link}' => $link,
                            '{sitename}' => $sitename
                        ]);

                        $mail = new \Mailer();
                        $send = $mail->send([
                            'sendto' => [
                                'name' => $data['name'],
                                'email' => $data['email']
                            ],
                            'subject' => 'Confirmation email (' . $sitename . ') | ' . $data['name'],
                            'message' => $message
                        ]);

                        if ($send) {
                            create_alert('success', 'A link to reset your password has been sent to your email.', $adminURL . '/reset-password');
                        } else {
                            create_alert('warning', $class->getLastError(), $adminURL . '/reset-password');
                        }
                    } else {
                        create_alert('danger', 'Password reset is disabled! Please contact admin.', $adminURL . '/reset-password');
                    }
                } else {
                    if (intval($data['role']) === 1) {
                        $token = encode($data['email'] . '|' . $exp);
                        create_alert('success', 'Please insert your new password!', $adminURL . '/reset-password/?token=' . $token);
                    } else {
                        create_alert('danger', 'Password reset is disabled! Please contact admin.', $adminURL . '/reset-password');
                    }
                }
            } else {
                create_alert('danger', 'User not found!', $adminURL . '/reset-password');
            }
        } else {
            session_write_close();
            if (empty($_POST['password']) || empty($_POST['retype_password'])) {
                create_alert('warning', 'Enter your new password!', $adminURL . '/reset-password/?token=' . $token);
            } elseif ($_POST['password'] !== $_POST['retype_password']) {
                create_alert('warning', 'Confirm password must be the same as the password!', $adminURL . '/reset-password&token=' . $token);
            } else {
                $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
                $class = new \Users();
                $class->setCriteria('email', $_POST['email']);
                $updated = $class->update(array(
                    'password' => $password,
                    'updated' => time()
                ));
                if ($updated) {
                    create_alert('success', 'Your password has been updated successfully!', $adminURL . '/login');
                } else {
                    create_alert('success', 'Error! You cannot reset your password at this time.', $adminURL . '/reset-password/?token=' . $token);
                }
            }
        }
    }
} else {
    session_write_close();
    if (!empty($_GET['token'])) {
        $token = decode($_GET['token']);
        list($email, $expired) = explode('|', $token, 2);
    } else {
        $token = '';
        $email = '';
        $expired = 0;
    }
}

$title = 'Reset Password';
include $adminDir . '/header.php';
if (!empty($token)) :
    if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL) && $expired > time()) :
?>
        <div class="row py-5">
            <div class="col-12 col-md-6 col-lg-4 mx-auto">
                <h1 class="h3 my-3 text-center">Reset Password</h1>
                <?php echo show_alert(); ?>
                <form id="frm" action="<?php echo $adminURL . '/reset-password/?save=true'; ?>" method="post" class="needs-validation" novalidate>
                    <div class="form-group">
                        <label for="password">New Password</label>
                        <input type="password" autocomplete="false" id="password" name="password" class="form-control" placeholder="New Password" onchange="if($(this).val() === $('#retype_password').val()) $('#retype_password').removeClass('is-invalid').addClass('is-valid'); else $('#retype_password').removeClass('is-valid').addClass('is-invalid');" required>
                        <div class="invalid-feedback">Must be filled!</div>
                    </div>
                    <div class="form-group">
                        <label for="password">Confirm New Password</label>
                        <input type="password" autocomplete="false" id="retype_password" name="retype_password" class="form-control" placeholder="Confirm New Password" onchange="if($(this).val() !== $('#password').val()) $(this).removeClass('is-valid').addClass('is-invalid'); else $(this).removeClass('is-invalid').addClass('is-valid');" required>
                        <div class="invalid-feedback">The new password confirmation must be the same as the new password!</div>
                    </div>
                    <div class="form-group text-center">
                        <div class="row">
                            <div class="col-12">
                                <?php
                                $recaptcha_site_key = get_option('recaptcha_site_key');
                                if ($recaptcha_site_key) :
                                ?>
                                    <div id="g-recaptcha" class="g-recaptcha" data-sitekey="<?php echo $recaptcha_site_key; ?>" data-size="invisible" data-callback="gCallback"></div>
                                <?php
                                endif;
                                ?>
                                <input type="hidden" id="captcha-response" name="captcha-response" />
                                <input type="hidden" name="email" value="<?php echo $email; ?>">
                                <input type="hidden" name="expired" value="<?php echo $expired; ?>">
                                <input type="hidden" name="token" value="<?php echo $token; ?>">
                                <button type="submit" name="submit" class="btn btn-custom btn-block shadow-sm">
                                    <i class="fas fa-sync-alt"></i><span class="ml-2">Reset Password</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php
    endif;
else :
    ?>
    <div class="row py-5">
        <div class="col-12 col-md-6 col-lg-4 mx-auto">
            <h1 class="h3 my-3 text-center">Reset Password</h1>
            <?php echo show_alert(); ?>
            <form id="frm" action="<?php echo $adminURL . '/reset-password/'; ?>" method="post" class="needs-validation" novalidate>
                <div class="form-group">
                    <label for="username">Username / Email</label>
                    <input type="text" id="username" name="username" class="form-control" placeholder="Enter your username / email address" required>
                    <div class="invalid-feedback">Must be filled!</div>
                </div>
                <div class="form-group text-center">
                    <div class="row">
                        <div class="col-7">
                            <?php
                            $recaptcha_site_key = get_option('recaptcha_site_key');
                            if ($recaptcha_site_key) :
                            ?>
                                <div id="g-recaptcha" class="g-recaptcha" data-sitekey="<?php echo $recaptcha_site_key; ?>" data-size="invisible" data-callback="gCallback"></div>
                            <?php
                            endif;
                            ?>
                            <input type="hidden" id="captcha-response" name="captcha-response" />
                            <button type="submit" name="submit" class="btn btn-custom btn-block shadow-sm">
                                <i class="fas fa-sync-alt"></i><span class="ml-2">Reset Password</span>
                            </button>
                        </div>
                        <div class="col-5">
                            <a href="<?php echo $adminURL; ?>/login" class="btn btn-block btn-secondary shadow-sm">
                                <span>Login</span>
                                <i class="fas fa-arrow-right ml-2"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php
endif;
include $adminDir . '/footer.php';
