<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

if (!is_admin()) {
    include $adminDir . '/views/403.php';
    exit();
}

$title = 'User List';
include $adminDir . '/header.php';
?>
<div class="row">
    <div class="col-12">
        <h1 class="h4 mb-3">User List</h1>
        <div class="mb-3">
            <a href="<?php echo $adminURL; ?>/users/new" class="btn shadow-sm btn-success btn-sm mr-2">
                <i class="fas fa-plus-circle"></i>
                <span class="ml-2">Add New</span>
            </a>
            <button type="button" class="btn shadow-sm btn-info btn-sm mr-2" onclick="users.list()">
                <i class="fas fa-sync-alt"></i>
                <span class="ml-2">Reload</span>
            </button>
        </div>
        <table id="tbUsers" class="table table-striped table-bordered table-hover table-sm m-0" style="width:100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Added On</th>
                    <th>Updated On</th>
                    <th>Role</th>
                    <th>Links</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Added On</th>
                    <th>Updated On</th>
                    <th>Role</th>
                    <th>Links</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<?php
include $adminDir . '/footer.php';
