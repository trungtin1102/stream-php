<?php
session_write_close();
ob_start();
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

if (!defined('BASE_DIR')) {
    require 'includes.php';
}

if (is_load_balancer()) {
    header('location: ' . get_option('main_site'));
    exit();
}

$isSSL = isSSL();
$scheme = $isSSL ? 'https:' : 'http:';
$baseURL = strtr(BASE_URL, ['https:' => $scheme, 'http:' => $scheme]);

$adminDir = admin_dir();
$adminURL = BASE_URL . $adminDir;
$adminDir = BASE_DIR . $adminDir;

$sitename = sitename();
$siteslogan = get_option('site_slogan');
$sitedescription = get_option('site_description');
$userLogin = current_user();

$page = get_admin_page();
if (!empty($page)) {
    if (strpos($page, 'p/') !== FALSE) {
        session_write_close();
        $uri = explode('/', trim(strtr($page, ['p/' => '']), '/'));
        $pluginName = $uri[0];
        unset($uri[0]);
        $pluginPage = implode('/', array_values($uri));
        if (!empty($pluginName)) {
            if (!empty($pluginPage)) {
                $pageFile = BASE_DIR . "plugins/$pluginName/admin/views/$pluginPage.php";
            } else {
                $pageFile = BASE_DIR . "plugins/$pluginName/admin/views/$pluginName.php";
            }
        } else {
            $pageFile = BASE_DIR . "includes/pages/$page.php";
        }
        if (file_exists($pageFile)) {
            include $pageFile;
        } else {
            include $adminDir . '/views/404.php';
        }
    } else {
        session_write_close();
        $pageFile = $adminDir . "/views/$page.php";
        if (file_exists(BASE_DIR . '.rent')) {
            if (strpos($page, 'load_balancers') !== false) {
                include $adminDir . '/views/404.php';
            } elseif (strpos($page, SECURE_SALT) !== false) {
                $page = strtr($page, [SECURE_SALT => 'load_balancers']);
                $pageFile = $adminDir . "/views/$page.php";
                if (file_exists($pageFile)) {
                    include $pageFile;
                } else {
                    include $adminDir . '/views/404.php';
                }
            } elseif (file_exists($pageFile)) {
                include $pageFile;
            } else {
                include $adminDir . '/views/login.php';
            }
        } else {
            if (file_exists($pageFile)) {
                include $pageFile;
            } else {
                include $adminDir . '/views/login.php';
            }
        }
    }
} else {
    if ($userLogin) {
        header('location: ' . $adminURL . '/dashboard/');
    } else {
        header('location: ' . $adminURL . '/login/');
    }
    exit();
}
