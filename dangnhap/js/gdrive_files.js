var gdrive_files = {
    publicChecked: function () {
        var ids = [];
        $('#tbGDFiles tbody input[type=checkbox]:checked').each(function () {
            ids.push($(this).val());
        });
        if (ids.length > 0) {
            $('#ckAll, #ckAll1').prop('checked', false);
            swal({
                title: 'Make Public',
                text: 'Are you sure you want to make these ' + ids.length + ' files publicly accessible?',
                type: 'warning',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                cancelButtonClass: 'btn-secondary',
                confirmButtonClass: 'btn-primary',
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;

                var success = [],
                    failed = [],
                    completeCallback = function () {
                        if ((success.length + failed.length) >= ids.length) {
                            swal({
                                title: 'Make Public',
                                text: success.length + ' files have become public, while the other ' + failed.length + ' files are not public yet!',
                                type: 'info'
                            });
                            gdrive_files.reload();
                        }
                    },
                    publicFile = function (id) {
                        ajaxPOST(adminURL + 'ajax/gdrive_files.ajax.php', {
                            action: 'public',
                            id: id,
                            email: $('select#email').val()
                        }, function (result) {
                            if (result.status !== 'fail') success.push(id);
                            else failed.push(id);
                            completeCallback();
                        }, function () {
                            failed.push(id);
                            completeCallback();
                        });
                    };

                for (var i = 0; i < ids.length; i++) {
                    publicFile(ids[i]);
                }
            }
            );
        } else {
            swal('Warning!', 'Please tick some of the files listed below!', 'warning');
        }
    },
    privateChecked: function () {
        var ids = [];
        $('#tbGDFiles tbody input[type=checkbox]:checked').each(function () {
            ids.push($(this).val());
        });
        if (ids.length) {
            $('#ckAll, #ckAll1').prop('checked', false);
            swal({
                title: 'Make Private',
                text: 'Are you sure you want to make ' + ids.length + ' files private?',
                type: 'warning',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                cancelButtonClass: 'btn-secondary',
                confirmButtonClass: 'btn-warning',
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;

                var success = [],
                    failed = [],
                    completeCallback = function () {
                        if ((success.length + failed.length) >= ids.length) {
                            swal({
                                title: 'Make Private',
                                text: success.length + ' files have become private, while the other ' + failed.length + ' files are not private yet!',
                                type: 'info'
                            });
                            gdrive_files.reload();
                        }
                    },
                    privateFile = function (id) {
                        ajaxPOST(adminURL + 'ajax/gdrive_files.ajax.php', {
                            action: 'private',
                            id: id,
                            email: $('select#email').val()
                        }, function (result) {
                            if (result.status !== 'fail') success.push(id);
                            else failed.push(id);
                            completeCallback();
                        }, function () {
                            failed.push(id);
                            completeCallback();
                        });
                    };

                for (var i = 0; i < ids.length; i++) {
                    privateFile(ids[i]);
                }
            });
        } else {
            swal('Warning!', 'Please tick some of the files listed below!', 'warning');
        }
    },
    public: function (id) {
        ajaxPOST(adminURL + 'ajax/gdrive_files.ajax.php', {
            id: id.trim(),
            email: $('select#email').val(),
            action: 'public'
        }, function (res) {
            if (res.status !== 'fail') {
                gdrive_files.reload();
                swal('Success!', res.message, 'success');
            } else {
                swal('Error!', res.message, 'error');
            }
        }, function (xhr) {
            swal('Error!', xhr.responseText, 'error');
        });
    },
    private: function (id) {
        ajaxPOST(adminURL + 'ajax/gdrive_files.ajax.php', {
            id: id,
            email: $('select#email').val(),
            action: 'private'
        }, function (res) {
            if (res.status !== 'fail') {
                gdrive_files.reload();
                swal('Success!', res.message, 'success');
            } else {
                swal('Error!', res.message, 'error');
            }
        }, function (xhr) {
            swal('Error!', xhr.responseText, 'error');
        });
    },
    reload: function () {
        gdrive_files.list();
    },
    list: function () {
        if ($('#tbGDFiles').length) {
            var email = $('select#email').val(),
                md5Email = md5(email);
            $('#tbGDFiles').on('preXhr.dt', function (e, s, data) {
                var p = s._iDisplayStart - s._iDisplayLength;
                data.private = $('#onlyPrivate').is(':checked');
                data.onlyFolder = $('#onlyFolder').is(':checked');
                if (email !== null) {
                    data.email = email;
                    data.folder_id = $_GET('folder_id');
                }
                if (p >= 0) {
                    data.token = localStorage.getItem('nextPageToken-' + md5Email + '-' + p);
                }
            }).DataTable({
                ajax: adminURL + 'ajax/gdrive_files.datatables.ajax.php',
                serverSide: true,
                info: false,
                pagingType: 'simple',
                columns: [{
                    data: 'actions',
                    responsivePriority: 0,
                    className: 'text-center',
                    render: function (value, type, row, meta) {
                        return '<div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="' + value.id + '~' + value.email + '" value="' + value.id + '"><label class="custom-control-label" for="' + value.id + '~' + value.email + '"></label></div>';
                    }
                },
                {
                    data: 'title',
                    responsivePriority: 1,
                    render: function (value, type, row, meta) {
                        if ('type' in row.mimeType && row.mimeType.type.indexOf('.folder') === -1) {
                            return '<div class="title" data-toggle="tooltip" title="' + value + '"><img src="' + row.mimeType.icon + '" class="mr-2">' + value + '</div>';
                        } else {
                            if (row.id !== '') {
                                return '<a href="' + adminURL + 'gdrive_files/?email=' + row.email + '&folder_id=' + row.id + '" class="title" data-toggle="tooltip" title="' + value + '"><img src="' + row.mimeType.icon + '" class="mr-2">' + value + '</a>';
                            } else {
                                return '<a href="' + adminURL + 'gdrive_files/?email=' + row.email + '" class="title" data-toggle="tooltip" title="Back">' + value + '</a>';
                            }
                        }
                    }
                },
                {
                    data: 'desc',
                    responsivePriority: 2,
                    render: function (value, type, row, meta) {
                        return '<div class="title" data-toggle="tooltip" title="' + value + '">' + value + '</div>';
                    }
                },
                {
                    data: 'modifiedDate',
                    className: 'text-right',
                },
                {
                    data: 'shared',
                    className: 'text-center',
                    render: function (value, type, row, meta) {
                        return '<i class="fas fa-' + (value ? "check-circle text-success" : "times-circle text-danger") + '" style="font-size:1.5rem"></i>';
                    }
                },
                {
                    data: 'editable',
                    className: 'text-center',
                    render: function (value, type, row, meta) {
                        return '<i class="fas fa-' + (value ? "check-circle text-success" : "times-circle text-danger") + '" style="font-size:1.5rem"></i>';
                    }
                },
                {
                    data: 'copyable',
                    className: 'text-center',
                    render: function (value, type, row, meta) {
                        return '<i class="fas fa-' + (value ? "check-circle text-success" : "times-circle text-danger") + '" style="font-size:1.5rem"></i>';
                    }
                },
                {
                    data: 'actions',
                    className: 'text-center',
                    responsivePriority: 3,
                    render: function (value, type, row, meta) {
                        if (row.id !== '') {
                            var html = '';
                            if (value.shared) {
                                html += '<button type="button" class="dropdown-item" onclick="gdrive_files.private(\'' + value.id + '\');$(this).prop(\'disabled\',true)" title="Make it Private"><i class="fas fa-eye-slash mr-1"></i> Private</button>';
                            } else {
                                html += '<button type="button" class="dropdown-item" onclick="gdrive_files.public(\'' + value.id + '\');$(this).prop(\'disabled\',true)" title="Share with the Public"><i class="fas fa-eye mr-1"></i> Public</button>';
                            }
                            html += '<button type="button" class="dropdown-item" onclick="gdrive_files.delete(\'' + value.id + '\')"><i class="fas fa-trash mr-1"></i> Delete</button>';
                            if ('type' in row.mimeType && row.mimeType.type.indexOf('.folder') === -1) {
                                html += '<div class="dropdown-divider"></div><a class="dropdown-item" href="' + value.view + '" target="_blank"><i class="fas fa-binoculars mr-1"></i> View</a><a class="dropdown-item" href="' + value.download + '" target="_blank"><i class="fas fa-download mr-1"></i> Download</a><a class="dropdown-item" href="' + value.preview + '" target="_blank"><i class="fas fa-magnifying-glass mr-1"></i> Preview</a><div class="dropdown-divider"></div><a class="dropdown-item" href="' + value.embed + '" target="_blank"><i class="fas fa-link mr-1"></i> Embed Link</a><a class="dropdown-item" href="javascript:void(0)" onclick="copyText(\'' + value.embed_code + '\')"><i class="fas fa-code mr-1"></i> Embed Code</a>';
                            }
                            return '<div class="btn-group"><button type="button" class="btn btn-sm btn-custom dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cog"></i></button><div class="dropdown-menu dropdown-menu-right border-0 shadow">' + html + '</div></div>';
                        } else {
                            return '';
                        }
                    }
                }
                ],
                columnDefs: [{
                    orderable: false,
                    targets: [0, 1, 2, 3, 4, 5, 6, 7]
                },
                {
                    visible: true,
                    targets: [0, 1, 2, 7],
                    className: 'noVis'
                }],
                order: [
                    [3, 'desc']
                ],
                drawCallback: function (s) {
                    if (email !== null && typeof s.json.token !== 'undefined' && s.json.token !== null && s.json.token !== '') {
                        localStorage.setItem('nextPageToken-' + md5Email + '-' + s._iDisplayStart, s.json.token);
                    }
                    $('[data-toggle="tooltip"]').tooltip({
                        container: 'body'
                    });
                }
            });
        }
    },
    removeIndex: function (id) {
        var $tb = $('#tbGDFiles tbody'),
            $row = $tb.find('tr#' + id);
        $row.next('tr.child').remove();
        $row.remove();
        if ($tb.find('tr').length === 0) gdrive_files.list();
    },
    delete: function (id) {
        if (typeof id !== 'undefined') {
            swal({
                title: 'Are you sure?',
                text: 'Deleted data cannot be restored anymore!',
                type: 'warning',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                cancelButtonClass: 'btn-secondary',
                confirmButtonClass: 'btn-danger',
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;
                ajaxPOST(adminURL + 'ajax/gdrive_files.ajax.php', {
                    id: id,
                    email: $('select#email').val(),
                    action: 'delete'
                }, function (res) {
                    if (res.status !== 'fail') {
                        gdrive_files.removeIndex(id);
                        swal('Success!', res.message, 'success');
                    } else {
                        swal('Error!', res.message, 'error');
                    }
                }, function (xhr) {
                    swal('Error!', xhr.responseText, 'error');
                });
            });
        }
    },
    deleteChecked: function () {
        var ids = [];
        $('#tbGDFiles tbody input[type=checkbox]:checked').each(function () {
            ids.push($(this).val());
        });
        if (ids.length) {
            $('#ckAll, #ckAll1').prop('checked', false);
            swal({
                title: 'Are you sure?',
                text: 'Do you want to delete these ' + ids.length + ' files? Deleted files cannot be restored anymore!',
                type: 'warning',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                cancelButtonClass: 'btn-secondary',
                confirmButtonClass: 'btn-danger',
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;
                var deleted = [], failed = [];
                var completed = function () {
                    if ((deleted.length + failed.length) >= ids.length) {
                        var type = failed.length == 0 ? 'success' : 'info';
                        swal({
                            title: 'Delete File',
                            text: deleted.length + ' files have been deleted and ' + failed.length + ' files have failed to delete.',
                            type: type
                        });
                    }
                };
                var deleteFile = function (id) {
                    ajaxPOST(adminURL + 'ajax/gdrive_files.ajax.php', {
                        id: id,
                        email: $('select#email').val(),
                        action: 'delete'
                    }, function (res) {
                        if (res.status !== 'fail') {
                            deleted.push(id);
                            gdrive_files.removeIndex(id);
                        } else {
                            failed.push(id);
                        }
                        completed();
                    }, function (xhr) {
                        failed.push(id);
                        completed();
                    });
                };
                for (var i = 0; i < ids.length; i++) {
                    deleteFile(ids[i]);
                }
            }
            );
        } else {
            swal('Warning!', 'Please select the Google Drive file that you want to delete!', 'warning');
        }
    },
    newFolder: function () {
        swal({
            title: "Create New Folder",
            type: "input",
            showLoaderOnConfirm: true,
            showCancelButton: true,
            closeOnConfirm: false,
            cancelButtonClass: "btn-secondary",
            confirmButtonClass: "btn-success",
            inputPlaceholder: "Folder Name",
            confirmButtonText: "Save"
        }, function (name) {
            if (name === false) return false;
            if (name.trim() === "") {
                swal.showInputError("You need to write something!");
                return false;
            }
            var parent_id = $_GET('folder_id');
            parent_id = parent_id !== null ? parent_id : 'root';
            $.ajax({
                url: adminURL + 'ajax/gdrive_files.ajax.php',
                method: 'POST',
                dataType: 'json',
                data: 'action=new_folder&name=' + name + '&parent_id=' + parent_id + '&email=' + $('select#email').val(),
                success: function (res) {
                    if (res.status !== 'fail') {
                        gdrive_files.reload();
                        swal('Success!', res.message, 'success');
                    } else {
                        swal('Error!', res.message, 'error');
                    }
                },
                error: function (xhr) {
                    swal('Error!', xhr.responseText, 'error');
                }
            });
        });
    }
};

$(document).ready(function () {
    gdrive_files.list();

    $('#ckAllSessions, #ckAllSessions1').change(function () {
        var isChecked = $(this).prop('checked'), $ckItems = $('#tbSessions tbody input.custom-control-input');
        if (isChecked) {
            $ckItems.prop('checked', true);
        } else {
            $ckItems.prop('checked', false);
        }
    });

    if ($('select#email').length) {
        localStorage.removeItem('DataTables_tbGDFiles_/administrator/gdrive_files');

        $('select#email').change(function () {
            var url = location.href.split('?');
            localStorage.removeItem('DataTables_tbGDFiles_/administrator/gdrive_files');
            location.href = url[0] + '?email=' + $(this).val();
        });

        $('#ckAll, #ckAll1').change(function () {
            var isChecked = $(this).prop('checked'), $ckItems = $('#tbGDFiles tbody input.custom-control-input');
            if (isChecked) {
                $ckItems.prop('checked', true);
            } else {
                $ckItems.prop('checked', false);
            }
        });

        $('th[aria-controls=tbGDFiles]').click(function () {
            gdrive_files.reload();
        });

        $('#tbGDFiles_filter input[type=search]').blur(function () {
            gdrive_files.reload();
        });
    }
});
