var gdrive_accounts = {
    reload: function () {
        $('#tbGDAccounts').DataTable().ajax.reload(null, false);
    },
    delete: function (id) {
        if (typeof id !== 'undefined') {
            swal({
                title: 'Are you sure?',
                text: 'Deleted data cannot be restored anymore!',
                type: 'warning',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                cancelButtonClass: 'btn-secondary',
                confirmButtonClass: 'btn-danger',
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;
                ajaxPOST(adminURL + 'ajax/gdrive_accounts.ajax.php', {
                    id: id,
                    action: 'delete'
                }, function (res) {
                    if (res.status === 'ok') {
                        swal('Success!', res.message, 'success');
                        gdrive_accounts.removeIndex(id);
                    } else {
                        swal('Error!', res.message, 'error');
                    }
                }, function (xhr) {
                    swal('Error!', xhr.responseText, 'error');
                });
            });
        }
    },
    removeIndex: function (id) {
        var $tb = $('#tbGDAccounts tbody'),
            $row = $tb.find('tr#' + id);
        $row.next('tr.child').remove();
        $row.remove();
        if ($tb.find('tr').length === 0) gdrive_accounts.list();
    },
    deleteChecked: function () {
        var ids = [];
        $('#tbGDAccounts input[type=checkbox]:checked').each(function () {
            ids.push($(this).val());
        });
        if (ids.length > 0) {
            $('#ckAllGDA, #ckAllGDA1').prop('checked', false);
            swal({
                title: 'Are you sure?',
                text: 'Are you sure you want to delete these ' + ids.length + ' Google Drive accounts? Deleted accounts cannot be restored anymore!',
                type: 'warning',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                cancelButtonClass: 'btn-secondary',
                confirmButtonClass: 'btn-danger',
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;
                var deleted = [], failed = [];
                var completed = function () {
                    if ((deleted.length + failed.length) >= ids.length) {
                        var type = failed.length === 0 ? 'success' : 'info';
                        swal({
                            title: 'Delete Accounts',
                            text: ids_deleted.length + ' accounts have been deleted and ' + ids_failed.length + ' accounts have failed to delete.',
                            type: type,
                            showLoaderOnConfirm: false
                        });
                    }
                };
                var deleteFile = function (id) {
                    ajaxPOST(adminURL + 'ajax/gdrive_accounts.ajax.php', {
                        action: 'delete',
                        id: id
                    }, function (result) {
                        if (result.status === 'ok') {
                            deleted.push(id);
                            gdrive_accounts.removeIndex(id);
                        } else {
                            failed.push(id);
                        }
                        completed();
                    }, function () {
                        failed.push(id);
                        completed();
                    });
                };
                for (var i = 0; i < ids.length; i++) {
                    deleteFile(ids[i]);
                }
            }
            );
        } else {
            swal('Warning!', 'Please select the account you want to delete!', 'warning');
        }
    },
    updateStatus: function (id, status) {
        ajaxPOST(adminURL + 'ajax/gdrive_accounts.ajax.php', {
            id: id,
            status: status,
            action: 'update_status'
        }, function (res) {
            if (res.status !== 'fail') {
                swal('Success!', res.message, 'success');
                gdrive_accounts.reload();
            } else {
                swal('Error!', res.message, 'error');
            }
        }, function (xhr) {
            swal('Error!', xhr.responseText, 'error');
        });
    },
    list: function () {
        if ($('#tbGDAccounts').length) {
            $('#tbGDAccounts').DataTable({
                ajax: adminURL + 'ajax/gdrive_accounts.datatables.ajax.php',
                serverSide: true,
                columns: [{
                    data: 'id',
                    className: 'text-center',
                    render: function (value, type, row, meta) {
                        return '<div class="custom-control custom-checkbox mx-auto"><input type="checkbox" class="custom-control-input" id="gda-' + value + '" value="' + value + '"><label class="custom-control-label" for="gda-' + value + '"></label></div>';
                    }
                },
                {
                    data: 'email',
                    responsivePriority: 0
                },
                {
                    data: 'status',
                    responsivePriority: 1,
                    className: 'text-center',
                    render: function (value, type, row, meta) {
                        var icon = 'fa-exclamation-circle text-warning',
                            title = 'User rate limit exceeded',
                            status = 0;
                        if (value === '1') {
                            icon = 'fa-check-circle text-success';
                            title = 'Enable';
                            status = 0;
                        } else if (value === '0') {
                            icon = 'fa-times-circle text-danger';
                            title = 'Disable';
                            status = 1;
                        }
                        return '<a href="javascript:void(0)" onclick="gdrive_accounts.updateStatus(' + row.id + ',' + status + ')"><i class="fas ' + icon + ' status" title="' + title + '" style="font-size:1.5rem"></i></a>';
                    }
                },
                {
                    data: 'created',
                    className: 'text-center',
                },
                {
                    data: 'modified',
                    className: 'text-center',
                },
                {
                    data: 'id',
                    className: 'text-center',
                    responsivePriority: 2,
                    render: function (value, type, row) {
                        return '<div class="btn-group"><button type="button" class="btn btn-sm btn-custom dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cog"></i></button><div class="dropdown-menu dropdown-menu-right border-0 shadow"><a href="' + adminURL + 'gdrive_accounts/edit/?id=' + value + '" class="dropdown-item"><i class="fas fa-edit"></i> Edit</a><button type="button" class="dropdown-item" onclick="gdrive_accounts.delete(\'' + value + '\')"><i class="fas fa-trash"></i> Delete</button></div></div>';
                    }
                }
                ],
                columnDefs: [{
                    orderable: false,
                    targets: [0, 5]
                },
                {
                    visible: true,
                    targets: [0, 1, 5],
                    className: 'noVis'
                }
                ],
                order: [
                    [1, 'asc']
                ],
                drawCallback: function (settings) {
                    $('i.status').tooltip();
                }
            });
        }
    }
};

$(document).ready(function () {
    gdrive_accounts.list();

    $('#ckAllGDA, #ckAllGDA1').change(function () {
        var isChecked = $(this).prop('checked'),
            $ckItems = $('#tbGDAccounts tbody input.custom-control-input');
        if (isChecked) {
            $ckItems.prop('checked', true);
        } else {
            $ckItems.prop('checked', false);
        }
    });
});
