var sessions = {
    delete: function (id) {
        if (typeof id !== 'undefined') {
            swal({
                title: "Are you sure?",
                text: "Deleted data cannot be restored anymore!",
                type: "warning",
                showLoaderOnConfirm: true,
                showCancelButton: true,
                cancelButtonClass: "btn-secondary",
                confirmButtonClass: "btn-danger",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;
                ajaxPOST(adminURL + "ajax/sessions.ajax.php", {
                    id: id,
                    action: 'delete'
                }, function (res) {
                    if (res.status !== 'fail') {
                        swal("Success!", res.message, "success");
                        setTimeout(function () {
                            sessions.reload();
                        }, 1000);
                    } else {
                        swal("Error!", res.message, "error");
                    }
                }, function (xhr) {
                    swal("Error!", xhr.responseText, "error");
                });
            });
        }
    },
    list: function () {
        if ($('#tbSessions').length) {
            $('#tbSessions').DataTable({
                ajax: adminURL + "ajax/sessions.datatables.ajax.php",
                serverSide: true,
                columns: [{
                    data: 'id',
                    responsivePriority: 0,
                    className: 'text-center',
                    render: function (value, type, row, meta) {
                        return '<div class="custom-control custom-checkbox mx-auto"><input type="checkbox" class="custom-control-input" id="row-' + meta.row + '" value="' + value + '"><label class="custom-control-label" for="row-' + meta.row + '"></label></div>';
                    }
                },
                {
                    data: 'username',
                    responsivePriority: 1
                },
                {
                    data: 'ip',
                    responsivePriority: 2
                },
                {
                    data: 'useragent'
                },
                {
                    data: 'created',
                    className: 'text-center',
                },
                {
                    data: 'expired',
                    className: 'text-center',
                },
                {
                    data: 'id',
                    className: 'text-center',
                    responsivePriority: 3,
                    render: function (value, type, row) {
                        return '<button type="button" class="btn btn-danger btn-sm" onclick="sessions.delete(' + value + ')"><i class="fas fa-trash"></i></button>';
                    }
                }
                ],
                columnDefs: [{
                    orderable: false,
                    targets: [0, 6]
                },
                {
                    visible: true,
                    targets: [0, 1, 2],
                    className: 'noVis'
                }
                ],
                order: [
                    [5, "desc"]
                ]
            });
        }
    },
    reload: function () {
        $('#tbSessions').DataTable().ajax.reload(null, false);
    },
    removeIndex: function (id) {
        var $row = $('#tbSessions').find('tbody').find('tr#' + id);
        $row.next('tr.child').remove();
        $row.remove();
        if ($('#tbSessions').find('tbody').find('tr').length == 0) {
            sessions.reload();
        }
    },
    deleteChecked: function () {
        var ids = [];
        var $ckItem = $('#tbSessions').find('tbody').find('input[type=checkbox]:checked');
        $ckItem.each(function () {
            ids.push($(this).val());
        });
        if (ids.length) {
            $('#ckAllSessions, #ckAllSessions1').prop('checked', false);
            swal({
                title: "Are you sure?",
                text: "Are you sure you want to delete these " + ids.length + " sessions? Deleted sessions cannot be restored anymore!",
                type: "warning",
                showLoaderOnConfirm: true,
                showCancelButton: true,
                cancelButtonClass: "btn-secondary",
                confirmButtonClass: "btn-danger",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;
                setTimeout(function () {
                    var ids_deleted = [];
                    var ids_failed = [];
                    var j = 0;
                    var deleteFile = function (id, i) {
                        $.ajax({
                            url: adminURL + 'ajax/sessions.ajax.php',
                            type: 'POST',
                            data: {
                                action: 'delete',
                                id: id
                            },
                            success: function (result) {
                                if (result.status !== 'fail') {
                                    ids_deleted.push(id);
                                    sessions.removeIndex(id);
                                } else {
                                    ids_failed.push(id);
                                }
                            },
                            error: function () {
                                ids_failed.push(id);
                            },
                            complete: function () {
                                j++;
                                if (j >= ids.length) {
                                    var type = ids_failed.length == 0 ? "success" : "info";
                                    swal({
                                        title: 'Delete File',
                                        text: ids_deleted.length + ' sessions have been deleted and ' + ids_failed.length + ' sessions have failed to delete.',
                                        type: type
                                    });
                                    return;
                                }
                            }
                        });
                    };
                    for (var i = 0; i < ids.length; i++) {
                        deleteFile(ids[i], i);
                    }
                }, 1000);
            }
            );
        } else {
            swal('Warning!', 'Please select the sessions that you want to delete!', 'warning');
        }
    }
};

$(document).ready(function () {
    sessions.list();
});
