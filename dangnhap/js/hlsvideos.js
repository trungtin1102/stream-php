// var $tbHlsVideo = $('#tbHlsVideos').DataTable({
//     paging: false,
//     info: false,
//     searching: false,
//     columns: [{
//         width: '10%'
//     },
//     {
//         width: '25%'
//     },
//     {
//         width: '50%'
//     },
//     {
//         width: '15%'
//     }],
//     columnDefs: [{
//         orderable: false,
//         targets: [0]
//     }],
//     order: [
//         [1, 'asc']
//     ],
//     drawCallback: function () {
//         loadTooltip();
//     }
// });
alert(5);
var hlsvideos = {
    url: adminURL + 'ajax/hlsVideos.ajax.php',
    addAlternativeHTML: function (val) {
        var $wrap = $('#altWrapper');
        var index = $wrap.find('.input-group').length;
        var html = '<div class="form-group" data-index="' + index + '"><div class="input-group"><input type="url" id="altLink-' + index + '" name="altLinks[]" class="form-control" placeholder="Insert alternative video url here" value="' + (typeof val !== 'undefined' ? val : '') + '"><div class="input-group-append"><button type="button" data-toggle="tooltip" title="Remove Alternative Video URL" class="btn btn-danger" onclick="videos.removeAlternativeHTML(' + index + ')"><i class="fas fa-minus"></i></button><a href="javascript:void(0)" data-toggle="tooltip" title="Move" class="btn btn-outline-secondary move"><i class="fas fa-expand-arrows-alt"></i></a></div></div></div>';
        $wrap.find('.form-group').eq(0).after(html);
        loadTooltip();
    },
    removeAlternativeHTML: function (i) {
        var $e = $('#altWrapper .form-group[data-index="' + i + '"]');
        if (i > 0) {
            $e.remove();
        } else {
            $e.find('.input-group-prepend, .input-group-append > .btn-danger').remove();
            $e.find('input').val('');
        }
        $('body > [role="tooltip"]').remove();
    },
    list: function () {
        if ($('#tbHlsVideos').length) {
            $('#tbHlsVideos').DataTable({
                ajax: adminURL + 'ajax/hlsVideos.datatables.ajax.php?status=' + $_GET('status'),
                serverSide: true,
                columns: [{
                    data: 'id',
                    responsivePriority: 0,
                    className: 'text-center',
                    render: function (value, type, row, meta) {
                        return '<div class="custom-control custom-checkbox mx-auto"><input type="checkbox" class="custom-control-input" id="row-' + meta.row + '" value="' + value + '"><label class="custom-control-label" for="row-' + meta.row + '"></label></div>';
                    }
                },
                {
                    data: 'title',
                    responsivePriority: 1,
                    render: function (value, type, row, meta) {
                        return '<div class="title" data-toggle="tooltip" title="' + value + '">' + value + '</div>';
                    }
                },
                {
                    data: 'host',
                    className: 'text-center',
                    render: function (value, type, row, meta) {
                        var links = '', icon, hostName, length = row.links.length;
                        if (length > 0) {
                            var first = row.links[0];
                            $.each(row.links, function (i, v) {
                                if (v.host !== '' && typeof vidHosts[v.host] !== 'undefined') {
                                    hostName = vidHosts[v.host];
                                    if (v.host === 'direct') {
                                        if (v.url.indexOf('.m3u') > -1 || v.url.indexOf('hls') > -1) {
                                            icon = 'm3u';
                                            hostName = 'HLS Link';
                                        } else if (v.url.indexOf('.mpd') > -1 || v.url.indexOf('mpd') > -1 || v.url.indexOf('dash') > -1) {
                                            icon = 'mpd';
                                            hostName = 'MPEG-DASH Link';
                                        } else {
                                            icon = 'direct';
                                        }
                                    } else {
                                        icon = v.host;
                                    }
                                    links += '<a class="dropdown-item" href="' + v.url + '" target="_blank"><img src="' + baseURL + 'assets/img/logo/' + icon + '.png" width="16" height="16" class="mr-2">' + hostName.replace('|Additional Host', '').replace('|New', '') + '</a>';
                                }
                            });
                            if (length > 1) {
                                return '<div class="dropdown"><a class="btn btn-outline-default btn-sm dropdown-toggle" href="#" role="button" id="ddLinks-' + (meta.row + 1) + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="' + baseURL + 'assets/img/logo/' + first.host + '.png" width="16" height="16"></a><div class="dropdown-menu shadow border-0" aria-labelledby="ddLinks-' + (meta.row + 1) + '">' + links + '</div></div>';
                            } else if (first.host !== '') {
                                return '<a href="' + first.url + '" target="_blank" title="' + (typeof vidHosts[first.host] !== 'undefined' ? vidHosts[first.host] : first.host).replace('|Additional Host', '').replace('|New', '') + '" data-toggle="tooltip"><img width="16" height="16" src="' + baseURL + 'assets/img/logo/' + first.host + '.png"></a>';
                            }
                        }
                        return '';
                    }
                },
                {
                    data: 'status',
                    className: 'text-center',
                    responsivePriority: 2,
                    render: function (value, type, row, meta) {
                        var icon = 'check',
                            color = 'success',
                            text = 'Good',
                            html = '';
                        if (value == 2) {
                            icon = 'minus';
                            color = 'warning';
                            text = 'Warning';
                        } else if (value == 1) {
                            icon = 'times';
                            color = 'danger';
                            text = 'Broken';
                        }
                        html = '<i class="fas fa-lg fa-' + icon + '-circle text-' + color + ' status" data-toggle="tooltip" id="status-' + row.id + '" title="' + text + '"></i>';
                        return html;
                    }
                },
                {
                    data: 'subtitles',
                    className: 'text-center',
                    render: function (value, type, row, meta) {
                        if (value.length) {
                            var html = '<div class="dropdown"><a class="btn btn-outline-primary btn-sm dropdown-toggle" href="#" role="button" id="ddSubs-' + (meta.row + 1) + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-lg fa-language"></i></a><div class="dropdown-menu shadow border-0" aria-labelledby="ddSubs-' + (meta.row + 1) + '">';
                            $.each(value, function (i, e) {
                                html += '<a class="dropdown-item" href="' + e.link + '" target="_blank">' + e.language + '</a>';
                            });
                            html += '</div></div>';
                            return html;
                        } else {
                            return '';
                        }
                    }
                },
                {
                    data: 'views',
                    className: 'text-right',
                },
                {
                    data: 'name',
                },
                {
                    data: 'updated',
                    className: 'text-right',
                    responsivePriority: 3
                },
                {
                    data: 'id',
                    className: 'text-center',
                    responsivePriority: 4,
                    render: function (value, type, row) {
                        return '<div class="btn-group"><button type="button" class="btn btn-sm btn-custom dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cog"></i></button><div class="dropdown-menu dropdown-menu-right border-0 shadow"><a class="dropdown-item" href="javascript:void(0)" onclick="copyText(\'' + row.actions.embed_code + '\')"><i class="fas fa-code mr-2"></i>Embed Code</a><a class="dropdown-item" href="' + row.actions.embed + '" target="_blank"><i class="fas fa-link mr-2"></i>Embed Link</a><a class="dropdown-item" href="' + row.actions.download + '" target="_blank"><i class="fas fa-download mr-2"></i>Download Link</a><div class="dropdown-divider"></div><a href="' + adminURL + 'videos/edit/?id=' + value + '" class="dropdown-item" type="button"><i class="fas fa-edit mr-2"></i>Edit</a><button onclick="videos.delete.single(' + value + ')" class="dropdown-item" type="button"><i class="fas fa-trash mr-2"></i>Delete</button><button onclick="videos.cache.clear.single($(this))" data-id="' + value + '" class="dropdown-item" type="button"><i class="fas fa-eraser mr-2"></i>Clear Cache</button></div></div>';
                    }
                }],
                columnDefs: [{
                    orderable: false,
                    targets: [0, 4, 8]
                },
                {
                    visible: true,
                    targets: [0, 8],
                    className: 'noVis'
                }],
                order: [
                    [7, 'desc']
                ],
                drawCallback: function (settings) {
                    loadTooltip();
                }
            });
        }
    },
    reload: function () {
        $('#ckAllVideos, #ckAllVideos1').prop('checked', false);
        $('#tbHlsVideos').DataTable().ajax.reload(null, false);
    },
    cache: {
        clear: {
            ajax: function (url, id, sCallback, eCallback) {
                ajaxPOST(url + 'admin-api/', {
                    id: id,
                    action: 'clear_video_cache',
                    token: Cookies.get('adv_token')
                }, sCallback, eCallback);
            },
            single: function (e) {
                var success = [],
                    failed = [],
                    length = 0,
                    completeCallback = function () {
                        if ((failed.length + success.length) >= length) {
                            swal('Success!', 'The video cache has been cleared.', 'success');
                            e.prop('disabled', false);
                        }
                    },
                    callback = function (url, id) {
                        videos.cache.clear.ajax(url, id, function (res) {
                            if (res.status !== 'fail') success.push(res);
                            else failed.push(res);
                            completeCallback();
                        }, function (xhr) {
                            failed.push(xhr);
                            completeCallback();
                        });
                    };

                e.prop('disabled', true);
                ajaxPOST(adminURL + 'ajax/public.ajax.php', {
                    action: 'get_load_balancer_list'
                }, function (res) {
                    length = res.data.length;
                    $.each(res.data, function (i, url) {
                        callback(url, e.data('id'));
                    });
                }, function (xhr) {
                    callback(baseURL, e.data('id'));
                });
            },
            multi: function (e) {
                var $ckItem = $('#tbHlsVideos tbody input[type=checkbox]:checked');
                if ($ckItem.length > 0) {
                    swal({
                        title: 'Are you sure?',
                        text: 'Are you sure you want to clear the cache of ' + $ckItem.length + ' videos?',
                        type: 'warning',
                        showLoaderOnConfirm: true,
                        showCancelButton: true,
                        cancelButtonClass: 'btn-secondary',
                        confirmButtonClass: 'btn-danger',
                        closeOnConfirm: false
                    }, function (isConfirm) {
                        if (!isConfirm) return;

                        var lbs = localStorage.getItem('lb'),
                            lb = lbs ? JSON.parse(lbs) : [],
                            links = [],
                            success = [],
                            failed = [],
                            type = 'info',
                            callback = function (url, id) {
                                videos.cache.clear.ajax(url, id, function (res) {
                                    if (res.status !== 'fail') success.push(id);
                                    else failed.push(id);
                                    completeCallback();
                                }, function (xhr) {
                                    failed.push(id);
                                    completeCallback();
                                });
                            },
                            completeCallback = function () {
                                var l = lb.length,
                                    s = success.length / l,
                                    f = failed.length / l;
                                if ((s + f) >= $ckItem.length) {
                                    if (f === 0) type = 'success';
                                    swal({
                                        title: 'Clear Cache',
                                        text: s + ' video cache cleared successfully. While the ' + f + ' video cache failed to clear.',
                                        type: type
                                    });
                                    $('#ckAllVideos, #ckAllVideos1').prop('checked', false);
                                    e.prop('disabled', false);
                                }
                            };

                        e.prop('disabled', true);

                        $ckItem.each(function () {
                            var id = $(this).val();
                            $.each(lb, function (i, url) {
                                callback(url, id);
                            });
                        });
                    }
                    );
                } else {
                    swal('Warning!', 'Please select the video to be cleared the cache!', 'warning');
                }
            }
        }
    },
    checker: {
        multi: function (e) {
            var $table = $('#tbHlsVideos tbody'),
                $ckItems = $table.find('input[type=checkbox]:checked'),
                completed = [],
                completeCallback = function (xhr) {
                    completed.push(xhr);
                    if ($ckItems.length === completed.length) {
                        e.prop('disabled', false);
                        videos.reload();
                    }
                },
                callback = function (id, data) {
                    ajaxPOST(videos.url, {
                        action: 'update_status',
                        id: id,
                        data: data,
                    }, function (res) {
                        completeCallback(res);
                    }, function (xhr) {
                        completeCallback(res);
                    });
                };
            if ($ckItems.length > 0) {
                e.prop('disabled', true);
                $ckItems.each(function (i, v) {
                    var id = $(this).val();
                    $table.find('#status-' + id).attr('class', 'fas fa-spin fa-lg fa-sync-alt text-info');
                    ajaxPOST(videos.url, {
                        action: 'get_server',
                        id: id,
                        source: 'db'
                    }, function (res) {
                        if (res.status !== 'fail') {
                            var serverLink = res.data;
                            ajaxPOST(videos.url, {
                                id: id,
                                action: 'db_query'
                            }, function (res) {
                                if (res.status !== 'fail') {
                                    var qry = res.data;
                                    ajaxGET(serverLink + 'api/?' + qry, function (res) {
                                        if (res.status !== 'fail') {
                                            callback(id, {
                                                sources: res.sources
                                            });
                                        } else {
                                            callback(id, {});
                                        }
                                    }, function (xhr) {
                                        completeCallback(xhr);
                                    });
                                }
                            }, function (xhr) {
                                completeCallback(xhr);
                            });
                        } else {
                            completeCallback(res);
                        }
                    }, function (xhr) {
                        completeCallback(xhr);
                    });
                });
            } else {
                swal('Warning!', 'Please select the video you want to check!', 'warning');
            }
        }
    },
    removePoster: function (id, $e) {
        if (typeof id !== 'undefined' && id !== '') {
            $e.prop('disabled', true);
            ajaxPOST(videos.url, {
                action: 'remove_poster',
                id: id
            }, function (res) {
                if (res.status !== 'fail') {
                    $e.closest('.form-group').remove();
                    showToast(res.message, 'success');
                } else {
                    showToast(res.message, 'error');
                }
                $e.prop('disabled', false);
            }, function (xhr) {
                showToast('Poster failed to delete. Insert a new poster if you want to replace it!', 'error');
            });
        }
    },
    delete: {
        ajax: function (id, sCallback, eCallback) {
            ajaxPOST(videos.url, {
                id: id,
                action: 'delete'
            }, sCallback, eCallback);
        },
        single: function (id) {
            if (typeof id !== 'undefined' && id !== '') {
                swal({
                    title: 'Are you sure?',
                    text: 'Deleted data cannot be restored anymore!',
                    type: 'warning',
                    showCancelButton: true,
                    showLoaderOnConfirm: true,
                    closeOnConfirm: false,
                    cancelButtonClass: "btn-secondary",
                    confirmButtonClass: "btn-danger"
                }, function (isConfirm) {
                    if (!isConfirm) return;
                    videos.delete.ajax(id, function (res) {
                        if (res.status !== 'fail') {
                            swal('Success!', res.message, 'success');
                            videos.reload();
                        } else {
                            swal('Error!', res.message, 'error');
                        }
                    }, function (xhr) {
                        swal('Error!', xhr.responseText, 'error');
                    });
                });
            }
        },
        multi: function (e) {
            var $ckItem = $('#tbHlsVideos tbody input[type=checkbox]:checked');
            if ($ckItem.length > 0) {
                swal({
                    title: 'Are you sure?',
                    text: 'Are you sure you want to delete these ' + $ckItem.length + ' videos? Deleted videos cannot be restored anymore!',
                    type: 'warning',
                    showCancelButton: true,
                    showLoaderOnConfirm: true,
                    closeOnConfirm: false,
                    cancelButtonClass: "btn-secondary",
                    confirmButtonClass: "btn-danger"
                }, function (isConfirm) {
                    if (!isConfirm) return;
                    var type = 'info',
                        success = [],
                        failed = [],
                        completeCallback = function () {
                            if ((success.length + failed.length) >= $ckItem.length) {
                                if (failed.length === 0) type = 'success';
                                videos.reload();
                                $('#btnDelete, #btnChecker, #btnClear').addClass('d-none');
                                $('#ckAllVideos, #ckAllVideos1').prop('checked', false);
                                e.prop('disabled', false);
                                swal({
                                    title: 'Delete Files',
                                    text: success.length + ' files have been deleted and ' + failed.length + ' files have failed to delete.',
                                    type: type
                                });
                            }
                        };

                    e.prop('disabled', true);
                    $ckItem.each(function () {
                        var id = $(this).val();
                        videos.delete.ajax(id, function (res) {
                            if (res.status !== 'fail') success.push(id);
                            else failed.push(id);
                            completeCallback();
                        }, function (xhr) {
                            failed.push(id);
                            completeCallback();
                        });
                    });
                }
                );
            } else {
                swal('Warning!', 'Please select the video you want to delete!', 'warning');
            }
        }
    },
    subtitles: {
        delete: function (id) {
            ajaxPOST(adminURL + 'ajax/subtitles.ajax.php', {
                action: 'delete_video_subtitle',
                id: id
            }, function (res) {
                if (res.status !== 'fail') {
                    $('[data-sub="' + id + '"]').remove();
                    showToast(res.message, "success");
                } else {
                    showToast(res.message, "error");
                }
            }, function (xhr) {
                showToast(xhr.responseText, "error");
            });
        },
        add: function () {
            var $cs = $('#subsWrapper'),
                $ig = $cs.find('.input-group'),
                html = '<div class="form-group" data-index="' + $ig.length + '"><div class="input-group"><div class="input-group-prepend">' + subtitleHTML + '<input type="hidden" id="sub-type-' + $ig.length + '" name="sub-type[]" value="url"></div><input type="url" name="sub-url[]" class="form-control subtitle" placeholder="Subtitle URL"><div class="input-group-append"><button type="button" class="btn btn-outline-primary" data-toggle="tooltip" aria-label="Upload Subtitle" onclick="videos.subtitles.upload($(this))" data-index="' + $ig.length + '" title="Upload Subtitle"><i class="fas fa-upload"></i></button><button type="button" class="btn btn-outline-danger" data-toggle="tooltip" title="Remove Subtitle" data-index="' + $ig.length + '" aria-label="Remove Subtitle" onclick="videos.subtitles.remove($(this))"><i class="fas fa-minus"></i></button></div></div></div>';

            loadTooltip();
            $cs.append(html);
        },
        remove: function (e) {
            $('#subsWrapper .form-group[data-index="' + e.data('index') + '"]').remove();
        },
        upload: function (e) {
            var $ig = e.closest('.input-group');
            $ig.find('#sub-type-' + e.data('index')).val('file');
            $ig.find('input.subtitle').replaceWith('<div class="custom-file"><input type="file" id="sub-' + e.data('index') + '" name="sub-file[]" class="custom-file-input subtitle" accept=".srt, .vtt, .ass, .sub, .stl, .dfxp, .ttml, .sbv, .txt"><label class="custom-file-label" for="sub-' + e.data('index') + '">Choose file</label></div>');
            e.replaceWith('<button type="button" class="btn btn-outline-primary" data-toggle="tooltip" title="Insert Subtitle URL" aria-label="Upload Subtitle" onclick="videos.subtitles.insertURL($(this))" data-index="' + e.data('index') + '"><i class="fas fa-link"></i></button>');
            loadTooltip();
            bsCustomFileInput.init();
        },
        insertURL: function (e) {
            var $ig = e.closest('.input-group');
            $ig.find('#sub-type-' + e.data('index')).val('url');
            $ig.find('.custom-file').replaceWith('<input type="url" name="sub-url[]" class="form-control subtitle" placeholder="Subtitle URL">');
            e.replaceWith('<button type="button" class="btn btn-outline-primary" data-toggle="tooltip" title="Upload Subtitle" aria-label="Upload Subtitle" onclick="videos.subtitles.upload($(this))" data-index="' + e.data('index') + '"><i class="fas fa-upload"></i></button>');
            loadTooltip();
        }
    },
    scrollToHosts: function () {
        $('#hostIDFormat').collapse('show');
        $('html,body').animate({
            scrollTop: $('#hostIDFormat').offset().top
        }, 'slow');
    },
    bulk: {
        get_embed: function () {
            var $ckItems = $('#tbHlsVideos tbody input[type=checkbox]:checked'), txt = '', completed = [];
            if ($ckItems.length > 0) {
                $ckItems.each(function (i, e) {
                    txt += $(this).data('title') + "\t" + $(this).data('embed');
                    txt += "\n\n";
                    completed.push(e);
                    if (completed.length >= $ckItems.length) {
                        copyText(txt.trim(), 'Embed URLs');
                    }
                });
            } else {
                showToast('Select the file you want to copy the embed url!', "error");
            }
        },
        save: function ($btn) {
            var $frm = $('#frmBulkVideo'),
                $btnGet = $('#btnGetEmbedURLs'),
                $link = $frm.find('#links'),
                links = $link.val().trim().split('\n'),
                completed = [],
                completeCallback = function (xhr) {
                    completed.push(xhr);
                    if (links.length === completed.length) {
                        $link.val('');
                        $btn.find('i').attr('class', 'fas fa-save');
                        $btn.prop('disabled', false);
                        $link.prop('disabled', false);
                        videos.reload();
                        $btnGet.prop('disabled', false);
                    }
                };

            if (links.length > 0) {
                $btnGet.prop('disabled', true);
                $tbHlsVideo.clear().draw();

                $btn.find('i').attr('class', 'fas fa-spin fa-sync-alt');
                $btn.prop('disabled', true);
                $link.prop('disabled', true);
                $.each(links, function (i, v) {
                    ajaxPOST(videos.url, {
                        action: 'get_server',
                        id: v
                    }, function (res) {
                        if (res.status !== 'fail') {
                            var serverLink = res.data;
                            ajaxPOST(videos.url, {
                                id: v,
                                useTitleAsSlug: $('#useTitleAsSlug').val(),
                                action: 'db_query'
                            }, function (res) {
                                if (res.status !== 'fail') {
                                    ajaxGET(serverLink + 'api/?' + res.data, function (res) {
                                        if (res.status !== 'fail') {
                                            $tbHlsVideo.row.add([
                                                '<div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="bulk-' + i + '" value="' + i + '" data-title="' + res.title + '" data-embed="' + res.embed_link + '"><label class="custom-control-label" for="bulk-' + i + '"></label></div>',
                                                res.title,
                                                '<strong>Source URL:</strong> <a href="' + links[i] + '" target="_blank">' + links[i] + '</a><br><strong>Embed URL: </strong><a href="' + res.embed_link + '" target="_blank">' + res.embed_link + '</a>',
                                                '<div class="text-center"><i class="fas fa-lg fa-check-circle text-success" data-toggle="tooltip" title="Success"></i><span class="d-none">Success</span></div>'
                                            ]).draw();
                                        } else {
                                            $tbHlsVideo.row.add([
                                                '<div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="bulk-' + i + '" value="' + i + '" data-title="" data-embed=""><label class="custom-control-label" for="bulk-' + i + '"></label></div>',
                                                '',
                                                '<strong>Source URL:</strong> <a href="' + links[i] + '" target="_blank">' + links[i] + '</a><br><strong>Embed URL: </strong>',
                                                '<div class="text-center"><i class="fas fa-lg fa-times-circle text-danger" data-toggle="tooltip" title="Failed"></i><span class="d-none">Failed</span></div>'
                                            ]).draw();
                                        }
                                        completeCallback(res);
                                    }, function (xhr) {
                                        $tbHlsVideo.row.add([
                                            '<div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="bulk-' + i + '" value="' + i + '" data-title="" data-embed=""><label class="custom-control-label" for="bulk-' + i + '"></label></div>',
                                            '',
                                            '<strong>Source URL:</strong> <a href="' + links[i] + '" target="_blank">' + links[i] + '</a><br><strong>Embed URL: </strong>',
                                            '<div class="text-center"><i class="fas fa-lg fa-times-circle text-danger" data-toggle="tooltip" title="Failed"></i><span class="d-none">Failed</span></div>'
                                        ]).draw();
                                        completeCallback(xhr);
                                    });
                                }
                            }, function (xhr) {
                                completeCallback(res);
                            });
                        } else {
                            completeCallback(res);
                        }
                    }, function (xhr) {
                        completeCallback(xhr);
                    });
                });
            } else {
                swal('Warning!', 'Video URL is required!', 'warning');
            }
        }
    },
};

$(document).ready(function () {
    console.log('ahiahi');
    hlsvideos.list();

    $('#frmBulkVideo #links').blur(function () {
        if ($(this).val() !== '') {
            $('#btnBulkSave').prop('disabled', false);
        } else {
            $('#btnBulkSave').prop('disabled', true);
        }
    });

    $('#mdVideosBulkLink').detach().appendTo('body');
    $('#mdVideosBulkLink').on('hidden.bs.modal', function () {
        var $frm = $('#frmBulkVideo');
        $frm.find('#links').val('');
        $frm.find('#useTitleAsSlug').prop('checked', false);
        $tbHlsVideo.clear().draw();
    });

    $('#ckAllBulkVideos, #ckAllBulkVideos1').change(function () {
        var $ckItem = $('#tbHlsVideos tbody input.custom-control-input');
        if ($(this).prop('checked')) {
            $ckItem.prop('checked', true);
        } else {
            $ckItem.prop('checked', false);
        }
    });

    $('#subtitles').sortable();
    $('#altWrapper').sortable({
        handle: '.move'
    });

    $('.btn-upload-poster').click(function () {
        $('#posterUpload').toggleClass('d-none');
        $('#posterURL').toggleClass('d-none');
    });

    $('#ckAllVideos, #ckAllVideos1').change(function () {
        var $ckItem = $('#tbHlsVideos tbody input.custom-control-input');
        if ($(this).prop('checked')) {
            $ckItem.prop('checked', true);
        } else {
            $ckItem.prop('checked', false);
        }
    });

    if (localStorage.getItem('hostIDFormat') !== null) {
        $('#hostIDFormat').collapse('show');
    }

    $('#hostIDFormat').on('shown.bs.collapse', function () {
        localStorage.setItem('hostIDFormat', true);
    }).on('hidden.bs.collapse', function () {
        localStorage.removeItem('hostIDFormat', true);
    });

    $toolbarVideos = $('#btnDelete, #btnChecker, #btnClear');
    $('#ckAllVideos, #ckAllVideos1').change(function () {
        var isChecked = [], $ckItem = $('#tbHlsVideos tbody input[type="checkbox"]');
        if ($(this).prop('checked')) $ckItem.prop('checked', true);
        else $ckItem.prop('checked', false);
        isChecked = $('#tbHlsVideos tbody input[type="checkbox"]:checked');
        if (isChecked.length > 0) {
            $('#toolbar .btn').removeClass('d-none');
        } else {
            $toolbarVideos.addClass('d-none');
        }
    });

    $(document).on('click', '#tbHlsVideos tbody input[type="checkbox"]', function () {
        var isChecked = $('#tbHlsVideos tbody input[type="checkbox"]:checked');
        if (isChecked.length > 0) {
            $('#toolbar .btn').removeClass('d-none');
        } else {
            $toolbarVideos.addClass('d-none');
        }
    });
});
