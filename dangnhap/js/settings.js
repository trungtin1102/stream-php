var settings = {
    deleteVideosWithWords: function () {
        swal({
            title: 'Delete Videos!',
            text: 'Are you sure you want to remove blacklisted videos?',
            type: "warning",
            showLoaderOnConfirm: true,
            showCancelButton: true,
            cancelButtonClass: "btn-secondary",
            confirmButtonClass: "btn-danger",
            closeOnConfirm: false
        }, function (isConfirm) {
            if (!isConfirm) return;
            var $cache = $('#deleteVideosBlacklisted'),
                oText = $cache.html();
            $cache.html(oText + ' <i class="fas fa-spin fa-sync-alt"></i>').prop('disabled', true);
            ajaxGET(adminURL + 'ajax/settings.ajax.php?action=delete_videos_blacklisted', function (res) {
                if (res.status !== 'fail') {
                    $('#word_blacklisted').val(res.data);
                    swal('Success!', res.message, 'success');
                } else {
                    swal('Error!', res.message, 'error');
                }
                $cache.html(oText).prop('disabled', false);
            }, function (xhr) {
                swal('Error!', res.responseText, 'error');
                $cache.html(oText).prop('disabled', false);
            });
        });
    },
    smtp: function () {
        var pv = $('#smtp_provider').val();
        var $host = $('#smtp_host'),
            $port = $('#smtp_port'),
            $tls = $('#smtp_tls');
        var provider = {
            gmail: {
                host: "smtp.gmail.com",
                port: 465,
                tls: false
            },
            ymail: {
                host: "smtp.mail.yahoo.com",
                port: 587,
                tls: true
            },
            outlook: {
                host: "smtp.office365.com",
                port: 587,
                tls: true
            }
        };
        var selected = {};
        if (pv !== 'other') {
            selected = provider[pv];
            $host.val(selected.host);
            $port.val(selected.port);
            $tls.prop('checked', true);
        } else {
            $host.val('');
            $port.val('');
            $tls.prop('checked', false);
        }
    },
    removeVastHTML: function (index) {
        $('#vastWrapper .form-group[data-index=' + index + ']').remove();
    },
    addVastHTML: function () {
        var $wrap = $('#vastWrapper');
        var index = $wrap.find('.form-group').length;
        $wrap.append('<div class="form-group" data-index="' + index + '"><div class="input-group"><div class="input-group-prepend" style="max-width:110px"><input type="text" placeholder="Ad Position" name="opt[vast_offset][]" id="vast_offset-' + index + '" class="form-control"></div><input type="url" name="opt[vast_xml][]" id="vast_xml-' + index + '" placeholder="VAST URL (.xml)" class="form-control"><div class="input-group-append"><button type="button" class="btn btn-danger" onclick="settings.removeVastHTML(' + index + ')" data-toggle="tooltip" title="Remove VAST"><i class="fas fa-minus-circle"></i></button></div></div></div>');
        loadTooltip();
    },
    checkProxy: function () {
        var $btn = $('#checkProxy'),
            oText = $btn.html(),
            hosts = ['proxy_docker_com', 'free_proxy_cz', 'free_proxy_list_net'],
            success = [],
            failed = [];

        var completed = function () {
            if ((success.length + failed.length) >= hosts.length) {
                $btn.html(oText).prop('disabled', false);
                swal('Success!', 'Proxy list has been updated!', 'success');
            }
        };
        var proxyChecker = function (v) {
            ajaxGET(baseURL + 'cron-proxy/?host=' + v, function (data) {
                if (data.status !== 'fail') {
                    success.push(v);
                    $('#proxy_list').val(data.result);
                } else {
                    failed.push(v);
                }
                completed();
            }, function (xhr) {
                failed.push(v);
                completed();
            });
        };

        $btn.html(oText + ' <i class="fas fa-spin fa-sync-alt"></i>').prop('disabled', true);
        if ($('#proxy_list').val() !== '') {
            ajaxGET(baseURL + 'cron-proxy/', function (data) {
                if (data.status !== 'fail') {
                    swal('Success!', data.message, 'success');
                } else {
                    swal('Error!', data.message, 'error');
                }
                $btn.html(oText).prop('disabled', false);
            }, function (xhr) {
                swal('Error!', xhr.responseText, 'error');
                $btn.html(oText).prop('disabled', false);
            });
        } else {
            $.each(hosts, function (i, v) {
                proxyChecker(v);
            });
        }
    },
    clearCache: function () {
        swal({
            title: 'Clear Cache!',
            text: 'Are you sure you want to clear the cache?',
            type: "warning",
            showLoaderOnConfirm: true,
            showCancelButton: true,
            cancelButtonClass: "btn-secondary",
            confirmButtonClass: "btn-danger",
            closeOnConfirm: false
        },
            function (isConfirm) {
                if (!isConfirm) return;

                var $cache = $('#clearCache'),
                    oText = $cache.html(),
                    success = [],
                    failed = [],
                    lbs = localStorage.getItem('lb'),
                    lb = lbs ? JSON.parse(lbs) : [];

                var callback = function (xhr) {
                    if (typeof xhr.responseJSON !== 'undefined') {
                        failed.push(xhr);
                    } else {
                        if (xhr.status !== 'fail') success.push(xhr);
                        else failed.push(xhr);
                    }
                    if ((success.length + failed.length) >= lb.length) {
                        if (success.length >= lb.length) {
                            swal('Success!', 'Cache cleared successfully!', 'success');
                        } else {
                            swal('Error!', 'Failed to clear cache or cache does not exist.', 'error');
                        }
                        $cache.html(oText).prop('disabled', false);
                    }
                };

                $cache.html(oText + ' <i class="fas fa-spin fa-sync-alt"></i>').prop('disabled', true);
                if (lb.length > 0) {
                    $.each(lb, function (i, v) {
                        ajaxPOST(v + 'admin-api/', {
                            action: 'clear_cache',
                            token: Cookies.get('adv_token')
                        }, callback, callback);
                    });
                }
            });
    },
    clearVideoCache: function () {
        swal({
            title: 'Clear Video Cache!',
            text: 'Are you sure you want to clear the video cache?',
            type: "warning",
            showLoaderOnConfirm: true,
            showCancelButton: true,
            cancelButtonClass: "btn-secondary",
            confirmButtonClass: "btn-danger",
            closeOnConfirm: false
        },
            function (isConfirm) {
                if (!isConfirm) return;

                var $cache = $('#clearCache'),
                    oText = $cache.html(),
                    success = [],
                    failed = [],
                    lbs = localStorage.getItem('lb'),
                    lb = lbs ? JSON.parse(lbs) : [];

                var callback = function (xhr) {
                    if (typeof xhr.responseJSON !== 'undefined') {
                        failed.push(xhr);
                    } else {
                        if (xhr.status !== 'fail') success.push(xhr);
                        else failed.push(xhr);
                    }
                    if ((success.length + failed.length) >= lb.length) {
                        if (success.length >= lb.length) {
                            swal('Success!', 'Cache cleared successfully!', 'success');
                        } else {
                            swal('Error!', 'Failed to clear cache or cache does not exist.', 'error');
                        }
                        $cache.html(oText).prop('disabled', false);
                    }
                };

                $cache.html(oText + ' <i class="fas fa-spin fa-sync-alt"></i>').prop('disabled', true);
                if (lb.length > 0) {
                    $.each(lb, function (i, v) {
                        ajaxPOST(v + 'admin-api/', {
                            action: 'clear_all_video_cache',
                            token: Cookies.get('adv_token')
                        }, callback, callback);
                    });
                }
            });
    },
    clearVideoHLSMPDCache: function () {
        swal({
            title: 'Clear Video Cache!',
            text: 'Are you sure you want to clear the video cache?',
            type: "warning",
            showLoaderOnConfirm: true,
            showCancelButton: true,
            cancelButtonClass: "btn-secondary",
            confirmButtonClass: "btn-danger",
            closeOnConfirm: false
        },
            function (isConfirm) {
                if (!isConfirm) return;

                var $cache = $('#clearCache'),
                    oText = $cache.html(),
                    success = [],
                    failed = [],
                    lbs = localStorage.getItem('lb'),
                    lb = lbs ? JSON.parse(lbs) : [];

                var callback = function (xhr) {
                    if (typeof xhr.responseJSON !== 'undefined') {
                        failed.push(xhr);
                    } else {
                        if (xhr.status !== 'fail') success.push(xhr);
                        else failed.push(xhr);
                    }
                    if ((success.length + failed.length) >= lb.length) {
                        if (success.length >= lb.length) {
                            swal('Success!', 'Cache cleared successfully!', 'success');
                        } else {
                            swal('Error!', 'Failed to clear cache or cache does not exist.', 'error');
                        }
                        $cache.html(oText).prop('disabled', false);
                    }
                };

                $cache.html(oText + ' <i class="fas fa-spin fa-sync-alt"></i>').prop('disabled', true);
                if (lb.length > 0) {
                    $.each(lb, function (i, v) {
                        ajaxPOST(v + 'admin-api/', {
                            action: 'clear_hlsmpd_video_cache',
                            token: Cookies.get('adv_token')
                        }, callback, callback);
                    });
                }
            });
    },
    clearSettingsCache: function () {
        swal({
            title: 'Clear Settings Cache!',
            text: 'Are you sure you want to clear the javascript cache?',
            type: "warning",
            showLoaderOnConfirm: true,
            showCancelButton: true,
            cancelButtonClass: "btn-secondary",
            confirmButtonClass: "btn-danger",
            closeOnConfirm: false
        },
            function (isConfirm) {
                if (!isConfirm) return;

                var $cache = $('#clearCache'),
                    oText = $cache.html(),
                    success = [],
                    failed = [],
                    lbs = localStorage.getItem('lb'),
                    lb = lbs ? JSON.parse(lbs) : [];

                var callback = function (xhr) {
                    if (typeof xhr.responseJSON !== 'undefined') {
                        failed.push(xhr);
                    } else {
                        if (xhr.status !== 'fail') success.push(xhr);
                        else failed.push(xhr);
                    }
                    if ((success.length + failed.length) >= lb.length) {
                        if (success.length >= lb.length) {
                            swal('Success!', 'Cache cleared successfully!', 'success');
                        } else {
                            swal('Error!', 'Failed to clear cache or cache does not exist.', 'error');
                        }
                        $cache.html(oText).prop('disabled', false);
                    }
                };

                $cache.html(oText + ' <i class="fas fa-spin fa-sync-alt"></i>').prop('disabled', true);
                if (lb.length > 0) {
                    $.each(lb, function (i, v) {
                        ajaxPOST(v + 'admin-api/', {
                            action: 'clear_settings_cache',
                            token: Cookies.get('adv_token')
                        }, callback, callback);
                    });
                }
            });
    },
    resetHost: function () {
        var oText = $('#resetHost').html();
        swal({
            title: 'Reset Hosts!',
            text: 'Return the hosts to their original position. Please clear the embed cache.',
            type: "warning",
            showLoaderOnConfirm: true,
            showCancelButton: true,
            cancelButtonClass: "btn-secondary",
            confirmButtonClass: "btn-warning",
            closeOnConfirm: false
        },
            function (isConfirm) {
                if (!isConfirm) return;
                $('#resetHost').html(oText + ' <i class="fas fa-spin fa-sync-alt"></i>').prop('disabled', true);
                ajaxGET(adminURL + 'ajax/settings.ajax.php?action=reset_host', function (data) {
                    $('#resetHost').html(oText).prop('disabled', false);
                    if (data.status !== 'fail') {
                        $('#bypass_host').find('option').removeAttr('selected');
                        $('#bypass_host').multiSelect('select', data.result);
                        $('#bypass_host').multiSelect('refresh');
                        swal('Success!', data.message, 'success');
                    } else {
                        swal('Error!', data.message, 'error');
                    }
                }, function (xhr) {
                    swal('Error!', xhr.responseText, 'error');
                });
            });
    },
};

$(document).ready(function () {
    if ($('#frmSettings').length) {
        var ckSettings = [];
        $('#frmSettings input, #frmSettings select, #frmSettings textarea').each(function () {
            ckSettings.push($(this).attr('id'));
            Cookies.set($('#frmSettings').data('cookie'), JSON.stringify(ckSettings));
        });
    }

    var admType = Cookies.get('adm-type'),
        admMsg = Cookies.get('adm-message');
    if (typeof admMsg !== 'undefined') {
        var msg = admMsg.split("\n");
        $.each(msg, function (i, v) {
            showToast(admMsg.trim(), admType);
        });
        setTimeout(function () {
            Cookies.remove('adm-type');
            Cookies.remove('adm-message');
        }, 1000);
    }

    $('#cache_instance').change(function () {
        ajaxGET(adminURL + 'ajax/settings.ajax.php?action=extension_checker&extension=' + $(this).val(), function (res) {
            if (res.status !== 'ok') {
                swal("Error!", res.message, "error");
                $('#cache_instance').find('option[value="files"]').prop("selected", true);
            }
        }, function (xhr) {
            swal("Error!", xhr.responseText, "error");
            $('#cache_instance').find('option[value="files"]').prop("selected", true);
        });
    });

    $('#modalCustomVAST').detach().appendTo('body');

    $('#frmCreateCustomVast').submit(function (e) {
        e.preventDefault();
        ajaxPOST(adminURL + 'ajax/settings.ajax.php', $(this).serialize(), function (res) {
            if (res.status !== 'fail') {
                $('#txtCustomVastResult').val(res.result);
                swal('Success!', res.message, 'success');
            } else {
                swal('Error!', res.message, 'error');
            }
            $('#customVastResult').toggleClass('d-none');
        }, function (xhr) {
            swal('Error!', 'Server error! Custom VASTs cannot be created.', 'error');
        });
    });
});
