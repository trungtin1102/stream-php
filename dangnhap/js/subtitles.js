var subtitles = {
    loadHosts: function () {
        ajaxPOST(adminURL + 'ajax/subtitles.ajax.php', {
            action: 'get_hosts'
        }, function (res) {
            if (res.status !== 'fail') {
                var html = '';
                $('#subHosts').html('');
                for (var i = 0; i < res.data.length; i++) {
                    html += '<option value="' + res.data[i] + '">' + res.data[i] + '</option>';
                }
                $('#subHosts').html(html);
            }
        }, function (xhr) {
            swal("Error!", xhr.responseText, "error");
        });
    },
    migrate: function () {
        subtitles.loadHosts();
        $('#modalHostSub').modal('show');
    },
    migrateNow: function () {
        var $frm = $('#frmMigrateSub'),
            $btn = $frm.find('[type="submit"]'),
            btnText = $btn.html();

        $btn.html('<span class="spinner-border spinner-border-sm" role="status"></span>Loading...').prop('disabled', true);
        ajaxPOST(adminURL + 'ajax/subtitles.ajax.php', $('#frmMigrateSub').serializeArray(), function (res) {
            if (res.status !== 'ok') {
                swal("Error!", res.message, "error");
            } else {
                subtitles.loadHosts();
                swal("Success!", res.message, "success");
            }
            $frm[0].reset();
            $btn.html(btnText).prop('disabled', false);
        }, function (xhr) {
            $btn.html(btnText).prop('disabled', false);
            swal("Error!", xhr.responseText, "error");
        });
    },
    delete: {
        ajax: function (id, action, sCallback, eCallback) {
            ajaxPOST(adminURL + 'ajax/subtitles.ajax.php', {
                action: action,
                id: id
            }, sCallback, eCallback);
        },
        single: function (id) {
            if (typeof id !== 'undefined' && id !== '') {
                swal({
                    title: "Are you sure?",
                    text: "Deleted data cannot be restored anymore!",
                    type: "warning",
                    showLoaderOnConfirm: true,
                    showCancelButton: true,
                    cancelButtonClass: "btn-secondary",
                    confirmButtonClass: "btn-danger",
                    closeOnConfirm: false
                }, function (isConfirm) {
                    if (!isConfirm) return;
                    subtitles.delete.ajax(id, 'delete', function (res) {
                        if (res.status !== 'fail') {
                            swal("Success!", res.message, "success");
                            subtitles.reload();
                        } else {
                            if (res.message.indexOf('any response') > -1) {
                                swal({
                                    title: "Are you sure?",
                                    text: "The subtitle file does not exist on this website. Do you still want to delete it?",
                                    type: "warning",
                                    showLoaderOnConfirm: true,
                                    showCancelButton: true,
                                    cancelButtonClass: "btn-secondary",
                                    confirmButtonClass: "btn-danger",
                                    closeOnConfirm: false
                                }, function (isConfirm) {
                                    if (!isConfirm) return;
                                    subtitles.delete.ajax(id, 'delete_db_only', function (res) {
                                        if (res.status !== 'fail') {
                                            swal("Success!", res.message, "success");
                                            subtitles.reload();
                                        } else {
                                            swal("Error!", res.message, "error");
                                        }
                                    }, function (xhr) {
                                        swal("Error!", xhr.responseText, "error");
                                    });
                                });
                            } else {
                                swal("Error!", res.message, "error");
                            }
                        }
                    }, function (xhr) {
                        swal("Error!", xhr.responseText, "error");
                    });
                });
            } else {
                swal('Warning!', 'Please select the subtitle you want to delete!', 'warning');
            }
        },
        multi: function (e) {
            var $ckItem = $('#tbSubtitles').find('tbody').find('input[type=checkbox]:checked');
            if ($ckItem.length) {
                $('#ckAllSubtitles, #ckAllSubtitles1').prop('checked', false);
                swal({
                    title: "Are you sure?",
                    text: "Are you sure you want to delete these " + $ckItem.length + " subtitles? Deleted subtitles cannot be restored anymore!",
                    type: "warning",
                    showLoaderOnConfirm: true,
                    showCancelButton: true,
                    cancelButtonClass: "btn-secondary",
                    confirmButtonClass: "btn-danger",
                    closeOnConfirm: false
                }, function (isConfirm) {
                    if (!isConfirm) return;
                    var type = 'success',
                        success = [],
                        failed = [],
                        completeCallback = function () {
                            if ((success.length + failed.length) >= $ckItem.length) {
                                if (failed.length === 0) type = "success";
                                swal({
                                    title: 'Delete Subtitles',
                                    text: success.length + ' subtitles have been deleted and ' + failed.length + ' subtitles have failed to delete.',
                                    type: type
                                });
                                $('#ckAllSubtitles, #ckAllSubtitles1').prop('checked', false);
                                e.prop('disabled', false);
                                subtitles.reload();
                            }
                        };
                    $ckItem.each(function () {
                        var id = $(this).val();
                        subtitles.delete.ajax(id, 'delete', function (res) {
                            if (res.status !== 'fail') {
                                success.push(id);
                                completeCallback();
                            } else {
                                subtitles.delete.ajax(id, 'delete_db_only', function (res) {
                                    if (res.status !== 'fail') success.push(id);
                                    else failed.push(id);
                                    completeCallback();
                                }, function (xhr) {
                                    failed.push(id);
                                    completeCallback();
                                });
                            }
                        }, function (xhr) {
                            failed.push(id);
                            completeCallback();
                        });
                    });
                }
                );
            } else {
                swal('Warning!', 'Please select the subtitle you want to delete!', 'warning');
            }
        },
    },
    list: function () {
        if ($('#tbSubtitles').length) {
            $('#tbSubtitles').DataTable({
                ajax: adminURL + "ajax/subtitles.datatables.ajax.php",
                serverSide: true,
                columns: [{
                    data: 'DT_RowId',
                    responsivePriority: 0,
                    className: 'text-center',
                    render: function (value, type, row, meta) {
                        return '<div class="custom-control custom-checkbox mx-auto"><input type="checkbox" class="custom-control-input" id="row-' + meta.row + '" value="' + value + '"><label class="custom-control-label" for="row-' + meta.row + '"></label></div>';
                    }
                }, {
                    data: 'file_name',
                    responsivePriority: 1,
                },
                {
                    data: 'language',
                    className: 'text-center',
                },
                {
                    data: 'name',
                },
                {
                    data: 'host',
                    render: function (value, type, row) {
                        if (value == null) {
                            return '';
                        }
                        return '<a href="' + value + '" target="_blank">' + value + '</a>';
                    }
                },
                {
                    data: 'added',
                    className: 'text-right',
                },
                {
                    data: 'id',
                    className: 'text-center',
                    responsivePriority: 2,
                    render: function (value, type, row) {
                        return '<div class="btn-group"><button type="button" class="btn btn-sm btn-custom dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cog"></i></button><div class="dropdown-menu dropdown-menu-right border-0 shadow"><a href="' + value.link + '" class="dropdown-item" title="Download"><i class="fas fa-download"></i> Download</a><button type="button" class="dropdown-item" onclick="subtitles.delete.single(' + value.id + ')" title="Delete"><i class="fas fa-trash"></i> Delete</button></div></div>';
                    }
                }
                ],
                columnDefs: [{
                    orderable: false,
                    targets: [0, 6]
                },
                {
                    visible: true,
                    targets: [0, 1, 6],
                    className: 'noVis'
                }
                ],
                order: [
                    [5, "desc"]
                ]
            });
        }
    },
    reload: function () {
        $('#tbSubtitles').DataTable().ajax.reload(null, false);
    }
};

$(document).ready(function () {
    subtitles.list();

    $('#ckAllSubtitles, #ckAllSubtitles1').change(function () {
        var isChecked = $(this).prop('checked');
        var $ckItem = $('#tbSubtitles').find('tbody').find('input.custom-control-input');
        if (isChecked) {
            $ckItem.prop('checked', true);
        } else {
            $ckItem.prop('checked', false);
        }
    });

    $('#modalHostSub').detach().appendTo('body');

    $('#frmUploadSub #uploadSubFile').change(function (e) {
        if ($(this).val() !== '') {
            $('#frmUploadSub #btnUploadSub').prop('disabled', false);
        }
    });

    $("#frmUploadSub").on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (e) {
                    if (e.lengthComputable) {
                        var percent = (e.loaded / e.total) * 100;
                        $("#frmUploadSub .progress-bar").attr('style', 'width:' + percent + '%');
                        $("#frmUploadSub .progress-bar").attr('aria-valuenow', percent);
                    }
                }, false);
                return xhr;
            },
            type: 'POST',
            url: 'upload.php',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $(".progress-bar").width('0%');
                $('#uploadStatus').html('<img src="images/loading.gif"/>');
            },
            error: function () {
                $('#uploadStatus').html('<p style="color:#EA4335;">File upload failed, please try again.</p>');
            },
            success: function (resp) {
                if (resp == 'ok') {
                    $('#uploadForm')[0].reset();
                    $('#uploadStatus').html('<p style="color:#28A74B;">File has uploaded successfully!</p>');
                } else if (resp == 'err') {
                    $('#uploadStatus').html('<p style="color:#EA4335;">Please select a valid file to upload.</p>');
                }
            }
        });
    });
});
