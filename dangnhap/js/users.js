var users = {
    delete: function (id) {
        if (typeof id !== 'undefined' && id !== '') {
            swal({
                title: "Are you sure?",
                text: "Deleted data cannot be restored anymore!",
                type: "warning",
                showLoaderOnConfirm: true,
                showCancelButton: true,
                cancelButtonClass: "btn-secondary",
                confirmButtonClass: "btn-danger",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;
                ajaxPOST(adminURL + "ajax/users.ajax.php", {
                    id: id,
                    action: "delete"
                }, function (res) {
                    if (res.status !== 'fail') {
                        swal("Success!", res.message, "success");
                        setTimeout(function () {
                            $('#tbUsers').DataTable().ajax.reload(null, false);
                        }, 1000);
                    } else {
                        swal("Error!", res.message, "error");
                    }
                }, function (xhr) {
                    swal("Error!", xhr.responseText, "error");
                });
            });
        }
    },
    list: function () {
        if ($('#tbUsers').length) {
            $('#tbUsers').DataTable({
                ajax: adminURL + "ajax/users.datatables.ajax.php",
                serverSide: true,
                columns: [{
                    data: 'name',
                    responsivePriority: 0,
                },
                {
                    data: 'user',
                },
                {
                    data: 'email',
                },
                {
                    data: 'status',
                    className: 'text-center',
                    render: function (value, type, row) {
                        if (value === 1) {
                            return '<i class="fas fa-check-circle text-success fa-lg status" title="Active"></i>';
                        } else if (value === 2) {
                            return '<i class="fas fa-question-circle text-info fa-lg status" title="Need Approval"></i>';
                        } else {
                            return '<i class="fas fa-times-circle text-danger fa-lg status" title="Inactive"></i>';
                        }
                    }
                },
                {
                    data: 'added',
                    className: 'text-right'
                },
                {
                    data: 'updated',
                    className: 'text-right'
                },
                {
                    data: 'role',
                },
                {
                    data: 'videos',
                    className: 'text-right',
                },
                {
                    data: 'id',
                    className: 'text-center',
                    responsivePriority: 1,
                    render: function (value, type, row) {
                        return '<div class="btn-group"><button type="button" class="btn btn-sm btn-custom dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cog"></i></button><div class="dropdown-menu dropdown-menu-right border-0 shadow"><a href="' + adminURL + 'users/edit/?id=' + value + '" class="dropdown-item"><i class="fas fa-user-edit"></i> Edit</a><button type="button" class="dropdown-item" onclick="users.delete(' + value + ')"><i class="fas fa-trash"></i> Delete</button></div></div>';
                    }
                }
                ],
                columnDefs: [{
                    orderable: false,
                    targets: [7, 8]
                },
                {
                    visible: true,
                    targets: [0, 8],
                    className: 'noVis'
                }
                ],
                order: [
                    [4, "desc"]
                ],
                drawCallback: function (settings) {
                    $('i.status').tooltip();
                }
            });
        }
    },
    changeEmail: function (email) {
        if (typeof email !== 'undefined' && email !== '') {
            ajaxPOST(adminURL + 'ajax/profile.ajax.php', {
                action: 'editEmail',
                email: email
            }, function (res) {
                var title = res.status !== 'fail' ? 'Success' : 'Error';
                swal(title + '!', res.message, title.toLowerCase());
            }, function (xhr) {
                swal('Error!', xhr.responseText, 'error');
            });
        }
    },
    changeUsername: function (user) {
        if (typeof user !== 'undefined' && user !== '') {
            ajaxPOST(adminURL + 'ajax/profile.ajax.php', {
                action: 'editUsername',
                user: user
            }, function (res) {
                var title = res.status !== 'fail' ? 'Success' : 'Error';
                swal(title + '!', res.message, title.toLowerCase());
                if (res.status !== 'fail') {
                    setTimeout(function () {
                        location.href = adminURL + 'login/';
                    }, 3000);
                }
            }, function (xhr) {
                swal('Error!', xhr.responseText, 'error');
            });
        }
    },
};

$(document).ready(function () {
    users.list();
});
