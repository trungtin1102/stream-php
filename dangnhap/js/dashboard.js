var dashboard = {
    popularVideos: {
        list: function () {
            if ($('#tbPopularVideos').length) {
                $('#tbPopularVideos').DataTable({
                    ajax: adminURL + 'ajax/videos.datatables.ajax.php?popular=true&length=7',
                    serverSide: true,
                    columns: [{
                        data: 'title',
                        responsivePriority: 0,
                        render: function (value, type, row, meta) {
                            if (value === '') value = '(No Title)';
                            return '<div class="title"><a href="' + row.actions.embed + '" target="_blank" data-toggle="tooltip" title="' + value + '">' + value + '</a></div>';
                        }
                    },
                    {
                        data: 'host',
                        className: 'text-center',
                        render: function (value, type, row, meta) {
                            var links = '', length = row.links.length;
                            if (length > 0) {
                                var first = row.links[0];
                                $.each(row.links, function (i, v) {
                                    if (typeof vidHosts[v.host] !== 'undefined') {
                                        links += '<a class="dropdown-item" href="' + v.url + '" target="_blank"><img src="' + baseURL + 'assets/img/logo/' + v.host + '.png" width="16" height="16" class="mr-2">' + vidHosts[v.host].replace('|Additional Host', '').replace('|New', '') + '</a>';
                                    }
                                });
                                if (length > 1) {
                                    return '<div class="dropdown"><a class="btn btn-outline-default btn-sm dropdown-toggle" href="#" role="button" id="ddLinks-' + (meta.row + 1) + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="' + baseURL + 'assets/img/logo/' + first.host + '.png" width="16" height="16"></a><div class="dropdown-menu shadow border-0" aria-labelledby="ddLinks-' + (meta.row + 1) + '">' + links + '</div></div>';
                                } else {
                                    return '<a href="' + first.url + '" target="_blank" title="' + (typeof vidHosts[first.host] !== 'undefined' ? vidHosts[first.host] : first.host).replace('|Additional Host', '').replace('|New', '') + '" data-toggle="tooltip"><img src="' + baseURL + 'assets/img/logo/' + first.host + '.png"></a>';
                                }
                            }
                        }
                    },
                    {
                        data: 'views',
                        className: 'text-right'
                    },
                    {
                        data: 'name',
                    },
                    {
                        data: 'added',
                        className: 'text-right'
                    },
                    {
                        data: 'id',
                        className: 'text-center',
                        render: function (value, type, row) {
                            return '<div class="btn-group"><button type="button" class="btn btn-sm btn-custom dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cog"></i></button><div class="dropdown-menu dropdown-menu-right border-0 shadow"><a class="dropdown-item" href="javascript:void(0)" onclick="copyText(\'' + row.actions.embed_code + '\')"><i class="fas fa-code mr-2"></i>Embed Code</a><a class="dropdown-item" href="' + row.actions.embed + '" target="_blank"><i class="fas fa-link mr-2"></i>Embed Link</a><a class="dropdown-item" href="' + row.actions.download + '" target="_blank"><i class="fas fa-download mr-2"></i>Download Link</a><div class="dropdown-divider"></div><a href="' + adminURL + 'videos/edit/?id=' + value + '" target="_blank" class="dropdown-item" type="button"><i class="fas fa-edit"></i> Edit</a><button onclick="videos.cache.clear.single($(this))" data-id="' + value + '" class="dropdown-item" type="button"><i class="fas fa-eraser"></i> Clear Cache</button></div></div>';
                        }
                    }],
                    searching: false,
                    bLengthChange: false,
                    info: false,
                    paging: false,
                    columnDefs: [{
                        orderable: false,
                        targets: [0, 1, 2, 3, 4, 5]
                    },
                    {
                        visible: true,
                        targets: [0, 1],
                        className: 'noVis'
                    }],
                    order: [
                        [2, 'desc']
                    ],
                    drawCallback: function (settings) {
                        $('[data-toggle="tooltip"]').tooltip({
                            container: 'body'
                        });
                    }
                });
            }
        }
    },
    recentVideos: {
        list: function () {
            if ($('#tbRecentVideos').length) {
                $('#tbRecentVideos').DataTable({
                    ajax: adminURL + 'ajax/videos.datatables.ajax.php?recent=true&length=7',
                    serverSide: true,
                    columns: [{
                        data: 'title',
                        responsivePriority: 0,
                        render: function (value, type, row, meta) {
                            if (value === '') value = '(No Title)';
                            return '<div class="title"><a href="' + row.actions.embed + '" target="_blank" data-toggle="tooltip" title="' + value + '">' + value + '</a></div>';
                        }
                    },
                    {
                        data: 'host',
                        className: 'text-center',
                        render: function (value, type, row, meta) {
                            var links = '', length = row.links.length;
                            if (length > 0) {
                                var first = row.links[0];
                                $.each(row.links, function (i, v) {
                                    if (typeof vidHosts[v.host] !== 'undefined') {
                                        links += '<a class="dropdown-item" href="' + v.url + '" target="_blank"><img src="' + baseURL + 'assets/img/logo/' + v.host + '.png" width="16" height="16" class="mr-2">' + vidHosts[v.host].replace('|Additional Host', '').replace('|New', '') + '</a>';
                                    }
                                });
                                if (length > 1) {
                                    return '<div class="dropdown"><a class="btn btn-outline-default btn-sm dropdown-toggle" href="#" role="button" id="ddLinks-' + (meta.row + 1) + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="' + baseURL + 'assets/img/logo/' + first.host + '.png" width="16" height="16"></a><div class="dropdown-menu shadow border-0" aria-labelledby="ddLinks-' + (meta.row + 1) + '">' + links + '</div></div>';
                                } else {
                                    return '<a href="' + first.url + '" target="_blank" title="' + (typeof vidHosts[first.host] !== 'undefined' ? vidHosts[first.host] : first.host).replace('|Additional Host', '').replace('|New', '') + '" data-toggle="tooltip"><img src="' + baseURL + 'assets/img/logo/' + first.host + '.png"></a>';
                                }
                            }
                        }
                    },
                    {
                        data: 'views',
                        className: 'text-right'
                    },
                    {
                        data: 'name',
                    },
                    {
                        data: 'added',
                        className: 'text-right'
                    },
                    {
                        data: 'id',
                        className: 'text-center',
                        render: function (value, type, row) {
                            return '<div class="btn-group"><button type="button" class="btn btn-sm btn-custom dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cog"></i></button><div class="dropdown-menu dropdown-menu-right border-0 shadow"><a class="dropdown-item" href="javascript:void(0)" onclick="copyText(\'' + row.actions.embed_code + '\')"><i class="fas fa-code mr-2"></i>Embed Code</a><a class="dropdown-item" href="' + row.actions.embed + '" target="_blank"><i class="fas fa-link mr-2"></i>Embed Link</a><a class="dropdown-item" href="' + row.actions.download + '" target="_blank"><i class="fas fa-download mr-2"></i>Download Link</a><div class="dropdown-divider"></div><a href="' + adminURL + 'videos/edit/?id=' + value + '" target="_blank" class="dropdown-item" type="button"><i class="fas fa-edit"></i> Edit</a><button onclick="videos.cache.clear.single($(this))" data-id="' + value + '" class="dropdown-item" type="button"><i class="fas fa-eraser"></i> Clear Cache</button></div></div>';
                        }
                    }],
                    searching: false,
                    bLengthChange: false,
                    info: false,
                    paging: false,
                    columnDefs: [{
                        orderable: false,
                        targets: [0, 1, 2, 3, 4, 5]
                    },
                    {
                        visible: true,
                        targets: [0, 1],
                        className: 'noVis'
                    }],
                    order: [
                        [4, 'desc']
                    ],
                    drawCallback: function (settings) {
                        $('[data-toggle="tooltip"]').tooltip({
                            container: 'body'
                        });
                    }
                });
            }
        }
    },
    chart: {
        videoStatus: function (el) {
            var chart,
                $chart = document.querySelector(el + ' > .chart'),
                $load = $(el + ' > i.fa-spin'),
                options = {
                    chart: {
                        id: 'videoStatus',
                        type: 'donut'
                    },
                    legend: {
                        position: 'bottom'
                    },
                    series: [],
                    labels: ["Good", "Broken", "Warning"],
                    colors: ['#28a745', '#dc3545', '#feb019']
                };
            if ($chart !== null) {
                $load.show();
                $chart.style.display = 'none';
                ajaxPOST(adminURL + 'ajax/dashboard.ajax.php', {
                    action: 'videos_status'
                }, function (res) {
                    $load.hide();
                    $chart.style.display = 'block';
                    if (res.status !== 'fail') {
                        options.series[0] = res.data.good;
                        options.series[1] = res.data.broken;
                        options.series[2] = res.data.warning;
                    }
                    chart = new ApexCharts($chart, options);
                    chart.render();
                }, function (xhr) {
                    $load.hide();
                    $chart.style.display = 'block';
                    chart = new ApexCharts($chart, options);
                    chart.render();
                });
            } else {
                $load.hide();
            }
        },
        serverStatus: function (el) {
            var chart,
                $chart = document.querySelector(el + ' > .chart'),
                $load = $(el + ' > i.fa-spin'),
                options = {
                    chart: {
                        id: 'serverStatus',
                        type: 'pie'
                    },
                    legend: {
                        position: 'bottom'
                    },
                    series: [],
                    labels: [],
                };
            if ($chart !== null) {
                $load.show();
                $chart.style.display = 'none';
                ajaxPOST(adminURL + 'ajax/dashboard.ajax.php', {
                    action: 'servers_status'
                }, function (res) {
                    $load.hide();
                    $chart.style.display = 'block';
                    if (res.status !== 'fail') {
                        options.labels = Object.keys(res.data);
                        options.series = Object.values(res.data);
                    }
                    chart = new ApexCharts($chart, options);
                    chart.render();
                }, function (xhr) {
                    $load.hide();
                    $chart.style.display = 'block';
                    chart = new ApexCharts($chart, options);
                    chart.render();
                });
            } else {
                $load.hide();
            }
        },
        views: function (filter, chart) {
            var $load = $('#views > i.fas');
            $load.show();
            ajaxPOST(adminURL + 'ajax/dashboard.ajax.php', {
                action: 'views',
                filter: filter
            }, function (res) {
                $load.hide();
                if (res.status !== 'fail') {
                    var options = {
                        series: [{
                            name: 'Visitors',
                            data: []
                        }],
                        dataLabels: {
                            enabled: false
                        }
                    };
                    if (res.data.length < 30) {
                        options.dataLabels.enabled = true;
                    }
                    options.series = [{
                        name: 'Visitors',
                        data: res.data
                    }];
                    chart.updateOptions(options);
                }
            }, function (xhr) {
                $load.hide();
            });
        }
    },
    supportChecker: function () {
        if (location.href.indexOf('/dashboard') > -1) {
            var $md = $('#modalExtApps'),
                changeStatus = function (id, status, app) {
                    if (status) {
                        app = app ? 'Installed' : 'Enabled';
                        $(id).html('<i class="fas fa-check-circle text-success"></i> ' + app + '</span>');
                    } else {
                        app = app ? 'Uninstalled' : 'Disabled';
                        $(id).html('<i class="fas fa-times-circle text-danger"></i> ' + app + '</span>');
                    }
                };
            $md.find('.btn-success').html('<i class="fas fa-spin fa-refresh mr-2"></i>Re-check').prop('disable', true);
            ajaxGET(adminURL + 'ajax/settings.ajax.php?action=get_dependencies', function (res) {
                if (res.status === 'fail' && typeof res.result !== 'undefined') {
                    changeStatus('#extExecChecker', res.result.exec);
                    changeStatus('#extPopenChecker', res.result.popen);
                    changeStatus('#extShell_ExecChecker', res.result.shell_exec);
                    changeStatus('#extProc_OpenChecker', res.result.proc_open);
                    changeStatus('#extProc_CloseChecker', res.result.proc_close);
                    changeStatus('#extProc_Get_StatusChecker', res.result.proc_get_status);
                    changeStatus('#appChromeChecker', res.result.chrome, true);
                    $md.modal('show');
                }
                $md.find('.btn-success').text('Re-check').prop('disable', false);
            }, function (xhr) {
                $md.find('.btn-success').text('Re-check').prop('disable', false);
            });
        }
    }
};

$(document).ready(function () {
    $('#modalExtApps').detach().appendTo('body');
    dashboard.popularVideos.list();
    dashboard.recentVideos.list();
    dashboard.chart.videoStatus('.video-status');
    dashboard.chart.serverStatus('.server-status');
    dashboard.supportChecker();

    var $chart = document.querySelector("#views > .chart");
    if ($chart) {
        var statisticOptions = {
            series: [{
                name: 'Visitors',
                data: []
            }],
            chart: {
                id: 'chartViews',
                type: 'area',
                height: 300,
                toolbar: {
                    show: false
                }
            },
            dataLabels: {
                enabled: false
            },
            markers: {
                size: 0,
                style: 'hollow',
            },
            xaxis: {
                type: 'datetime',
                tickAmount: 6,
                labels: {
                    datetimeUTC: false,
                }
            },
            tooltip: {
                x: {
                    format: 'dd MMM yyyy'
                }
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shadeIntensity: 1,
                    opacityFrom: 0.7,
                    opacityTo: 0.9,
                    stops: [0, 100]
                }
            },
        };
        var chart = new ApexCharts($chart, statisticOptions);
        chart.render();
        dashboard.chart.views('seven_days', chart);
        $('#views input[name="options"]').click(function () {
            dashboard.chart.views($(this).val(), chart);
        });
    }
});
