var load_balancers = {
    reload: function () {
        $('#tbLoadBalancers').DataTable().ajax.reload(null, false);
    },
    delete: function (id) {
        if (typeof id !== 'undefined' && id !== '') {
            swal({
                title: 'Are you sure?',
                text: 'Deleted data cannot be restored anymore!',
                type: 'warning',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                cancelButtonClass: 'btn-secondary',
                confirmButtonClass: 'btn-danger',
                closeOnConfirm: false
            }, function (isConfirm) {
                if (!isConfirm) return;
                ajaxPOST(adminURL + 'ajax/load_balancers.ajax.php', {
                    id: id,
                    action: 'delete'
                }, function (res) {
                    if (res.status !== 'fail') {
                        swal('Success!', res.message, 'success');
                        load_balancers.reload();
                    } else {
                        swal('Error!', res.message, 'error');
                    }
                }, function (xhr) {
                    swal('Error!', xhr.responseText, 'error');
                });
            });
        }
    },
    updateStatus: function (id, status) {
        ajaxPOST(adminURL + 'ajax/load_balancers.ajax.php', {
            id: id,
            status: status,
            action: 'update_status'
        }, function (res) {
            if (res.status !== 'fail') {
                swal('Success!', res.message, 'success');
                load_balancers.reload();
            } else {
                swal('Error!', res.message, 'error');
            }
        }, function (xhr) {
            swal('Error!', xhr.responseText, 'error');
        });
    },
    list: function () {
        if ($('#tbLoadBalancers').length) {
            $('#tbLoadBalancers').DataTable({
                ajax: adminURL + 'ajax/load_balancers.datatables.ajax.php',
                serverSide: true,
                columns: [{
                    data: 'name',
                    responsivePriority: 0
                },
                {
                    data: 'link',
                    responsivePriority: 1,
                    render: function (value, type, row, meta) {
                        return '<a href="' + value + '" target="_blank">' + value + '</a>';
                    }
                },
                {
                    data: 'playbacks',
                    className: 'text-right'
                },
                {
                    data: 'status',
                    className: 'text-center',
                    render: function (value, type, row, meta) {
                        return '<a href="javascript:void(0)" onclick="load_balancers.updateStatus(' + row.id + ', ' + (row.status == 1 ? 0 : 1) + ')"><i class="fas fa-' + (value == 1 ? 'check-circle text-success' : 'times-circle text-danger') + ' status" title="' + (value == 1 ? 'Enable' : 'Disable') + '" style="font-size:1.5rem"></i></a>';
                    }
                },
                {
                    data: 'public',
                    className: 'text-center',
                    render: function (value, type, row, meta) {
                        return '<i class="fas fa-' + (value == 1 ? 'check-circle text-success' : 'times-circle text-danger') + ' status" title="' + (value == 1 ? 'Enable' : 'Disable') + '" style="font-size:1.5rem"></i>';
                    }
                },
                {
                    data: 'added',
                    className: 'text-right',
                },
                {
                    data: 'updated',
                    className: 'text-right',
                },
                {
                    data: 'id',
                    className: 'text-center',
                    responsivePriority: 2,
                    render: function (value, type, row) {
                        return '<div class="btn-group"><button type="button" class="btn btn-sm btn-custom dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cog"></i></button><div class="dropdown-menu dropdown-menu-right border-0 shadow"><a href="' + adminURL + row.slug + '/edit/?id=' + value + '" class="dropdown-item"><i class="fas fa-edit"></i> Edit</a><button type="button" class="dropdown-item" onclick="load_balancers.delete(' + value + ')"><i class="fas fa-trash"></i> Delete</button><button type="button" class="dropdown-item" onclick="load_balancers.clear(\'' + row.link + 'admin-api/\', ' + value + ')"><i class="fas fa-refresh"></i> Clear Cache</button></div></div>';
                    }
                }
                ],
                columnDefs: [{
                    orderable: false,
                    targets: [2, 7]
                },
                {
                    visible: true,
                    targets: [3, 4, 5, 6],
                    className: 'noVis'
                }
                ],
                order: [
                    [5, 'desc']
                ],
                drawCallback: function (settings) {
                    $('i.status').tooltip();
                }
            });
        }
    },
    clear: function (url, id) {
        ajaxPOST(url, {
            id: id,
            action: 'clear_load_balancer',
            token: Cookies.get('adv_token')
        }, function (res) {
            if (res.status !== 'fail') {
                swal('Success!', res.message, 'success');
            } else {
                swal('Error!', res.message, 'error');
            }
        }, function (xhr) {
            swal('Error!', xhr.responseText, 'error');
        });
    }
};

$(document).ready(function () {
    load_balancers.list();
});
