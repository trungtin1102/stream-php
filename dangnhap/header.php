<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

$themeColor = get_option('pwa_themecolor');
$rgbColor = hex2RGB($themeColor, true, ',');
$rgbColor = $rgbColor ? $rgbColor : '103, 58, 183';
$themeColor = !empty($themeColor) ? '#' . $themeColor : '#673ab7';
$customColor = get_option('custom_color');
$customColor = !empty($customColor) ? '#' . $customColor : '#673ab7';
$customColor2 = get_option('custom_color2');
$customColor2 = !empty($customColor2) ? '#' . $customColor2 : '#3f51b5';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php if ($isSSL) : ?>
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <?php endif; ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex">

    <meta name="msapplication-TileColor" content="<?php echo $themeColor; ?>">
    <meta name="theme-color" content="<?php echo $themeColor; ?>">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link rel="manifest" href="<?php echo $baseURL; ?>manifest.webmanifest">
    <link rel="mask-icon" href="<?php echo $baseURL; ?>assets/img/maskable_icon.png" color="#ffffff">

    <link rel="shortcut icon" href="<?php echo $baseURL; ?>assets/img/logo.ico" type="image/ico">
    <link rel="apple-touch-icon" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-152x152-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-120x120-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-precomposed.png">

    <title><?php echo $title . ' - ' . $sitename; ?></title>

    <link rel="preconnect" href="//t.dtscdn.com">
    <link rel="preconnect" href="//e.dtscout.com">
    <link rel="preconnect" href="//s4.histats.com">
    <link rel="preconnect" href="//s10.histats.com">
    <link rel="preconnect" href="//tags.bluekai.com">
    <link rel="preconnect" href="//www.gstatic.com">
    <link rel="preconnect" href="//fonts.gstatic.com">
    <link rel="preconnect" href="//www.googleapis.com">
    <link rel="preconnect" href="//www.googletagmanager.com">
    <link rel="preconnect" href="//www.google.com">
    <link rel="preconnect" href="//www.google-analytics.com">
    <link rel="preconnect" href="//static.cloudflareinsights.com">
    <link rel="preconnect" href="//googleusercontent.com">
    <link rel="preconnect" href="//lh3.googleusercontent.com">
    <link rel="preconnect" href="//drive-thirdparty.googleusercontent.com">
    <link rel="preconnect" href="//static.addtoany.com">
    <link rel="preconnect" href="//i0.wp.com">

    <link rel="preload" href="<?php echo $baseURL; ?>assets/vendor/bootstrap/css/bootstrap.min.css" as="style">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/vendor/fontawesome6/css/all.min.css" as="style">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/css/sweetalert.css" as="style">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/css/multi-select.dist.css" as="style">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/vendor/select2/css/select2.min.css" as="style">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/css/select2-bootstrap4.min.css" as="style">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/vendor/jquery-wheelcolorpicker/css/wheelcolorpicker.css" as="style">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/vendor/toastify-js/toastify.min.css" as="style">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/vendor/apexcharts/apexcharts.css" as="style">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/css/style.css" as="style">

    <link rel="preload" href="<?php echo $baseURL; ?>assets/js/jquery.min.js" as="script">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/js/polyfill.js" as="script">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/js/pwacompat.min.js" as="script">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/js/js.cookie.min.js" as="script">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/js/sweetalert.min.js" as="script">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/js/jquery-ui.min.js" as="script">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js" as="script">


    <link rel="stylesheet" type="text/css" href="<?php echo $baseURL; ?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseURL; ?>assets/vendor/fontawesome6/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseURL; ?>assets/css/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseURL; ?>assets/css/multi-select.dist.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseURL; ?>assets/vendor/datatables/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseURL; ?>assets/vendor/select2/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseURL; ?>assets/css/select2-bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseURL; ?>assets/vendor/jquery-wheelcolorpicker/css/wheelcolorpicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseURL; ?>assets/vendor/toastify-js/toastify.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseURL; ?>assets/vendor/apexcharts/apexcharts.css">

    <?php
    $class = new \Plugins();
    $adminCSS = $class->getAdminCSS(true);
    if ($adminCSS) {
        echo $adminCSS;
    }
    ?>

    <link rel="stylesheet" type="text/css" href="<?php echo $baseURL; ?>assets/css/style.css">

    <style>
        .btn-outline-custom {
            color: <?php echo $customColor; ?>;
            border-color: <?php echo $customColor; ?>;
        }

        .bg-custom,
        .btn-custom,
        .nav-pills .nav-link.active,
        .nav-pills .show>.nav-link {
            color: #fff;
            border-color: <?php echo $customColor; ?>;
            background-color: <?php echo $customColor; ?>;
            background-image: -webkit-linear-gradient(to right, <?php echo $customColor; ?>, <?php echo $customColor2; ?>);
            background-image: -moz-linear-gradient(to right, <?php echo $customColor; ?>, <?php echo $customColor2; ?>);
            background-image: -o-linear-gradient(to right, <?php echo $customColor; ?>, <?php echo $customColor2; ?>);
            background-image: linear-gradient(to right, <?php echo $customColor; ?>, <?php echo $customColor2; ?>)
        }

        .btn-outline-custom.active,
        .btn-outline-custom:active,
        .btn-outline-custom.focus,
        .btn-outline-custom:focus,
        .btn-outline-custom:not(:disabled):hover,
        .btn-custom.focus,
        .btn-custom:focus,
        .btn-custom:not(:disabled):hover {
            color: #fff !important;
            border-color: <?php echo $customColor2; ?>;
            background-image: -webkit-linear-gradient(to right, <?php echo $customColor2; ?>, <?php echo $customColor; ?>);
            background-image: -moz-linear-gradient(to right, <?php echo $customColor2; ?>, <?php echo $customColor; ?>);
            background-image: -o-linear-gradient(to right, <?php echo $customColor2; ?>, <?php echo $customColor; ?>);
            background-image: linear-gradient(to right, <?php echo $customColor2; ?>, <?php echo $customColor; ?>);
            background-color: <?php echo $customColor2; ?>;
        }

        .btn-outline-custom.focus,
        .btn-outline-custom:focus,
        .btn-outline-custom:not(:disabled):hover,
        .btn-custom.focus,
        .btn-custom:focus {
            box-shadow: 0 0 0 .2rem rgba(<?php echo $rgbColor; ?>, .2) !important
        }

        .table-hover tbody tr:hover td {
            background-color: rgba(<?php echo $rgbColor; ?>, .07) !important;
        }
    </style>

    <script src="<?php echo $baseURL; ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript">
        var localStorage = typeof window.localStorage !== "undefined" ? window.localStorage : window.sessionStorage,
            isIE = function() {
                var ua = window.navigator.userAgent,
                    msie = ua.indexOf('MSIE '),
                    trident = ua.indexOf('Trident/'),
                    rv = ua.indexOf('rv:');
                if (msie > 0) return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
                else if (trident > 0 && rv > -1) return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
                return false
            };
        if (isIE()) document.write('<script src="<?php echo strtr($baseURL, ['/' => '\/']); ?>assets\/js\/polyfill.js"><\/script>');

        var $ = jQuery.noConflict(),
            uid = <?php echo $userLogin ? $userLogin['id'] : 0; ?>,
            email = '<?php echo $userLogin ? $userLogin['email'] : ''; ?>',
            baseURL = '<?php echo $baseURL; ?>',
            adminURL = '<?php echo $adminURL; ?>/';

        function loadScript(url) {
            var e = document.createElement("script");
            e.src = url;
            e.type = "text/javascript";
            document.getElementsByTagName("head")[0].appendChild(e);
        }
    </script>
    <?php include BASE_DIR . 'includes/ga.php'; ?>
    <?php include BASE_DIR . 'includes/gtm_head.php'; ?>
</head>

<body class="bg-light mb-5">
    <?php include BASE_DIR . 'includes/gtm_body.php'; ?>
    <a class="sr-only sr-only-focusable" href="#main">Skip to main content</a>
    <div class="container-lg bg-white rounded-bottom shadow">
        <header id="header">
            <nav class="navbar container-lg navbar-expand-lg navbar-dark fixed-top bg-custom shadow">
                <a class="navbar-brand" href="<?php echo $adminURL; ?>">Control Panel</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse navbar-nav-scroll" id="navbarCollapse" style="max-height:calc(100vh - 50px)">
                    <?php if ($userLogin) : ?>
                        <ul class="navbar-nav">
                            <li class="nav-item <?php echo strpos($_SERVER['REQUEST_URI'], '/dashboard') !== FALSE ? 'active' : ''; ?>">
                                <a class="nav-link" href="<?php echo $adminURL; ?>/dashboard">
                                    <i class="fas fa-tachometer-alt mr-2"></i>Dashboard
                                </a>
                            </li>
                            <li class="nav-item dropdown <?php echo strpos($_SERVER['REQUEST_URI'], 'videos') !== FALSE || strpos($_SERVER['REQUEST_URI'], 'subtitles') !== FALSE ? 'active' : ''; ?>">
                                <a class="nav-link dropdown-toggle" href="#" id="ndVideos" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-film mr-2"></i>Video
                                </a>
                                <div class="dropdown-menu shadow border-0" aria-labelledby="ndVideos">
                                    <a class="dropdown-item" href="<?php echo $adminURL; ?>/videos/new">
                                        <i class="fas fa-plus-circle mr-2"></i>Add New Video
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="<?php echo $adminURL; ?>/videos">
                                        <i class="fas fa-film mr-2"></i>List
                                    </a>
                                    <a class="dropdown-item" href="<?php echo $adminURL; ?>/subtitles">
                                        <i class="fas fa-copy mr-2"></i>Subtitles
                                    </a>
                                </div>
                            </li>
                            <li class="nav-item dropdown <?php echo strpos($_SERVER['REQUEST_URI'], 'hls') !== FALSE || strpos($_SERVER['REQUEST_URI'], 'hls') !== FALSE ? 'active' : ''; ?>">
                                <a class="nav-link dropdown-toggle" href="#" id="hlsVideos" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-film mr-2"></i>HLS
                                </a>
                                <div class="dropdown-menu shadow border-0" aria-labelledby="hlsVideos">
                                    <a class="dropdown-item" href="<?php echo $adminURL; ?>/hls/new">
                                        <i class="fas fa-plus-circle mr-2"></i>Add New Video
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="<?php echo $adminURL; ?>/hls">
                                        <i class="fas fa-film mr-2"></i>List
                                    </a>
                                </div>
                            </li>
                            <?php if (intval($userLogin['role']) === 0) : ?>
                                <li class="nav-item dropdown <?php echo strpos($_SERVER['REQUEST_URI'], 'gdrive') !== FALSE ? 'active' : ''; ?>">
                                    <a class="nav-link dropdown-toggle" href="#" id="gdrive" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fab fa-google-drive mr-2"></i>Google Drive
                                    </a>
                                    <div class="dropdown-menu shadow border-0" aria-labelledby="gdrive">
                                        <a class="dropdown-item" href="<?php echo $adminURL; ?>/gdrive_accounts/new">
                                            <i class="fas fa-plus-circle mr-2"></i>Add New Account
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="<?php echo $adminURL; ?>/gdrive_accounts">
                                            <i class="fas fa-key text-right mr-2"></i>Accounts
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $adminURL; ?>/gdrive_files">
                                            <i class="fas fa-film text-right mr-2"></i>Videos
                                        </a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown <?php echo strpos($_SERVER['REQUEST_URI'], 'plugin/') !== FALSE ? 'active' : ''; ?>">
                                    <a class="nav-link dropdown-toggle" href="#" id="plugin" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-puzzle-piece mr-2"></i>Plugins
                                    </a>
                                    <div class="dropdown-menu shadow border-0" aria-labelledby="plugin">
                                        <?php
                                        $list = createPluginMenus();
                                        if (!empty($list)) echo $list;
                                        else echo '<a class="dropdown-item" href="#">No Plugins</a>';
                                        ?>
                                    </div>
                                </li>
                                <li class="nav-item dropdown <?php echo strpos($_SERVER['REQUEST_URI'], 'users') !== FALSE ? 'active' : ''; ?>">
                                    <a class="nav-link dropdown-toggle" href="#" id="gdUser" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-users mr-2"></i>User
                                    </a>
                                    <div class="dropdown-menu shadow border-0" aria-labelledby="gdUser">
                                        <a class="dropdown-item" href="<?php echo $adminURL; ?>/users/new">
                                            <i class="fas fa-plus-circle mr-2"></i>Add New User
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="<?php echo $adminURL; ?>/users">
                                            <i class="fas fa-users mr-2"></i>List
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $adminURL; ?>/users_sessions">
                                            <i class="fas fa-history mr-2"></i>Sessions
                                        </a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown <?php echo strpos($_SERVER['REQUEST_URI'], 'p/') === FALSE && strpos($_SERVER['REQUEST_URI'], 'settings/') !== FALSE || strpos($_SERVER['REQUEST_URI'], 'balancers') !== FALSE ? 'active' : ''; ?>">
                                    <a class="nav-link dropdown-toggle" href="#" id="ndSettings" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-cog mr-2"></i>App
                                    </a>
                                    <div class="dropdown-menu shadow border-0" aria-labelledby="ndSettings">
                                        <?php if (!file_exists(BASE_DIR . '.rent')) : ?>
                                            <h6 class="dropdown-header">
                                                <i class="fas fa-server mr-2"></i>Load Balancer
                                            </h6>
                                            <a class="dropdown-item" href="<?php echo $adminURL; ?>/load_balancers">
                                                <i class="fas fa-angle-double-right text-right mr-2"></i>Servers
                                            </a>
                                            <div class="dropdown-divider"></div>
                                        <?php endif; ?>
                                        <h6 class="dropdown-header">
                                            <i class="fas fa-cog mr-2"></i>Settings
                                        </h6>
                                        <a class="dropdown-item" href="<?php echo $adminURL; ?>/settings/web">
                                            <i class="fas fa-angle-double-right text-right mr-2"></i>Website
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $adminURL; ?>/settings/general">
                                            <i class="fas fa-angle-double-right text-right mr-2"></i>General
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $adminURL; ?>/settings/public">
                                            <i class="fas fa-angle-double-right text-right mr-2"></i>Public
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $adminURL; ?>/settings/video">
                                            <i class="fas fa-angle-double-right text-right mr-2"></i>Video Player
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $adminURL; ?>/settings/ads">
                                            <i class="fas fa-angle-double-right text-right mr-2"></i>Advertisement
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $adminURL; ?>/settings/shortlink">
                                            <i class="fas fa-angle-double-right text-right mr-2"></i>Shortener Link
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $adminURL; ?>/settings/smtp">
                                            <i class="fas fa-angle-double-right text-right mr-2"></i>SMTP
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $adminURL; ?>/settings/misc">
                                            <i class="fas fa-angle-double-right text-right mr-2"></i>Miscellaneous
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $adminURL; ?>/settings/reset">
                                            <i class="fas fa-angle-double-right text-right mr-2"></i>Reset Settings
                                        </a>
                                    </div>
                                </li>
                            <?php endif; ?>
                            <?php
                            $list = createPluginAdminMenus('top');
                            if (!empty($list)) echo $list;
                            ?>
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown <?php echo strpos($_SERVER['REQUEST_URI'], 'profile') !== FALSE ? 'active' : ''; ?>">
                                <a class="nav-link dropdown-toggle" href="#" id="ndLoginuser" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-user-circle mr-2"></i><?php echo $userLogin['name']; ?>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right shadow border-0" aria-labelledby="ndLoginuser">
                                    <a class="dropdown-item" href="<?php echo $adminURL; ?>/profile">
                                        <i class="fas fa-user mr-2"></i>My Account
                                    </a>
                                    <?php
                                    $list = createPluginAdminMenus('user_details');
                                    if (!empty($list)) echo $list;
                                    ?>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="javascript:void(0)" onclick="location.href='<?php echo $adminURL; ?>/logout.php';" rel="noopener">
                                        <i class="fas fa-sign-out-alt mr-2"></i>Logout
                                    </a>
                                </div>
                            </li>
                        </ul>
                    <?php else : ?>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo $baseURL; ?>">
                                    <i class="fas fa-home mr-2"></i>Home
                                </a>
                            </li>
                            <li class="nav-item <?php if (strpos($_SERVER['REQUEST_URI'], 'login') !== FALSE) echo 'active'; ?>">
                                <a class="nav-link" href="<?php echo $adminURL . '/'; ?>">
                                    <i class="fas fa-sign-in-alt mr-2"></i>Login
                                </a>
                            </li>
                            <?php if (!file_exists(BASE_DIR . '.rent') && !filter_var(get_option('disable_registration'), FILTER_VALIDATE_BOOLEAN)) : ?>
                                <li class="nav-item <?php if (strpos($_SERVER['REQUEST_URI'], 'register') !== FALSE) echo 'active'; ?>">
                                    <a class="nav-link" href="<?php echo $adminURL . '/register'; ?>">
                                        <i class="fas fa-user-plus mr-2"></i>Register
                                    </a>
                                </li>
                            <?php endif; ?>
                            <li class="nav-item <?php if (strpos($_SERVER['REQUEST_URI'], 'reset-password') !== FALSE) echo 'active'; ?>">
                                <a class="nav-link" href="<?php echo $adminURL . '/reset-password'; ?>">
                                    <i class="fas fa-sync-alt mr-2"></i>Reset Password
                                </a>
                            </li>
                            <?php
                            $dev = parse_url($baseURL, PHP_URL_HOST);
                            if (strpos($dev, 'TvHay.') !== false || strpos($dev, 'localhost') !== false) : ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="https://TvHay.tv/">
                                        <i aria-hidden="true" class="fas fa-tv mr-2"></i>Live TV
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="btn btn-block btn-green dropdown-toggle" href="#" id="buy" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i aria-hidden="true" class="fas fa-shopping-basket mr-2"></i>Buy
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right shadow border-0" aria-labelledby="buy">
                                        <a class="dropdown-item" href="<?php echo $baseURL; ?>buy/" rel="noopener">
                                            <i aria-hidden="true" class="fas fa-shopping-basket mr-2"></i>Buy Source Code
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $baseURL; ?>buy-additional-host/" rel="noopener">
                                            <i aria-hidden="true" class="fas fa-plus-circle mr-2"></i>Buy Additional Hosts
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $baseURL; ?>rent/" rel="noopener">
                                            <i aria-hidden="true" class="fas fa-server mr-2"></i>Rent Server
                                        </a>
                                    </div>
                                </li>
                            <?php endif; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </nav>
        </header>
        <main id="main" role="main" class="mt-5 pt-5">