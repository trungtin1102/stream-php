<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

$data = [];
$recordsTotal = 0;
$recordsFiltered = 0;
$draw = isset($_GET['draw']) ? intval($_GET['draw']) : 1;
$token = '';

if (!is_admin()) {
    session_write_close();
    echo json_encode([
        'data' => $data,
        'draw' => $draw,
        'recordsTotal' => $recordsTotal,
        'recordsFiltered' => $recordsFiltered
    ]);
} else {
    session_write_close();
    // kolom yang kan ditampilkan
    $cols = ["id", "title", "description", "modifiedDate", "shared", "editable", "copyable", "id"];

    // datatables request
    $start = isset($_GET['start']) ? intval($_GET['start']) : 0;
    $length = isset($_GET['length']) && intval($_GET['length']) > 0 ? intval($_GET['length']) : 25;
    $orderBy = isset($_GET['order'][0]['column']) ? $cols[intval($_GET['order'][0]['column'])] : $cols[4];
    $orderDir = isset($_GET['order'][0]['dir']) ? htmlspecialchars($_GET['order'][0]['dir']) : 'desc';
    $search = isset($_GET['search']['value']) ? strtr(htmlspecialchars($_GET['search']['value']), ["'" => "\'", '"' => '\"']) : '';
    $email = !empty($_GET['email']) ? filter_var($_GET['email'], FILTER_SANITIZE_EMAIL) : '';
    $token = !empty($_GET['token']) ? htmlspecialchars($_GET['token']) : '';
    $private = !empty($_GET['private']) ? filter_var($_GET['private'], FILTER_VALIDATE_BOOLEAN) : false;
    $onlyFolder = !empty($_GET['onlyFolder']) ? filter_var($_GET['onlyFolder'], FILTER_VALIDATE_BOOLEAN) : false;
    $folder_id = !empty($_GET['folder_id']) ? htmlspecialchars($_GET['folder_id']) : 'root';

    // result
    if (!empty($email)) {
        $class = new \GDrive_Auth();
        $class->set_email($email);
        $list = $class->get_files($search, $length, $orderBy . ' ' . $orderDir, $token, $private, $folder_id, $onlyFolder);
        if ($list) {
            foreach ($list as $dt) {
                if (!empty($dt['id'])) {
                    $embed_link = BASE_URL . 'embed/?' . encode(http_build_query([
                        'host'  => 'gdrive',
                        'id'    => $dt['id'],
                        'email' => $email,
                        'saved' => false
                    ]));
                    $shared = isset($dt['shared']) ? $dt['shared'] : false;

                    $data[] = [
                        'DT_RowId' => $dt['id'],
                        'id' => $dt['id'],
                        'title' => isset($dt['title']) ? $dt['title'] : '',
                        'desc' => isset($dt['description']) ? $dt['description'] : '',
                        'mimeType' => [
                            'type' => isset($dt['mimeType']) ? $dt['mimeType'] : '',
                            'icon' => isset($dt['iconLink']) ? $dt['iconLink'] : ''
                        ],
                        'email' => $email,
                        'shared' => $shared,
                        'editable' => isset($dt['editable']) ? $dt['editable'] : false,
                        'copyable' => isset($dt['copyable']) ? $dt['copyable'] : false,
                        'modifiedDate' => isset($dt['modifiedDate']) ? date('Y-m-d H:i:s', strtotime($dt['modifiedDate'])) : '',
                        'actions' => [
                            'id'    => $dt['id'],
                            'shared' => $shared,
                            'embed'     => $embed_link,
                            'download'  => isset($dt['webContentLink']) ? $dt['webContentLink'] : '',
                            'preview'   => isset($dt['embedLink']) ? $dt['embedLink'] : '',
                            'view'      => isset($dt['alternateLink']) ? $dt['alternateLink'] : '',
                            'embed_code' => htmlspecialchars('<iframe src="' . $embed_link . '" frameborder="0" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" width="640" height="320"></iframe>')
                        ],
                    ];
                }
            }
            $recordsTotal = 50000000;
            $token = $class->get_nextPageToken();
        }
    }

    echo json_encode([
        'draw' => $draw,
        'data' => $data,
        'recordsTotal' => $recordsTotal,
        'recordsFiltered' => $recordsTotal,
        'token' => $token
    ]);
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
