<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

if (!is_admin()) {
    session_write_close();
    echo json_encode([
        'status' => 'fail',
        'message' => 'You are not authorized to access this feature!'
    ]);
} else {
    session_write_close();
    if (!empty($_POST['action'])) {
        session_write_close();
        switch ($_POST['action']) {
            case 'update_status':
                if (!file_exists(BASE_DIR . '.rent')) {
                    if (!empty($_POST['id']) && isset($_POST['status'])) {
                        $class = new \LoadBalancers();
                        $class->setCriteria('id', $_POST['id']);
                        $updated = $class->update(array(
                            'status' => intval($_POST['status']),
                            'updated' => time()
                        ));
                        if ($updated) {
                            echo json_encode([
                                'status' => 'ok',
                                'message' => 'Status updated successfully.'
                            ]);
                        } else {
                            echo json_encode([
                                'status' => 'fail',
                                'message' => $class->getLastError()
                            ]);
                        }
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'Invalid parameters!'
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'You are not authorized to access this feature!'
                    ]);
                }
                break;

            case 'delete':
                if (!file_exists(BASE_DIR . '.rent')) {
                    if (!empty($_POST['id'])) {
                        $class = new \LoadBalancers();
                        $class->setCriteria('id', $_POST['id']);
                        $deleted = $class->delete();
                        if ($deleted) {
                            echo json_encode([
                                'status' => 'ok',
                                'message' => 'Data deleted successfully.'
                            ]);
                        } else {
                            echo json_encode([
                                'status' => 'fail',
                                'message' => $class->getLastError()
                            ]);
                        }
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'Invalid parameters!'
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'You are not authorized to access this feature!'
                    ]);
                }
                break;

            default:
                echo json_encode([
                    'status' => 'fail',
                    'message' => 'What do you want?'
                ]);
                break;
        }
    } else {
        session_write_close();
        echo json_encode([
            'status' => 'fail',
            'message' => 'What do you want?'
        ]);
    }
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
