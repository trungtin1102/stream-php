<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

if (!is_admin()) {
    session_write_close();
    echo json_encode([
        'status' => 'fail',
        'message' => 'You are not authorized to access this feature!'
    ]);
} else {
    session_write_close();
    if (!empty($_POST['action'])) {
        session_write_close();
        switch ($_POST['action']) {
            case 'new_folder':
                $iCache = new \InstanceCache();
                $iCache->deleteItemsByTag('gdrive_files');

                $class = new \GDrive_Auth();
                if (isset($_POST['name']) && isset($_POST['email'])) {
                    $parents = !empty($_POST['parent_id']) ? [$_POST['parent_id']] : ['root'];
                    $create = $class->create_folder($_POST['name'], $parents, $_POST['email']);
                    if ($create) {
                        echo json_encode([
                            'status' => 'ok',
                            'message' => 'New folder created successfully!'
                        ]);
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'Failed to create new folder!'
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Email and folder name are required!'
                    ]);
                }
                break;

            case 'delete':
                if (!empty($_POST['id']) && !empty($_POST['email'])) {
                    $iCache = new \InstanceCache();
                    $iCache->deleteItemsByTag('gdrive_files');

                    $class = new \GDrive_Auth($_POST['id'], $_POST['email']);
                    $deleted = $class->delete_file();
                    if ($deleted) {
                        echo json_encode([
                            'status' => 'ok',
                            'message' => 'Data successfully deleted.'
                        ]);
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'The file cannot be deleted.'
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Invalid parameter!'
                    ]);
                }
                break;

            case 'public':
                if (!empty($_POST['id']) && !empty($_POST['email'])) {
                    $iCache = new \InstanceCache();
                    $iCache->deleteItemsByTag('gdrive_files');

                    $class = new \GDrive_Auth($_POST['id'], $_POST['email']);
                    $updated = $class->insert_permissions();
                    if ($updated) {
                        echo json_encode([
                            'status' => 'ok',
                            'message' => 'Now the file is publicly accessible!'
                        ]);
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'Failed to change the status of the file!'
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Invalid parameter!'
                    ]);
                }
                break;

            case 'private':
                if (!empty($_POST['id']) && !empty($_POST['email'])) {
                    $iCache = new \InstanceCache();
                    $iCache->deleteItemsByTag('gdrive_files');
                    
                    $class = new \GDrive_Auth($_POST['id'], $_POST['email']);
                    $deleted = $class->delete_permissions();
                    if ($deleted) {
                        echo json_encode([
                            'status' => 'ok',
                            'message' => 'Now the file is not accessible to the public!'
                        ]);
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'Failed to change the status of the file!'
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Invalid parameter!'
                    ]);
                }
                break;

            default:
                echo json_encode([
                    'status' => 'fail',
                    'message' => 'What do you want?'
                ]);
                break;
        }
    } else {
        session_write_close();
        echo json_encode([
            'status' => 'fail',
            'message' => 'What do you want?'
        ]);
    }
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
