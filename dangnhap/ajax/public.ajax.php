<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

if (!empty($_POST['action'])) {
    session_write_close();
    switch ($_POST['action']) {
        case 'get_load_balancer_list':
            $class = new \LoadBalancers();
            $class->setCriteria('status', 1);
            $list = $class->get(['link']);
            if ($list) {
                $list = array_column($list, 'link');
            } else {
                $list = [];
            }
            $list[] = get_option('main_site');

            echo json_encode(array(
                'status' => 'ok',
                'data' => $list
            ), true);
            break;

        case 'get_load_balancer_rand':
            if (!empty($_POST['hosts'])) {
                $hosts = $_POST['hosts'];
            } else {
                $hosts = [];
            }
            echo json_encode(array(
                'status' => 'ok',
                'data' => get_load_balancer_rand($hosts)
            ), true);
            break;

        case "check_username":
            if (!empty($_POST['username'])) {
                $class = new \Users();
                $class->setCriteria('user', $_POST['username']);
                $data = $class->getOne();
                if ($data) {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'The username is already registered, please use another one!'
                    ]);
                } else {
                    echo json_encode([
                        'status' => 'ok',
                        'message' => ''
                    ]);
                }
            }
            break;

        case "check_email":
            if (!empty($_POST['email'])) {
                $class = new \Users();
                $class->setCriteria('email', $_POST['email']);
                $data = $class->getOne();
                if ($data) {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'The email is already registered, please use another one!'
                    ]);
                } else {
                    echo json_encode([
                        'status' => 'ok',
                        'message' => ''
                    ]);
                }
            }
            break;

        default:
            echo json_encode([
                'status' => 'fail',
                'message' => 'What do you want?'
            ]);
            break;
    }
} else {
    session_write_close();
    echo json_encode([
        'status' => 'fail',
        'message' => 'What do you want?'
    ]);
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
