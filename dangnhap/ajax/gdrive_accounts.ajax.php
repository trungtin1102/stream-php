<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

if (is_admin()) {
    session_write_close();
    if (!empty($_POST['action'])) {
        session_write_close();
        switch ($_POST['action']) {
            case 'update_status':
                if (!file_exists(BASE_DIR . '.rent')) {
                    if (!empty($_POST['id']) && isset($_POST['status'])) {
                        $class = new \GDriveAuth();
                        $class->setCriteria('id', $_POST['id']);
                        $updated = $class->update(array(
                            'status' => intval($_POST['status']),
                            'modified' => time()
                        ));
                        if ($updated) {
                            echo json_encode([
                                'status' => 'ok',
                                'message' => 'Status updated successfully.'
                            ]);
                        } else {
                            echo json_encode([
                                'status' => 'fail',
                                'message' => $class->getLastError()
                            ]);
                        }
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'Invalid parameters!'
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'You are not authorized to access this feature!'
                    ]);
                }
                break;

            case 'delete':
                if (!empty($_POST['id'])) {
                    $iCache = new \InstanceCache();
                    $iCache->deleteItemsByTag('gdrive_files');

                    $class = new \GDriveAuth();
                    $class->setCriteria('id', $_POST['id']);
                    $get = $class->getOne(['email']);
                    if ($get) {
                        $excludes = gdrive_rent_excludes();
                        if (file_exists(BASE_DIR . '.rent') && in_array($get['email'], $excludes)) {
                            echo json_encode([
                                'status' => 'fail',
                                'message' => 'You are not allowed to delete this Google Drive account!'
                            ]);
                        } else {
                            $class->setCriteria('id', $_POST['id']);
                            $deleted = $class->delete();
                            if ($deleted) {
                                $class = new \GDriveMirrors();
                                $class->setCriteria('mirror_email', $get['email']);
                                $class->delete();
                                echo json_encode([
                                    'status' => 'ok',
                                    'message' => 'Data successfully deleted.'
                                ]);
                            } else {
                                echo json_encode([
                                    'status' => 'fail',
                                    'message' => $class->getLastError()
                                ]);
                            }
                        }
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Invalid parameter!'
                    ]);
                }
                break;
            default:
                echo json_encode([
                    'status' => 'fail',
                    'message' => 'What do you want?'
                ]);
                break;
        }
    } else {
        session_write_close();
        echo json_encode([
            'status' => 'fail',
            'message' => 'What do you want?'
        ]);
    }
} else {
    session_write_close();
    echo json_encode([
        'status' => 'fail',
        'message' => 'You are not authorized to access this feature!'
    ]);
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
