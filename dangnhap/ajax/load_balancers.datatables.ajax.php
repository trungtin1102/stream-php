<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

$data = [];
$recordsTotal = 0;
$recordsFiltered = 0;
$draw = isset($_GET['draw']) ? intval($_GET['draw']) : 1;

if (!is_admin()) {
    session_write_close();
    echo json_encode([
        'data' => $data,
        'draw' => $draw,
        'recordsTotal' => $recordsTotal,
        'recordsFiltered' => $recordsFiltered
    ]);
} else {
    session_write_close();
    // kolom yang kan ditampilkan
    $cols = ['name', 'link', 'id', 'status', 'public', 'added', 'updated', 'id'];

    // datatables request
    $start = isset($_GET['start']) ? intval($_GET['start']) : 0;
    $length = isset($_GET['length']) ? intval($_GET['length']) : 10;
    $orderBy = isset($_GET['order'][0]['column']) ? $cols[intval($_GET['order'][0]['column'])] : $cols[4];
    $orderDir = isset($_GET['order'][0]['dir']) ? htmlspecialchars($_GET['order'][0]['dir']) : 'DESC';
    $search = isset($_GET['search']['value']) ? strtr(htmlspecialchars($_GET['search']['value']), ["'" => "\'", '"' => '\"']) : '';

    $slug = file_exists(BASE_DIR . '.rent') ? SECURE_SALT : 'load_balancers';

    $class = new \LoadBalancers();
    $class->setLimit($start, $length);
    $class->setOrderBy($orderBy, $orderDir);
    if (!empty($search)) {
        $class->setCriteria('link', "$search%", 'LIKE');
        $class->setCriteria('name', "$search%", 'LIKE', 'OR');
        $class->setCriteria('added', "$search%", 'LIKE', 'OR');
        $class->setCriteria('updated', "$search%", 'LIKE', 'OR');
        $class->setCriteria('status', $search, '=', 'OR');
    }
    $list = $class->get(['id', 'name', 'link', 'public', 'status', 'added', 'updated']);
    if ($list) {
        foreach ($list as $dt) {
            $data[] = [
                'DT_RowId' => $dt['id'],
                'name' => $dt['name'],
                'link' => $dt['link'],
                'playbacks' => loadBalancers_PBCount($dt['id']),
                'public' => $dt['public'],
                'status' => $dt['status'],
                'added' => !empty($dt['added']) ? date('M d, Y H:i', $dt['added']) : '',
                'updated' => !empty($dt['updated']) ? date('M d, Y H:i', $dt['updated']) : '',
                'slug' => $slug,
                'id' => $dt['id']
            ];
        }

        $recordsTotal = (int) $class->getTotalRows();
        $recordsFiltered = (int) $class->getNumRows();
    }

    echo json_encode([
        'draw' => $draw,
        'data' => $data,
        'recordsTotal' => $recordsTotal,
        'recordsFiltered' => $recordsFiltered
    ]);
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
