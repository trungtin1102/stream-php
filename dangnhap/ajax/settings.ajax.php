<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

if (!is_admin()) {
    session_write_close();
    echo json_encode([
        'status' => 'fail',
        'message' => 'You are not authorized to use this feature!'
    ]);
} else {
    session_write_close();
    if (isset($_POST['action'])) {
        session_write_close();
        switch ($_POST['action']) {
            case 'create_custom_vast':
                if (empty($_POST['adFilename'])) {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Filename is required!'
                    ]);
                } elseif (empty($_POST['adClickThrough'])) {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Destination URL are required!'
                    ]);
                } elseif (empty($_POST['adMediaFile'])) {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Video File URL are required!'
                    ]);
                } elseif (!validate_url($_POST['adMediaFile'])) {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Video File URL must be filled with URL!'
                    ]);
                } elseif (!validate_url($_POST['adClickThrough'])) {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Destination URL must be filled with URL!'
                    ]);
                } else {
                    $adminDir = admin_dir();
                    open_resources_handler();
                    $fp = @fopen(BASE_DIR . $adminDir . '/templates/vast.xml', 'rb');
                    if ($fp) {
                        $content = stream_get_contents($fp);
                        fclose($fp);

                        $seconds = intval($_POST['adSkipOffset']);
                        $hours = intval($seconds / 3600);
                        $mins = intval($seconds / 60 % 60);
                        $secs = intval($seconds % 60);
                        $offset = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);

                        $seconds = intval($_POST['adDuration']);
                        $hours = intval($seconds / 3600);
                        $mins = intval($seconds / 60 % 60);
                        $secs = intval($seconds % 60);
                        $duration = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);

                        $replace = array(
                            '{siteName}' => get_option('site_name'),
                            '{adClickThrough}' => $_POST['adClickThrough'],
                            '{adMediaFile}' => $_POST['adMediaFile'],
                            '{adTitle}' => $_POST['adTitle'],
                            '{adDuration}' => $duration,
                            '{adSkipOffset}' => 'skipoffset="' . $offset . '"',
                        );
                        $content = strtr($content, $replace);

                        $name = rtrim($_POST['adFilename'], '.xml') . '.xml';
                        if (strpos($name, '/') !== false) {
                            echo json_encode([
                                'status' => 'fail',
                                'message' => 'Custom VAST ad failed to generate!'
                            ]);
                        } else {
                            create_dir(BASE_DIR . '/uploads');
                            $vastFile = BASE_DIR . 'uploads/' . $name;
                            if (file_exists($vastFile)) @unlink($vastFile);

                            $created = create_file($vastFile, $content);
                            if ($created) {
                                $vast = get_option('custom_vast');
                                $vast = is_array($vast) ? $vast : json_decode($vast, true);
                                $vast[] = $name;
                                set_option('custom_vast', json_encode($vast, true));
                                echo json_encode([
                                    'status' => 'ok',
                                    'message' => 'A custom VAST ad has been created successfully!',
                                    'result' => BASE_URL . 'uploads/' . $name
                                ]);
                            } else {
                                echo json_encode([
                                    'status' => 'fail',
                                    'message' => 'Custom VAST ad failed to generate!'
                                ]);
                            }
                        }
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'Custom VAST template not found!'
                        ]);
                    }
                }
                break;

            default:
                echo json_encode([
                    'status' => 'fail',
                    'message' => 'What do you want?'
                ]);
                break;
        }
    } elseif (isset($_GET['action'])) {
        session_write_close();
        switch ($_GET['action']) {
            case 'get_dependencies':
                $ext = [];
                $ext['exec'] = function_exists('exec');
                $ext['popen'] = function_exists('popen');
                $ext['shell_exec'] = function_exists('shell_exec');
                $ext['proc_open'] = function_exists('proc_open');
                $ext['proc_close'] = function_exists('proc_close');
                $ext['proc_get_status'] = function_exists('proc_get_status');
                $ext['chrome'] = detect_chrome();
                
                $checker = array_values($ext);
                if (in_array(false, $checker)) {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Some php functions and applications are not enabled/installed yet!',
                        'result' => $ext
                    ]);
                } else {
                    echo json_encode([
                        'status' => 'ok',
                        'message' => 'Supported php functions and applications have been enabled/installed.',
                        'result' => $ext
                    ]);
                }
                break;
            case 'extension_checker':
                if (!empty($_GET['extension'])) {
                    $ext = strtr($_GET['extension'], ['sqlite' => 'pdo_sqlite']);
                    if ($ext === 'redis' && (extension_loaded('redis') || extension_loaded('predis') || extension_loaded('phpredis'))) {
                        echo json_encode([
                            'status' => 'ok',
                            'message' => 'Extensions enabled!'
                        ]);
                    } elseif (extension_loaded($ext)) {
                        echo json_encode([
                            'status' => 'ok',
                            'message' => 'Extensions enabled!'
                        ]);
                    } elseif ($ext === 'files') {
                        echo json_encode([
                            'status' => 'ok',
                            'message' => 'Extensions enabled!'
                        ]);
                    } else {
                        echo json_encode([
                            'status' => 'error',
                            'message' => 'Extension not found or disabled!'
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'error',
                        'message' => 'Extension not found or disabled!'
                    ]);
                }
                break;

            case 'delete_videos_blacklisted':
                $list = strtolower(get_option('word_blacklisted'));
                $list = explode("\n", strtr($list, ["\r\n" => "\n"]));
                $list = array_unique($list);
                $list = array_filter($list, function ($a) {
                    return !empty(trim($a));
                });
                sort($list, SORT_NATURAL | SORT_FLAG_CASE);
                $data = implode("\n", $list);
                set_option('word_blacklisted', $data);

                if (!empty($list)) {
                    $where = '';
                    foreach ($list as $row) {
                        $row = addslashes(htmlspecialchars($row));
                        $where .= " LOWER(`title`) LIKE '$row%' OR";
                    }
                    $where = trim($where, 'OR');

                    $class = new \Model();
                    $deleted = $class->rawQuery("DELETE FROM `tb_videos` WHERE $where");
                    if ($deleted) {
                        $deleted = null;
                        unset($deleted);
                        echo json_encode([
                            'status' => 'ok',
                            'message' => 'Blacklisted videos have been successfully deleted!',
                            'data' => $data
                        ]);
                    } else {
                        $deleted = null;
                        unset($deleted);
                        echo json_encode([
                            'status' => 'fail',
                            'message' => $class->getLastError(),
                            'data' => $list
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'ok',
                        'message' => 'Blacklisted videos not found!',
                        'data' => $list
                    ]);
                }
                break;

            case 'clear_cache':
                try {
                    $deleted[] = deleteDir(BASE_DIR . 'cache');

                    set_option('chrome_ua', '');

                    $class = new \InstanceCache();
                    $deleted[] = $class->clear();

                    $class = new \VideosHash();
                    $class->setCriteria('id', '', '<>');
                    $deleted[] = $class->delete();

                    $class = new \VideoSources();
                    $class->setCriteria('id', '', '<>');
                    $deleted[] = $class->delete();

                    $deleted = array_unique($deleted);

                    if (in_array(true, $deleted)) {
                        echo json_encode([
                            'status' => 'ok',
                            'message' => 'Cache cleared successfully!'
                        ]);
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'Cache cannot be cleared properly!'
                        ]);
                    }
                } catch (\Exception $e) {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => $e->getMessage()
                    ]);
                }
                break;

            case 'reset_host':
                try {
                    remove_option('bypass_host');

                    $hosts = ['uploadbuzz', 'onedrive', 'vudeo', 'blogger', 'viu', 'vidio', 'streamlare', 'supervideo', 'voe', 'uqload', 'gofile', 'bayfiles', 'yadisk', 'anonfile', 'uploadsmobi', 'filesim', 'zippyshare', 'dropbox', 'fembed', 'filerio', 'gdrive', 'mixdropto', 'vidoza', 'yourupload', 'upstream', 'okru', 'mp4upload', 'mediafire', 'streamsb', 'tiktok', 'streamtape', 'dailymotion', 'pcloud', 'sibnet', 'sendvid', 'mymailru', 'gomunime', 'filecm', 'facebook', 'uptobox', 'racaty', 'solidfiles'];

                    set_option('bypass_host', $hosts);

                    echo json_encode([
                        'status' => 'ok',
                        'message' => 'Bypassed host reset was successful!',
                        'result' => $hosts
                    ]);
                } catch (\Exception $e) {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => $e->getMessage()
                    ]);
                }
                break;

            default:
                echo json_encode([
                    'status' => 'fail',
                    'message' => 'What do you want?'
                ]);
                break;
        }
    } else {
        session_write_close();
        echo json_encode([
            'status' => 'fail',
            'message' => 'What do you want?'
        ]);
    }
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
