<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

$data = [];
$recordsTotal = 0;
$recordsFiltered = 0;
$draw = isset($_GET['draw']) ? intval($_GET['draw']) : 1;

if (!is_admin()) {
    session_write_close();
    echo json_encode([
        'data' => $data,
        'draw' => $draw,
        'recordsTotal' => $recordsTotal,
        'recordsFiltered' => $recordsFiltered
    ]);
} else {
    session_write_close();
    // kolom yang kan ditampilkan
    $cols = ["u.`name`", "u.`user`", "u.`email`", "u.`status`", "u.`added`", "u.`updated`", "u.`role`", "u.`id`", "u.`id`"];

    // datatables request
    $start = isset($_GET['start']) ? intval($_GET['start']) : 0;
    $length = isset($_GET['length']) ? intval($_GET['length']) : 10;
    $orderBy = isset($_GET['order'][0]['column']) ? $cols[intval($_GET['order'][0]['column'])] : $cols[4];
    $orderDir = isset($_GET['order'][0]['dir']) ? htmlspecialchars($_GET['order'][0]['dir']) : "DESC";
    $search = isset($_GET['search']['value']) ? strtr(htmlspecialchars($_GET['search']['value']), ["'" => "\'", '"' => '\"']) : "";

    // search
    $where = '';
    if (!empty($search)) {
        $cols = array_unique($cols);
        $where = 'WHERE ';
        foreach ($cols as $col) {
            $where .= "$col LIKE '$search%' OR ";
        }
        $where = trim(trim($where), 'OR');
    }

    // result
    $class = new \Users();
    $qry = $class->rawQuery("SELECT u.`id`, u.`name`, u.`user`, u.`email`, u.`status`, u.`added`, u.`updated`, u.`role`, (SELECT COUNT(v.`id`) FROM `tb_videos` v WHERE v.`uid` = u.`id`) as videos FROM `tb_users` u $where ORDER BY $orderBy $orderDir LIMIT $start, $length");
    $list = $qry->fetchAll(\PDO::FETCH_ASSOC);

    $qry = null;
    unset($qry);

    if ($list) {
        foreach ($list as $dt) {
            $data[] = [
                'DT_RowId' => $dt['id'],
                'name' => $dt['name'],
                'user' => $dt['user'],
                'email' => $dt['email'],
                'status' => intval($dt['status']),
                'added' => !empty($dt['added']) ? date('M d, Y H:i', $dt['added']) : '',
                'updated' => !empty($dt['updated']) ? date('M d, Y H:i', $dt['updated']) : '',
                'role' => user_roles($dt['role']),
                'videos' => $dt['videos'],
                'id' => intval($dt['id'])
            ];
        }

        $recordsTotal = $class->rawQuery("SELECT COUNT(`id`) FROM `tb_users`")->fetchColumn();
        $recordsFiltered = $class->rawQuery("SELECT COUNT(u.`id`) FROM `tb_users` u $where")->fetchColumn();
    }

    echo json_encode([
        'draw' => $draw,
        'data' => $data,
        'recordsTotal' => $recordsTotal,
        'recordsFiltered' => $recordsFiltered
    ]);

    $recordsTotal = null;
    $recordsFiltered = null;
    unset($recordsTotal);
    unset($recordsFiltered);
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
