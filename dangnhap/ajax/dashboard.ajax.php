<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

$userLogin = current_user();
if ($userLogin) {
    session_write_close();
    if (!empty($_POST['action'])) {
        session_write_close();
        switch ($_POST['action']) {
            case 'views':
                $endDate = time();
                if (!empty($_POST['filter'])) {
                    if ($_POST['filter'] === 'this_year') {
                        $year = date('Y');
                        $startDate = strtotime($year . '-01-01 00:00:00');
                        $endDate = strtotime($year . '-12-31 23:59:00');
                    } elseif ($_POST['filter'] === 'last_year') {
                        $year = intval(date('Y')) - 1;
                        $startDate = strtotime($year . '-01-01 00:00:00');
                        $endDate = strtotime($year . '-12-31 23:59:00');
                    } elseif ($_POST['filter'] === 'six_months') {
                        $startDate = strtotime('-6 months');
                    } elseif ($_POST['filter'] === 'one_month') {
                        $startDate = strtotime('-1 month');
                    } else {
                        $startDate = strtotime('-7 days');
                    }
                    $filter = $_POST['filter'];
                } else {
                    $startDate = strtotime('-7 days');
                    $filter = 'seven_days';
                }

                $data = [];
                $iCache = new \InstanceCache();
                $iCache->setKey('views_' . $filter . '_' . $userLogin['id']);
                $cache = $iCache->get();
                if ($cache) {
                    $data = $cache;
                } else {
                    $class = new \Model();
                    $list = false;
                    if (intval($userLogin['role']) > 0) {
                        $list = $class->rawQuery("SELECT COUNT(s.`id`) AS jml, s.`created` FROM `tb_stats` s JOIN `tb_videos` v ON s.`vid` = v.`id` WHERE v.`uid`={$userLogin['id']} AND s.`created` >= {$startDate} AND s.`created` <= {$endDate} GROUP BY s.`created`");
                    } else {
                        $list = $class->rawQuery("SELECT COUNT(s.`id`) AS jml, s.`created` FROM `tb_stats` s JOIN `tb_videos` v ON s.`vid` = v.`id` WHERE s.`created` >= {$startDate} AND s.`created` <= {$endDate} GROUP BY s.`created`");
                    }
                    if ($list) {
                        foreach ($list as $dt) {
                            $time = intval($dt['created']) * 1000;
                            $data[] = array($time, intval($dt['jml']));
                        }
                        $iCache->save($data, 600, ['views', 'options']);
                    }
                    $list = null;
                    unset($list);
                }
                echo json_encode(array(
                    'status' => 'ok',
                    'data' => $data
                ));
                break;

            case 'servers_status':
                $result = [];
                $class = new \LoadBalancers();
                $class->setCriteria('status', 1);
                $list = $class->get(['id', 'name']);

                $class = new \VideoSources();
                if ($list) {
                    foreach ($list as $dt) {
                        $class->setCriteria('sid', $dt['id']);
                        $result[$dt['name']] = (int) $class->getNumRows();
                    }
                }
                $class->setCriteria('sid', 0);
                $result['Main Server'] = (int) $class->getNumRows();
                echo json_encode([
                    'status' => 'ok',
                    'data' => $result
                ]);
                break;

            case 'videos_status':
                $result = [];
                $class = new \Videos();
                if (intval($userLogin['role']) === 0) {
                    $class->setCriteria('status', 0);
                } else {
                    $class->setCriteria('uid', $userLogin['id']);
                    $class->setCriteria('status', 0, '=', 'AND');
                }
                $result['good'] = (int) $class->getNumRows();

                if (intval($userLogin['role']) === 0) {
                    $class->setCriteria('status', 1);
                } else {
                    $class->setCriteria('uid', $userLogin['id']);
                    $class->setCriteria('status', 1, '=', 'AND');
                }
                $result['broken'] = (int) $class->getNumRows();

                if (intval($userLogin['role']) === 0) {
                    $class->setCriteria('status', 2);
                } else {
                    $class->setCriteria('uid', $userLogin['id']);
                    $class->setCriteria('status', 2, '=', 'AND');
                }
                $result['warning'] = (int) $class->getNumRows();

                echo json_encode([
                    'status' => 'ok',
                    'data' => $result
                ]);
                break;

            default:
                echo json_encode([
                    'status' => 'fail',
                    'message' => 'What do you want?'
                ]);
                break;
        }
    } else {
        session_write_close();
        echo json_encode([
            'status' => 'fail',
            'message' => 'What do you want?'
        ]);
    }
} else {
    session_write_close();
    echo json_encode([
        'status' => 'fail',
        'message' => 'You are not authorized to access this feature!'
    ]);
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
