<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

if (!is_admin()) {
    session_write_close();
    echo json_encode([
        'status' => 'fail',
        'message' => 'You are not authorized to access this feature!'
    ]);
} else {
    session_write_close();
    $data = !empty($_POST) ? $_POST : $_GET;
    if (!empty($data['action'])) {
        switch ($data['action']) {
            case 'delete':
                $class = new \Videos();
                $class->setCriteria('uid', $data['id']);
                $count = $class->getNumRows();
                if ($count === 0) {
                    $class = new \Users();
                    $class->setCriteria('id', $data['id']);
                    $deleted = $class->delete();
                    if ($deleted) {
                        echo json_encode([
                            'status' => 'ok',
                            'message' => 'Data successfully deleted.'
                        ]);
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => $class->getLastError()
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => ''
                    ]);
                }
                break;

            default:
                echo json_encode([
                    'status' => 'fail',
                    'message' => 'What do you want?'
                ]);
                break;
        }
    } else {
        echo json_encode([
            'status' => 'fail',
            'message' => 'What do you want?'
        ]);
    }
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
