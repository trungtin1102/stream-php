<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

$data = [];
$recordsTotal = 0;
$recordsFiltered = 0;
$draw = isset($_GET['draw']) ? intval($_GET['draw']) : 1;

$userLogin = current_user();
if (!$userLogin) {
    session_write_close();
    echo json_encode([
        'data' => $data,
        'draw' => $draw,
        'recordsTotal' => $recordsTotal,
        'recordsFiltered' => $recordsFiltered
    ]);
} else {
    session_write_close();
    // validate file
    $class = new \SubtitleManager();
    $class->setCriteria('host', '', '=');
    $class->setCriteria('host', 'IS NULL', 'OR');
    $class->setCriteria('host', 'localhost', '=', 'OR');
    $list = $class->get(['id', 'file_name']);
    if ($list) {
        foreach ($list as $dt) {
            if (file_exists(BASE_DIR . 'subtitles/' . $dt['file_name']) || file_exists(BASE_DIR . 'uploads/subtitles/' . $dt['file_name'])) {
                $class->setCriteria('id', $dt['id']);
                $class->update(array(
                    'host' => BASE_URL
                ));
            }
        }
    }

    // kolom yang kan ditampilkan
    $cols = ["s.`id`", "s.`file_name`", "s.`language`", "u.`name`", "s.`host`", "s.`added`", "s.`id`"];

    // datatables request
    $start = isset($_GET['start']) ? intval($_GET['start']) : 0;
    $length = isset($_GET['length']) ? intval($_GET['length']) : 10;
    $orderBy = isset($_GET['order'][0]['column']) ? htmlspecialchars($cols[intval($_GET['order'][0]['column'])]) : $cols[4];
    $orderDir = isset($_GET['order'][0]['dir']) ? htmlspecialchars($_GET['order'][0]['dir']) : "DESC";
    $search = isset($_GET['search']['value']) ? strtr(htmlspecialchars($_GET['search']['value']), ["'" => "\'", '"' => '\"']) : "";

    // search
    $uid = (int) $userLogin['id'];
    $role = (int) $userLogin['role'];
    $where = '';
    if ($role > 0) {
        $where = 'WHERE s.`uid`=' . $uid;
    }
    // search
    if (!empty($search)) {
        $cols = array_unique($cols);
        if ($role > 0) {
            $where .= ' AND (';
        } else {
            $where .= 'WHERE (';
        }
        foreach ($cols as $col) {
            $where .= "$col LIKE '$search%' OR ";
        }
        $where = trim(trim($where), 'OR') . ')';
    }

    // result
    $scheme = isSSL() ? 'https://' : 'http://';
    $class = new \SubtitleManager();
    $qry = $class->rawQuery("SELECT s.`id`, s.`file_name`, s.`language`, s.`host`, s.`added`, u.`name` FROM `tb_subtitle_manager` s INNER JOIN `tb_users` u ON u.`id` = s.`uid` $where ORDER BY $orderBy $orderDir LIMIT $start, $length");
    $list = $qry->fetchAll(\PDO::FETCH_ASSOC);

    $qry = null;
    unset($qry);

    if ($list) {
        foreach ($list as $dt) {
            $data[] = [
                'DT_RowId' => $dt['id'],
                'file_name' => '<a href="' . $scheme . trim(strtr($dt['host'], ['https://' => '', 'http://']), '/') . '/uploads/subtitles/' . $dt['file_name'] . '" target="_blank">' . $dt['file_name'] . '</a>',
                'language' => $dt['language'],
                'name' => $dt['name'],
                'added' => !empty($dt['added']) ? date('M d, Y H:i', $dt['added']) : '',
                'host' => $dt['host'],
                'id' => [
                    'id'    => $dt['id'],
                    'link'  => $scheme . trim(strtr($dt['host'], ['https://' => '', 'http://']), '/') . '/uploads/subtitles/' . $dt['file_name']
                ]
            ];
        }

        $where = $role > 0 ? "WHERE s.`uid` = $uid" : '';
        $recordsTotal = $class->rawQuery("SELECT COUNT(s.`id`) FROM `tb_subtitle_manager` s $where")->fetchColumn();
        $recordsFiltered = $class->rawQuery("SELECT COUNT(s.`id`) FROM `tb_subtitle_manager` s JOIN `tb_users` u ON u.`id` = s.`uid` $where")->fetchColumn();
    }

    echo json_encode([
        'draw' => $draw,
        'data' => $data,
        'recordsTotal' => $recordsTotal,
        'recordsFiltered' => $recordsFiltered
    ]);

    $recordsTotal = null;
    $recordsFiltered = null;
    unset($recordsTotal);
    unset($recordsFiltered);
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
