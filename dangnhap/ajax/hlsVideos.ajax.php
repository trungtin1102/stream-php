<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

$userLogin = current_user();
if (!$userLogin) {
    session_write_close();
    echo json_encode([
        'status' => 'fail',
        'message' => 'You must login first!'
    ]);
} else {
    session_write_close();
    if (!empty($_POST['action'])) {
        session_write_close();
        switch ($_POST['action']) {
            case 'get_server':
                if (!empty($_POST['id'])) {
                    $isLB = is_load_balancer();
                    if ($isLB) {
                        echo json_encode(array(
                            'status' => 'ok',
                            'data' => BASE_URL
                        ));
                    } else {
                        $url = BASE_URL;
                        $hosts = [];
                        if (!empty($_POST['source'])) {
                            $class = new \Videos();
                            $class->setCriteria('id', $_POST['id']);
                            $data = $class->getOne(['host', 'ahost', 'host_id', 'ahost_id']);
                            if ($data) {
                                if (!empty($data['host_id'])) {
                                    $hosts[] = array(
                                        'host' => $data['host'],
                                        'host_id' => $data['host_id']
                                    );
                                }
                                if (!empty($data['ahost_id'])) {
                                    $hosts[] = array(
                                        'host' => $data['ahost'],
                                        'host_id' => $data['ahost_id']
                                    );
                                }
                            }
                        } elseif (!empty($_POST['id'])) {
                            $class = new \Hosting($_POST['id']);
                            $hosts[] = array(
                                'host' => $class->getHost(),
                                'host_id' => $class->getID()
                            );
                        }
                        if (!empty($hosts)) {
                            sleep(3);
                            $url = get_load_balancer_rand(array_column($hosts, 'host'));
                            echo json_encode(array(
                                'status' => 'ok',
                                'data' => $url
                            ));
                        } else {
                            echo json_encode([
                                'status' => 'fail',
                                'message' => 'Sources not found!'
                            ]);
                        }
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'What do you want?'
                    ]);
                }
                break;

            case 'db_query':
                if (!empty($_POST['id'])) {
                    $data = false;
                    $mainSite = parse_url(get_option('main_site'), PHP_URL_HOST);
                    $token = isset($_COOKIE['adv_token']) ? $_COOKIE['adv_token'] : '';
                    $useTitleAsSlug = isset($_POST['useTitleAsSlug']) ? boolval($_POST['useTitleAsSlug']) : false;
                    if (validate_url($_POST['id'])) {
                        $class = new \Hosting($_POST['id']);
                        $host = $class->getHost();
                        $id = $class->getID();
                        $data = array(
                            'id' => $id,
                            'host' => $host,
                            'uid' => $userLogin['id'],
                            'origin' => $mainSite,
                            'token' => $token,
                            'saved' => true,
                            'useTitleAsSlug' => $useTitleAsSlug
                        );
                    } else {
                        $class = new \Videos();
                        $class->setCriteria('id', $_POST['id']);
                        $data = $class->getOne(['uid']);
                        if ($data) $uid = $data['uid'];
                        else $uid = $userLogin['id'];
                        $data = array(
                            'id' => $_POST['id'],
                            'source' => 'db',
                            'uid' => $uid,
                            'origin' => $mainSite,
                            'token' => $token,
                            'saved' => true,
                            'useTitleAsSlug' => $useTitleAsSlug
                        );
                    }
                    if ($data) {
                        echo json_encode(array(
                            'status' => 'ok',
                            'data' => encode(http_build_query($data))
                        ));
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'Data not found!'
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'What do you want?'
                    ]);
                }
                break;

            case 'update_status':
                if (!empty($_POST['id'])) {
                    $class = new \Videos();
                    $class->setCriteria('id', $_POST['id']);
                    $data = $class->getOne(['host_id', 'ahost_id']);
                    if ($data) {
                        $slength = isset($_POST['data']['sources']) ? count($_POST['data']['sources']) : 0;
                        $alength = isset($_POST['data']['sources_alt']) ? count($_POST['data']['sources_alt']) : 0;
                        $status = 0;
                        if (!empty($data['host_id']) && !empty($data['ahost_id'])) {
                            if ($slength > 0 && $alength > 0) $status = 0;
                            elseif ($slength > 0 || $alength > 0) $status = 2;
                            else $status = 1;
                        } elseif (!empty($data['host_id']) || !empty($data['ahost_id'])) {
                            if ($slength > 0 || $alength > 0) $status = 0;
                            else $status = 1;
                        } else {
                            $status = 1;
                        }
                        $class->setCriteria('id', $_POST['id']);
                        $updated = $class->update(array(
                            'status' => $status
                        ));
                        if ($updated) {
                            echo json_encode([
                                'status' => 'ok',
                                'message' => 'Video updated successfully.'
                            ]);
                        } else {
                            echo json_encode([
                                'status' => 'fail',
                                'message' => $class->getLastError()
                            ]);
                        }
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'Video does not exist!'
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'What do you want?'
                    ]);
                }
                break;

            case 'remove_poster':
                if (!empty($_POST['id'])) {
                    $class = new \Videos();
                    $class->setCriteria('id', $_POST['id']);
                    $data = $class->getOne();
                    if ($data) {
                        if (!empty($data['poster'])) {
                            $mainSite = get_option('main_site');
                            if (strpos($data['poster'], $mainSite) !== FALSE) {
                                @unlink(strtr($data['poster'], [$mainSite => BASE_DIR]));
                            } else {
                                @unlink(BASE_DIR . 'uploads/images/' . $data['poster']);
                            }
                        }
                        $class->setCriteria('id', $_POST['id']);
                        $updated = $class->update([
                            'poster' => ''
                        ]);
                        if ($updated) {
                            echo json_encode([
                                'status' => 'ok',
                                'message' => 'The poster image has been successfully removed!'
                            ]);
                        } else {
                            echo json_encode([
                                'status' => 'fail',
                                'message' => $class->getLastError()
                            ]);
                        }
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => $class->getLastError()
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'What do you want?'
                    ]);
                }
                break;

            case 'delete':
                if (!empty($_POST['id'])) {
                    sleep(3);
                    $id = $_POST['id'];
                    $class = new \Videos();
                    $class->setCriteria('id', $id);
                    $data = $class->getOne(['host', 'host_id', 'ahost', 'ahost_id']);
                    if ($data) {
                        // delete video
                        $class->setCriteria('id', $id);
                        $deleted[] = $class->delete();

                        // delete video alternatives
                        $class = new \VideosAlternatives();
                        $class->setCriteria('vid', $id);
                        $list = $class->get(['id', 'host', 'host_id']);
                        if ($list) {
                            foreach ($list as $dt) {
                                $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $dt['host'] . '/' . keyFilter($dt['host_id']) . '.m3u8');
                                $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $dt['host'] . '/' . keyFilter($dt['host_id']) . '.mpd');
                            }
                            $class->setCriteria('vid', $id);
                            $deleted[] = $class->delete();
                        }

                        // delete hash
                        $class = new \VideoShort();
                        $class->setCriteria('vid', $id);
                        $deleted[] = $class->delete();

                        // delete subtitles
                        $class = new \Subtitles();
                        $class->setCriteria('vid', $id);
                        $deleted[] = $class->delete();

                        // delete hls/mpd file cache
                        $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $data['host'] . '/' . keyFilter($data['host_id']) . '.m3u8');
                        $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $data['host'] . '/' . keyFilter($data['host_id']) . '.mpd');

                        open_resources_handler();
                        $playlistDir = is_dir(BASE_DIR . 'cache/playlist') ? new \DirectoryIterator(BASE_DIR . 'cache/playlist') : false;
                        if ($playlistDir) {
                            foreach ($playlistDir as $file) {
                                if (!$file->isDot() && $file->isFile() && strpos($file->getFilename(), $data['host'] . '~' . keyFilter($data['host_id'])) !== FALSE) {
                                    $deleted[] = @unlink(BASE_DIR . 'cache/playlist/' . $file->getFilename());
                                }
                            }
                        }

                        if (!empty($data['ahost']) && !empty($data['ahost_id'])) {
                            $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $data['ahost'] . '/' . keyFilter($data['ahost_id']) . '.m3u8');
                            $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $data['ahost'] . '/' . keyFilter($data['ahost_id']) . '.mpd');
                            if ($playlistDir) {
                                foreach ($playlistDir as $file) {
                                    if (!$file->isDot() && $file->isFile() && strpos($file->getFilename(), $data['ahost'] . '~' . keyFilter($data['ahost_id'])) !== FALSE) {
                                        $deleted[] = @unlink(BASE_DIR . 'cache/playlist/' . $file->getFilename());
                                    }
                                }
                            }
                        }

                        $deleted = array_unique($deleted);

                        if (in_array(true, $deleted)) {
                            echo json_encode([
                                'status' => 'ok',
                                'message' => 'Video deleted successfully.'
                            ]);
                        } else {
                            echo json_encode([
                                'status' => 'fail',
                                'message' => 'Failed to clear video cache or cache does not exist.'
                            ]);
                        }
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => $class->getLastError()
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'What do you want?'
                    ]);
                }
                break;

            default:
                echo json_encode([
                    'status' => 'fail',
                    'message' => 'What do you want?'
                ]);
                break;
        }
    } else {
        session_write_close();
        echo json_encode([
            'status' => 'fail',
            'message' => 'What do you want?'
        ]);
    }
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
