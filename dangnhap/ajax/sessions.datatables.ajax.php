<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

$data = [];
$recordsTotal = 0;
$recordsFiltered = 0;
$draw = isset($_GET['draw']) ? intval($_GET['draw']) : 1;

if (!is_admin()) {
    session_write_close();
    echo json_encode([
        'data' => $data,
        'draw' => $draw,
        'recordsTotal' => $recordsTotal,
        'recordsFiltered' => $recordsFiltered
    ]);
} else {
    session_write_close();
    // kolom yang kan ditampilkan
    $cols = ["id", "username", "ip", "useraganet", "created", "expired", "id"];

    // datatables request
    $start = isset($_GET['start']) ? intval($_GET['start']) : 0;
    $length = isset($_GET['length']) && intval($_GET['length']) > 0 ? intval($_GET['length']) : 25;
    $orderBy = isset($_GET['order'][0]['column']) ? $cols[intval($_GET['order'][0]['column'])] : $cols[0];
    $orderDir = isset($_GET['order'][0]['dir']) ? htmlspecialchars($_GET['order'][0]['dir']) : "asc";
    $search = isset($_GET['search']['value']) ? strtr(htmlspecialchars($_GET['search']['value']), ["'" => "\'", '"' => '\"']) : "";

    $class = new \Sessions();
    $class->setLimit($start, $length);
    $class->setOrderBy($orderBy, $orderDir);
    if (!empty($search)) {
        $class->setCriteria('ip', "$search%", 'LIKE');
        $class->setCriteria('useragent', "$search%", 'LIKE', 'OR');
        $class->setCriteria('created', "$search%", 'LIKE', 'OR');
        $class->setCriteria('expired', "$search%", 'LIKE', 'OR');
        $class->setCriteria('username', "$search%", 'LIKE', 'OR');
    }
    $list = $class->get(['id', 'ip', 'username', 'useragent', 'created', 'expired']);
    if ($list) {
        foreach ($list as $dt) {
            $data[] = [
                'DT_RowId' => $dt['id'],
                'id' => $dt['id'],
                'ip' => $dt['ip'],
                'username' => $dt['username'],
                'useragent' => $dt['useragent'],
                'created' => !empty($dt['created']) ? date('M d, Y H:i', $dt['created']) : '',
                'expired' => !empty($dt['expired']) ? date('M d, Y H:i', $dt['expired']) : '',
                'actions' => $dt['id']
            ];
        }

        $recordsTotal = (int) $class->getTotalRows();
        $recordsFiltered = (int) $class->getNumRows();
    }

    echo json_encode([
        'draw' => $draw,
        'data' => $data,
        'recordsTotal' => $recordsTotal,
        'recordsFiltered' => $recordsFiltered
    ]);
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
