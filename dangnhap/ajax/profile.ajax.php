<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

$userLogin = current_user();
if (!$userLogin) {
    session_write_close();
    echo json_encode([
        'status' => 'fail',
        'message' => 'You must login first!'
    ]);
} else {
    session_write_close();
    if ($userLogin['user'] === 'demo') {
        session_write_close();
        echo json_encode([
            'status' => 'fail',
            'message' => 'You are not authorized to use this feature!'
        ]);
    } else {
        session_write_close();
        if (!empty($_POST['action'])) {
            session_write_close();
            switch ($_POST['action']) {
                case 'editEmail':
                    if (!empty($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                        $class = new \Users();
                        $class->setCriteria('id', $userLogin['id'], '<>');
                        $class->setCriteria('email', $_POST['email'], '=', 'AND');
                        $data = $class->getOne();
                        if ($data) {
                            echo json_encode([
                                'status' => 'fail',
                                'message' => 'Email has already been used by another user.'
                            ]);
                        } else {
                            $class->setCriteria('id', $userLogin['id']);
                            $data = $class->update(array(
                                'email' => $_POST['email'],
                                'updated' => time()
                            ));
                            if ($data) {
                                $class = new \Login();
                                $class->logout();
                                echo json_encode([
                                    'status' => 'ok',
                                    'message' => 'Email successfully updated. Please log back in.'
                                ]);
                            } else {
                                echo json_encode([
                                    'status' => 'fail',
                                    'message' => $class->getLastError()
                                ]);
                            }
                        }
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'Email must be valid!'
                        ]);
                    }
                    break;

                case 'editUsername':
                    if (!empty($_POST['user'])) {
                        $class = new \Users();
                        $class->setCriteria('id', $userLogin['id'], '<>');
                        $class->setCriteria('user', $_POST['user'], '=', 'AND');
                        $data = $class->getOne();
                        if ($data) {
                            echo json_encode([
                                'status' => 'fail',
                                'message' => 'The username is already taken by another user!'
                            ]);
                        } else {
                            $class->setCriteria('id', $userLogin['id']);
                            $data = $class->update(array(
                                'user' => $_POST['user'],
                                'updated' => time()
                            ));
                            if ($data) {
                                $class = new \Login();
                                $class->logout();
                                echo json_encode([
                                    'status' => 'ok',
                                    'message' => 'Username successfully updated. Please log back in.'
                                ]);
                            } else {
                                echo json_encode([
                                    'status' => 'fail',
                                    'message' => $class->getLastError()
                                ]);
                            }
                        }
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'The username must be filled in!'
                        ]);
                    }
                    break;

                default:
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'What do you want?'
                    ]);
                    break;
            }
        } else {
            session_write_close();
            echo json_encode([
                'status' => 'fail',
                'message' => 'What do you want?'
            ]);
        }
    }
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
