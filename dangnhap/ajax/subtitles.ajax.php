<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

if (!current_user()) {
    session_write_close();
    echo json_encode([
        'status' => 'fail',
        'message' => 'You must login first!'
    ]);
} else {
    session_write_close();
    if (!empty($_POST['action'])) {
        session_write_close();
        switch ($_POST['action']) {
            case 'migrate':
                if (is_admin()) {
                    if (!empty($_POST['subHosts']) && !empty($_POST['subNewLoc'])) {
                        if (validate_url($_POST['subNewLoc'])) {
                            $class = new \SubtitleManager();
                            $class->setCriteria('host', $_POST['subHosts']);
                            $list = $class->get();
                            if ($list) {
                                $url = rtrim(filter_var($_POST['subNewLoc'], FILTER_SANITIZE_URL), '/') . '/';
                                $class->setCriteria('host', $_POST['subHosts']);
                                $updated = $class->update(array(
                                    'host' => $url
                                ));
                                if ($updated) {
                                    if (parse_url(BASE_URL, PHP_URL_HOST) === parse_url($url, PHP_URL_HOST)) {
                                        foreach ($list as $dt) {
                                            $file = BASE_DIR . 'uploads/subtitles/' . $dt['file_name'];
                                            if (!file_exists($file)) {
                                                open_resources_handler();
                                                $exist = @fopen($dt['host'] . '/subtitles/' . $dt['file_name'], 'r');
                                                if ($exist) {
                                                    stream_copy_to_stream($exist, $file);
                                                    fclose($exist);
                                                }
                                                open_resources_handler();
                                                $exist1 = @fopen($dt['host'] . '/uploads/subtitles/' . $dt['file_name'], 'r');
                                                if ($exist1) {
                                                    stream_copy_to_stream($exist1, $file);
                                                    fclose($exist1);
                                                }
                                            }
                                        }
                                    }
                                    echo json_encode(array(
                                        'status' => 'ok',
                                        'message' => 'Subtitle migration successful!'
                                    ));
                                } else {
                                    echo json_encode(array(
                                        'status' => 'fail',
                                        'message' => 'Old and new locations cannot be empty!'
                                    ));
                                }
                            } else {
                                echo json_encode(array(
                                    'status' => 'fail',
                                    'message' => 'Old location not found!'
                                ));
                            }
                        } else {
                            echo json_encode(array(
                                'status' => 'fail',
                                'message' => 'Old and new locations cannot be empty!'
                            ));
                        }
                    } else {
                        echo json_encode(array(
                            'status' => 'fail',
                            'message' => 'Old and new locations cannot be empty!'
                        ));
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'You are not authorized to use this feature!'
                    ]);
                }
                break;

            case 'get_hosts':
                $list = [];
                if (is_admin()) {
                    $class = new \SubtitleManager();
                    $class->setGroupBy(['host']);
                    $class->setOrderBy('host');
                    $list = $class->get();
                    if ($list) {
                        $list = array_column($list, 'host');
                        $list = array_filter($list);
                        $list = array_unique($list);
                    }
                }
                echo json_encode(array(
                    'status' => 'ok',
                    'message' => '',
                    'data' => $list
                ));
                break;

            case 'delete':
                if (!empty($_POST['id'])) {
                    $class = new \SubtitleManager();
                    $class->setCriteria('id', $_POST['id']);
                    $data = $class->getOne(['host', 'file_name']);
                    if ($data) {
                        // delete subtitle file
                        @unlink(BASE_DIR . 'subtitles/' . $data['file_name']);
                        @unlink(BASE_DIR . 'uploads/subtitles/' . $data['file_name']);

                        // delete subtitle file manager
                        $class->setCriteria('id', $_POST['id']);
                        $class->delete();

                        // delete subtitles from video
                        $link = $data['host'] . 'subtitles/' . $data['file_name'];
                        $link2 = $data['host'] . 'uploads/subtitles/' . $data['file_name'];

                        $class = new \Subtitles();
                        $class->setCriteria('link', $link, '=');
                        $class->setCriteria('link', $link2, '=', 'OR');
                        $class->delete();

                        echo json_encode([
                            'status' => 'ok',
                            'message' => 'The subtitle has been removed.'
                        ]);
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => $class->getLastError()
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Select the subtitle first.'
                    ]);
                }
                break;

            case 'delete_video_subtitle':
                if (!empty($_POST['id'])) {
                    $class = new \Subtitles();
                    $class->setCriteria('id', $_POST['id']);
                    $deleted = $class->delete();
                    if ($deleted) {
                        echo json_encode([
                            'status' => 'ok',
                            'message' => 'The subtitle has been removed.'
                        ]);
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => $class->getLastError()
                        ]);
                    }
                } else {
                }
                break;

            default:
                echo json_encode([
                    'status' => 'fail',
                    'message' => 'What do you want?'
                ]);
                break;
        }
    } else {
        session_write_close();
        echo json_encode([
            'status' => 'fail',
            'message' => 'What do you want?'
        ]);
    }
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
