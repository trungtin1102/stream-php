<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

$data = [];
$recordsTotal = 0;
$recordsFiltered = 0;
$draw = isset($_GET['draw']) ? intval($_GET['draw']) : 1;

$userLogin = current_user();
if (!$userLogin) {
    session_write_close();
    echo json_encode([
        'data' => $data,
        'draw' => $draw,
        'recordsTotal' => $recordsTotal,
        'recordsFiltered' => $recordsFiltered
    ]);
} else {
    session_write_close();
    // kolom yang kan ditampilkan
    $cols = ["v.`id`", "v.`title`", "v.`host`", "v.`status`", "v.`id`", 'v.`views`', "u.`name`", "v.`updated`", "v.`id`"];

    // datatables request
    $start = isset($_GET['start']) ? intval($_GET['start']) : 0;
    $length = isset($_GET['length']) && intval($_GET['length']) > -1 ? intval($_GET['length']) : 10;
    $search = isset($_GET['search']['value']) ? strtr(htmlspecialchars($_GET['search']['value']), ["'" => "\'", '"' => '\"']) : "";
    if (isset($_GET['recent'])) {
        $orderBy = 'v.`added`';
        $orderDir = 'DESC';
    } elseif (isset($_GET['popular'])) {
        $orderBy = 'v.`views`';
        $orderDir = 'DESC';
    } else {
        $orderBy = isset($_GET['order'][0]['column']) ? $cols[intval($_GET['order'][0]['column'])] : $cols[6];
        $orderDir = isset($_GET['order'][0]['dir']) ? htmlspecialchars($_GET['order'][0]['dir']) : "DESC";
    }

    $where = '';
    if (intval($userLogin['role']) > 0) {
        $where = 'WHERE v.`uid`=' . $userLogin['id'];
    }
    // search
    if (!empty($search)) {
        $cols[] = 'v.`host_id`';
        $cols[] = 'v.`ahost`';
        $cols[] = 'v.`ahost_id`';
        $cols = array_unique($cols);
        if (intval($userLogin['role']) > 0) {
            $where .= ' AND (';
        } else {
            $where .= 'WHERE (';
        }
        foreach ($cols as $col) {
            $where .= "$col LIKE '$search%' OR ";
        }
        $where = trim(trim($where), 'OR') . ')';
    }
    // show by status
    if (isset($_GET['status']) && !is_null($_GET['status']) && $_GET['status'] !== 'null') {
        if (!empty($where)) {
            $where .= ' AND v.`status`=' . $_GET['status'];
        } else {
            $where = ' WHERE v.`status`=' . $_GET['status'];
        }
    }

    // result
    $class = new \Model();
    $list = $class->rawFetchAll("SELECT v.*, u.`name` FROM `tb_videos` v INNER JOIN `tb_users` u ON u.`id` = v.`uid` $where ORDER BY $orderBy $orderDir LIMIT $start, $length");
    if ($list) {
        $hosting = new \Hosting();
        $subtitle = new \Subtitles();
        $shortKey = new \VideoShort();
        $alt = new \VideosAlternatives();
        foreach ($list as $dt) {
            $hosts = [];
            $downloads = [];
            $subs = [];

            $shortKey->setCriteria('vid', $dt['id']);
            $short = $shortKey->getOne();
            if ($short) {
                $embed_link     = BASE_URL . 'embed/' . $short['key'];
                $download_link  = BASE_URL . 'download/' . $short['key'];
            } else {
                $query = array(
                    'source' => 'db',
                    'id' => $dt['id'],
                );
                $query_encoded  = encode(http_build_query($query));
                $embed_link     = BASE_URL . 'embed/?' . $query_encoded;
                $download_link  = BASE_URL . 'download/?' . $query_encoded;
            }
            $embed_iframe = htmlspecialchars('<iframe src="' . $embed_link . '" frameborder="0" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" width="640" height="320"></iframe>');

            if (!empty($dt['host_id'])) {
                $host = strtr($dt['host'], ['amazondrive' => 'amazon']);
                $hosts[] = $host;
                $hosting->setHost($dt['host']);
                $hosting->setID($dt['host_id']);
                $downloads[] = [
                    'host' => $dt['host'],
                    'url' => $hosting->getDownloadLink()
                ];
            }

            if (!empty($dt['ahost_id'])) {
                $ahost = strtr($dt['ahost'], ['amazondrive' => 'amazon']);
                $hosts[] = $ahost;
                $hosting->setHost($dt['ahost']);
                $hosting->setID($dt['ahost_id']);
                $downloads[] = [
                    'host' => $dt['ahost'],
                    'url' => $hosting->getDownloadLink()
                ];
            }

            $alt->setCriteria('vid', $dt['id']);
            $list = $alt->get(['host', 'host_id']);
            if ($list) {
                $hosts = array_merge($hosts, array_column($list, 'host'));
                $hosts = array_filter($hosts);
                $hosts = array_unique($hosts);
                $hosts = array_values($hosts);
                foreach ($list as $row) {
                    $hosting->setHost($row['host']);
                    $hosting->setID($row['host_id']);
                    $downloads[] = [
                        'host' => $row['host'],
                        'url' => $hosting->getDownloadLink()
                    ];
                }
            }

            $subtitle->setCriteria('vid', $dt['id']);
            $subList = $subtitle->get(['language', 'link']);
            if (!empty($subList)) {
                foreach ($subList as $sub) {
                    $subs[] = $sub;
                }
            }

            $data[] = [
                'DT_RowId' => $dt['id'],
                'id' => $dt['id'],
                'title' => strip_tags(htmlspecialchars_decode($dt['title'])),
                'host' => array_filter($hosts),
                'status' => $dt['status'],
                'links' => $downloads,
                'actions' => [
                    'embed' => $embed_link,
                    'download' => $download_link,
                    'embed_code' => $embed_iframe,
                ],
                'subtitles' => $subs,
                'views' => $dt['views'],
                'name' => $dt['name'],
                'added' => !empty($dt['added']) ? date('M d, Y H:i', $dt['added']) : '',
                'updated' => !empty($dt['updated']) ? date('M d, Y H:i', $dt['updated']) : ''
            ];
        }

        $whereTotal = intval($userLogin['role']) === 0 ? '' : 'WHERE `uid` = ' . $userLogin['id'];
        $recordsTotal = $class->rawQuery("SELECT COUNT(`id`) FROM `tb_videos` $whereTotal")->fetchColumn();
        $recordsFiltered = $class->rawQuery("SELECT COUNT(v.`id`) FROM `tb_videos` v INNER JOIN `tb_users` u ON u.`id` = v.`uid` $where")->fetchColumn();
    }

    echo json_encode([
        'draw' => $draw,
        'data' => $data,
        'recordsTotal' => $recordsTotal,
        'recordsFiltered' => $recordsFiltered
    ]);

    $recordsTotal = null;
    $recordsFiltered = null;
    unset($recordsTotal);
    unset($recordsFiltered);
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
