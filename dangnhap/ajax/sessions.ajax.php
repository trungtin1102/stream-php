<?php
session_write_close();

require '../includes.php';

ob_start();
header('content-type:application/json');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

if (!is_admin()) {
    session_write_close();
    echo json_encode([
        'status' => 'fail',
        'message' => 'You must login first!'
    ]);
} else {
    session_write_close();
    if (!empty($_POST['action'])) {
        session_write_close();
        switch ($_POST['action']) {
            case 'delete':
                if (!empty($_POST['id'])) {
                    $class = new \Sessions();
                    $class->setCriteria('id', $_POST['id']);
                    $deleted = $class->delete();
                    if ($deleted) {
                        echo json_encode([
                            'status' => 'ok',
                            'message' => 'Data successfully deleted.'
                        ]);
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => $class->getLastError()
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Session not found!'
                    ]);
                }
                break;

            default:
                echo json_encode([
                    'status' => 'fail',
                    'message' => 'What do you want?'
                ]);
                break;
        }
    } else {
        session_write_close();
        echo json_encode([
            'status' => 'fail',
            'message' => 'What do you want?'
        ]);
    }
}

$class = new \Minify();
$output = $class->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
