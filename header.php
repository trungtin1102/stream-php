<?php
if (!defined('BASE_DIR')) exit();

session_write_close();
ob_start();

$userLogin = current_user();
$isAdmin = $userLogin && intval($userLogin['role']) === 0;
$baseURL = strtr(BASE_URL, ['https:' => '', 'http:' => '']);
$themeColor = get_option('pwa_themecolor');
$rgbColor = hex2RGB($themeColor, true, ',');
$rgbColor = $rgbColor ? $rgbColor : '103, 58, 183';
$themeColor = !empty($themeColor) ? '#' . $themeColor : '#673ab7';
$customColor = get_option('custom_color');
$customColor = !empty($customColor) ? '#' . $customColor : '#673ab7';
$customColor2 = get_option('custom_color2');
$customColor2 = !empty($customColor2) ? '#' . $customColor2 : '#3f51b5';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php if ($isSSL) : ?>
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <?php endif; ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="msapplication-TileColor" content="<?php echo $themeColor; ?>">
    <meta name="theme-color" content="<?php echo $themeColor; ?>">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <meta name="description" content="<?php echo strip_tags($sitedescription); ?>">

    <!-- facebook -->
    <meta name="og:sitename" content="<?php echo $sitename; ?>">
    <meta name="og:title" content="<?php echo strip_tags($title); ?>">
    <meta name="og:description" content="<?php echo strip_tags($sitedescription); ?>">
    <meta name="og:type" content="<?php echo $pagetype; ?>">
    <meta name="og:image" content="<?php echo $poster; ?>">

    <!-- twitter -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="<?php echo strip_tags($title); ?>">
    <meta name="twitter:description" content="<?php echo strip_tags($sitedescription); ?>">
    <meta name="twitter:image" content="<?php echo $poster; ?>">

    <link rel="manifest" href="<?php echo $baseURL; ?>manifest.webmanifest">
    <link rel="mask-icon" href="<?php echo $baseURL; ?>assets/img/maskable_icon.png" color="#ffffff">

    <link rel="shortcut icon" href="<?php echo $baseURL; ?>assets/img/logo.ico" type="image/ico">
    <link rel="apple-touch-icon" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-152x152-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-120x120-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $baseURL; ?>assets/img/apple-touch-icon-precomposed.png">

    <title><?php echo strip_tags($title) . ' - ' . $sitename; ?></title>

    <link rel="preconnect" href="//t.dtscdn.com">
    <link rel="preconnect" href="//e.dtscout.com">
    <link rel="preconnect" href="//s4.histats.com">
    <link rel="preconnect" href="//s10.histats.com">
    <link rel="preconnect" href="//tags.bluekai.com">
    <link rel="preconnect" href="//www.gstatic.com">
    <link rel="preconnect" href="//fonts.gstatic.com">
    <link rel="preconnect" href="//www.googleapis.com">
    <link rel="preconnect" href="//www.googletagmanager.com">
    <link rel="preconnect" href="//www.google.com">
    <link rel="preconnect" href="//www.google-analytics.com">
    <link rel="preconnect" href="//static.cloudflareinsights.com">
    <link rel="preconnect" href="//googleusercontent.com">
    <link rel="preconnect" href="//lh3.googleusercontent.com">
    <link rel="preconnect" href="//drive-thirdparty.googleusercontent.com">
    <link rel="preconnect" href="//static.addtoany.com">

    <link rel="preload" href="<?php echo $baseURL; ?>assets/vendor/bootstrap/css/bootstrap.min.css" as="style">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/vendor/fontawesome6/css/all.min.css" as="style">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/css/sweetalert.css" as="style">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/css/style.css" as="style">

    <link rel="preload" href="<?php echo $baseURL; ?>assets/js/jquery.min.js" as="script">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js" as="script">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/vendor/bs-custom-file-input/bs-custom-file-input.min.js" as="script">
    <link rel="preload" href="<?php echo $baseURL; ?>assets/js/sweetalert.min.js" as="script">

    <link rel="stylesheet" href="<?php echo $baseURL; ?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseURL; ?>assets/vendor/fontawesome6/css/all.min.css">
    <link rel="stylesheet" href="<?php echo $baseURL; ?>assets/css/sweetalert.css">

    <?php
    $class = new \Plugins();
    $publicCSS = $class->getPublicCSS(true);
    if ($publicCSS) {
        echo $publicCSS;
    }
    ?>

    <link rel="stylesheet" href="<?php echo $baseURL; ?>assets/css/style.css">
    <style>
        .btn-outline-custom {
            color: <?php echo $customColor; ?>;
            border-color: <?php echo $customColor; ?>;
        }

        .bg-custom,
        .btn-custom,
        .nav-pills .nav-link.active,
        .nav-pills .show>.nav-link {
            color: #fff;
            border-color: <?php echo $customColor; ?>;
            background-image: -webkit-linear-gradient(to right, <?php echo $customColor; ?>, <?php echo $customColor2; ?>);
            background-image: -moz-linear-gradient(to right, <?php echo $customColor; ?>, <?php echo $customColor2; ?>);
            background-image: -o-linear-gradient(to right, <?php echo $customColor; ?>, <?php echo $customColor2; ?>);
            background-image: linear-gradient(to right, <?php echo $customColor; ?>, <?php echo $customColor2; ?>);
            background-color: <?php echo $customColor; ?>;
        }

        .btn-outline-custom.active,
        .btn-outline-custom:active,
        .btn-outline-custom.focus,
        .btn-outline-custom:focus,
        .btn-outline-custom:not(:disabled):hover,
        .btn-custom.focus,
        .btn-custom:focus,
        .btn-custom:not(:disabled):hover {
            color: #fff !important;
            border-color: <?php echo $customColor2; ?>;
            background-image: -webkit-linear-gradient(to right, <?php echo $customColor2; ?>, <?php echo $customColor; ?>);
            background-image: -moz-linear-gradient(to right, <?php echo $customColor2; ?>, <?php echo $customColor; ?>);
            background-image: -o-linear-gradient(to right, <?php echo $customColor2; ?>, <?php echo $customColor; ?>);
            background-image: linear-gradient(to right, <?php echo $customColor2; ?>, <?php echo $customColor; ?>);
            background-color: <?php echo $customColor2; ?>;
        }

        .btn-outline-custom.focus,
        .btn-outline-custom:focus,
        .btn-outline-custom:not(:disabled):hover,
        .btn-custom.focus,
        .btn-custom:focus {
            box-shadow: 0 0 0 .2rem rgba(<?php echo $rgbColor; ?>, .2);
        }

        .table-hover tbody tr:hover td {
            background-color: rgba(<?php echo $rgbColor; ?>, .07) !important;
        }
    </style>

    <script src="<?php echo $baseURL; ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo $baseURL; ?>assets/js/js.cookie.min.js"></script>
    <script>
        var $ = jQuery.noConflict(),
            uid = <?php echo $userLogin ? $userLogin['id'] : 0; ?>,
            email = '<?php echo $userLogin ? $userLogin['email'] : ''; ?>',
            baseURL = '<?php echo BASE_URL; ?>',
            adminURL = '<?php echo $adminURL; ?>/';

        function loadScript(url) {
            var e = document.createElement("script");
            e.src = url;
            e.type = "text/javascript";
            document.getElementsByTagName("head")[0].appendChild(e);
        }
    </script>

    <?php echo include BASE_DIR . 'includes/ga.php'; ?>
    <?php echo include BASE_DIR . 'includes/gtm_head.php'; ?>
</head>

<body class="bg-light my-5 pt-2">
    <?php echo include BASE_DIR . 'includes/gtm_body.php'; ?>
    <a class="sr-only sr-only-focusable" href="#main">Skip to main content</a>
    <div class="container-lg bg-white rounded-bottom shadow">
        <header id="header">
            <nav class="navbar container-lg navbar-expand-lg navbar-dark fixed-top bg-custom shadow">
                <a class="navbar-brand" href="<?php echo $baseURL; ?>"><?php echo $sitename; ?></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse navbar-nav-scroll" id="navbarCollapse" style="max-height:calc(100vh - 56px)">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item <?php if ($page === 'home' || empty($page)) echo 'active'; ?>">
                            <a class="nav-link" href="<?php echo $baseURL; ?>">
                                <i aria-hidden="true" class="fas fa-home mr-2"></i>Home
                            </a>
                        </li>
                        <?php
                        if ($isAdmin || (filter_var(get_option('anonymous_generator'), FILTER_VALIDATE_BOOLEAN) && filter_var(get_option('enable_gsharer'), FILTER_VALIDATE_BOOLEAN))) : ?>
                            <li class="nav-item <?php if ($page === 'sharer') echo 'active'; ?>">
                                <a class="nav-link hide" href="<?php echo $baseURL; ?>sharer/" data-toggle="tooltip" data-placement="bottom" title="Google Drive Anti-Limit">
                                    <i aria-hidden="true" class="fab fa-google-drive mr-2"></i>Bypass Limit
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php echo createPluginFrontMenus('top', false); ?>
                        <?php if ($userLogin === false) : ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo $adminURL; ?>/login/">
                                    <i aria-hidden="true" class="fas fa-sign-in-alt mr-2"></i>Login
                                </a>
                            </li>
                            <?php if (!file_exists(BASE_DIR . '.rent') && !filter_var(get_option('disable_registration'), FILTER_VALIDATE_BOOLEAN)) : ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo $adminURL; ?>/register/">
                                        <i aria-hidden="true" class="fas fa-user-plus mr-2"></i>Register
                                    </a>
                                </li>
                            <?php endif; ?>
                        <?php else : ?>
                            <?php echo createPluginFrontMenus('top', true); ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="upanel" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i aria-hidden="true" class="fas fa-user-circle mr-2"></i>User Panel
                                </a>
                                <div class="dropdown-menu dropdown-menu-right shadow border-0" aria-labelledby="upanel">
                                    <a class="dropdown-item" href="<?php echo $adminURL; ?>/dashboard" rel="noopener">
                                        <i aria-hidden="true" class="fas fa-tachometer-alt mr-2"></i>Dashboard
                                    </a>
                                    <a class="dropdown-item" href="<?php echo $adminURL; ?>/videos" rel="noopener">
                                        <i aria-hidden="true" class="fas fa-film mr-2"></i>My Videos
                                    </a>
                                    <a class="dropdown-item" href="<?php echo $adminURL; ?>/profile" rel="noopener">
                                        <i aria-hidden="true" class="fas fa-user mr-2"></i>My Account
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="javascript:void(0)" onclick="location.href='<?php echo $adminURL; ?>/logout.php';" rel="noopener">
                                        <i aria-hidden="true" class="fas fa-sign-out-alt mr-2"></i>Logout
                                    </a>
                                </div>
                            </li>
                        <?php endif; ?>
                        <?php
                        $dev = parse_url($baseURL, PHP_URL_HOST);
                        if (strpos($dev, 'TvHay.') !== false || strpos($dev, 'localhost') !== false) : ?>
                            <li class="nav-item">
                                <a class="nav-link" href="https://TvHay.tv/">
                                    <i aria-hidden="true" class="fas fa-tv mr-2"></i>Live TV
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="btn btn-block btn-green dropdown-toggle" href="#" id="buy" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i aria-hidden="true" class="fas fa-shopping-basket mr-2"></i>Buy
                                </a>
                                <div class="dropdown-menu dropdown-menu-right shadow border-0" aria-labelledby="buy">
                                    <a class="dropdown-item" href="<?php echo $baseURL; ?>buy/" rel="noopener">
                                        <i aria-hidden="true" class="fas fa-shopping-basket mr-2"></i>Buy Source Code
                                    </a>
                                    <a class="dropdown-item" href="<?php echo $baseURL; ?>buy-additional-host/" rel="noopener">
                                        <i aria-hidden="true" class="fas fa-plus-circle mr-2"></i>Buy Additional Hosts
                                    </a>
                                    <a class="dropdown-item" href="<?php echo $baseURL; ?>rent/" rel="noopener">
                                        <i aria-hidden="true" class="fas fa-server mr-2"></i>Rent Server
                                    </a>
                                </div>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </nav>
        </header>
        <main id="main" role="main">