-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 5.7.24 - MySQL Community Server (GPL)
-- OS Server:                    Win64
-- HeidiSQL Versi:               10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- membuang struktur untuk table db_videoplayer.tb_gdrive_auth
DROP TABLE IF EXISTS `tb_gdrive_auth`;
CREATE TABLE IF NOT EXISTS `tb_gdrive_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `api_key` varchar(50) NOT NULL,
  `client_id` varchar(100) NOT NULL,
  `client_secret` varchar(50) NOT NULL,
  `refresh_token` varchar(150) NOT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL DEFAULT '0',
  `uid` int(11) NOT NULL DEFAULT '1',
  `status` int(1) NOT NULL DEFAULT '1',
  `project_number` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Pengeluaran data tidak dipilih.

-- membuang struktur untuk table db_videoplayer.tb_gdrive_mirrors
DROP TABLE IF EXISTS `tb_gdrive_mirrors`;
CREATE TABLE IF NOT EXISTS `tb_gdrive_mirrors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gdrive_id` varchar(50) NOT NULL,
  `mirror_id` varchar(50) NOT NULL,
  `mirror_email` varchar(100) NOT NULL,
  `added` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Pengeluaran data tidak dipilih.

-- membuang struktur untuk table db_videoplayer.tb_loadbalancers
DROP TABLE IF EXISTS `tb_loadbalancers`;
CREATE TABLE IF NOT EXISTS `tb_loadbalancers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `link` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `public` int(1) NOT NULL DEFAULT '0',
  `added` int(11) NOT NULL,
  `updated` int(11) DEFAULT NULL,
  `histats` varchar(50) DEFAULT NULL,
  `disallow_hosts` text,
  `load_ram` int(3) DEFAULT '0',
  `load_cpu` int(3) DEFAULT '0',
  `load` int(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Pengeluaran data tidak dipilih.

-- membuang struktur untuk table db_videoplayer.tb_plugins
DROP TABLE IF EXISTS `tb_plugins`;
CREATE TABLE IF NOT EXISTS `tb_plugins` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` varchar(150) NOT NULL,
  `value` text,
  `updated` int(15) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Pengeluaran data tidak dipilih.

-- membuang struktur untuk table db_videoplayer.tb_sessions
DROP TABLE IF EXISTS `tb_sessions`;
CREATE TABLE IF NOT EXISTS `tb_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` text NOT NULL,
  `useragent` varchar(250) NOT NULL,
  `created` int(15) NOT NULL,
  `username` varchar(50) NOT NULL,
  `expired` int(15) NOT NULL,
  `token` varchar(250) NOT NULL,
  `stat` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Pengeluaran data tidak dipilih.

-- membuang struktur untuk table db_videoplayer.tb_settings
DROP TABLE IF EXISTS `tb_settings`;
CREATE TABLE IF NOT EXISTS `tb_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(150) NOT NULL,
  `value` text,
  `updated` int(15) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Pengeluaran data tidak dipilih.

-- membuang struktur untuk table db_videoplayer.tb_stats
DROP TABLE IF EXISTS `tb_stats`;
CREATE TABLE IF NOT EXISTS `tb_stats` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vid` bigint(20) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `ua` text NOT NULL,
  `created` int(15) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Pengeluaran data tidak dipilih.

-- membuang struktur untuk table db_videoplayer.tb_subtitles
DROP TABLE IF EXISTS `tb_subtitles`;
CREATE TABLE IF NOT EXISTS `tb_subtitles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(25) NOT NULL,
  `link` text NOT NULL,
  `vid` int(11) NOT NULL,
  `added` int(15) NOT NULL,
  `uid` int(11) NOT NULL,
  `order` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Pengeluaran data tidak dipilih.

-- membuang struktur untuk table db_videoplayer.tb_subtitle_manager
DROP TABLE IF EXISTS `tb_subtitle_manager`;
CREATE TABLE IF NOT EXISTS `tb_subtitle_manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `file_size` int(11) NOT NULL,
  `file_type` varchar(25) NOT NULL,
  `language` varchar(50) DEFAULT NULL,
  `added` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `host` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Pengeluaran data tidak dipilih.

-- membuang struktur untuk table db_videoplayer.tb_users
DROP TABLE IF EXISTS `tb_users`;
CREATE TABLE IF NOT EXISTS `tb_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(500) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` int(1) NOT NULL,
  `added` int(15) NOT NULL,
  `updated` int(15) DEFAULT NULL,
  `role` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user` (`user`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Pengeluaran data tidak dipilih.

-- membuang struktur untuk table db_videoplayer.tb_videos
DROP TABLE IF EXISTS `tb_videos`;
CREATE TABLE IF NOT EXISTS `tb_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1000) DEFAULT NULL,
  `host` varchar(50) NOT NULL,
  `host_id` varchar(1500) DEFAULT NULL,
  `ahost` varchar(50) NOT NULL,
  `ahost_id` varchar(1500) DEFAULT NULL,
  `uid` int(11) NOT NULL,
  `added` int(15) NOT NULL,
  `updated` int(15) NOT NULL,
  `poster` varchar(1500) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `fid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Pengeluaran data tidak dipilih.

-- membuang struktur untuk table db_videoplayer.tb_videos_alternatives
DROP TABLE IF EXISTS `tb_videos_alternatives`;
CREATE TABLE IF NOT EXISTS `tb_videos_alternatives` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vid` bigint(20) NOT NULL,
  `host` varchar(50) NOT NULL,
  `host_id` varchar(1500) NOT NULL,
  `order` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Pengeluaran data tidak dipilih.

-- membuang struktur untuk table db_videoplayer.tb_videos_hash
DROP TABLE IF EXISTS `tb_videos_hash`;
CREATE TABLE IF NOT EXISTS `tb_videos_hash` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `host` varchar(50) NOT NULL,
  `host_id` varchar(1500) DEFAULT NULL,
  `gdrive_email` varchar(250) DEFAULT NULL,
  `hash_host` varchar(100) DEFAULT NULL,
  `hash_id` varchar(3000) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Pengeluaran data tidak dipilih.

-- membuang struktur untuk table db_videoplayer.tb_videos_short
DROP TABLE IF EXISTS `tb_videos_short`;
CREATE TABLE IF NOT EXISTS `tb_videos_short` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) NOT NULL,
  `vid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Pengeluaran data tidak dipilih.

-- membuang struktur untuk table db_videoplayer.tb_videos_sources
DROP TABLE IF EXISTS `tb_videos_sources`;
CREATE TABLE IF NOT EXISTS `tb_videos_sources` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `host` varchar(50) NOT NULL,
  `host_id` varchar(1500) NOT NULL,
  `data` text NOT NULL,
  `dl` tinyint(1) NOT NULL DEFAULT '0',
  `sid` int(11) DEFAULT '0',
  `created` int(15) NOT NULL DEFAULT '0',
  `expired` int(15) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Pengeluaran data tidak dipilih.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
