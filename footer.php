<?php
if (!defined('BASE_DIR')) exit();

session_write_close();

$dmca_link = get_option('dmca_page_link');
$contact_link = get_option('contact_page_link');
?>
</main>
<footer id="footer" class="row mt-5 py-5 bg-dark text-center text-white rounded-bottom">
    <div class="col-12">
        <ul class="nav justify-content-center mb-3">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo $baseURL; ?>">Home</a>
            </li>
            <?php echo createPluginFrontMenus('bottom', false); ?>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo $baseURL; ?>changelog">Change Log</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo $baseURL; ?>terms">Terms</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo $baseURL; ?>privacy">Privacy</a>
            </li>
            <?php if (!empty($dmca_link)) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $dmca_link; ?>" target="_blank" rel="noopener">DMCA</a>
                </li>
            <?php endif; ?>
            <?php if (!empty($contact_link)) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $contact_link; ?>" target="_blank" rel="noopener">Contact</a>
                </li>
            <?php endif; ?>
        </ul>
        <p>&copy; 2020 - <?php echo date('Y'); ?>. Developed by <?php echo sitename(); ?>.</p>
    </div>
</footer>
</div>
<button id="gotoTop" class="bg-custom shadow">
    <span class="gotoContent">
        <i class="fas fa-chevron-up"></i>
    </span>
</button>
<script src="<?php echo $baseURL; ?>assets/js/pwacompat.min.js" defer></script>
<script src="<?php echo $baseURL; ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js" defer></script>
<script src="<?php echo $baseURL; ?>assets/vendor/bs-custom-file-input/bs-custom-file-input.min.js" defer></script>
<script src="<?php echo $baseURL; ?>assets/js/sweetalert.min.js" defer></script>
<script src="<?php echo $baseURL; ?>assets/vendor/crypto-js/crypto-js.js"></script>
<script src="<?php echo $baseURL; ?>assets/vendor/crypto-js/aes.js"></script>
<script src="<?php echo $baseURL; ?>assets/js/app.js"></script>
<?php include 'includes/recaptcha.php'; ?>
<script>
    $(document).ready(function() {
        bsCustomFileInput.init();

        Cookies.remove('VideoSettings');
        Cookies.remove('GeneralSettings');
        Cookies.remove('PublicSettings');
        Cookies.remove('WebSettings');
        Cookies.remove('SmtpSettings');
        Cookies.remove('MiscSettings');
        Cookies.remove('SLinkSettings');
        Cookies.remove('AdsSettings');

        $('.carousel').carousel({
            interval: 5000,
            pause: 'hover'
        });

        $('.btn-toggle-upload').click(function() {
            $('#posterUpload').toggleClass('d-none');
            $('#posterURL').toggleClass('d-none');
        });

        loadTooltip();

        var showIDFormat = localStorage.getItem('hostIDFormat');
        if (showIDFormat) {
            $('#hostIDFormat').collapse('show');
        }
        $('#hostIDFormat').on('shown.bs.collapse', function() {
            localStorage.setItem('hostIDFormat', true);
        }).on('hidden.bs.collapse', function() {
            localStorage.removeItem('hostIDFormat', true);
        });

        $(window).on('scroll', function() {
            var g = $("#gotoTop");
            if (document.body.scrollTop > 640 || document.documentElement.scrollTop > 480) {
                g.fadeIn();
            } else {
                g.fadeOut();
            }
        });

        $('#gotoTop').on('click', function() {
            $('html,body').animate({
                scrollTop: 0
            }, 'slow');
        });

        $('.btn-info').click(function() {
            $('#hostIDFormat').collapse('show');
            $('html,body').animate({
                scrollTop: $('#hostIDFormat').offset().top - 50
            }, 'slow');
        });

        $('#frmCreatePlayer').on('submit', function(e) {
            e.preventDefault();

            var $frm = $('#frmCreatePlayer'),
                data = new FormData($frm[0]),
                $btn = $(this).find('#submit'),
                btnText = $btn.html(),
                spinner = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...',
                $result = $('#createPlayerResult');

            $btn.html(spinner).prop('disabled', true);
            $result.addClass('d-none');
            $('#embedIframe').attr('src', '');

            if ($('#captcha-response').length && $('#captcha-response').val() === '') {
                swal({
                    title: 'Error!',
                    text: 'Invalid Captcha! Load a new captcha.',
                    type: 'warning',
                    showCancelButton: true,
                    showLoaderOnConfirm: true,
                    cancelButtonClass: "btn-secondary",
                    confirmButtonClass: "btn-primary",
                    closeOnConfirm: false
                }, function(isConfirm) {
                    if (!isConfirm) {
                        $btn.html(btnText).prop('disabled', false);
                    } else {
                        grecaptcha.reset();
                        grecaptcha.execute().then(function() {
                            $frm.trigger('submit');
                            swal('Success', 'The form has been resubmitted!', 'success');
                        });
                    }
                });
            } else {
                data.append('action', 'createPlayer');
                $.ajax({
                    type: 'POST',
                    url: '<?php echo $baseURL; ?>ajax/',
                    contentType: false,
                    processData: false,
                    cache: false,
                    data: data,
                    complete: function() {
                        $btn.html(btnText).prop('disabled', false);
                    },
                    success: function(res) {
                        if (res.status === 'success') {
                            $('#txtEmbed').val(res.result.embed_link);
                            $('#txtEmbedCode').val(res.result.embed_code);
                            $('#txtDl').val(res.result.download_link);
                            $('#txtReq').val(res.result.embed2_link);
                            $('#txtJson').val(res.result.api_link);
                            $('#embedIframe').attr('src', res.result.embed_link);
                            $result.removeClass('d-none');
                            $frm[0].reset();
                        } else {
                            swal('Error!', res.message, 'error');
                        }
                        if (typeof grecaptcha !== 'undefined') {
                            grecaptcha.reset();
                            grecaptcha.execute();
                        }
                    },
                    error: function() {
                        swal('Error!', 'Cannot create player!', 'error');
                    }
                });
            }
        });
    });

    function loadTooltip() {
        $('[data-toggle="tooltip"]').tooltip({
            container: 'body'
        });
        $('[data-toggle="tooltip"]').on('show.bs.tooltip', function() {
            $('body > [role="tooltip"]').remove();
        });
    }

    function addSomeAlternative() {
        swal({
                title: "Info",
                text: "You can add some alternative video urls from the user page!",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-primary",
                confirmButtonText: "OK",
                cancelButtonClass: "btn-danger",
                cancelButtonText: "Cancel",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) location.href = '<?php echo $adminURL; ?>/videos/new/';
            });
    }

    function uploadSubtitle(e) {
        var $ig = e.closest('.input-group');
        $ig.find('#sub-type-' + e.data('index')).val('file');
        $ig.find('input.subtitle').replaceWith('<div class="custom-file"><input type="file" id="sub-' + e.data('index') + '" name="sub-file[]" class="custom-file-input subtitle" accept=".srt, .vtt, .ass, .sub, .stl, .dfxp, .ttml, .sbv, .txt"><label class="custom-file-label" for="sub-' + e.data('index') + '">Choose file (.srt, .vtt, .ass, .sub, .stl, .dfxp, .ttml, .sbv, .txt)</label></div>');
        e.replaceWith('<button type="button" class="btn btn-outline-primary" data-toggle="tooltip" title="Insert Subtitle URL" aria-label="Upload Subtitle" onclick="insertSubtitleURL($(this))" data-index="' + e.data('index') + '"><i aria-hidden="true" class="fas fa-link"></i></button>');
        loadTooltip();
        bsCustomFileInput.init();
    }

    function insertSubtitleURL(e) {
        var $ig = e.closest('.input-group');
        $ig.find('#sub-type-' + e.data('index')).val('url');
        $ig.find('.custom-file').replaceWith('<input type="text" name="sub-url[]" class="form-control subtitle" placeholder="Subtitle URL (.srt, .vtt, .ass, .sub, .stl, .dfxp, .ttml, .sbv, .txt)">');
        e.replaceWith('<button type="button" class="btn btn-outline-primary" data-toggle="tooltip" title="Upload Subtitle" aria-label="Upload Subtitle" onclick="uploadSubtitle($(this))" data-index="' + e.data('index') + '"><i aria-hidden="true" class="fas fa-upload"></i></button>');
        loadTooltip();
    }

    function addSubtitle() {
        var $cs = $('#subsWrapper'),
            $fg = $cs.find('.form-group'),
            langs = '<?php echo subtitle_languages("lang[]", ""); ?>',
            html = '<div class="form-group" data-index="' + $fg.length + '"><div class="input-group"><div class="input-group-prepend">' + langs + '<input type="hidden" id="sub-type-' + $fg.length + '" name="sub-type[]" value="url"></div><input type="text" name="sub-url[]" class="form-control subtitle" placeholder="Subtitle URL (.srt, .vtt, .ass, .sub, .stl, .dfxp, .ttml, .sbv, .txt)"><div class="input-group-append"><button type="button" class="btn btn-outline-primary" data-toggle="tooltip" aria-label="Upload Subtitle" onclick="uploadSubtitle($(this))" data-index="' + $fg.length + '" title="Upload Subtitle"><i aria-hidden="true" class="fas fa-upload"></i></button><button type="button" class="btn btn-outline-danger" data-toggle="tooltip" title="Remove Subtitle" data-index="' + $fg.length + '" aria-label="Remove Subtitle" onclick="removeSubtitle($(this))"><i aria-hidden="true" class="fas fa-minus"></i></button></div></div></div>';

        loadTooltip();

        if ($cs.find('.form-group').length < 10) $cs.append(html);
        else swal('Warning!', 'Only 10 subtitles are allowed.', 'warning');
    }

    function removeSubtitle(e) {
        $('#subsWrapper .form-group[data-index="' + e.data('index') + '"]').remove();
    }

    function ajaxPOST(url, data, sCallback, eCallback) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: sCallback,
            error: eCallback
        });
    }

    function require(url) {
        var e = document.createElement("script");
        e.src = url;
        e.type = "text/javascript";
        document.getElementsByTagName("head")[0].appendChild(e);
    }
</script>
<!-- Start Google Drive Bypass Limit -->
<script>
    <?php echo !empty($jsCode) ? $jsCode : ''; ?>
</script>
<!-- End Google Drive Bypass Limit -->
<?php
$class = new \Plugins();
$adminJS = $class->getPublicJS(true);
if ($adminJS) {
    echo $adminJS;
}
echo include 'includes/share.php';
echo include 'includes/histats.php';
?>
<?php echo html_entity_decode(get_option('chat_widget')); ?>
<script>
    if ("serviceWorker" in navigator) {
        navigator.serviceWorker
            .register('<?php echo $baseURL; ?>sw.js')
            .then(function(res) {
                console.log("service worker registered")
            })
            .catch(function(err) {
                console.log("service worker not registered", err)
            })
    }
</script>
</body>

</html>
<?php
$min = new \Minify();
$output = $min->minify_html(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
