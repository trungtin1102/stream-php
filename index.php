<?php
session_write_close();

require 'includes.php';

$adminDir = admin_dir();
$pageWithHF = ['home', 'rent', 'buy', 'changelog', 'privacy', 'sharer', 'terms', 'buy-additional-host'];
$uri = get_page_uris();
$page = !empty($uri[0]) ? trim(pathinfo($uri[0], PATHINFO_FILENAME)) : 'home';

if($page == 'em'){
    include BASE_DIR . 'embed/index.php';
}
elseif ($page === 'subtitles') {
    // internal subtitle
    header('Location: ' . strtr(BASE_URL, [BASE_URL => BASE_URL . 'uploads/']) . ltrim($_SERVER['REQUEST_URI'], '/'));
    exit('Redirecting...');
} elseif ($page === $adminDir) {
    // admin page
    include BASE_DIR . $adminDir . '/index.php';
} elseif (!in_array($page, $pageWithHF) && file_exists(BASE_DIR . 'includes/pages/' . $page . '.php')) {
    // copy stream page
    include BASE_DIR . 'includes/pages/' . $page . '.php';
} else {
    // public page
    $isSSL = isSSL();
    $title = '';
    $poster = '';
    $pagetype = 'website';
    $sitename = strip_tags(get_option('site_name'));
    $siteslogan = '';
    $sitedescription = '';
    $adminURL = BASE_URL . $adminDir;
    if ($page === 'p') {
        // plugin page
        if (isset($uri[2]) && file_exists(BASE_DIR . "plugins/$uri[1]/public/views/$uri[2].php")) {
            include BASE_DIR . "plugins/$uri[1]/public/views/$uri[2].php";
        } elseif (isset($uri[1]) && file_exists(BASE_DIR . "plugins/$uri[1]/public/views/$uri[1].php")) {
            include BASE_DIR . "plugins/$uri[1]/public/views/$uri[1].php";
        } elseif (file_exists(BASE_DIR . 'includes/pages/' . $page . '.php')) {
            include BASE_DIR . 'includes/pages/' . $page . '.php';
        } else {
            http_response_code(404);
        }
    } elseif ($page !== 'home' && file_exists(BASE_DIR . 'includes/pages/' . $page . '.php')) {
        include BASE_DIR . 'includes/pages/' . $page . '.php';
    } else {
        // home page
        $currentUser = current_user();
        if (file_exists(BASE_DIR . '.rent') && !$currentUser) {
            http_response_code(404);
            echo '<div style="text-align:center">Silence is golden!</div>';
        } else {
            $mainSite = get_option('main_site');
            $mainHost = ltrim(parse_url($mainSite, PHP_URL_HOST), 'www.');
            $allowAnonymous = filter_var(get_option('anonymous_generator'), FILTER_VALIDATE_BOOLEAN);
            $isBalancer = is_load_balancer();
            if ($allowAnonymous || $currentUser) {
                $showBalancer = show_public_balancer();
                $thisHost = ltrim(parse_url(BASE_URL, PHP_URL_HOST), 'www.');
                if ((!empty($mainSite) && $mainHost === $thisHost) || ($isBalancer && $showBalancer)) {
                    include BASE_DIR . 'includes/pages/home.php';
                } elseif (!empty($mainSite)) {
                    header("location: $mainSite");
                    exit('Redirecting...');
                } else {
                    http_response_code(404);
                    echo '<div style="text-align:center">Silence is golden!</div>';
                }
            } elseif (!empty($mainSite) && validate_url($mainSite) && $isBalancer) {
                header("location: $mainSite");
                exit('Redirecting...');
            } else {
                http_response_code(404);
                echo '<div style="text-align:center">Silence is golden!</div>';
            }
        }
    }
}
