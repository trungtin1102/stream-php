<?php
session_write_close();

if (floatval(substr(PHP_VERSION, 0, 3)) < 7.3) die('The PHP version you are using is not supported. Use PHP 7.3+.');

if (session_status() === PHP_SESSION_NONE) {
    session_start(array(
        'read_and_close' => true
    ));
}

$baseDir = dirname(__FILE__) . '/';
require $baseDir . 'vendor/autoload.php';
require $baseDir . 'includes/config.php';
require $baseDir . 'includes/conn.php';
require $baseDir . 'includes/functions.php';
require $baseDir . 'includes/plugins.php';


session_write_close();
