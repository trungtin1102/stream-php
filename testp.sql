-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost
-- Thời gian đã tạo: Th12 13, 2022 lúc 05:59 AM
-- Phiên bản máy phục vụ: 10.4.12-MariaDB-log
-- Phiên bản PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `testp`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_gdrive_auth`
--

CREATE TABLE `tb_gdrive_auth` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `api_key` varchar(50) NOT NULL,
  `client_id` varchar(100) NOT NULL,
  `client_secret` varchar(50) NOT NULL,
  `refresh_token` varchar(150) NOT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL DEFAULT 0,
  `uid` int(11) NOT NULL DEFAULT 1,
  `status` int(1) NOT NULL DEFAULT 1,
  `project_number` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_gdrive_mirrors`
--

CREATE TABLE `tb_gdrive_mirrors` (
  `id` bigint(20) NOT NULL,
  `gdrive_id` varchar(50) NOT NULL,
  `mirror_id` varchar(50) NOT NULL,
  `mirror_email` varchar(100) NOT NULL,
  `added` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_gdrive_queue`
--

CREATE TABLE `tb_gdrive_queue` (
  `id` bigint(20) NOT NULL,
  `gdrive_id` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_hls_videos`
--

CREATE TABLE `tb_hls_videos` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `folder` varchar(255) DEFAULT NULL,
  `embed_code` varchar(255) DEFAULT NULL,
  `embed_link` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT 0,
  `poster` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `tb_hls_videos`
--

INSERT INTO `tb_hls_videos` (`id`, `title`, `folder`, `embed_code`, `embed_link`, `status`, `views`, `poster`, `updated`) VALUES
(5, 'zzz', 'zzz', 'f3abb86bd34cf4d52698f14c0da1dc60', NULL, NULL, 0, NULL, NULL),
(6, 'thien quang', 'thien-quang', 'e3331199b28d54517eb04df2a4653296', NULL, NULL, 0, NULL, NULL),
(7, 'qq', 'qq', '099b3b060154898840f0ebdfb46ec78f', NULL, NULL, 0, NULL, NULL),
(8, 'fff', 'fff', '343d9040a671c45832ee5381860e2996', NULL, NULL, 0, NULL, NULL),
(9, 'TTT', 'ttt', '9990775155c3518a0d7917f7780b24aa', NULL, NULL, 0, NULL, NULL),
(10, 'errr', 'errr', 'f757382eeb95b07b792bb2e7195468b8', NULL, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_loadbalancers`
--

CREATE TABLE `tb_loadbalancers` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `link` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `public` int(1) NOT NULL DEFAULT 0,
  `added` int(11) NOT NULL,
  `updated` int(11) NOT NULL DEFAULT 0,
  `disallow_hosts` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_plugins`
--

CREATE TABLE `tb_plugins` (
  `id` bigint(20) NOT NULL,
  `key` varchar(150) NOT NULL,
  `value` text DEFAULT NULL,
  `updated` int(15) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_sessions`
--

CREATE TABLE `tb_sessions` (
  `id` int(11) NOT NULL,
  `ip` text DEFAULT NULL,
  `useragent` varchar(250) NOT NULL,
  `created` int(15) NOT NULL,
  `username` varchar(50) NOT NULL,
  `expired` int(15) NOT NULL,
  `token` varchar(250) NOT NULL,
  `stat` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tb_sessions`
--

INSERT INTO `tb_sessions` (`id`, `ip`, `useragent`, `created`, `username`, `expired`, `token`, `stat`) VALUES
(1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 1668598746, 'admin', 0, '', 0),
(2, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 1668599036, 'admin', 1668685436, 'de5627a8e61207a076369997be87301eee4fd5cd', 0),
(3, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', 1670468582, 'admin', 0, '', 0),
(4, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', 1670468590, 'admin', 1670554990, 'efbf601581348e7a81b0479d1846cdfcc0c36ae3', 0),
(5, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', 1670669039, 'admin', 1670755439, '84225e9f4ee202abd03f14ad1f535d02152b0ba7', 0),
(6, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', 1670763545, 'admin', 1670849945, 'e583eca7e9bc8bbf4422313b95107e21750140ac', 0),
(7, '115.78.8.20', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', 1670826341, 'admin', 1670912741, 'b0c615fd946f191e7eb85656ec90f88ebe07eb5f', 0),
(8, '113.161.95.239', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', 1670832316, 'admin', 1670918716, '90a499244c2ca6256b2b541f0551bc2e41b710c2', 0),
(9, '2402:800:6379:316d:8c68:cec2:2a88:99e3', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', 1670861754, 'admin', 1670948154, 'f56e38a5dfe0ec0d4a356c8511e6b979371f345d', 0),
(10, '115.79.5.170', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', 1670919756, 'admin', 1671006156, 'd0a7524486f21d35b70e417cf9c7b71243a914c8', 0),
(11, '113.161.95.239', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', 1670924881, 'admin', 1671011281, '7f8c27ab58e06c82dbc35a9ff426fbcc62f27491', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_settings`
--

CREATE TABLE `tb_settings` (
  `id` int(11) NOT NULL,
  `key` varchar(150) NOT NULL,
  `value` text DEFAULT NULL,
  `updated` int(15) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tb_settings`
--

INSERT INTO `tb_settings` (`id`, `key`, `value`, `updated`) VALUES
(1, 'SECURE_SALT', 'kbDX-', 1668599143),
(2, 'updated', '35', 1668599143),
(3, 'db_engine', 'InnoDB', 1668599144),
(4, 'repaired', '1670864391', 1670864391),
(5, 'player', 'jwplayer_latest', 1670763708),
(6, 'player_skin', '', 1670763709),
(7, 'player_color', '673AB7', 1670763709),
(8, 'stretching', 'uniform', 1670763709),
(9, 'preload', 'auto', 1670763709),
(10, 'default_resolution', '100', 1670763709),
(11, 'default_subtitle', 'Default', 1670763709),
(12, 'subtitle_color', 'ffff00', 1670763709),
(13, 'autoplay', 'true', 1670763709),
(14, 'mute', '', 1670763709),
(15, 'repeat', '', 1670763709),
(16, 'display_title', '', 1670763709),
(17, 'playback_rate', '', 1670763709),
(18, 'enable_share_button', '', 1670763709),
(19, 'enable_download_button', '', 1670763709),
(20, 'continue_watching', '', 1670763709),
(21, 'poster', '', 1670763709),
(22, 'small_logo_file', '', 1670763709),
(23, 'small_logo_link', '', 1670763709),
(24, 'logo_file', '', 1670763709),
(25, 'logo_open_link', '', 1670763709),
(26, 'logo_position', 'top-right', 1670763709),
(27, 'logo_margin', '8', 1670763709),
(28, 'logo_hide', '', 1670763709),
(29, 'p2p', '', 1670763709),
(30, 'torrent_tracker', 'wss://tracker.openwebtorrent.com\r\nwss://spacetradersapi-chatbox.herokuapp.com:443/announce\r\nwss://tracker.btorrent.xyz/', 1670763709),
(31, 'text_title', 'Watch {title} - {siteName}', 1670763709),
(32, 'text_loading', 'Please wait...', 1670763709),
(33, 'text_download', 'Download {title}', 1670763709),
(34, 'text_resume', 'Welcome back! You left off at <span id=\"timez\"></span>. Would you like to resume watching?', 1670763709),
(35, 'text_resume_yes', 'Yes, Please', 1670763709),
(36, 'text_resume_no', 'No, Thanks', 1670763709),
(37, 'text_rewind', 'Rewind 10 Seconds', 1670763709),
(38, 'text_forward', 'Forward 10 Seconds', 1670763709);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_stats`
--

CREATE TABLE `tb_stats` (
  `id` bigint(20) NOT NULL,
  `vid` bigint(20) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `ua` text NOT NULL,
  `created` int(15) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_subtitles`
--

CREATE TABLE `tb_subtitles` (
  `id` int(11) NOT NULL,
  `language` varchar(25) NOT NULL,
  `link` text NOT NULL,
  `vid` int(11) NOT NULL,
  `added` int(15) NOT NULL,
  `uid` int(11) NOT NULL,
  `order` int(5) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_subtitle_manager`
--

CREATE TABLE `tb_subtitle_manager` (
  `id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_size` int(11) NOT NULL,
  `file_type` varchar(25) NOT NULL,
  `language` varchar(50) NOT NULL DEFAULT 'Default',
  `added` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `host` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_users`
--

CREATE TABLE `tb_users` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(500) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` int(1) NOT NULL,
  `added` int(15) NOT NULL,
  `updated` int(15) NOT NULL DEFAULT 0,
  `role` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tb_users`
--

INSERT INTO `tb_users` (`id`, `user`, `email`, `password`, `name`, `status`, `added`, `updated`, `role`) VALUES
(1, 'admin', 'admin@TvHay.top', '$2y$10$qnWXyHmEU0rTR8B84djhlOdqiOZtJ3/OATTWb.bgZ6yLb55duk74e', 'Admin', 1, 0, 1642938985, 0),
(2, 'demo', 'demo@TvHay.top', '$2y$10$CmeA6fPZckFBp2pnZlpPIOs8KSFxAC.BQtjImAPBTBu6m4D.qfWzm', 'Demo', 1, 0, 1630917289, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_videos`
--

CREATE TABLE `tb_videos` (
  `id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `folder` varchar(255) DEFAULT NULL,
  `embed_code` varchar(255) DEFAULT NULL,
  `host` varchar(50) NOT NULL,
  `host_id` varchar(1500) NOT NULL,
  `ahost` varchar(50) DEFAULT NULL,
  `ahost_id` varchar(1500) DEFAULT NULL,
  `uid` int(11) NOT NULL,
  `added` int(15) NOT NULL,
  `updated` int(15) NOT NULL,
  `poster` varchar(1500) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `views` int(11) NOT NULL DEFAULT 0,
  `fid` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tb_videos`
--

INSERT INTO `tb_videos` (`id`, `title`, `folder`, `embed_code`, `host`, `host_id`, `ahost`, `ahost_id`, `uid`, `added`, `updated`, `poster`, `status`, `views`, `fid`) VALUES
(1, 'test video 1', NULL, NULL, 'direct', 'https://hp-streamcloud.s45k.s3-hcm-r1.longvan.net/tu-ma-y.mp4', '', '', 1, 1668599129, 1668599129, NULL, 0, 0, 0),
(2, 'test link youtube', NULL, NULL, 'youtube', '7GqClqvlObY', '', '', 1, 1668601692, 1668601692, NULL, 0, 0, 0),
(3, 'test video', NULL, NULL, 'gdrive', '1Xio80goOIZA4yJ8TaAIvRGbeFfxoqUZy', '', '', 1, 1670468826, 1670469471, NULL, 0, 0, 0),
(4, 'test video nè', NULL, NULL, 'gdrive', '1Xio80goOIZA4yJ8TaAIvRGbeFfxoqUZy', '', '', 1, 1670763632, 1670763632, 'hinh-nen-cong-nghe-K-5DJn_IjQBothd.jpg', 0, 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_videos_alternatives`
--

CREATE TABLE `tb_videos_alternatives` (
  `id` bigint(20) NOT NULL,
  `vid` bigint(20) NOT NULL,
  `host` varchar(50) NOT NULL,
  `host_id` varchar(1500) NOT NULL,
  `order` int(5) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_videos_hash`
--

CREATE TABLE `tb_videos_hash` (
  `id` bigint(20) NOT NULL,
  `host` varchar(50) NOT NULL,
  `host_id` varchar(1500) NOT NULL,
  `gdrive_email` varchar(250) DEFAULT NULL,
  `hash_host` varchar(100) NOT NULL,
  `hash_id` varchar(3000) NOT NULL,
  `data` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tb_videos_hash`
--

INSERT INTO `tb_videos_hash` (`id`, `host`, `host_id`, `gdrive_email`, `hash_host`, `hash_id`, `data`) VALUES
(1, 'youtube', '7GqClqvlObY', '', '23b6575198994d018b10760a1ed8588b', '9f7ec8098448951d4d3a4492a2d604b2', NULL),
(2, 'gdrive', '1Xio80goOIZA4yJ8TaAIvRGbeFfxoqUZy', '', '85f9fac63744712fe2ed9802526bb197', '2db483780a299ae31f30eaf45c68e733', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_videos_short`
--

CREATE TABLE `tb_videos_short` (
  `id` int(11) NOT NULL,
  `key` varchar(50) NOT NULL,
  `vid` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tb_videos_short`
--

INSERT INTO `tb_videos_short` (`id`, `key`, `vid`) VALUES
(1, '07ee022b-0b2d-45ae-925a-57dac4ea7450', 1),
(2, '233a998e-921a-4ce6-b9ad-22256e0764a9', 2),
(3, '728be7a2-66c1-435e-ab1b-3aedfec76dce', 3),
(4, 'eb887589-c9a3-42e7-9746-a7d9b01f3c75', 4);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_videos_sources`
--

CREATE TABLE `tb_videos_sources` (
  `id` bigint(20) NOT NULL,
  `host` varchar(50) NOT NULL,
  `host_id` varchar(1500) NOT NULL,
  `data` text NOT NULL,
  `dl` tinyint(1) NOT NULL DEFAULT 0,
  `sid` int(11) DEFAULT 0,
  `created` int(15) NOT NULL DEFAULT 0,
  `expired` int(15) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tb_gdrive_auth`
--
ALTER TABLE `tb_gdrive_auth`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Chỉ mục cho bảng `tb_gdrive_mirrors`
--
ALTER TABLE `tb_gdrive_mirrors`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_gdrive_queue`
--
ALTER TABLE `tb_gdrive_queue`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `gdrive_id` (`gdrive_id`);

--
-- Chỉ mục cho bảng `tb_hls_videos`
--
ALTER TABLE `tb_hls_videos`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_loadbalancers`
--
ALTER TABLE `tb_loadbalancers`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_plugins`
--
ALTER TABLE `tb_plugins`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_sessions`
--
ALTER TABLE `tb_sessions`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_settings`
--
ALTER TABLE `tb_settings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_stats`
--
ALTER TABLE `tb_stats`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_subtitles`
--
ALTER TABLE `tb_subtitles`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_subtitle_manager`
--
ALTER TABLE `tb_subtitle_manager`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_users`
--
ALTER TABLE `tb_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user` (`user`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Chỉ mục cho bảng `tb_videos`
--
ALTER TABLE `tb_videos`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_videos_alternatives`
--
ALTER TABLE `tb_videos_alternatives`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_videos_hash`
--
ALTER TABLE `tb_videos_hash`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_videos_short`
--
ALTER TABLE `tb_videos_short`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Chỉ mục cho bảng `tb_videos_sources`
--
ALTER TABLE `tb_videos_sources`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tb_gdrive_auth`
--
ALTER TABLE `tb_gdrive_auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tb_gdrive_mirrors`
--
ALTER TABLE `tb_gdrive_mirrors`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tb_gdrive_queue`
--
ALTER TABLE `tb_gdrive_queue`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tb_hls_videos`
--
ALTER TABLE `tb_hls_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `tb_loadbalancers`
--
ALTER TABLE `tb_loadbalancers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tb_plugins`
--
ALTER TABLE `tb_plugins`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tb_sessions`
--
ALTER TABLE `tb_sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `tb_settings`
--
ALTER TABLE `tb_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT cho bảng `tb_stats`
--
ALTER TABLE `tb_stats`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tb_subtitles`
--
ALTER TABLE `tb_subtitles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tb_subtitle_manager`
--
ALTER TABLE `tb_subtitle_manager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tb_users`
--
ALTER TABLE `tb_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tb_videos`
--
ALTER TABLE `tb_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `tb_videos_alternatives`
--
ALTER TABLE `tb_videos_alternatives`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tb_videos_hash`
--
ALTER TABLE `tb_videos_hash`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tb_videos_short`
--
ALTER TABLE `tb_videos_short`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `tb_videos_sources`
--
ALTER TABLE `tb_videos_sources`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
