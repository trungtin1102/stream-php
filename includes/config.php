<?php
/**
 * Insert your website homepage url. Example: https://yoursite.com/ (slash at the end)
 * @param string BASE_URL
 */
define('BASE_URL', 'https://hpstream.live/');

//if you rename the /e/ folder to /embed/ then change the value of this variable to 'EMBED_FOLDER'
//and line 100 and line 44 in dangnhap/views/hls/new
define('EMBED_FOLDER', BASE_URL . 'embed/' );

define('DIR_ROOT', $_SERVER['DOCUMENT_ROOT'] . '/' );


/**
 * Insert random alphanumeric as security code to encrypt/decrypt the existing string on your site
 * @param string SECURE_SALT
 */
define('SECURE_SALT', 'kbDX-');

/**
 * Insert the /administrator/ folder name you want. The value of this variable must be the same as the /administrator/ folder name. For example, if you rename the /administrator/ folder to /admin/ then change the value of this variable to 'admin'
 * @param string ADMIN_DIR
 */
define('ADMIN_DIR', 'dangnhap');

/**
 * The absolute path directory where TvHay is installed. Don't Change BASE_DIR value
 * @param string BASE_DIR
 */
define('BASE_DIR', dirname(__FILE__, 2) . '/');

/**
 * User agent makes this tool appear accessible to humans, not robots
 * @param string USER_AGENT
 */
define('USER_AGENT', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36');
