<?php
if (!defined('BASE_DIR')) die('access denied!');

set_time_limit(0);
ignore_user_abort(true);
session_write_close();
header_remove();

ob_start();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: false');
header('Cache-Control: no-store');
header('Content-Type: application/json; charset=utf-8');
header('Developed-By: TvHay.top');

if (strtolower($_SERVER['REQUEST_METHOD']) === 'post') {
    $token = isset($_POST['token']) ? htmlspecialchars($_POST['token']) : (isset($_COOKIE['adv_token']) ? $_COOKIE['adv_token'] : '');
    $is_admin = adminTokenValidation($token);
    if ($is_admin) {
        switch ($_POST['action']) {
            case 'clear_load_balancer':
                if (!empty($_POST['id'])) {
                    $class = new \VideoSources();
                    $class->setCriteria('sid', $_POST['id']);
                    $deleted[] = $class->delete();

                    $class = new \InstanceCache();
                    $deleted[] = $class->clear();

                    $deleted[] = deleteDir(BASE_DIR . 'cache');
                    $deleted[] = deleteDir(BASE_DIR . 'tmp');

                    $deleted = array_unique($deleted);

                    if (in_array(true, $deleted)) {
                        echo json_encode([
                            'status' => 'ok',
                            'message' => 'Cache cleared successfully.'
                        ]);
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'Failed to clear cache or cache does not exist.'
                        ]);
                    }
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'You are not authorized to access this page!'
                    ]);
                }
                break;

            case 'clear_hlsmpd_video_cache':
                $deleted = deleteDir(BASE_DIR . 'cache/playlist');
                if ($deleted) {
                    echo json_encode([
                        'status' => 'ok',
                        'message' => 'Cache cleared successfully.'
                    ]);
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Failed to clear cache or cache does not exist.'
                    ]);
                }
                break;

            case 'clear_video_cache':
                session_write_close();
                if (!empty($_POST['id'])) {
                    $deleted = [];

                    $class = new \Videos();
                    $class->setCriteria('id', $_POST['id']);
                    $data = $class->getOne(['host', 'host_id', 'ahost', 'ahost_id']);
                    if ($data) {
                        $class = new \VideoSources();
                        $class->setCriteria('host', $data['host']);
                        $class->setCriteria('host_id', $data['host_id'], '=', 'AND');
                        $deleted[] = $class->delete();

                        $host_id = keyFilter($data['host_id']);
                        $cacheFile = BASE_DIR . 'tmp/hosts/' . $data['host'] . '/' . $host_id;
                        $deleted[] = @unlink($cacheFile . '.m3u8');
                        $deleted[] = @unlink($cacheFile . '.mpd');

                        open_resources_handler();
                        $playlistDir = is_dir(BASE_DIR . 'cache/playlist') ? new \DirectoryIterator(BASE_DIR . 'cache/playlist') : false;
                        if ($playlistDir) {
                            foreach ($playlistDir as $file) {
                                if (!$file->isDot() && $file->isFile() && strpos($file->getFilename(), $data['host'] . '~' . $host_id) !== FALSE) {
                                    $deleted[] = @unlink(BASE_DIR . 'cache/playlist/' . $file->getFilename());
                                }
                            }
                        }

                        if (!empty($data['ahost_id'])) {
                            $class->setCriteria('host', $data['ahost']);
                            $class->setCriteria('host_id', $data['ahost_id'], '=', 'AND');
                            $deleted[] = $class->delete();

                            $ahost_id = keyFilter($data['ahost_id']);
                            $cacheFile = BASE_DIR . 'tmp/hosts/' . $data['ahost'] . '/' . $ahost_id;
                            $deleted[] = @unlink($cacheFile . '.m3u8');
                            $deleted[] = @unlink($cacheFile . '.mpd');

                            if ($playlistDir) {
                                foreach ($playlistDir as $file) {
                                    if (!$file->isDot() && $file->isFile() && strpos($file->getFilename(), $data['ahost'] . '~' . $ahost_id) !== FALSE) {
                                        $deleted[] = @unlink(BASE_DIR . 'cache/playlist/' . $file->getFilename());
                                    }
                                }
                            }
                        }

                        $class = new \VideosAlternatives();
                        $class->setCriteria('vid', $_POST['id']);
                        $list = $class->get(['host', 'host_id']);
                        if ($list) {
                            $class = new \VideoSources();
                            foreach ($list as $dt) {
                                $class->setCriteria('host', $dt['host']);
                                $class->setCriteria('host_id', $dt['host_id'], '=', 'AND');
                                $deleted[] = $class->delete();

                                $host_id = keyFilter($dt['host_id']);
                                $cacheFile = BASE_DIR . 'tmp/hosts/' . $dt['host'] . '/' . $host_id;
                                $deleted[] = @unlink($cacheFile . '.m3u8');
                                $deleted[] = @unlink($cacheFile . '.mpd');

                                if ($playlistDir) {
                                    foreach ($playlistDir as $file) {
                                        if (!$file->isDot() && $file->isFile() && strpos($file->getFilename(), $dt['host'] . '~' . $host_id) !== FALSE) {
                                            $deleted[] = @unlink(BASE_DIR . 'cache/playlist/' . $file->getFilename());
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $deleted = array_unique($deleted);

                    if (in_array(true, $deleted)) {
                        echo json_encode([
                            'status' => 'success',
                            'message' => 'Video cache cleared successfully.'
                        ]);
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'Failed to clear video cache or cache does not exist.'
                        ]);
                    }
                } else {
                    echo json_encode(array(
                        'status' => 'fail',
                        'message' => 'Invalid parameter!'
                    ));
                }
                break;

            case 'clear_cache':
                $class = new \InstanceCache();
                $deleted[] = $class->clear();

                $class = new \VideoSources();
                $class->setCriteria('id', '', '<>');
                $deleted[] = $class->delete();

                $deleted[] = deleteDir(BASE_DIR . 'cache');
                $deleted[] = deleteDir(BASE_DIR . 'tmp');

                $deleted = array_unique($deleted);

                if (in_array(true, $deleted)) {
                    echo json_encode([
                        'status' => 'ok',
                        'message' => 'Cache cleared successfully.'
                    ]);
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Failed to clear cache or cache does not exist.'
                    ]);
                }
                break;

            case 'clear_all_video_cache':
                $class = new \VideoSources();
                $class->setCriteria('id', '', '<>');
                $deleted[] = $class->delete();

                $deleted[] = deleteDir(BASE_DIR . 'cache/playlist');
                $deleted[] = deleteDir(BASE_DIR . 'cache/streaming');
                $deleted[] = deleteDir(BASE_DIR . 'tmp/hosts');

                $deleted = array_unique($deleted);

                if (in_array(true, $deleted)) {
                    echo json_encode([
                        'status' => 'ok',
                        'message' => 'Cache cleared successfully.'
                    ]);
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Failed to clear cache or cache does not exist.'
                    ]);
                }
                break;

            case 'clear_settings_cache':
                $iCache = new \InstanceCache();
                $deleted = $iCache->deleteItemsByTag('options');
                if ($deleted) {
                    echo json_encode([
                        'status' => 'ok',
                        'message' => 'Cache cleared successfully.'
                    ]);
                } else {
                    echo json_encode([
                        'status' => 'fail',
                        'message' => 'Failed to clear cache or cache does not exist.'
                    ]);
                }
                break;

            default:
                echo json_encode([
                    'status' => 'fail',
                    'message' => 'API not found!'
                ]);
                break;
        }
    } else {
        echo json_encode([
            'status' => 'fail',
            'message' => 'You are not authorized to access this page!'
        ]);
    }
} else {
    echo json_encode([
        'status' => 'fail',
        'message' => 'You are not authorized to access this page!'
    ]);
}

$min = new \Minify();
$output = $min->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
