<?php
if (!defined('BASE_DIR')) die('access denied!');

set_time_limit(0);
ignore_user_abort(true);
session_write_close();
header_remove();

ob_start();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: false');
header('Cache-Control: no-store');
header('Content-Type: application/json; charset=utf-8');
header('Developed-By: TvHay.top');

if (strtolower($_SERVER['REQUEST_METHOD']) === 'get') {
    session_write_close();
    if (isset($_GET['action'])) {
        session_write_close();
        switch ($_GET['action']) {
            case 'stat':
                session_write_close();
                if (!empty($_GET['data'])) {
                    $ip = getUserIP();
                    $ua = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'bot';
                    $max = intval(get_option('visit_counter'));
                    $time = strtotime(date('Y-m-d'));
                    $views = 0;
                    $fembed_id = '';
                    $hostStat = false;

                    $data = decode($_GET['data']);
                    parse_str($data, $qry);
                    if (isset($qry['source'])) {
                        $class = new \Videos();
                        $class->setCriteria('id', $qry['id']);
                        $data = $class->getOne(['views', 'host', 'host_id']);
                        if ($data) {
                            $fembed_id = $data['host'] === 'fembed' ? $data['host_id'] : '';
                            $views = intval($data['views']);
                        }
                    } else {
                        $newQry = [];
                        if (validate_url($qry['id'])) {
                            $class = new \Hosting($qry['id']);
                            $newQry['host'] = $class->getHost();
                            $newQry['id'] = $class->getID();
                        } else {
                            $newQry['host'] = $qry['host'];
                            $newQry['id'] = $qry['id'];
                        }
                        if (!empty($qry['aid'])) {
                            if (validate_url($qry['aid'])) {
                                $class = new \Hosting($qry['aid']);
                                $newQry['ahost'] = $class->getHost();
                                $newQry['aid'] = $class->getID();
                            } else {
                                $newQry['ahost'] = $qry['ahost'];
                                $newQry['aid'] = $qry['aid'];
                            }
                        } else {
                            $newQry['ahost'] = '';
                            $newQry['aid'] = '';
                        }
                        $class = new \Videos();
                        $class->setCriteria('host', $newQry['host']);
                        $class->setCriteria('host_id', $newQry['id'], '=', 'AND');
                        $class->setCriteria('ahost', $newQry['ahost'], '=', 'AND');
                        $class->setCriteria('ahost_id', $newQry['aid'], '=', 'AND');
                        $data = $class->getOne(['id', 'views']);
                        if ($data) {
                            $qry['id'] = $data['id'];
                            $views = intval($data['views']);
                        }
                    }
                    if (!empty($qry['id'])) {
                        session_write_close();

                        if (isset($qry['alt'])) {
                            if (intval($qry['alt']) > 0) {
                                $class = new \VideosAlternatives();
                                $class->setCriteria('vid', $qry['alt']);
                                $data = $class->getOne(['host_id']);
                                if ($data) {
                                    $fembed_id = $data['host_id'];
                                }
                            }
                        } elseif ($newQry['host'] === 'fembed' && !empty($newQry['id'])) {
                            $fembed_id = $newQry['id'];
                        } elseif ($newQry['ahost'] && !empty($newQry['aid'])) {
                            $fembed_id = $newQry['aid'];
                        }

                        if (!empty($fembed_id)) {
                            $class = new \VideoSources();
                            $class->setCriteria('host', 'fembed', '=');
                            $class->setCriteria('host_id', $fembed_id, '=', 'AND');
                            $data = $class->getOne(['data']);
                            if ($data) {
                                $data = json_decode($data['data'], true);
                                if (isset($data['log_url'])) {
                                    $class = new \fembed($fembed_id);
                                    $hostStat = $class->send_stats($data['log_url']);
                                }
                            }
                        }

                        $class = new \Videos();
                        $stats = new \Stats();
                        $stats->setCriteria('ip', $ip);
                        $stats->setCriteria('vid', $qry['id'], '=', 'AND');
                        $stats->setCriteria('created', $time, '>=', 'AND');
                        $stats->setOrderBy('created', 'DESC');
                        $stats->setLimit(0, $max);
                        $num = $stats->getNumRows();
                        if ($num < $max) {
                            $views += 1;
                            $class->setCriteria('id', $qry['id']);
                            $class->update(array(
                                'views' => $views
                            ));
                            $stats->insert(array(
                                'vid' => $qry['id'],
                                'ip' => $ip,
                                'ua' => $ua,
                                'created' => $time
                            ));
                            echo json_encode(array(
                                'status' => 'ok',
                                'message' => 'Total daily visits successfully added.',
                                'result' => $hostStat
                            ));
                        } else {
                            echo json_encode(array(
                                'status' => 'fail',
                                'message' => 'Total daily visits have been exceeded.',
                                'result' => $hostStat
                            ));
                        }
                    }
                } else {
                    echo json_encode(array(
                        'status' => 'fail',
                        'message' => 'Invalid parameter!'
                    ));
                }
                break;
                
            case 'clear-cache':
                session_write_close();
                if (!empty($_GET['data'])) {
                    $deleted = [];

                    $data = decode($_GET['data']);
                    if ($data) {
                        parse_str($data, $qry);
                        open_resources_handler();
                        $playlistDir = is_dir(BASE_DIR . 'cache/playlist') ? new \DirectoryIterator(BASE_DIR . 'cache/playlist') : false;
                        if (isset($qry['source'])) {
                            if (isset($qry['alt']) && intval($qry['alt']) > 0) {
                                $class = new \VideosAlternatives();
                                $class->setCriteria('id', $qry['alt']);
                                $data = $class->getOne(['host', 'host_id']);
                                if ($data) {
                                    $class = new \VideoSources();
                                    $class->setCriteria('host', $data['host']);
                                    $class->setCriteria('host_id', $data['host_id'], '=', 'AND');
                                    $deleted[] = $class->delete();

                                    $host_id = keyFilter($data['host_id']);
                                    $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $data['host'] . '/' . $host_id . '.m3u8');
                                    $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $data['host'] . '/' . $host_id . '.mpd');

                                    if ($playlistDir) {
                                        foreach ($playlistDir as $file) {
                                            if (!$file->isDot() && $file->isFile() && strpos($file->getFilename(), $data['host'] . '~' . $host_id) !== FALSE) {
                                                $deleted[] = @unlink(BASE_DIR . 'cache/playlist/' . $file->getFilename());
                                            }
                                        }
                                    }
                                }
                            } else {
                                $class = new \Videos();
                                $class->setCriteria('id', $qry['id']);
                                $data = $class->getOne(['host', 'host_id', 'ahost', 'ahost_id']);
                                if ($data) {
                                    $class = new \VideoSources();
                                    $class->setCriteria('host', $data['host']);
                                    $class->setCriteria('host_id', $data['host_id'], '=', 'AND');
                                    $deleted[] = $class->delete();

                                    $host_id = keyFilter($data['host_id']);
                                    $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $data['host'] . '/' . $host_id . '.m3u8');
                                    $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $data['host'] . '/' . $host_id . '.mpd');

                                    if ($playlistDir) {
                                        foreach ($playlistDir as $file) {
                                            if (!$file->isDot() && $file->isFile() && strpos($file->getFilename(), $data['host'] . '~' . $host_id) !== FALSE) {
                                                $deleted[] = @unlink(BASE_DIR . 'cache/playlist/' . $file->getFilename());
                                            }
                                        }
                                    }

                                    if (!empty($data['ahost_id'])) {
                                        $ahost_id = keyFilter($data['ahost_id']);
                                        $class->setCriteria('host', $data['ahost']);
                                        $class->setCriteria('host_id', $ahost_id, '=', 'AND');
                                        $deleted[] = $class->delete();

                                        $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $data['ahost'] . '/' . $ahost_id . '.m3u8');
                                        $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $data['ahost'] . '/' . $ahost_id . '.mpd');

                                        if ($playlistDir) {
                                            foreach ($playlistDir as $file) {
                                                if (!$file->isDot() && $file->isFile() && strpos($file->getFilename(), $data['ahost'] . '~' . $ahost_id) !== FALSE) {
                                                    $deleted[] = @unlink(BASE_DIR . 'cache/playlist/' . $file->getFilename());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            $class = new \VideoSources();
                            $class->setCriteria('host', $qry['host']);
                            $class->setCriteria('host_id', $qry['id'], '=', 'AND');
                            $deleted[] = $class->delete();

                            $host_id = keyFilter($qry['id']);
                            $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $qry['host'] . '/' . $host_id . '.m3u8');
                            $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $qry['host'] . '/' . $host_id . '.mpd');

                            if ($playlistDir) {
                                foreach ($playlistDir as $file) {
                                    if (!$file->isDot() && $file->isFile() && strpos($file->getFilename(), $qry['host'] . '~' . $host_id) !== FALSE) {
                                        $deleted[] = @unlink(BASE_DIR . 'cache/playlist/' . $file->getFilename());
                                    }
                                }
                            }

                            if (!empty($qry['aid'])) {
                                $class->setCriteria('host', $qry['ahost']);
                                $class->setCriteria('host_id', $qry['aid'], '=', 'AND');
                                $deleted[] = $class->delete();

                                $ahost_id = keyFilter($qry['aid']);
                                $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $qry['ahost'] . '/' . $ahost_id . '.m3u8');
                                $deleted[] = @unlink(BASE_DIR . 'tmp/hosts/' . $qry['ahost'] . '/' . $ahost_id . '.mpd');

                                if ($playlistDir) {
                                    foreach ($playlistDir as $file) {
                                        if (!$file->isDot() && $file->isFile() && strpos($file->getFilename(), $qry['ahost'] . '~' . $ahost_id) !== FALSE) {
                                            $deleted[] = @unlink(BASE_DIR . 'cache/playlist/' . $file->getFilename());
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $deleted = array_unique($deleted);

                    if (in_array(true, $deleted)) {
                        echo json_encode([
                            'status' => 'success',
                            'message' => 'Video cache cleared successfully.'
                        ]);
                    } else {
                        echo json_encode([
                            'status' => 'fail',
                            'message' => 'Failed to clear video cache or cache does not exist.'
                        ]);
                    }
                } else {
                    echo json_encode(array(
                        'status' => 'fail',
                        'message' => 'Invalid parameter!'
                    ));
                }
                break;

            default:
                session_write_close();
                echo json_encode(array(
                    'status' => 'fail',
                    'message' => 'What do you want?'
                ));
                break;
        }
    } else {
        session_write_close();
        echo json_encode(array(
            'status' => 'fail',
            'message' => 'What do you want?'
        ));
    }
} elseif (strtolower($_SERVER['REQUEST_METHOD']) === 'post') {
    session_write_close();
    if (isset($_POST['action'])) {
        session_write_close();
        switch ($_POST['action']) {
            case 'createPlayer':
                if (!empty($_POST['id'])) {
                    $user = current_user();
                    if ($user) {
                        $uid = (int) $user['id'];
                    } else {
                        $uid = (int) get_option('public_video_user');
                        $uid = !empty($uid) ? $uid : 1;
                    }

                    $token = isset($_POST['captcha-response']) ? $_POST['captcha-response'] : '';
                    $recaptchaIsValid = recaptcha_validate($token);
                    if ($recaptchaIsValid) {
                        unset($_POST['action']);
                        unset($_POST['captcha-response']);
                        unset($_POST['g-recaptcha-response']);

                        if (!empty($_FILES['poster-file']['tmp_name'])) {
                            $posterURL = upload_poster(array(
                                'tmp_name' => $_FILES['poster-file']['tmp_name'],
                                'name' => $_FILES['poster-file']['name'],
                                'size' => $_FILES['poster-file']['size'],
                                'type' => $_FILES['poster-file']['type'],
                                'error' => $_FILES['poster-file']['error']
                            ));
                            if ($posterURL) {
                                $_POST['poster'] = basename($posterURL);
                            } else {
                                $_POST['poster'] = '';
                            }
                        }

                        // upload subtitles
                        $now = time();
                        $subtitles = [];
                        $subLabelFiles = [];
                        $subLabelURLs = [];
                        $thisHost = strtr(parse_url(BASE_URL, PHP_URL_HOST), ['www.' => '']);

                        foreach ($_POST['sub-type'] as $i => $dt) {
                            if ($dt === 'file') $subLabelFiles[] = $_POST['lang'][$i];
                            elseif ($dt === 'url') $subLabelURLs[] = $_POST['lang'][$i];
                        }

                        $subManager = new \SubtitleManager();
                        if (!empty($_FILES['sub-file']['tmp_name']) && is_array($_FILES['sub-file']['tmp_name'])) {
                            foreach ($_FILES['sub-file']['tmp_name'] as $i => $dt) {
                                $subtitleURL = upload_subtitle(array(
                                    'tmp_name' => $_FILES['sub-file']['tmp_name'][$i],
                                    'name' => $_FILES['sub-file']['name'][$i],
                                    'size' => $_FILES['sub-file']['size'][$i],
                                    'type' => $_FILES['sub-file']['type'][$i],
                                    'error' => $_FILES['sub-file']['error'][$i]
                                ));
                                if ($subtitleURL) {
                                    $subManager->insert(array(
                                        'file_name' => basename($subtitleURL),
                                        'file_size' => $_FILES['sub-file']['size'][$i],
                                        'file_type' => $_FILES['sub-file']['type'][$i],
                                        'language' => $subLabelFiles[$i],
                                        'added' => $now,
                                        'uid' => $uid,
                                        'host' => $thisHost
                                    ));
                                    $subtitles[] = [
                                        'file' => $subtitleURL,
                                        'label' => $subLabelFiles[$i]
                                    ];
                                }
                            }
                        }

                        // save subtitle link
                        if (!empty($_POST['sub-url'])) {
                            create_dir(BASE_DIR . 'uploads');
                            create_dir(BASE_DIR . 'uploads/images');
                            create_dir(BASE_DIR . 'uploads/subtitles');
                            
                            $class = new \Subscene();
                            foreach ($_POST['sub-url'] as $i => $dt) {
                                if (validate_url($dt)) {
                                    if (strpos(parse_url($dt, PHP_URL_HOST), 'subscene') !== FALSE) {
                                        $class->set_url($dt);
                                        $class->download();
                                        $data = $class->file();
                                        if ($data) {
                                            $subManager->insert(array(
                                                'file_name' => basename($data['url']),
                                                'file_size' => filesize($data['path']),
                                                'file_type' => mime_content_type($data['path']),
                                                'language' => $subLabelURLs[$i],
                                                'added' => $now,
                                                'uid' => $uid,
                                                'host' => $thisHost
                                            ));
                                            $subtitles[] = [
                                                'file' => $data['url'],
                                                'label' => $subLabelURLs[$i]
                                            ];
                                        }
                                    } else {
                                        $subtitles[] = [
                                            'file' => $dt,
                                            'label' => $subLabelURLs[$i]
                                        ];
                                    }
                                }
                            }
                        }
                        unset($_POST['sub-type']);
                        unset($_POST['sub-file']);
                        unset($_POST['sub-url']);
                        unset($_POST['lang']);

                        $_POST['sub'] = array_column($subtitles, 'file');
                        $_POST['lang'] = array_column($subtitles, 'label');

                        if (!empty($_POST['id']) && validate_url($_POST['id'])) {
                            $class = new \Hosting($_POST['id']);
                            $_POST['host'] = $class->getHost();
                            $_POST['id'] = $class->getID();
                        } else {
                            $_POST['host'] = '';
                            $_POST['id'] = '';
                        }

                        if (!empty($_POST['aid']) && validate_url($_POST['aid'])) {
                            $class = new \Hosting($_POST['aid']);
                            $_POST['ahost'] = $class->getHost();
                            $_POST['aid'] = $class->getID();
                        } else {
                            $_POST['ahost'] = '';
                            $_POST['aid'] = '';
                        }

                        $qry = array_filter($_POST);
                        $qry = http_build_query($qry);
                        $qryEncoded = encode($qry);
                        $scheme = isSSL() ? 'https:' : 'http:';
                        $mainBaseURL = strtr(BASE_URL, ['https:' => $scheme, 'http:' => $scheme]);
                        $embed_link = $mainBaseURL . 'embed/?' . $qryEncoded;
                        $embed2_link = $mainBaseURL . 'embed2/?' . $qry;
                        $download_link = $mainBaseURL . 'download/?' . $qryEncoded;
                        $api_link = $mainBaseURL . 'api/?' . $qry;
                        $embed_code = '<iframe src="' . $embed_link . '" frameborder="0" width="640" height="320" allow="encrypted-media *;" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>';

                        echo json_encode(array(
                            'status' => 'success',
                            'result' => array(
                                'embed_link' => $embed_link,
                                'download_link' => $download_link,
                                'embed2_link' => $embed2_link,
                                'api_link' => $api_link,
                                'embed_code' => $embed_code
                            )
                        ));
                    } else {
                        echo json_encode(array(
                            'status' => 'fail',
                            'message' => 'Invalid credentials! Try again'
                        ));
                    }
                } else {
                    echo json_encode(array(
                        'status' => 'fail',
                        'message' => 'Insert the video link!'
                    ));
                }
                break;

            case 'gdriveAccessToken':
                session_write_close();
                $enableAjaxDownload = get_option('enable_gdrive_downloader');
                $enableAjaxDownload = filter_var($enableAjaxDownload, FILTER_VALIDATE_BOOLEAN);
                $token = isset($_POST['captcha-response']) ? $_POST['captcha-response'] : '';
                $recaptchaIsValid = recaptcha_validate($token);
                if ($enableAjaxDownload) {
                    if ($recaptchaIsValid) {
                        if (!empty($_COOKIE['adv_token'])) {
                            $allowed = loginTokenValidation($_COOKIE['adv_token']);
                        } elseif (!empty($_POST['token'])) {
                            $ip = getUserIP();
                            $access_token_ip = decode($_POST['token']);
                            if ($access_token_ip) {
                                $allowed = $_SERVER['REMOTE_ADDR'] === '::1' || getUserIPASN($ip) === getUserIPASN($access_token_ip);
                            } else {
                                $access_token_md5 = md5($ip . '.' . SECURE_SALT);
                                $allowed = $_POST['token'] === $access_token_md5 || loginTokenValidation($_POST['token']);
                            }
                        } else {
                            $allowed = false;
                        }
                        if ($allowed) {
                            session_write_close();
                            $class = new \GDrive_Auth();
                            $tokens = $class->get_access_tokens();
                            if ($tokens) {
                                $key = array_rand($tokens);
                                echo json_encode(array(
                                    'status' => 'success',
                                    'message' => 'Access Token created successfully!',
                                    'result' => $tokens[$key]
                                ));
                            } else {
                                echo json_encode(array(
                                    'status' => 'fail',
                                    'message' => 'Access Token not found or disabled!'
                                ));
                            }
                        } else {
                            session_write_close();
                            echo json_encode(array(
                                'status' => 'fail',
                                'message' => 'Invalid credentials! Try again'
                            ));
                        }
                    } else {
                        session_write_close();
                        echo json_encode(array(
                            'status' => 'fail',
                            'message' => 'Invalid credentials! Try again'
                        ));
                    }
                } else {
                    session_write_close();
                    echo json_encode(array(
                        'status' => 'fail',
                        'message' => 'This feature is disabled! Try again'
                    ));
                }
                break;

            case 'gdriveBypassLimit':
                session_write_close();
                $allowed = false;
                $enableSharer = filter_var(get_option('enable_gsharer'), FILTER_VALIDATE_BOOLEAN);
                if (!$enableSharer) {
                    session_write_close();
                    echo json_encode(array(
                        'status' => 'fail',
                        'message' => 'This feature is disabled!'
                    ));
                } elseif (!empty($_POST['gdrive_id'])) {
                    session_write_close();
                    if (!empty($_COOKIE['adv_token'])) {
                        $allowed = loginTokenValidation($_COOKIE['adv_token']);
                    } elseif (!empty($_POST['token'])) {
                        $ip = getUserIP();
                        $ip_token = decode($_POST['token']);
                        if ($ip_token) {
                            $allowed = getUserIPASN($ip) === getUserIPASN($ip_token);
                        } else {
                            $allowed = loginTokenValidation($_POST['token']);
                        }
                    }
                    if ($allowed) {
                        session_write_close();
                        $class = new \GDrive_Auth();
                        $class->encrypt_title(true);
                        $mirror = $class->get_mirrors(true, $_POST['gdrive_id']);
                        if ($mirror) {
                            session_write_close();
                            $info = $class->get_account($mirror['owners'][0]['emailAddress']);
                            if ($info) {
                                $link = $mirror['downloadUrl'] . '&key=' . $info['api_key'];
                            } else {
                                $link = 'https://drive.google.com/file/d/' . $mirror['id'] . '/view';
                            }
                            echo json_encode(array(
                                'status' => 'success',
                                'message' => 'Google Drive file limit has been bypassed successfully!',
                                'result' => array(
                                    'link' => $link,
                                    'id' => getDriveId($link)
                                )
                            ));
                        } else {
                            session_write_close();
                            echo json_encode(array(
                                'status' => 'fail',
                                'message' => 'Cannot bypass limit! Please try again later.'
                            ));
                        }
                    } else {
                        session_write_close();
                        echo json_encode(array(
                            'status' => 'fail',
                            'message' => 'Invalid credentials! Try again'
                        ));
                    }
                } else {
                    session_write_close();
                    echo json_encode(array(
                        'status' => 'fail',
                        'message' => 'Invalid parameter! Try again'
                    ));
                }
                break;

            case 'getDownloadLink':
                session_write_close();
                if (!empty($_POST['sources'])) {
                    session_write_close();
                    $result = [];
                    foreach ($_POST['sources'] as $src) {
                        $dt = [];
                        $url = download_link($src['file']);
                        $dt['file'] = validate_url($url) ? $url : rawurldecode($url);
                        $dt['label'] = $src['label'];
                        if (isset($src['type'])) {
                            $dt['type'] = $src['type'];
                        }
                        $result[] = $dt;
                    }
                    echo json_encode(array(
                        'status' => 'success',
                        'message' => 'Download links have been created successfully!',
                        'result' => $result
                    ));
                } else {
                    session_write_close();
                    echo json_encode(array(
                        'status' => 'fail',
                        'message' => 'Insert data sources!'
                    ));
                }
                break;

            default:
                session_write_close();
                echo json_encode(array(
                    'status' => 'fail',
                    'message' => 'What do you want?'
                ));
                break;
        }
    } else {
        session_write_close();
        echo json_encode(array(
            'status' => 'fail',
            'message' => 'What do you want?'
        ));
    }
} else {
    session_write_close();
    echo json_encode(array(
        'status' => 'fail',
        'message' => 'Invalid request!'
    ));
}

$min = new \Minify();
$output = $min->minify_json(ob_get_contents());
ob_end_clean();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
