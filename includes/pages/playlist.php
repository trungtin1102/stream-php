<?php
if (!defined('BASE_DIR')) die('access denied');

ignore_user_abort(true);
session_write_close();
header_remove();

if (ob_get_level()) ob_end_clean();
ob_start();
header('Developed-By: TvHay.top');
header('x-download-options: noopen');

if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('HTTP/1.1 204 No Content');
} else {
    $uri = get_page_uris();
    if (isset($uri[0]) && substr($uri[0], 0, 8) === 'playlist') {
        unset($uri[0]);
        $uri = array_values($uri);
        $token = isset($_GET['token']) ? $_GET['token'] : '';
        $allowed = cors_policy($token);
        if ($allowed) {
            if (!empty($_GET['url']) && !empty($_GET['data'])) {
                $qry['url'] = isset($_GET['url']) ? decode($_GET['url']) : '';
                $qry['data'] = isset($_GET['data']) ? decode($_GET['data']) : '';
                $qry['token'] = $token;

                $class = new \HLS($qry);
                $class->playlistStream();
            } else {
                $iCache = new \InstanceCache();
                $iCache->setKey('videos_hash_compact~' . $uri[0] . '~' . $uri[1]);
                $cache = $iCache->get();
                if ($cache) {
                    $data = $cache;
                } else {
                    $class = new \VideosHash();
                    $class->setCriteria('hash_host', $uri[0]);
                    $class->setCriteria('hash_id', $uri[1], '=', 'AND');
                    $data = $class->getOne(['host', 'host_id', 'gdrive_email']);
                    if ($data) {
                        $iCache->save($data, 2592000, 'videos_hash');
                    }
                }
                if ($data) {
                    $vHost = $data['host'];
                    $vid = $data['host_id'];

                    $host = parse_url(BASE_URL, PHP_URL_HOST);
                    $class = new \LoadBalancers();
                    $class->setCriteria('link', "%//{$host}%", 'LIKE');
                    $row = $class->getOne(['id']);
                    $sid = $row ? $row['id'] : 0;

                    $class = new \VideoSources();
                    $class->setCriteria('host', $vHost, '=');
                    $class->setCriteria('host_id', $vid, '=', 'AND');
                    $class->setCriteria('sid', $sid, '=', 'AND');
                    $class->setCriteria('dl', 0, '=', 'AND');
                    $data = $class->getOne(['data']);
                    if ($data) {
                        $data = json_decode($data['data'], true);
                        $qry = array(
                            'url' => $data['sources'][0]['file'],
                            'data' => $vHost . '~' . $vid,
                            'token' => $token
                        );
                        $class = new \HLS($qry);
                        $class->playlistStream();
                    } else {
                        http_response_code(404);
                        echo 'Page not found!';
                    }
                } else {
                    http_response_code(404);
                    echo 'Page not found!';
                }
            }
        } else {
            http_response_code(403);
            echo 'Access denied!';
        }
    } else {
        http_response_code(404);
        echo 'Page not found!';
    }
}
