<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

header('Cache-Control: no-store');
header('Developed-By: TvHay.top');

$class = new \Player();
$output = $class->embed_response_invalid(array(
    'title' => 'DMCA Takedown',
    'message' => 'Sorry this video is unavailable: DMCA Takedown'
));
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
