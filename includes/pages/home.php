<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

$title = get_option('site_slogan');
$sitedescription = get_option('site_description');

include 'header.php';
?>
<div class="row py-5 bg-custom text-center">
    <div class="col">
        <h1 class="h3"><?php echo htmlspecialchars_decode($title); ?></h1>
        <p><?php echo htmlspecialchars_decode($sitedescription); ?></p>
    </div>
</div>
<div class="row mt-5 mb-3">
    <div class="col">
        <form id="frmCreatePlayer" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <div class="input-group">
                    <input tabindex="0" type="url" id="id" name="id" class="form-control" placeholder="Main Video URL" required>
                    <div class="input-group-append">
                        <button type="button" class="btn btn-info" data-toggle="tooltip" title="Example URL" aria-label="Example URL">
                            <i aria-hidden="true" class="fas fa-info-circle"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="url" id="aid" name="aid" class="form-control" placeholder="Alternative Video URL">
                    <div class="input-group-append">
                        <button onclick="addSomeAlternative()" type="button" class="btn btn-custom" data-toggle="tooltip" title="Add More Alternative Video URL" aria-label="Add Alternative Video URL">
                            <i aria-hidden="true" class="fas fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div id="posterURL" class="input-group">
                    <input type="url" id="poster" name="poster" class="form-control" placeholder="Poster URL (.jpg, .jpeg, .png, .webp, .gif)">
                    <div class="input-group-append">
                        <button type="button" class="btn btn-primary btn-toggle-upload" data-toggle="tooltip" title="Upload Poster">
                            <i aria-hidden="true" class="fas fa-upload"></i>
                        </button>
                    </div>
                </div>
                <div id="posterUpload" class="input-group d-none">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input form-control" id="poster-file" name="poster-file" accept=".jpg, .jpeg, .png, .webp, .gif">
                        <label class="custom-file-label" for="poster-file">Choose file (.jpg, .jpeg, .png, .webp, .gif)</label>
                    </div>
                    <div class="input-group-append">
                        <button type="button" class="btn btn-primary btn-toggle-upload" data-toggle="tooltip" title="Insert Poster URL">
                            <i aria-hidden="true" class="fas fa-link"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="alert alert-success"><strong>NEW!</strong> Now we support subscene.com link. Example: https://subscene.com/subtitles/spider-man-no-way-home/indonesian/2656183</div>
            </div>
            <div id="subsWrapper">
                <div class="form-group" data-index="0">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <?php echo subtitle_languages('lang[]'); ?>
                            <input type="hidden" id="sub-type-0" name="sub-type[]" value="url">
                        </div>
                        <input type="text" name="sub-url[]" class="form-control subtitle" placeholder="Subtitle URL (.srt, .vtt, .ass, .sub, .stl, .dfxp, .ttml, .sbv, .txt)">
                        <div class="input-group-append">
                            <button type="button" class="btn btn-outline-primary" data-toggle="tooltip" title="Upload Subtitle" aria-label="Upload Subtitle" onclick="uploadSubtitle($(this))" data-index="0">
                                <i aria-hidden="true" class="fas fa-upload"></i>
                            </button>
                            <button type="button" class="btn btn-outline-success" data-toggle="tooltip" title="Add Subtitle" aria-label="Add Subtitle" onclick="addSubtitle()">
                                <i aria-hidden="true" class="fas fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group text-center mb-0">
                <?php
                $recaptcha_site_key = get_option('recaptcha_site_key');
                if ($recaptcha_site_key) : ?>
                    <div id="g-recaptcha" class="g-recaptcha" data-sitekey="<?php echo $recaptcha_site_key; ?>" data-size="invisible" data-callback="gCallback"></div>
                    <input type="hidden" id="captcha-response" name="captcha-response" />
                <?php endif; ?>
                <button id="submit" type="submit" class="btn btn-custom btn-block">
                    <i aria-hidden="true" class="fas fa-cog"></i>
                    <span class="ml-2">Create Player</span>
                </button>
            </div>
        </form>
    </div>
</div>
<?php
if (strpos(BASE_URL, 'TvHay') !== false || strpos(BASE_URL, '.test') !== false || strpos(BASE_URL, 'localhost') !== false) {
    include 'includes/sponsored.php';
}
?>
<div id="createPlayerResult" class="d-none">
    <div class="row my-3">
        <div class="col">
            <ul class="nav nav-pills nav-justified mb-3" role="tablist" aria-owns="url-tab embed-tab dl-tab req-tab json-tab">
                <li class="nav-item">
                    <a role="tab" class="nav-link active" id="url-tab" data-toggle="tab" href="#turl" aria-controls="turl">Embed URL</a>
                </li>
                <li class="nav-item">
                    <a role="tab" class="nav-link" id="embed-tab" data-toggle="tab" href="#tembed" aria-controls="tembed">Embed Code</a>
                </li>
                <li class="nav-item">
                    <a role="tab" class="nav-link" id="dl-tab" data-toggle="tab" href="#tdl" aria-controls="tdl">Download URL</a>
                </li>
                <li class="nav-item">
                    <a role="tab" class="nav-link" id="req-tab" data-toggle="tab" href="#treq" aria-controls="treq">Request URL</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="turl" role="tabpanel" aria-labelledby="url-tab">
                    <label for="txtEmbed">Embed URL</label>
                    <textarea onfocus="this.select()" id="txtEmbed" cols="30" rows="6" class="form-control" readonly></textarea>
                </div>
                <div class="tab-pane fade" id="tembed" role="tabpanel" aria-labelledby="embed-tab">
                    <label for="txtEmbedCode">Embed Code</label>
                    <textarea onfocus="this.select()" id="txtEmbedCode" cols="30" rows="6" class="form-control" readonly></textarea>
                </div>
                <div class="tab-pane fade" id="tdl" role="tabpanel" aria-labelledby="dl-tab">
                    <label for="txtDl">Download URL</label>
                    <textarea onfocus="this.select()" id="txtDl" cols="30" rows="6" class="form-control" readonly></textarea>
                </div>
                <div class="tab-pane fade" id="treq" role="tabpanel" aria-labelledby="req-tab">
                    <label for="txtReq">Request URL</label>
                    <textarea onfocus="this.select()" id="txtReq" cols="30" rows="6" class="form-control" readonly></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-3 text-center">
        <div class="col-12">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe id="embedIframe" class="embed-responsive-item" frameborder="0" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" allow="encrypted-media *;" scrolling="no"></iframe>
            </div>
        </div>
    </div>
</div>
<?php
include 'includes/link_format.php';
include 'includes/disqus.php';
include 'footer.php';
