<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

header('Cache-Control: no-store');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

$class = new \Player();
$output = $class->render_download_html();
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
