<?php
if (!defined('BASE_DIR')) die('access denied');

ignore_user_abort(true);
session_write_close();
header_remove();

if (ob_get_level()) ob_end_clean();
ob_start();
header('Developed-By: TvHay.top');
header('x-download-options: noopen');

if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('HTTP/1.1 204 No Content');
} else {
    $token = isset($_GET['token']) ? $_GET['token'] : '';
    $allowed = cors_policy($token);
    if ($allowed) {
        if (!empty($_GET['ref']) && !empty($_GET['data'])) {
            $qry['url'] = isset($_GET['url']) ? decode($_GET['url']) : '';
            $qry['ref'] = isset($_GET['ref']) ? decode($_GET['ref']) : '';
            $qry['data'] = isset($_GET['data']) ? decode($_GET['data']) : '';
            $qry['token'] = $token;

            $range = isset($_SERVER['HTTP_RANGE']) ? $_SERVER['HTTP_RANGE'] : '';
            $class = new \MPD($qry);
            $class->stream($range);
        } else {
            http_response_code(404);
            echo 'Page not found!';
        }
    } else {
        http_response_code(403);
        echo 'Access denied!';
    }
}
