<?php
if (!defined('BASE_DIR')) die('access denied');

ignore_user_abort(true);
session_write_close();
header_remove();

if (ob_get_level()) ob_end_clean();
ob_start();
header('Developed-By: TvHay.top');
header('x-download-options: noopen');

$uri = get_page_uris();
if (isset($uri[0]) && substr($uri[0], 0, 13) === 'videoplayback') {
    unset($uri[0]);
    $uri = array_values($uri);
    $token = isset($_GET['token']) ? $_GET['token'] : '';
    $allowed = cors_policy($token);
    if ($allowed) {
        $res = isset($uri[2]) ? ucwords(rawurldecode($uri[2])) : 'Original';
        $qry = [];
        $iCache = new \InstanceCache();
        if (isset($_GET['data'])) {
            $decode = decode($_GET['data']);
            parse_str($decode, $qry);
        } elseif (!empty($uri[0]) && !empty($uri[1])) {
            $iCache->setKey('video_hash_compact~' . $uri[0] . '~' . $uri[1]);
            $cache = $iCache->get();
            if ($cache) {
                $qry = [
                    'host' => $cache['host'],
                    'id' => $cache['host_id'],
                    'res' => $res,
                    'email' => $cache['gdrive_email']
                ];
            } else {
                $class = new \VideosHash();
                $class->setCriteria('hash_host', $uri[0], '=');
                $class->setCriteria('hash_id', $uri[1], '=', 'AND');
                $data = $class->getOne(['host', 'host_id', 'gdrive_email']);
                if ($data) {
                    $iCache->save($data, 2592000, 'videos_hash');
                    $qry = [
                        'host' => $data['host'],
                        'id' => $data['host_id'],
                        'res' => $res,
                        'email' => $data['gdrive_email']
                    ];
                }
            }
        }
        if (isset($qry['id']) && isset($qry['host'])) {
            $range = isset($_SERVER['HTTP_RANGE']) ? $_SERVER['HTTP_RANGE'] : '';
            $baseHost = ltrim(parse_url(BASE_URL, PHP_URL_HOST), 'www.');
            if (strpos($qry['id'], $baseHost) !== FALSE) {
                $iCache->setKey('videoplayback_direct~' . $qry['id']);
                $cache = $iCache->get();
                if ($cache && file_exists($cache)) {
                    $video = new \FileStream($cache);
                    $video->stream($range);
                } else {
                    $class = new \VideoSources();
                    $class->setCriteria('host', 'direct', '=');
                    $class->setCriteria('host_id', $qry['id'], '=', 'AND');
                    $class->setOrderBy('dl', 'DESC');
                    $row = $class->getOne(['data']);
                    if ($row) {
                        $data = json_decode($row['data'], true);
                        $key = array_search($res, array_column($data['sources'], 'label'));
                        $file = '';
                        if (isset($data['sources'][$key])) {
                            $file = $data['sources'][$key]['file'];
                        } else {
                            $file = $data['sources'][0]['file'];
                        }
                        $path = trim(parse_url($file, PHP_URL_PATH), '/');
                        $fileName = basename($path);
                        $path = strtr($path, [$fileName => '']);
                        $file = !empty($path) ? BASE_DIR . $path . '/' . $fileName : BASE_DIR . $fileName;
                        $iCache->save($file, 3600, 'local_video_file');
                        if (file_exists($file)) {
                            $video = new \FileStream($file);
                            $video->stream($range);
                        } else {
                            http_response_code(404);
                            echo 'Page not found!';
                        }
                    } else {
                        http_response_code(404);
                        echo 'Page not found!';
                    }
                }
            } else {
                $video = new \Video_Player($qry);
                $video->stream($range);
            }
        } else {
            http_response_code(404);
            echo 'Page not found!';
        }
    } else {
        http_response_code(403);
        echo 'Access denied!';
    }
} else {
    http_response_code(404);
    echo 'Page not found!';
}
