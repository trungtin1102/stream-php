<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();
header_remove();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: false');
header('Content-Type: application/xml; charset=UTF-8');
header('Developed-By: TvHay.top');

$premiumLink = class_exists('Premium') ? '<url>
<loc>' . BASE_URL . 'p/premium</loc>
<lastmod>2021-11-19T07:47:44+00:00</lastmod>
<priority>0.80</priority>
</url>' : '';

echo '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <url>
        <loc>' . BASE_URL . '</loc>
        <lastmod>2021-11-19T07:47:44+00:00</lastmod>
        <priority>1.00</priority>
    </url>
    <url>
        <loc>' . BASE_URL . 'sharer/</loc>
        <lastmod>2021-11-19T07:47:44+00:00</lastmod>
        <priority>0.80</priority>
    </url>
    <url>
        <loc>' . BASE_URL . 'changelog</loc>
        <lastmod>2021-11-19T07:47:44+00:00</lastmod>
        <priority>0.80</priority>
    </url>
    <url>
        <loc>' . BASE_URL . 'terms</loc>
        <lastmod>2021-11-19T07:47:44+00:00</lastmod>
        <priority>0.80</priority>
    </url>
    <url>
        <loc>' . BASE_URL . 'privacy</loc>
        <lastmod>2021-11-19T07:47:44+00:00</lastmod>
        <priority>0.80</priority>
    </url>
</urlset>';
