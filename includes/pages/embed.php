<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();

header('Cache-Control: no-store');
header('Developed-By: TvHay.top');

$class = new \Player();
if (is_smartTV()) {
    $output = $class->render_smartTV_html();
} else {
    $output = $class->render_embed_html();
}
$output = gzencode($output, 9);

header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
