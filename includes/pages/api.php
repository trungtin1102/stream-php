<?php
if (!defined('BASE_DIR')) die('access denied!');

set_time_limit(0);
ignore_user_abort(true);
session_write_close();
header_remove();

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: false');
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Content-Type: application/json; charset=utf-8');
header('Developed-By: TvHay.top');

$class = new \API();
$sources = $class->get_sources();
if ($sources) {
    if (validate_url($sources)) {
        header("location: $sources");
        exit();
    } else {
        echo $sources;
    }
} else {
    echo $class->get_status();
}
