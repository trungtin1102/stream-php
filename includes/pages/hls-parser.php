<?php
if (!defined('BASE_DIR')) die('access denied');

ignore_user_abort(true);
session_write_close();
header_remove();

if (ob_get_level()) ob_end_clean();
ob_start();
header('Content-Type: application/vnd.apple.mpegurl');
header('Developed-By: TvHay.top');
header('x-download-options: noopen');

if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('HTTP/1.1 204 No Content');
} else {
    $data = false;
    if (!empty($_GET['data'])) {
        $data = decode($_GET['data']);
    } else {
        $uri = get_page_uris();
        unset($uri[0]);
        $uri = array_values($uri);

        $iCache = new \InstanceCache();
        $iCache->setKey('video_hash_compact~' . $uri[0] . '~' . $uri[1]);
        $cache = $iCache->get();
        if ($cache) {
            $data = $cache['host'] . '/' . keyFilter($cache['host_id']) . '.m3u8';
        } else {
            $class = new \VideosHash();
            $class->setCriteria('hash_host', $uri[0]);
            $class->setCriteria('hash_id', $uri[1], '=', 'AND');
            $cache = $class->getOne(['host', 'host_id', 'gdrive_email']);
            if ($cache) {
                $iCache->save($cache, 2592000, 'videos_hash');
                $data = $cache['host'] . '/' . keyFilter($cache['host_id']) . '.m3u8';
            }
        }
    }
    if ($data) {
        open_resources_handler();
        $fp = @fopen(BASE_DIR . 'tmp/hosts/' . $data, 'rb');
        if ($fp) {
            stream_set_blocking($fp, false);
            $content = stream_get_contents($fp);
            fclose($fp);

            echo $content;
        } else {
            http_response_code(404);
            echo 'Page not found!';
        }
    } else {
        http_response_code(404);
        echo 'Page not found!';
    }
}
ob_flush();
flush();
