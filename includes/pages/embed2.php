<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();
header('Developed-By: TvHay.top');

if (empty($_SERVER['QUERY_STRING']) || empty($_GET['id'])) {
    http_response_code(404);
    exit('Page not found!');
}

if (!is_anonymous()) {
    $userLogin = current_user();
    if (!$userLogin || ($userLogin && intval($userLogin['role']) > 0)) {
        http_response_code(403);
        exit('Access denied!');
    }
}

if (is_load_balancer()) {
    $mainSite = get_option('main_site');
    header('location: ' . $mainSite . 'embed2/?' . $_SERVER['QUERY_STRING']);
    exit('Redirecting...');
}

$embedUrl = '';
$dlUrl = '';
$query = [];

if (!empty($_GET['id'])) {
    if (filter_var(validate_url($_GET['id']))) {
        $class = new \Hosting($_GET['id']);
        $query['host'] = $class->getHost();
        $query['id'] = $class->getID();
    } else {
        $query['host'] = isset($_GET['host']) ? $_GET['host'] : '';
        $query['id'] = $_GET['id'];
    }
}
if (!empty($_GET['aid'])) {
    if (filter_var(validate_url($_GET['aid']))) {
        $class = new \Hosting($_GET['aid']);
        $query['ahost'] = $class->getHost();
        $query['aid'] = $class->getID();
    } else {
        $query['ahost'] = isset($_GET['ahost']) ? $_GET['ahost'] : '';
        $query['aid'] = $_GET['aid'];
    }
}

if (!empty($query['id'])) {
    $qry = http_build_query($query);
    $eqry = encode($qry);
    $embedUrl = filter_var(BASE_URL . 'embed/?' . $eqry, FILTER_SANITIZE_URL);
    $dlUrl = filter_var(BASE_URL . 'download/?' . $eqry, FILTER_SANITIZE_URL);
}

if (isset($_GET['onlylink']) && filter_var($_GET['onlylink'], FILTER_VALIDATE_BOOLEAN)) {
    if (isset($_GET['download']) && filter_var($_GET['download'], FILTER_VALIDATE_BOOLEAN)) {
        echo $dlUrl;
    } else {
        echo $embedUrl;
    }
} else {
    if (isset($_GET['download']) && filter_var($_GET['download'], FILTER_VALIDATE_BOOLEAN)) {
        header('location: ' . $dlUrl);
    } else {
        header('location: ' . $embedUrl);
    }
}
