<?php
if (!defined('BASE_DIR')) die('access denied!');

set_time_limit(0);
ignore_user_abort(true);
session_write_close();

header('Cache-Control: no-store');
header('Content-Type: application/json; charset=UTF-8');
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

$result = '';
$username = '';
$password = '';
$is_login = FALSE;
$is_admin = FALSE;
if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
    $username = $_SERVER['PHP_AUTH_USER'];
    $password = $_SERVER['PHP_AUTH_PW'];
    $user = userValidation($username);
    if ($user) {
        $password_db = $user['password'];
        if (password_verify($password, $password_db)) {
            $is_login = TRUE;
            $is_admin = intval($user['role']) == 0;
        }
    }
} elseif (!empty($_SERVER['HTTP_AUTHORIZATION'])) {
    list($username, $password) = explode(':', base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));
    $user = userValidation($username);
    if ($user) {
        $password_db = $user['password'];
        if (password_verify($password, $password_db)) {
            $is_login = TRUE;
            $is_admin = intval($user['role']) == 0;
        }
    }
} elseif (!empty($_GET['username']) && !empty($_GET['password'])) {
    $username = htmlspecialchars($_GET['username']);
    $password = htmlspecialchars($_GET['password']);
    $user = userValidation($username);
    if ($user) {
        $password_db = $user['password'];
        if (password_verify($password, $password_db)) {
            $is_login = TRUE;
            $is_admin = intval($user['role']) == 0;
        }
    }
} else {
    $user = current_user();
    if ($user) {
        $is_login = TRUE;
        $is_admin = intval($user['role']) == 0;
    }
}
if ($is_login && $is_admin) {
    $disableProxy = get_option('disable_proxy');
    if (!filter_var($disableProxy, FILTER_VALIDATE_BOOLEAN)) {
        $excludeFreeProxy = get_option('free_proxy');
        $excludeFreeProxy = filter_var($excludeFreeProxy, FILTER_VALIDATE_BOOLEAN);
        $proxyList = trim(get_option('proxy_list'));
        if (!empty($proxyList) || $excludeFreeProxy) {
            $proxyList = explode("\n", strtr($proxyList, ["\r\n" => "\n"]));
            $proxyList = array_filter($proxyList);
            $proxyList = array_unique($proxyList, SORT_REGULAR);
            $usedPrx = FALSE;
            $prx = new \Proxy();
            $usedPrx = $prx->proxy_checker($proxyList);
            if ($usedPrx) {
                $usedPrx = array_filter($usedPrx);
                $usedPrx = array_unique($usedPrx, SORT_REGULAR);
                $savedPrx = trim(implode("\n", $usedPrx));
                set_option('proxy_list', $savedPrx);
                $result = json_encode([
                    'status' => 'ok',
                    'message' => 'Proxy has been successfully validated and can be used.',
                    'result' => $savedPrx
                ]);
            } else {
                $result = json_encode([
                    'status' => 'fail',
                    'message' => 'Failed to retrieve validated proxy status. If there is a proxy in the proxy list column, the proxy is validated and can be used.'
                ]);
            }
        } else {
            $prx = new \Proxy();
            $usedPrx = FALSE;
            if (!empty($_GET['host'])) {
                $proxyList = [];
                if ($_GET['host'] === 'proxy_docker_com') {
                    for ($i = 1; $i <= 10; $i++) {
                        $xlist = $prx->proxy_docker_com($i);
                        $proxyList = array_merge($proxyList, $xlist);
                    }
                    $proxyList = array_filter($proxyList);
                    $proxyList = array_unique($proxyList, SORT_REGULAR);
                    $usedPrx = $prx->proxy_checker($proxyList);
                } elseif ($_GET['host'] === 'free_proxy_list_net') {
                    $proxyList = $prx->free_proxy_list_net();
                    $proxyList = array_filter($proxyList);
                    $proxyList = array_unique($proxyList, SORT_REGULAR);
                    $usedPrx = $prx->proxy_checker($proxyList);
                } else {
                    $types = ['https', 'socks4', 'socks5'];
                    foreach($types as $type) {
                        $xlist = $prx->free_proxy_cz($type);
                        $proxyList = array_merge($proxyList, $xlist);
                    }
                    $proxyList = array_filter($proxyList);
                    $proxyList = array_unique($proxyList, SORT_REGULAR);
                    $usedPrx = $prx->proxy_checker($proxyList);
                }
            } else {
                $proxyList = $prx->free_proxy_cz();
                $proxyList = array_filter($proxyList);
                $proxyList = array_unique($proxyList, SORT_REGULAR);
                $usedPrx = $prx->proxy_checker($proxyList);
            }
            if ($usedPrx) {
                $usedPrx = array_filter($usedPrx);
                $usedPrx = array_unique($usedPrx, SORT_REGULAR);
                $list = explode("\n", trim(get_option('proxy_list')));
                $list = array_merge($list, $usedPrx);
                $savedPrx = trim(implode("\n", $list));
                set_option('proxy_list', $savedPrx);
                $result = json_encode([
                    'status' => 'ok',
                    'message' => 'Proxy has been successfully validated and can be used.',
                    'result' => $savedPrx
                ]);
            } else {
                $result = json_encode([
                    'status' => 'fail',
                    'message' => 'Failed to retrieve validated proxy status. If there is a proxy in the proxy list column, the proxy is validated and can be used.'
                ]);
            }
        }
    } else {
        $result = json_encode([
            'status' => 'fail',
            'message' => 'Could not check the proxy because the proxy is disabled.'
        ]);
    }
} else {
    $result = json_encode([
        'status' => 'fail',
        'message' => 'You are not authorized to access this page!'
    ]);
}

$min = new \Minify();
$output = $min->minify_json($result);
$output = gzencode($output, 9);
header('Content-Encoding: gzip');
header('Content-Length: ' . strlen($output));
echo $output;
