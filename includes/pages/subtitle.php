<?php
if (!defined('BASE_DIR')) die('access denied!');

ignore_user_abort(true);
session_write_close();
header_remove();

if (isset($_GET['url'])) {
	$url = rawurldecode($_GET['url']);
	$url = validate_url($url) ? strtr($url, [" " => '%20', 'www.' => '', 'fembed.com' => 'thumb.fvs.io']) : decode($url);

	$host = parse_url($url, PHP_URL_HOST);
	$query 	= parse_URL($url, PHP_URL_QUERY);
	$fragment = parse_URL($url, PHP_URL_FRAGMENT);
	$ext = strtr(pathinfo(basename($url), PATHINFO_EXTENSION), ['?' . $query => '', '#' . $fragment => '']);

	$mainHost = parse_url(get_option('main_site'), PHP_URL_HOST);
	$time = time();
	if ($host === $mainHost) {
		$url = strpos($url, '/uploads/subtitles/') !== FALSE ? $url : strtr($url, ['/subtitles/' => '/uploads/subtitles/']);
		if ($ext === 'vtt' || $ext === 'webvtt') {
			$scheme = isSSL() ? 'https:' : 'http:';
			header('location: ' . strtr($url, ['https:' => $scheme, 'http:' => $scheme]));
			exit();
		} else {
			$file = strtr($url, [BASE_URL => BASE_DIR, '%20' => ' ']);
			if (file_exists($file)) {
				open_resources_handler();
				$fp = @fopen($file, 'rb');
				if ($fp) {
					stream_set_blocking($fp, false);
					$content = stream_get_contents($fp);
					fclose($fp);

					if (ob_get_level()) ob_end_clean();
					ob_start();
					header('Access-Control-Allow-Origin: *');
					header('Access-Control-Allow-Credentials: false');
					header('Content-Type: text/vtt;charset=UTF-8');
					header('Developed-By: TvHay.top');
					header('Expires: ' . gmdate('D, d M Y H:i:s', $time + 2592000) . ' GMT');
					header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $time) . ' GMT');
					$content = strip_tags($content);
					$class = new \Done\Subtitles\Subtitles();
					$subtitles = $class::loadString($content, $ext);
					echo $subtitles->content('vtt');
					ob_flush();
					flush();
					exit();
				} else {
					error_log("subtitle error $url => cannot read the file");
					http_response_code(404);
					exit();
				}
			} else {
				error_log("subtitle error $file => file not exists");
				http_response_code(404);
				exit();
			}
		}
	}

	if (ob_get_level()) ob_end_clean();
	ob_start();
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Credentials: false');
	header('Content-Type: text/vtt;charset=UTF-8');
	header('Developed-By: TvHay.top');
	header('Expires: ' . gmdate('D, d M Y H:i:s', $time + 2592000) . ' GMT');
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $time) . ' GMT');

	$scheme = parse_url($url, PHP_URL_SCHEME);
	$port 	= parse_URL($url, PHP_URL_PORT);
	if (empty($port)) $port = $scheme === 'https' ? 443 : 80;
	$ipv4 = gethostbyname($host . '.');
	$resolveHost = array(implode(':', array($host, $port, $ipv4)));

	session_write_close();
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
	curl_setopt($ch, CURLOPT_ENCODING, '');
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	curl_setopt($ch, CURLOPT_RESOLVE, $resolveHost);
	curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
	curl_setopt($ch, CURLOPT_DNS_SERVERS, dns_servers());
	curl_setopt($ch, CURLOPT_NOSIGNAL, true);
	curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
	curl_setopt($ch, CURLOPT_TCP_NODELAY, true);
	curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
	curl_setopt($ch, CURLOPT_FAILONERROR, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		"host: $host",
		"origin: $scheme://$host"
	));
	curl_setopt($ch, CURLOPT_REFERER, "$scheme://$host/");
	curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
	curl_setopt($ch, CURLOPT_HEADERFUNCTION, function ($ch, $header) {
		session_write_close();
		return strlen($header);
	});

	session_write_close();
	$response = curl_exec($ch);
	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$err = curl_error($ch);
	curl_close($ch);

	if ($status >= 200 && $status < 400) {
		$response = trim($response);
		if ($ext !== 'vtt' && $ext !== 'webvtt' && substr(trim($response), 0, 6) !== 'WEBVTT') {
			$class = new \Done\Subtitles\Subtitles();
			$subtitles = $class::loadString(strip_tags($response), $ext);
			echo $subtitles->content('vtt');
		} else {
			echo $response;
		}
		ob_flush();
		flush();
	} else {
		error_log("subtitle error $url => $status: $err");
		http_response_code($status);
	}
} else {
	http_response_code(404);
}
