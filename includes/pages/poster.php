<?php
if (!defined('BASE_DIR')) die('access denied');

ignore_user_abort(true);
session_write_close();
header_remove();

if (!empty($_GET['url'])) {
	$url = rawurldecode($_GET['url']);
	$url = validate_url($url) ? $url : decode($url);

	$time = time();
	$host = parse_url($url, PHP_URL_HOST);
	$mainHost = parse_url(get_option('main_site'), PHP_URL_HOST);

	if ($host === $mainHost) {
		$scheme = isSSL() ? 'https:' : 'http:';
		header('location: ' . strtr($url, ['https:' => $scheme, 'http:' => $scheme]));
		exit();
	} else {
		if (ob_get_level()) ob_end_clean();
		ob_start();
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Credentials: false');
		header('Content-Type: text/vtt;charset=UTF-8');
		header('Developed-By: TvHay.top');
		header('Expires: ' . gmdate('D, d M Y H:i:s', $time + 2592000) . ' GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $time) . ' GMT');

		$headers = array();
		array_push($headers, 'Host: ' . $host);

		$class = new \VideoSources();
		if (strpos($url, 'zippyshare.com') !== FALSE) {
			list($qry, $time) = array_pad(explode('&time', parse_url($url, PHP_URL_QUERY)), 2, '');
			$class->setCriteria('data', '%' . $qry . '%', 'LIKE');
			$get = $class->getOne(['data']);
			if ($get) {
				$data = json_decode($get['data'], true);
				array_push($headers, 'Referer: ' . $data['referer']);
				array_push($headers, 'Cookie: ' . implode(';', $data['cookies']));
			}
		} else {
			$class->setCriteria('data', '%' . $host . '%', 'LIKE');
			$get = $class->getOne(['data']);
			if ($get) {
				$data = json_decode($get['data'], true);
				array_push($headers, 'Referer: ' . $data['referer']);
			}
		}

		$scheme = parse_url($url, PHP_URL_SCHEME);
		$port 	= parse_URL($url, PHP_URL_PORT);
		if (empty($port)) $port = $scheme === 'https' ? 443 : 80;
		$ipv4 = gethostbyname($host . '.');
		$resolveHost = array(implode(':', array($host, $port, $ipv4)));

		session_write_close();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
		curl_setopt($ch, CURLOPT_ENCODING, '');
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_RESOLVE, $resolveHost);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($ch, CURLOPT_DNS_SERVERS, dns_servers());
		curl_setopt($ch, CURLOPT_NOSIGNAL, true);
		curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
		curl_setopt($ch, CURLOPT_TCP_NODELAY, true);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_NOBODY, false);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
		curl_setopt($ch, CURLOPT_HEADERFUNCTION, function ($ch, $header) {
			session_write_close();
			$header = strtolower($header);
			if (strpos($header, 'content-type:') !== false || strpos($header, 'content-length:') !== false) {
				header($header);
			}
			return strlen($header);
		});
		curl_setopt($ch, CURLOPT_WRITEFUNCTION, function ($ch, $body) {
			session_write_close();
			echo $body;
			ob_flush();
			flush();
			return strlen($body);
		});

		session_write_close();
		curl_exec($ch);
		$type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$err = curl_error($ch);
		curl_close($ch);

		if ($status < 200 || $status >= 400) {
			error_log("Poster Error $url => $status: $err");
			http_response_code($status);
		}
	}
} else {
	http_response_code(404);
}
