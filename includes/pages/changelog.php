<?php
if (!defined('BASE_DIR')) exit();

session_write_close();
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

$title = 'Change Log';
$sitedescription = 'These change logs indicate that this website is being kept up to date.';
include 'header.php';
?>
<div class="row pt-5" id="changelog">
    <div class="col-12">
        <h3 class="mb-4">Change Log V8.07.1</h3>
        <ul>
            <li class="mb-3">
                <strong>Jul 15, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Direct, Fembed, Streamlare, Uqload Hosts</li>
                    <li><span class="text-bold text-primary">Fixed</span> Cache System</li>
                    <li><span class="text-bold text-primary">Fixed</span> Plyr Video Player</li>
                    <li><span class="text-bold text-primary">Fixed</span> Background Process</li>
                    <li><span class="text-bold text-primary">Fixed</span> Load Balancer Handler</li>
                    <li><span class="text-bold text-primary">Fixed</span> Bypassing Traffic Control</li>
                    <li><span class="text-bold text-primary">Fixed</span> Video Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Open Files Handler</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Jul 01, 2022</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> Individual Load Balancer Cache Cleaner</li>
                    <li><span class="text-bold text-primary">Fixed</span> Bypassing Traffic Control</li>
                    <li><span class="text-bold text-primary">Fixed</span> All Hosts</li>
                    <li><span class="text-bold text-primary">Fixed</span> Proxy Rotator</li>
                    <li><span class="text-bold text-primary">Fixed</span> Background Process</li>
                    <li><span class="text-bold text-primary">Fixed</span> Video, HLS, MPD Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Increase Security</li>
                    <li><span class="text-bold text-primary">Fixed</span> Increase Buffer Size</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive File Manager</li>
                    <li><span class="text-bold text-primary">Fixed</span> Open Files Handler</li>
                    <li><span class="text-bold text-primary">Fixed</span> Player Error Timeout Handler</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-info">Updated</span> PHP Libraries</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Jun 01, 2022</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> Uploadbuzz</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive Sharer Page</li>
                    <li><span class="text-bold text-primary">Fixed</span> Subtitle & Poster Bypass Parser</li>
                    <li><span class="text-bold text-primary">Fixed</span> Background Process</li>
                    <li><span class="text-bold text-primary">Fixed</span> Nginx Configuration File</li>
                    <li><span class="text-bold text-primary">Fixed</span> Headless Chromium</li>
                    <li><span class="text-bold text-primary">Fixed</span> Plyr Video Player</li>
                    <li><span class="text-bold text-primary">Fixed</span> Video Player Error handler</li>
                    <li><span class="text-bold text-primary">Fixed</span> Bypassing Traffic Control</li>
                    <li><span class="text-bold text-primary">Fixed</span> Decrease Buffer Size</li>
                    <li><span class="text-bold text-primary">Fixed</span> Proxy Checker</li>
                    <li><span class="text-bold text-primary">Fixed</span> Cache System</li>
                    <li><span class="text-bold text-primary">Fixed</span> Video, HLS, MPD Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Blogger, Dropbox, Fembed, Files.im, Google Drive, Google Photos, OneDrive, PandaFiles, StreamSB, Yourupload Hosts</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-info">Updated</span> PHP Libraries</li>
                    <li><span class="text-bold text-danger">Removed</span> DoodStream</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>May 9, 2022</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> Default Video Resolution</li>
                    <li><span class="text-bold text-success">Added</span> Default Subtitle Language</li>
                    <li><span class="text-bold text-primary">Fixed</span> Bypassing Traffic Control</li>
                    <li><span class="text-bold text-primary">Fixed</span> Subtitle & Poster Bypass Parser</li>
                    <li><span class="text-bold text-primary">Fixed</span> PHP Functions Dependency Checker</li>
                    <li><span class="text-bold text-primary">Fixed</span> Resources Limit Handler</li>
                    <li><span class="text-bold text-primary">Fixed</span> Video, HLS, MPD Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Fembed, Gofile, Google Drive, Vidoza, VIU Hosts</li>
                    <li><span class="text-bold text-primary">Fixed</span> Database Connection</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>May 5, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Bypassing Traffic Control</li>
                    <li><span class="text-bold text-primary">Fixed</span> Headless Chromium</li>
                    <li><span class="text-bold text-primary">Fixed</span> StreamSB, Upstream Hosts</li>
                    <li><span class="text-bold text-primary">Fixed</span> Error Logs</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Apr 30, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Direct, Soundcloud, Tiktok, Youtube Hosts</li>
                    <li><span class="text-bold text-primary">Fixed</span> Disabled Resolutions</li>
                    <li><span class="text-bold text-primary">Fixed</span> Error Logs</li>
                    <li><span class="text-bold text-primary">Fixed</span> PHP Functions Dependency Checker</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Apr 29, 2022</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> Global Public DNS Servers</li>
                    <li><span class="text-bold text-success">Added</span> vast.js for JW Player 8.9.5</li>
                    <li><span class="text-bold text-primary">Fixed</span> Quality Selector on Plyr</li>
                    <li><span class="text-bold text-primary">Fixed</span> P2P on JW Player</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Chrome Checker</li>
                    <li><span class="text-bold text-primary">Fixed</span> Proxy Checker & Rotation</li>
                    <li><span class="text-bold text-primary">Fixed</span> Enable KeepAlive Connection on htaccess and nginx.conf</li>
                    <li><span class="text-bold text-primary">Fixed</span> Resource Usage Checker</li>
                    <li><span class="text-bold text-primary">Fixed</span> Cache Control Response</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS & MPD Fragment MP4 Cache System</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS & MPD Subtitle Bypass Parser</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS & MPD Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Video Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-info">Updated</span> Increase Buffer Size</li>
                    <li><span class="text-bold text-info">Updated</span> Service Workers</li>
                    <li><span class="text-bold text-info">Updated</span> PHP and Javascript Libraries</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Apr 17, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> StreamSB Host</li>
                    <li><span class="text-bold text-primary">Fixed</span> Headless Chromium</li>
                    <li><span class="text-bold text-primary">Fixed</span> Change Database Connection Charset</li>
                    <li><span class="text-bold text-primary">Fixed</span> Video Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Cache System</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-info">Updated</span> PHP Libraries</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Apr 12, 2022</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> Support for Smart TV</li>
                    <li><span class="text-bold text-success">Added</span> Use Load Balancer with Least Resource Usage</li>
                    <li><span class="text-bold text-success">Added</span> Background Process</li>
                    <li><span class="text-bold text-primary">Fixed</span> Video Player Error Handler</li>
                    <li><span class="text-bold text-primary">Fixed</span> Cache System</li>
                    <li><span class="text-bold text-primary">Fixed</span> Vidio (Free Video Only) Host</li>
                    <li><span class="text-bold text-primary">Fixed</span> Headless Chromium</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-info">Updated</span> PHP Libraries</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Apr 09, 2022</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> Vidio (Free Video Only) Host</li>
                    <li><span class="text-bold text-primary">Fixed</span> Headless Chromium</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive, VIU Hosts</li>
                    <li><span class="text-bold text-primary">Fixed</span> Security</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> MPD Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Apr 08, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Cookies Validation on Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Cache System</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive File manager</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-info">Updated</span> PHP Libraries</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Apr 04, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Security</li>
                    <li><span class="text-bold text-primary">Fixed</span> Cache System</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Mar 30, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Security</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> MPD Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Mar 29, 2022</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> Streamlare Hosts</li>
                    <li><span class="text-bold text-primary">Fixed</span> Video Player Tracks</li>
                    <li><span class="text-bold text-primary">Fixed</span> Subtitle Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Headless Chromium</li>
                    <li><span class="text-bold text-primary">Fixed</span> StreamSB</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-info">Updated</span> PHP Libraries</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Mar 27, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Fembed, Google Drive, OneDrive, Sibnet.ru</li>
                    <li><span class="text-bold text-primary">Fixed</span> Increase Buffer Size</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Mar 25, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Facebook, OneDrive, StreamSB</li>
                    <li><span class="text-bold text-primary">Fixed</span> Cache Control Response</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> MPD Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS Error Handler on Plyr</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Mar 20, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Headless Chromium Configuration</li>
                    <li><span class="text-bold text-primary">Fixed</span> Plugin Core</li>
                    <li><span class="text-bold text-primary">Fixed</span> Cache Control Response</li>
                    <li><span class="text-bold text-primary">Fixed</span> Buffer Stalled on Plyr</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> MPD Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive, VOE, Vudeo Hosts</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive Bypass System</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive File Manager</li>
                    <li><span class="text-bold text-primary">Fixed</span> RESTful API</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-info">Updated</span> PHP and Javascript Libraries</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Mar 15, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Blocking System</li>
                    <li><span class="text-bold text-primary">Fixed</span> Disable DNS Cache if the Video Direct URL is an IP Address</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Mar 10, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> MPD Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Alternate Host Rotation</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive File Manager</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Mar 08, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Cache Control Response</li>
                    <li><span class="text-bold text-primary">Fixed</span> RESTful API</li>
                    <li><span class="text-bold text-primary">Fixed</span> Embed Shortcut</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Mar 07, 2022</strong>
                <ul>
                    <li><span class="text-bold text-success">Improved</span> Copy Stream Performance</li>
                    <li><span class="text-bold text-primary">Fixed</span> AdBlocker Detector</li>
                    <li><span class="text-bold text-primary">Fixed</span> Fembed</li>
                    <li><span class="text-bold text-primary">Fixed</span> Caching System</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> MPD Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Mar 05, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Move Fireload, PandaFiles, Streamff, Vudeo hosts into direct hosts</li>
                    <li><span class="text-bold text-primary">Fixed</span> AdBlocker Detector</li>
                    <li><span class="text-bold text-primary">Fixed</span> Facebook, StreamSB Caching System</li>
                    <li><span class="text-bold text-primary">Fixed</span> CORS Policy</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-danger">Removed</span> JW Player Custom Lisense</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Mar 04, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Login System</li>
                    <li><span class="text-bold text-primary">Fixed</span> Subtitle List</li>
                    <li><span class="text-bold text-primary">Fixed</span> Encrypted Javascript Cache</li>
                    <li><span class="text-bold text-primary">Fixed</span> CORS Policy</li>
                    <li><span class="text-bold text-primary">Fixed</span> Alternate Host Rotation</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Mar 03, 2022</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> Visit Stats on Fembed</li>
                    <li><span class="text-bold text-primary">Fixed</span> Alternate Host Rotation</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-danger">Removed</span> Uptobox HLS</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Mar 02, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Google IMA Ads on Plyr</li>
                    <li><span class="text-bold text-primary">Fixed</span> Add Bulk Videos</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> MPD Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Mar 01, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Fembed, Google Drive</li>
                    <li><span class="text-bold text-primary">Fixed</span> Proxy Rotation</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Feb 27, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Facebook</li>
                    <li><span class="text-bold text-primary">Fixed</span> Copy Stream System</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Feb 25, 2022</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Facebook, Files.im, StreamSB, Uploads.mobi</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> MPD Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Subtitle Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Proxy Rotation</li>
                    <li><span class="text-bold text-primary">Fixed</span> AdBlocker Detector</li>
                    <li><span class="text-bold text-primary">Fixed</span> Show Small Logo on JW Player Control</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Feb 24, 2022</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> File.cm, Fireload, Mail.ru Video, PandaFiles, Sibnet.ru Video, Streamff, SuperVideo, VOE, Vudeo Hosts</li>
                    <li><span class="text-bold text-success">Added</span> Non-Blocking File Stream</li>
                    <li><span class="text-bold text-success">Added</span> Supports CEA 608/708 Subtitles if p2p is Disabled on Plyr</li>
                    <li><span class="text-bold text-success">Added</span> HLS Multiple Audio Groups on Plyr</li>
                    <li><span class="text-bold text-success">Added</span> Mail.ru, Sendvid, Sibnet.ru, SuperVideo, VOE Hosts</li>
                    <li><span class="text-bold text-success">Added</span> Custom Administration Folder</li>
                    <li><span class="text-bold text-success">Added</span> Use VMAP Proxy on Multi VAST on Plyr</li>
                    <li><span class="text-bold text-success">Added</span> Combining Internal and External Subtitles on HLS and MPD Videos on Plyr</li>
                    <li><span class="text-bold text-success">Added</span> Support for Subscene.com Links</li>
                    <li><span class="text-bold text-success">Added</span> Automated Database Optimization and Repair</li>
                    <li><span class="text-bold text-success">Added</span> Multi Additional Hosts</li>
                    <li><span class="text-bold text-success">Improved</span> Security</li>
                    <li><span class="text-bold text-success">Improved</span> Change MySQL Database Engine to InnoDB</li>
                    <li><span class="text-bold text-primary">Fixed</span> Cache System</li>
                    <li><span class="text-bold text-primary">Fixed</span> P2P System</li>
                    <li><span class="text-bold text-primary">Fixed</span> Disable Cache on HLS and MPD Live Streaming</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> MPD Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google IMA Ads on Plyr</li>
                    <li><span class="text-bold text-primary">Fixed</span> Proxy Rotation</li>
                    <li><span class="text-bold text-primary">Fixed</span> Subtitle Uploader</li>
                    <li><span class="text-bold text-primary">Fixed</span> Add Bulk Videos</li>
                    <li><span class="text-bold text-primary">Fixed</span> Copy Stream System for DoodStream, MixDrop, mp4upload and Vupload Hosts</li>
                    <li><span class="text-bold text-primary">Fixed</span> Direct Host, Fembed, Google Drive, HxFile, Racaty, StreamSB, Tiktok, Uploads.mobi, Upstream, Uptobox, Userscloud</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-info">Updated</span> PHP and Javascript Libraries</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Dec 31, 2021</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> Visitor Statistics</li>
                    <li><span class="text-bold text-primary">Fixed</span> Cache System</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive, VIU</li>
                    <li><span class="text-bold text-primary">Fixed</span> Poster Uploader</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Dec 30, 2021</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> Custom Slug</li>
                    <li><span class="text-bold text-success">Added</span> Supported Subtitle Formats (.ass, .sub, .stl, .dfxp, .ttml, .sbv, .qt.txt)</li>
                    <li><span class="text-bold text-primary">Fixed</span> Subtitle Bypass Parser</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Dec 18, 2021</strong>
                <ul>
                    <li><span class="text-bold text-danger">Removed</span> Vidlox</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Cache System</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Dec 10, 2021</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> MPD Copy Stream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Dec 04, 2021</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive</li>
                    <li><span class="text-bold text-primary">Fixed</span> Proxy Rotator</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Nov 26, 2021</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive, Youtube</li>
                    <li><span class="text-bold text-primary">Fixed</span> Video Cache</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Nov 23, 2021</strong>
                <ul>
                    <li><span class="text-bold text-success">Improved</span> Copy Stream Performance</li>
                    <li><span class="text-bold text-success">Improved</span> Security</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive Sharer, REST API</li>
                    <li><span class="text-bold text-primary">Fixed</span> GoFile, Streamable, Streamtape, Tiktok</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Nov 20, 2021</strong>
                <ul>
                    <li><span class="text-bold text-danger">Removed</span> NinjaStream, Okstream</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive, HxFile</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive Sharer, REST API</li>
                    <li><span class="text-bold text-primary">Fixed</span> Subtitle Color on Plyr</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-info">Updated</span> PHP Libraries</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Nov 09, 2021</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Upstream</li>
                    <li><span class="text-bold text-primary">Fixed</span> VAST on Plyr</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Oct 30, 2021</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Mixdrop, mp4upload, Vupload, Yourupload, Youtube</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-info">Updated</span> PHP Libraries</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Oct 25, 2021</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> Google VPAID on Plyr</li>
                    <li><span class="text-bold text-primary">Fixed</span> VAST position on Plyr</li>
                    <li><span class="text-bold text-primary">Fixed</span> OneDrive, Uploads.mobi</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Oct 20, 2021</strong>
                <ul>
                    <li><span class="text-bold text-success">Improved</span> Copy Stream Performance</li>
                    <li><span class="text-bold text-primary">Fixed</span> Blogger, Direct Host, Files.fm, Gofile, Google Photos, OneDrive, pCloud, Soundcloud, Streamable, StreamSB, Tiktok, VUP</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-info">Updated</span> PHP Libraries</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Oct 05, 2021</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> multi VAST on Plyr</li>
                    <li><span class="text-bold text-primary">Fixed</span> Fembed, Google Drive, Racaty, StreamSB</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive Anti-Limit</li>
                    <li><span class="text-bold text-primary">Fixed</span> Cache System</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-info">Updated</span> PHP Libraries</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Sep 23, 2021</strong>
                <ul>
                    <li><span class="text-bold text-success">Improved</span> Copy Stream Performance</li>
                    <li><span class="text-bold text-success">Improved</span> Cache System</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS & MPD live streaming</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS & MPD cache</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-info">Updated</span> PHP Libraries</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Aug 30, 2021</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> Media Session API</li>
                    <li><span class="text-bold text-success">Added</span> PWA Info Options (Icon, Theme, etc)</li>
                    <li><span class="text-bold text-success">Added</span> Plyr Video Player</li>
                    <li><span class="text-bold text-success">Added</span> Poster Uploader</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Drive, Ninjastream, Yandex Disk</li>
                    <li><span class="text-bold text-primary">Fixed</span> HLS & MPD Cache</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                    <li><span class="text-bold text-info">Updated</span> PHP Libraries</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Aug 02, 2021</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> MPD Bypass Feature</li>
                    <li><span class="text-bold text-success">Improved</span> Copy Stream Performance</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>July 29, 2021</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Fembed, Uptobox</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>July 21, 2021</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Fembed, Google Drive, Google Sharer</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>July 12, 2021</strong>
                <ul>
                    <li><span class="text-bold text-danger">Removed</span> DoodStream, MegaUp, Vidmoly</li>
                    <li><span class="text-bold text-primary">Fixed</span> Gofile, Google Drive, Google Photos, MixDrop, Solidfiles, Uploads.mobi, Vidoza</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>July 11, 2021</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Uptobox</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Sharer</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>July 1, 2021</strong>
                <ul>
                    <li><span class="text-bold text-primary">Fixed</span> Ninjastream, StreamSB</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li class="mb-3">
                <strong>Jun 30, 2021</strong>
                <ul>
                    <li><span class="text-bold text-success">Added</span> Archive, Dailymotion, pCloud, Tiktok</li>
                    <li><span class="text-bold text-success">Improved</span> Copy Stream Performance</li>
                    <li><span class="text-bold text-primary">Fixed</span> Google Photos, Solidfiles, Facebook, Hexupload, Streamtape</li>
                    <li><span class="text-bold text-primary">Fixed</span> Some Bugs</li>
                </ul>
            </li>
            <li>
                <strong>May 01, 2020</strong>
                <ul>
                    <li><span class="text-bold text-success">Initial Release</span></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<?php
include 'footer.php';
