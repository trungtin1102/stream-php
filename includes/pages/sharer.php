<?php
if (!defined('BASE_DIR')) die('access denied!');

session_write_close();
ob_start();
header('Developed-By: TvHay.top');
header('X-Frame-Options: SAMEORIGIN');

$title = 'Google Drive Direct Link Generator & Downloader';
$sitedescription = 'Generate direct links and download Google Drive files without signing in to a Google account.';

$is_admin = is_admin();
$disableSharer = filter_var(get_option('disable_gsharer'), FILTER_VALIDATE_BOOLEAN);
$recaptcha_site_key = get_option('recaptcha_site_key');

$ip = getUserIP();
$token = encode($ip);
$directAdsLink = !filter_var(get_option('disable_direct_ads'), FILTER_VALIDATE_BOOLEAN) ? get_option('direct_ads_link') : '#';

$enableAjaxDownload = get_option('enable_gdrive_downloader');
$enableAjaxDownload = filter_var($enableAjaxDownload, FILTER_VALIDATE_BOOLEAN);

$disableAds = filter_var(get_option('disable_banner_ads'), FILTER_VALIDATE_BOOLEAN);

$jsCode = "$('#frmBypassLimit').on('submit', function(e) {
    var btnText = $('#submit').html();
    $.ajax({
        url: baseURL + 'ajax/',
        method: 'POST',
        dataType: 'json',
        cache: false,
        xhrFields: {
            withCredentials: true
        },
        data: $(this).serialize() + '&token=" . $token . "',
        beforeSend: function() {
            $('#submit').html('<span class=\"spinner-border spinner-border-sm\" role=\"status\" aria-hidden=\"true\"></span> Please wait...').prop('disabled', true);
        },
        complete: function() {
            $('#submit').html(btnText).prop('disabled', false);
        },
        success: function(res) {
            if (res.status === 'success') {
                $('#bypassedLink').val(res.result.link);
                $('#bypassedLink').attr('data-id', res.result.id);
                $('#bypassedLinkDL').prop('disabled', false);
                swal('Success!', res.message, 'success');
            } else {
                swal('Error!', res.message, 'error');
            }
        }
    });
    e.preventDefault();
});
function DownloadGDrive() {
    var btnText = $('#bypassedLinkDL').html(),
        fileID = $('#bypassedLink').data('id'),
        accessToken = '',
        captcha = '';
    var downloadCompleted = function(){
        $('#bypassedLinkDL').html(btnText).prop('disabled', false);
        $('#bypassedLinkProgress').addClass('d-none');
    };
    if ($('#captcha-response').length) captcha = $('#captcha-response').val();
    $.ajax({
        url: baseURL + 'ajax/',
        method: 'POST',
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        data: 'action=gdriveAccessToken&token=" . $token . "&captcha-response=' + captcha,
        beforeSend: function() {
            $('#bypassedLinkDL').html('<span class=\"spinner-border spinner-border-sm\" role=\"status\" aria-hidden=\"true\"></span> Please wait...').prop('disabled', true);
        },
        success: function(res) {
            if (res.status === 'success') {
                accessToken = res.result.access_token;
                $.ajax({
                    type: 'GET',
                    url: 'https://www.googleapis.com/drive/v2/files/' + fileID + '?alt=json',
                    headers: {
                        authorization: 'Bearer ' + accessToken
                    },
                    success: function(res) {
                        var fileName = res.originalFilename;
                        $.ajax({
                            url: 'https://www.googleapis.com/drive/v2/files/' + fileID + '?alt=media&source=downloadUrl',
                            method: 'GET',
                            headers: {
                                authorization: 'Bearer ' + accessToken
                            },
                            beforeSend: function() {
                                $('#bypassedLinkProgress').removeClass('d-none');
                            },
                            xhr: function() {
                                var xhr = new XMLHttpRequest();
                                xhr.onreadystatechange = function() {
                                    if (xhr.readyState === 2) {
                                        if (xhr.status === 200) {
                                            xhr.responseType = 'blob';
                                        } else {
                                            xhr.responseType = 'text';
                                        }
                                    }
                                };
                                xhr.addEventListener('progress', function(e) {
                                    if (e.lengthComputable) {
                                        var percent = parseInt((e.loaded / e.total) * 100);
                                        $('#bypassedLinkProgress .progress-bar').attr('style', 'width:' + percent + '%').attr('aria-valuenow', percent).text(percent + '%');
                                        if (percent >= 100) {
                                            var completed = setTimeout(function() {
                                                downloadCompleted();
                                                clearTimeout(completed);
                                            }, 3000);
                                        }
                                    }
                                }, false);
                                return xhr;
                            },
                            success: function(data) {
                                var blob = new Blob([data], {
                                    type: 'application/octet-stream'
                                });
                                var isIE = false || !!document.documentMode;
                                if (isIE) {
                                    window.navigator.msSaveBlob(blob, fileName);
                                } else {
                                    var url = window.URL || window.webkitURL,
                                        a = $('<a />');
                                    link = url.createObjectURL(blob);
                                    a.attr('download', fileName);
                                    a.attr('href', link);
                                    $('body').append(a);
                                    a[0].click();
                                    $('body').remove(a);
                                }
                            },
                            error: function(xhr) {
                                downloadCompleted();
                                swal('Error!', 'Cannot download the file!', 'error');
                            }
                        });
                    },
                    error: function(xhr) {
                        downloadCompleted();
                        swal('Error!', 'Could not load the file info!', 'error');
                    }
                });
            } else {
                downloadCompleted();
                swal('Error!', res.message, 'error');
            }
        },
        error: function(xhr) {
            downloadCompleted();
            swal('Error!', 'Cannot create access token!', 'error');
        }
    });
}";
$productionMode = get_option('production_mode');
if (filter_var($productionMode, FILTER_VALIDATE_BOOLEAN)) {
    $jsCode .= 'function devtoolIsOpening() {
        console.clear();
        var before = new Date().getTime();
        debugger;
        var after = new Date().getTime();
        if (after - before > 200) {
            document.write(" Dont open Developer Tools. ");
            window.location.replace("https://www.google.com");
        }
        setTimeout(devtoolIsOpening, 100);
    }
    devtoolIsOpening();';
    $class = new \Obfuscator();
    $jsCode = $class->jsObfustator(trim($jsCode), $token, 'gdriveAntiLimit', $ip);
}

include 'header.php';
if (!$disableSharer || $is_admin) :
?>
    <div class="row py-5 bg-custom text-center">
        <div class="col">
            <h1 class="h3">Google Drive Direct Link Generator & Downloader</h1>
            <p>Generate direct link and download Google Drive file without signing in to a Google account.</p>
        </div>
    </div>
    <div class="row pt-5">
        <div class="col-12 mb-3"><?php echo !$disableAds ? htmlspecialchars_decode(get_option('sh_banner_top')) : ''; ?></div>
        <div class="col-12">
            <form id="frmBypassLimit" method="post">
                <div class="form-group">
                    <label for="gdrive_id">Google Drive ID</label>
                    <input type="text" name="gdrive_id" id="gdrive_id" class="form-control" placeholder="1KPtwrGMyjC9c_ZpOID4eLaSDErrA5x3O or https://drive.google.com/file/d/1KPtwrGMyjC9c_ZpOID4eLaSDErrA5x3O/view" required>
                </div>
                <div class="form-group text-center">
                    <?php
                    $recaptcha_site_key = get_option('recaptcha_site_key');
                    if ($recaptcha_site_key) : ?>
                        <div id="g-recaptcha" class="g-recaptcha" data-sitekey="<?php echo $recaptcha_site_key; ?>" data-size="invisible" data-callback="gCallback"></div>
                        <input type="hidden" id="captcha-response" name="captcha-response" />
                    <?php endif; ?>
                    <input type="hidden" name="action" value="gdriveBypassLimit">
                    <button id="submit" type="submit" class="btn btn-custom btn-block">
                        <i aria-hidden="true" class="fas fa-cog"></i>
                        <span class="ml-2">Generate Direct Link</span>
                    </button>
                </div>
                <?php
                if (strpos(BASE_URL, 'TvHay') !== false || strpos(BASE_URL, '.test') !== false || strpos(BASE_URL, 'localhost') !== false) {
                    include 'includes/sponsored.php';
                }
                ?>
                <?php if ($enableAjaxDownload) : ?>
                    <div class="form-group">
                        <div class="alert alert-info">The file name may be different than the original file name. If you find something like that, rename the file when you download it.</div>
                    </div>
                    <div class="form-group">
                        <div class="alert alert-warning">
                            <strong>If the direct link doesn't work then download the file via the Download Now button.</strong>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="result">Direct Link</label>
                        <div class="input-group">
                            <input id="bypassedLink" type="text" onfocus="this.select()" data-id="" class="form-control" placeholder="The direct link is here!" aria-describedby="bypassedLinkDL" readonly>
                            <div class="input-group-append">
                                <button class="btn btn-outline-custom" type="button" id="bypassedLinkDL" onclick="DownloadGDrive()" disabled>
                                    <i class="fas fa-download mr-2"></i> Download Now
                                </button>
                            </div>
                        </div>
                        <div id="bypassedLinkProgress" class="d-none mt-3">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
                            </div>
                            <div class="alert alert-warning mt-3"><i class="fas fa-exclamation-triangle mr-2"></i> Do not reload or close this page during the download process!</div>
                        </div>
                    </div>
                <?php else : ?>
                    <div class="form-group">
                        <div class="alert alert-info">The file name may be different than the original file name. If you find something like that, rename the file when you download it.</div>
                    </div>
                    <div class="form-group">
                        <input id="bypassedLink" type="text" onfocus="this.select()" data-id="" class="form-control" placeholder="The direct link is here!" aria-describedby="bypassedLinkDL" readonly>
                    </div>
                <?php endif; ?>
            </form>
        </div>
        <div class="col-12 mb-3"><?php echo !$disableAds ? htmlspecialchars_decode(get_option('sh_banner_bottom')) : ''; ?></div>
    </div>
<?php else : ?>
    <div class="row pt-5">
        <div class="col text-center">
            <h1 class="h3 text-danger"><strong>402</strong> Unauthorized!</h1>
            <h3 class="h4 text-secondary">This page can only be accessed by registered users.</h3>
        </div>
    </div>
<?php
endif;
include 'footer.php';
