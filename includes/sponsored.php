<div class="row my-2">
    <div class="col">
        <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/owl.carousel@latest/dist/assets/owl.carousel.min.css">
        <style>
            .owl-header {
                bottom: 0;
                margin: 0;
                padding: 0;
                position: absolute;
                z-index: 2;
                background: rgba(0, 0, 0, .7);
                color: #fff;
                width: 100%;
            }

            .owl-title {
                margin: 0;
                padding: 10px;
                width: 220px;
                text-overflow: ellipsis;
                overflow: hidden;
                word-break: break-word;
                white-space: nowrap;
                font-size: 14px;
                font-family: Arial, sans-serif;
                font-weight: 400;
            }

            .owl-theme .owl-nav {
                display: none;
            }

            .owl-dots {
                margin-top: 10px !important;
            }

            .owl-carousel .owl-video-tn {
                background-size: cover;
                padding-bottom: 56.25%;
                /* 16:9 */
                padding-top: 25px;
            }

            .owl-video-frame {
                position: relative;
                padding-bottom: 56.25%;
                /* 16:9 */
                padding-top: 25px;
                height: 0;
            }

            .owl-video-frame iframe {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }

            .owl-dots {
                text-align: center;
                margin-top: 20px;
            }

            .owl-dot {
                display: inline-block;
            }

            .owl-dot span {
                width: 11px;
                height: 11px;
                background-color: #ccc;
                border-radius: 50%;
                display: block;
                margin: 5px 3px;
            }

            .owl-dot.active span {
                background-color: #000;
            }
        </style>
        <h3 class="h5">Ads</h3>
        <div class="owl-carousel owl-theme">
            <?php
            $list = [];
            $adsSorted = ['p', 'da', 'dd'];
            $adsSort = array_rand($adsSorted);
            $sort = $adsSorted[$adsSort];
            $class = new \InstanceCache();
            $class->setKey('youtube_ads_youtechnoid_' . $sort);
            $cache = $class->get();
            if ($cache) {
                $list = $cache;
            } else {
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://www.youtube.com/c/Youtechnoid/videos?view=0&sort=$sort&flow=grid",
                    CURLOPT_SSL_VERIFYHOST => 0,
                    CURLOPT_SSL_VERIFYPEER => 0,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => array(
                        'Host: www.youtube.com',
                        'Origin: https://www.youtube.com',
                        'Referer: https://www.youtube.com/',
                        'Cookie: GPS=1; VISITOR_INFO1_LIVE=GjJq4eTFQx0; YSC=CNdGt8oP0YQ'
                    ),
                    CURLOPT_TCP_NODELAY => true,
                    CURLOPT_TCP_FASTOPEN => true,
                    CURLOPT_FAILONERROR => true,
                ));
                $response = curl_exec($curl);
                $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                $err = curl_error($curl);
                curl_close($curl);
                if ($status >= 200 && $status < 400) {
                    $data = get_string_between($response, 'ytInitialData =', ';');
                    $data = @json_decode($data, true);
                    if (json_last_error() === JSON_ERROR_NONE) {
                        if (isset($data['contents']['twoColumnBrowseResultsRenderer']['tabs'])) {
                            $content = [];
                            foreach ($data['contents']['twoColumnBrowseResultsRenderer']['tabs'] as $tab) {
                                if (isset($tab['tabRenderer']['selected']) && $tab['tabRenderer']['selected'] && !empty($tab['tabRenderer']['content']['sectionListRenderer']['contents'][0]['itemSectionRenderer']['contents'][0]['gridRenderer']['items'])) {
                                    $content = $tab['tabRenderer']['content']['sectionListRenderer']['contents'][0]['itemSectionRenderer']['contents'][0]['gridRenderer']['items'];
                                    break;
                                }
                            }
                            if (!empty($content)) {
                                $limit = 18;
                                $initNum = 0;
                                foreach ($content as $k => $v) {
                                    if (isset($v['gridVideoRenderer']) && $initNum < $limit) {
                                        $v = $v['gridVideoRenderer'];
                                        $title = explode(' ', $v['title']['runs'][0]['text']);
                                        $title = array_filter($title, function ($v) {
                                            return strpos($v, '#') === false;
                                        });
                                        $title = trim(implode(' ', $title));
                                        $list[] = array(
                                            'title' => $title,
                                            'videoId' => $v['videoId']
                                        );
                                        $initNum++;
                                    }
                                }
                                $class->save($list, 3600, 'youtube_ads');
                            }
                        }
                    }
                } else {
                    error_log("youtube ads error => $status: $err");
                }
            }
            if (!empty($list)) {
                foreach ($list as $v) {
                    echo '<div class="item-video" data-merge="1"><a class="owl-video" href="https://www.youtube.com/watch?v=' . $v['videoId'] . '"></a><div class="owl-header"><h3 class="owl-title">' . $v['title'] . '</h3></div></div>';
                }
            }
            ?>
        </div>
        <script src="//cdn.jsdelivr.net/npm/owl.carousel@latest/dist/owl.carousel.min.js" defer></script>
        <script>
            $(document).ready(function() {
                $('.owl-carousel').owlCarousel({
                    items: 3,
                    loop: true,
                    video: true,
                    lazyLoad: false,
                    nav: false,
                    margin: 10,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: true,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 1,
                            nav: true
                        },
                        480: {
                            items: 2,
                            nav: false
                        },
                        720: {
                            items: 3,
                            nav: false
                        },
                        960: {
                            items: 4,
                            nav: false
                        }
                    },
                    onInitialized: function() {
                        getOwlWidth();
                    },
                    onResized: function() {
                        getOwlWidth();
                    }
                });
                $('.owl-item').click(function(e) {
                    window.open($(this).data('video'), '_blank');
                    e.stopPropagation();
                    e.preventDefault();
                });
            });

            function getOwlWidth() {
                $('.owl-title').css('width', $('.owl-video-tn').width().toString() + 'px');
            }
        </script>
    </div>
</div>