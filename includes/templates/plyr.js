var player,
    hls,
    shakaPlayer,
    logStat,
    hosts = {hosts},
    timeElapse = "latestplay.{localKey}",
    lastTime = xStorage.getItem(timeElapse),
    retryKey = "retry.{localKey}",
    retry = Math.round(xStorage.getItem(retryKey)),
    controls = ["play-large", "pause-large", "progress", "play", "rewind", "fast-forward", "mute", "volume", "live", "current-time", "duration"],
    title = "{title}",
    plyrConfig = {
        ads: {
            enabled: false,
            tagUrl: ''
        },
        urls: {
            download: ''
        },
        controls: controls,
        ratio: "16:9",
        autoplay: {autoplay},
        muted: {mute},
        loop: {
            active: {repeat}
        },
        captions: {
            active: true,
            language: 'auto',
            update: true
        },
        tooltips: {
            controls: true,
            seek: true
        },
        settings: ['quality', 'captions'],
        iconUrl: "{thisSite}assets/img/plyr-custom.svg",
        blankVideo: "{thisSite}assets/blank.mp4",
    },
    poster = '{poster}',
    sources = [],
    sources_alt = [],
    tracks = [],
    videoType = "video/mp4",
    engine,
    enableP2P = {enableP2P},
    isP2PSupported = enableP2P && typeof p2pml !== "undefined" && p2pml.core.HybridLoader.isSupported(),
    p2pConfig = {
        segments: {
            swarmId: "{localKey}"
        },
        loader: {
            trackerAnnounce: {torrentList}
        }
    },
    visitAdsOnplay = {visitAdsOnplay},
    showDownloadButton = {showDownloadButton},
    enableDownloadPage = {enableDownloadPage},
    statCounted = false,
    vast = {vastAds},
    adBlocker = {block_adblocker};
window.timeElapse = timeElapse;
window.retryKey = retryKey;

if ('schedule' in vast && vast.schedule.length > 0) {
    plyrConfig.ads = {
        enabled: true,
        tagUrl: vast.schedule[0].tag
    };
}

if (showDownloadButton && enableDownloadPage) {
    controls.push('download');
    plyrConfig.urls = {
        download: "{downloadLink}"
    };
}

if({displayRateControls}){
    plyrConfig['settings'].push("speed");
}

controls.push("captions", "settings", "pip", "googlecast", "airplay", "fullscreen");

var init = {
    isLive: false,
    ads: {
        manager: null,
        mute: false,
        fullscreen: false,
        playing: false,
        timer: false,
        createControls: function(e) {
            $('.plyr__ads').append('<div class="plyr__controls"><div class="plyr__custom__controls"><button type="button" id="adsPlay" class="plyr__controls__item plyr__control plyr__control--pressed"></svg><svg class="icon--pressed" role="presentation" focusable="false"><use xlink:href="{thisSite}assets/img/plyr-custom.svg#plyr-pause"></use></svg><svg class="icon--not-pressed" role="presentation" focusable="false"><use xlink:href="{thisSite}assets/img/plyr-custom.svg#plyr-play"></use></button><button type="button" id="adsVolume" class="plyr__control"><svg class="icon--pressed" role="presentation" focusable="false"><use xlink:href="{thisSite}assets/img/plyr-custom.svg#plyr-muted"></use></svg><svg class="icon--not-pressed" role="presentation" focusable="false"><use xlink:href="{thisSite}assets/img/plyr-custom.svg#plyr-volume"></use></svg></button><div id="adsTimer" class="plyr__controls__item plyr__time--current plyr__time"></div><button type="button" id="adsFullscreen" class="plyr__controls__item plyr__control" style="margin-left:auto"><svg class="icon--pressed" role="presentation" focusable="false"><use xlink:href="{thisSite}assets/img/plyr-custom.svg#plyr-exit-fullscreen"></use></svg><svg class="icon--not-pressed" role="presentation" focusable="false"><use xlink:href="{thisSite}assets/img/plyr-custom.svg#plyr-enter-fullscreen"></use></svg></button></div></div>');
            init.ads.events(e);
            if (!init.ads.timer) {
                init.ads.timer = setInterval(function() {
                    $('#adsTimer').text($('.plyr__ads').attr('data-badge-text'));
                }, 500);
            }
        },
        removeContainerClasses: function(e) {
            $('.plyr__ads .plyr__controls').remove();
        },
        events: function(e) {
            var adManager = e.detail.plyr.ads.manager;
            document.querySelector('#adsPlay').addEventListener('click', function(e) {
                if (init.ads.playing) {
                    this.classList.remove('plyr__control--pressed');
                    adManager.pause();
                    init.ads.playing = false;
                } else {
                    this.classList.add('plyr__control--pressed');
                    adManager.resume();
                    init.ads.playing = true;
                }
            });
            document.querySelector('#adsVolume').addEventListener('click', function(e) {
                if (init.ads.mute) {
                    this.classList.remove('plyr__control--pressed');
                    adManager.setVolume(1);
                    init.ads.mute = false;
                } else {
                    this.classList.add('plyr__control--pressed');
                    adManager.setVolume(0);
                    init.ads.mute = true;
                }
            });
            document.querySelector('#adsFullscreen').addEventListener('click', function(e) {
                if (init.ads.fullscreen) {
                    this.classList.remove('plyr__control--pressed');
                    player.fullscreen.exit();
                    init.ads.fullscreen = false;
                } else {
                    this.classList.add('plyr__control--pressed');
                    player.fullscreen.enter();
                    init.ads.fullscreen = true;
                }
            });
        },
        initialize: function(p) {
            p.on("adsloaded", function(e) {
                $('#resume').hide();
                init.ads.playing = true;
                init.ads.removeContainerClasses(e);
                init.ads.createControls(e);
            });
            p.on("adstarted", function(e) {
                $('#resume').hide();
                init.ads.playing = true;
                document.querySelector('#adsPlay').classList.add('plyr__control--pressed');
            });
            p.on('adscontentresume', function(e) {
                $('#resume').hide();
                init.ads.playing = true;
                document.querySelector('#adsPlay').classList.add('plyr__control--pressed');
            });
            p.on('adscontentpause', function(e) {
                var adManager = e.detail.plyr.ads.manager;
                adManager.pause();
                $('#resume').hide();
                init.ads.playing = false;
                document.querySelector('#adsPlay').classList.remove('plyr__control--pressed');
            });
            p.on('adsclick', function(e) {
                if ($('.plyr').hasClass('plyr--ad-nonlinear')) {
                    init.ads.playing = false;
                    $('.plyr__ads').hide();
                    clearInterval(init.ads.timer);
                    init.ads.timer = false;
                    timeChecker();
                } else {
                    var adManager = e.detail.plyr.ads.manager;
                    adManager.pause();
                    $('#resume').hide();
                    init.ads.playing = false;
                    document.querySelector('#adsPlay').classList.remove('plyr__control--pressed');
                }
            });
            p.on("adscomplete adsallcomplete", function(e) {
                init.ads.playing = false;
                init.ads.removeContainerClasses(e);
                clearInterval(init.ads.timer);
                init.ads.timer = false;
                timeChecker();
                e.target.classList.remove('plyr--ad-loaded');
                e.target.classList.remove('plyr--ad-linear');
                e.target.classList.remove('plyr--ad-nonlinear');
                e.target.classList.remove('plyr--hide-controls');
            });
        }
    },
    fembedStat: function() {
        if (hosts.indexOf('fembed') > -1) {
            logStat = setInterval(function() {
                $.ajax({
                    url: "{lbSite}ajax/?action=stat&data={cacheKey}",
                    method: "GET",
                    dataType: "json",
                    cache: false,
                    timeout: 30000,
                    success: function(res) {},
                    error: function(xhr) {}
                });
            }, 3000);
        }
    },
    loadAlternative: function() {
        var $next, $prev, $link, 
            $server = $('#servers li'),
            sChecked = xStorage.getItem('retry_multi~{localKey}'),
            checked = sChecked !== null ? JSON.parse(sChecked) : [];
        showLoading();
        if (checked.length < $server.length) {
            if (checked.length < $server.length) {
                $server.each(function(i, e) {
                    if ($(this).find('a').hasClass('active')) {
                        $next = $(this).next();
                        $prev = $(this).prev();
                        if ($next.length > 0) {
                            checked.push(i);
                            xStorage.setItem('retry_multi~{localKey}', JSON.stringify(checked));
                            $link = $next.find('a').attr('href');
                        } else if ($prev.length > 0) {
                            checked.push(i);
                            xStorage.setItem('retry_multi~{localKey}', JSON.stringify(checked));
                            $link = $prev.find('a').attr('href');
                        }
                        return;
                    }
                });
                window.location.href = $link;
            } else {
                showMessage(res.message);
                gtagReport("video_error", res.message, "plyr", false);
            }
        } else {
            showMessage('Sorry this video is unavailable.');
            gtagReport("video_error", 'Sources not found', "plyr", false);
        }
    },
    loadExternalTracks: function() {
        if (tracks.length > 0) {
            $.each(tracks, function(i, e) {
                $('video').append('<track kind="subtitles" src="' + e.file.replace('https:', '').replace('http:', '') + '" label="' + e.label + '" ' + (i === 0 ? 'default' : '') + '>');
            });
        }
    },
    updateTime: function(time) {
        lastTime = Math.round(xStorage.getItem(timeElapse));
        if (time > 0 && time > lastTime) {
            xStorage.setItem(timeElapse, time);
            xStorage.removeItem(retryKey);
        }
        if (time >= {visit_counter_runtime} && statCounted === false) {
            $.ajax({
                url: "{lbSite}ajax/?action=stat&data={cacheKey}",
                method: "GET",
                dataType: "json",
                cache: false,
                timeout: 30000,
                success: function(res) {},
                error: function(xhr) {}
            });
        }
    },
    retry: function() {
        retry = Math.round(xStorage.getItem(retryKey));
        xStorage.setItem(retryKey, retry == null || retry == 'NaN' ? 1 : (retry + 1));
    },
    retryAjax: function() {
        init.retry();
        xStorage.removeItem("plyr");
        $.ajax({
            url: "{lbSite}ajax/?action=clear-cache&data={cacheKey}",
            method: "GET",
            dataType: "json",
            cache: false,
            timeout: 30000,
            success: function(res) {
                xStorage.setItem("autoplay", true);
                gtagReport("video_error", "Reload Page", "plyr", false);
                location.reload(true);
            },
            error: function(xhr) {
                gtagReport("video_error", "Unable to load source", "plyr", false);
                showMessage('Unable to load source. <a href="javascript:void(0)" onclick="xStorage.clear();location.reload()">Reload Page</a>');
            }
        });
    },
    loadPlayer: function() {
        if (videoType === "hls") {
            if (Hls.isSupported()) {
                init.hls.load(sources[0].file, resume);
            } else {
                gtagReport("video_error", "This browser not supported! " + navigator.userAgent, "plyr", false);
                showMessage('This browser not supported!');
            }
        } else if (videoType === "mpd") {
            if (shaka.Player.isBrowserSupported()) {
                shaka.polyfill.installAll();
                init.mpd.load(sources[0].file, resume);
            } else {
                gtagReport("video_error", "This browser not supported! " + navigator.userAgent, "plyr", false);
                showMessage('This browser not supported!');
            }
        } else {
            init.mp4.load(sources, resume);
        }
    },
    initialize: function() {
        var lang = window.navigator.languages ? window.navigator.languages[0] : 'en';
        lang = lang || window.navigator.language || window.navigator.browserLanguage || window.navigator.userLanguage;
        $.ajax({
            url: "{thisSite}assets/vendor/plyr/translations/"+ lang.substring(0, 2) +".json", 
            dataType: 'json', 
            timeout: 30000,
            success: function(res) {
                plyrConfig.i18n = res;
                init.loadPlayer();
            },
            error: function() {
                init.loadPlayer();
            }
        });
    },
    onTimeUpdate: function() {
        var time = Math.round(player.currentTime);
        init.updateTime(time);
    },
    onError: function(e) {
        var err = e.detail.plyr.media.error;
        if (err !== null) {
            if ($('#servers li').length > 0) {
                init.loadAlternative();
            } else {
                showLoading();
                retry = Math.round(xStorage.getItem(retryKey));
                if (retry === null || retry === 'NaN' || Math.round(retry) < 3) {
                    if ('message' in err) {
                        if (err.message.indexOf('PIPELINE_ERROR_DECODE') > -1) {
                            showPlayer();
                        } else {
                            showMessage(err.message);
                            gtagReport("video_error", err.message, "plyr", false);
                            init.retryAjax();
                        }
                    } else {
                        showMessage('Sorry this video is unavailable. <a href="javascript:void(0)" onclick="xStorage.clear();location.reload()">Reload Page</a>');
                        gtagReport("video_error", 'Video is unavailable', "plyr", false);
                    }
                } else {
                    showMessage(err.message);
                    gtagReport("video_error", 'Video is unavailable', "plyr", false);
                }
            }
        } else {
            showPlayer();
        }
    },
    onPlay: function() {
        xStorage.removeItem('autoplay');
        if ("{directAdsLink}" !== '' && "{directAdsLink}" !== '#' && visitAdsOnplay) {
            var wo = window.open("{directAdsLink}", "_blank");
            var directAdsTimer = setTimeout(function() {
                if (_hasPopupBlocker(wo) || typeof window.canRunAds == "undefined") {
                    $("#iframeAds").attr("src", "{directAdsLink}");
                    $("#directAds").show();
                    clearTimeout(directAdsTimer);
                }
            }, 3000);
        }
        $('.plyr-title').hide();
        timeChecker();
        init.fembedStat();
    },
    onPause: function() {
        if (logStat) clearInterval(logStat);
    },
    onEnded: function() {
        $('.plyr-title').show();
        gtagReport("video_complete", "Playback Has Ended", "plyr", false);
        xStorage.removeItem(timeElapse);
        if (logStat) clearInterval(logStat);
    },
    onEnterFullscreen: function() {
        $('.plyr__video-wrapper').attr('style', '');
        $('.plyr video').css('position', 'absolute').css('top', 0).css('bottom', 0);
        $(document).find('#adsFullscreen').addClass('plyr__control--pressed');
    },
    onExitFullscreen: function() {
        $(document).find('#adsFullscreen').removeClass('plyr__control--pressed');
    },
    mp4: {
        parseSources: function(sources) {
            var newSources = [], sizes = [];
            if (sources.length > 0) {
                $.each(sources, function(i, e) {
                    size = Math.round(e.label.replace('Default', 360).replace('Original', 720).replace('p', ''));
                    if (size > 0 && sizes.indexOf(size) === -1) {
                        sizes.push(size);
                        newSources.push({
                            src: e.file,
                            type: e.type,
                            size: size
                        });
                    }
                });
            }
            return {
                'sources': newSources,
                'sizes': sizes
            };
        },
        load: function(sources, resume) {
            if (player) {
                player.destroy();
                player = null;
            }
            
            var newTracks = [], sourceParser = init.mp4.parseSources(sources), defaultRes = {default_resolution}, selectDefaultRes = 360;
            
            $.each(tracks, function(i, e){
                newTracks.push({
                    src: e.file,
                    label: e.label,
                    kind: 'subtitles',
                    default: (typeof e.default !== 'undefined' ? e.default : false)
                });
            });

            $.each(sourceParser.sizes, function(i, e) {
                if (e >= 100 && e < 200) label = 100;
                else if (e >= 200 && e < 300) label = 200;
                else if (e >= 300 && e < 400) label = 300;
                else if (e >= 400 && e < 500) label = 400;
                else if (e >= 500 && e < 600) label = 500;
                else if (e >= 600 && e < 700) label = 600;
                else if (e >= 700 && e < 800) label = 700;
                else if (e >= 800 && e < 900) label = 800;
                else if (e >= 900 && e < 1000) label = 900;
                else if (e >= 1000) label = 1000;
                if (label === defaultRes) selectDefaultRes = e;
            });

            plyrConfig.quality = {
                default: selectDefaultRes,
                options: sourceParser.sizes,
                forced: true
            };

            player = new Plyr('#videoContainer', plyrConfig);
            player.source = {
                type: "video",
                title: title,
                sources: sourceParser.sources,
                tracks: newTracks
            };
            player.poster = poster;
            player.on("ready", function() {
                init.isLive = false;
                loadHTML();
            });
            player.on("loadedmetadata", function(e) {
                player.currentTrack = 0;
            });
            player.on("timeupdate", init.onTimeUpdate);
            player.on("pause", init.onPause);
            player.on("enterfullscreen", init.onEnterFullscreen);
            player.on("exitfullscreen", init.onExitFullscreen);
            player.once("play", init.onPlay);
            player.once("ended", init.onEnded);
            player.once("error", init.onError);
            init.ads.initialize(player);

            if (typeof resume === 'number') {
                $('#resume').hide();
                setTimeout(function() {
                    player.speed = 1;
                    player.currentTime = resume;
                    if (!player.playing) player.play();
                }, 1000);
            }

            window.player = player;
        }
    },
    hls: {
        resume: undefined,
        load: function(source, resume) {
            init.hls.resume = resume;
            if (hls) {
                hls.destroy();
                clearInterval(hls.bufferTimer);
                hls = null;
            }
            if (player) {
                player.destroy();
                player = null;
            }
            plyrConfig.source = {
                type: 'hls',
                src: source
            };
            if (isP2PSupported) {
                engine = new p2pml.hlsjs.Engine(p2pConfig);
                hls = new Hls({
                    liveSyncDurationCount: 7,
                    loader: engine.createLoaderClass()
                });
            } else {
                hls = new Hls();
            }
            hls.attachMedia(document.querySelector('#videoContainer'));
            hls.on(Hls.Events.MEDIA_ATTACHED, function() {
                hls.loadSource(source);
            });
            hls.on(Hls.Events.MANIFEST_PARSED, function(e, d) {
                if (typeof d.levels[0].details !== 'undefined') init.isLive = d.levels[0].details.live;
                
                var qualitySelector = [0], ql = 0, qli = null, bitrate = 0, bitlength = 0, fixed = 0;
                $.each(d.levels, function(i, e) {
                    if (typeof(e.height) !== 'undefined' && e.height) {
                        bitrate = 'bitrate' in e ? Math.floor(e.bitrate / 1000) : 0;
                        bitlength = e.bitrate.toString().length;
                        fixed = (bitlength - 3) > 1 ? bitrate.toString().length : 1;
                        ql = parseFloat(e.height + '.' + bitrate).toFixed(fixed);
                        qualitySelector.push(ql);
                    }
                });
                if (qualitySelector.length > 2) {
                    qualitySelector.sort(function(a, b) {
                        var x = a.toString().split('.'), y = b.toString().split('.');
                        if (parseInt(x[0]) === parseInt(y[0])) {
                            return parseInt(x[1]) - parseInt(y[1]);
                        } else {
                            return a - b;
                        }
                    });
                    plyrConfig.quality = {
                        default: 0,
                        options: qualitySelector,
                        forced: true,
                        onChange: function(quality) {
                            var span = $(".plyr__menu__container [data-plyr='quality'][value='0'] span");
                            if (quality === 0) {
                                hls.currentLevel = -1;
                                if (!qli) {
                                    qli = setInterval(function() {
                                        if (hls.currentLevel > -1) span.html(plyrConfig.i18n.auto + ' (' + hls.levels[hls.currentLevel].height + 'p)');
                                        else span.html(plyrConfig.i18n.auto);
                                    }, 1000);
                                }
                            } else {
                                var x = quality.toString().split('.');
                                quality = hls.levels.findIndex(function(e) {
                                    return e.height === Number(x[0]) && Math.floor(e.bitrate / 1000) === Number(x[1]);
                                });
                                hls.currentLevel = quality;
                                if (qli) {
                                    clearInterval(qli);
                                    qli = null;
                                }
                                span.html(plyrConfig.i18n.auto);
                            }
                        }
                    };
                }

                var textTracks, hlsTextTracks, hlsTextStartIndex = -1, defaultTextTrack = -1, hideTextTracks;

                if (init.isLive) {
                    plyrConfig.ads.enabled = false;
                    $('.plyr__progress').hide();
                }
                player = new Plyr('#videoContainer', plyrConfig);
                player.poster = poster;
                player.on("ready", function() {
                    loadHLSAudio();
                    init.loadExternalTracks();
                    loadHTML();
                });
                player.on("timeupdate", init.onTimeUpdate);
                player.on("pause", init.onPause);
                player.on("loadedmetadata", function(e) {
                    hlsTextTracks = hls.subtitleTracks;
                    textTracks = document.querySelector('#videoContainer').textTracks;
                    if (textTracks.length > 0) {
                        hideTextTracks = function() {
                            textTracks.forEach(function(e) {
                                if (e.mode !== 'disabled') e.mode = 'disabled';
                            });
                        };
                        defaultTextTrack = hlsTextTracks.findIndex(function(e) {
                            return e.default;
                        });
                        if (hlsTextTracks.length > 0) {
                            textTracks.forEach(function(e, i) {
                                if (e.label === hlsTextTracks[0].name && e.language === hlsTextTracks[0].lang && e.kind === hlsTextTracks[0].type.toLowerCase()) {
                                    hlsTextStartIndex = i;
                                    return;
                                }
                            });
                        }
                        hideTextTracks();
                        if (defaultTextTrack > -1) {
                            player.currentTrack = defaultTextTrack;
                            textTracks[defaultTextTrack].mode = 'showing';
                        } else {
                            player.currentTrack = 0;
                            textTracks[0].mode = 'showing';
                        }
                    }
                });
                player.on("languagechange captionsenabled", function(e) {
                    var selected = player.currentTrack;
                    textTracks = document.querySelector('#videoContainer').textTracks;
                    if (player.currentTrack > -1) {
                        if (hideTextTracks) hideTextTracks();
                        if (player.currentTrack >= hlsTextStartIndex) hls.subtitleTrack = player.currentTrack;
                        if (textTracks) textTracks[selected].mode = 'showing';
                    }
                });
                player.on("enterfullscreen", init.onEnterFullscreen);
                player.on("exitfullscreen", init.onExitFullscreen);
                player.once("play", init.onPlay);
                player.once("ended", init.onEnded);
                init.ads.initialize(player);
                
                if (xStorage.getItem('autoplay') === 'true') {
                    $('#resume').hide();
                    setTimeout(function(){
                        if (typeof init.hls.resume === 'number') {
                            player.currentTime = init.hls.resume;
                            if (!player.playing) player.play();
                        }
                    }, 1000);
                }
                window.player = player;
            });
            hls.on(Hls.Events.ERROR, function(e, d) {
                if (d.fatal) {
                    if ('response' in d && (d.response.code === 0 || d.response.code >= 400)) {
                        if ($('#servers li').length > 0) {
                            init.loadAlternative();
                        } else {
                            showLoading();
                            retry = Math.round(xStorage.getItem(retryKey));
                            if (retry === null || retry === 'NaN' || Math.round(retry) < 3) {
                                init.retryAjax();
                            } else {
                                showMessage(d.details + ' <a href="javascript:void(0)" onclick="xStorage.clear();location.reload()">Reload Page</a>');
                                gtagReport("video_error", d.details, "plyr", false);
                                hls.destroy();
                                player.destroy();
                            }
                        }
                    } else {
                        switch (d.type) {
                            case Hls.ErrorTypes.NETWORK_ERROR:
                                gtagReport("video_error", 'fatal network error encountered, try to recover', "plyr", false);
                                setTimeout(function () {
                                    hls.startLoad();
                                }, 2000);
                                break;
    
                            case Hls.ErrorTypes.MEDIA_ERROR:
                                gtagReport("video_error", 'fatal media error encountered, try to recover', "plyr", false);
                                hls.recoverMediaError();
                                setTimeout(function () {
                                    hls.startLoad();
                                }, 2000);
                                break;
                                
                            default:
                                showLoading();
                                retry = Math.round(xStorage.getItem(retryKey));
                                if (retry === null || retry === 'NaN' || Math.round(retry) < 3) {
                                    init.retryAjax();
                                } else {
                                    showMessage(d.details + ' <a href="javascript:void(0)" onclick="xStorage.clear();location.reload()">Reload Page</a>');
                                    gtagReport("video_error", d.details, "plyr", false);
                                    hls.destroy();
                                    player.destroy();
                                }
                                break;
                        }
                    }
                }
            });
            hls.on(Hls.Events.LEVEL_UPDATED, function(e, d) {
                var span = $(".plyr__menu__container [data-plyr='quality'][value='0'] span");
                if (hls.autoLevelEnabled) {
                    span.html(plyrConfig.i18n.auto + ' (' + hls.levels[d.level].height + 'p)');
                } else {
                    span.html(plyrConfig.i18n.auto);
                }
                if ('live' in d.details) {
                    init.isLive = d.details.live;
                    if (init.isLive) {
                        $('.plyr__progress').hide();
                    }
                }
            });
            window.hls = hls;
        }
    },
    mpd: {
        startTrackValue: -1,
        selectTrack: function(selected) {
            var $subtitle = $(".plyr__menu__container [data-plyr='language'][value='" + selected + "']"),
                $div = $(".plyr__menu__container > div > div"), $home;
            $(".plyr__menu__container [data-plyr='language']").attr('aria-checked', 'false');
            if ($subtitle.text().indexOf('Shaka Player') > -1) {
                $subtitle = $subtitle.next();
            }
            $subtitle.attr('aria-checked', 'true');
            $div.each(function(i, e) {
                if ($(this).attr('id').indexOf('home') > -1) {
                    $home = $(this);
                    return;
                }
            });
            $home.find('button[data-plyr="settings"]').each(function(i, e) {
                if ($(this).text().indexOf(plyrConfig.i18n.captions) > -1) {
                    $(this).find('.plyr__menu__value').text($subtitle.text().replace($subtitle.find('.plyr__menu__value').text(), ''));
                    return;
                }
            });
        },
        onLoadError: function(e) {
            if ($('#servers li').length > 0) {
                init.loadAlternative();
            } else {
                showLoading();
                retry = Math.round(xStorage.getItem(retryKey));
                if (retry === null || retry === 'NaN' || Math.round(retry) < 3) {
                    init.retryAjax();
                } else {
                    var err = init.mpd.errorDetail(e.code);
                    $('#message').css('font-size', '16px');
                    showMessage(err.name +'! '+ err.description + ' <a href="javascript:void(0)" onclick="xStorage.clear();location.reload()">Reload Page</a>');
                }
            }
        },
        errorDetail: function(code) {
            if (code) {
                var errors = xStorage.getItem('shakaPlayer-error-codes');
                if (errors) {
                    return JSON.parse(errors).find(function(obj) {
                        return obj.code === code;
                    });
                }
            }
            return {
                code: undefined,
                name: 'Browser Error',
                description: 'Failed to init decoder'
            };
        },
        load: function(source, resume) {
            if (shakaPlayer) {
                shakaPlayer.detach();
                shakaPlayer.unload();
                shakaPlayer.destroy();
                shakaPlayer = null;
            }
            if (player) {
                player.destroy();
                player = null;
            }
            plyrConfig.source = {
                type: 'mpd',
                src: source
            };
            if (xStorage.getItem('shakaPlayer-error-codes') === null) {
                $.ajax({
                    url: '{thisSite}assets/vendor/shaka-player/error-codes.json',
                    dataType: 'json',
                    timeout: 30000,
                    success: function(res) {
                        xStorage.setItem('shakaPlayer-error-codes', JSON.stringify(res));
                    },
                    error: function(){}
                });
            }
            var shakaConfig = {
                abr: {
                    enabled: true
                },
                manifest: {
                    dash: {
                        autoCorrectDrift: true,
                        ignoreEmptyAdaptationSet: true
                    }
                },
                streaming: {
                    rebufferingGoal: 0.01
                },
            };
            if (!isP2PSupported) {
                shakaConfig.streaming = {
                    lowLatencyMode: true,
                    inaccurateManifestTolerance: 0,
                    rebufferingGoal: 0.01
                };
            }
            shakaPlayer = new shaka.Player(document.querySelector('#videoContainer'));
            shakaPlayer.configure(shakaConfig);
            if (isP2PSupported) {
                engine = new p2pml.shaka.Engine(p2pConfig);
                engine.initShakaPlayer(shakaPlayer);
            }
            shakaPlayer.load(source).then(function(e) {
                init.isLive = shakaPlayer.isLive();

                var qualitySelector = [0], ql = 0, qli = null, bandwidth = 0, bitlength = 0, fixed = 0;
                $.each(shakaPlayer.getVariantTracks(), function(i, e) {
                    if (typeof(e.height) !== 'undefined' && e.height) {
                        bandwidth = 'bandwidth' in e ? Math.floor(e.bandwidth / 1000) : 0;
                        bitlength = e.bandwidth.toString().length;
                        fixed = (bitlength - 3) > 1 ? bandwidth.toString().length : 1;
                        ql = parseFloat(e.height + '.' + bandwidth).toFixed(fixed);
                        qualitySelector.push(ql);
                    }
                });
                if (qualitySelector.length > 2) {
                    qualitySelector.sort(function(a, b) {
                        var x = a.toString().split('.'), y = b.toString().split('.');
                        if (parseInt(x[0]) === parseInt(y[0])) {
                            return parseInt(x[1]) - parseInt(y[1]);
                        } else {
                            return a - b;
                        }
                    });
                    plyrConfig.quality = {
                        default: 0,
                        options: qualitySelector,
                        forced: true,
                        onChange: function(quality) {
                            var span = $(".plyr__menu__container [data-plyr='quality'][value='0'] span");
                            if (quality === 0) {
                                shakaPlayer.configure({
                                    abr: {
                                        enabled: true
                                    },
                                    streaming: {
                                        rebufferingGoal: 0.01
                                    }
                                });
                                if (!qli) {
                                    qli = setInterval(function() {
                                        var quality = shakaPlayer.getVariantTracks().find(function(e) {
                                            return e.active === true;
                                        });
                                        span.html(plyrConfig.i18n.auto + ' (' + quality.height + 'p)');
                                    }, 1000);
                                }
                            } else {
                                var x = quality.toString().split('.');
                                quality = shakaPlayer.getVariantTracks().find(function(e) {
                                    return e.height === Number(x[0]) && Math.floor(e.bandwidth / 1000) === Number(x[1]) && e.type === 'variant';
                                });
                                shakaPlayer.configure({
                                    abr: {
                                        enabled: false
                                    },
                                    streaming: {
                                        rebufferingGoal: 0.01
                                    }
                                });
                                shakaPlayer.selectVariantTrack(quality, true);
                                if (qli) {
                                    clearInterval(qli);
                                    qli = null;
                                }
                                span.html(plyrConfig.i18n.auto);
                            }
                        }
                    };
                }

                var $video = document.querySelector('video#videoContainer'), track;
                $.each(shakaPlayer.getTextTracks(), function(i, e) {
                    track = $video.addTextTrack('captions', (e.label ? e.label : e.language), e.language);
                    track.id = e.id;
                });
                $.each($video.textTracks, function(i, e) {
                    if (e.label === 'Shaka Player TextTrack') {
                        init.mpd.startTrackValue = i;
                    }
                });

                if (init.isLive) {
                    plyrConfig.ads.enabled = false;
                }
                player = new Plyr('#videoContainer', plyrConfig);
                player.poster = poster;
                player.currentTrack = init.mpd.startTrackValue;
                player.on("ready", function() {
                    loadHTML();
                });
                player.on("loadedmetadata", function() {
                    $(".plyr__menu__container [data-plyr='language'][value='" + init.mpd.startTrackValue + "']").hide();
                    player.speed = 1;
                    if (shakaPlayer.getTextTracks().length > 0) {
                        player.currentTrack = 0;
                        shakaPlayer.selectTextTrack(0);
                        shakaPlayer.setTextTrackVisibility(true);
                        init.mpd.selectTrack(1);
                    } else {
                        $('button[data-plyr="captions"]').hide();
                    }
                    init.loadExternalTracks();
                });
                player.on("timeupdate", init.onTimeUpdate);
                player.on("pause", init.onPause);
                player.on("qualitychange", function(e) {
                    var span = $(".plyr__menu__container [data-plyr='quality'][value='0'] span");
                    if (e.detail.quality === 0) {
                        if (qli == null) {
                            qli = setInterval(function() {
                                var quality = shakaPlayer.getVariantTracks().find(function(e) {
                                    return e.active === true;
                                });
                                span.html(plyrConfig.i18n.auto + ' (' + quality.height + 'p)');
                            }, 1000);
                        }
                    } else {
                        span.html(plyrConfig.i18n.auto);
                    }
                });
                player.on("languagechange captionsenabled", function(e) {
                    var selected = player.currentTrack,
                        textTracks = document.querySelector('video#videoContainer').textTracks,
                        shakaTextTracks = shakaPlayer.getTextTracks(),
                        mpdSelected = -1;

                    $.each(textTracks, function(i, e) {
                        e.mode = 'hidden';
                    });
                    if (selected > -1) {
                        if (selected >= init.mpd.startTrackValue) {
                            if (selected > init.mpd.startTrackValue) {
                                mpdSelected = (init.mpd.startTrackValue + selected) - 1;
                            } else {
                                mpdSelected = init.mpd.startTrackValue;
                            }
                            player.currentTrack = init.mpd.startTrackValue;
                            shakaPlayer.selectTextTrack(shakaTextTracks[mpdSelected]);
                            shakaPlayer.setTextTrackVisibility(true);
                        } else {
                            player.currentTrack = selected;
                            shakaPlayer.setTextTrackVisibility(false);
                            player.captions.currentTrackNode.mode = 'showing';
                        }
                        init.mpd.selectTrack(selected);
                    } else {
                        player.currentTrack = -1;
                        shakaPlayer.setTextTrackVisibility(false);
                    }
                });
                player.on("enterfullscreen", init.onEnterFullscreen);
                player.on("exitfullscreen", init.onExitFullscreen);
                player.once("play", init.onPlay);
                player.once("ended", init.onEnded);
                init.ads.initialize(player);

                if (typeof resume === 'number') {
                    $('#resume').hide();
                    setTimeout(function() {
                        player.speed = 1;
                        player.currentTime = resume;
                        if (!player.playing) player.play();
                    }, 1000);
                }
            }).catch(init.mpd.onLoadError);
            window.shakaPlayer = shakaPlayer;
            window.player = player;
        }
    }
};
window.init = init;

if (hosts.indexOf('fembed') === -1) {
    $(document).ajaxSend(function(res, xhr, opt) {
        if (opt.url.indexOf('{lbSite}ajax/?action=stat') > -1) {
            if (statCounted) {
                xhr.abort();
            } else {
                statCounted = true;
            }
        }
    });
}

$(document).on('contextmenu', function(e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
});

function sandboxDetector() {
    window.sandboxed = false;
    try {
        if (window.frameElement.hasAttribute("sandbox")) window.sandboxed = true;
        return;
    } catch (t) {}
    try {
        document.domain = document.domain;
    } catch (t) {
        try {
            if (-1 != t.toString().toLowerCase().indexOf("sandbox")) window.sandboxed = true;
            return;
        } catch (t) {}
    }
    try {
        if (!window.navigator.plugins["namedItem"]("Chrome PDF Viewer")) return false;
    } catch (e) {
        return false;
    }
    var e = document.createElement('object');
    e.data = "data:application/pdf;base64,aG1t";
    e.style = "position:absolute;top:-500px;left:-500px;visibility:hidden;";
    e.onerror = function() {
        window.sandboxed = true;
    };
    e.onload = function() {
        e.parentNode.removeChild(e);
    };
    document.body.appendChild(e);
    setTimeout(function() {
        if (e.parentNode !== null) e.parentNode.removeChild(e);
    }, 150);
}
sandboxDetector();

if (!window.sandboxed) {
    justDetectAdblock.detectAnyAdblocker().then(function(d) {
        if (adBlocker && (d || typeof canRunAds === 'undefined')) {
            destroyer();
        } else {
            if (sources.length > 0) {
                videoType = sources[0].type;
                init.initialize();
            } else if (sources_alt.length > 0) {
                sources = sources_alt;
                sources_alt = [];
                videoType = sources[0].type;
                init.initialize();
            } else {
                $.ajax({
                    url: "{lbSite}api/?{originQry}",
                    type: "GET",
                    dataType: "json",
                    cache: false,
                    timeout: 30000,
                    success: function(res) {
                        var isOK = false;
                        if (res.status === "ok") {
                            poster = res.poster;
                            $('.plyr-title').text(res.title);
                            if ('sources' in res) {
                                sources = res.sources;
                                tracks = res.tracks;
                                if (sources.length > 0) {
                                    isOK = true;
                                    videoType = sources[0].type;
                                    init.initialize();
                                }
                            }
                        }
                        if (!isOK) {
                            if ($('#servers li').length > 0) {
                                var $next, $prev, $link, 
                                    $server = $('#servers li'),
                                    sChecked = xStorage.getItem('retry_multi~{localKey}'),
                                    checked = sChecked !== null ? JSON.parse(sChecked) : [];
                                
                                if (checked.length < $server.length) {
                                    if (checked.length < $server.length) {
                                        $server.each(function(i, e) {
                                            if ($(this).find('a').hasClass('active')) {
                                                $next = $(this).next();
                                                $prev = $(this).prev();
                                                if ($next.length > 0) {
                                                    checked.push(i);
                                                    xStorage.setItem('retry_multi~{localKey}', JSON.stringify(checked));
                                                    $link = $next.find('a').attr('href');
                                                } else if ($prev.length > 0) {
                                                    checked.push(i);
                                                    xStorage.setItem('retry_multi~{localKey}', JSON.stringify(checked));
                                                    $link = $prev.find('a').attr('href');
                                                }
                                                return;
                                            }
                                        });
                                        window.location.href = $link;
                                    } else {
                                        showMessage(res.message);
                                        gtagReport("video_error", res.message, "plyr", false);
                                    }
                                } else {
                                    showMessage('Sorry this video is unavailable.');
                                    gtagReport("video_error", 'Sources not found', "plyr", false);
                                }
                            } else {
                                if (res.status === 'ok' && res.sources.length === 0) {
                                    showMessage('Sorry this video is unavailable.');
                                    gtagReport("video_error", 'Sources not found', "plyr", false);
                                } else {
                                    showMessage(res.message);
                                    gtagReport("video_error", res.message, "plyr", false);
                                }
                            }
                        }
                    },
                    error: function(xhr) {
                        showMessage('Failed to fetch video sources from server! <a href="javascript:void(0)" onclick="xStorage.clear();location.reload()">Reload Page</a>');
                        gtagReport("video_error", 'Failed to fetch video sources from server', "plyr", false);
                    }
                });
            }
        }
    });
} else {
    showMessage('Sandboxed embed is not allowed!');
}

function destroyer() {
    if (hls) {
        hls.destroy();
        clearInterval(hls.bufferTimer);
        hls = null;
    }
    if (shakaPlayer) {
        shakaPlayer.detach();
        shakaPlayer.unload();
        shakaPlayer.destroy();
        shakaPlayer = null;
    }
    if (player) {
        player.destroy();
        player = null;
    }
    showMessage('<p><img src="{thisSite}assets/img/stop-sign-hand.webp" width="100" height="100"></p><p>Please support us by disabling AdBlocker.</p>');
}