var sources = [],
    sources_alt = [],
    tracks = [],
    directAdsLink = '{directAdsLink}';

function tracksParser(tracks) {
    if (tracks.length > 0) {
        $.each(tracks, function(i, e) {
            if (directAdsLink !== '') {
                $("#dlWrapper").append('<a href="javascript:void(0)" class="btn btn-success btn-lg btn-block btn-download" onclick="showAds(\'' + e.file + '\', \'' + directAdsLink + '\')">Download ' + e.label + ' Subtitle</a>');
            } else {
                $("#dlWrapper").append('<a href="' + e.file + '" class="btn btn-success btn-lg btn-block btn-download">Download ' + e.label + ' Subtitle</a>');
            }
        });
    }
}

function sourcesParser(sources, alt) {
    var color = alt ? 'secondary' : 'primary';
    if (sources.length > 0) {
        $.each(sources, function(i, e) {
            if (directAdsLink !== '') {
                $("#dlWrapper").append('<a href="javascript:void(0)" class="btn btn-'+ color +' btn-lg btn-block btn-download" onclick="showAds(\'' + e.file + '\', \'' + directAdsLink + '\')">Download ' + e.label + ' Video</a>');
            } else {
                $("#dlWrapper").append('<a href="' + e.file + '" class="btn btn-'+ color +' btn-lg btn-block btn-download" target="_blank">Download ' + e.label + ' Video</a>');
            }
        });
    }
}

function loadSources() {
    $.ajax({
        url: "{lbSite}api/?{originQry}",
        type: "GET",
        dataType: "json",
        cache: false,
        timeout: 30000,
        beforeSend: function() {
            $("#dlWrapper").html('<div class="d-flex justify-content-center"><div class="spinner-grow text-success" role="status" style="width:5rem;height:5rem"><span class="sr-only">Loading...</span></div></div>');
        },
        success: function(res) {
            var isOK = false;
            
            $("#dlWrapper").html("");
            $(".btn-watch").show();

            if (res.status === "ok") {
                if (res.sources.length > 0) {
                    isOK = true;
                    sourcesParser(res.sources, false);
                    tracksParser(res.tracks);
                }
            }
            if (!isOK) {
                if ($('#server-list').length > 0) {
                    var $li, $next, $prev, 
                        $server = $('#server-list li a'),
                        sChecked = xStorage.getItem('retry_download~{localKey}'),
                        checked = sChecked !== null ? JSON.parse(sChecked) : [];
                    
                    if (checked.length < $server.length) {
                        $server.each(function(i, e) {
                            if ($(this).hasClass('active')) {
                                $li = $(this).closest('li');
                                $next = $li.next();
                                $prev = $li.prev();
                                if ($next.length > 0) {
                                    checked.push(i);
                                    window.location.href = $next.find('a').attr('href');
                                } else if ($prev.length > 0) {
                                    checked.push(i);
                                    window.location.href = $prev.find('a').attr('href');
                                } else {
                                    checked.push(0);
                                    window.location.href = $('#server-list li').eq(0).find('a').attr('href');
                                }
                                return false;
                            }
                        });
                        xStorage.setItem('retry_download~{localKey}', JSON.stringify(checked));
                    } else {
                        showMessage(res.message);
                        gtagReport("download_error", res.message, "download", false);
                    }
                } else {
                    if (res.status === 'ok' && res.sources.length === 0) {
                        showMessage('Sorry this video is unavailable.');
                        gtagReport("download_error", 'Sources not found', "download", false);
                    } else {
                        showMessage(res.message);
                        gtagReport("download_error", res.message, "download", false);
                    }
                }
            }
        },
        error: function(xhr) {
            if (xhr.status >= 500) {
                loadSources();
            } else {
                var res = JSON.parse(xhr.responseText);
                $(".btn-watch, .alert, h1").hide();
                if (typeof res.status !== "undefined") {
                    $("#dlWrapper").html("<h3 class=\"text-danger text-center\">" + res.message + "</h3>");
                } else {
                    $("#dlWrapper").html(xhr.responseText);
                }
            }
        }
    });
}

if (sources.length > 0) {
    sourcesParser(sources, false);
    tracksParser(tracks);
} else if (sources_alt.length > 0) {
    sourcesParser(sources_alt, true);
    tracksParser(tracks);
} else {
    loadSources();
}

$(document).on('contextmenu', function (e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
});