var player = jwplayer("videoContainer"), 
    sources_alt = [],
    timeElapse = "latestplay.{localKey}",
    retryKey = "retry.{localKey}",
    lastTime = xStorage.getItem(timeElapse),
    retry = xStorage.getItem(retryKey),
    enableP2P = {enableP2P},
    hosts = {hosts},
    logStat,
    jwpConfig = {
        title: "{title}",
        autostart: {autoplay},
        repeat: {repeat},
        mute: {mute},
        image: '{poster}',
        rewind: false,
        abouttext: "GunDeveloper.com",
        aboutlink: "https://gundeveloper.com",
        controls: true,
        hlshtml: true,
        primary: "html5",
        preload: "{preload}",
        cast: {},
        androidhls: true,
        stretching: "{stretching}",
        displaytitle: {displayTitle},
        displaydescription: false,
        playbackRateControls: {displayRateControls},
        captions: {
            color: "{captionsColor}",
            backgroundOpacity: 0
        },
        sources: [],
        tracks: [],
        aspectratio: "16:9",
        floating: false,
        skin: loadSkin("{playerSkin}"),
        advertising: {vastAds}
    },
    statCounted = false,
    adBlocker = {block_adblocker},
    enableShare = {enableShare},
    visitAdsOnplay = {visitAdsOnplay};
window.timeElapse = timeElapse;
window.retryKey = retryKey;

if (typeof jwplayer().key === 'undefined') {
    jwpConfig.key = 'ITWMv7t88JGzI0xPwW8I0+LveiXX9SWbfdmt0ArUSyc=';
}

if ("{logoImage}" !== "") {
    jwpConfig.logo = {
        "file": "{logoImage}",
        "link": "{logoLink}",
        "hide": {logoHide},
        "position": "{logoPosition}"
    };
}

if (enableShare) {
    jwpConfig.sharing = {
        "sites": ["facebook", "twitter", "email"]
    };
}

function sandboxDetector() {
    window.sandboxed = false;
    try {
        if (window.frameElement.hasAttribute("sandbox")) window.sandboxed = true;
        return;
    } catch (t) {}
    try {
        document.domain = document.domain;
    } catch (t) {
        try {
            if (-1 != t.toString().toLowerCase().indexOf("sandbox")) window.sandboxed = true;
            return;
        } catch (t) {}
    }
    try {
        if (!window.navigator.plugins["namedItem"]("Chrome PDF Viewer")) return false;
    } catch (e) {
        return false;
    }
    var e = document.createElement('object');
    e.data = "data:application/pdf;base64,aG1t";
    e.style = "position:absolute;top:-500px;left:-500px;visibility:hidden;";
    e.onerror = function() {
        window.sandboxed = true;
    };
    e.onload = function() {
        e.parentNode.removeChild(e);
    };
    document.body.appendChild(e);
    setTimeout(function() {
        if (e.parentNode !== null) e.parentNode.removeChild(e);
    }, 150);
}
sandboxDetector();

if (!window.sandboxed) {
    justDetectAdblock.detectAnyAdblocker().then(function(d) {
        if (adBlocker && (d || typeof canRunAds === 'undefined')) {
            destroyer();
        } else {
            if (jwpConfig.sources.length > 0) {
                loadPlayer(false);
            } else {
                $.ajax({
                    url: "{lbSite}api/?{originQry}",
                    type: "GET",
                    dataType: "json",
                    cache: false,
                    timeout: 30000,
                    success: function(res) {
                        var isOK = false;
                        if (res.status === "ok") {
                            jwpConfig.title = res.title;
                            jwpConfig.tracks = res.tracks;
                            jwpConfig.image = res.poster;
        
                            if (res.sources.length > 0) {
                                isOK = true;
                                jwpConfig.sources = res.sources;
                                loadPlayer(false);
                            } else {
                                showMessage(res.message);
                            }
                        }
                        if (!isOK) {
                            if ($('#servers li').length > 0) {
                                var $next, $prev, $link, 
                                    $server = $('#servers li'),
                                    sChecked = xStorage.getItem('retry_multi~{localKey}'),
                                    checked = sChecked !== null ? JSON.parse(sChecked) : [];
                                
                                if (checked.length < $server.length) {
                                    if (checked.length < $server.length) {
                                        $server.each(function(i, e) {
                                            if ($(this).find('a').hasClass('active')) {
                                                $next = $(this).next();
                                                $prev = $(this).prev();
                                                if ($next.length > 0) {
                                                    checked.push(i);
                                                    xStorage.setItem('retry_multi~{localKey}', JSON.stringify(checked));
                                                    $link = $next.find('a').attr('href');
                                                } else if ($prev.length > 0) {
                                                    checked.push(i);
                                                    xStorage.setItem('retry_multi~{localKey}', JSON.stringify(checked));
                                                    $link = $prev.find('a').attr('href');
                                                }
                                                return;
                                            }
                                        });
                                        window.location.href = $link;
                                    } else {
                                        showMessage(res.message);
                                        gtagReport("video_error", res.message, "jwplayer", false);
                                    }
                                } else {
                                    showMessage('Sorry this video is unavailable.');
                                    gtagReport("video_error", 'Sources not found', "jwplayer", false);
                                }
                            } else {
                                if (res.status === 'ok' && res.sources.length === 0) {
                                    showMessage('Sorry this video is unavailable.');
                                    gtagReport("video_error", 'Sources not found', "jwplayer", false);
                                } else {
                                    showMessage(res.message);
                                    gtagReport("video_error", res.message, "jwplayer", false);
                                }
                            }
                        }
                    },
                    error: function(xhr) {
                        showMessage('Failed to fetch video sources from server! <a href="javascript:void(0)" onclick="xStorage.clear();location.reload()">Reload Page</a>');
                        gtagReport("video_error", 'Failed to fetch video sources from server', "jwplayer", false);
                    }
                });
            }
        }
    });
} else {
    showMessage('Sandboxed embed is not allowed!');
}

if (hosts.indexOf('fembed') > -1) {
    logStat = setInterval(function() {
        $.ajax({
            url: "{lbSite}ajax/?action=stat&data={cacheKey}",
            method: "GET",
            dataType: "json",
            cache: false,
            timeout: 30000,
            success: function(res) {},
            error: function(xhr) {}
        });
    }, 3000);
}

$(document).ajaxSend(function (res, xhr, opt) {
    if (opt.url.indexOf('{lbSite}ajax/?action=stat') > -1) {
        if (statCounted) {
            xhr.abort();
        } else {
            statCounted = true;
        }
    }
});

function errorHandler(e) {
    showLoading();
    retry = xStorage.getItem(retryKey);
    if (e.code === 221000 && (retry === null || retry === 'NaN' || parseInt(retry) < 3)) {
        xStorage.setItem(retryKey, retry === null || retry === 'NaN' ? 1 : parseInt(retry) + 1);
        xStorage.setItem('autoplay', 'true');
        loadPlayer(true);
    } else {
        if (sources_alt.length > 0) {
            jwpConfig.sources = sources_alt;
            loadPlayer(true);
            return;
        } else {
            if ($('#servers li').length > 0) {
                var $next, $prev, $link, 
                    $server = $('#servers li'),
                    sChecked = xStorage.getItem('retry_multi~{localKey}'),
                    checked = sChecked !== null ? JSON.parse(sChecked) : [];
                showLoading();
                if (checked.length < $server.length) {
                    if (checked.length < $server.length) {
                        $server.each(function(i, e) {
                            if ($(this).find('a').hasClass('active')) {
                                $next = $(this).next();
                                $prev = $(this).prev();
                                if ($next.length > 0) {
                                    checked.push(i);
                                    xStorage.setItem('retry_multi~{localKey}', JSON.stringify(checked));
                                    $link = $next.find('a').attr('href');
                                } else if ($prev.length > 0) {
                                    checked.push(i);
                                    xStorage.setItem('retry_multi~{localKey}', JSON.stringify(checked));
                                    $link = $prev.find('a').attr('href');
                                }
                                return;
                            }
                        });
                        window.location.href = $link;
                    } else {
                        showMessage(res.message);
                        gtagReport("video_error", res.message, "jwplayer", false);
                    }
                } else {
                    showMessage('Sorry this video is unavailable.');
                    gtagReport("video_error", 'Sources not found', "jwplayer", false);
                }
            } else if (retry === null || retry === 'NaN' || parseInt(retry) < 3) {
                xStorage.setItem(retryKey, retry === null || retry === 'NaN' ? 1 : parseInt(retry) + 1);
                xStorage.setItem("autoplay", true);
                xStorage.removeItem("jwplayer.qualityLabel");
                $.ajax({
                    url: "{lbSite}ajax/?action=clear-cache&data={cacheKey}",
                    method: "GET",
                    dataType: "json",
                    cache: false,
                    timeout: 30000,
                    success: function(res) {
                        gtagReport("video_error", "Reload Page", "jwplayer", false);
                        location.reload();
                    },
                    error: function(xhr) {
                        gtagReport("video_error", "Unable to load source", "jwplayer", false);
                        showMessage('Unable to load source. <a href="javascript:void(0)" onclick="xStorage.clear();location.reload()">Reload Page</a>');
                    }
                });
                return;
            } else if (e.code === 301161 && e.sourceError === null) {
                gtagReport("video_error", "Redirect to HTTPS", "jwplayer", false);
                location.href = location.href.replace('http:', 'https:');
                return;
            } else {
                var errDetail = e.sourceError !== null && 'message' in e.sourceError ? e.sourceError.message : '';
                gtagReport("video_error", e.message + ' ' + errDetail, "jwplayer", false);
                showMessage(e.message + ' ' + errDetail + ' <a href="javascript:void(0)" onclick="xStorage.clear();location.reload()">Reload Page</a>');
                return;
            }
        }
    }
}

function loadPlayer(resume) {
    player.setup(jwpConfig);
    if (enableP2P && p2pml.core.HybridLoader.isSupported() && typeof jwpConfig.advertising.schedule === 'undefined') {
        jwplayer_hls_provider.attach();
        var engine,
            config = {
                segments: {
                    swarmId: '{localKey}'
                },
                loader: {
                    trackerAnnounce: {torrentList}
                }
            };
        engine = new p2pml.hlsjs.Engine(config);
        p2pml.hlsjs.initJwPlayer(player, {
            liveSyncDurationCount: 7,
            loader: engine.createLoaderClass()
        });
    }
    addButton(player);
    player.on("setupError error", errorHandler);
    player.once("ready", function (e) {
        var autoplay = xStorage.getItem("autoplay");
        if ("{playerSkin}" === "netflix" || "{playerSkin}" === "hotstar") {
            $(".jw-slider-time").prepend($(".jw-text-elapsed")).append($(".jw-text-duration"));
        }
        $("#mContainer").hide();
        $("#videoContainer").show();
        if (autoplay === "true") {
            xStorage.removeItem("autoplay");
            player.play();
            if (resume) player.seek(xStorage.getItem(timeElapse));
        }
        gtagReport("video_ready_to_play", "Ready To Play", "jwplayer", false);
    });
    player.once("beforePlay", function () {
        var $jrwn = $(".jw-icon-rewind"),
            $rwn = $("[button=\"rewind\"]"),
            $fwd = $("[button=\"forward\"]");
        if ($jrwn.length) {
            $jrwn.after($rwn);
            $rwn.after($fwd);
        }
        timeChecker();
        if (player.getCaptionsList().length > 1) player.setCurrentCaptions(1);
    });
    player.on("time", function (e) {
        lastTime = xStorage.getItem(timeElapse);
        if (e.position > 0 && e.position > lastTime) {
            xStorage.setItem(timeElapse, Math.round(e.position));
            xStorage.removeItem(retryKey);
        }
        if (e.position >= {visit_counter_runtime} && statCounted === false) {
            $.ajax({
                url: "{lbSite}ajax/?action=stat&data={cacheKey}",
                method: "GET",
                dataType: "json",
                cache: false,
                timeout: 30000,
                success: function(res) {},
                error: function(xhr) {}
            });
        }
    });
    player.once("complete playlistComplete", function (e) {
        gtagReport("video_complete", "Playback Has Ended", "jwplayer", false);
        xStorage.removeItem(timeElapse);
        if (logStat) clearInterval(logStat);
    });
    if ("{directAdsLink}" !== '' && "{directAdsLink}" !== '#' && visitAdsOnplay) {
        player.once("play", function () {
            var wo = window.open("{directAdsLink}", "_blank");
            setTimeout(function () {
                if (_hasPopupBlocker(wo) || typeof window.canRunAds === "undefined") {
                    $("#iframeAds").attr("src", "{directAdsLink}");
                    $("#directAds").show();
                }
            }, 3000);
        });
    }
    window.player = player;
}

function destroyer() {
    if (player) {
        player.remove();
        player = null;
    }
    showMessage('<p><img src="{thisSite}assets/img/stop-sign-hand.webp" width="100" height="100"></p><p>Please support us by disabling AdBlocker.</p>');
}
