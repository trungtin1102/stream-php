var hosts = {hosts},
    poster = '{poster}',
    sources = [],
    sources_alt = [],
    tracks = [],
    videoType = "video/mp4",
    hls,
    shakaPlayer,
    logStat,
    statCounted = false;

var initPlayer = function() {
    if (videoType === "hls") {
        if (Hls.isSupported()) {
            init.hls.load(sources[0].file);
        } else {
            gtagReport("video_error", "This browser not supported! " + navigator.userAgent, 'smartTV', false);
            showMessage('This browser not supported!');
        }
    } else if (videoType === "mpd") {
        if (shaka.Player.isBrowserSupported()) {
            shaka.polyfill.installAll();
            init.mpd.load(sources[0].file);
        } else {
            gtagReport("video_error", "This browser not supported! " + navigator.userAgent, 'smartTV', false);
            showMessage('This browser not supported!');
        }
    } else {
        init.mp4.load(sources);
    }
};

var loadPlayer = function() {
    $.ajax({
        url: "{lbSite}api/?{originQry}",
        type: "GET",
        dataType: "json",
        cache: false,
        timeout: 30000,
        success: function(res) {
            if (res.status !== "fail") {
                poster = res.poster;
                if ('sources' in res) {
                    sources = res.sources;
                    tracks = res.tracks;
                    if (sources.length > 0) {
                        videoType = sources[0].type;
                        initPlayer();
                    }
                }
            } else {
                showMessage(res.message);
                gtagReport("video_error", res.message, 'smartTV', false);
            }
        },
        error: function(xhr) {
            showMessage('Failed to fetch video sources from server! <a href="javascript:void(0)" onclick="xStorage.clear();location.reload()">Reload Page</a>');
            gtagReport("video_error", 'Failed to fetch video sources from server', "plyr", false);
        }
    });
};

var init = {
    retry: 0,
    isLive: false,
    loadAlternative: function() {
        var $next, $link, $server = $('#servers li');
        showLoading();
        if ($server.length > 0) {
            $server.each(function(i, e) {
                if ($(this).find('a').hasClass('active')) {
                    $next = $(this).next();
                    if ($next.length > 0) {
                        $link = $next.find('a').attr('href');
                    }
                    return;
                }
            });
            window.location.href = $link;
        } else if (init.retry < 3) {
            init.retryAjax();
        } else {
            showMessage('Sorry this video is unavailable.');
            gtagReport("video_error", 'Sources not found', "plyr", false);
        }
    },
    fembedStat: function() {
        if (hosts.indexOf('fembed') > -1) {
            logStat = setInterval(function() {
                $.ajax({
                    url: "{lbSite}ajax/?action=stat&data={cacheKey}",
                    method: "GET",
                    dataType: "json",
                    cache: false,
                    timeout: 30000,
                    success: function(res) {},
                    error: function(xhr) {}
                });
            }, 3000);
        }
    },
    loadExternalTracks: function() {
        if (tracks.length > 0) {
            $.each(tracks, function(i, e) {
                $('video').append('<track kind="subtitles" src="' + e.file.replace('https:', '').replace('http:', '') + '" label="' + e.label + '" ' + (i === 0 ? 'default' : '') + '>');
            });
        }
    },
    retryAjax: function() {
        init.retry += 1;
        $.ajax({
            url: "{lbSite}ajax/?action=clear-cache&data={cacheKey}",
            method: "GET",
            dataType: "json",
            cache: false,
            timeout: 30000,
            success: function(res) {
                gtagReport("video_error", "Reload Page", 'smartTV', false);
                loadPlayer();
            },
            error: function(xhr) {
                showMessage('Unable to load source');
                gtagReport("video_error", "Unable to load source", 'smartTV', false);
            }
        });
    },
    onPause: function() {
        if (logStat) clearInterval(logStat);
    },
    onEnded: function() {
        if (logStat) clearInterval(logStat);
        gtagReport("video_complete", "Playback Has Ended", 'smartTV', false);
    },
    mp4: {
        load: function(sources) {
            var $video = document.querySelector('#videoContainer');
            $video.src = sources[sources.length-1].file;
            $video.onerror = function() {
                init.loadAlternative();
            };
            init.loadExternalTracks();
            init.fembedStat();
            showPlayer();
        }
    },
    hls: {
        load: function(source) {
            if (hls) {
                hls.destroy();
                clearInterval(hls.bufferTimer);
                hls = null;
            }
            hls = new Hls({
                "debug": false,
                "enableWorker": true,
                "lowLatencyMode": true,
                "backBufferLength": 90
            });
            hls.attachMedia(document.querySelector('#videoContainer'));
            hls.on(Hls.Events.MEDIA_ATTACHED, function() {
                var hlsTextTracks;
                
                hls.loadSource(source);
                
                hls.on(Hls.Events.MANIFEST_PARSED, function(e, d) {
                    hlsTextTracks = hls.subtitleTracks;
                    if (hlsTextTracks.length === 0) {
                        init.loadExternalTracks();
                    } else {
                        hls.subtitleTrack = 0;
                    }
                    showPlayer();
                });
                
                hls.on(Hls.Events.ERROR, function(e, d) {
                    if (d.fatal) {
                        if ($('#servers li').length > 0) {
                            init.loadAlternative();
                        } else {
                            if ('response' in d && (d.response.code === 0 || d.response.code >= 400)) {
                                showMessage(d.details);
                                gtagReport("video_error", d.details, 'smartTV', false);
                                hls.destroy();
                            } else {
                                switch (d.type) {
                                    case Hls.ErrorTypes.NETWORK_ERROR:
                                        gtagReport("video_error", 'fatal network error encountered, try to recover', 'smartTV', false);
                                        setTimeout(function () {
                                            hls.startLoad();
                                        }, 2000);
                                        break;
            
                                    case Hls.ErrorTypes.MEDIA_ERROR:
                                        gtagReport("video_error", 'fatal media error encountered, try to recover', 'smartTV', false);
                                        setTimeout(function () {
                                            hls.recoverMediaError();
                                        }, 2000);
                                        break;
                                        
                                    default:
                                        showMessage(d.details);
                                        gtagReport("video_error", d.details, 'smartTV', false);
                                        hls.destroy();
                                        break;
                                }
                            }
                        }
                    }
                });
            });
        }
    },
    mpd: {
        errorList: [],
        onLoadError: function(e) {
            if ($('#servers li').length > 0) {
                init.loadAlternative();
            } else {
                var err = init.mpd.errorDetail(e.code);
                $('#message').css('font-size', '16px');
                showMessage(err.name +'! '+ err.description);
                gtagReport("video_error", "Reload Page", 'smartTV', false);
            }
        },
        errorDetail: function(code) {
            if (code && init.mpd.errorList.length) {
                return init.mpd.errorList.find(function(obj) {
                    return obj.code === code;
                });
            }
            return {
                code: undefined,
                name: 'Browser Error',
                description: 'Failed to init decoder'
            };
        },
        load: function(source) {
            if (shakaPlayer) {
                shakaPlayer.detach();
                shakaPlayer.unload();
                shakaPlayer.destroy();
                shakaPlayer = null;
            }
            if (init.mpd.errorList.length === 0) {
                $.ajax({
                    url: '{thisSite}assets/vendor/shaka-player/error-codes.json',
                    dataType: 'json',
                    timeout: 30000,
                    success: function(res) {
                        init.mpd.errorList = res;
                    },
                    error: function(){}
                });
            }
            var shakaConfig = {
                abr: {
                    enabled: true
                },
                manifest: {
                    dash: {
                        autoCorrectDrift: true,
                        ignoreEmptyAdaptationSet: true
                    }
                },
                streaming: {
                    rebufferingGoal: 0.01
                },
            };
            shakaPlayer = new shaka.Player(document.querySelector('#videoContainer'));
            shakaPlayer.configure(shakaConfig);
            shakaPlayer.load(source).then(function(e) {
                init.isLive = shakaPlayer.isLive();
                showPlayer();
            }).catch(init.mpd.onLoadError);
        }
    }
};
window.init = init;

if (hosts.indexOf('fembed') === -1) {
    $(document).ajaxSend(function(res, xhr, opt) {
        if (opt.url.indexOf('{lbSite}ajax/?action=stat') > -1) {
            if (statCounted) {
                xhr.abort();
            } else {
                statCounted = true;
            }
        }
    });
}

$(document).on('contextmenu', function(e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
});

if (sources.length > 0) {
    videoType = sources[0].type;
    initPlayer();
} else if (sources_alt.length > 0) {
    sources = sources_alt;
    sources_alt = [];
    videoType = sources[0].type;
    initPlayer();
} else {
    loadPlayer();
}