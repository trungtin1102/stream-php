<?php
class gdrive
{
    public $name = 'Google Drive';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://docs.google.com/u/4/get_video_info?docid=';
    private $email = '';
    private $cookies = [];
    private $token = [];
    private $ch;
    private $proxy = false;

    function __construct($id = '', $email = '')
    {
        session_write_close();

        if (!empty($id)) {
            $this->id = $id;
            if (!empty($email)) {
                $this->email = $email;
            }

            $scheme = parse_url($this->url, PHP_URL_SCHEME);
            $host = parse_url($this->url, PHP_URL_HOST);
            $port = parse_URL($this->url, PHP_URL_PORT);
            if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
            $ipv4 = gethostbyname($host);
            $resolveHost = implode(':', array($host, $port, $ipv4));

            session_write_close();
            $this->ch = curl_init();
            curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
            curl_setopt($this->ch, CURLOPT_ENCODING, '');
            curl_setopt($this->ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
            curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
            curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
            curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
            curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
            curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
            curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
            curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
            curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        }
    }

    private function getNewSources(array $oldSources = [])
    {
        session_write_close();

        $headers = [];
        if (!empty($this->token['access_token'])) {
            $headers[] = 'authorization: ' . $this->token['token_type'] . ' ' . $this->token['access_token'];
        }

        $mh = curl_multi_init();
        $ch = [];

        foreach ($oldSources as $i => $dt) {
            $headers[] = 'host: ' . parse_url($dt['file'], PHP_URL_HOST);
            $ch[$i] = curl_init($dt['file']);
            curl_setopt($ch[$i], CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch[$i], CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch[$i], CURLOPT_FOLLOWLOCATION, 0);
            curl_setopt($ch[$i], CURLOPT_MAXREDIRS, 5);
            curl_setopt($ch[$i], CURLOPT_ENCODING, '');
            curl_setopt($ch[$i], CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch[$i], CURLOPT_DNS_SERVERS, dns_servers());
            curl_setopt($ch[$i], CURLOPT_NOSIGNAL, true);
            curl_setopt($ch[$i], CURLOPT_DNS_CACHE_TIMEOUT, 300);
            curl_setopt($ch[$i], CURLOPT_TCP_KEEPALIVE, true);
            curl_setopt($ch[$i], CURLOPT_TCP_NODELAY, true);
            curl_setopt($ch[$i], CURLOPT_FORBID_REUSE, true);
            curl_setopt($ch[$i], CURLOPT_FAILONERROR, true);
            curl_setopt($ch[$i], CURLOPT_TIMEOUT, 10);
            curl_setopt($ch[$i], CURLOPT_HEADER, true);
            curl_setopt($ch[$i], CURLOPT_NOBODY, true);
            curl_setopt($ch[$i], CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch[$i], CURLOPT_COOKIE, trim(implode(';', $this->cookies), ';'));
            curl_setopt($ch[$i], CURLOPT_USERAGENT, USER_AGENT);
            curl_multi_add_handle($mh, $ch[$i]);
        }

        $active = null;
        do {
            $mrc = curl_multi_exec($mh, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc == CURLM_OK) {
            if (curl_multi_select($mh) == -1) {
                usleep(10);
            }
            do {
                $mrc = curl_multi_exec($mh, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }

        $result = [];
        foreach ($oldSources as $i => $dt) {
            $effectiveURL = curl_getinfo($ch[$i], CURLINFO_EFFECTIVE_URL);
            $redirectURL = curl_getinfo($ch[$i], CURLINFO_REDIRECT_URL);
            $status = curl_getinfo($ch[$i], CURLINFO_RESPONSE_CODE);
            $type = curl_getinfo($ch[$i], CURLINFO_CONTENT_TYPE);
            $err = curl_error($ch[$i]);
            if ($status >= 200 && $status < 400 && strpos($type, 'json') === FALSE) {
                if (!empty($effectiveURL) && $dt['file'] !== $effectiveURL) {
                    $result[] = [
                        'file' => $effectiveURL,
                        'label' => $dt['label'],
                        'type' => $dt['type']
                    ];
                } elseif (!empty($redirectURL) && $dt['file'] !== $redirectURL) {
                    $result[] = [
                        'file' => $redirectURL,
                        'label' => $dt['label'],
                        'type' => $dt['type']
                    ];
                } else {
                    $result[] = [
                        'file' => $dt['file'],
                        'label' => $dt['label'],
                        'type' => $dt['type']
                    ];
                }
            } else {
                error_log("gdrive getNewSources {$dt['file']} => $type => $status: $err");
            }
            curl_multi_remove_handle($mh, $ch[$i]);
        }
        curl_multi_close($mh);

        if (!empty($result)) {
            $default = array_search('Default', array_column($result, 'label'));
            if (!$default) {
                $first = $result[0];
                array_reverse($result);
                $result[] = [
                    'file' => $first['file'],
                    'type' => $first['type'],
                    'label' => 'Default'
                ];
                array_reverse($result);
            }
            $original = array_search('Original', array_column($result, 'label'));
            if (!$original) {
                $end = end($result);
                $result[] = [
                    'file' => $end['file'],
                    'type' => $end['type'],
                    'label' => 'Original'
                ];
            }
            $result = array_map('unserialize', array_unique(array_map('serialize', $result)));
        }

        return $result;
    }

    private function get_api_sources(string $id = '')
    {
        session_write_close();
        if (!empty($id)) {
            $result = [];
            $downloaded = false;
            $email = !empty($this->email) ? $this->email : '';
            $class = new \GDrive_Auth();
            $token = $class->get_access_token($email);
            $file = $class->get_file_info($id, $email);
            if ($file) {
                if (!empty($file['description'])) {
                    $useDesc = [];
                    $onDesc = ['copy by', 'downloaded', 'uploaded', 'upload', 'download', 'mirror'];
                    foreach ($onDesc as $find) {
                        $useDesc[] = strpos(strtolower($file['description']), $find);
                    }
                    $useDesc = array_unique($useDesc);
                    $useDesc = array_values($useDesc);
                    $ctitle = count($useDesc);
                    if ($ctitle === 1 && $useDesc[0] === false) {
                        $this->title = $file['description'];
                    } else {
                        $this->title = $file['title'];
                    }
                } else {
                    $this->title = $file['title'];
                }
                $now = time();
                $created = strtotime($file['createdDate']);
                $diff = $now - $created;
                if ($diff >= 3600) {
                    $this->image = 'https://drive.google.com/thumbnail?id=' . $file['id'] . '&authuser=0&sz=w9999';
                    $downloaded = $class->download_checker($id);
                } else {
                    if (strpos($file['mimeType'], '/x-msvideo') !== FALSE && $file['id'] !== $this->id) {
                        $id = $this->id;
                    } else {
                        $this->image = 'https://drive.google.com/thumbnail?id=' . $id . '&authuser=0&sz=w9999';
                        if (!empty($token['access_token'])) {
                            $result[] = [
                                'file' => 'https://www.googleapis.com/drive/v3/files/' . $id . '?alt=media&source=downloadUrl',
                                'type' => 'video/mp4',
                                'label' => 'Default'
                            ];
                            $result[] = [
                                'file' => 'https://www.googleapis.com/drive/v3/files/' . $id . '?alt=media&source=downloadUrl',
                                'type' => 'video/mp4',
                                'label' => 'Original'
                            ];
                        } else {
                            $result[] = [
                                'file' => 'https://www.googleapis.com/drive/v3/files/' . $id . '?alt=media&source=downloadUrl&key=AIzaSyAcYqSp6iDJUuQCl8im5zDrfmr04dgu9O8',
                                'type' => 'video/mp4',
                                'label' => 'Default'
                            ];
                            $result[] = [
                                'file' => 'https://www.googleapis.com/drive/v3/files/' . $id . '?alt=media&source=downloadUrl&key=AIzaSyAcYqSp6iDJUuQCl8im5zDrfmr04dgu9O8',
                                'type' => 'video/mp4',
                                'label' => 'Original'
                            ];
                        }
                        $result = $this->getNewSources($result);
                        return $result;
                    }
                }
            } else {
                $this->image = 'https://drive.google.com/thumbnail?id=' . $this->id . '&authuser=0&sz=w9999';
            }

            $url = $this->url . $id . '&drive_originator_app=303';

            curl_setopt($this->ch, CURLOPT_URL, $url);
            if (!empty($token['access_token'])) {
                curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                    'authorization: ' . $token['token_type'] . ' ' . $token['access_token']
                ));
            }
            curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
                if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                    $this->cookies[] = $cookie[1];
                }
                return strlen($headerLine);
            });
            $this->proxy = proxy_rotator();
            if ($this->proxy) {
                curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy['proxy']);
                curl_setopt($this->ch, CURLOPT_PROXYTYPE, $this->proxy['type']);
                curl_setopt($this->ch, CURLOPT_PROXYUSERPWD, $this->proxy['usrpwd']);
            }

            session_write_close();
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                if (strpos($response, 'recaptcha') !== FALSE) {
                    error_log('gdrive get_api_sources ' . $this->id . ' error 1 => g-recapcha detected');
                } else {
                    $result = [];
                    parse_str($response, $json);
                    if (isset($json['status']) && $json['status'] === 'ok') {
                        $excludes = ['5', '17', '36', '132', '133'];
                        $fmt_stream_map = $json['fmt_stream_map'];
                        if (!empty($json['fmt_stream_map'])) {
                            $fmt_stream_map = explode(',', $fmt_stream_map);
                            foreach ($fmt_stream_map as $source) {
                                $src = explode('|', $source);
                                $file = strtr(rawurldecode(end($src)), ['\u003d' => '=', '\u0026' => '&']);
                                $label = $this->label($src[0]);
                                if (!in_array($src[0], $excludes)) {
                                    $result[] = [
                                        'file' => $file,
                                        'type' => 'video/mp4',
                                        'label' => $label,
                                    ];
                                }
                            }

                            $result[] = [
                                'file' => $result[0]['file'],
                                'type' => $result[0]['type'],
                                'label' => 'Default'
                            ];

                            if ($downloaded) {
                                $result[] = [
                                    'file' => 'https://www.googleapis.com/drive/v3/files/' . $id . '?alt=media&source=downloadUrl',
                                    'type' => 'video/mp4',
                                    'label' => 'Original'
                                ];
                            } else {
                                $last = end($result);
                                $result[] = [
                                    'file' => $last['file'],
                                    'type' => $last['type'],
                                    'label' => 'Original'
                                ];
                            }
                            $result = $this->getNewSources($result);
                            return $result;
                        } else {
                            error_log("gdrive get_api_sources $id => DMCA violation");
                        }
                    } elseif ($file) {
                        if ($json['status'] === 'fail' && intval($json['errorcode']) === 150 && intval($json['suberrorcode']) === 3) {
                            $users = $class->get_accounts();
                            if ($users) {
                                foreach ($users as $dt) {
                                    $this->email = $dt['email'];
                                    $sources = $this->get_api_sources($id);
                                    if (!empty($sources)) {
                                        $result = $sources;
                                        break;
                                    }
                                }
                                $result = $this->getNewSources($result);
                                return $result;
                            } else {
                                error_log("gdrive get_api_sources $id => $response");
                            }
                        } else {
                            if (!empty($token['access_token'])) {
                                $result[] = [
                                    'file' => 'https://www.googleapis.com/drive/v3/files/' . $id . '?alt=media&source=downloadUrl',
                                    'type' => 'video/mp4',
                                    'label' => 'Default'
                                ];
                                $result[] = [
                                    'file' => 'https://www.googleapis.com/drive/v3/files/' . $id . '?alt=media&source=downloadUrl',
                                    'type' => 'video/mp4',
                                    'label' => 'Original'
                                ];
                                $result = $this->getNewSources($result);
                                return $result;
                            } else {
                                $result[] = [
                                    'file' => 'https://www.googleapis.com/drive/v3/files/' . $id . '?alt=media&source=downloadUrl&key=AIzaSyAcYqSp6iDJUuQCl8im5zDrfmr04dgu9O8',
                                    'type' => 'video/mp4',
                                    'label' => 'Default'
                                ];
                                $result[] = [
                                    'file' => 'https://www.googleapis.com/drive/v3/files/' . $id . '?alt=media&source=downloadUrl&key=AIzaSyAcYqSp6iDJUuQCl8im5zDrfmr04dgu9O8',
                                    'type' => 'video/mp4',
                                    'label' => 'Original'
                                ];
                                $result = $this->getNewSources($result);
                                return $result;
                            }
                        }
                    }
                }
            } elseif (strpos($err, 'Operation not supported') !== false) {
                if (!empty($token['access_token'])) {
                    $result[] = [
                        'file' => 'https://www.googleapis.com/drive/v3/files/' . $id . '?alt=media&source=downloadUrl',
                        'type' => 'video/mp4',
                        'label' => 'Default'
                    ];
                    $result[] = [
                        'file' => 'https://www.googleapis.com/drive/v3/files/' . $id . '?alt=media&source=downloadUrl',
                        'type' => 'video/mp4',
                        'label' => 'Original'
                    ];
                    $result = $this->getNewSources($result);
                    return $result;
                } else {
                    $result[] = [
                        'file' => 'https://www.googleapis.com/drive/v3/files/' . $id . '?alt=media&source=downloadUrl&key=AIzaSyAcYqSp6iDJUuQCl8im5zDrfmr04dgu9O8',
                        'type' => 'video/mp4',
                        'label' => 'Default'
                    ];
                    $result[] = [
                        'file' => 'https://www.googleapis.com/drive/v3/files/' . $id . '?alt=media&source=downloadUrl&key=AIzaSyAcYqSp6iDJUuQCl8im5zDrfmr04dgu9O8',
                        'type' => 'video/mp4',
                        'label' => 'Original'
                    ];
                    $result = $this->getNewSources($result);
                    return $result;
                }
            } elseif ($this->proxy) {
                error_log("gdrive get_api_sources $id => proxy {$this->proxy['format']} doesn't work => $status: $err");
                $proxyList = proxy_list();
                $key = array_search($this->proxy['format'], $proxyList);
                if ($key) {
                    $brokenProxy = $proxyList[$key];
                    unset($proxyList[$key]);
                    $proxyList = array_values($proxyList);
                    if (count($proxyList) > 0) {
                        if (!filter_var(get_option('delete_unused_proxy'), FILTER_VALIDATE_BOOLEAN)) $proxyList[] = $brokenProxy;
                        $proxyList = trim(implode("\n", $proxyList));
                        set_option('proxy_list', $proxyList);
                        return $this->get_api_sources($id);
                    } else {
                        set_option('proxy_list', []);
                    }
                }
            } else {
                error_log("gdrive get_api_sources $id => $status: $err");
            }
        }
        return [];
    }

    function get_sources()
    {
        session_write_close();

        $result = [];
        $alwaysCopy = filter_var(get_option('gdrive_copy'), FILTER_VALIDATE_BOOLEAN);
        if ($alwaysCopy) {
            $class = new \GDriveQueue();
            $class->insert(array(
                'gdrive_id' => $this->id
            ));
        }
        $class = new \GDrive_Auth();
        $file = $class->get_mirrors(true, $this->id);
        if (!empty($file['id'])) {
            $this->email = !empty($file['owners'][0]['emailAddress']) ? $file['owners'][0]['emailAddress'] : '';
            $result = $this->get_api_sources($file['id']);
        } else {
            $result = $this->get_api_sources($this->id);
        }
        if (!empty($result)) {
            $this->status = 'ok';
            if (strpos($result[0]['file'], 'googleapis.com') !== FALSE) {
                $this->referer = 'https://drive.google.com/file/d/' . getDriveId($result[0]['file']) . '/view';
            } else {
                $this->referer = 'https://youtube.googleapis.com/';
            }
        } else {
            $class->set_id($this->id);
            $file = $class->copy_file_now();
            if (!empty($file['id'])) {
                $this->status = 'ok';
                $this->image = 'https://drive.google.com/thumbnail?id=' . $file['id'] . '&authuser=0&sz=w9999';
                $this->referer = 'https://drive.google.com/file/d/' . $file['id'] . '/view';
                $this->email = $file['owners'][0]['emailAddress'];
                $this->token = $class->get_access_token($this->email);
                $result[] = [
                    'file' => 'https://www.googleapis.com/drive/v3/files/' . $file['id'] . '?alt=media&source=downloadUrl',
                    'type' => 'video/mp4',
                    'label' => 'Default'
                ];
                $result[] = [
                    'file' => 'https://www.googleapis.com/drive/v3/files/' . $file['id'] . '?alt=media&source=downloadUrl',
                    'type' => 'video/mp4',
                    'label' => 'Original'
                ];
                $result = $this->getNewSources($result);
            } else {
                $file = $class->copy_file_3rd_party();
                if (!empty($file['id'])) {
                    $this->status = 'ok';
                    $this->image = 'https://drive.google.com/thumbnail?id=' . $file['id'] . '&authuser=0&sz=w9999';
                    $this->referer = 'https://drive.google.com/file/d/' . $file['id'] . '/view';
                    $users = $class->get_accounts();
                    $key = array_rand($users);
                    $this->email = $users[$key]['email'];
                    $this->token = $class->get_access_token($users[$key]['email']);
                    $result[] = [
                        'file' => 'https://www.googleapis.com/drive/v3/files/' . $file['id'] . '?alt=media&source=downloadUrl',
                        'type' => 'video/mp4',
                        'label' => 'Default'
                    ];
                    $result[] = [
                        'file' => 'https://www.googleapis.com/drive/v3/files/' . $file['id'] . '?alt=media&source=downloadUrl',
                        'type' => 'video/mp4',
                        'label' => 'Original'
                    ];
                    $result = $this->getNewSources($result);
                }
            }
        }
        return $result;
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_email()
    {
        session_write_close();
        return $this->email;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    private function label(string $itag = '')
    {
        session_write_close();
        switch ($itag) {
            case '18':
                $label = "360p";
                break;
            case '59':
                $label = "480p";
                break;
            case '22':
                $label = "720p";
                break;
            case '37':
                $label = "1080p";
                break;
            case '34':
                $label = "360p";
                break;
            case '35':
                $label = "480p";
                break;
            case '38':
                $label = "Original";
                break;
            case '43':
                $label = "360p";
                break;
            case '44':
                $label = "480p";
                break;
            case '45':
                $label = "720p";
                break;
            case '46':
                $label = "1080p";
                break;
            case '82':
                $label = "360p";
                break;
            case '84':
                $label = "720p";
                break;
            case '102':
                $label = "360p";
                break;
            case '104':
                $label = "720p";
                break;
            default:
                $label = "Unknown";
                break;
        }
        return $label;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
