<?php
class amazon
{
    public $name = 'Amazon Drive';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://www.amazon.com/drive/v1/shares/';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = 'https://www.amazon.com/';
        
        $this->id = $id;
        $this->url .= $this->id . '?shareId=' . $this->id . '&resourceVersion=V2&ContentType=JSON&_=' . time();

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'host: www.amazon.com',
            'origin: https://www.amazon.com'
        ));
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    function get_sources()
    {
        session_write_close();

        if (!empty($this->id)) {
            session_write_close();
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $data = json_decode($response, TRUE);
                if ($data['statusCode'] === 200) {
                    curl_setopt($this->ch, CURLOPT_URL, 'https://www.amazon.com/drive/v1/nodes/' . $data['nodeInfo']['id'] . '/children?asset=ALL&limit=1&searchOnFamily=false&tempLink=true&shareId=' . $this->id . '&offset=0&resourceVersion=V2&ContentType=JSON&_=' . time());
                    session_write_close();
                    $response = curl_exec($this->ch);
                    $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);

                    if ($status >= 200 && $status < 400) {
                        $data = json_decode($response, TRUE);
                        if ($data['count'] > 0) {
                            $this->status = 'ok';
                            $this->title = $data['data'][0]['name'];

                            $result = [];
                            if (!empty($data['data'][0]['assets'])) {
                                foreach ($data['data'][0]['assets'] as $vid) {
                                    if ($vid['status'] === 'AVAILABLE' && strpos($vid['contentProperties']['contentType'], 'video/') !== FALSE) {
                                        $result[] = [
                                            'file' => $vid['tempLink'] . '?ownerId=' . $vid['ownerId'],
                                            'type' => $vid['contentProperties']['contentType'],
                                            'label' => $vid['contentProperties']['video']['height'] . 'p'
                                        ];
                                    } elseif (strpos($vid['contentProperties']['contentType'], 'image/') !== FALSE) {
                                        $this->image = $vid['tempLink'] . '?ownerId=' . $vid['ownerId'];
                                    }
                                }
                            } else {
                                $result[] = [
                                    'file' => $data['data'][0]['tempLink'] . '?ownerId=' . $data['data'][0]['ownerId'],
                                    'type' => $data['data'][0]['contentProperties']['contentType'],
                                    'label' => $data['data'][0]['contentProperties']['video']['height'] . 'p'
                                ];
                            }
                            return $result;
                        } else {
                            error_log("amazon get_sources nodes {$this->id} count is zero");
                        }
                    } else {
                        error_log("amazon get_sources nodes {$this->id} => $status: $err");
                    }
                } else {
                    error_log("amazon get_sources shares {$this->id} => {$data['statusCode']}");
                }
            } else {
                error_log("amazon get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();

        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();

        return $this->status;
    }

    function get_title()
    {
        session_write_close();

        return $this->title;
    }

    function get_image()
    {
        session_write_close();

        return $this->image;
    }

    function get_referer()
    {
        session_write_close();

        return $this->referer;
    }

    function get_id()
    {
        session_write_close();

        return $this->id;
    }

    function __destruct()
    {
        session_write_close();

        curl_close($this->ch);
    }
}
