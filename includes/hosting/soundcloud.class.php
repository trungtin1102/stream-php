<?php
class soundcloud
{
    public $name = 'Soundcloud';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = '';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = 'https://w.soundcloud.com/';
        $this->id = $id;
        $this->url = $id;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            curl_setopt($this->ch, CURLOPT_URL, $this->url);
            curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
            session_write_close();
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);

            if ($status >= 200 && $status < 400) {
                $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                $twitter = $dom->find('meta[property="twitter:player"]', 0);
                if (!empty($twitter)) {
                    $twitter = $twitter->content;
                    $qry = parse_url($twitter, PHP_URL_QUERY);
                    parse_str($qry, $qry);

                    curl_setopt($this->ch, CURLOPT_URL, $twitter);
                    curl_setopt($this->ch, CURLOPT_REFERER, 'https://twitter.com/');
                    session_write_close();
                    $response = curl_exec($this->ch);
                    $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                    $err = curl_error($this->ch);

                    if ($status >= 200 && $status < 400) {
                        if (preg_match_all('/src="([^"]+)"/', $response, $urls) && !empty($urls[1])) {
                            $url = end($urls[1]);
                            curl_setopt($this->ch, CURLOPT_URL, $url);
                            curl_setopt($this->ch, CURLOPT_REFERER, 'https://w.soundcloud.com/');
                            session_write_close();
                            $response = curl_exec($this->ch);
                            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                            $err = curl_error($this->ch);

                            if ($status >= 200 && $status < 400) {
                                if (preg_match('/client_id\:u\?"([^"]+)"/', $response, $client_id) && !empty($client_id[1])) {
                                    $url = 'https://api-widget.soundcloud.com/resolve?url=' . $qry['url'] . '&format=json&client_id=' . $client_id[1] . '&app_version=1634634459';

                                    curl_setopt($this->ch, CURLOPT_URL, $url);
                                    curl_setopt($this->ch, CURLOPT_REFERER, 'https://w.soundcloud.com/');
                                    session_write_close();
                                    $response = curl_exec($this->ch);
                                    $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                                    $err = curl_error($this->ch);

                                    if ($status >= 200 && $status < 400) {
                                        $data = json_decode($response, true);
                                        if (!empty($data['media'])) {
                                            $this->title = $data['title'];
                                            $this->image = $data['artwork_url'];
                                            $url = $data['media']['transcodings'][0]['url'] . '?client_id=' . $client_id[1];

                                            curl_setopt($this->ch, CURLOPT_URL, $url);
                                            session_write_close();
                                            $response = curl_exec($this->ch);
                                            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);

                                            if ($status >= 200 && $status < 400) {
                                                $data = json_decode($response, true);
                                                if (!empty($data['url'])) {
                                                    $this->status = 'ok';
                                                    $result = [];
                                                    $result[] = [
                                                        'file' => $data['url'],
                                                        'type' => 'hls',
                                                        'label' => 'Original'
                                                    ];
                                                    return $result;
                                                } else {
                                                    error_log("soundcloud get_sources {$this->id} => $url => url not found");
                                                }
                                            } else {
                                                error_log("soundcloud get_sources {$this->id} => $url => $status: $err");
                                            }
                                        } else {
                                            error_log("soundcloud get_sources {$this->id} => $url => media not found");
                                        }
                                    } else {
                                        error_log("soundcloud get_sources {$this->id} => $url => $status: $err");
                                    }
                                } else {
                                    error_log("soundcloud get_sources {$this->id} => $url => client_id not found");
                                }
                            } else {
                                error_log("soundcloud get_sources {$this->id} => $url => $status: $err");
                            }
                        } else {
                            error_log("soundcloud get_sources {$this->id} => widget-*.js not found");
                        }
                    } else {
                        error_log("soundcloud get_sources {$this->id} => $twitter => $status: $err");
                    }
                } else {
                    error_log("soundcloud get_sources {$this->id} => meta twitter player not found");
                }
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
