<?php
class streamlare
{
    public $name = 'Streamlare';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://slwatch.co/';
    private $tracks = [];

    function __construct($id = '')
    {
        session_write_close();

        $this->referer = $this->url;
        $this->id = strtr($id, ['/e/' => '', '/v/' => '']);
        $this->url .= 'v/' . $this->id;
    }

    private function getNewSources(array $oldSources = [])
    {
        session_write_close();
        $mh = curl_multi_init();
        $ch = [];

        foreach ($oldSources as $i => $dt) {
            $ch[$i] = curl_init($dt['file']);
            curl_setopt($ch[$i], CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch[$i], CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch[$i], CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch[$i], CURLOPT_MAXREDIRS, 5);
            curl_setopt($ch[$i], CURLOPT_ENCODING, '');
            curl_setopt($ch[$i], CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch[$i], CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch[$i], CURLOPT_TCP_NODELAY, 1);
            curl_setopt($ch[$i], CURLOPT_FORBID_REUSE, 1);
            curl_setopt($ch[$i], CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch[$i], CURLOPT_TIMEOUT, 30);
            curl_setopt($ch[$i], CURLOPT_HEADER, 1);
            curl_setopt($ch[$i], CURLOPT_NOBODY, 1);
            curl_setopt($ch[$i], CURLOPT_HTTPHEADER, array(
                'accept: */*',
                "cache-control: no-cache",
                'pragma: no-cache',
                'Connection: keep-alive',
                'range: bytes=0-'
            ));
            curl_setopt($ch[$i], CURLOPT_REFERER, $this->referer);
            curl_setopt($ch[$i], CURLOPT_USERAGENT, USER_AGENT);
            curl_multi_add_handle($mh, $ch[$i]);
        }

        $active = null;
        do {
            $mrc = curl_multi_exec($mh, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc == CURLM_OK) {
            if (curl_multi_select($mh) == -1) {
                usleep(10);
            }
            do {
                $mrc = curl_multi_exec($mh, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }

        $result = [];
        foreach ($oldSources as $i => $dt) {
            $effectiveURL = curl_getinfo($ch[$i], CURLINFO_EFFECTIVE_URL);
            $redirectURL = curl_getinfo($ch[$i], CURLINFO_REDIRECT_URL);
            $status = curl_getinfo($ch[$i], CURLINFO_RESPONSE_CODE);
            $err = curl_error($ch[$i]);
            if ($status >= 200 && $status < 400) {
                if (!empty($effectiveURL) && $dt['file'] !== $effectiveURL) {
                    $result[] = [
                        'file' => strtr($effectiveURL, ['?dl' => '?stream']),
                        'label' => $dt['label'],
                        'type' => $dt['type']
                    ];
                } elseif (!empty($redirectURL) && $dt['file'] !== $redirectURL) {
                    $result[] = [
                        'file' => strtr($redirectURL, ['?dl' => '?stream']),
                        'label' => $dt['label'],
                        'type' => $dt['type']
                    ];
                } else {
                    $result[] = [
                        'file' => strtr($dt['file'], ['?dl' => '?stream']),
                        'label' => $dt['label'],
                        'type' => $dt['type']
                    ];
                }
            } else {
                error_log("streamlare getNewSources $dt => $status: $err");
            }
            curl_multi_remove_handle($mh, $ch[$i]);
        }
        curl_multi_close($mh);
        return $result;
    }

    private function getInfo()
    {
        session_write_close();
        $dom = \KubAT\PhpSimple\HtmlDomParser::file_get_html(strtr($this->url, ['/v/' => '/e/']));
        if ($dom) {
            $file = get_string_between($dom->innertext, ':file="', '"');
            $file = strtr($file, ['&quot;' => '"']);
            $file = @json_decode($file, true);
            if (JSON_ERROR_NONE === json_last_error()) {
                $this->title = $file['name'];
                $this->image = 'https://cdn.streamlare.com/' . $file['poster'];
            }
            $subtitles = get_string_between($dom->innertext, ':subtitles="', '"');
            $subtitles = strtr($subtitles, ['&quot;' => '"']);
            $subtitles = @json_decode($subtitles, true);
            if (JSON_ERROR_NONE === json_last_error()) {
                foreach ($subtitles as $dt) {
                    $this->tracks[] = [
                        'file' => $dt['file'],
                        'label' => $dt['label']
                    ];
                }
            }
        }
    }

    function get_sources()
    {
        session_write_close();
        $logFile = execHeadlessChromium($this->url, 'streamlare~' . keyFilter($this->id));
        if ($logFile) {
            open_resources_handler();
            $fp = @fopen($logFile, 'r');
            if ($fp) {
                stream_set_blocking($fp, false);
                $content = stream_get_contents($fp);
                fclose($fp);

                if ($content) {
                    $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($content);
                    $video = $dom->find('a.btn-link.btn-primary');
                    if (!empty($video)) {
                        $this->status = 'ok';

                        $this->getInfo();
                        if (empty($this->title)) {
                            $title = $dom->find('h2.file-title', 0);
                            $this->title = !empty($title) ? $title->plaintext : '';
                        }

                        $result = [];
                        foreach ($video as $dt) {
                            list($label, $size) = array_pad(explode(' ', trim($dt->plaintext)), 2, '');
                            $result[] = [
                                'file' => $dt->href,
                                'type' => 'video/mp4',
                                'label' => $label
                            ];
                        }
                        $result = $this->getNewSources($result);

                        usort($result, function ($a, $b) {
                            return strnatcasecmp($a['label'], $b['label']);
                        });

                        @unlink($logFile);

                        return $result;
                    } else {
                        error_log("streamlare get_sources {$this->id} => sources not found");
                    }
                } else {
                    error_log("streamlare get_sources {$this->id} => cannot read log file");
                }
            } else {
                error_log("streamlare get_sources {$this->id} => cannot open log file");
            }
        } else {
            error_log("streamlare get_sources {$this->id} => cannot create log file");
        }
        return [];
    }

    function get_tracks()
    {
        session_write_close();
        return $this->tracks;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
    }
}
