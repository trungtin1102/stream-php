<?php
class dropbox
{
    public $name = 'Dropbox';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://www.dropbox.com/';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();

        $this->referer = $this->url;

        $this->id = $id;
        $this->url .= 's/' . $this->id;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            "host: www.dropbox.com",
            "origin: https://www.dropbox.com",
        ));
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    private function direct_link($url = '')
    {
        session_write_close();
        if (!empty($url)) {
            curl_setopt($this->ch, CURLOPT_URL, $url);
            curl_setopt($this->ch, CURLOPT_HEADER, 1);
            curl_setopt($this->ch, CURLOPT_NOBODY, 1);

            session_write_close();
            curl_exec($this->ch);
            $effectiveURL = curl_getinfo($this->ch, CURLINFO_EFFECTIVE_URL);
            $redirectURL = curl_getinfo($this->ch, CURLINFO_REDIRECT_URL);

            if (!empty($effectiveURL) && $effectiveURL !== $url) {
                return $effectiveURL;
            } elseif (!empty($redirectURL) && $redirectURL !== $url) {
                return $redirectURL;
            } else {
                return $url;
            }
        }
        return FALSE;
    }

    function get_sources($mp4 = false)
    {
        session_write_close();
        if (!empty($this->id)) {
            session_write_close();
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $this->image = get_string_between($response, 'poster_url_tmpl": "', '"');
                $this->title = get_string_between($response, 'filename": "', '"');
                if ($mp4) {
                    $video = strtr(get_string_between($response, 'href": "', '"'), ['dl=0' => 'dl=1']);
                    if (!empty($video)) {
                        $this->status = 'ok';
                        $result[] = [
                            'file' => $video,
                            'type' => 'video/mp4',
                            'label' => 'Original'
                        ];
                        return $result;
                    }
                } else {
                    $video = get_string_between($response, 'transcode_url": "', '"');
                    if (!empty($video)) {
                        $this->status = 'ok';
                        $result[] = [
                            'file' => $video,
                            'type' => 'hls',
                            'label' => 'Original'
                        ];

                        return $result;
                    }
                }
            } else {
                error_log("dropbox get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
