<?php
class streamable
{
    public $name = 'Streamable';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://api.streamable.com/videos/';
    private $cookies = [];
    private $ch;
    private $proxy = false;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->id = $id;
        $this->url .= $this->id;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    private function thirdParty()
    {
        session_write_close();
        curl_setopt($this->ch, CURLOPT_URL, 'https://api.streamabledl.com/streamable/' . $this->id);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'Host: api.streamabledl.com',
            'Origin: https://streamabledl.com'
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, 'https://streamabledl.com/');
        $response = curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);

        if ($status >= 200 && $status < 400) {
            $json = json_decode($response, true);
            if ($json['success']) {
                $this->status = 'ok';
                $this->referer = 'https://streamable.com/';
                $this->title = $json['result']['title'];
                $this->image = $json['result']['thumbnail'];

                $result = [];
                foreach ($json['result']['files'] as $value) {
                    if (!empty($value['url'])) {
                        $result[] = [
                            'file' => $value['url'],
                            'type' => 'video/mp4',
                            'label' => $value['height'] . 'p'
                        ];
                    }
                }
                return $result;
            } else {
                error_log("streamable thirdParty {$this->id} => $response");
            }
        } else {
            error_log("streamable thirdParty {$this->id} => $status: $err");
        }
        return [];
    }

    function get_sources(bool $skipParty = false)
    {
        session_write_close();
        if (!empty($this->id)) {
            $data = $skipParty ? [] : $this->thirdParty();
            if (!empty($data)) {
                return $data;
            } else {
                curl_setopt($this->ch, CURLOPT_URL, $this->url);
                $this->proxy = proxy_rotator();
                if ($this->proxy) {
                    curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy['proxy']);
                    curl_setopt($this->ch, CURLOPT_PROXYTYPE, $this->proxy['type']);
                    curl_setopt($this->ch, CURLOPT_PROXYUSERPWD, $this->proxy['usrpwd']);
                }
                
                $response = curl_exec($this->ch);
                $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                $err = curl_error($this->ch);

                if ($status >= 200 && $status < 400) {
                    $data = json_decode($response, true);
                    if (!empty($data['files'])) {
                        if ($data['status'] === 2 && $data['percent'] === 100) {
                            $this->status = 'ok';
                            $this->title = $data['title'];
                            $this->image = $data['thumbnail_url'];
                            $this->referer = 'https://streamable.com/';
                            $result = [];
                            foreach ($data['files'] as $dt) {
                                if (!empty($dt['url']) && validate_url($dt['url'])) {
                                    $result[] = array(
                                        'file' => $dt['url'],
                                        'type' => 'video/mp4',
                                        'label' => $dt['height'] . 'p'
                                    );
                                }
                            }
                            return $result;
                        } else {
                            error_log("streamable get_sources {$this->id} => processing");
                        }
                    } else {
                        error_log("streamable get_sources {$this->id} => $response");
                    }
                } elseif ($this->proxy) {
                    error_log("streamable get_sources {$this->id} => proxy {$this->proxy['format']} doesn't work => $status: $err");
                    $proxyList = proxy_list();
                    $key = array_search($this->proxy['format'], $proxyList);
                    if ($key) {
                        $brokenProxy = $proxyList[$key];
                        unset($proxyList[$key]);
                        $proxyList = array_values($proxyList);
                        if (count($proxyList) > 0) {
                            if (!filter_var(get_option('delete_unused_proxy'), FILTER_VALIDATE_BOOLEAN)) $proxyList[] = $brokenProxy;
                            $proxyList = implode("\n", $proxyList);
                            set_option('proxy_list', $proxyList);
                            return $this->get_sources(true);
                        } else {
                            set_option('proxy_list', []);
                        }
                    }
                } else {
                    error_log("streamable get_sources {$this->id} => $status: $err");
                }
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
