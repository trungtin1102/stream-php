<?php
class facebook
{
    public $name = 'Facebook';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://www.facebook.com/';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();

        $this->referer = $this->url;
        $this->id = rawurlencode($id);
        $this->url .= trim($this->id, '/') . '/';

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'host: ' . $host,
            'origin: ' . $scheme . '://' . $host,
        ));
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
    }

    private function mpdParser(string $response = '', string $logFile = '', string $instanceKey = '')
    {
        session_write_close();
        $response = get_string_between($response, '?xml', 'MPD>');
        if (!empty($response)) {
            $response = '<?xml' . strtr($response, ["\\r\\n" => '', "\\n" => '', '\u003C' => '<', '\\' => '', '<MPD' => "\n<MPD"]) . 'MPD>';
            $dom = new \DOMDocument('1.0');
            $dom->preserveWhiteSpace = true;
            $dom->formatOutput = true;
            $dom->loadXML($response);
            $response = $dom->saveXML();
            if (!empty($instanceKey)) {
                $response = strtr($response, ['&amp;oe=' => '&amp;_nc_psid=' . $instanceKey . '&amp;oe=']);
            }

            $fileName = BASE_DIR . 'tmp/hosts/facebook/' . keyFilter($this->id) . '.mpd';
            @unlink($fileName);

            create_dir('tmp');
            create_dir('tmp/hosts');
            create_dir('tmp/hosts/facebook');

            open_resources_handler();
            $fp = @fopen($fileName, 'w+');
            if ($fp) {
                stream_set_blocking($fp, false);
                if (flock($fp, LOCK_EX)) {
                    fwrite($fp, $response, strlen($response));
                    fflush($fp);
                    flock($fp, LOCK_UN);
                }
                fclose($fp);

                $this->status = 'ok';

                @unlink($logFile);
                $result[] = [
                    'file' => BASE_URL . 'tmp/hosts/facebook/' . keyFilter($this->id) . '.mpd',
                    'type' => 'mpd',
                    'label' => 'Original'
                ];
                return $result;
            } else {
                error_log("facebook mpdParser {$this->id} => cannot read $fileName");
            }
        }
    }

    private function fuckTimeout(int $retry = 0)
    {
        session_write_close();
        $logFile = BASE_DIR . 'cache/logs/facebook~' . keyFilter($this->id) . '.log';
        if (file_exists($logFile)) {
            open_resources_handler();
            $fp = @fopen($logFile, 'r');
            if ($fp) {
                stream_set_blocking($fp, false);
                $response = stream_get_contents($fp);
                fclose($fp);

                $this->image = trim(strtr(get_string_between($response, 'twitter:image" content="', '"'), ['amp;' => '']));
                $this->title = trim(strtr(get_string_between($response, '<title>', '</title>'), ['| Facebook' => '']));
                $instanceKey = trim(get_string_between($response, 'instancekey="', '"'));

                return $this->mpdParser($response, $logFile, $instanceKey);
            } else {
                error_log("facebook fuckTimeout {$this->id} => cannot read $logFile");
            }
        } else {
            error_log("facebook fuckTimeout {$this->id} => file not found");
        }
        if ($retry <= 3) return $this->fuckTimeout($retry + 1);
    }

    function get_sources()
    {
        session_write_close();

        if (!empty($this->id)) {
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $this->title = trim(strtr(get_string_between($response, '<title>', '</title>'), ['Facebook' => '', '|' => '']));
                $this->image = trim(strtr(get_string_between($response, 'og:image" content="', '"'), ['amp;' => '']));
                $video = trim(strtr(get_string_between($response, 'og:video" content="', '"'), ['amp;' => '']));
                $instanceKey = trim(get_string_between($response, 'instancekey="', '"'));
                if (!empty($instanceKey) && !empty($video) && validate_url($video) && strpos($video, '.mpd') !== false) {
                    $this->status = 'ok';
                    $result[] = [
                        'file' => $video . '&_nc_psid=' . $instanceKey,
                        'type' => 'mpd',
                        'label' => 'Original'
                    ];
                    return $result;
                } elseif (strpos($response, 'MPD>') !== false) {
                    return $this->mpdParser($response, '', $instanceKey);
                } else {
                    $response = execHeadlessChromium($this->url, 'facebook~' . keyFilter($this->id));
                    if ($response) {
                        return $this->fuckTimeout(0);
                    } else {
                        error_log('facebook get_sources => headless chromium error timeout');
                        return $this->fuckTimeout(0);
                    }
                }
            } else {
                error_log('facebook get_sources => ' . $status . ': ' . $err);
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
