<?php
class hxfile
{
    public $name = 'HxFile';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://hxfile.co/';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = $this->url;
        $this->id = strtr($id, ['embed-' => '', '.html' => '']);

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'origin: https://' . $host,
            'host: ' . $host
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
    }

    private function embed_sources()
    {
        session_write_close();
        curl_setopt($this->ch, CURLOPT_URL, $this->url . 'embed-' . $this->id . '.html');
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });

        session_write_close();
        $response = curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);

        if ($status >= 200 && $status < 400) {
            $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
            $scripts = $dom->find('script');
            if (!empty($scripts)) {
                $eval = '';
                foreach ($scripts as $sc) {
                    if (strpos($sc->innertext, 'eval(function(p,a,c,k,e,d)') !== FALSE) {
                        $eval = $sc->innertext;
                        break;
                    }
                }
                if (!empty($eval)) {
                    $unpacker = new \JavascriptUnpacker\JavascriptUnpacker();
                    $data = $unpacker->unpack($eval);
                    $start = explode('player.setup(', $data);
                    $end = explode(');', end($start));
                    $jsObject = strtr($end[0], ['],}' => ']}']);
                    $json = \OviDigital\JsObjectToJson\JsConverter::convertToJson($jsObject);
                    $arr = json_decode($json, true);
                    if (!empty($arr['sources'])) {
                        $this->status = 'ok';
                        $this->title = $dom->find('title', 0)->plaintext;
                        $this->image = $arr['image'];
                        $result[] = [
                            'file' => $arr['sources'][0]['file'],
                            'type' => $arr['sources'][0]['type'],
                            'label' => 'Original'
                        ];
                        return $result;
                    }
                } else {
                    error_log("hxfile embed_sources {$this->id} => eval is empty");
                }
            } else {
                error_log("hxfile embed_sources {$this->id} => script not found");
            }
        } else {
            error_log("hxfile embed_sources {$this->id} => $status: $err");
        }
        return [];
    }

    private function download_sources()
    {
        session_write_close();
        curl_setopt($this->ch, CURLOPT_URL, $this->url . $this->id);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, 'op=download2&id=' . $this->id . '&rand=&referer=&method_free=&method_premium=&adblock_detected=0');
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });

        session_write_close();
        $response = curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);
        if ($status >= 200 && $status < 400) {
            $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
            $dl = $dom->find('.download-button', 0);
            if ($dl) {
                $this->status = 'ok';
                $this->title = trim($dom->find('.dfilename', 0)->plaintext);
                $result[] = [
                    'file' => trim($dl->find('a.btn-dow', 0)->href),
                    'type' => 'video/mp4',
                    'label' => 'Original'
                ];
                return $result;
            } else {
                error_log("hxfile download_sources {$this->id} => download button not found");
            }
        } else {
            error_log("hxfile get_sources {$this->id} => $status: $err");
        }
        return [];
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            $embed = $this->embed_sources();
            if (!empty($embed)) return $embed;
            else return $this->download_sources();
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
