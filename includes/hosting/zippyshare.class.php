<?php
class zippyshare
{
    public $name = 'Zippyshare';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = '';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->id = $id;
        $this->url = $this->id;
        $this->referer = $this->url;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $head) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $head, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($head);
        });
    }

    private function security_1(string $data = '')
    {
        session_write_close();
        try {
            $ex = explode("\n", strtr(trim($data), ["\r\n" => "\n"]));
            list($var, $val) = explode('=', rtrim($ex[0], ';'), 2);
            $a = (int) $val;
            if (strpos($ex[1], '=')) {
                list($var, $val) = explode('=', rtrim($ex[1], ';'), 2);
                $b = (int) $val;
            } else {
                $b = '';
            }
            if (strpos($data, '.omg = "f"') !== FALSE) {
                $a = floor($a / 3);
            } else {
                $a = ceil($a / 3);
            }
            list($var, $val) = explode('.href = "', $data, 2);
            list($link, $script) = explode(';', $val, 2);
            $link = strtr($link, ['"' => '']);
            $linkArr = array_values(array_filter(explode('/', trim($link))));
            $math = strtr(trim($linkArr[2], '+'), ['a' => $a, 'b' => $b, '(' => '', ')' => '']);
            if (!empty($math)) {
                $run = 'return ' . trim(trim($math), '+') . ';';
                $code = eval($run);

                $host = parse_url($this->url, PHP_URL_HOST);
                $scheme = parse_url($this->url, PHP_URL_SCHEME);
                $video = $scheme . '://' . $host . '/' . implode('/', [$linkArr[0], $linkArr[1], $code, $linkArr[3]]);
                if (validate_url($video)) {
                    $this->status = 'ok';
                    $this->image = strtr($video, ['/d/' => '/i/']);

                    $result[] = [
                        'file' => $video,
                        'type' => 'video/mp4',
                        'label' => 'Original'
                    ];
                    return $result;
                }
            }
        } catch (\Exception $e) {
            error_log('zippyshare security_1 ' . $this->id . ' => ' . $e->getMessage());
        }
        return FALSE;
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);
            if ($status >= 200 && $status < 400) {
                $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                $video = $dom->find('#plyr', 0);
                if (!empty($video)) {
                    $data = 'https:' . $video->find('source', 0)->src;
                } else {
                    $data = '';
                    $scripts = $dom->find('script');
                    foreach ($scripts as $src) {
                        if (strpos($src->innertext, "document.getElementById('dlbutton').href") !== FALSE) {
                            $data = $src->innertext;
                            break;
                        }
                    }
                }
                if (!empty($data)) {
                    if (preg_match('/html\]([^"]+)\[\/url]/', $response, $title)) {
                        $this->title = $title[1];
                    }
                    if (validate_url($data)) {
                        $this->status = 'ok';
                        $poster = 'data-poster';
                        $this->image = 'https:' . htmlspecialchars_decode($video->$poster);

                        $result[] = [
                            'file' => htmlspecialchars_decode($data),
                            'type' => 'video/mp4',
                            'label' => 'Original'
                        ];
                        return $result;
                    } else {
                        $r1 = $this->security_1($data);
                        if ($r1) return $r1;
                    }
                } else {
                    error_log("zippyshare get_sources {$this->id} => video not found");
                }
            } else {
                error_log("zippyshare get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
