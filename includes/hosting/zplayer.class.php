<?php
class zplayer
{
    public $name = 'zPlayer.live';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://v2.zplayer.live/';
    private $tracks = [];
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = $this->url;
        $this->id = trim(strtr($id, ['/embed/' => '', '/video/' => '', '/download/' => '']));
        $this->url .= 'embed/' . $this->id;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'accept: */*',
            'cache-control: no-cache',
            'pragma: no-cache',
            'host: v2.zplayer.live',
            'origin: https://v2.zplayer.live'
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
    }

    private function title()
    {
        session_write_close();
        curl_setopt($this->ch, CURLOPT_URL, strtr($this->url, ['/embed/' => '/video/']));
        $response = curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);
        if ($status >= 200 && $status < 400) {
            $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
            $title = $dom->find('title', 0);
            if (!empty($title)) return trim(strtr($title->plaintext, ['Watching' => '', 'Watch' => '', 'Download' => '']), " -");
            else error_log("zplayer title {$this->id} => title not found");
        } else {
            error_log("zplayer title {$this->id} => $status: $err");
        }
        return '';
    }

    private function get_download_links(array $links = [])
    {
        session_write_close();
        $replace = ['l' => 'Low Quality', 'n' => 'Normal Quality', 'h' => 'High Quality', 'o' => 'Original Quality'];
        $dlLinks = [];
        foreach ($links as $link) {
            $qry = parse_url($link, PHP_URL_QUERY);
            parse_str($qry, $qry);
            $dlLinks[] = [
                'link' => $link,
                'mode' => trim(strtr($qry['mode'], $replace)),
            ];
        }
        if (!empty($dlLinks)) {
            session_write_close();
            $mh = curl_multi_init();
            $ch = [];
            foreach ($dlLinks as $i => $data) {
                $ch[$i] = curl_init($data['link']);
                curl_setopt($ch[$i], CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch[$i], CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch[$i], CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch[$i], CURLOPT_ENCODING, '');
                curl_setopt($ch[$i], CURLOPT_TIMEOUT, 30);
                curl_setopt($ch[$i], CURLOPT_TCP_FASTOPEN, 1);
                curl_setopt($ch[$i], CURLOPT_TCP_NODELAY, 1);
                curl_setopt($ch[$i], CURLOPT_FORBID_REUSE, 1);
                curl_setopt($ch[$i], CURLOPT_REFERER, $this->url);
                curl_setopt($ch[$i], CURLOPT_USERAGENT, USER_AGENT);
                curl_setopt($ch[$i], CURLOPT_HTTPHEADER, array(
                    "host: v2.zplayer.live",
                    "origin: https://v2.zplayer.live"
                ));
                curl_multi_add_handle($mh, $ch[$i]);
            }

            $active = null;
            do {
                $mrc = curl_multi_exec($mh, $active);
            } while (
                $mrc == CURLM_CALL_MULTI_PERFORM
            );

            while ($active && $mrc == CURLM_OK) {
                if (curl_multi_select($mh) == -1) {
                    usleep(100);
                }
                do {
                    $mrc = curl_multi_exec($mh, $active);
                } while ($mrc == CURLM_CALL_MULTI_PERFORM);
            }

            $result = [];
            $newDLinks = [];
            foreach ($dlLinks as $i => $data) {
                $response = curl_multi_getcontent($ch[$i]);
                $status = curl_getinfo($ch[$i], CURLINFO_HTTP_CODE);
                $err = curl_error($ch[$i]);
                if ($status >= 200 && $status < 400) {
                    $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                    if (!empty($dom->find('.err', 0))) {
                        $frm = $dom->find('form[name="F1"]', 0);
                        if ($frm) {
                            $newDLinks[] = "https://v2.zplayer.live/dl?op=" . $frm->find('input[name="op"]', 0)->value . "&id=" . $frm->find('input[name="id"]', 0)->value . "&mode=" . $frm->find('input[name="mode"]', 0)->value . "&hash=" . $frm->find('input[name="hash"]', 0)->value;
                        } else {
                            error_log("zplayer get_download_links {$this->title} => form not found");
                        }
                    } else {
                        $link = $dom->find('#tm-main', 0)->find('.tm-content', 0)->find('.uk-article', 0)->find('.uk-card-small.uk-card-body', 0)->find('.uk-card-small.uk-card-body', 1)->find('.uk-flex', 0)->find('.uk-card', 0)->find('.uk-card-body', 0)->find('a.uk-button', 0);
                        if (!empty($link) && validate_url($link->href) && pathinfo($link->href, PATHINFO_EXTENSION) === 'mp4') {
                            $result[] = [
                                'file' => $link->href,
                                'type' => 'video/mp4',
                                'label' => $data['mode']
                            ];
                        } else {
                            error_log("zplayer get_download_links {$this->title} => download button not found");
                        }
                    }
                } else {
                    error_log("zplayer get_download_links {$this->id} => $status: $err");
                }
                curl_multi_remove_handle($mh, $ch[$i]);
            }
            curl_multi_close($mh);

            if (!empty($newDLinks)) {
                $result = $this->get_download_links($newDLinks);
            }

            if (!empty($result)) {
                $this->status = 'ok';
                return $result;
            }
        }
        return FALSE;
    }

    function get_sources($mp4 = false)
    {
        session_write_close();
        if (!empty($this->id)) {
            if ($mp4) {
                curl_setopt($this->ch, CURLOPT_URL, 'https://v2.zplayer.live/video/' . $this->id);
                curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $head) {
                    if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $head, $cookie) && !empty($cookie[1])) {
                        $this->cookies[] = $cookie[1];
                    }
                    return strlen($head);
                });

                session_write_close();
                $response = curl_exec($this->ch);
                $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                $err = curl_error($this->ch);

                if ($status >= 200 && $status < 400) {
                    $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                    $main = $dom->find('#tm-main', 0)->find('.uk-container', 0)->find('.tm-content', 0)->find('.uk-article', 0)->find('.uk-card-default.uk-card-body', 0)->find('.uk-card-secondary.uk-card-body', 0)->find('.uk-grid-medium', 0);
                    if ($main) {
                        $this->title = $dom->find('title', 0)->plaintext;
                        $cbox = $main->find('.uk-first-column', 0)->find('.uk-grid-medium', 0)->find('.uk-width-expand@s', 1)->find('div', 1);

                        $links = [];
                        $search = ['download_video', '(', ')', "'"];
                        foreach ($cbox->find('button.uk-button') as $btn) {
                            $ex = explode(',', str_replace($search, '', $btn->onclick));
                            if (isset($ex[2])) $links[] = "https://v2.zplayer.live/dl?op=download_orig&id={$ex[0]}&mode={$ex[1]}&hash={$ex[2]}";
                        }
                        if (!empty($links)) {
                            sleep(3);
                            $result = $this->get_download_links($links);
                            return $result;
                        } else {
                            error_log("zplayer get_sources mp4 {$this->id} => download links not found");
                        }
                    } else {
                        error_log("zplayer get_sources mp4 {$this->id} => main content not found");
                    }
                } else {
                    error_log("zplayer get_sources mp4 {$this->id} => $status: $err");
                }
            } else {
                curl_setopt($this->ch, CURLOPT_URL, $this->url);
                curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $head) {
                    if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $head, $cookie) && !empty($cookie[1])) {
                        $this->cookies[] = $cookie[1];
                    }
                    return strlen($head);
                });
                session_write_close();
                $response = curl_exec($this->ch);
                $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                $err = curl_error($this->ch);

                if ($status >= 200 && $status < 400) {
                    $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                    $scripts = $dom->find('script');
                    if (!empty($scripts)) {
                        $eval = '';
                        $script = '';
                        foreach ($scripts as $sc) {
                            if (strpos($sc->innertext, 'eval(function(p,a,c,k,e,d)') !== FALSE) {
                                $eval = $sc->innertext;
                                break;
                            } elseif (preg_match('/file:"([^"]+)"/', $sc->innertext, $dt)) {
                                $script = $sc->innertext;
                                break;
                            }
                        }
                        if (!empty($eval)) {
                            $unpacker = new \JavascriptUnpacker\JavascriptUnpacker();
                            $script = $unpacker->unpack($eval);
                        }
                        if (!empty($script)) {
                            $json = get_string_between($script, 'jwplayer("vplayer").setup(', ');');
                            $json = \OviDigital\JsObjectToJson\JsConverter::convertToJson(strtr($json, ['},]' => '}]']));
                            $json = @json_decode($json, true);
                            if (json_last_error() === JSON_ERROR_NONE) {
                                if (!empty($json['sources'])) {
                                    $this->status = 'ok';
                                    $this->image = $json['image'];
                                    $this->title = $this->title();

                                    foreach ($json['tracks'] as $dt) {
                                        if (isset($dt['kind']) && $dt['kind'] === 'captions') {
                                            $this->tracks[] = [
                                                'file' => $dt['file'],
                                                'label' => $dt['label']
                                            ];
                                        }
                                    }

                                    $result = [];
                                    foreach ($json['sources'] as $dt) {
                                        $result[] = [
                                            'file' => $dt['file'],
                                            'type' => 'hls',
                                            'label' => 'Original'
                                        ];
                                    }
                                    return $result;
                                } else {
                                    error_log("zplayer get_sources {$this->id} => sources not found");
                                }
                            } else {
                                error_log("zplayer get_sources {$this->id} => " . json_last_error_msg());
                            }
                        } else {
                            error_log("zplayer get_sources {$this->id} => eval/script not found");
                        }
                    } else {
                        error_log("zplayer get_sources {$this->id} => scripts not found");
                    }
                } else {
                    error_log("zplayer get_sources {$this->id} => $status: $err");
                }
            }
        }
        return [];
    }

    function get_tracks()
    {
        session_write_close();
        return $this->tracks;
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
