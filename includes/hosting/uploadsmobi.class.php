<?php
class uploadsmobi
{
    public $name = 'Uploads.mobi';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://uploads.mobi/';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = $this->url;
        $this->id = strtr($id, ['embed-' => '', '.html' => '']);
        $this->url .= $this->id;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'accept: */*',
            'origin: https://uploads.mobi'
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $head) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $head, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($head);
        });
    }

    private function post_form(string $response = '')
    {
        session_write_close();
        $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
        $F1 = $dom->find('form[name="F1"]', 0);
        if (!empty($F1)) {
            sleep(11);

            $data['op'] = $F1->find('input[name="op"]', 0)->value;
            $data['id'] = $F1->find('input[name="id"]', 0)->value;
            $data['rand'] = $F1->find('input[name="rand"]', 0)->value;
            $data['referer'] = $F1->find('input[name="referer"]', 0)->value;
            $data['method_free'] = $F1->find('input[name="method_free"]', 0)->value;
            $data['method_premium'] = $F1->find('input[name="method_premium"]', 0)->value;
            $data['adblock_detected'] = $F1->find('input[name="adblock_detected"]', 0)->value;
            $data['down_direct'] = $F1->find('input[name="down_direct"]', 0)->value;

            curl_setopt($this->ch, CURLOPT_URL, $this->url);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, http_build_query($data));
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                $btn = $dom->find('a.btn-outline-primary', 0);
                if (!empty($btn)) {
                    $this->status = 'ok';

                    $title = $dom->find('h6.text-uppercase', 0);
                    $this->title = !empty($title) ? $title->plaintext : '';

                    $this->image = 'https://cdn.u-p.pw/img/' . $this->id . '.jpg';
                    
                    $result[] = [
                        'file' => $btn->href,
                        'label' => 'Original',
                        'type' => 'video/mp4'
                    ];
                    return $result;
                } else {
                    error_log("uploadsmobi post_form {$this->id} => download button not found");
                }
            } else {
                error_log("uploadsmobi post_form {$this->id} => $status: $err");
            }
        } else {
            $pre = $dom->find('pre.bg-dark', 0);
            if (!empty($pre)) {
                $this->status = 'ok';

                $title = $dom->find('h6.text-uppercase', 0);
                $this->title = !empty($title) ? $title->plaintext : '';

                $this->image = 'https://cdn.u-p.pw/img/' . $this->id . '.jpg';
                $this->referer = 'https://uploads.mobi/';
                $result[] = [
                    'file' => $dom->find('a.btn-outline-primary', 0)->href,
                    'label' => 'Original',
                    'type' => 'video/mp4'
                ];
                return $result;
            } else {
                error_log("uploadsmobi post_form {$this->id} => pre.bg-dark dom not found");
            }
        }
        return [];
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            curl_setopt($this->ch, CURLOPT_URL, $this->url);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                $F1 = $dom->find('form[name="F1"]', 0);
                if (!empty($F1)) {
                    //$data['usr_login'] = $F1->find('input[name="usr_login"]', 0)->value;
                    //$data['fname'] = $F1->find('input[name="fname"]', 0)->value;
                    $data['op'] = $F1->find('input[name="op"]', 0)->value;
                    $data['id'] = $F1->find('input[name="id"]', 0)->value;
                    $data['rand'] = $F1->find('input[name="rand"]', 0)->value;
                    $data['referer'] = $F1->find('input[name="referer"]', 0)->value;
                    $data['method_free'] = $F1->find('input[name="method_free"]', 0)->value;
                    $data['method_premium'] = $F1->find('input[name="method_premium"]', 0)->value;
                    $data['adblock_detected'] = $F1->find('input[name="adblock_detected"]', 0)->value;
                    $data['down_direct'] = $F1->find('input[name="down_direct"]', 0)->value;

                    curl_setopt($this->ch, CURLOPT_URL, $this->url);
                    curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($this->ch, CURLOPT_POSTFIELDS, http_build_query($data));
                    $response = curl_exec($this->ch);
                    $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                    $err = curl_error($this->ch);

                    if ($status >= 200 && $status < 400) {
                        return $this->post_form($response);
                    } else {
                        error_log("uploadsmobi get_sources {$this->id} POST => $status: $err");
                    }
                } else {
                    error_log("uploadsmobi get_sources {$this->id} => form F1 not found");
                }
            } else {
                error_log("uploadsmobi get_sources {$this->id} GET => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
