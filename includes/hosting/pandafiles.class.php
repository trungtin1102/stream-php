<?php
class pandafiles
{
    public $name = 'PandaFiles';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://pandafiles.com/';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = $this->url;
        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        $id = explode('?', $id);
        $id = explode('/', $id[0]);
        $this->id = $id[0];
        $this->url .= $this->id;

        list($id, $fname) = array_pad(explode('/', $this->id), 2, '');
        $this->title = $fname;

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'cache-control: max-age=0',
            'content-type: application/x-www-form-urlencoded',
            'origin: https://pandafiles.com',
            'cookie: login=mancingiwakuu; xfss=pjq2u7pga58wlhti; aff=8'
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        $this->cookies = array('login=mancingiwakuu', 'xfss=pjq2u7pga58wlhti', 'aff=8');
    }

    private function getDirectLink($response)
    {
        session_write_close();
        if (!empty($response)) {
            $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
            $video = $dom->find('#direct_link', 0);
            $captcha = $dom->find('.captcha', 0);
            if (!empty($video)) {
                $this->status = 'ok';
                $this->title = basename(parse_url($video->find('a', 0)->href, PHP_URL_PATH));
                $result[] = [
                    'file' => $video->find('a', 0)->href,
                    'type' => 'video/mp4',
                    'label' => 'Original'
                ];
                return $result;
            } elseif (!empty($captcha)) {
                return $this->bypassCaptcha($response);
            } else {
                error_log("pandafiles getDirectLink {$this->id} => direct link not found");
            }
        } else {
            error_log("pandafiles getDirectLink {$this->id} => response is empty");
        }
    }

    private function bypassCaptcha($response)
    {
        session_write_close();
        if (!empty($response)) {
            $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
            $captcha = $dom->find('.captcha', 0);
            if (!empty($captcha)) {
                $span = $captcha->find('table', 0)->find('tr', 1)->find('td', 0)->find('span');
                $captcha = [];
                foreach ($span as $dt) {
                    $captcha[] = array(
                        'pos' => get_string_between($dt->style, 'padding-left:', 'px;'),
                        'val' => html_entity_decode($dt->plaintext)
                    );
                }
                if (!empty($captcha)) {
                    usort($captcha, function ($a, $b) {
                        if ($a['pos'] === $b['pos']) return 0;
                        elseif (intval($a['pos']) < intval($b['pos'])) return -1;
                        return 1;
                    });
                    $captcha = array_column($captcha, 'val');
                    $captcha = trim(implode('', $captcha));

                    curl_setopt($this->ch, CURLOPT_URL, $this->url);
                    curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($this->ch, CURLOPT_POSTFIELDS, 'op=download2&id=' . $this->id . '&rand=' . $dom->find('form[name="F1"]', 0)->find('input[name="rand"]', 0)->value . '&referer=' . $this->url . '&method_free=Free%20Download&method_premium=&adblock_detected=0&code=' . $captcha);

                    $response = curl_exec($this->ch);
                    $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                    $err = curl_error($this->ch);

                    if ($status >= 200 && $status < 400) {
                        $captcha = $dom->find('.captcha', 0);
                        if (!empty($captcha)) {
                            return $this->bypassCaptcha($response);
                        } else {
                            return $this->getDirectLink($response);
                        }
                    } else {
                        error_log("pandafiles get_sources {$this->id} => $status: $err");
                    }
                } else {
                    error_log("pandafiles get_sources {$this->id} => captcha not found");
                }
            } else {
                return $this->getDirectLink($response);
            }
        } else {
            error_log("pandafiles bypassCaptcha {$this->id} => response is empty");
        }
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            curl_setopt($this->ch, CURLOPT_URL, $this->url);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, 'op=download1&usr_login=&id=' . $this->id . '&fname=' . $this->title . '&referer=&method_free=Free%20Download');
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);
            if ($status >= 200 && $status < 400) {
                return $this->bypassCaptcha($response);
            } else {
                error_log("pandafiles get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
