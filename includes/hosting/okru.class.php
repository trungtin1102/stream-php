<?php
class okru
{
    public $name = 'OK.ru';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://ok.ru/videoembed/';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();

        $this->referer = 'https://ok.ru/';
        $this->id = $id;
        $this->url .= $this->id;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'host: ok.ru',
            'origin: https://ok.ru',
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    private function mp4Sources($sources = [])
    {
        $res = ['mobile' => '144p', 'lowest' => '240p', 'low' => '360p', 'sd' => '480p', 'hd' => '720p', 'full' => '1080p', 'quad' => '1440p', 'ultra' => '2160p'];
        $result = [];
        foreach ($sources as $video) {
            $label = strtr($video['name'], $res);
            $result[] = [
                'file' => $video['url'],
                'type' => 'video/mp4',
                'label' => $label,
            ];
        }
        return $result;
    }

    function get_sources($mp4 = false)
    {
        session_write_close();
        $response = curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);
        if ($status >= 200 && $status < 400) {
            $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
            $attr = 'data-options';
            $options = $dom->find('div[data-module="OKVideo"]', 0);
            if (!empty($options)) {
                $json = json_decode(htmlspecialchars_decode($options->$attr), TRUE);

                $this->image = $json['poster'];

                if (!empty($json['flashvars']['metadata'])) {
                    $json = json_decode($json['flashvars']['metadata'], TRUE);
                    $this->title = $json['movie']['title'];
                    if ($mp4) {
                        $this->status = 'ok';

                        return $this->mp4Sources($json['videos']);
                    } elseif (!empty($json['ondemandHls'])) {
                        $this->status = 'ok';

                        $result[] = [
                            'file' => $json['ondemandHls'],
                            'type' => 'hls',
                            'label' => 'Original',
                        ];
                        return $result;
                    } elseif (!empty($json['ondemandDash'])) {
                        $this->status = 'ok';

                        $result[] = [
                            'file' => $json['ondemandDash'],
                            'type' => 'mpd',
                            'label' => 'Original',
                        ];
                        return $result;
                    } elseif (!empty($json['hlsManifestUrl'])) {
                        $this->status = 'ok';

                        $result[] = [
                            'file' => $json['hlsManifestUrl'],
                            'type' => 'hls',
                            'label' => 'Original',
                        ];
                        return $result;
                    } elseif (!empty($json['hlsMasterPlaylistUrl'])) {
                        $this->status = 'ok';

                        $result[] = [
                            'file' => $json['hlsMasterPlaylistUrl'],
                            'type' => 'hls',
                            'label' => 'Original',
                        ];
                        return $result;
                    } else {
                        return $this->mp4Sources($json['videos']);
                    }
                } else {
                    error_log("okru get_sources {$this->id} => flashvars metadata not found");
                }
            } else {
                error_log("okru get_sources {$this->id} => data-module okvideo not found");
            }
        } else {
            error_log("streamsb get_sources {$this->id} => $status: $err");
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
