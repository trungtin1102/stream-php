<?php
class gofile
{
    public $name = 'GoFile';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://api.gofile.io/';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = 'https://gofile.io/';
        $this->id = $id;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'origin: https://gofile.io'
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, 'https://gofile.io/');
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    private function websiteToken()
    {
        session_write_close();
        curl_setopt($this->ch, CURLOPT_URL, 'https://gofile.io/contents/files.html');
        curl_setopt($this->ch, CURLOPT_COOKIE, implode(',', $this->cookies));
        $response = curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);
        if ($status >= 200 && $status < 400) {
            return trim(get_string_between($response, 'websiteToken: "', '"'));
        } else {
            error_log("gofile websiteToken {$this->id} => $status: $err");
        }
        return '';
    }

    private function getContent($token, $websiteToken)
    {
        session_write_close();
        $url = $this->url . "getContent?contentId={$this->id}&token=$token&websiteToken=$websiteToken&cache=true";
        curl_setopt($this->ch, CURLOPT_URL, $url);
        $response = curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);

        if ($status >= 200 && $status < 400) {
            $data = json_decode($response, true);
            if ($data['status'] === 'ok') {
                $keys = array_keys($data['data']['contents']);
                if (!empty($data['data']['contents'][$keys[0]])) {
                    $this->status = 'ok';
                    $this->title = $data['data']['contents'][$keys[0]]['name'];
                    $result[] = [
                        'file' => $data['data']['contents'][$keys[0]]['link'],
                        'type' => 'video/mp4',
                        'label' => 'Original'
                    ];
                    return $result;
                } else {
                    error_log("gofile getContent $url => {$keys[0]} content not found");
                }
            } else {
                error_log("gofile getContent $url => $response");
            }
        } else {
            error_log("gofile getContent $url => $status: $err");
        }
    }

    private function createAccountToken()
    {
        session_write_close();
        curl_setopt($this->ch, CURLOPT_URL, 'https://api.gofile.io/createAccount');
        $response = curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);

        if ($status >= 200 && $status < 400) {
            $data = json_decode($response, true);
            if (isset($data['data']['token'])) return $data['data']['token'];
            else error_log("gofile createAccountToken {$this->id} => $response");
        } else {
            error_log("gofile createAccountToken {$this->id} => $status: $err");
        }
    }

    function get_sources()
    {
        session_write_close();
        curl_setopt($this->ch, CURLOPT_URL, 'https://gofile.io/d/' . $this->id);
        curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);

        if ($status >= 200 && $status < 400) {
            $cookies = array_filter($this->cookies, function ($val) {
                return strpos($val, 'accountToken=') !== false;
            });
            $cookies = array_values($cookies);
            if (!empty($cookies)) {
                list($key, $token) = array_pad(explode('=', $cookies[0]), 2, '');
            } else {
                $token = $this->createAccountToken();
                $this->cookies[] = 'accountToken=' . $token;
            }
            if (!empty($token)) {
                $websiteToken = $this->websiteToken();
                if (!empty($websiteToken)) {
                    return $this->getContent($token, $websiteToken);
                } else {
                    error_log("gofile get_sources {$this->id} => websiteToken is empty");
                }
            } else {
                error_log("gofile get_sources {$this->id} => token is empty");
            }
        } else {
            error_log("gofile get_sources {$this->id} => $status: $err");
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
