<?php
class mymailru
{
    public $name = 'Video Mail.Ru';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://my.mail.ru/video/embed/';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();

        $this->referer = 'https://my.mail.ru/';
        $this->id = $id;
        $this->url .= $id;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'host: my.mail.ru',
            'origin: https://my.mail.ru',
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html(htmlspecialchars_decode($response));
                $div = $dom->find('div[data-mru-fragment="video/embed/main"]', 0);
                if (!empty($div)) {
                    $script = $div->find('script', 0);
                    if (!empty($script)) {
                        $json = json_decode(trim($script->innertext), true);
                        if (!empty($json['flashVars']['metadataUrl'])) {
                            $url = 'https:' . $json['flashVars']['metadataUrl'] . '?xemail=&ajax_call=1&func_name=&mna=&mnb=&ext=1&_=' . time();
                            curl_setopt($this->ch, CURLOPT_URL, $url);
                            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                                'Accept: application/json, text/javascript, */*; q=0.01',
                                'host: my.mail.ru',
                                'origin: https://my.mail.ru',
                                'cookie: ' . implode(';', $this->cookies),
                                'x-requested-with: XMLHttpRequest',
                            ));
                            curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
                            curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);

                            session_write_close();
                            $response = curl_exec($this->ch);
                            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                            $err = curl_error($this->ch);

                            if ($status >= 200 && $status < 400) {
                                $json = json_decode($response, true);
                                if (!empty($json['videos'])) {
                                    $this->status = 'ok';
                                    $this->title = $json['meta']['title'];
                                    $this->image = 'https:' . $json['meta']['poster'];
                                    $result = [];
                                    foreach ($json['videos'] as $dt) {
                                        $result[] = [
                                            'file' => 'https:' . $dt['url'],
                                            'type' => 'video/mp4',
                                            'label' => $dt['key']
                                        ];
                                    }
                                    return $result;
                                }
                            } else {
                                error_log("mymailru get_sources {$this->id} => $url => $status: $err");
                            }
                        } else {
                            error_log("mymailru get_sources {$this->id} => metadataUrl not found");
                        }
                    } else {
                        error_log("mymailru get_sources {$this->id} => script not found");
                    }
                } else {
                    error_log("mymailru get_sources {$this->id} => data-mru-fragment not found");
                }
            } else {
                error_log("mymailru get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
