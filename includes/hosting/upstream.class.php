<?php
class upstream
{
    public $name = 'UpStream';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://upstream.to/';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = $this->url;
        $this->id = strtr($id, ['embed-' => '', '.html' => '']);
        $this->url .= $this->id . '.html';

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'host: upstream.to',
            'origin: https://upstream.to',
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $head) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $head, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($head);
        });
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                $this->title = trim(strtr($dom->find('title', 0)->plaintext, ['Watching' => '', 'Watch' => '', 'Download' => '', 'UpStream' => '']), " -");
                $scripts = $dom->find('script');
                if (!empty($scripts)) {
                    $eval = '';
                    foreach ($scripts as $sc) {
                        if (strpos($sc->innertext, 'eval(function(p,a,c,k,e,d)') !== FALSE) {
                            $eval = $sc->innertext;
                            break;
                        }
                    }
                    if (!empty($eval)) {
                        $unpacker = new \JavascriptUnpacker\JavascriptUnpacker();
                        $script = $unpacker->unpack($eval);
                        $json = get_string_between($script, 'jwplayer("vplayer").setup(', ');');
                        if (preg_match('/file:"([^"]+)"/', $json, $file) && !empty($file[1])) {
                            $this->status = 'ok';
                            $this->image = preg_match('/image:"([^"]+)"/', $json, $image) && !empty($image[1]) ? $image[1] : '';
                            $result[] = [
                                'file' => $file[1],
                                'type' => 'hls',
                                'label' => 'Original'
                            ];
                            return $result;
                        } else {
                            error_log('upstream get_sources ' . $this->id . ' => file not found');
                        }
                    } else {
                        error_log('upstream get_sources ' . $this->id . ' => eval not found');
                    }
                } else {
                    error_log('upstream get_sources ' . $this->id . ' => script not found');
                }
            } else {
                error_log("upstream get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
