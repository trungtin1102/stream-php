<?php
class videobin
{
    public $name = 'Videobin';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://videobin.co/';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = $this->url;
        $this->id = strtr($id, ['embed-' => '', '.html' => '']);

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'host: videobin.co',
            'origin: https://videobin.co',
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $head) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $head, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($head);
        });
    }

    private function parse_sources($response = '', $mp4 = false)
    {
        session_write_close();
        $start = explode('Clappr.Player(', $response);
        $end = explode('.jpg",', end($start));
        $jsObject = $end[0] . '.jpg"}';
        $json = \OviDigital\JsObjectToJson\JsConverter::convertToJson($jsObject);
        $arr = @json_decode($json, true);
        if (json_last_error() == JSON_ERROR_NONE) {
            if (!empty($arr['sources'])) {
                $this->status = 'ok';
                $this->image = $arr['poster'];

                $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                $this->title = !empty($dom->find('title', 0)) ? trim(strtr($dom->find('title', 0)->plaintext, ['Watch' => ''])) : '';

                $result = [];
                $quality = ['720p', '360p'];
                $i = 0;
                if ($mp4) {
                    foreach ($arr['sources'] as $dt) {
                        if (strpos($dt, '.m3u') === FALSE) {
                            $result[] = [
                                'file' => $dt,
                                'type' => 'video/mp4',
                                'label' => $quality[$i]
                            ];
                            $i++;
                        }
                    }
                } else {
                    foreach ($arr['sources'] as $dt) {
                        if (strpos($dt, '.m3u') !== FALSE) {
                            $result[] = [
                                'file' => $dt,
                                'type' => 'hls',
                                'label' => 'Original'
                            ];
                            break;
                        } else {
                            $result[] = [
                                'file' => $dt,
                                'type' => 'video/mp4',
                                'label' => $quality[$i]
                            ];
                            $i++;
                        }
                    }
                }
                return $result;
            } else {
                error_log("videobin parse_sources {$this->id} => sources not found");
            }
        } else {
            error_log("videobin parse_sources {$this->id} => " . json_last_error_msg());
        }
        return [];
    }

    function get_sources($mp4 = false)
    {
        session_write_close();
        if (!empty($this->id)) {
            $url = $this->url . $this->id . '.html';
            curl_setopt($this->ch, CURLOPT_URL, $url);
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);
            if ($status >= 200 && $status < 400) {
                $downloadPage = $this->parse_sources($response, $mp4);
                if (!empty($downloadPage)) {
                    return $downloadPage;
                } else {
                    $url = $this->url . 'embed-' . $this->id . '.html';
                    curl_setopt($this->ch, CURLOPT_URL, $url);
                    session_write_close();
                    $response = curl_exec($this->ch);
                    $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                    $err = curl_error($this->ch);
                    if ($status >= 200 && $status < 400) {
                        return $this->parse_sources($response, $mp4);
                    } else {
                        error_log("videobin get_sources {$this->id} => $url => $status: $err");
                    }
                }
            } else {
                error_log("videobin get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
