<?php
class onedrive
{
    public $name = 'One Drive';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = '';
    private $cookies = [];

    function __construct($id = '')
    {
        session_write_close();

        if (validate_url($id)) {
            $this->id = $id;
            $this->url = $id;
        } else {
            $this->id = strpos($id, 'resid') !== FALSE ? $id : 'resid=' . $id;
            $this->url = 'https://onedrive.live.com/GetDownloadUrl?' . $this->id;
        }

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        $this->referer = $scheme . '://' . $host . '/';

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    function get_sources()
    {
        session_write_close();
        if (validate_url($this->id)) {
            if (strpos($this->url, 'authkey=') !== FALSE) {
                $url = parse_url($this->url, PHP_URL_QUERY);
                parse_str(rawurldecode($url), $qry);
                $authKey = isset($qry['authkey']) ? $qry['authkey'] : '';
                $id = isset($qry['id']) ? $qry['id'] : '';
                if (!empty($id)) list($cid, $trash) = array_pad(explode('!', $id), 2, '');
                else $cid = '';

                if (!empty($authKey) && !empty($id) && !empty($cid)) {
                    curl_setopt($this->ch, CURLOPT_URL, "https://api.onedrive.com/v1.0/drives/$cid/items/$id?select=id%2C%40content.downloadUrl&authkey=$authKey");
                    curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                        'accept: application/json',
                        'accept-encoding: gzip, deflate, br',
                        'accept-language: id,id-ID;q=0.9',
                        'user-agent: ' . USER_AGENT,
                        'origin: https://onedrive.live.com',
                        'referer: https://onedrive.live.com/'
                    ));
                    $response = curl_exec($this->ch);
                    $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                    $err = curl_error($this->ch);

                    if ($status >= 200 && $status < 400) {
                        $data = json_decode($response, true);
                        if (isset($data['@content.downloadUrl'])) {
                            $this->status = 'ok';
                            $result[] = [
                                'file' => $data['@content.downloadUrl'],
                                'type' => 'video/mp4',
                                'label' => 'Original'
                            ];
                            return $result;
                        } else {
                            error_log("onedrive get_sources {$this->id} => $status: $response");
                        }
                    } else {
                        error_log("onedrive get_sources {$this->id} => $status: $err");
                    }
                } else {
                    error_log("onedrive get_sources {$this->id} => authKey, cid or id query not found");
                }
            } else {
                $logFile = execHeadlessChromium($this->url, 'onedrive~' . keyFilter($this->id), true);
                if ($logFile) {
                    open_resources_handler();
                    $fp = @fopen($logFile, 'rb');
                    if ($fp) {
                        stream_set_blocking($fp, false);
                        $response = stream_get_contents($fp);
                        fclose($fp);

                        if ($response) {
                            @unlink($logFile);
                            if (strpos($response, 'rootFolder\":\"') !== false) {
                                $rootFolder = get_string_between($response, 'rootFolder\":\"', '\"');
                            } elseif (strpos($response, 'rootFolder":"') !== false) {
                                $rootFolder = get_string_between($response, 'rootFolder":"', '"');
                            }
                            if (strpos($response, 'webAbsoluteUrl\":\"') !== false) {
                                $webAbsoluteUrl = get_string_between($response, 'webAbsoluteUrl\":\"', '\"');
                            } elseif (strpos($response, 'webAbsoluteUrl":"') !== false) {
                                $webAbsoluteUrl = get_string_between($response, 'webAbsoluteUrl":"', '"');
                            }
                            if (strpos($response, 'layoutsUrl\":\"') !== false) {
                                $layoutsUrl = get_string_between($response, 'layoutsUrl\":\"', '\"');
                            } elseif (strpos($response, 'layoutsUrl":"') !== false) {
                                $layoutsUrl = get_string_between($response, 'layoutsUrl":"', '"');
                            }
                            if (strpos($response, 'correlationID\":\"') !== false) {
                                $correlationID = get_string_between($response, 'correlationID\":\"', '\"');
                            } elseif (strpos($response, 'correlationID":"') !== false) {
                                $correlationID = get_string_between($response, 'correlationID":"', '"');
                            }
                            if (strpos($response, 'set-cookie":"') !== false) {
                                $this->cookies[] = get_string_between($response, 'set-cookie":"', ';');
                            }
                            if (!empty($webAbsoluteUrl) && !empty($layoutsUrl) && !empty($rootFolder) && !empty($correlationID) && !empty($this->cookies)) {
                                $this->status = 'ok';
                                $this->title = basename($rootFolder);
                                $this->image = '';
                                $videoURL = $webAbsoluteUrl . '/' . $layoutsUrl . '/download.aspx?SourceUrl=' . strtr(rawurlencode($rootFolder), ['_' => '%5F', '.' => '%2E']) . '&correlationid=' . $correlationID;
                                $result[] = [
                                    'file' => $videoURL,
                                    'type' => 'video/mp4',
                                    'label' => 'Original'
                                ];
                                return $result;
                            }
                        } else {
                            error_log("onedrive get_sources {$this->id} => cannot read $logFile");
                        }
                    } else {
                        error_log("onedrive get_sources {$this->id} => cannot open $logFile");
                    }
                } else {
                    error_log("onedrive get_sources {$this->id} => cannot create logFile");
                }
            }
        } else {
            curl_setopt($this->ch, CURLOPT_URL, $this->url);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                'accept: application/json',
                'accept-encoding: gzip, deflate, br',
                'accept-language: id,id-ID;q=0.9',
                'appid: 1141147648',
                'cache-control: private',
                'dnt: 1',
                'pragma: no-cache',
                'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36 OPR/80.0.4170.72',
                'x-forcecache: 1'
            ));
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $data = json_decode($response, true);
                if (isset($data['DownloadUrl'])) {
                    $this->status = 'ok';
                    $this->referer = 'https://onedrive.live.com/?' . $this->id;

                    if (strpos($data['DownloadUrl'], '.mp4') !== FALSE) {
                        $result[] = [
                            'file' => $data['DownloadUrl'],
                            'type' => 'video/mp4',
                            'label' => 'Original'
                        ];
                    } else {
                        $path = parse_url($data['DownloadUrl'], PHP_URL_PATH);
                        $result[] = [
                            'file' => strtr($data['DownloadUrl'], ['/' . basename($path) => '/']) . '&videoformat=dash&part=index&pretranscode=0&transcodeahead=0',
                            'type' => 'mpd',
                            'label' => 'Original'
                        ];
                    }
                    return $result;
                } else {
                    error_log("onedrive get_sources {$this->id} => DownloadUrl not found");
                }
            } else {
                error_log("onedrive get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_url()
    {
        return $this->url;
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
