<?php
class fembed
{
    public $name = 'Fembed';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $tracks = [];
    private $url = '';
    private $host = 'fembed.com';
    private $cookies = [];
    private $ch;
    private $logURL = '';
    private $restrictDomain = 'suzihaza.com';

    function __construct($id = '')
    {
        session_write_close();

        $host = parse_url($id, PHP_URL_HOST);
        $scheme = parse_url($id, PHP_URL_SCHEME);
        $port = parse_URL($id, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;

        $this->host = 'suzihaza.com';
        $ipv4 = gethostbyname($this->host);
        $this->id = strtr($id, [$host => $this->host]);
        $this->referer = 'https://' . $this->host . '/';

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array(implode(':', array($this->host, $port, $ipv4))));
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    function set_restrictDomain(string $domain = 'suzihaza.com')
    {
        session_write_close();
        $this->restrictDomain = $domain;
    }

    private function title()
    {
        session_write_close();
        if (!empty($this->id)) {
            $url = strtr($this->id, ['/f/' => '/v/']);
            curl_setopt($this->ch, CURLOPT_URL, $url);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($this->ch, CURLOPT_REFERER, $url);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                'accept: */*',
                'cache-control: no-cache',
                'pragma: no-cache',
                'origin: https://' . $this->host
            ));

            session_write_close();
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                $title = $dom->find('title', 0);
                if (!empty($title)) return ltrim($title->plaintext, 'Video ');
                else error_log("fembed title {$url} => title element not found");
            } else {
                error_log("fembed title {$url} => $status: $err");
            }
        }
        return '';
    }

    private function watch()
    {
        session_write_close();
        $id = explode('/', trim($this->id, '/'));
        $id = end($id);
        $user = explode('/', trim($this->image, '/'));
        $user = array_filter($user, function ($v) {
            return is_numeric($v);
        });
        $user = array_values($user);
        if (!empty($user) && !empty($id)) {
            curl_setopt($this->ch, CURLOPT_URL, 'https://v3.fstats.xyz/watch');
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, 'data=' . rawurlencode('{"id":"' . $id . '","user":"' . $user[0] . '","ref":"","vip":0}'));
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                'accept: */*',
                'cache-control: no-cache',
                'pragma: no-cache',
                'content-type: application/x-www-form-urlencoded; charset=UTF-8',
                'origin: https://' . $this->host,
                "x-requested-with: XMLHttpRequest"
            ));
            curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
            curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);

            session_write_close();
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $arr = json_decode($response, true);
                if (isset($arr['success']) && $arr['success']) {
                    $this->logURL = "https://v3.fstats.xyz/log?u={$user[0]}&h=$id&" . http_build_query($arr['data']);
                } else {
                    error_log("fembed watch {$this->id} => $response");
                }
            } else {
                error_log("fembed watch {$this->id} => $status: $err");
            }
        }
    }

    function send_stats(string $logURL = '')
    {
        session_write_close();
        $url = validate_url($logURL) ? $logURL : $this->logURL;
        if (!empty($url)) {
            curl_setopt($this->ch, CURLOPT_URL, $url);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                'accept: */*',
                'cache-control: no-cache',
                'pragma: no-cache',
                'origin: https://' . $this->host,
                "x-requested-with: XMLHttpRequest"
            ));
            curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
            curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);

            session_write_close();
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                return $response;
            } else {
                error_log("fembed send_stats $url => $status: $err");
            }
        }
        return false;
    }

    private function getNewSources(array $oldSources = [])
    {
        session_write_close();

        $headers = [];
        $mh = curl_multi_init();
        $ch = [];

        foreach ($oldSources as $i => $dt) {
            $ch[$i] = curl_init($dt['file']);
            curl_setopt($ch[$i], CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch[$i], CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch[$i], CURLOPT_FOLLOWLOCATION, 0);
            curl_setopt($ch[$i], CURLOPT_MAXREDIRS, 5);
            curl_setopt($ch[$i], CURLOPT_ENCODING, '');
            curl_setopt($ch[$i], CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch[$i], CURLOPT_DNS_SERVERS, dns_servers());
            curl_setopt($ch[$i], CURLOPT_NOSIGNAL, true);
            curl_setopt($ch[$i], CURLOPT_DNS_CACHE_TIMEOUT, 300);
            curl_setopt($ch[$i], CURLOPT_TCP_KEEPALIVE, true);
            curl_setopt($ch[$i], CURLOPT_TCP_NODELAY, true);
            curl_setopt($ch[$i], CURLOPT_FORBID_REUSE, true);
            curl_setopt($ch[$i], CURLOPT_FAILONERROR, true);
            curl_setopt($ch[$i], CURLOPT_TIMEOUT, 10);
            curl_setopt($ch[$i], CURLOPT_HEADER, true);
            curl_setopt($ch[$i], CURLOPT_NOBODY, true);
            curl_setopt($ch[$i], CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch[$i], CURLOPT_COOKIE, trim(implode(';', $this->cookies), ';'));
            curl_setopt($ch[$i], CURLOPT_USERAGENT, USER_AGENT);
            curl_multi_add_handle($mh, $ch[$i]);
        }

        $active = null;
        do {
            $mrc = curl_multi_exec($mh, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc == CURLM_OK) {
            if (curl_multi_select($mh) == -1) {
                usleep(10);
            }
            do {
                $mrc = curl_multi_exec($mh, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }

        $result = [];
        foreach ($oldSources as $i => $dt) {
            $effectiveURL = curl_getinfo($ch[$i], CURLINFO_EFFECTIVE_URL);
            $redirectURL = curl_getinfo($ch[$i], CURLINFO_REDIRECT_URL);
            $status = curl_getinfo($ch[$i], CURLINFO_RESPONSE_CODE);
            $type = curl_getinfo($ch[$i], CURLINFO_CONTENT_TYPE);
            $err = curl_error($ch[$i]);
            if ($status >= 200 && $status < 400) {
                if (!empty($effectiveURL) && $dt['file'] !== $effectiveURL) {
                    $result[] = [
                        'file' => $effectiveURL,
                        'label' => $dt['label'],
                        'type' => $dt['type']
                    ];
                } elseif (!empty($redirectURL) && $dt['file'] !== $redirectURL) {
                    $result[] = [
                        'file' => $redirectURL,
                        'label' => $dt['label'],
                        'type' => $dt['type']
                    ];
                } else {
                    $result[] = [
                        'file' => $dt['file'],
                        'label' => $dt['label'],
                        'type' => $dt['type']
                    ];
                }
            } else {
                error_log("fembed getNewSources {$dt['file']} => $type => $status: $err");
                $result[] = [
                    'file' => $dt['file'],
                    'label' => $dt['label'],
                    'type' => $dt['type']
                ];
            }
            curl_multi_remove_handle($mh, $ch[$i]);
        }
        curl_multi_close($mh);
        return $result;
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            $header = array(
                'accept: */*',
                'cache-control: no-cache',
                'pragma: no-cache',
                'content-type: application/x-www-form-urlencoded; charset=UTF-8',
                'origin: https://' . $this->host,
                "x-requested-with: XMLHttpRequest"
            );
            $proxy = rate_limit_ips();
            if ($proxy) {
                $header[] = 'X-Originating-IP: ' . $proxy;
                $header[] = 'X-Forwarded-For: ' . $proxy;
                $header[] = 'X-Remote-IP: ' . $proxy;
                $header[] = 'X-Remote-Addr: ' . $proxy;
                $header[] = 'X-Client-IP: ' . $proxy;
                $header[] = 'X-Host: ' . $proxy;
                $header[] = 'X-Forwared-Host: ' . $proxy;
            }
            $url = strtr($this->id, ['/v/' => '/api/source/', '/f/' => '/api/source/']);
            curl_setopt($this->ch, CURLOPT_URL, $url);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, 'r=' . $this->url . '&d=' . $this->restrictDomain);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
            curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);

            session_write_close();
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400 && !empty($response)) {
                $arr = json_decode($response, true);
                if (isset($arr['success'])) {
                    if ($arr['success']) {
                        $this->status = 'ok';
                        $this->image = "https://thumb.fvs.io/asset" . $arr['player']['poster_file'];
                        $this->title = $this->title();
                        $this->watch();

                        if (!empty($arr['captions'])) {
                            $ex = explode('/', trim($arr['player']['poster_file'], '/'));
                            $cbase = "https://thumb.fvs.io/asset/" . $ex[0] . "/" . $ex[1] . '/caption/';
                            foreach ($arr['captions'] as $dt) {
                                $this->tracks[] = [
                                    'file' => $cbase . $dt['hash'] . "/" . $dt['id'] . "." . $dt['extension'],
                                    'label' => $dt['language']
                                ];
                            }
                        }
                        $result = [];
                        foreach ($arr['data'] as $dt) {
                            $result[] = [
                                'file' => $dt['file'],
                                'type' => 'video/mp4',
                                'label' => $dt['label']
                            ];
                        }
                        $result = $this->getNewSources($result);
                        return $result;
                    } elseif (!empty($arr['player']['restrict_domain'])) {
                        $this->set_restrictDomain($arr['player']['restrict_domain']);
                        return $this->get_sources();
                    } else {
                        error_log("fembed get_sources {$this->id} => $response");
                    }
                } else {
                    error_log("fembed get_sources {$this->id} => $response");
                }
            } else {
                error_log("fembed get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_logURL()
    {
        session_write_close();
        return $this->logURL;
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_tracks()
    {
        session_write_close();
        return $this->tracks;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
