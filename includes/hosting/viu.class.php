<?php
class viu
{
    public $name = 'VIU';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = '';
    private $cookies = [];
    private $ch;
    private $proxy = false;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = 'https://www.viu.com/';
        $this->url = $id;
        $id = explode('?', $id);
        $id = explode('-', $id[0]);
        $this->id = end($id);

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'host: ' . $host,
            'origin: https://' . $host,
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            // cek penggunaan proxy
            $this->proxy = proxy_rotator();
            if ($this->proxy) {
                curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy['proxy']);
                curl_setopt($this->ch, CURLOPT_PROXYTYPE, $this->proxy['type']);
                curl_setopt($this->ch, CURLOPT_PROXYUSERPWD, $this->proxy['usrpwd']);
            }
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $drm = preg_match('/"drm_content_url":"([^"]+)"/', $response, $url) ? $url[1] : '';
                $iid = preg_match('/"iid":"([^"]+)"/', $response, $id) ? $id[1] : '';
                $session_id = preg_match('/"sessionId":"([^"]+)"/', $response, $session) ? $session[1] : '';
                if (empty($session_id)) {
                    $session_id = preg_match('/"session_id":"([^"]+)"/', $response, $session) ? $session[1] : '';
                }
                $ccode = preg_match('/"ccode":"([^"]+)"/', $response, $cc) ? $cc[1] : '';

                $this->title = preg_match('/"display_title":"([^"]+)"/', $response, $title) ? $title[1] : '';
                $this->image = preg_match('/"thumbnailUrl":\["([^"]+)"\]/', $response, $img) ? $img[1] : '';

                if (!empty($drm) && !empty($ccode) && !empty($iid) && !empty($session_id)) {
                    $url = "https://um.viuapi.io/user/identity?ver=1.0&fmt=json&aver=5.0&appver=2.0&appid=viu_desktop&platform=desktop&iid=$iid";
                    $scheme = parse_url($url, PHP_URL_SCHEME);
                    $host = parse_url($url, PHP_URL_HOST);
                    $port = parse_URL($url, PHP_URL_PORT);
                    if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
                    $ipv4 = gethostbyname($host);
                    $resolveHost = implode(':', array($host, $port, $ipv4));

                    curl_setopt($this->ch, CURLOPT_URL, $url);
                    curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
                    curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode(array(
                        'deviceId' => $iid
                    )));
                    curl_setopt($this->ch, CURLOPT_HEADER, 0);
                    curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                        'accept: application/json',
                        'content-type: application/json',
                        'host: um.viuapi.io',
                        'origin: https://viu.com',
                        'referer: https://viu.com/',
                        'x-client: browser',
                        'x-session-id: ' . $session_id
                    ));
                    curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $head) {
                        if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $head, $cookie) && !empty($cookie[1])) {
                            $this->cookies[] = $cookie[1];
                        }
                        return strlen($head);
                    });
                    // cek penggunaan proxy
                    if ($this->proxy) {
                        curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy['proxy']);
                        curl_setopt($this->ch, CURLOPT_PROXYTYPE, $this->proxy['type']);
                        curl_setopt($this->ch, CURLOPT_PROXYUSERPWD, $this->proxy['usrpwd']);
                    }

                    session_write_close();
                    $response = curl_exec($this->ch);
                    $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                    $err = curl_error($this->ch);

                    if ($status >= 200 && $status < 400) {
                        $data = @json_decode($response, TRUE);
                        if (!empty($data['token'])) {
                            $url = $drm . $this->id;
                            $scheme = parse_url($url, PHP_URL_SCHEME);
                            $host = parse_url($url, PHP_URL_HOST);
                            $port = parse_URL($url, PHP_URL_PORT);
                            if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
                            $ipv4 = gethostbyname($host);
                            $resolveHost = implode(':', array($host, $port, $ipv4));

                            curl_setopt($this->ch, CURLOPT_URL, $url);
                            curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
                            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
                            curl_setopt($this->ch, CURLOPT_POSTFIELDS, '');
                            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                                'authorization: ' . $data['token'],
                                'origin: https://viu.com',
                                'referer: https://viu.com/',
                                'x-client: browser',
                                'x-session-id: ' . $session_id,
                                'ccode: ' . $ccode,
                                'actiontype: s',
                                'content-length: 0',
                                'drm_level: l3',
                                'hdcp_level: none',
                            ));

                            session_write_close();
                            $response = curl_exec($this->ch);
                            $err = curl_error($this->ch);

                            if (!$err) {
                                $data = @json_decode($response, TRUE);
                                if (!empty($data['playUrl'])) {
                                    $this->status = 'ok';
                                    $result[] = [
                                        'file' => $data['playUrl'],
                                        'type' => 'hls',
                                        'label' => 'Playlist'
                                    ];
                                    return $result;
                                }
                            }
                        } else {
                            error_log("viu get_sources {$this->id} => token not found => $response");
                        }
                    } elseif($this->proxy) {
                        error_log("Proxy {$this->proxy['format']} doesn't work => $status: $err");
                        $proxyList = proxy_list();
                        $key = array_search($this->proxy['format'], $proxyList);
                        if ($key) {
                            $brokenProxy = $proxyList[$key];
                            unset($proxyList[$key]);
                            $proxyList = array_values($proxyList);
                            if (count($proxyList) > 0) {
                                if (!filter_var(get_option('delete_unused_proxy'), FILTER_VALIDATE_BOOLEAN)) $proxyList[] = $brokenProxy;
                                $proxyList = implode("\n", $proxyList);
                                set_option('proxy_list', $proxyList);
                                return $this->get_sources();
                            } else {
                                set_option('proxy_list', []);
                            }
                        }
                    } else {
                        error_log("viu get_sources {$this->id} => $status: $err");
                    }
                } elseif($this->proxy) {
                    error_log("viu get_sources {$this->id} => iid=$iid, session_id=$session_id, ccode=$ccode, drm=$drm");
                    $proxyList = proxy_list();
                    $key = array_search($this->proxy['format'], $proxyList);
                    if ($key) {
                        $brokenProxy = $proxyList[$key];
                        unset($proxyList[$key]);
                        $proxyList = array_values($proxyList);
                        if (count($proxyList) > 0) {
                            if (!filter_var(get_option('delete_unused_proxy'), FILTER_VALIDATE_BOOLEAN)) $proxyList[] = $brokenProxy;
                            $proxyList = implode("\n", $proxyList);
                            set_option('proxy_list', $proxyList);
                            return $this->get_sources();
                        } else {
                            set_option('proxy_list', []);
                        }
                    }
                } else {
                    error_log("viu get_sources {$this->id} => $status: $err");
                }
            } elseif ($this->proxy) {
                error_log("viu get_sources {$this->id} => proxy {$this->proxy['format']} doesn't work => $status: $err");
                $proxyList = proxy_list();
                $key = array_search($this->proxy['format'], $proxyList);
                if ($key) {
                    $brokenProxy = $proxyList[$key];
                    unset($proxyList[$key]);
                    $proxyList = array_values($proxyList);
                    if (count($proxyList) > 0) {
                        if (!filter_var(get_option('delete_unused_proxy'), FILTER_VALIDATE_BOOLEAN)) $proxyList[] = $brokenProxy;
                        $proxyList = implode("\n", $proxyList);
                        set_option('proxy_list', $proxyList);
                        return $this->get_sources();
                    } else {
                        set_option('proxy_list', []);
                    }
                }
            } else {
                error_log("viu get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
