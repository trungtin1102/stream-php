<?php
class filesfm
{
    public $name = 'Files.fm';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = '';
    private $cookies = [];
    private $ch;

    function __construct($url = '')
    {
        session_write_close();

        $this->id = $url;
        $this->url = $url;
        $this->host = parse_url($url, PHP_URL_HOST);
        $this->scheme = parse_url($url, PHP_URL_SCHEME);
        $this->referer = 'https://' . $this->host . '/';

        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $this->scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($this->host);
        $resolveHost = implode(':', array($this->host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'host: ' . $this->host,
            'origin: ' . $this->scheme . '://' . $this->host,
            'referer: ' . $this->scheme . '://' . $this->host . '/',
        ));
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
    }

    private function direct_link($url = '')
    {
        session_write_close();
        if (!empty($url)) {
            curl_setopt($this->ch, CURLOPT_URL, $url);
            curl_setopt($this->ch, CURLOPT_HEADER, 1);
            curl_setopt($this->ch, CURLOPT_NOBODY, 1);

            session_write_close();
            curl_exec($this->ch);
            $effectiveURL = curl_getinfo($this->ch, CURLINFO_EFFECTIVE_URL);
            $redirectURL = curl_getinfo($this->ch, CURLINFO_REDIRECT_URL);
            $status = curl_getinfo($this->ch, CURLINFO_RESPONSE_CODE);
            $err = curl_error($this->ch);
            if ($status >= 200 && $status < 400) {
                if (!empty($effectiveURL) && $effectiveURL !== $url) {
                    return $effectiveURL;
                } elseif (!empty($redirectURL) && $redirectURL !== $url) {
                    return $redirectURL;
                } else {
                    return $url;
                }
            } else {
                error_log("filesfm direct_link $url => $status: $err");
            }
        }
        return $url;
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            session_write_close();
            curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $head) {
                if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $head, $cookie) && !empty($cookie[1])) {
                    $this->cookies[] = $cookie[1];
                }
                return strlen($head);
            });
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);
            if ($status >= 200 && $status < 400) {
                $file_view_hash = get_string_between($response, 'file_view_hash":"', '"');
                if (!empty($file_view_hash)) {
                    $video = $this->direct_link('https://files.fm/down.php?i=' . $file_view_hash);
                    if ($video) {
                        $this->status = 'ok';
                        $this->title = trim(get_string_between($response, 'file_name":"', '"'));
                        $this->image = trim(get_string_between($response, 'og:image:secure_url" content="', '"'));
                        $result[] = [
                            'file' => $video,
                            'type' => 'video/mp4',
                            'label' => 'Original'
                        ];
                        return $result;
                    }
                } else {
                    error_log("filefm get_sources {$this->id} => file_view_hash not found");
                }
            } else {
                error_log("filefm get_sources {$this->id} => $status => $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
