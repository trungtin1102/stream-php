<?php
class supervideo
{
    public $name = 'SuperVideo';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://supervideo.tv/';
    private $cookies = [];
    private $ch;
    private $tracks = [];

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = $this->url;
        $this->id = strtr($id, ['/e/' => '/']);
        $this->url .= $this->id;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'accept: */*',
            'host: supervideo.tv',
            'origin: https://supervideo.tv',
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    function get_sources(bool $mp4 = false)
    {
        session_write_close();
        if (!empty($this->id)) {
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                $scripts = $dom->find('script');
                if (!empty($scripts)) {
                    $eval = '';
                    foreach ($scripts as $script) {
                        if (strpos($script->innertext, 'eval(function(p,a,c,k,e,d)') !== FALSE) {
                            $eval = $script->innertext;
                            break;
                        }
                    }
                    if (!empty($eval)) {
                        $unpacker = new \JavascriptUnpacker\JavascriptUnpacker();
                        $decode = $unpacker->unpack($eval);
                        list($json, $trash) = array_pad(explode(');', strtr($decode, ['jwplayer("vplayer").setup(' => ''])), 2, '');
                        if (!empty($json)) {
                            $json = \OviDigital\JsObjectToJson\JsConverter::convertToJson($json);
                            $json = @json_decode($json, true);
                            if (json_last_error() === JSON_ERROR_NONE && !empty($json['sources'])) {
                                $this->status = 'ok';
                                $this->title = $dom->find('meta[name="keywords"]', 0)->content;
                                $this->image = $json['image'];
                                foreach ($json['tracks'] as $dt) {
                                    if (!isset($dt['kind']) || $dt['kind'] !== 'thumbnails') {
                                        $this->tracks[] = [
                                            'file' => $dt['file'],
                                            'label' => $dt['label']
                                        ];
                                    }
                                }
                                $result = [];
                                if ($mp4) {
                                    foreach ($json['sources'] as $dt) {
                                        if (isset($dt['label'])) {
                                            $result[] = [
                                                'file' => $dt['file'],
                                                'type' => 'video/mp4',
                                                'label' => $dt['label']
                                            ];
                                        }
                                    }
                                } else {
                                    foreach ($json['sources'] as $dt) {
                                        if (!isset($dt['type']) && !isset($dt['label'])) {
                                            $result[] = [
                                                'file' => $dt['file'],
                                                'type' => 'hls',
                                                'label' => 'Original'
                                            ];
                                        }
                                    }
                                }
                                return $result;
                            } else {
                                error_log("supervideo get_sources {$this->id} => sources not found => " . json_last_error_msg());
                            }
                        } else {
                            error_log("supervideo get_sources {$this->id} => json not found");
                        }
                    } else {
                        error_log("supervideo get_sources {$this->id} => eval not found");
                    }
                } else {
                    error_log("supervideo get_sources {$this->id} => script not found");
                }
            } else {
                error_log("supervideo get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_tracks()
    {
        session_write_close();
        return $this->tracks;
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
