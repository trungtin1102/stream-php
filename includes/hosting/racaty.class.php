<?php
class racaty
{
    public $name = 'Racaty';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://racaty.net/';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = $this->url;
        $this->id = $id;
        $this->url .= $this->id . '.html';

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'host: racaty.net',
            'origin: https://racaty.net',
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    private function embedSources()
    {
        session_write_close();
        curl_setopt($this->ch, CURLOPT_URL, 'https://racaty.net/embed-' . $this->id . '.html');
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        $response = curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);
        
        if ($status >= 200 && $status < 400) {
            $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
            $vjs = $dom->find('#vjsplayer');
            if ($vjs) {
                $this->status = 'ok';
                $this->image = $vjs[0]->poster;

                $result[] = [
                    'file' => $vjs[0]->find('source', 0)->src,
                    'type' => 'video/mp4',
                    'label' => 'Original'
                ];
                return $result;
            } else {
                $player = $dom->find('#player_code', 0);
                if (!empty($player)) {
                    $eval = $player->find('script', 0)->innertext;
                    if (strpos($eval, 'eval(function(p,a,c,k,e,d)') !== FALSE) {
                        $unpacker = new \JavascriptUnpacker\JavascriptUnpacker();
                        $decode = $unpacker->unpack($eval);

                        $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($decode);
                        $embed = $dom->find('#np_vid', 0);
                        if (!empty($embed)) {
                            $this->status = 'ok';
                            $this->image = $dom->find('param[name="previewImage"]', 0)->value;

                            $result[] = [
                                'file' => $embed->src,
                                'type' => 'video/mp4',
                                'label' => 'Original'
                            ];
                            return $result;
                        } else {
                            error_log("racaty embedSources {$this->id} => np_vid not found");
                        }
                    } else {
                        error_log("racaty embedSources {$this->id} => eval not found");
                    }
                } else {
                    error_log("racaty embedSources {$this->id} => player_code not found");
                }
            }
        } else {
            error_log("racaty embedSources {$this->id} => $status: $err");
        }
        return [];
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            $data = $this->embedSources();
            if (!empty($data)) {
                return $data;
            } else {
                session_write_close();
                curl_setopt($this->ch, CURLOPT_URL, $this->url);
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($this->ch, CURLOPT_POSTFIELDS, 'op=download2&id=' . $this->id . '&rand=&referer=&method_free=&method_premium=');
                $response = curl_exec($this->ch);
                $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                $err = curl_error($this->ch);

                if ($status >= 200 && $status < 400) {
                    $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                    $dl = $dom->find('#uniqueExpirylink');
                    if ($dl) {
                        $this->status = 'ok';
                        $this->title = trim($dom->find('.name', 0)->find('strong', 0)->plaintext);

                        $result[] = [
                            'file' => $dl[0]->href,
                            'type' => 'video/mp4',
                            'label' => 'Original'
                        ];
                        return $result;
                    } else {
                        error_log("racaty get_sources {$this->id} => uniqueExpirylink not found");
                    }
                } else {
                    error_log("racaty get_sources {$this->id} => $status: $err");
                }
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
