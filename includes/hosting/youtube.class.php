<?php
class youtube
{
    public $name = 'Youtube';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://youtubei.googleapis.com/youtubei/v1/player?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8';
    private $cookies = [];
    private $ch;
    private $proxy = false;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->id = $id;

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
    }

    private function private_sources()
    {
        session_write_close();
        $url = 'https://yt1s.com';
        $scheme = parse_url($url, PHP_URL_SCHEME);
        $host = parse_url($url, PHP_URL_HOST);
        $port = parse_URL($url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        $headers = array(
            'cache-control: no-cache',
            'pragma: no-cache',
            'content-type: application/x-www-form-urlencoded; charset=UTF-8',
            'host: yt1s.com',
            'origin: ' . $url,
            'referer: ' . $url . '/id13',
            'x-requested-with: XMLHttpRequest'
        );

        curl_setopt($this->ch, CURLOPT_URL, $url . '/api/ajaxSearch/index');
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, http_build_query(array(
            'q' => 'https://www.youtube.com/watch?v=' . $this->id,
            'vt' => 'home'
        )));
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
        // cek penggunaan proxy
        $this->proxy = proxy_rotator();
        if ($this->proxy) {
            curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy['proxy']);
            curl_setopt($this->ch, CURLOPT_PROXYTYPE, $this->proxy['type']);
            curl_setopt($this->ch, CURLOPT_PROXYUSERPWD, $this->proxy['usrpwd']);
        }
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $head) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $head, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($head);
        });

        session_write_close();
        $response = curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);

        if ($status >= 200 && $status < 400) {
            $data = json_decode($response, true);
            if ($data['status'] === 'ok' && !empty($data['links']['mp4']) && !empty($data['title'])) {
                $this->status = 'ok';
                $this->image = 'https://i.ytimg.com/vi/' . $this->id . '/maxresdefault.jpg';
                $this->referer = $url . '/id15';
                $this->title = $data['title'];

                $vid = $data['vid'];
                $k = array_values($data['links']['mp4']);
                $k = array_column($k, 'k');

                $mh = curl_multi_init();
                $ch = [];
                foreach ($k as $i => $key) {
                    $ch[$i] = curl_init($url . '/api/ajaxConvert/convert');
                    curl_setopt($ch[$i], CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch[$i], CURLOPT_SSL_VERIFYPEER, 0);
                    curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch[$i], CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt($ch[$i], CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($ch[$i], CURLOPT_POSTFIELDS, http_build_query(array(
                        'vid' => $vid,
                        'k' => $key
                    )));
                    curl_setopt($ch[$i], CURLOPT_USERAGENT, USER_AGENT);
                    curl_setopt($ch[$i], CURLOPT_HTTPHEADER, $headers);
                    curl_multi_add_handle($mh, $ch[$i]);
                    // cek penggunaan proxy
                    if ($this->proxy) {
                        curl_setopt($ch[$i], CURLOPT_PROXY, $this->proxy['proxy']);
                        curl_setopt($ch[$i], CURLOPT_PROXYTYPE, $this->proxy['type']);
                        curl_setopt($ch[$i], CURLOPT_PROXYUSERPWD, $this->proxy['usrpwd']);
                    }
                }

                $active = null;
                do {
                    $mrc = curl_multi_exec($mh, $active);
                } while ($mrc == CURLM_CALL_MULTI_PERFORM);

                while ($active && $mrc == CURLM_OK) {
                    if (curl_multi_select($mh) == -1) {
                        usleep(10);
                    }
                    do {
                        $mrc = curl_multi_exec($mh, $active);
                    } while ($mrc == CURLM_CALL_MULTI_PERFORM);
                }

                $result = [];
                foreach ($k as $i => $key) {
                    $response = curl_multi_getcontent($ch[$i]);
                    $status = curl_getinfo($ch[$i], CURLINFO_HTTP_CODE);
                    $err = curl_error($ch[$i]);
                    if ($status >= 200 && $status < 400) {
                        $arr = json_decode($response, true);
                        if (!empty($arr['dlink']) && strpos($arr['dlink'], 'googlevideo.com') !== FALSE && $arr['fquality'] !== 'auto') {
                            $result[] = [
                                'file' => $arr['dlink'],
                                'type' => 'video/mp4',
                                'label' => strtr($arr['fquality'] . 'p', ['pp' => 'p'])
                            ];
                        } else {
                            error_log("youtube private_sources {$this->id} => dlink, googlevideo.com, fquality not found");
                        }
                    } else {
                        error_log("youtube private_sources {$this->id} => $status: $err");
                    }
                    curl_multi_remove_handle($mh, $ch[$i]);
                }
                curl_multi_close($mh);
                return $result;
            }
        } elseif ($this->proxy) {
            error_log("youtube get_sources {$this->id} => proxy {$this->proxy['format']} doesn't work => $status: $err");
            $proxyList = proxy_list();
            $key = array_search($this->proxy['format'], $proxyList);
            if ($key) {
                $brokenProxy = $proxyList[$key];
                unset($proxyList[$key]);
                $proxyList = array_values($proxyList);
                if (count($proxyList) > 0) {
                    if (!filter_var(get_option('delete_unused_proxy'), FILTER_VALIDATE_BOOLEAN)) $proxyList[] = $brokenProxy;
                    $proxyList = implode("\n", $proxyList);
                    set_option('proxy_list', $proxyList);
                    return $this->private_sources();
                } else {
                    set_option('proxy_list', []);
                }
            }
        } else {
            error_log("youtube get_sources {$this->id} => $status: $err");
        }
        return [];
    }

    private function party_api_sources(bool $live = false)
    {
        session_write_close();

        $url = 'https://yt2html5.com/';
        $scheme = parse_url($url, PHP_URL_SCHEME);
        $host = parse_url($url, PHP_URL_HOST);
        $port = parse_URL($url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        curl_setopt($this->ch, CURLOPT_URL, $url . '?id=' . $this->id);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'accept: */*',
            'host: yt2html5.com',
            'origin: https://yt2html5.com',
            'referer: https://yt2html5.com/',
            'connection: keep-alive',
            'cache-control: no-cache',
            'pragma: no-cache'
        ));
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $head) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $head, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($head);
        });

        $response = curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);

        if ($status >= 200 && $status < 400) {
            $data = json_decode($response, true);
            if ($data['success']) {
                $data = $data['data'];
                if (isset($data['player_response']['playabilityStatus']['status']) && $data['player_response']['playabilityStatus']['status'] === 'OK' && !empty($data['formats'])) {
                    $this->status = 'ok';
                    $this->title = !empty($data['videoDetails']['title']) ? $data['videoDetails']['title'] : '';
                    $this->image = 'https://i.ytimg.com/vi/' . $this->id . '/maxresdefault.jpg';
                    $this->private = false;

                    if ($live) {
                        $this->referer = 'https://www.youtube.com/';
                        $result[] = [
                            'file' => $data['player_response']['streamingData']['hlsManifestUrl'],
                            'type' => 'mpd',
                            'label' => 'Original'
                        ];
                        return $result;
                    } else {
                        if (!empty($data['formats'])) {
                            $result = [];
                            foreach ($data['formats'] as $vid) {
                                if (!empty($vid['url']) && !empty($vid['qualityLabel']) && filter_var($vid['hasAudio'], FILTER_VALIDATE_BOOLEAN) && (strpos($vid['mimeType'], 'video/') !== FALSE || strpos($vid['mimeType'], 'audio/') !== FALSE) && ($vid['hasAudio'] || $vid['hasVideo']) && $vid['qualityLabel'] !== 'auto') {
                                    $dt = [];
                                    $dt['file'] = $vid['url'];
                                    $dt['label'] = $vid['qualityLabel'];
                                    $dt['type'] = $vid['mimeType'];
                                    $result[] = $dt;
                                }
                            }
                            return $result;
                        }
                    }
                } else {
                    error_log("youtube party_api_sources {$this->id} => playabilityStatus status != ok");
                }
            }
        } else {
            error_log("youtube party_api_sources {$this->id} => $status: $err");
        }
        return [];
    }

    private function direct_api_sources()
    {
        session_write_close();

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, '{
            "context": {
                "client": {
                    "hl": "en",
                    "clientName": "WEB",
                    "clientVersion": "2.20210721.00.00",
                    "mainAppWebInfo": {
                        "graftUrl": "/watch?v=' . $this->id . '"
                    }
                }
            },
            "videoId": "' . $this->id . '"
        }');
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'accept: */*',
            'host: youtubei.googleapis.com',
            'origin: https://youtubei.googleapis.com',
            'referer: https://youtube.googleapis.com/',
            'connection: keep-alive',
            'cache-control: no-cache',
            'pragma: no-cache',
            'Content-Type: application/json',
            'cookie: VISITOR_INFO1_LIVE=5ugaFaZeQzo;YSC=ateYRZxctRo'
        ));
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $head) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $head, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($head);
        });

        $response = curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);

        if ($status >= 200 && $status < 400) {
            $data = json_decode($response, true);
            if (isset($data['playabilityStatus']['status']) && $data['playabilityStatus']['status'] === 'OK' && !empty($data['streamingData'])) {
                $this->status = 'ok';
                $this->title = !empty($data['videoDetails']['title']) ? $data['videoDetails']['title'] : '';
                $this->image = 'https://i.ytimg.com/vi/' . $this->id . '/maxresdefault.jpg';
                $this->referer = 'https://youtube.googleapis.com/';
                $this->private = false;

                if (!empty($data['streamingData']['formats'])) {
                    $result = [];
                    foreach ($data['streamingData']['formats'] as $vid) {
                        if (!empty($vid['url']) && strpos($vid['mimeType'], 'video/mp4') !== FALSE) {
                            $dt = [];
                            $dt['file'] = $vid['url'];
                            $dt['label'] = $this->label($vid['itag']);
                            $dt['type'] = 'video/mp4';
                            $result[] = $dt;
                        }
                    }
                    return $result;
                }
            } else {
                error_log("youtube direct_api_sources {$this->id} => playabilityStatus status != ok");
            }
        } else {
            error_log("youtube direct_api_sources {$this->id} => $status: $err");
        }
        return [];
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            $sources = $this->direct_api_sources();
            if (!empty($sources)) {
                return $sources;
            } else {
                $sources = $this->party_api_sources();
                if (!empty($sources)) {
                    return $sources;
                } else {
                    return $this->private_sources();
                }
            }
        }
        return [];
    }
    
    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    private function label($itag)
    {
        session_write_close();
        switch ($itag) {
            case '18':
                $label = "360p";
                break;
            case '59':
                $label = "480p";
                break;
            case '22':
                $label = "720p";
                break;
            case '37':
                $label = "1080p";
                break;
            case '5':
                $label = "240p";
                break;
            case '17':
                $label = "144p";
                break;
            case '34':
                $label = "360p";
                break;
            case '35':
                $label = "480p";
                break;
            case '36':
                $label = "240p";
                break;
            case '38':
                $label = "Original";
                break;
            case '43':
                $label = "360p";
                break;
            case '44':
                $label = "480p";
                break;
            case '45':
                $label = "720p";
                break;
            case '46':
                $label = "1080p";
                break;
            case '82':
                $label = "360p";
                break;
            case '84':
                $label = "720p";
                break;
            case '102':
                $label = "360p";
                break;
            case '104':
                $label = "720p";
                break;
            case '132':
                $label = "144p";
                break;
            case '133':
                $label = "240p";
                break;
            case '134':
                $label = "360p";
                break;
            case '135':
                $label = "480p";
                break;
            case '136':
                $label = "720p";
                break;
            case '137':
                $label = "1080p";
                break;
            default:
                $label = "Unknown";
                break;
        }
        return $label;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
