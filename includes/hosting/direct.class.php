<?php
class direct
{
    public $name = 'Direct';
    private $id = '';
    private $title = '';
    private $image = '';
    private $status = 'fail';
    private $url = '';
    private $host = '';
    private $origin = '';
    private $referer = '';
    private $scheme = '';
    private $cookies = [];
    private $ch;
    private $proxy = false;
    private $follow = true;

    function __construct($url)
    {
        session_write_close();

        $this->id = $url;
        $this->url = $url;
        $this->host = parse_url($url, PHP_URL_HOST);
        $this->scheme = parse_url($url, PHP_URL_SCHEME);

        $port = parse_URL($url, PHP_URL_PORT);
        if (empty($port)) $port = $this->scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($this->host);
        $resolveHost = implode(':', array($this->host, $port, $ipv4));

        if (strpos($this->host, 'tvmalaysia') !== FALSE) {
            $this->origin = 'https://www.tvmalaysia.online';
            $this->referer = 'https://www.tvmalaysia.online/';
        } elseif (strpos($this->host, 'dens.tv') !== FALSE) {
            $this->origin = 'http://www.dens.tv';
            $this->referer = 'http://www.dens.tv/';
        } elseif (strpos($this->host, '/o-tvri/') !== FALSE) {
            $this->origin = 'http://klik.tvri.go.id';
            $this->referer = 'http://klik.tvri.go.id/';
        } elseif (strpos($this->host, 'useetv.com') !== FALSE) {
            $this->origin = 'https://www.useetv.com';
            $this->referer = 'https://www.useetv.com/';
        } elseif (strpos($url, 'cloudfront.net/RCTI') !== FALSE || strpos($url, 'cloudfront.net/MNCTV') !== FALSE || strpos($url, 'cloudfront.net/GTV') !== FALSE || strpos($url, 'cloudfront.net/INEWS') !== FALSE) {
            $this->origin = 'https://www.rctiplus.com';
            $this->referer = 'https://www.rctiplus.com/';
        } elseif (strpos($this->host, 'rctiplus.id') !== FALSE) {
            $this->origin = 'https://www.rctiplus.com';
            $this->referer = 'https://www.rctiplus.com/';
        } elseif (strpos($url, 'googlevideo.com') !== FALSE || strpos($url, 'googleusercontent.com') !== FALSE) {
            $this->origin = 'https://youtube.googleapis.com';
            $this->referer = 'https://youtube.googleapis.com/';
        } elseif (strpos($url, 'vidio-com.akamaized.net') !== FALSE || strpos($url, 'vidio.com') !== FALSE) {
            $this->origin = 'https://www.vidio.com';
            $this->referer = 'https://www.vidio.com/';
        } elseif (strpos($this->host, 'ttvnw.net') !== FALSE) {
            $this->origin = 'https://www.twitch.tv';
            $this->referer = 'https://www.twitch.tv/';
        } else {
            $this->origin = $this->scheme . '://' . $this->host;
            $this->referer = $this->url;
        }

        if (strpos($this->url, 'iptvtree') !== false || strpos($this->url, 'skylogic') !== false || strpos($this->url, 'mvtvtech') !== false) {
            $this->follow = false;
        } else {
            $this->follow = true;
        }

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, $this->follow);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        if (!filter_var($this->host, FILTER_VALIDATE_IP)) {
            curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        }
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'accept: */*',
            'accept-encoding: gzip, deflate, br',
            'accept-language: id,en;q=0.9,id-ID;q=0.8',
            'origin: ' . $this->origin,
            'referer: ' . $this->referer,
            'user-agent: ' . USER_AGENT
        ));
        curl_setopt($this->ch, CURLOPT_HEADER, true);
        curl_setopt($this->ch, CURLOPT_NOBODY, true);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->url)) {
            session_write_close();
            $response = curl_exec($this->ch);
            $effectiveURL = curl_getinfo($this->ch, CURLINFO_EFFECTIVE_URL);
            $redirectURL = curl_getinfo($this->ch, CURLINFO_REDIRECT_URL);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $type = curl_getinfo($this->ch, CURLINFO_CONTENT_TYPE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $this->status = 'ok';
                if ($this->follow) {
                    if (!empty($effectiveURL) && $this->url !== $effectiveURL) {
                        $url = $effectiveURL;
                    } elseif (!empty($redirectURL) && $this->url !== $redirectURL) {
                        $url = $redirectURL;
                    } else {
                        $url = $this->url;
                    }
                } else {
                    $url = $this->url;
                }

                $title = rawurldecode($url);
                $title = parse_url($title, PHP_URL_PATH);
                $title = basename($title);
                $this->title = !empty($title) ? $title : '';

                $type = strtolower($type);
                $response = strtolower($response);

                if (
                    strpos($type, 'dash') !== FALSE ||
                    strpos($response, '<?xml') !== FALSE ||
                    strpos($response, '<mpd') !== FALSE ||
                    strpos($url, '.mpd') !== FALSE
                ) {
                    $type = 'mpd';
                } elseif (
                    strpos($type, 'mp2t') !== FALSE ||
                    strpos($type, 'mpegurl') !== FALSE ||
                    strpos($type, 'vnd') !== FALSE ||
                    strpos($response, '#ext') !== FALSE ||
                    strpos($url, '.ts') !== FALSE ||
                    strpos($url, '.m3u') !== FALSE
                ) {
                    $type = 'hls';
                } elseif (
                    (preg_match('/audio|mp3|aac/', $type, $mimeType) && !empty($mimeType)) ||
                    (preg_match('/mp3|aac/', $url, $mimeType) && !empty($mimeType))
                ) {
                    $type = 'audio/mp3';
                } else {
                    $type = 'video/mp4';
                }

                $result[] = [
                    'file' => $url,
                    'type' => $type,
                    'label' => 'Original'
                ];
                return $result;
            } else {
                error_log("direct get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
