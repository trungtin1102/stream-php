<?php
ini_set('max_execution_time', 0);
set_time_limit(0);

class uptobox
{
    public $name = 'Uptobox';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $token = '';
    private $waitingToken = '';
    private $url = 'https://uptobox.com/';
    private $cookies = [];
    private $ch;
    private $iCache;

    function __construct($id = '')
    {
        session_write_close();

        $this->iCache = new \InstanceCache();
        
        $this->referer = $this->url;
        $this->id = $id;
        $this->token = get_option('uptobox_api');
        $this->cookies[] = 'video=' . $this->id;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
    }

    private function getDownloadLink()
    {
        session_write_close();

        $url = $this->url . 'api/link?token=' . $this->token . '&file_code=' . $this->id . '&waitingToken=' . $this->waitingToken;
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'accept: */*',
            'cache-control: no-cache',
            'pragma: no-cache',
            'connection: keep-alive',
            'host: uptobox.com',
            'origin: https://uptobox.com',
            'referer: https://uptobox.com/',
        ));

        session_write_close();
        $response = curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);
        if ($status >= 200 && $status < 400) {
            $arr = json_decode($response, true);
            if ($arr['statusCode'] === 0) {
                $this->iCache->setKey('uptobox_watingToken~' . $this->id);
                $this->iCache->delete();

                $this->status = 'ok';
                $this->title = basename($arr['data']['dlLink']);

                $result[] = [
                    'file' => $arr['data']['dlLink'],
                    'type' => 'video/mp4',
                    'label' => 'Original'
                ];
                return $result;
            } else {
                error_log("uptobox getDownloadLink {$url} => $response");
            }
        } else {
            error_log("uptobox getDownloadLink {$url} => $status: $err");
        }
        return [];
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            $this->iCache->setKey('uptobox_watingToken~' . $this->id);
            $cache = $this->iCache->get();
            if ($cache) {
                $this->waitingToken = $cache;
                return $this->getDownloadLink();
            } else {
                $url = $this->url . 'api/link?token=' . $this->token . '&file_code=' . $this->id;
                curl_setopt($this->ch, CURLOPT_URL, $url);
                curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                    'accept: */*',
                    'cache-control: no-cache',
                    'pragma: no-cache',
                    'connection: keep-alive',
                    'host: uptobox.com',
                    'origin: https://uptobox.com',
                    'referer: https://uptobox.com/',
                ));
                curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $head) {
                    if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $head, $cookie) && !empty($cookie[1])) {
                        $this->cookies[] = trim($cookie[1]);
                    }
                    return strlen($head);
                });

                session_write_close();
                $response = curl_exec($this->ch);
                $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                $err = curl_error($this->ch);
                if ($status >= 200 && $status < 400) {
                    $arr = json_decode($response, true);
                    if ($arr['statusCode'] === 0) {
                        $this->status = 'ok';
                        $this->title = basename($arr['data']['dlLink']);
                        $result[] = [
                            'file' => $arr['data']['dlLink'],
                            'type' => 'video/mp4',
                            'label' => 'Original'
                        ];
                        return $result;
                    } elseif ($arr['statusCode'] === 16) {
                        if (!empty($arr['data']['waitingToken'])) {
                            $this->waitingToken = $arr['data']['waitingToken'];
                            $this->iCache->save($this->waitingToken, intval($arr['data']['waiting']) + 10, 'uptobox_watingToken');
                            sleep($arr['data']['waiting']);
                            return $this->getDownloadLink();
                        } else {
                            error_log("uptobox get_sources {$this->id} => waitingToken not found");
                        }
                    } else {
                        error_log("uptobox get_sources {$this->id} => $response");
                    }
                } else {
                    error_log("uptobox get_sources {$this->id} => $status: $err");
                }
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_token()
    {
        session_write_close();
        return $this->token;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
