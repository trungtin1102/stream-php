<?php
class tiktok
{
    public $name = 'Tiktok';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://www.tiktok.com/';
    private $cookies = [];
    private $ch;
    private $proxy = false;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = $this->url;
        $this->id = $id;
        $this->url .= $this->id;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $head) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $head, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($head);
        });
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            $this->proxy = proxy_rotator();
            if ($this->proxy) {
                curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy['proxy']);
                curl_setopt($this->ch, CURLOPT_PROXYTYPE, $this->proxy['type']);
                curl_setopt($this->ch, CURLOPT_PROXYUSERPWD, $this->proxy['usrpwd']);
            }
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                $data = $dom->find('script#SIGI_STATE', 0);
                if (!empty($data)) {
                    $data = strtr($data->innertext, ["window['SIGI_STATE']=" => '']);
                    list($data, $trash) = array_pad(explode(';window', $data), 2, '');
                    $data = @json_decode($data, true);
                    if (JSON_ERROR_NONE === json_last_error()) {
                        $data = array_values($data['ItemModule']);
                        if (!empty($data[0]['video']['downloadAddr'])) {
                            $this->status = 'ok';
                            $image = $dom->find('meta[property="og:image"]', 0);
                            $image = !empty($image) ? $image : $dom->find('meta[property="og:image"]', 0);
                            $this->image = isset($image->description) ? htmlspecialchars_decode($image->description) : htmlspecialchars_decode($image->content);

                            $title = $dom->find('meta[property="og:title"]', 0);
                            $title = !empty($title) ? $title : $dom->find('meta[property="og:title"]', 0);
                            $this->title = isset($title->description) ? $title->description : $title->content;

                            $result[] = [
                                'file' => $data[0]['video']['downloadAddr'],
                                'type' => 'video/mp4',
                                'label' => $data[0]['video']['ratio']
                            ];
                            return $result;
                        } else {
                            error_log("tiktok get-sources {$this->id} => video downloadAddr not found");
                        }
                    } else {
                        error_log("tiktok get-sources {$this->id} => " . json_last_error_msg());
                    }
                } else {
                    error_log("tiktok get-sources {$this->id} => script#SIGI_STATE not found");
                }
            } elseif($this->proxy) {
                error_log("tiktok get_sources {$this->id} => proxy {$this->proxy['format']} doesn't work => $status: $err");
                $proxyList = proxy_list();
                $key = array_search($this->proxy['format'], $proxyList);
                if ($key) {
                    $brokenProxy = $proxyList[$key];
                    unset($proxyList[$key]);
                    $proxyList = array_values($proxyList);
                    if (count($proxyList) > 0) {
                        if (!filter_var(get_option('delete_unused_proxy'), FILTER_VALIDATE_BOOLEAN)) $proxyList[] = $brokenProxy;
                        $proxyList = implode("\n", $proxyList);
                        set_option('proxy_list', $proxyList);
                        return $this->get_sources();
                    } else {
                        set_option('proxy_list', []);
                    }
                }
            } else {
                error_log("tiktok get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
