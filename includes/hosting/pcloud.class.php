<?php
class pcloud
{
    public $name = 'pCloud';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = '';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();

        $this->referer = $this->url;
        if (validate_url($id)) {
            $this->id = $id;
            $this->url = $id;
        } else {
            $id = explode('?', $id);
            $this->id = $id[0];
            $this->url = 'https://u.pcloud.link/publink/show?code=' . $this->id;
        }

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'host: ' . $host,
            'origin: https://' . $host,
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            session_write_close();
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);
            if ($status >= 200 && $status < 400) {
                $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                $scripts = $dom->find('script');
                if (!empty($scripts)) {
                    $publinkData = '';
                    foreach ($scripts as $sc) {
                        if (strpos($sc->innertext, 'publinkData') !== FALSE) {
                            $publinkData = $sc->innertext;
                            break;
                        }
                    }
                    if (!empty($publinkData)) {
                        $start = explode('=', $publinkData, 2);
                        $end = explode(';', $start[1]);
                        $arr = @json_decode($end[0], true);
                        if (strpos($arr['metadata']['contenttype'], 'video') !== FALSE) {
                            $this->title = $arr['metadata']['name'];
                            $this->image = $arr['thumb'];
                            if (!empty($arr['variants'])) {
                                $this->status = 'ok';

                                $result = [];
                                foreach ($arr['variants'] as $dt) {
                                    $result[] = [
                                        'file' => 'https://' . $dt['hosts'][0] . strtr($dt['path'], ['\/' => '/']),
                                        'type' => 'video/mp4',
                                        'label' => $dt['height'] . 'p'
                                    ];
                                }
                                return $result;
                            } elseif (!empty($arr['downloadlink'])) {
                                $this->status = 'ok';

                                $result = [];
                                $result[] = [
                                    'file' => strtr($arr['downloadlink'], ['\/' => '/']),
                                    'type' => 'video/mp4',
                                    'label' => 'Original'
                                ];
                                return $result;
                            } else {
                                error_log("pcloud get_sources {$this->id} => variants and downloadlink not found");
                            }
                        } else {
                            error_log("pcloud get_sources {$this->id} => metadata video not found");
                        }
                    } else {
                        error_log("pcloud get_sources {$this->id} => publinkData not found");
                    }
                } else {
                    error_log("pcloud get_sources {$this->id} => script not found");
                }
            } else {
                error_log("pcloud get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
