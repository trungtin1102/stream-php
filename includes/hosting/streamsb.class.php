<?php
class streamsb
{
    public $name = 'StreamSB';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $host = '';
    private $url = 'https://streamsb.net/';
    private $tracks = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();

        $this->referer = $this->url;
        $this->id = strtr($id, ['embed-' => '', '.html' => '', '/view/' => '', '/play/' => '']);
        $this->url .= 'embed-' . $this->id . '.html';

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $this->host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($this->host);
        $resolveHost = implode(':', array($this->host, $port, $ipv4));

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
    }

    private function title()
    {
        session_write_close();

        $url = strtr($this->url, ['embed-' => '']);
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'host: ' . $this->host,
            'origin: https://streamsb.net',
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);

        $response = curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);

        if ($status >= 200 && $status < 400) {
            return strtr(get_string_between($response, '<title>', '</title>'), ['Watch' => '', 'Download' => '']);
        } else {
            error_log("streamsb title {$url} => $status: $err");
        }
        return '';
    }

    function get_sources()
    {
        session_write_close();
        $logFile = execHeadlessChromium($this->url, 'streamsb~' . keyFilter($this->id), true);
        if ($logFile) {
            open_resources_handler();
            $fp = @fopen($logFile, 'rb');
            if ($fp) {
                stream_set_blocking($fp, false);
                $content = stream_get_contents($fp);
                fclose($fp);

                if ($content) {
                    preg_match_all('/"url":"([^"]+)"/', $content, $urls);
                    preg_match_all('/\"([^"]+)\"/', $content, $urls1);
                    $data = array_filter($urls[1], function ($v) {
                        return strpos($v, '.m3u8') !== FALSE;
                    });
                    $data1 = array_filter($urls1[1], function ($v) {
                        return strpos($v, '.m3u8') !== FALSE;
                    });
                    $data = array_values($data);
                    $data1 = array_values($data1);
                    $data = array_merge($data, $data1);
                    if (!empty($data[0])) {
                        $tracks = array_filter($urls[1], function ($v) {
                            return validate_url($v) && strpos($v, '/srt/') !== FALSE;
                        });
                        foreach ($tracks as $dt) {
                            $x = explode('_', $dt);
                            $x = trim(strtr(end($x), ['.vtt' => '', '.srt' => '', '.ass' => '']));
                            $this->tracks[] = [
                                'file' => trim($dt, '\\/'),
                                'label' => trim($x, '\\/')
                            ];
                        }

                        @unlink($logFile);
                        $this->status = 'ok';
                        $this->title = $this->title();
                        $this->image = 'https://akamai-img-content.com/' . $this->id . '.jpg';
                        $result[] = [
                            'file' => trim($data[0], '\/'),
                            'type' => 'hls',
                            'label' => 'Original'
                        ];
                        return $result;
                    } else {
                        $ua = get_option('chrome_ua');
                        $ua = !empty($ua) && strpos($ua, 'HeadlessChrome') !== false ? $ua : getHeadlessChromeUA();
                        $ua = $ua ? $ua : USER_AGENT;

                        $data = array_filter($urls[1], function ($v) {
                            return strpos($v, 'streamsb.net/sources') !== FALSE;
                        });
                        $data = array_values($data);
                        if (!empty($data[0]) && validate_url($data[0])) {
                            $scheme = parse_url($data[0], PHP_URL_SCHEME);
                            $host = parse_url($data[0], PHP_URL_HOST);
                            $port = parse_URL($data[0], PHP_URL_PORT);
                            if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
                            $ipv4 = gethostbyname($host);
                            $resolveHost = implode(':', array($host, $port, $ipv4));

                            curl_setopt($this->ch, CURLOPT_URL, $data[0]);
                            curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
                            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                                'host: ' . $host,
                                'origin: https://streamsb.net',
                                'watchsb: streamsb',
                                'x-requested-with: XMLHttpRequest'
                            ));
                            curl_setopt($this->ch, CURLOPT_REFERER, 'https://streamsb.net/');
                            curl_setopt($this->ch, CURLOPT_USERAGENT, $ua);

                            $response = curl_exec($this->ch);
                            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                            $err = curl_error($this->ch);

                            if ($status >= 200 && $status < 400) {
                                $data = json_decode($response, true);
                                if (isset($data['stream_data'])) {
                                    @unlink($logFile);
                                    $this->status = 'ok';
                                    $this->title = $data['stream_data']['title'];
                                    $this->referer = 'https://streamsb.net/';
                                    $this->image = 'https://akamai-img-content.com/' . $this->id . '.jpg';
                                    if (!empty($data['stream_data']['subs'])) {
                                        foreach ($data['stream_data']['subs'] as $dt) {
                                            $this->tracks[] = [
                                                'file' => 'https://streamsb.net' . $dt['file'],
                                                'label' => $dt['label']
                                            ];
                                        }
                                    }
                                    $result[] = [
                                        'file' => $data['stream_data']['file'],
                                        'type' => 'hls',
                                        'label' => 'Original'
                                    ];
                                    return $result;
                                } else {
                                    error_log("streamsb get_sources {$this->url} => $response");
                                }
                            } else {
                                error_log("streamsb get_sources {$this->url} => $status: $err");
                            }
                        } else {
                            error_log("streamsb get_sources {$this->url} => sources url not found");
                        }
                    }
                } else {
                    error_log("streamsb get_sources {$this->url} => cannot read $logFile");
                }
            } else {
                error_log("streamsb get_sources {$this->url} => cannot open $logFile");
            }
        } else {
            error_log("streamsb get_sources {$this->url} => cannot create load file");
        }
        return [];
    }

    function get_tracks()
    {
        session_write_close();
        return $this->tracks;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
