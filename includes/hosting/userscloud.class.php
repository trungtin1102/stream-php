<?php
class userscloud
{
    public $name = 'Userscloud';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://userscloud.com/';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = $this->url;
        $this->id = $id;
        $this->url .= $this->id;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'host: userscloud.com',
            'origin: https://userscloud.com',
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);
            if ($status >= 200 && $status < 400) {
                $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                if ($dom) {
                    $video = $dom->find('video', 0);
                    if (!empty($video)) {
                        $this->status = 'ok';
                        $this->image = $video->poster;
                        $this->title = $dom->find('h2', 0)->plaintext;

                        $result[] = [
                            'file' => $video->find('source', 0)->src,
                            'type' => 'video/mp4',
                            'label' => 'Original'
                        ];
                        return $result;
                    } else {
                        $eval = '';
                        foreach ($dom->find('script') as $dt) {
                            if (strpos($dt->innertext, 'eval(function(p,a,c,k,e,d)') !== FALSE) {
                                $eval = $dt->innertext;
                                break;
                            }
                        }
                        if (!empty($eval)) {
                            $class = new \JavascriptUnpacker\JavascriptUnpacker();
                            $script = $class->unpack($eval);
                            $script = strtr($script, ["document.write('" => '', "');" => '']);

                            $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($script);
                            $video = $dom->find('param[name="src"]', 0);
                            if (!empty($video)) {
                                $this->status = 'ok';
                                $this->image = $dom->find('param[name="previewImage"]', 0)->value;

                                $result[] = [
                                    'file' => $video->value,
                                    'type' => 'video/mp4',
                                    'label' => 'Original'
                                ];
                                return $result;
                            } else {
                                error_log("userscloud get_sources {$this->id} => param src not found");
                            }
                        } else {
                            error_log("userscloud get_sources {$this->id} => eval not found");
                        }
                    }
                }
            } else {
                error_log("userscloud get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
