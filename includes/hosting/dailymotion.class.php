<?php
class dailymotion
{
    public $name = 'Dailymotion';
    private $id = '';
    private $title = '';
    private $image = '';
    private $tracks = [];
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://www.dailymotion.com/video/';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();

        $this->referer = 'https://www.dailymotion.com/';

        $this->id = $id;
        $this->url .= $this->id;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'Cache-Control: max-age=0',
            'Connection: keep-alive',
            'host: ' . $host,
            'origin: https://' . $host,
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    function get_sources()
    {
        session_write_close();

        if (!empty($this->id)) {
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $v1st = '';
                $dmTs = '';
                if (!empty($this->cookies)) {
                    foreach ($this->cookies as $dt) {
                        if (strpos($dt, 'v1st=') !== false) $v1st = $dt;
                        elseif (strpos($dt, 'ts=') !== false) $dmTs = $dt;
                    }
                    $url = 'https://www.dailymotion.com/player/metadata/video/' . $this->id . '?embedder=https%3A%2F%2Fwww.dailymotion.com%2Fvideo%2F' . $this->id . '&referer=&app=com.dailymotion.neon&client_type=website&dmTs=' . $dmTs . '&section_type=player&component_style=_&is_native_app=0&dmV1st=' . $v1st;
                    curl_setopt($this->ch, CURLOPT_URL, $url);
                    curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
                    curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                        'Cache-Control: max-age=0',
                        'Connection: keep-alive',
                        'host: www.dailymotion.com',
                        'X-Requested-With: XMLHttpRequest'
                    ));
                    curl_setopt($this->ch, CURLOPT_COOKIE, trim(implode(';', $this->cookies), ';'));
                    curl_setopt($this->ch, CURLOPT_REFERER, 'https://www.dailymotion.com/embed?api=postMessage&apimode=json&app=com.dailymotion.neon&autoplay-mute=true&client_type=website&collections-action=trigger_event&collections-enable=fullscreen_only&endscreen-enable=false&like-action=trigger_event&like-enable=fullscreen_only&queue-enable=false&sharing-action=trigger_event&sharing-enable=fullscreen_only&ui-logo=false&watchlater-action=trigger_event&watchlater-enable=fullscreen_only&dmTs=' . $dmTs);

                    session_write_close();
                    $response = curl_exec($this->ch);
                    $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);

                    if ($status >= 200 && $status < 400) {
                        $arr = @json_decode($response, true);
                        if (!empty($arr['qualities']['auto'])) {
                            $this->status = 'ok';
                            $this->title = $arr['title'];

                            $kp = array_keys($arr['posters']);
                            $kp = end($kp);
                            $this->image = $arr['posters'][$kp];

                            if (!empty($arr['subtitles']['data'])) {
                                foreach ($arr['subtitles']['data'] as $iso => $sub) {
                                    $this->tracks[] = [
                                        'file' => $sub['urls'][0],
                                        'label' => $sub['label'],
                                        'kind' => 'captions',
                                        'srclang' => $iso
                                    ];
                                }
                            }

                            $result = [];
                            $result[] = [
                                'file' => strtr($arr['qualities']['auto'][0]['url'], ['#cell=sg1' => '']),
                                'type' => 'hls',
                                'label' => 'Original'
                            ];
                            return $result;
                        } else {
                            error_log("dailymotion get_sources {$url} => auto qualities not found");
                        }
                    } else {
                        error_log("dailymotion get_sources {$url} => $status: $err");
                    }
                } else {
                    error_log("dailymotion get_sources {$this->id} => cookies is empty");
                }
            } else {
                error_log("dailymotion get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();

        return $this->cookies;
    }

    function get_tracks()
    {
        session_write_close();

        return $this->tracks;
    }

    function get_status()
    {
        session_write_close();

        return $this->status;
    }

    function get_title()
    {
        session_write_close();

        return $this->title;
    }

    function get_image()
    {
        session_write_close();

        return $this->image;
    }

    function get_referer()
    {
        session_write_close();

        return $this->referer;
    }

    function get_id()
    {
        session_write_close();

        return $this->id;
    }

    function __destruct()
    {
        session_write_close();

        curl_close($this->ch);
    }
}
