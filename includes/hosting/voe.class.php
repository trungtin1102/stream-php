<?php
class voe
{
    public $name = 'VOE';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://voe.sx/';
    private $cookies = [];
    private $ch;
    private $tracks = [];

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = $this->url;
        $this->id = strtr($id, ['/e/' => '/']);
        $this->url .= 'e/' . $this->id;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'accept: */*',
            'host: voe.sx',
            'origin: https://voe.sx',
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                $tracks = $dom->find('track');
                if (isset($tracks[0])) {
                    foreach ($tracks as $dt) {
                        $this->tracks[] = array(
                            'file' => 'https://voe.sx' . $dt->src,
                            'label' => $dt->label,
                            'kind' => $dt->kind
                        );
                    }
                }
                list($trash, $response) = array_pad(explode('const sources =', $response), 2, '');
                list($sources, $trash) = array_pad(explode(';', $response), 2, '');
                $json = \OviDigital\JsObjectToJson\JsConverter::convertToJson($sources);
                $json = @json_decode(strtr($json, [',}' => '}']), true);
                if (json_last_error() === JSON_ERROR_NONE) {
                    $this->status = 'ok';
                    $this->title = strtr($dom->find('title', 0)->plaintext, ['Watch' => '']);
                    $posterAttr = 'data-poster';
                    $this->image = $dom->find('#voe-player', 0)->$posterAttr;

                    $result[] = [
                        'file' => $json['hls'],
                        'type' => 'hls',
                        'label' => 'Original'
                    ];
                    return $result;
                } else {
                    error_log("voe get_sources {$this->id} => " . json_last_error_msg());
                }
            } else {
                error_log("voe get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_tracks()
    {
        session_write_close();
        return $this->tracks;
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
