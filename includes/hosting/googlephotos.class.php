<?php
class googlephotos
{
    public $name = 'Google Photos';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = '';
    private $cookies = [];
    private $ch;
    private $proxy = false;

    function __construct($id = '')
    {
        session_write_close();

        $this->referer = 'https://youtube.googleapis.com/';
        $this->id = $id;
        if (strlen($id) > 17) {
            $this->url = 'https://photos.google.com/share/' . $id;
        } else {
            $this->url = 'https://photos.app.goo.gl/' . $id;
        }

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, [
            'Host: ' . $host,
            'Origin: https://' . $host
        ]);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    private function getNewSources(array $oldSources = [])
    {
        session_write_close();
        $mh = curl_multi_init();
        $ch = [];

        foreach ($oldSources as $i => $dt) {
            $host = parse_url($dt['file'], PHP_URL_HOST);
            $ch[$i] = curl_init($dt['file']);
            curl_setopt($ch[$i], CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch[$i], CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch[$i], CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch[$i], CURLOPT_MAXREDIRS, 5);
            curl_setopt($ch[$i], CURLOPT_ENCODING, '');
            curl_setopt($ch[$i], CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch[$i], CURLOPT_DNS_SERVERS, dns_servers());
            curl_setopt($ch[$i], CURLOPT_NOSIGNAL, true);
            curl_setopt($ch[$i], CURLOPT_DNS_CACHE_TIMEOUT, 300);
            curl_setopt($ch[$i], CURLOPT_TCP_KEEPALIVE, true);
            if (defined('CURLOPT_TCP_FASTOPEN')) {
                curl_setopt($ch[$i], CURLOPT_TCP_FASTOPEN, true);
            }
            curl_setopt($ch[$i], CURLOPT_TCP_NODELAY, true);
            curl_setopt($ch[$i], CURLOPT_FORBID_REUSE, true);
            curl_setopt($ch[$i], CURLOPT_FAILONERROR, true);
            curl_setopt($ch[$i], CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch[$i], CURLOPT_TIMEOUT, 30);
            curl_setopt($ch[$i], CURLOPT_HEADER, 1);
            curl_setopt($ch[$i], CURLOPT_NOBODY, 1);
            curl_setopt($ch[$i], CURLOPT_HTTPHEADER, array(
                'accept: */*',
                "cache-control: no-cache",
                'pragma: no-cache',
                'Connection: keep-alive',
                "host: $host",
                "referer: https://youtube.googleapis.com/",
                'range: bytes=0-'
            ));
            curl_setopt($ch[$i], CURLOPT_COOKIE, trim(implode(';', $this->cookies), ';'));
            curl_setopt($ch[$i], CURLOPT_USERAGENT, USER_AGENT);
            curl_multi_add_handle($mh, $ch[$i]);
        }

        $active = null;
        do {
            $mrc = curl_multi_exec($mh, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc == CURLM_OK) {
            if (curl_multi_select($mh) == -1) {
                usleep(10);
            }
            do {
                $mrc = curl_multi_exec($mh, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }

        $result = [];
        foreach ($oldSources as $i => $dt) {
            $status = curl_getinfo($ch[$i], CURLINFO_RESPONSE_CODE);
            $type = curl_getinfo($ch[$i], CURLINFO_CONTENT_TYPE);
            $err = curl_error($ch[$i]);
            if ($status >= 200 && $status < 400) {
                $result[] = [
                    'file' => $dt['file'],
                    'label' => $dt['label'],
                    'type' => $dt['type']
                ];
            } else {
                error_log("googlephotos getNewSources {$dt['file']} => $type => $status: $err");
            }
            curl_multi_remove_handle($mh, $ch[$i]);
        }
        curl_multi_close($mh);
        return $result;
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            $this->proxy = proxy_rotator();
            if ($this->proxy) {
                curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy['proxy']);
                curl_setopt($this->ch, CURLOPT_PROXYTYPE, $this->proxy['type']);
                curl_setopt($this->ch, CURLOPT_PROXYUSERPWD, $this->proxy['usrpwd']);
            }

            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $video = trim(get_string_between($response, 'property="og:video" content="', '"'));
                if (!empty($video)) {
                    $sharedLink = get_string_between($response, './share/', '"');
                    if (!empty($sharedLink)) {
                        $class = new \googlephotos($sharedLink);
                        return $class->get_sources();
                    } else {
                        error_log("googlephotos get_sources {$this->id} => /share/ url not found");
                    }
                } else {
                    $isvideo = trim(get_string_between($response, 'data-isvideo="', '"'));
                    $video = trim(get_string_between($response, 'data-url="', '"'));
                    if ($isvideo === 'true' && !empty($video)) {
                        $this->status = 'ok';
                        $this->image = $video . '=s1024-k-rw-no';

                        $result[] = [
                            'file' => $video . '=m18',
                            'type' => 'video/mp4',
                            'label' => '360p'
                        ];
                        $result[] = [
                            'file' => $video . '=m22',
                            'type' => 'video/mp4',
                            'label' => '720p'
                        ];
                        $result[] = [
                            'file' => $video . '=m37',
                            'type' => 'video/mp4',
                            'label' => '1080p'
                        ];
                        return $this->getNewSources($result);
                    } else {
                        error_log("googlephotos get_sources {$this->id} => property=\"og:video\" & data-isvideo not found");
                    }
                }
            } elseif ($this->proxy) {
                error_log("googlephotos get_sources {$this->id} => proxy {$this->proxy['format']} doesn't work => $status: $err");
                $proxyList = proxy_list();
                $key = array_search($this->proxy['format'], $proxyList);
                if ($key) {
                    $brokenProxy = $proxyList[$key];
                    unset($proxyList[$key]);
                    $proxyList = array_values($proxyList);
                    if (count($proxyList) > 0) {
                        if (!filter_var(get_option('delete_unused_proxy'), FILTER_VALIDATE_BOOLEAN)) $proxyList[] = $brokenProxy;
                        $proxyList = implode("\n", $proxyList);
                        set_option('proxy_list', $proxyList);
                        return $this->get_sources();
                    } else {
                        set_option('proxy_list', []);
                    }
                }
            } else {
                error_log("googlephotos get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
