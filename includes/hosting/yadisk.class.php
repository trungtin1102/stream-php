<?php
class yadisk
{
    public $name = 'Yandex Disk';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://yadi.sk/i/';
    private $cookies = [];
    private $ch;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = 'https://yadi.sk/';
        $this->id = $id;

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
    }

    function get_sources(bool $mp4 = false)
    {
        session_write_close();
        if (!empty($this->id)) {
            if ($mp4) {
                $url = 'https://cloud-api.yandex.net/v1/disk/public/resources/download?public_key=' . urlencode('https://yadi.sk/i/' . $this->id);
                $scheme = parse_url($url, PHP_URL_SCHEME);
                $host = parse_url($url, PHP_URL_HOST);
                $port = parse_URL($url, PHP_URL_PORT);
                if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
                $ipv4 = gethostbyname($host);
                $resolveHost = implode(':', array($host, $port, $ipv4));

                curl_setopt($this->ch, CURLOPT_URL, $url);
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));

                session_write_close();
                $response = curl_exec($this->ch);
                $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                $err = curl_error($this->ch);
                if ($status >= 200 && $status < 400) {
                    $json = json_decode($response, true);
                    if (!empty($json['href'])) {
                        $result[] = [
                            'file' => $json['href'],
                            'type' => 'video/mp4',
                            'label' => 'Original'
                        ];
                        return $result;
                    } else {
                        error_log("yadisk get_sources mp4 {$this->id} => $response");
                    }
                } else {
                    error_log("yadisk get_sources mp4 {$this->id} => $status: $err");
                }
            } else {
                curl_setopt($this->ch, CURLOPT_URL, $this->url . $this->id);
                curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                    'host: yadi.sk',
                    'origin: https://yadi.sk'
                ));
                curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $head) {
                    if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $head, $cookie) && !empty($cookie[1])) {
                        $this->cookies[] = $cookie[1];
                    }
                    return strlen($head);
                });

                session_write_close();
                $response = curl_exec($this->ch);
                $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                $err = curl_error($this->ch);
                if ($status >= 200 && $status < 400) {
                    $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                    $json = $dom->find('script[type="application/json"]', 0);
                    if (!empty($json)) {
                        $json = @json_decode($json->innertext, true);
                        if (!empty($json['rootResourceId'])) {
                            $id = $json['rootResourceId'];
                            $res = $json['resources'][$id];
                            $videos = $res['videoStreams']['videos'];
                            $key = array_search('adaptive', array_column($videos, 'dimension'));

                            if (!empty($videos[$key]['url'])) {
                                $this->status = 'ok';
                                $this->image = html_entity_decode($res['meta']['defaultPreview']) . '&crop=1&size=640x320';
                                $this->title = $res['name'];
                                $result[] = [
                                    'file' => $videos[$key]['url'],
                                    'type' => 'hls',
                                    'label' => 'Original'
                                ];
                                return $result;
                            } else {
                                error_log("yadisk get_sources {$this->id} => videos not found");
                            }
                        } else {
                            error_log("yadisk get_sources {$this->id} => rootResourceId not found");
                        }
                    } else {
                        error_log("yadisk get_sources {$this->id} => application/json not found");
                    }
                } else {
                    error_log("yadisk get_sources {$this->id} => $status: $err");
                }
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
