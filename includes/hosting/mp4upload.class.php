<?php
class mp4upload
{
    public $name = 'mp4upload';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://www.mp4upload.com/';
    private $cookies = [];
    private $ch;
    private $proxy = false;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = $this->url;
        $this->id = strtr($id, ['embed-' => '', '.html' => '']);

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'accept: */*',
            'host: www.mp4upload.com',
            'origin: https://www.mp4upload.com',
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
    }

    private function title()
    {
        session_write_close();
        $url = $this->url . $this->id . '.html';
        $dom = \KubAT\PhpSimple\HtmlDomParser::file_get_html($url);
        if ($dom) {
            $title = $dom->find('h2', 0);
            if (!empty($title)) return trim(strtr($title->plaintext, ['Download' => '', 'File' => '']));
            else error_log("mp4upload title $url => title dom not found");
        } else {
            error_log("mp4upload title $url => dom not found");
        }
        return '';
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            $URL = $this->url . 'embed-' . $this->id . '.html';
            curl_setopt($this->ch, CURLOPT_URL, $URL);
            curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
                if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                    $this->cookies[] = $cookie[1];
                }
                return strlen($headerLine);
            });
            $this->proxy = proxy_rotator();
            if ($this->proxy) {
                curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy['proxy']);
                curl_setopt($this->ch, CURLOPT_PROXYTYPE, $this->proxy['type']);
                curl_setopt($this->ch, CURLOPT_PROXYUSERPWD, $this->proxy['usrpwd']);
            }

            session_write_close();
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                $scripts = $dom->find('script');
                if (!empty($scripts)) {
                    $eval = '';
                    foreach ($scripts as $script) {
                        if (strpos($script->innertext, 'eval(function(p,a,c,k,e,d)') !== FALSE) {
                            $eval = $script->innertext;
                            break;
                        }
                    }
                    if (!empty($eval)) {
                        $unpacker = new \JavascriptUnpacker\JavascriptUnpacker();
                        $decode = $unpacker->unpack($eval);
                        $video = get_string_between($decode, 'player.src("', '"');
                        if (!empty($video)) {
                            $this->status = 'ok';
                            $title = trim(get_string_between($decode, 'title: "(', ')"'));
                            $this->title = !empty($title) ? $title : $this->title();
                            $this->image = trim(get_string_between($decode, 'player.poster("', '"'));
                            $result[] = [
                                'file' => $video,
                                'type' => 'video/mp4',
                                'label' => 'Original'
                            ];
                            return $result;
                        } else {
                            error_log("mp4upload get_sources {$this->id} => player.src is empty");
                        }
                    } else {
                        error_log("mp4upload get_sources {$this->id} => eval is empty");
                    }
                } else {
                    error_log("mp4upload get_sources {$this->id} => script dom not found");
                }
            } elseif($this->proxy) {
                error_log("mp4upload get_sources {$this->id} => proxy {$this->proxy['format']} doesn't work => $status: $err");
                $proxyList = proxy_list();
                $key = array_search($this->proxy['format'], $proxyList);
                if ($key) {
                    $brokenProxy = $proxyList[$key];
                    unset($proxyList[$key]);
                    $proxyList = array_values($proxyList);
                    if (count($proxyList) > 0) {
                        if (!filter_var(get_option('delete_unused_proxy'), FILTER_VALIDATE_BOOLEAN)) $proxyList[] = $brokenProxy;
                        $proxyList = trim(implode("\n", $proxyList));
                        set_option('proxy_list', $proxyList);
                        return $this->get_sources();
                    } else {
                        set_option('proxy_list', []);
                    }
                }
            } else {
                error_log("mp4upload get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();
        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
