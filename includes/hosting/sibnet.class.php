<?php
class sibnet
{
    public $name = 'Video.Sibnet.Ru';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://video.sibnet.ru/';
    private $ch;

    function __construct($id = '')
    {
        session_write_close();
        
        $this->referer = 'https://video.sibnet.ru/';
        
        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        $this->id = $id;
        $this->url .= $id;

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'host: video.sibnet.ru',
            'origin: https://video.sibnet.ru',
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
    }

    private function getNewSources(array $oldSources = [])
    {
        session_write_close();

        $mh = curl_multi_init();
        $ch = [];

        foreach ($oldSources as $i => $dt) {
            $host = parse_url($dt['file'], PHP_URL_HOST);
            $ch[$i] = curl_init($dt['file']);
            curl_setopt($ch[$i], CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch[$i], CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch[$i], CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch[$i], CURLOPT_MAXREDIRS, 5);
            curl_setopt($ch[$i], CURLOPT_ENCODING, '');
            curl_setopt($ch[$i], CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch[$i], CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch[$i], CURLOPT_TIMEOUT, 30);
            curl_setopt($ch[$i], CURLOPT_DNS_SERVERS, dns_servers());
            curl_setopt($ch[$i], CURLOPT_NOSIGNAL, true);
            curl_setopt($ch[$i], CURLOPT_DNS_CACHE_TIMEOUT, 300);
            curl_setopt($ch[$i], CURLOPT_TCP_KEEPALIVE, true);
            curl_setopt($ch[$i], CURLOPT_TCP_NODELAY, true);
            curl_setopt($ch[$i], CURLOPT_FORBID_REUSE, true);
            curl_setopt($ch[$i], CURLOPT_FAILONERROR, true);
            curl_setopt($ch[$i], CURLOPT_HEADER, 1);
            curl_setopt($ch[$i], CURLOPT_NOBODY, 1);
            curl_setopt($ch[$i], CURLOPT_HTTPHEADER, array(
                "host: $host",
                "referer: https://video.sibnet.ru/",
                'range: bytes=0-'
            ));
            curl_setopt($ch[$i], CURLOPT_USERAGENT, USER_AGENT);
            curl_multi_add_handle($mh, $ch[$i]);
        }

        $active = null;
        do {
            $mrc = curl_multi_exec($mh, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc == CURLM_OK) {
            if (curl_multi_select($mh) == -1) {
                usleep(10);
            }
            do {
                $mrc = curl_multi_exec($mh, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }

        $result = [];
        foreach ($oldSources as $i => $dt) {
            $effectiveUrl = curl_getinfo($ch[$i], CURLINFO_EFFECTIVE_URL);
            $redirectUrl = curl_getinfo($ch[$i], CURLINFO_EFFECTIVE_URL);
            if (!empty($effectiveUrl) && $dt['file'] !== $effectiveUrl) {
                $result[] = [
                    'file' => $effectiveUrl,
                    'label' => $dt['label'],
                    'type' => $dt['type']
                ];
            } elseif (!empty($redirectUrl) && $dt['file'] !== $redirectUrl) {
                $result[] = [
                    'file' => $redirectUrl,
                    'label' => $dt['label'],
                    'type' => $dt['type']
                ];
            } else {
                $result[] = [
                    'file' => $dt['file'],
                    'label' => $dt['label'],
                    'type' => $dt['type']
                ];
            }
            curl_multi_remove_handle($mh, $ch[$i]);
        }
        curl_multi_close($mh);
        return $result;
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $json = str_replace(',]', ']', get_string_between($response, 'player.src(', ')'));
                $json = \OviDigital\JsObjectToJson\JsConverter::convertToJson($json);
                $json = @json_decode($json, true);
                if (JSON_ERROR_NONE === json_last_error()) {
                    $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                    $this->status = 'ok';
                    $this->title = $dom->find('meta[property="og:title"]', 0)->content;
                    $this->image = $dom->find('meta[property="og:image"]', 0)->content;
                    $video = 'https://video.sibnet.ru' . $json[0]['src'];
                    $result = [];
                    $result[] = [
                        'file' => $video,
                        'type' => $json[0]['type'],
                        'label' => 'Original'
                    ];
                    $result = $this->getNewSources($result);
                    return $result;
                } else {
                    error_log("sibnet get_sources {$this->id} => " . json_last_error_msg());
                }
            } else {
                error_log("sibnet get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
