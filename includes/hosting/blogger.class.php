<?php
class blogger
{
    public $name = 'Blogger';
    private $id = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = 'https://www.blogger.com/video.g?token=';
    private $cookies = [];
    private $ch;
    private $proxy = false;

    function __construct($id = '')
    {
        session_write_close();

        if (validate_url($id)) {
            $this->id = $id;
            $this->url = $this->id;
        } else {
            $this->id = $id;
            $this->url .= $this->id;
        }

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host);
        $resolveHost = implode(':', array($host, $port, $ipv4));

        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
    }

    private function parseSources(string $url = '')
    {
        session_write_close();

        if (!empty($url)) {
            curl_setopt($this->ch, CURLOPT_URL, $url);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                "cache-control: no-cache",
                "pragma: no-cache",
                'host: www.blogger.com',
                'origin: https://www.blogger.com'
            ));
            curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
                if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                    $this->cookies[] = $cookie[1];
                }
                return strlen($headerLine);
            });
            $this->proxy = proxy_rotator();
            if ($this->proxy) {
                curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy['proxy']);
                curl_setopt($this->ch, CURLOPT_PROXYTYPE, $this->proxy['type']);
                curl_setopt($this->ch, CURLOPT_PROXYUSERPWD, $this->proxy['usrpwd']);
            }

            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $stream = explode('VIDEO_CONFIG =', $response);
                $xstream = count($stream) >= 1 ? explode('</script>', trim(end($stream))) : '';
                if (!empty($xstream)) {
                    $data = json_decode(html_entity_decode(strtr(trim($xstream[0]), ['\u0026' => '&', '\u003d' => '=']), ENT_QUOTES, 'UTF-8'), true);
                    if (!empty($data['streams'])) {
                        $this->status = 'ok';
                        $this->title = trim(strtr($data['iframe_id'], ['BLOGGER-' => '']));
                        $this->image = $data['thumbnail'];
                        $this->referer = 'https://www.youtube.com/embed/?autohide=1&enablecastapi=0&html5=1&ps=blogger&enablejsapi=1&origin=https%3A%2F%2Fwww.blogger.com&widgetid=1';

                        $result = [];
                        foreach ($data['streams'] as $vid) {
                            $dt = [];
                            $dt['file'] = $vid['play_url'];
                            $dt['label'] = $this->label($vid['format_id']);
                            $dt['type'] = 'video/mp4';
                            $result[]   = $dt;
                        }
                        return $result;
                    } else {
                        error_log("blogger parseSources $url => streams not found");
                    }
                } else {
                    error_log("blogger parseSources $url => VIDEO_CONFIG not found");
                }
            } elseif($this->proxy) {
                error_log("blogger parseSources $url => proxy {$this->proxy['format']} doesn't work => $status: $err");
                $proxyList = proxy_list();
                $key = array_search($this->proxy['format'], $proxyList);
                if ($key) {
                    $brokenProxy = $proxyList[$key];
                    unset($proxyList[$key]);
                    $proxyList = array_values($proxyList);
                    if (count($proxyList) > 0) {
                        if (!filter_var(get_option('delete_unused_proxy'), FILTER_VALIDATE_BOOLEAN)) $proxyList[] = $brokenProxy;
                        $proxyList = trim(implode("\n", $proxyList));
                        set_option('proxy_list', $proxyList);
                        return $this->parseSources($url);
                    } else {
                        set_option('proxy_list', []);
                    }
                }
            } else {
                error_log("blogger $url => $status: $err");
            }
        }
        return [];
    }

    function get_sources()
    {
        session_write_close();

        if (!empty($this->id)) {
            if (validate_url($this->id)) {
                curl_setopt($this->ch, CURLOPT_URL, $this->id);
                $this->proxy = proxy_rotator();
                if ($this->proxy) {
                    curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy['proxy']);
                    curl_setopt($this->ch, CURLOPT_PROXYTYPE, $this->proxy['type']);
                    curl_setopt($this->ch, CURLOPT_PROXYUSERPWD, $this->proxy['usrpwd']);
                }

                $response = curl_exec($this->ch);
                $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                $err = curl_error($this->ch);

                if ($status >= 200 && $status < 400) {
                    $url = get_string_between($response, 'mozallowfullscreen" src="', '"');
                    return $this->parseSources($url);
                } elseif($this->proxy) {
                    error_log("blogger get_sources {$this->id} => proxy {$this->proxy['format']} doesn't work => $status: $err");
                    $proxyList = proxy_list();
                    $key = array_search($this->proxy['format'], $proxyList);
                    if ($key) {
                        $brokenProxy = $proxyList[$key];
                        unset($proxyList[$key]);
                        $proxyList = array_values($proxyList);
                        if (count($proxyList) > 0) {
                            if (!filter_var(get_option('delete_unused_proxy'), FILTER_VALIDATE_BOOLEAN)) $proxyList[] = $brokenProxy;
                            $proxyList = trim(implode("\n", $proxyList));
                            set_option('proxy_list', $proxyList);
                            return $this->get_sources();
                        } else {
                            set_option('proxy_list', []);
                        }
                    }
                } else {
                    error_log("blogger get_sources {$this->id} => $status: $err");
                }
            } else {
                return $this->parseSources($this->url);
            }
        }
        return [];
    }

    function get_cookies()
    {
        session_write_close();

        return $this->cookies;
    }

    function get_status()
    {
        session_write_close();

        return $this->status;
    }

    function get_title()
    {
        session_write_close();

        return $this->title;
    }

    function get_image()
    {
        session_write_close();

        return $this->image;
    }

    function get_referer()
    {
        session_write_close();

        return $this->referer;
    }

    function get_id()
    {
        session_write_close();

        return $this->id;
    }

    private function label($itag)
    {
        session_write_close();

        switch ($itag) {
            case '18':
                $label = "360p";
                break;
            case '59':
                $label = "480p";
                break;
            case '22':
                $label = "720p";
                break;
            case '37':
                $label = "1080p";
                break;
            case '5':
                $label = "240p";
                break;
            case '17':
                $label = "144p";
                break;
            case '34':
                $label = "360p";
                break;
            case '35':
                $label = "480p";
                break;
            case '36':
                $label = "240p";
                break;
            case '38':
                $label = "Original";
                break;
            case '43':
                $label = "360p";
                break;
            case '44':
                $label = "480p";
                break;
            case '45':
                $label = "720p";
                break;
            case '46':
                $label = "1080p";
                break;
            case '82':
                $label = "360p";
                break;
            case '84':
                $label = "720p";
                break;
            case '102':
                $label = "360p";
                break;
            case '104':
                $label = "720p";
                break;
            case '132':
                $label = "144p";
                break;
            case '133':
                $label = "240p";
                break;
            case '134':
                $label = "360p";
                break;
            case '135':
                $label = "480p";
                break;
            case '136':
                $label = "720p";
                break;
            case '137':
                $label = "1080p";
                break;
            default:
                $label = "Unknown";
                break;
        }
        return $label;
    }

    function __destruct()
    {
        session_write_close();

        curl_close($this->ch);
    }
}
