<?php
class vidio
{
    public $name = 'Vidio';
    private $id = '';
    private $host = '';
    private $title = '';
    private $image = '';
    private $referer = '';
    private $status = 'fail';
    private $url = '';
    private $user_agent;
    private $language;
    private $tracks = [];
    private $ch;
    private $cookies = [];
    private $proxy = false;

    function __construct($id = '')
    {
        session_write_close();

        $this->referer = $this->url;
        $this->id = $id;
        if (strpos($id, '/live') !== FALSE) {
            $qry = parse_url($id, PHP_URL_QUERY);
            if (empty($qry)) {
                $this->url = rtrim($id, '/') . '/embed?autoplay=false&player_only=true&live_chat=false&enable_websocket=false&mute=false';
            }
        } else {
            $this->url = $id;
        }

        $scheme = parse_url($this->url, PHP_URL_SCHEME);
        $this->host = parse_url($this->url, PHP_URL_HOST);
        $port = parse_URL($this->url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($this->host);
        $resolveHost = implode(':', array($this->host, $port, $ipv4));

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'accept: */*',
            'accept-encoding: gzip, deflate, br',
            'accept-language: en,id;q=0.9,id-ID;q=0.8,ms;q=0.7',
            'content-type: text/plain;charset=utf-8',
            'origin: https://www.vidio.com',
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $headerLine) {
            if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) && !empty($cookie[1])) {
                $this->cookies[] = $cookie[1];
            }
            return strlen($headerLine);
        });
    }

    function get_sources()
    {
        session_write_close();
        if (!empty($this->id)) {
            curl_setopt($this->ch, CURLOPT_URL, $this->url);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
            $this->proxy = proxy_rotator();
            if ($this->proxy) {
                curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy['proxy']);
                curl_setopt($this->ch, CURLOPT_PROXYTYPE, $this->proxy['type']);
                curl_setopt($this->ch, CURLOPT_PROXYUSERPWD, $this->proxy['usrpwd']);
            }

            session_write_close();
            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);

            if ($status >= 200 && $status < 400) {
                $this->title = get_string_between($response, 'video-title="', '"');
                $this->image = get_string_between($response, 'image-url="', '"');
                $dash_url = get_string_between($response, 'dash-url="', '"');
                $hls_url = get_string_between($response, 'hls-url="', '"');
                $watch_url = get_string_between($response, 'watchpage-url="', '"');
                $video_id = get_string_between($response, 'Video" data-id="', '"');

                if (!empty($watch_url)) {
                    if (!empty($dash_url)) {
                        curl_setopt($this->ch, CURLOPT_URL, $watch_url . '/tokens?type=dash');
                        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
                        curl_setopt($this->ch, CURLOPT_POSTFIELDS, '');
                        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                            'accept: */*',
                            'accept-encoding: gzip, deflate, br',
                            'accept-language: en,id;q=0.9,id-ID;q=0.8,ms;q=0.7',
                            'content-type: text/plain;charset=utf-8',
                            'origin: https://www.vidio.com',
                        ));
                        curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
                        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
                        if (!empty($this->cookies)) {
                            curl_setopt($this->ch, CURLOPT_COOKIE, implode(',', $this->cookies));
                        }
                        if ($this->proxy) {
                            curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy['proxy']);
                            curl_setopt($this->ch, CURLOPT_PROXYTYPE, $this->proxy['type']);
                            curl_setopt($this->ch, CURLOPT_PROXYUSERPWD, $this->proxy['usrpwd']);
                        }

                        session_write_close();
                        $response = curl_exec($this->ch);
                        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                        $err = curl_error($this->ch);

                        if ($status >= 200 && $status < 400) {
                            $arr = json_decode($response, true);
                            if (!empty($arr['token'])) {
                                list($start, $end) = array_pad(explode('/vp9/', $dash_url), 2, '');
                                $this->status = 'ok';
                                $result[] = [
                                    'file' => $start . '/' . $arr['token'] . '/vp9/' . $end,
                                    'type' => 'mpd',
                                    'label' => 'Original'
                                ];
                                return $result;
                            } else {
                                error_log('vidio get_sources ' . $this->id . ' => get dash => token not found => ' . $response);
                            }
                        } else {
                            error_log("vidio get-sources {$this->id} => get dash token => $status: $err");
                        }
                    } elseif (!empty($hls_url)) {
                        if (strpos($hls_url, 'common_tokenized_playlist') !== false) {
                            $this->status = 'ok';
                            $result[] = [
                                'file' => $hls_url,
                                'type' => 'hls',
                                'label' => 'Original'
                            ];
                            return $result;
                        } else {
                            curl_setopt($this->ch, CURLOPT_URL, $watch_url . '/tokens');
                            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
                            curl_setopt($this->ch, CURLOPT_POSTFIELDS, '');
                            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                                'accept: */*',
                                'accept-encoding: gzip, deflate, br',
                                'accept-language: en,id;q=0.9,id-ID;q=0.8,ms;q=0.7',
                                'content-type: text/plain;charset=utf-8',
                                'origin: https://www.vidio.com',
                            ));
                            curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
                            curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
                            if (!empty($this->cookies)) {
                                curl_setopt($this->ch, CURLOPT_COOKIE, implode(',', $this->cookies));
                            }
                            if ($this->proxy) {
                                curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy['proxy']);
                                curl_setopt($this->ch, CURLOPT_PROXYTYPE, $this->proxy['type']);
                                curl_setopt($this->ch, CURLOPT_PROXYUSERPWD, $this->proxy['usrpwd']);
                            }

                            session_write_close();
                            $response = curl_exec($this->ch);
                            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                            $err = curl_error($this->ch);

                            if ($status >= 200 && $status < 400) {
                                $arr = json_decode($response, true);
                                if (!empty($arr['token'])) {
                                    $this->status = 'ok';
                                    $result[] = [
                                        'file' => $hls_url . '?' . $arr['token'],
                                        'type' => 'hls',
                                        'label' => 'Original'
                                    ];
                                    return $result;
                                } else {
                                    error_log('vidio get_sources ' . $this->id . ' => get hls token not found => '  . $response);
                                }
                            } else {
                                error_log("vidio get_sources {$this->id} => get hls token => $status: $err");
                            }
                        }
                    } else {
                        curl_setopt($this->ch, CURLOPT_URL, 'https://www.vidio.com/interactions_stream.json?video_id=' . $video_id . '&type=videos');
                        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
                        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                            'accept: application/json, text/javascript, */*; q=0.01',
                            'accept-encoding: gzip, deflate, br',
                            'accept-language: en,id;q=0.9,id-ID;q=0.8,ms;q=0.7',
                            'cache-control: no-cache',
                            'cookie: ahoy_visitor=00316d3e-d038-4b2f-a872-6a46d3a48fb8; ahoy_visit=3b782ae6-97e3-4668-8db9-2647160ed06d; g_state={"i_p":1651738537769,"i_l":1}; shva=1; remember_user_token=eyJfcmFpbHMiOnsibWVzc2FnZSI6IlcxczJNRGMwTURRNE1WMHNJaVF5WVNReE1DUmtXR1l3Wm5SaFRVOHdjMjluVUVOMU5UUkhjM3BsSWl3aU1UWTFNVGN6TVRRME5pNDJOak13TkRBMElsMD0iLCJleHAiOiIyMDI0LTA1LTA1VDA2OjE3OjI2LjY2M1oiLCJwdXIiOiJjb29raWUucmVtZW1iZXJfdXNlcl90b2tlbiJ9fQ%3D%3D--352c78b388dbba239efe34b4ac6308f805d3367d; plenty_id=60740481; _vidio=true; access_token=eyJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7InR5cGUiOiJhY2Nlc3NfdG9rZW4iLCJ1aWQiOjYwNzQwNDgxfSwiZXhwIjoxNjUxODE3ODQ5fQ.A9jn1sI2T_0fQstzgy_f6zH6FJjf0SzsCH3RM_Eb2bY; luws=b8ec299dd31d22eb1c9f965c_60740481; _vidio_session=dVc4SGQ2eVBOaXhtMTRLWlFITjdaU0NKUFVoN21tZ3hFU3pNK0NEYzNBTVZ4Mm9xWlp5N00rM09aMzFhRno0ZmkrOUJWYWJkUktiRWNZT1BGSlpQc2pHSkJ5a2F2eUhIc3YyREE5eGdIYk5idGQzM3luOTNZVGlHRzJ3N3pFdFNrSll5Tk1SMVhRZEhsZkxIUTBuOXpteXIrRlU0MFRmK2Y0L2ZubU1aTDY2S21VcnFQSy9JQWpMWXFwMUhhaStTRHlVTytUS2NUWDZGZHBRcUtzMHY2ZktIcGpVN1RLbjN5bExqUmpEWnlabHYwZ3RjREVyYk1UYmpkeHJOU2s3ZkNSQnRUOVI4RHJNZ2NoSERIRzg5bENnUi9IUWMxRnRJdC9PTnViMk93TGQ5Q0FZcmNBWDZSRkF1VU1jd0dZMUs4eEdhYlZWUVJLbzFnTWhRbHVPUFFTWjNEMnNkOHpZY3g2T0RxQ1FTNnVhWE55Ym5HMHBueXJZelJ3cGVLejhObXhiVi9wZFJNVWJxai9YWmpJWkVCZDhSak94N0FwejhjZkhIZnVQc3hGNU00cW0zb0FsZFJnS0gyZkp5OFJVUGFTT0JzNlpyeFQvUXp0RlowTWpVbFV2ZTR4ckVLL0RZWkg3RXZ3a2w5STNtRVoyNy8yanphVVBjK1NGMjlOeEtnY3ZERmtHei9KNXNGSDMxaDI1UEZRQ2RhWXphU0NxaHlaMThBT2xLSG84dUJVeFF0djc3cnBJMlhhWjRsNEpBM1R3cEpibjVPUkFvTFA2MnlPWnpUd2FiMVVSWDhrVG55Z2lkUmdYb2RvYllWN3ZibnhlZXJYQVdwMk9FNS92ZmNCaVJsbUNiMmhyM1RJYVhDblVkZEdXNFpjZXF4WGZtMGFCWFc3TTUwZ009LS03cUtaRWFZWElNdkFIOHZ3MmpyRkl3PT0%3D--847c6c3ff7992ea42f4eda8863d67ab89b35df23; _vidio_session=NDRuK2VORm9hSGh5VEI2WmU5akRkY3NJODQ0Njd2RzhFTy8vOU1mdzErQ3kwN2hROTdabXZqOU91c2hqd1J2OVJ6d3crWWJGTGZMbWNuS0xEcmY0ZExLMEJpbktxNXUwR3Y1My9JN3dKWVBwRnNCcnNpMm5KUHIzeTg5c3ZyZ0g5Um1acWFGNGlVMXFEZ3lPMjVPVWVzdXpyTHUrcUV4RGtkeCtxLzliRlVYUDA5TTlXMmR0SjRmVm1sb2JxWGE4Q1p1QktZVUFLcXE0c1ozRXRWdldEcUt5d3hSdmJvRXRzTGpadkNYdmNYSThnWSt3Rzd1NDJFaHA1VFo3b0lidCtrTEUzN0FCbnB6UC9CSFgvb3NuNmx0MzIyaGM5MmIrUFVKYzh2U3hTZXExQlE0V3MrbnJzd004S2NPYkQwbElUUVA3Z212YldTWENOTG1URndpeVFoa0hYMVJDdWtnN0ZCZFFGVDNMNm9IR2NyOExlcmdoNHBtYWtwKzROYWh2VkRUR083eUtTR1BUZUMvOXFUVjVENE9FbUhhYXJOMm5kWFZGN3dXWjk2NEJXY1FSOWkrWFFiMGRITWIza3dGQ1pEVlZZdXRuVUQzdkxHUEZ6WmVhMmNBYkUybVdjeU1RL0lrR2xCRnREUjZ4TSs0OUdrNStZVGN3d2hhQ1dMVTM2SmNmS1N2Y2pzdWNyQ3FWRWk1d0wxUEdjeEgzZzlUT3V0Vnpqd1VaQ0U3SU9TNlFwMWI2bllZaFdzY0dxK0ZvQTVNQVlPRThJWld3U2hJR2VRR2s4RjFFOWpRNFFsYStjdjMvWHlMaU9Xb2V3N3RVTzk3bnk0TGlOdW5Yb3o0d2h0QmljUnprNDNWdWJCb1JPRXRWVzlmblJDam1qbDlyVHB5U2RNWlIwaWc9LS1OS1d0V3ZHcnRrZU1vV2d3SktncVF3PT0%3D--6b46a1c6a8813c5529eb3a48d60a92028f5d4fa4',
                            'pragma: no-cache',
                            'referer: https://www.vidio.com/embed/6562795-my-lovely-angel?autoplay=true&player_only=true&live_chat=false&enable_websocket=true&mute=true',
                            'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36',
                            'x-csrf-token: qocgzvDi0nsjOZ4tEYJLuDNyMF/3HrwZBFvPnzDa8tbYpTvXFG3VqCdT0zKBRAga29g2p5ya10mEF+/V8zTGIg==',
                            'x-requested-with: XMLHttpRequest'
                        ));
                        curl_setopt($this->ch, CURLOPT_REFERER, $this->url);
                        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
                        if (!empty($this->cookies)) {
                            curl_setopt($this->ch, CURLOPT_COOKIE, implode(',', $this->cookies));
                        }
                        if ($this->proxy) {
                            curl_setopt($this->ch, CURLOPT_PROXY, $this->proxy['proxy']);
                            curl_setopt($this->ch, CURLOPT_PROXYTYPE, $this->proxy['type']);
                            curl_setopt($this->ch, CURLOPT_PROXYUSERPWD, $this->proxy['usrpwd']);
                        }

                        session_write_close();
                        $response = curl_exec($this->ch);
                        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                        $err = curl_error($this->ch);

                        if ($status >= 200 && $status < 400) {
                            $arr = json_decode($response, true);
                            if (!empty($arr['source'])) {
                                $this->status = 'ok';
                                $result[] = [
                                    'file' => $arr['source'],
                                    'type' => 'hls',
                                    'label' => 'Original'
                                ];
                                return $result;
                            } elseif (!empty($arr['source_dash'])) {
                                $this->status = 'ok';
                                $result[] = [
                                    'file' => $arr['source_dash'],
                                    'type' => 'mpd',
                                    'label' => 'Original'
                                ];
                                return $result;
                            } else {
                                error_log('vidio get hls token not found => '  . $response);
                            }
                        } else {
                            error_log("vidio get hls token => $status: $err");
                        }
                    }
                } else {
                    $logFile = execHeadlessChromium($this->url, 'vidio~' . keyFilter($this->id), true);
                    if ($logFile) {
                        open_resources_handler();
                        $fp = @fopen($logFile, 'r');
                        if ($fp) {
                            stream_set_blocking($fp, false);
                            $content = stream_get_contents($fp);
                            fclose($fp);

                            if ($content) {
                                preg_match_all('/"url":"([^"]+)"/', $content, $urls);
                                preg_match_all('/\"([^"]+)\"/', $content, $urls1);

                                $hls_list = array_filter($urls[1], function ($v) {
                                    return strpos($v, '.m3u8') !== FALSE;
                                });
                                $hls_list = array_values($hls_list);
                                $hls_list1 = array_filter($urls1[1], function ($v) {
                                    return strpos($v, '.m3u8') !== FALSE;
                                });
                                $hls_list1 = array_values($hls_list1);
                                $hls = array_merge($hls_list, $hls_list1);

                                $dash_list = array_filter($urls[1], function ($v) {
                                    return strpos($v, '.mpd') !== FALSE;
                                });
                                $dash_list = array_values($dash_list);
                                $dash_list1 = array_filter($urls1[1], function ($v) {
                                    return strpos($v, '.mpd') !== FALSE;
                                });
                                $dash_list1 = array_values($dash_list1);
                                $dash = array_merge($dash_list, $dash_list1);

                                if (!empty($hls[0])) {
                                    unlink($logFile);
                                    $this->status = 'ok';
                                    $result[] = [
                                        'file' => trim($hls[0], '\/'),
                                        'type' => 'hls',
                                        'label' => 'Original'
                                    ];
                                    return $result;
                                } elseif (!empty($dash[0])) {
                                    unlink($logFile);
                                    $this->status = 'ok';
                                    $result[] = [
                                        'file' => trim($dash[0], '\/'),
                                        'type' => 'mpd',
                                        'label' => 'Original'
                                    ];
                                    return $result;
                                } else {
                                    error_log("vidio get_sources not found");
                                }
                            } else {
                                error_log("vidio get_sources log file cannot open");
                            }
                        } else {
                            error_log("vidio get_sources log file not found");
                        }
                    }
                }
            } elseif ($this->proxy) {
                error_log("vidio get_sources {$this->id} => proxy {$this->proxy['format']} doesn't work => $status: $err");
                $proxyList = proxy_list();
                $key = array_search($this->proxy['format'], $proxyList);
                if ($key) {
                    $brokenProxy = $proxyList[$key];
                    unset($proxyList[$key]);
                    $proxyList = array_values($proxyList);
                    if (count($proxyList) > 0) {
                        if (!filter_var(get_option('delete_unused_proxy'), FILTER_VALIDATE_BOOLEAN)) $proxyList[] = $brokenProxy;
                        $proxyList = trim(implode("\n", $proxyList));
                        set_option('proxy_list', $proxyList);
                        return $this->get_sources();
                    } else {
                        set_option('proxy_list', []);
                    }
                }
            } else {
                error_log("vidio get_sources {$this->id} => $status: $err");
            }
        }
        return [];
    }

    function get_tracks()
    {
        session_write_close();
        return $this->tracks;
    }

    function get_user_agent()
    {
        session_write_close();
        return $this->user_agent;
    }

    function get_language()
    {
        session_write_close();
        return $this->language;
    }

    function get_status()
    {
        session_write_close();
        return $this->status;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_image()
    {
        session_write_close();
        return $this->image;
    }

    function get_referer()
    {
        session_write_close();
        return $this->referer;
    }

    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
