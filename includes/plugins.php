<?php
session_write_close();

$class = new \Plugins();
$class->load();

function get_plugin_option(string $key = '')
{
    session_write_close();
    $key .= '_settings';
    $class = new \Plugins();
    if (!empty($key)) {
        $class->setLimit(1);
        $class->setCriteria('key', $key);
        $data = $class->getOne(['value']);
        if ($data) return json_decode($data['value'], true);
    } else {
        $result = [];
        $data = $class->get(['key', 'value']);
        foreach ($data as $dt) {
            $result[$dt['key']] = json_decode($dt['value'], true);
        }
        return $result;
    }
}

function set_plugin_option(string $key = '', $value = null)
{
    session_write_close();
    $key .= '_settings';
    $class = new \Plugins();
    $class->setCriteria('key', $key);
    $data = $class->getOne(['id']);
    if ($data) {
        $class->setCriteria('key', $key);
        $value = is_array($value) ? json_encode($value, TRUE) : $value;
        return $class->update(array(
            'key' => $key,
            'value' => $value,
            'updated' => time()
        ));
    } else {
        return $class->insert(array(
            'key' => $key,
            'value' => $value,
            'updated' => time()
        ));
    }
}

function remove_plugin_option(string $key = '')
{
    session_write_close();
    $key .= '_settings';
    if (!empty($key)) {
        $class = new \Plugins();
        $class->setCriteria('key', $key);
        return $class->delete();
    }
    return FALSE;
}

function createPluginMenus()
{
    session_write_close();
    $html = '';
    $class = new \Plugins();
    $class->setCriteria('key', '%_settings', 'NOT LIKE');
    $list = $class->get(['value']);
    if ($list) {
        $adminURL = BASE_URL . admin_dir() . '/';
        foreach ($list as $dt) {
            $data = json_decode($dt['value'], true);
            if (file_exists(BASE_DIR . 'plugins/' . $data['name'] . '/classes/' . $data['name'] . '.class.php')) {
                $html .= '<a class="dropdown-item" href="' . $adminURL . $data['slug'] . '"><i aria-hidden="true" class="fas fa-angle-double-right"></i><span class="ml-2">' . $data['name'] . '</span></a>';
            }
        }
    }
    return $html;
}

function createPluginAdminMenus(string $position = 'top')
{
    session_write_close();
    $html = '';
    $userLogin = current_user();
    if ($userLogin) {
        $class = new \Plugins();
        $class->setCriteria('key', '%_settings', 'NOT LIKE');
        $list = $class->get(['value']);
        if ($list) {
            $adminURL = BASE_URL . admin_dir() . '/';
            foreach ($list as $i) {
                $j = json_decode($i['value'], true);
                if (file_exists(BASE_DIR . 'plugins/' . $j['name'] . '/classes/' . $j['name'] . '.class.php') && !empty($j['admin_menus'])) {
                    $menus = array_filter($j['admin_menus'], function ($val) use ($position) {
                        return $val['position'] === $position;
                    });
                    foreach ($menus as $dt) {
                        if ($position === 'top') {
                            if (!empty($dt['sub_menu'])) {
                                foreach ($dt['sub_menu'] as $sub) {
                                    $icon = !empty($sub['icon']) ? '<i aria-hidden="true" class="' . $sub['icon'] . ' mr-2"></i>' : '';
                                    $sub_menu = '<a class="dropdown-item" href="' . $adminURL . $sub['slug'] . '">' . $icon . $sub['name'] . '</a>';
                                }
                                $icon = !empty($dt['icon']) ? '<i aria-hidden="true" class="' . $dt['icon'] . ' mr-2"></i>' : '';
                                $html .= '<li class="nav-item dropdown ' . (strpos($_SERVER['REQUEST_URI'], $dt['slug']) !== FALSE ? 'active' : '') . '"><a class="nav-link dropdown-toggle" href="#" id="dp' . $dt['name'] . '" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' . $icon . $dt['name'] . '</a><div class="dropdown-menu shadow border-0" aria-labelledby="dp' . $dt['name'] . '">' . $sub_menu . '</div></li>';
                            } else {
                                $icon = !empty($dt['icon']) ? '<i aria-hidden="true" class="' . $dt['icon'] . ' mr-2"></i>' : '';
                                $html .= '<li class="nav-item ' . (strpos($_SERVER['REQUEST_URI'], $dt['slug']) !== FALSE ? 'active' : '') . '"><a class="nav-link" href="' . $adminURL . $dt['slug'] . '">' . $icon . $dt['name'] . '</a></li>';
                            }
                        } else {
                            $icon = !empty($dt['icon']) ? '<i aria-hidden="true" class="' . $dt['icon'] . ' mr-2"></i>' : '';
                            $html .= '<a class="dropdown-item" href="' . $adminURL . $dt['slug'] . '">' . $icon . $dt['name'] . '</a>';
                        }
                    }
                }
            }
        }
    }
    return $html;
}

function createPluginFrontMenus(string $position = 'top', bool $restricted = false)
{
    session_write_close();
    $html = '';
    $class = new \Plugins();
    $class->setCriteria('key', '%_settings', 'NOT LIKE');
    $data = $class->getOne(['value']);
    if ($data) {
        $data = json_decode($data['value'], true);
        if (file_exists(BASE_DIR . 'plugins/' . $data['name'] . '/classes/' . $data['name'] . '.class.php') && !empty($data['front_menus'])) {
            $data = array_filter($data['front_menus'], function ($val) use ($position, $restricted) {
                return $val['position'] === $position && $val['restricted'] === $restricted;
            });
            foreach ($data as $dt) {
                $icon = !empty($dt['icon']) ? '<i aria-hidden="true" class="' . $dt['icon'] . ' mr-2"></i>' : '';
                $html .= '<li class="nav-item ' . (strpos($_SERVER['REQUEST_URI'], '/p/' . $dt['slug']) !== FALSE ? 'active' : '') . '"><a class="nav-link" href="' . BASE_URL . 'p/' . $dt['slug'] . '">' . $icon . $dt['name'] . '</a></li>';
            }
        }
    }
    return $html;
}
