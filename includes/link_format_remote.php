<?php
if (!defined('BASE_DIR')) exit();
session_write_close();
$scheme = $isSSL ? 'https:' : 'http:';
$baseUrl = strtr(BASE_URL, ['https:' => $scheme, 'http:' => $scheme]);
?>
<div class="row my-3">
    <div class="col-12">
        <button class="btn btn-custom" data-toggle="collapse" data-target="#hostIDFormat" aria-expanded="true">⚡ Show/Hide Supported Sites</button>
        <div class="card collapse my-3" id="hostIDFormat">
            <div class="card-header">⚡ Supported Sites</div>
            <div class="card-body p-0">
                <div class="card-content px-3 pt-3">This tool supports remote link to websites listed below. We are always developing to support more websites in the future. You may use them but please support these sites by visiting their sponsors or donate in order to keep these sites available. If the site you want to use is not listed below. Please don't hesitate to contact us.</div>
                <div class="card-content p-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="search-host">
                                <i class="fas fa-magnifying-glass"></i>
                            </span>
                        </div>
                        <input id="txtSearchHost" type="search" class="form-control" placeholder="Search..." aria-describedby="search-host" aria-label="Search">
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="tbHost" class="table table-striped table-hover mb-0" style="font-size:14px!important; min-width:640px">
                        <thead class="thead-dark">
                            <tr>
                                <th style="width:45px;"></th>
                                <th style="width:125px;">Host</th>
                                <th style="width:110px;">Status</th>
                                <th>Link Format</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr data-host="gdrive"><td><img alt="Google Drive" title="Google Drive" width="16" height="16" src="http://stream.local:8080/assets/img/logo/gdrive.png"></td><td>Google Drive</td><td><span class="text-success"><i aria-hidden="true" class="fas fa-check-circle fa-lg"></i><span class="ml-1">Working</span></span></td><td><input type="url" readonly="" onfocus="this.select()" class="form-control form-control-sm mb-1" value="https://drive.google.com/file/d/1KPtwrGMyjC9c_ZpOID4eLaSDErrA5x3O/view" aria-label="Example Link"><input type="url" readonly="" onfocus="this.select()" class="form-control form-control-sm mb-1" value="https://drive.google.com/open?id=1KPtwrGMyjC9c_ZpOID4eLaSDErrA5x3O" aria-label="Example Link"></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function searchHost(txt) {
        var $list = $('#tbHost tbody tr');
        $list.each(function(i, e) {
            if ($(e).data('host').toLowerCase().indexOf(txt) > -1 || $(e).html().toLowerCase().indexOf(txt) > -1) {
                $(e).removeClass('d-none');
            } else {
                $(e).addClass('d-none');
            }
        });
    }

    $('#txtSearchHost').on('blur keyup change', function() {
        searchHost($(this).val().toLowerCase());
    });
</script>