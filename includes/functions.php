<?php
session_write_close();

function autoloadClasses($class)
{
    session_write_close();
    $classFile = BASE_DIR . "includes/classes/$class.class.php";
    if (file_exists($classFile)) {
        require $classFile;
    }
}
function autoloadModels($class)
{
    session_write_close();
    $classFile = BASE_DIR . "includes/models/$class.class.php";
    if (file_exists($classFile)) {
        require $classFile;
    }
}
function autoloadHosting($class)
{
    session_write_close();
    $classFile = BASE_DIR . "includes/hosting/$class.class.php";
    if (file_exists($classFile)) {
        require $classFile;
    }
}
spl_autoload_register("autoloadClasses");
spl_autoload_register("autoloadModels");
spl_autoload_register("autoloadHosting");

$cacheInstance = get_option('cache_instance');
if ($cacheInstance === 'redis' && (extension_loaded('redis') || extension_loaded('predis') || extension_loaded('phpredis'))) {
    $cacheConfig = new \Phpfastcache\Drivers\Redis\Config([
        'host' => '127.0.0.1',
        'port' => 6379
    ]);
} else {
    $cacheInstance = $cacheInstance === 'sqlite' && extension_loaded('pdo_sqlite') ? 'sqlite' : 'files';
    $cacheConfig = new \Phpfastcache\Config\ConfigurationOption([
        'path' => BASE_DIR . 'tmp',
        'preventCacheSlams' => TRUE,
        'cacheSlamsTimeout' => 30
    ]);
}
$InstanceCache = \Phpfastcache\CacheManager::getInstance($cacheInstance, $cacheConfig, keyFilter(BASE_URL));

$timeZone = get_option('timezone');
if (!empty($timeZone) && date_default_timezone_get() !== $timeZone) {
    date_default_timezone_set($timeZone);
}

function dns_servers()
{
    session_write_close();
    return '1.1.1.1,1.0.0.1,8.8.8.8,8.8.4.4';
}

function gdrive_rent_excludes()
{
    session_write_close();
    $iCache = new \InstanceCache();
    $iCache->setKey('gdrive_rent_excludes');
    $cache = $iCache->get();
    if ($cache) {
        return $cache;
    } else {
        open_resources_handler();
        $fp = @fopen(BASE_DIR . 'includes/gdrive_rent_excludes.json', 'r');
        if ($fp) {
            stream_set_blocking($fp, false);
            $content = stream_get_contents($fp);
            fclose($fp);

            if ($content) {
                $content = json_decode($content, true);
                $iCache->save($content, 10, 'gdrive_rent_excludes');
                return $content;
            }
        }
    }
    return [];
}

function is_smartTV(string $ua = '')
{
    session_write_close();
    if (empty($ua) && isset($_SERVER['HTTP_USER_AGENT'])) $ua = $_SERVER['HTTP_USER_AGENT'];
    $class = new \WhichBrowser\Parser($ua);
    $isTV = $class->device->type === 'television';
    $model = isset($class->device->model) ? strtolower($class->device->model) : '';
    $isModelTV = !empty($model) && (strpos($model, 'smart tv') !== false || strpos($model, 'smart-tv') !== false || strpos($model, 'smart_tv') !== false);
    $isCustomTV = preg_match('/seraphic|hbbtv|sraf|crkey|tpm171e|a95x-r1|netbox|googletv|mxqpro|tx3|m8s|neo-u1|t95zplus|beelink|abox|p281|hx_3229|v88|mx9|mxiii|zidoo|r-tv|brightsign|leelbox|minia5x|x96|mobile vr|directfb|kylo|ppc|telefunken|linux sh4|linux mips|kded|h96|x96mini|freebox| tv |smarttv|smart_tv|smart tv|smart-tv|wii|playstation|nintendo|xbox/i', $ua, $tv) && !empty($tv);
    return $isTV || $isModelTV || $isCustomTV;
}

function detect_chrome()
{
    session_write_close();
    if (PHP_OS_FAMILY === 'Linux') {
        if (function_exists('exec')) {
            return exec('google-chrome-stable --disable-gpu --no-sandbox --version');
        } else {
            error_log("detect_chrome => exec function not exist");
        }
    } else {
        $detect = new \HeadlessChromium\AutoDiscover();
        return $detect->guessChromeBinaryPath();
    }
    return false;
}

function getHeadlessChromeUA()
{
    session_write_close();
    $logFile = execHeadlessChromium('https://dnschecker.org/user-agent-info.php', 'chrome-version', false);
    if ($logFile) {
        open_resources_handler();
        $fp = @fopen($logFile, 'r');
        if ($fp) {
            stream_set_blocking($fp, false);
            $content = stream_get_contents($fp);
            fclose($fp);

            $dom = KubAT\PhpSimple\HtmlDomParser::str_get_html($content);
            if ($dom) {
                $ua = $dom->find('#uai_details', 0)->find('tr', 0)->find('td', 0);
                if ($ua) {
                    @unlink($logFile);
                    $ua = trim($ua->plaintext);
                    $class = new \InstanceCache();
                    $class->setKey('chrome_ua');
                    $class->save($ua, 600);
                    return $ua;
                }
            }
        }
    }
    return false;
}

function execHeadlessChromium(string $url = '', string $logName = '', bool $debug = false)
{
    session_write_close();
    if (detect_chrome()) {
        create_dir(BASE_DIR . 'tmp/hosts');
        create_dir(BASE_DIR . 'cache/playlist');
        create_dir(BASE_DIR . 'cache/streaming');
        create_dir(BASE_DIR . 'cache/logs/process');

        $logName = keyFilter($logName);
        $logName = substr($logName, 0, 240);
        $parseURL = parse_url($url);
        $now = time();
        $logFile = strtr(BASE_DIR . 'cache/logs/' . $logName . '.log', ['/' => DIRECTORY_SEPARATOR, '\/' => DIRECTORY_SEPARATOR]);
        $logProccess = strtr(BASE_DIR . 'cache/logs/process/' . $logName . '.log', ['/' => DIRECTORY_SEPARATOR, '\/' => DIRECTORY_SEPARATOR]);
        if (file_exists($logFile)) {
            $updated = @filemtime($logFile);
            if ($updated && ($now - $updated) > 600) {
                @unlink($logFile);
                return execHeadlessChromium($url, $logName, $debug);
            } else {
                return $logFile;
            }
        } elseif (!file_exists($logProccess)) {
            $config = array(
                'noSandbox' => true,
                'headless' => true,
                'enableImages' => false,
                'ignoreCertificateErrors' => true
            );

            $header = array(
                'Accept' => '*/*',
                'Accept-Encoding' => 'gzip, deflate, br',
                'Accept-Language' => 'id,en,q=0.9,id-ID,q=0.8',
                'Connection' => 'keep-alive',
                'Referer' => $parseURL['scheme'] . '://' . $parseURL['host'] . '/',
                'Cookie' => 'tvshow=nq5fgj6tqtp335t4gjj0rggt94; token=62b7f6e82a08d'
            );

            if (strpos($logFile, 'chrome-version') !== false) {
                $config['headers'] = $header;
            } else {
                if (strpos($logFile, 'streamsb') !== false) {
                    $class = new \InstanceCache();
                    $ua = $class->get('chrome_ua');
                    $ua = $ua && strpos($ua, 'HeadlessChrome') !== false ? $ua : getHeadlessChromeUA();
                    $header['User-Agent'] = $ua ? $ua : USER_AGENT;
                } else {
                    $header['User-Agent'] = USER_AGENT;
                }
                $config['headers'] = $header;
                $config['userAgent'] = $header['User-Agent'];
            }

            if ($debug) {
                $log = new \Monolog\Logger($logName);
                $log->pushHandler(new \Monolog\Handler\StreamHandler($logProccess, \Monolog\Logger::DEBUG));
                $config['debugLogger'] = $log;
            }
            $chromeInstance = PHP_OS_FAMILY === 'Linux' ? 'google-chrome' : null;
            $browserFactory = new \HeadlessChromium\BrowserFactory($chromeInstance);
            $browserInstance = $browserFactory->createBrowser($config);

            try {
                $page = $browserInstance->createPage();
                $page->getSession()->sendMessageSync(new \HeadlessChromium\Communication\Message('Network.setExtraHTTPHeaders', [
                    'headers' => $header
                ]));

                $content = '';
                if ($debug) {
                    $page->navigate($url)->waitForNavigation();
                    $content .= $page->getHtml(10000);
                } else {
                    $page->navigate($url)->waitForNavigation(\HeadlessChromium\Page::LOAD, 5000000);
                    $content .= $page->getHtml(5000000);
                }

                $created = create_file($logProccess, $content, 'a+');
                if ($created) {
                    $copied = copy($logProccess, $logFile);
                    $browserInstance->close();
                    if ($copied) {
                        @unlink($logProccess);
                        return $logFile;
                    } else {
                        error_log("execHeadlessChromium $url => cannot copy $logProccess to $logFile");
                    }
                } else {
                    $created = create_file($logFile, $content, 'a+');
                    if ($created) {
                        $browserInstance->close();
                        @unlink($logProccess);
                        return $logFile;
                    } else {
                        error_log("execHeadlessChromium $url => cannot create $logFile");
                    }
                }
            } catch (\Exception $e) {
                error_log("execHeadlessChromium $url => " . $e->getMessage());
            }
        } else {
            $class = new \UUID();
            return execHeadlessChromium($url, $logName . '~' . $class->v4(), $debug);
        }
    }
    return false;
}

function open_resources_handler()
{
    session_write_close();
    $res = get_resources('stream');
    $resCount = count($res);
    if (PHP_OS_FAMILY === 'Linux' && function_exists('exec')) {
        $resLimit = exec('ulimit -n');
        if ($resLimit) {
            $resLimit = intval($resLimit);
            if ($resCount > $resLimit) {
                $resLimit += 100;
                exec("ulimit -n $resLimit");
                sleep(3);
                open_resources_handler();
            }
        } elseif ($resCount > 900) {
            sleep(3);
            open_resources_handler();
        }
    } elseif ($resCount > 900) {
        sleep(3);
        open_resources_handler();
    }
}

function admin_dir()
{
    session_write_close();
    if (defined('ADMIN_DIR') && !empty(ADMIN_DIR)) return ADMIN_DIR;
    else return 'administrator';
}

function upload_subtitle(array $data = [])
{
    session_write_close();
    if (!empty($data['tmp_name'])) {
        create_dir(BASE_DIR . 'uploads/subtitles');
        $maxSize = 2097152;
        if ($data['size'] <= $maxSize && $data['error'] === UPLOAD_ERR_OK) {
            // user
            $userLogin = current_user();
            if ($userLogin) {
                $uid = $userLogin['id'];
            } else {
                $uid = intval(get_option('public_video_user'));
                $uid = $uid > 0 ? $uid : 1;
            }

            // buat direktori upload subtitles
            $uploadDir = BASE_DIR . 'uploads/subtitles/';
            if (!is_dir($uploadDir)) {
                mkdir($uploadDir, 0755, true);
                chmod($uploadDir, 0755);
            }

            // validasi extensi
            $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
            $allowExt = ['srt', 'vtt', 'ass', 'sub', 'stl', 'dfxp', 'ttml', 'sbv', 'txt'];
            if (in_array($ext, $allowExt)) {
                // tambahkan string random pada nama file
                $name = pathinfo($data['name'], PATHINFO_FILENAME);
                $allowChars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_.';
                $subtitleFile = $uploadDir . substr($name, 0, 200) . '-' . substr(str_shuffle($allowChars), 0, 15) . '.' . $ext;

                // cari dan rename file yang sama
                $i = 0;
                while (file_exists($subtitleFile)) {
                    $i++;
                    $subtitleFile = $uploadDir . $name . '-' . $i . '.' . $ext;
                }

                // simpan file subtitle
                $uploaded = move_uploaded_file($data['tmp_name'], $subtitleFile);
                if ($uploaded) {
                    return BASE_URL . 'uploads/subtitles/' . basename($subtitleFile);
                }
            }
        }
    }
    return FALSE;
}

function upload_poster(array $data = [])
{
    session_write_close();
    if (!empty($data['tmp_name'])) {
        create_dir(BASE_DIR . 'uploads/images');
        $isImage = getimagesize($data['tmp_name']);
        if ($isImage && $data['error'] === UPLOAD_ERR_OK) {
            // buat direktori upload images
            $uploadDir = BASE_DIR . 'uploads/images/';
            if (!is_dir($uploadDir)) {
                mkdir($uploadDir, 0755, true);
                chmod($uploadDir, 0755);
            }

            // tambahkan string random pada nama file
            $name = pathinfo($data['name'], PATHINFO_FILENAME);
            $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
            $allowChars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_.';
            $posterFile = $uploadDir . substr($name, 0, 200) . '-' . substr(str_shuffle($allowChars), 0, 15) . '.' . $ext;

            // cari dan rename file yang sama
            $i = 0;
            while (file_exists($posterFile)) {
                $i++;
                $posterFile = $uploadDir . $name . '-' . $i . '.' . $ext;
            }

            // simpan file poster
            $uploaded = move_uploaded_file($data['tmp_name'], $posterFile);
            if ($uploaded) {
                return BASE_URL . 'uploads/images/' . basename($posterFile);
            }
        }
    }
    return FALSE;
}

function get_load_balancers_ip()
{
    session_write_close();
    $result = array();
    $class = new \LoadBalancers();
    $class->setCriteria('status', 1);
    $list = $class->get(['link']);
    if ($list) {
        $list = array_column($list, 'link');
        foreach ($list as $domain) {
            $result[] = gethostbyname(parse_url($domain, PHP_URL_HOST));
        }
    }
    return $result;
}

function get_load_balancers()
{
    session_write_close();
    $class = new \LoadBalancers();
    $class->setCriteria('status', 1);
    $list = $class->get();
    if ($list) {
        $list = array_column($list, 'link');
        return $list;
    }
    return [];
}

function get_load_balancer_rand(array $hosts = [])
{
    session_write_close();
    $query = '';
    if (!empty($hosts)) {
        foreach ($hosts as $i => $dt) {
            $query .= $i === 0 ? "`disallow_hosts` NOT LIKE '%\"$dt\"%'" : " AND `disallow_hosts` NOT LIKE '%\"$dt\"%'";
        }
        $query = !empty($query) ? 'OR ' . $query : '';
    }
    $class = new \Model();
    $list = $class->rawFetchAll("SELECT `id`, `link` FROM `tb_loadbalancers` WHERE `status` = 1 AND (`disallow_hosts` = '[]' OR `disallow_hosts` = '' OR `disallow_hosts` IS NULL $query)");
    if ($list) {
        if (count($list) > 1) {
            foreach ($list as $i => $row) {
                $list[$i]['count'] = loadBalancers_PBCount($row['id']);
            }
            usort($list, function ($item1, $item2) {
                return $item1['count'] < $item2['count'] ? -1 : 1;
            });
        }
        return rtrim($list[0]['link'], '/') . '/';
    }
    return BASE_URL;
}

function is_load_balancer(string $host = '')
{
    session_write_close();
    if (!empty($host)) {
        if (validate_url($host)) {
            $wwwHost = parse_url($host, PHP_URL_HOST);
            $host = ltrim($wwwHost, 'www.');
        } else {
            $wwwHost = 'www.' . ltrim($host, 'www.');
            $host = ltrim($wwwHost, 'www.');
        }
    } else {
        $wwwHost = 'www.' . ltrim(BASE_URL, 'www.');
        $host = ltrim($wwwHost, 'www.');
    }
    $class = new \LoadBalancers();
    $class->setCriteria('link', "%$wwwHost%", 'LIKE');
    $class->setCriteria('link', "%$host%", 'LIKE', 'OR');
    return $class->getOne();
}

function loadBalancers_PBCount(int $id = 0)
{
    session_write_close();
    $iCache = new \InstanceCache();
    $iCache->setKey('lb_count~' . $id);
    $cache = $iCache->get();
    if ($cache) {
        return (int) $cache;
    } else {
        $class = new \Core();
        $hosts = $class->bypass_host();
        $hostCriteria = '';
        foreach ($hosts as $dt) {
            $hostCriteria .= " `host`='$dt' OR";
        }
        $hostCriteria = '(' . rtrim($hostCriteria, 'OR') . ')';
        $class = new \Model();
        $data = $class->rawQuery("SELECT COUNT(`id`) FROM `tb_videos_sources` WHERE `sid`=$id AND $hostCriteria")->fetchColumn();
        if ($data) {
            $iCache->save($data, 10, 'lb_count');
            return (int) $data;
        }
    }
    return 0;
}

function show_public_balancer()
{
    session_write_close();
    $host = ltrim(parse_url(BASE_URL, PHP_URL_HOST), 'www.');
    $class = new \LoadBalancers();
    $class->setCriteria('status', 1, '=');
    $class->setCriteria('public', 1, '=', 'AND');
    $class->setCriteria('link', "%$host%", 'LIKE', 'AND');
    return $class->getNumRows() > 0;
}

function is_mainsite()
{
    session_write_close();
    $thisHost = ltrim(parse_url(BASE_URL, PHP_URL_HOST), 'www.');
    $mainHost = ltrim(parse_url(get_option('main_site'), PHP_URL_HOST), 'www.');
    return $thisHost === $mainHost;
}

function slugify(string $str = '', string $delimiter = '-')
{
    session_write_close();

    $str = mb_convert_encoding($str, 'UTF-8', mb_list_encodings());

    $options = array(
        'delimiter' => $delimiter,
        'limit' => 50,
        'lowercase' => true,
        'replacements' => array(),
        'transliterate' => true,
    );

    $char_map = array(
        // Decompositions for Latin-1 Supplement
        'ª' => 'a', 'º' => 'o',
        'À' => 'A', 'Á' => 'A',
        'Â' => 'A', 'Ã' => 'A',
        'Ä' => 'A', 'Å' => 'A',
        'Æ' => 'AE', 'Ç' => 'C',
        'È' => 'E', 'É' => 'E',
        'Ê' => 'E', 'Ë' => 'E',
        'Ì' => 'I', 'Í' => 'I',
        'Î' => 'I', 'Ï' => 'I',
        'Ð' => 'D', 'Ñ' => 'N',
        'Ò' => 'O', 'Ó' => 'O',
        'Ô' => 'O', 'Õ' => 'O',
        'Ö' => 'O', 'Ù' => 'U',
        'Ú' => 'U', 'Û' => 'U',
        'Ü' => 'U', 'Ý' => 'Y',
        'Þ' => 'TH', 'ß' => 's',
        'à' => 'a', 'á' => 'a',
        'â' => 'a', 'ã' => 'a',
        'ä' => 'a', 'å' => 'a',
        'æ' => 'ae', 'ç' => 'c',
        'è' => 'e', 'é' => 'e',
        'ê' => 'e', 'ë' => 'e',
        'ì' => 'i', 'í' => 'i',
        'î' => 'i', 'ï' => 'i',
        'ð' => 'd', 'ñ' => 'n',
        'ò' => 'o', 'ó' => 'o',
        'ô' => 'o', 'õ' => 'o',
        'ö' => 'o', 'ø' => 'o',
        'ù' => 'u', 'ú' => 'u',
        'û' => 'u', 'ü' => 'u',
        'ý' => 'y', 'þ' => 'th',
        'ÿ' => 'y', 'Ø' => 'O',
        // Decompositions for Latin Extended-A
        'Ā' => 'A', 'ā' => 'a',
        'Ă' => 'A', 'ă' => 'a',
        'Ą' => 'A', 'ą' => 'a',
        'Ć' => 'C', 'ć' => 'c',
        'Ĉ' => 'C', 'ĉ' => 'c',
        'Ċ' => 'C', 'ċ' => 'c',
        'Č' => 'C', 'č' => 'c',
        'Ď' => 'D', 'ď' => 'd',
        'Đ' => 'D', 'đ' => 'd',
        'Ē' => 'E', 'ē' => 'e',
        'Ĕ' => 'E', 'ĕ' => 'e',
        'Ė' => 'E', 'ė' => 'e',
        'Ę' => 'E', 'ę' => 'e',
        'Ě' => 'E', 'ě' => 'e',
        'Ĝ' => 'G', 'ĝ' => 'g',
        'Ğ' => 'G', 'ğ' => 'g',
        'Ġ' => 'G', 'ġ' => 'g',
        'Ģ' => 'G', 'ģ' => 'g',
        'Ĥ' => 'H', 'ĥ' => 'h',
        'Ħ' => 'H', 'ħ' => 'h',
        'Ĩ' => 'I', 'ĩ' => 'i',
        'Ī' => 'I', 'ī' => 'i',
        'Ĭ' => 'I', 'ĭ' => 'i',
        'Į' => 'I', 'į' => 'i',
        'İ' => 'I', 'ı' => 'i',
        'Ĳ' => 'IJ', 'ĳ' => 'ij',
        'Ĵ' => 'J', 'ĵ' => 'j',
        'Ķ' => 'K', 'ķ' => 'k',
        'ĸ' => 'k', 'Ĺ' => 'L',
        'ĺ' => 'l', 'Ļ' => 'L',
        'ļ' => 'l', 'Ľ' => 'L',
        'ľ' => 'l', 'Ŀ' => 'L',
        'ŀ' => 'l', 'Ł' => 'L',
        'ł' => 'l', 'Ń' => 'N',
        'ń' => 'n', 'Ņ' => 'N',
        'ņ' => 'n', 'Ň' => 'N',
        'ň' => 'n', 'ŉ' => 'n',
        'Ŋ' => 'N', 'ŋ' => 'n',
        'Ō' => 'O', 'ō' => 'o',
        'Ŏ' => 'O', 'ŏ' => 'o',
        'Ő' => 'O', 'ő' => 'o',
        'Œ' => 'OE', 'œ' => 'oe',
        'Ŕ' => 'R', 'ŕ' => 'r',
        'Ŗ' => 'R', 'ŗ' => 'r',
        'Ř' => 'R', 'ř' => 'r',
        'Ś' => 'S', 'ś' => 's',
        'Ŝ' => 'S', 'ŝ' => 's',
        'Ş' => 'S', 'ş' => 's',
        'Š' => 'S', 'š' => 's',
        'Ţ' => 'T', 'ţ' => 't',
        'Ť' => 'T', 'ť' => 't',
        'Ŧ' => 'T', 'ŧ' => 't',
        'Ũ' => 'U', 'ũ' => 'u',
        'Ū' => 'U', 'ū' => 'u',
        'Ŭ' => 'U', 'ŭ' => 'u',
        'Ů' => 'U', 'ů' => 'u',
        'Ű' => 'U', 'ű' => 'u',
        'Ų' => 'U', 'ų' => 'u',
        'Ŵ' => 'W', 'ŵ' => 'w',
        'Ŷ' => 'Y', 'ŷ' => 'y',
        'Ÿ' => 'Y', 'Ź' => 'Z',
        'ź' => 'z', 'Ż' => 'Z',
        'ż' => 'z', 'Ž' => 'Z',
        'ž' => 'z', 'ſ' => 's',
        // Decompositions for Latin Extended-B
        'Ș' => 'S', 'ș' => 's',
        'Ț' => 'T', 'ț' => 't',
        // Euro Sign
        '€' => 'E',
        // GBP (Pound) Sign
        '£' => '',
        // Vowels with diacritic (Vietnamese)
        // unmarked
        'Ơ' => 'O', 'ơ' => 'o',
        'Ư' => 'U', 'ư' => 'u',
        // grave accent
        'Ầ' => 'A', 'ầ' => 'a',
        'Ằ' => 'A', 'ằ' => 'a',
        'Ề' => 'E', 'ề' => 'e',
        'Ồ' => 'O', 'ồ' => 'o',
        'Ờ' => 'O', 'ờ' => 'o',
        'Ừ' => 'U', 'ừ' => 'u',
        'Ỳ' => 'Y', 'ỳ' => 'y',
        // hook
        'Ả' => 'A', 'ả' => 'a',
        'Ẩ' => 'A', 'ẩ' => 'a',
        'Ẳ' => 'A', 'ẳ' => 'a',
        'Ẻ' => 'E', 'ẻ' => 'e',
        'Ể' => 'E', 'ể' => 'e',
        'Ỉ' => 'I', 'ỉ' => 'i',
        'Ỏ' => 'O', 'ỏ' => 'o',
        'Ổ' => 'O', 'ổ' => 'o',
        'Ở' => 'O', 'ở' => 'o',
        'Ủ' => 'U', 'ủ' => 'u',
        'Ử' => 'U', 'ử' => 'u',
        'Ỷ' => 'Y', 'ỷ' => 'y',
        // tilde
        'Ẫ' => 'A', 'ẫ' => 'a',
        'Ẵ' => 'A', 'ẵ' => 'a',
        'Ẽ' => 'E', 'ẽ' => 'e',
        'Ễ' => 'E', 'ễ' => 'e',
        'Ỗ' => 'O', 'ỗ' => 'o',
        'Ỡ' => 'O', 'ỡ' => 'o',
        'Ữ' => 'U', 'ữ' => 'u',
        'Ỹ' => 'Y', 'ỹ' => 'y',
        // acute accent
        'Ấ' => 'A', 'ấ' => 'a',
        'Ắ' => 'A', 'ắ' => 'a',
        'Ế' => 'E', 'ế' => 'e',
        'Ố' => 'O', 'ố' => 'o',
        'Ớ' => 'O', 'ớ' => 'o',
        'Ứ' => 'U', 'ứ' => 'u',
        // dot below
        'Ạ' => 'A', 'ạ' => 'a',
        'Ậ' => 'A', 'ậ' => 'a',
        'Ặ' => 'A', 'ặ' => 'a',
        'Ẹ' => 'E', 'ẹ' => 'e',
        'Ệ' => 'E', 'ệ' => 'e',
        'Ị' => 'I', 'ị' => 'i',
        'Ọ' => 'O', 'ọ' => 'o',
        'Ộ' => 'O', 'ộ' => 'o',
        'Ợ' => 'O', 'ợ' => 'o',
        'Ụ' => 'U', 'ụ' => 'u',
        'Ự' => 'U', 'ự' => 'u',
        'Ỵ' => 'Y', 'ỵ' => 'y',
        // Vowels with diacritic (Chinese, Hanyu Pinyin)
        'ɑ' => 'a',
        // macron
        'Ǖ' => 'U', 'ǖ' => 'u',
        // acute accent
        'Ǘ' => 'U', 'ǘ' => 'u',
        // caron
        'Ǎ' => 'A', 'ǎ' => 'a',
        'Ǐ' => 'I', 'ǐ' => 'i',
        'Ǒ' => 'O', 'ǒ' => 'o',
        'Ǔ' => 'U', 'ǔ' => 'u',
        'Ǚ' => 'U', 'ǚ' => 'u',
        // grave accent
        'Ǜ' => 'U', 'ǜ' => 'u',
    );

    // Make custom replacements
    $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

    // Transliterate characters to ASCII
    if ($options['transliterate']) {
        $str = str_replace(array_keys($char_map), $char_map, $str);
    }

    // Replace non-alphanumeric characters with our delimiter
    $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

    // Remove duplicate delimiters
    $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

    // Truncate slug to max. characters
    $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

    // Remove delimiter from ends
    $str = trim($str, $options['delimiter']);

    return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
}

function get_page_uris()
{
    session_write_close();
    $baseDirPath = trim(parse_url(BASE_URL, PHP_URL_PATH), '/');
    $uri = isset($_SERVER['REQUEST_URI']) ? trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/') : '';
    $uri = ltrim($uri, $baseDirPath);
    $uri = explode('/', $uri);
    $uri = array_filter($uri);
    $uri = array_values($uri);
    return $uri;
}

function get_admin_page()
{
    session_write_close();
    $baseDirPath = trim(parse_url(BASE_URL, PHP_URL_PATH), '/');
    $uri = isset($_SERVER['REQUEST_URI']) ? trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/') : '';
    $uri = ltrim($uri, $baseDirPath);
    $uri = explode('/', $uri);
    $uri = array_filter($uri);
    $uri = array_values($uri);
    if (isset($uri[0]) && $uri[0] === admin_dir()) {
        unset($uri[0]);
        $uri = array_values($uri);
    }
    return implode('/', $uri);
}

function execInBackground(string $script = '', string $program = 'php')
{
    session_write_close();
    if (!empty($script)) {
        $cmd = trim("$program $script");
        if (PHP_OS_FAMILY === 'Windows' && function_exists('popen')) {
            $p = popen("start /B $cmd", 'r');
            sleep(1);
            pclose($p);
        } elseif (function_exists('exec')) {
            exec("nohup $cmd > /dev/null &");
        }
    }
    return false;
}

function clear_html(string $html = '')
{
    session_write_close();
    // Strip HTML Tags
    $clear = strip_tags($html);
    // Clean up things like &amp;
    $clear = html_entity_decode($clear);
    // Strip out any url-encoded stuff
    $clear = urldecode($clear);
    // Replace non alphanumeric-number-dot-slash-underscore characters with space
    $clear = preg_replace('/[^A-Za-z0-9\.\-\_]/', ' ', $clear);
    // Replace Multiple spaces with single space
    $clear = preg_replace('/ +/', ' ', $clear);
    // Trim the string of leading/trailing space
    $clear = trim($clear);
    return $clear;
}

function create_file(string $file = '', string $content = '', string $mode = 'w+')
{
    session_write_close();
    $created = false;
    open_resources_handler();
    $fp = @fopen($file, $mode);
    if ($fp) {
        if (flock($fp, LOCK_EX)) {
            $created = fwrite($fp, $content, strlen($content));
            fflush($fp);
            flock($fp, LOCK_UN);
        } else {
            error_log("create_file $file => cannot lock the file");
        }
        fclose($fp);
    } else {
        error_log("create_file $file => cannot open the file");
    }
    return $created;
}

function create_dir(string $dir = '')
{
    session_write_close();
    if (!is_dir($dir) || !is_writable($dir) || !file_exists($dir)) return @mkdir($dir, 0755, true);
}

function create_htaccess(string $dir = '')
{
    session_write_close();
    create_dir($dir);
    return create_file(rtrim($dir, '/') . '/.htaccess', 'deny from all', 'x');
}

function create_alert(string $type = '', string $message = '', string $header = '')
{
    session_write_close();
    setcookie('adm-type', $type, time() + 3600, '/');
    setcookie('adm-message', strip_tags($message), time() + 3600, '/');
    if (!empty($header)) {
        header('location: ' . $header);
        return;
    }
}

function show_alert()
{
    session_write_close();
    if (!empty($_COOKIE['adm-message'])) {
        $type = isset($_COOKIE['adm-type']) ? strtolower($_COOKIE['adm-type']) : 'primary';
        $message = $_COOKIE['adm-message'];

        unset($_COOKIE['adm-message']);
        unset($_COOKIE['adm-type']);

        $now = time();
        setcookie('adm-message', null, $now, '/');
        setcookie('adm-type', null, $now, '/');

        return "<div class='alert alert-$type'>$message</div>";
    }
}

function deleteDir(string $dir = '')
{
    session_write_close();
    if (is_dir($dir)) {
        $scandir = @scandir($dir);
        if ($scandir) {
            $files = array_diff($scandir, array('.', '..'));
            if (!empty($files)) {
                foreach ($files as $file) {
                    if (is_dir("$dir/$file")) deleteDir("$dir/$file");
                    elseif (is_file("$dir/$file")) @unlink("$dir/$file");
                }
                return @rmdir($dir);
            }
        }
    }
    return false;
}

function auto_clear_expired_caches()
{
    session_write_close();
    $iCache = new \InstanceCache();
    $class = new \VideoSources();
    $now = time();
    $class->setCriteria('expired', $now, '<=');
    $list = $class->get(['host', 'host_id', 'dl']);
    if ($list) {
        foreach ($list as $data) {
            $iCache->delete('video_sources~' . $data['host'] . '~' . keyFilter($data['host_id']) . '~' . $data['dl']);
        }
    }
    $class->setCriteria('expired', $now, '<=');
    $class->delete();
}

function hex2RGB($hexStr = null, bool $returnAsString = false, string $seperator = ',')
{
    session_write_close();
    if (!empty($hexStr)) {
        $hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
        $rgbArray = array();
        if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
            $colorVal = hexdec($hexStr);
            $rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
            $rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
            $rgbArray['blue'] = 0xFF & $colorVal;
        } elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
            $rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
            $rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
            $rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
        } else {
            return false; //Invalid hex color code
        }
        return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray; // returns the rgb string or the associative array
    }
    return false;
}

function convertToDurationTimeFormat(int $seconds = 0)
{
    session_write_close();
    $hours = intval($seconds / 3600);
    $mins = intval($seconds / 60 % 60);
    $secs = intval($seconds % 60);
    return sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
}

function get_vast()
{
    session_write_close();
    $disabled = get_option('disable_vast_ads');
    if (!filter_var($disabled, FILTER_VALIDATE_BOOLEAN)) {
        $vast_client = get_option('vast_client');
        $vast_xml = get_option('vast_xml');
        $vast_offset = get_option('vast_offset');
        $vast_skip = intval(get_option('vast_skip'));
        if (!empty($vast_xml) && !empty($vast_offset)) {
            $vast_xml = is_array($vast_xml) ? $vast_xml : json_decode($vast_xml, true);
            $vast_xml = array_filter($vast_xml, function ($val) {
                return !is_null($val) && $val !== '';
            });
            $vast_xml = array_values($vast_xml);
            $cVast = count($vast_xml);

            $vast_offset = is_array($vast_offset) ? $vast_offset : json_decode($vast_offset, true);
            $vast_offset = array_filter($vast_offset, function ($val) {
                return !is_null($val) && $val !== '';
            });
            $vast_offset = array_values($vast_offset);
            $cOffset = count($vast_offset);

            $schedule = [];
            if ($cVast > 0 && $cVast === $cOffset) {
                foreach ($vast_xml as $i => $vast) {
                    if (is_numeric($vast_offset[$i])) {
                        $offset = convertToDurationTimeFormat($vast_offset[$i]);
                    } else {
                        $offset = strtr($vast_offset[$i], ['start' => 'preroll', 'end' => 'postroll']);
                    }
                    $schedule[] = [
                        'tag' => $vast,
                        'offset' => $offset
                    ];
                }
                return [
                    'client' => $vast_client,
                    'schedule' => $schedule,
                    'skipoffset' => $vast_skip,
                    'skipmessage' => 'Skip XX',
                    'conditionaladoptout' => true,
                    'creativeTimeout' => 5000,
                    'loadVideoTimeout' => 5000,
                    'vastLoadTimeout' => 5000,
                    'placement' => 'interstitial',
                    'preloadAds' => true
                ];
            }
        }
    }
    return [];
}

function migrate(string $sourceDir = '', string $destinationDir = '')
{
    session_write_close();
    try {
        if (is_dir($sourceDir)) {
            open_resources_handler();
            $list = new \DirectoryIterator($sourceDir);
            foreach ($list as $file) {
                if (!$file->isDot() && $file->isFile()) {
                    @copy($sourceDir . '/' . $file->getFilename(), $destinationDir . '/' . $file->getFilename());
                }
            }
            return deleteDir($sourceDir);
        }
    } catch (\Exception $e) {
        error_log("migrate $sourceDir => " . $e->getMessage());
    }
    return FALSE;
}

function keyFilter($key)
{
    session_write_close();
    if (!empty($key)) return preg_replace('/[^A-Za-z0-9\-_\~]/', '', strtr($key, [' ' => '-']));
    else return $key;
}

function encode(string $data = '')
{
    session_write_close();
    try {
        $class = new \InstanceCache();
        $class->setKey('encode' . SECURE_SALT . '~' . hash('SHA256', $data));
        $cache = $class->get();
        if ($cache) return $cache;

        // Remove the base64 encoding from our key
        $secret_key = @base64_decode(SECURE_SALT);
        // Generate an initialization vector
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-128-cbc'));
        // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
        $encrypted = @openssl_encrypt($data, 'aes-128-cbc', $secret_key, 0, $iv);
        // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
        $encrypted = rawurlencode(base64_encode($encrypted . '::' . $iv));

        $class->save($encrypted, 86400, 'encode');

        return $encrypted;
    } catch (\Exception $e) {
        error_log('encode ' . $data . ' => ' . $e->getMessage());
    }
    return FALSE;
}

function decode(string $data = '')
{
    session_write_close();
    try {
        // Remove the base64 encoding from our key
        $secret_key = @base64_decode(SECURE_SALT);
        // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
        list($encrypted_data, $iv) = array_pad(explode('::', base64_decode(rawurldecode($data))), 2, '');
        $decode = @openssl_decrypt($encrypted_data, 'aes-128-cbc', $secret_key, 0, $iv);

        return $decode;
    } catch (\Exception $e) {
        error_log('decode => ' . $e->getMessage());
    }
    return FALSE;
}

function validate_url($url)
{
    session_write_close();
    if (!empty($url)) {
        $url = strtr($url, [' ' => '%20']);
        $path = parse_url($url, PHP_URL_PATH);
        $encoded_path = array_map('urlencode', explode('/', $path));
        $url = str_replace($path, implode('/', $encoded_path), $url);
        return filter_var($url, FILTER_VALIDATE_URL);
    }
    return false;
}

function files_identical($fn1, $fn2)
{
    session_write_close();
    if (filetype($fn1) !== filetype($fn2)) {
        return FALSE;
    } elseif (filesize($fn1) !== filesize($fn2)) {
        return FALSE;
    }

    open_resources_handler();
    $fp1 = @fopen($fn1, 'rb');
    if (!$fp1) return FALSE;

    open_resources_handler();
    $fp2 = @fopen($fn2, 'rb');
    if (!$fp2) return FALSE;

    $same = TRUE;
    stream_set_blocking($fp1, false);
    stream_set_blocking($fp2, false);
    while (!feof($fp1) && !feof($fp2)) {
        if (fread($fp1, 4096) !== fread($fp2, 4096)) {
            $same = FALSE;
            break;
        }
    }

    if (feof($fp1) !== feof($fp2)) {
        $same = FALSE;
    }

    fclose($fp1);
    fclose($fp2);

    return $same;
}

function isSSL()
{
    session_write_close();
    if (isset($_SERVER['HTTP_CF_VISITOR'])) {
        $visitor = json_decode($_SERVER['HTTP_CF_VISITOR'], true);
        return $visitor['scheme'] === 'https';
    } elseif (isset($_SERVER['HTTPS'])) {
        return (bool) $_SERVER['HTTPS'];
    }
    return FALSE;
}

function sitename()
{
    session_write_close();
    $sitename = get_option('site_name');
    if (!empty($sitename)) return $sitename;
    return 'HopPhim';
}

function adminTokenValidation(string $token = '')
{
    session_write_close();
    $data = loginTokenValidation($token);
    if ($data) {
        $class = new \Users();
        $class->setCriteria('user', $data['username'], '=');
        $class->setCriteria('status', 1, '=', 'AND');
        $class->setCriteria('role', 0, '=', 'AND');
        $data = (int) $class->getNumRows();
        return $data > 0;
    }
    return FALSE;
}

function loginTokenValidation(string $token = '')
{
    session_write_close();
    $class = new \Sessions();
    $class->setCriteria('token', $token, '=');
    $class->setCriteria('expired', time(), '>', 'AND');
    $class->setCriteria('stat', 9, '<>', 'AND');
    $data = $class->getOne(['ip', 'username']);
    if ($data && getUserIPASN(getUserIP()) === getUserIPASN($data['ip'])) {
        return $data;
    }
    return false;
}

function userValidation(string $username = '')
{
    session_write_close();
    $class = new \Users();
    $class->setCriteria('user', $username, '=');
    $class->setCriteria('email', $username, '=', 'OR');
    $class->setCriteria('status', 1, '=', 'AND');
    return $class->getOne();
}

function user_roles($selectIndex = null)
{
    $roles = ['Admin', 'User', 'Premium'];
    if (!is_null($selectIndex) && isset($roles[$selectIndex])) {
        $selectIndex = intval($selectIndex);
        return $roles[$selectIndex];
    } else {
        return FALSE;
    }
    return $roles;
}

function current_user()
{
    session_write_close();
    $login = new \Login();
    return $login->cek_login();
}

function is_admin()
{
    session_write_close();
    $user = current_user();
    return $user && intval($user['role']) === 0;
}

function is_public()
{
    session_write_close();
    $user = current_user();
    $public_user = intval(get_option('public_video_user'));
    return $user || ($user && intval($user['id']) === $public_user);
}

function is_anonymous()
{
    session_write_close();
    $anonymous = get_option('anonymous_generator');
    return filter_var($anonymous, FILTER_VALIDATE_BOOLEAN);
}

function get_option(string $key = '')
{
    session_write_close();
    $class = new \Settings();
    if (!empty($key)) {
        $excludeCache = ['cache_instance', 'updated'];
        if (in_array($key, $excludeCache)) {
            $class->setCriteria('key', $key);
            $data = $class->getOne(['value']);
            if ($data) {
                return $data['value'];
            }
        } else {
            $iCache = new \InstanceCache();
            $iCache->setKey('opt_' . $key);
            $cache = $iCache->get();
            if ($cache) {
                return $cache;
            } else {
                $class->setCriteria('key', $key);
                $data = $class->getOne(['value']);
                if ($data) {
                    $iCache->save($data['value'], 2592000, 'options');
                    return $data['value'];
                }
            }
        }
    } else {
        $data = [];
        $list = $class->get(['key', 'value']);
        if ($list) {
            foreach ($list as $row) {
                $data[$row['key']] = $row['value'];
            }
        }
        return $data;
    }
}

function set_option(string $key = '', $value = null)
{
    session_write_close();
    if (!empty($key)) {
        $iCache = new \InstanceCache();
        $iCache->setKey('opt_' . $key);
        $iCache->delete();

        $excludeCache = ['cache_instance', 'updated'];

        $now = time();
        $class = new \Settings();
        $class->setCriteria('key', $key);
        if ($class->getNumRows() > 0) {
            if (!in_array($key, $excludeCache)) $iCache->save($value, 2592000, 'options');

            $class->setCriteria('key', $key);
            $value = is_array($value) ? json_encode($value, TRUE) : $value;
            return $class->update(array(
                'value' => $value,
                'updated' => $now
            ));
        } else {
            if (!in_array($key, $excludeCache)) $iCache->save($value, 2592000, 'options');

            return $class->insert(array(
                'key' => $key,
                'value' => $value,
                'updated' => $now
            ));
        }
    }
}

function remove_option(string $key = '')
{
    session_write_close();
    if (!empty($key)) {
        // delete database
        $class = new \Settings();
        $class->setCriteria('key', $key);
        $deleted[] = $class->delete();

        // delete cache
        $class = new \InstanceCache();
        $class->setKey('opt_' . $key);
        $deleted[] = $class->delete();

        return in_array(true, $deleted);
    }
    return false;
}

function get_host_count(string $host = '')
{
    session_write_close();
    $host = htmlspecialchars($host);
    $class = new \Videos();
    $class->setCriteria('host', "$host", '=');
    $class->setCriteria('ahost', "$host", '=', 'OR');
    return $class->getNumRows();
}

function get_host_status(string $host = '', bool $html = FALSE)
{
    session_write_close();
    $disabled_hosts = get_option('disable_host');
    $disabled = false;
    if (!empty($disabled_hosts)) {
        $disabled_hosts = is_array($disabled_hosts) ? $disabled_hosts : json_decode($disabled_hosts, TRUE);
        $disabled = in_array($host, $disabled_hosts);
    }
    if ($html) {
        if ($disabled) {
            return '<span class="text-danger"><i aria-hidden="true" class="fas fa-ban fa-lg"></i><span class="ml-1">Disabled</span></span>';
        } else {
            return '<span class="text-success"><i aria-hidden="true" class="fas fa-check-circle fa-lg"></i><span class="ml-1">Working</span></span>';
        }
    } else {
        return $disabled;
    }
    return FALSE;
}

function rate_limit_ips()
{
    session_write_close();
    $proxies = [
        '173.245.48.0/20',
        '103.21.244.0/22',
        '103.22.200.0/22',
        '103.31.4.0/22',
        '141.101.64.0/18',
        '108.162.192.0/18',
        '190.93.240.0/20',
        '188.114.96.0/20',
        '197.234.240.0/22',
        '198.41.128.0/17',
        '162.158.0.0/15',
        '104.16.0.0/13',
        '104.24.0.0/14',
        '172.64.0.0/13',
        '131.0.72.0/22'
    ];
    $proxies = array_merge($proxies, proxy_list());
    $key = array_rand($proxies);
    if (strpos($proxies[$key], '/') !== false) {
        list($first, $len) = array_pad(explode('/', $proxies[$key]), 2, '');
        $ip = explode('.', $first);
        array_pop($ip);
        $last = rand(0, $len);
        $ip[] = $last;
        return implode('.', $ip);
    } else {
        list($ip, $trash) = array_pad(explode(',', $proxies[$key]), 2, '');
        list($ip, $port) = array_pad(explode(':', $ip), 2, '');
        return $ip;
    }
}

function proxy_list()
{
    session_write_close();
    return explode("\n", trim(strtr(get_option('proxy_list'), ["\r\n" => "\n"])));
}

function unused_proxy_list()
{
    session_write_close();
    return explode("\n", trim(strtr(get_option('unused_proxy_list'), ["\r\n" => "\n"])));
}

function proxy_rotator()
{
    session_write_close();
    $proxyDisabled = filter_var(get_option('disable_proxy'), FILTER_VALIDATE_BOOLEAN);
    if (!$proxyDisabled) {
        $proxyList = proxy_list();
        if (!empty($proxyList)) {
            $key = array_rand($proxyList);
            $format = trim($proxyList[$key]);
            $ex = array_pad(explode(',', $format), 3, '');

            $proxy = trim($ex[0]);
            if (!empty($proxy)) {
                $curl_types = [CURLPROXY_HTTP, CURLPROXY_HTTP_1_0, CURLPROXY_HTTPS, CURLPROXY_SOCKS4, CURLPROXY_SOCKS5, CURLPROXY_SOCKS4A];
                $types = ['http', 'http1.0', 'https', 'socks4', 'socks5', 'socks4a'];
                $type = CURLPROXY_HTTP;
                $usrpwd = '';

                if (!empty($ex[1])) {
                    $userpass_OR_type = strtolower($ex[1]);
                    if (in_array($userpass_OR_type, $types)) {
                        $key = array_search($userpass_OR_type, $types);
                        if ($key) $type = $curl_types[$key];
                    } else {
                        $usrpwd = $ex[1];
                    }
                }
                if (!empty($ex[2])) {
                    $type = array_search(strtolower($ex[2]), $types);
                }

                return [
                    'format' => $format,
                    'proxy' => $proxy,
                    'type'  => $type,
                    'usrpwd' => $usrpwd
                ];
            }
        }
    }
    return FALSE;
}

function getDriveId(string $url = '')
{
    session_write_close();
    if (validate_url($url)) {
        $query = parse_url($url, PHP_URL_QUERY);
        parse_str($query, $qry);
        if (!empty($qry['id'])) {
            return $qry['id'];
        } elseif (preg_match('/d\/([^"]+)\//', $url, $fileid)) {
            return $fileid[1];
        } elseif (preg_match('/files\/([^"]+)/', $url, $fileid)) {
            $fileid = explode('?', $fileid[1]);
            return $fileid[0];
        }
    } else {
        return $url;
    }
}

function get_string_between($string, $start, $end)
{
    session_write_close();
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

function getUserIP()
{
    session_write_close();
    if (isset($_SERVER["HTTP_TRUE_CLIENT_IP"])) {
        // to get shared cloudflare IP address
        $ip = $_SERVER["HTTP_TRUE_CLIENT_IP"];
    } elseif (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
        // to get shared cloudflare IP address
        $ip = $_SERVER["HTTP_CF_CONNECTING_IP"];
    } elseif (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        // to get shared ISP IP address
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ipAddressList = array_values(array_filter(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']), function ($ip) {
            return filter_var($ip, FILTER_VALIDATE_IP);
        }));
        $ip = isset($ipAddressList[0]) ? $ipAddressList[0] : $_SERVER['REMOTE_ADDR'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED'];
    } elseif (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_FORWARDED_FOR'];
    } elseif (!empty($_SERVER['HTTP_FORWARDED'])) {
        $ip = $_SERVER['HTTP_FORWARDED'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    if (filter_var($ip, FILTER_VALIDATE_IP)) {
        return $ip;
    } else {
        return FALSE;
    }
}

function getUserIPASN(string $ip = '')
{
    session_write_close();
    if (filter_var($ip, FILTER_VALIDATE_IP)) {
        $class = new \InstanceCache();
        $class->setKey('ASN~' . $ip);
        $cache = $class->get();
        if ($class) {
            return (int) $cache;
        } elseif (file_exists(BASE_DIR . 'includes/bin/maxmind/asn.mmdb')) {
            try {
                $reader = new \GeoIp2\Database\Reader(BASE_DIR . 'includes/bin/maxmind/asn.mmdb');
                $record = $reader->asn($ip);
                $info = $record->jsonSerialize();
                $ASN = (int) $info['autonomous_system_number'];
                $class->save($ASN, 604800, 'ASN_DB');
                return $ASN;
            } catch (\GeoIp2\Exception\GeoIp2Exception | \Exception $e) {
                error_log('getUserIPASN ' . $ip . ' => ' . $e->getMessage());
            }
        }
    }
    return FALSE;
}

function updateMaxMindGeoIP()
{
    session_write_close();
    $license = get_option('maxmind_license_key');
    if (!empty($license) && date('l') === 'Tuesday') {
        $updated = get_option('maxmind_updated');
        $today = date('Y-m-d');
        if ($updated !== $today) {
            create_dir(BASE_DIR . 'includes/bin/maxmind/');

            open_resources_handler();
            $fp = @fopen(BASE_DIR . 'includes/bin/maxmind/maxmind.tar.gz', 'wb');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-ASN&license_key=$license&suffix=tar.gz");
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FAILONERROR, 1);
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);
            fclose($fp);

            if (!$err) {
                if (file_exists(BASE_DIR . 'includes/bin/maxmind/maxmind.tar.gz')) {
                    $class = new \PharData(BASE_DIR . 'includes/bin/maxmind/maxmind.tar.gz');
                    $done = $class->decompress();
                    if ($done) {
                        @unlink(BASE_DIR . 'includes/bin/maxmind/maxmind.tar.gz');
                        if (file_exists(BASE_DIR . 'includes/bin/maxmind/maxmind.tar')) {
                            $class = new \PharData(BASE_DIR . 'includes/bin/maxmind/maxmind.tar');
                            $done = $class->extractTo(BASE_DIR . 'includes/bin/maxmind');
                            if ($done) {
                                @unlink(BASE_DIR . 'includes/bin/maxmind/maxmind.tar');
                                open_resources_handler();
                                $list = new \DirectoryIterator(BASE_DIR . 'includes/bin/maxmind');
                                foreach ($list as $dt) {
                                    if (!$dt->isDot() && $dt->isDir()) {
                                        $dir = BASE_DIR . 'includes/bin/maxmind/' . $dt->getFilename();
                                        open_resources_handler();
                                        $list = new \DirectoryIterator($dir);
                                        foreach ($list as $dt) {
                                            if (!$dt->isDot() && $dt->isFile() && strpos($dt->getFilename(), '.mmdb') !== FALSE) {
                                                $done = @rename($dir . '/' . $dt->getFilename(), $dir . '/' . 'asn.mmdb');
                                                if ($done) {
                                                    $done = @copy($dir . '/' . 'asn.mmdb', BASE_DIR . 'includes/bin/maxmind/asn.mmdb');
                                                    if ($done) {
                                                        set_option('maxmind_updated', $today);
                                                        deleteDir($dir);
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

function domain_whitelisted(): array
{
    session_write_close();
    $list = array();
    $domains = get_option('domain_whitelisted');
    if (!empty($domains)) {
        $list = explode("\n", strtr(trim($domains), ['www.' => '', "\r\n" => "\n"]));
        $thisSite = ltrim(parse_url(BASE_URL, PHP_URL_HOST), 'www.');
        $mainSite = ltrim(parse_url(get_option('main_site'), PHP_URL_HOST), 'www.');
        $list = array_merge($list, array($thisSite, $mainSite));
        $list = array_unique($list);
        $list = array_values($list);
        return $list;
    }
    return $list;
}

function is_domain_whitelisted(string $referer = ''): bool
{
    session_write_close();
    $domains = domain_whitelisted();
    if (!empty($domains) && !empty($referer)) {
        $domains = array_merge($domains, array('bit.ly', 'adf.ly', 'clk.sh', 'iir.ai', 'adtival.network', 'apk.miuiku.com', 'cutpaid.com', 'ouo.io', 'apk.sekilastekno.com', 'cararegistrasi.com', 'paypou.com', 'tiddis.net'));
        $domain = parse_url($referer, PHP_URL_HOST);
        $domain = ltrim($domain, 'www.');
        return in_array($domain, $domains);
    } else {
        return TRUE;
    }
    return FALSE;
}

function is_ip_whitelisted(string $ip = ''): bool
{
    session_write_close();
    $domains = domain_whitelisted();
    if (!empty($domains)) {
        if (filter_var($ip, FILTER_VALIDATE_IP)) {
            $domains = array_merge($domains, get_load_balancers_ip());
            $domains = array_filter($domains, function ($v) {
                return filter_var($v, FILTER_VALIDATE_IP);
            });
            $domains = array_values($domains);
            return in_array($ip, $domains);
        }
    } else {
        return TRUE;
    }
    return FALSE;
}

function domain_blacklisted(): array
{
    session_write_close();
    $list = array();
    $domains = get_option('domain_blacklisted');
    if (!empty($domains)) {
        $list = explode("\n", strtr(trim($domains), ['www.' => '', "\r\n" => "\n"]));
    }
    return $list;
}

function is_domain_blacklisted(string $referer = ''): bool
{
    session_write_close();
    $domains = domain_blacklisted();
    if (!empty($domains) && !empty($referer)) {
        $domain = validate_url($referer) ? parse_url($referer, PHP_URL_HOST) : $referer;
        $domain = ltrim($domain, 'www.');
        return in_array($domain, $domains);
    }
    return FALSE;
}

function is_ip_blacklisted(string $ip = ''): bool
{
    session_write_close();
    $domains = domain_blacklisted();
    if (!empty($domains) && !empty($ip) && filter_var($ip, FILTER_VALIDATE_IP)) {
        $domains = array_filter($domains, function ($v) {
            return filter_var($v, FILTER_VALIDATE_IP);
        });
        $domains = array_values($domains);
        return in_array($ip, $domains);
    }
    return FALSE;
}

function is_referer_blacklisted(string $referer = ''): bool
{
    session_write_close();
    $links = get_option('link_blacklisted');
    if (!empty($links) && !empty($referer)) {
        $replace = ['https://' => '', 'http://' => '', 'www.' => ''];
        $links = trim(strtr($links, $replace));
        $referer = trim(strtr($referer, $replace));
        return strpos($links, $referer) !== FALSE ? TRUE : FALSE;
    }
    return FALSE;
}

function is_title_blacklisted(string $title = ''): bool
{
    session_write_close();
    if (!empty($title)) {
        $title = strtolower($title);
        $words = strtolower(get_option('word_blacklisted'));
        if (!empty($words)) {
            $words = explode("\n", strtr($words, ["\r\n" => "\n"]));
            $words = array_filter($words);
            $words = array_unique($words);
            $words = array_values($words);
            $result = FALSE;
            foreach ($words as $word) {
                if ((is_numeric($title) && is_numeric($word) && $title == $word) || (!is_numeric($word) && strpos($title, $word) !== FALSE)) {
                    $result = TRUE;
                    break;
                }
            }
            return $result;
        }
    }
    return FALSE;
}

function accessValidation()
{
    session_write_close();
    $ip = getUserIP();
    $referer = '';
    if (!empty($_SERVER['HTTP_REFERER'])) {
        $referer = $_SERVER['HTTP_REFERER'];
    } elseif (!empty($_SERVER['HTTP_ORIGIN'])) {
        $referer = $_SERVER['HTTP_ORIGIN'];
    }
    $allowDomain = is_domain_whitelisted($referer);
    $allowIPAddress = is_ip_whitelisted($ip);
    $disallowDomain = is_domain_blacklisted($referer);
    $disallowIPAddress = is_ip_blacklisted($ip);
    $disallowReferer = is_referer_blacklisted($referer);
    return ($allowDomain || $allowIPAddress) && !$disallowIPAddress && !$disallowDomain && !$disallowReferer;
}

function cors_policy($token = null)
{
    session_write_close();
    $origin = !empty($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
    $referer = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $origin;
    $disableValidation = get_option('disable_validation') === 'true';
    header('Referrer-Policy: same-origin, no-referrer, strict-origin-when-cross-origin');
    if ($disableValidation) {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Credentials: false');
        return $disableValidation;
    } elseif (!empty($token)) {
        if (isset($_SERVER['HTTP_USER_AGENT']) && is_smartTV($_SERVER['HTTP_USER_AGENT'])) {
            $class = new \WhichBrowser\Parser($_SERVER['HTTP_USER_AGENT']);
            $tvToken = hash('SHA256', $class->device->manufacturer . '~' . (isset($class->device->series) ? $class->device->series . '~' : '') . (isset($class->os->name) ? $class->os->name . '~' : '') . $class->engine->name);
            if ($token === $tvToken) {
                header('Access-Control-Allow-Origin: *');
                header('Access-Control-Allow-Credentials: false');
                return true;
            }
        } else {
            $userIP = getUserIP();
            $ipToken = decode($token);
            if ($ipToken) {
                header('Access-Control-Allow-Origin: *');
                header('Access-Control-Allow-Credentials: false');
                return $ipToken === $userIP || getUserIPASN($userIP) === getUserIPASN($ipToken);
            } else {
                $urlParser = parse_url($referer);
                if (isset($urlParser['scheme']) && isset($urlParser['host'])) {
                    $scheme = $urlParser['scheme'];
                    $host = $urlParser['host'];
                    header("Access-Control-Allow-Origin: $scheme://$host");
                    header('Access-Control-Allow-Credentials: true');
                    header('Vary: Origin');
                }
                return adminTokenValidation($token);
            }
        }
    }
    return FALSE;
}

function recaptcha_validate(string $captcha = '')
{
    session_write_close();
    $secretKey = get_option('recaptcha_secret_key');
    if (!empty($secretKey)) {
        if (!empty($captcha)) {
            $url = 'https://www.google.com/recaptcha/api/siteverify';
            $data = array(
                'secret' => $secretKey,
                'response' => $captcha,
                'remoteip' => getUserIP()
            );

            $scheme = parse_url($url, PHP_URL_SCHEME);
            $host = parse_url($url, PHP_URL_HOST);
            $port = parse_URL($url, PHP_URL_PORT);
            if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
            $ipv4 = gethostbyname($host);
            $resolveHost = implode(':', array($host, $port, $ipv4));

            open_resources_handler();
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
            curl_setopt($ch, CURLOPT_ENCODING, '');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_RESOLVE, array($resolveHost));
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch, CURLOPT_TCP_NODELAY, 1);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

            $response = curl_exec($ch);
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            if ($status >= 200 && $status < 400) {
                $json = json_decode($response, TRUE);
                return (bool) $json['success'];
            }
        }
    } else {
        return TRUE;
    }
    return FALSE;
}

function random_string(int $length = 5)
{
    session_write_close();
    return substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_'), 0, $length);
}

function subtitle_languages(string $name = '', string $selected = '', bool $required = FALSE, bool $json = FALSE)
{
    session_write_close();
    $languages = [
        "Default",
        "Afrikanns",
        "Albanian",
        "Arabic",
        "Armenian",
        "Basque",
        "Bengali",
        "Bulgarian",
        "Catalan",
        "Cambodian",
        "Chinese",
        "Croatian",
        "Czech",
        "Danish",
        "Dutch",
        "English",
        "Estonian",
        "Fiji",
        "Finnish",
        "French",
        "Georgian",
        "German",
        "Greek",
        "Gujarati",
        "Hebrew",
        "Hindi",
        "Hungarian",
        "Icelandic",
        "Indonesian",
        "Irish",
        "Italian",
        "Japanese",
        "Javanese",
        "Korean",
        "Latin",
        "Latvian",
        "Lithuanian",
        "Macedonian",
        "Malay",
        "Malayalam",
        "Maltese",
        "Maori",
        "Marathi",
        "Mongolian",
        "Nepali",
        "Norwegian",
        "Persian",
        "Polish",
        "Portuguese",
        "Punjabi",
        "Quechua",
        "Romanian",
        "Russian",
        "Samoan",
        "Serbian",
        "Slovak",
        "Slovenian",
        "Spanish",
        "Swahili",
        "Swedish",
        "Tamil",
        "Tatar",
        "Telugu",
        "Thai",
        "Tibetan",
        "Tonga",
        "Turkish",
        "Ukranian",
        "Urdu",
        "Uzbek",
        "Vietnamese",
        "Welsh",
        "Xhosa"
    ];
    if (!$json) {
        $id = get_string_between($name, '[', ']');
        $id = !empty($id) ? $id : strtr($name, ['[]' => '-']) . random_string();
        $html = '<select name="' . $name . '" id="' . $id . '" class="form-control" ' . ($required ? 'required' : '') . '>';
        foreach ($languages as $lang) {
            $html .= '<option value="' . $lang . '" ' . ($selected === $lang ? 'selected' : '') . '>' . $lang . '</option>';
        }
        $html .= '</select>';
        return $html;
    }
    return json_encode($languages);
}

function earnmoney_website(string $id = '', string $selected = '', bool $required = FALSE, bool $json = FALSE)
{
    session_write_close();
    $website = [
        "random" => "Random",
        "adf.ly" => "AdFly",
        "adtival.network" => "Adtival Network",
        "clk.sh" => "Clk.sh",
        "cutpaid.com" => "Cutpaid",
        "ouo.io" => "ouo.io",
        "shrinkads.com" => "Shrink Ads (Safelink Blog)",
        "safelinku.com" => "SafelinkU",
        "wi.cr" => "Wicr!",
        "ylinkz.com" => "YLinkz"
    ];
    if (!$json) {
        $html = '<select name="opt[' . $id . ']" id="' . $id . '" class="form-control" ' . ($required ? 'required' : '') . '>';
        foreach ($website as $key => $value) {
            $html .= '<option value="' . $key . '" ' . ($selected === $key ? 'selected' : '') . '>' . $value . '</option>';
        }
        $html .= '</select>';
        return $html;
    }
    return json_encode($website, true);
}

function earnmoney_link(string $link = '', string $provider = '')
{
    session_write_close();
    if (!empty($link) && !empty($provider)) {
        $apikey = get_option('additional_url_shortener_' . $provider);
        if (!empty($apikey)) {
            $link = rawurlencode($link);
            switch ($provider) {
                case 'ylinkz.com':
                    return 'https://ylinkz.com/st?api=' . $apikey . '&url=' . $link;
                    break;
                case 'ouo.io':
                    return 'http://ouo.io/qs/' . $apikey . '?s=' . $link;
                    break;
                case 'safelinku.com':
                    return 'https://semawur.com/full/?type=2&api=' . $apikey . '&url=' . $link;
                    break;

                case 'adtival.network':
                    return 'https://www.adtival.network/st?api=' . $apikey . '&url=' . $link;
                    break;

                case 'safelinkblog.com':
                    return 'https://www.shrinkads.com/st?api=' . $apikey . '&url=' . $link;
                    break;

                case 'shrinkads.com':
                    return 'https://www.shrinkads.com/st?api=' . $apikey . '&url=' . $link;
                    break;

                case 'noyads.com':
                    return 'https://noyads.com/st?api=' . $apikey . '&url=' . $link;
                    break;

                case 'safelinkblogger.com':
                    return 'https://safelinkblogger.com/st?api=' . $apikey . '&url=' . $link;
                    break;

                case 'shrink.world':
                    return 'https://shrink.world/st?api=' . $apikey . '&url=' . $link;
                    break;

                case 'shorten-link.com':
                    return 'https://shorten-link.com/st?api=' . $apikey . '&url=' . $link;
                    break;

                case 'shortzon.com':
                    return 'https://shortzon.com/st?api=' . $apikey . '&url=' . $link;
                    break;

                case 'cutpaid.com':
                    return 'https://cutpaid.com/st?api=' . $apikey . '&url=' . $link;
                    break;

                case 'wi.cr':
                    return 'https://wi.cr/st?api=' . $apikey . '&url=' . $link;
                    break;

                case 'adf.ly':
                    return 'http://adf.ly/' . $apikey . '/' . $link;
                    break;

                case 'clk.sh':
                    return 'https://clk.sh/st?api=' . $apikey . '&url=' . $link;
                    break;

                case 'l2s.pet':
                    return 'https://l2s.pet/st?api=' . $apikey . '&url=' . $link;
                    break;

                default:
                    return rawurldecode($link);
                    break;
            }
        }
    }
    return $link;
}

function bitly_link(string $longurl = '')
{
    session_write_close();
    $token = get_option('main_url_shortener');
    if (!empty($token)) {
        $url = 'https://api-ssl.bitly.com/v4/bitlinks';
        $scheme = parse_url($url, PHP_URL_SCHEME);
        $host = parse_url($url, PHP_URL_HOST);
        $port = parse_URL($url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host . '.');
        $resolveHost = implode(':', array($host, $port, $ipv4));

        open_resources_handler();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['long_url' => $longurl]));
        curl_setopt($ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_TCP_NODELAY, 1);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer " . $token,
            "Content-Type: application/json"
        ]);
        curl_setopt($ch, CURLOPT_HEADERFUNCTION, function ($ch, $line) {
            session_write_close();
            return strlen($line);
        });
        curl_setopt($ch, CURLOPT_WRITEFUNCTION, function ($ch, $line) {
            session_write_close();
            return strlen($line);
        });

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if (!$err) {
            $bitly = json_decode($response, TRUE);
            return !empty($bitly['link']) ? $bitly['link'] : $longurl;
        }
    }
    return $longurl;
}

function download_link(string $link = '')
{
    session_write_close();
    if (!empty($link)) {
        $providers = earnmoney_website('', '', FALSE, TRUE);
        $providers = json_decode($providers, TRUE);
        $provider = get_option('additional_url_shortener');
        $provider = !empty($provider) ? $provider : 'random';
        if ($provider === 'random') {
            $provider = array_rand($providers);
        }
        $longurl = earnmoney_link($link, $provider);
        return bitly_link($longurl);
    }
    return $link;
}

function newUpdate()
{
    session_write_close();

    $time = time();
    $repaired = (int) get_option('repaired');

    $dbEngine = get_option('db_engine');

    $version = 35;
    $updated = intval(get_option('updated'));

    $class = new \Model();
    if ($version > $updated) {
        set_option('updated', $version);

        $qry = $class->rawQuery("ALTER TABLE `tb_videos_hash` MODIFY COLUMN `data` TEXT NULL;");
        $qry = $class->rawQuery("ALTER TABLE `tb_videos_hash` MODIFY COLUMN `host_id` VARCHAR(1500) NOT NULL;");
        $qry = $class->rawQuery("ALTER TABLE `tb_videos_hash` MODIFY COLUMN `gdrive_email` VARCHAR(250) NULL;");
        $qry = $class->rawQuery("ALTER TABLE `tb_videos_hash` MODIFY COLUMN `hash_host` VARCHAR(100) NOT NULL;");
        $qry = $class->rawQuery("ALTER TABLE `tb_videos_hash` MODIFY COLUMN `hash_id` VARCHAR(3000) NOT NULL;");

        $qry = $class->rawQuery("ALTER TABLE `tb_videos` MODIFY COLUMN `title` VARCHAR(1000) NOT NULL;");
        $qry = $class->rawQuery("ALTER TABLE `tb_videos` MODIFY COLUMN `host_id` VARCHAR(1500) NOT NULL;");
        $qry = $class->rawQuery("ALTER TABLE `tb_videos` MODIFY COLUMN `ahost` VARCHAR(50) NULL;");
        $qry = $class->rawQuery("ALTER TABLE `tb_videos` MODIFY COLUMN `ahost_id` VARCHAR(1500) NULL;");
        $qry = $class->rawQuery("ALTER TABLE `tb_videos` MODIFY COLUMN `poster` VARCHAR(1500) NULL;");
        $qry = $class->rawQuery("ALTER TABLE `tb_videos` MODIFY COLUMN `views` INT(11) NOT NULL DEFAULT 0;");
        $qry = $class->rawQuery("ALTER TABLE `tb_videos` MODIFY COLUMN `status` INT(1) NOT NULL DEFAULT 0;");

        $qry = $class->rawQuery("ALTER TABLE `tb_videos_alternatives` MODIFY COLUMN `order` INT(5) NOT NULL DEFAULT 0;");

        $qry = $class->rawQuery("ALTER TABLE `tb_users` MODIFY COLUMN `updated` INT(15) NOT NULL DEFAULT 0;");

        $qry = $class->rawQuery("ALTER TABLE `tb_subtitle_manager` MODIFY COLUMN `host` VARCHAR(255) NOT NULL;");
        $qry = $class->rawQuery("ALTER TABLE `tb_subtitle_manager` MODIFY COLUMN `language` VARCHAR(50) NOT NULL DEFAULT 'Default';");

        $qry = $class->rawQuery("ALTER TABLE `tb_subtitles` MODIFY COLUMN `order` INT(5) NOT NULL DEFAULT 0;");

        $qry = $class->rawQuery("ALTER TABLE `tb_loadbalancers` MODIFY COLUMN `updated` INT(11) NOT NULL DEFAULT 0;");
        $qry = $class->rawQuery("ALTER TABLE `tb_loadbalancers` MODIFY COLUMN `disallow_hosts` TEXT NULL;");

        $qry = $class->rawQuery("CREATE TABLE IF NOT EXISTS `tb_plugins` (
            `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
            `key` VARCHAR(150) NOT NULL,
            `value` TEXT NOT NULL,
            `updated` INT(15) NOT NULL DEFAULT 0,
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");

        $qry = $class->rawQuery("CREATE TABLE IF NOT EXISTS `tb_gdrive_queue` (
                `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                `gdrive_id` VARCHAR(50) NOT NULL,
                PRIMARY KEY (`id`),
                UNIQUE INDEX `gdrive_id` (`gdrive_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");

        $qry = $class->rawQuery("CREATE TABLE IF NOT EXISTS `tb_videos_sources` (
                `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                `host` VARCHAR(50) NOT NULL,
                `host_id` VARCHAR(1500) NOT NULL,
                `data` TEXT NOT NULL,
                `dl` INT(1) NOT NULL DEFAULT 0,
                `sid` INT(11) NOT NULL DEFAULT 0,
                `created` INT(15) NOT NULL DEFAULT 0,
                `expired` INT(15) NOT NULL DEFAULT 0,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");

        $qry = $class->rawQuery("CREATE TABLE IF NOT EXISTS `tb_videos_short` (
                `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                `key` VARCHAR(50) NOT NULL,
                `vid` BIGINT(20) NOT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `key` (`key`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");

        $qry = $class->rawQuery("CREATE TABLE IF NOT EXISTS `tb_videos_hash` (
                `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                `host` VARCHAR(50) NOT NULL,
                `host_id` VARCHAR(1500) NOT NULL,
                `gdrive_email` VARCHAR(250) NOT NULL,
                `hash_host` VARCHAR(100) NOT NULL,
                `hash_id` VARCHAR(3000) NOT NULL,
                `data` TEXT NOT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");

        $qry = $class->rawQuery("CREATE TABLE IF NOT EXISTS `tb_stats` (
                `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                `vid` BIGINT(20) NOT NULL,
                `ip` VARCHAR(50) NOT NULL,
                `ua` TEXT NOT NULL,
                `created` INT(15) NOT NULL DEFAULT 0,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");

        $qry = $class->rawQuery("CREATE TABLE IF NOT EXISTS `tb_videos_alternatives` (
                `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                `vid` BIGINT(20) NOT NULL,
                `host` VARCHAR(50) NOT NULL,
                `host_id` VARCHAR(1500) NOT NULL,
                `order` INT(5) NOT NULL DEFAULT 0,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");

        $qry = $class->rawQuery("UPDATE `tb_videos` SET `updated` = `added` WHERE `updated` = 0;");

        $qry = $class->rawQuery("ALTER TABLE `tb_sessions` MODIFY COLUMN `ip` TEXT NULL;");

        $qry = $class->rawQuery("ALTER TABLE `tb_videos_hash` ADD COLUMN `data` TEXT NULL;");

        $qry = $class->rawQuery("ALTER TABLE `tb_loadbalancers` DROP COLUMN `histats`;");
        $qry = $class->rawQuery("ALTER TABLE `tb_loadbalancers` DROP COLUMN `load_ram`;");
        $qry = $class->rawQuery("ALTER TABLE `tb_loadbalancers` DROP COLUMN `load_cpu`;");
        $qry = $class->rawQuery("ALTER TABLE `tb_loadbalancers` DROP COLUMN `load`;");
        $qry = $class->rawQuery("ALTER TABLE `tb_loadbalancers` DROP COLUMN `ga`;");
        $qry = $class->rawQuery("ALTER TABLE `tb_loadbalancers` DROP COLUMN `gtm`;");
        $qry = $class->rawQuery("ALTER TABLE `tb_loadbalancers` DROP COLUMN `ga`;");

        if ($dbEngine !== 'InnoDB') {
            $updateEngine = $class->rawQuery('ALTER TABLE `tb_gdrive_auth` ENGINE = InnoDB;
                ALTER TABLE `tb_gdrive_mirrors` ENGINE = InnoDB;
                ALTER TABLE `tb_loadbalancers` ENGINE = InnoDB;
                ALTER TABLE `tb_plugins` ENGINE = InnoDB;
                ALTER TABLE `tb_sessions` ENGINE = InnoDB;
                ALTER TABLE `tb_settings` ENGINE = InnoDB;
                ALTER TABLE `tb_stats` ENGINE = InnoDB;
                ALTER TABLE `tb_subtitles` ENGINE = InnoDB;
                ALTER TABLE `tb_subtitle_manager` ENGINE = InnoDB;
                ALTER TABLE `tb_users` ENGINE = InnoDB;
                ALTER TABLE `tb_videos` ENGINE = InnoDB;
                ALTER TABLE `tb_videos_alternatives` ENGINE = InnoDB;
                ALTER TABLE `tb_videos_hash` ENGINE = InnoDB;
                ALTER TABLE `tb_videos_short` ENGINE = InnoDB;
                ALTER TABLE `tb_videos_sources` ENGINE = InnoDB;');
            if ($updateEngine) {
                set_option('db_engine', 'InnoDB');
            }
            $updateEngine = null;
            unset($updateEngine);
        }
        migrate(BASE_DIR . 'subtitles', BASE_DIR . 'uploads/subtitles');

        $qry = null;
        unset($qry);
    }
    // check table every 2 hours
    if ($time >= ($repaired + 7200)) {
        // Check for errors in tables or views
        $check = $class->rawFetchAll('CHECK TABLE `tb_gdrive_auth`, `tb_gdrive_mirrors`, `tb_loadbalancers`, `tb_plugins`, `tb_sessions`, `tb_settings`, `tb_stats`, `tb_subtitles`, `tb_subtitle_manager`, `tb_users`, `tb_videos`, `tb_videos_alternatives`, `tb_videos_hash`, `tb_videos_short`, `tb_videos_sources`');
        if ($check) {
            $analyzeResult = false;
            $tableError = [];
            $tableFine = [];
            foreach ($check as $dt) {
                if (strpos($dt['Msg_text'], 'OK') === FALSE && strpos($dt['Msg_text'], 'up to date') === FALSE) {
                    $tableError[] = $dt['Table'];
                } else {
                    $tableFine[] = $dt['Table'];
                }
            }
            $tables = trim(implode(',', $tableFine), ',');

            // On heavily modified tables that are displaying performance issues, analyzing may give a performance benefit by making sure the statistics are representative of the data distribution in the tables and indexes
            $analyzeResult = $class->rawFetchAll("ANALYZE TABLE $tables");
            if ($analyzeResult) {
                set_option('repaired', time());
            }
        }
    }
}

function import_gdrive_auth_json()
{
    session_write_close();
    try {
        $sourceDir = BASE_DIR . 'includes/gdrive_auth/';
        if (is_dir($sourceDir)) {
            $data = [];
            $time = time();
            $class = new \GDriveAuth();
            open_resources_handler();
            $list = new \DirectoryIterator($sourceDir);
            foreach ($list as $file) {
                if (!$file->isDot() && $file->isFile()) {
                    if ($file->getFilename() !== 'sample.json' && pathinfo($file->getFilename(), PATHINFO_EXTENSION) === 'json') {
                        open_resources_handler();
                        $fp = @fopen($sourceDir . $file->getFilename(), 'r');
                        if ($fp) {
                            stream_set_blocking($fp, false);
                            $content = stream_get_contents($fp);
                            fclose($fp);

                            $data = json_decode($content, true);
                            $data['created'] = $time;
                            $data['modified'] = $time;
                            $data['status'] = 1;
                            $data['uid'] = 1;
                            $class->insert($data);
                        }
                    }
                }
            }
        }
    } catch (\JsonException | \PDOException | \Exception $e) {
        error_log('Import GDrive JSON Error => ' . $e->getMessage());
    }
}

if (!function_exists('str_slug')) {
    function str_slug($string)
    {
        $search = array(
            '#(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)#',
            '#(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)#',
            '#(ì|í|ị|ỉ|ĩ)#',
            '#(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)#',
            '#(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)#',
            '#(ỳ|ý|ỵ|ỷ|ỹ)#',
            '#(đ)#',
            '#(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)#',
            '#(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)#',
            '#(Ì|Í|Ị|Ỉ|Ĩ)#',
            '#(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)#',
            '#(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)#',
            '#(Ỳ|Ý|Ỵ|Ỷ|Ỹ)#',
            '#(Đ)#',
            "/[^a-zA-Z0-9\-\_]/",
        );
        $replace = array(
            'a',
            'e',
            'i',
            'o',
            'u',
            'y',
            'd',
            'A',
            'E',
            'I',
            'O',
            'U',
            'Y',
            'D',
            '-',
        );
        $string = preg_replace($search, $replace, $string);
        $string = preg_replace('/(-)+/', '-', $string);
        $string = strtolower($string);
        return $string;
    }
}
