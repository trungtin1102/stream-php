<?php
if (php_sapi_name() !== "cli") {
    die('Please run this script from command line');
} else {
    require dirname(__FILE__) . '/../includes.php';

    $time = date('Y-m-d H:i:s');

    $class = new \LoadBalancers();
    $class->setCriteria('status', 1, '=');
    $num = $class->getNumRows();
    $isLB = is_load_balancer(BASE_URL);

    if ($num === 0 || ($num > 0 && $isLB)) {
        $class = new \InstanceCache();
        $class->setKey('check-chrome-version');
        $cache = $class->get();
        if (!$cache) {
            $class->save('check headless chrome background ' . $time, 3600);
            $chromeUA = getHeadlessChromeUA();
            echo "Chrome done\n";
            unset($chromeUA);
        } else {
            echo "Chrome done\n";
        }

        $class->setKey('check-server');
        $cache = $class->get();
        if (!$cache) {
            $class->save('check server in background ' . $time, 600);

            $queue = new \GDriveQueue();
            $queue->setLimit(0, 600);
            $list = $queue->get(['gdrive_id']);
            if ($list) {
                $gdrive = new \GDrive_Auth();
                $gdrive->encrypt_title(true);
                $alwaysCopy = filter_var(get_option('gdrive_copy'), FILTER_VALIDATE_BOOLEAN);
                if ($alwaysCopy) {
                    $copyToAll = filter_var(get_option('gdrive_copy_all'), FILTER_VALIDATE_BOOLEAN);
                    foreach ($list as $dt) {
                        sleep(1);
                        $info = $gdrive->get_file_info($dt['gdrive_id']);
                        if ($info) {
                            $files = $gdrive->get_mirrors(false, $dt['gdrive_id']);
                            if ($files) {
                                $queue->setCriteria('gdrive_id', $dt['gdrive_id']);
                                $queue->delete();
                            } else {
                                $gdrive->set_id($dt['gdrive_id']);
                                $copied = $copyToAll ? $gdrive->copy_file_to_all() : $gdrive->copy_file_now();
                                if ($copied) {
                                    $queue->setCriteria('gdrive_id', $dt['gdrive_id']);
                                    $queue->delete();
                                }
                            }
                        } else {
                            $queue->setCriteria('gdrive_id', $dt['gdrive_id']);
                            $queue->delete();
                        }
                    }
                }
                unset($gdrive);
                unset($alwaysCopy);
                unset($copyToAll);
                unset($files);
                unset($copied);
            }
            unset($queue);
            unset($list);

            $secure_salt = get_option('SECURE_SALT');
            if (empty($secure_salt)) set_option('SECURE_SALT', SECURE_SALT);

            create_dir(BASE_DIR . 'cache/playlist');
            create_dir(BASE_DIR . 'cache/streaming');
            create_dir(BASE_DIR . 'cache/logs/process');
            create_dir(BASE_DIR . 'tmp/hosts');

            deleteDir(BASE_DIR . 'cache/js');
            deleteDir(BASE_DIR . 'cache/opcache');

            newUpdate();
            auto_clear_expired_caches();
            updateMaxMindGeoIP();

            $class = new \Proxy();
            $class->autoupdateProxy();

            echo "Server done\n";
        } else {
            echo "Server done\n";
        }
        unset($class);
        unset($cache);
    }

    die('OK');
}
