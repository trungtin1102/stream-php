<?php
if (!defined('BASE_DIR')) exit();

session_write_close();

$recaptcha_site_key = get_option('recaptcha_site_key');
if (!empty($recaptcha_site_key)) :
?>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback" async defer></script>
    <script>
        var $token = $('#captcha-response');
        var onloadCallback = function() {
            if ($token.length) {
                grecaptcha.ready(function() {
                    grecaptcha.execute();
                });
            }
        };

        function gCallback(token) {
            if (token !== '' && $token.length) $token.val(token);
            else swal('Error!', 'Captcha failed to load!', 'error');
        }
    </script>
<?php endif; ?>
