<?php
class Proxy
{
    private $ch;

    function __construct()
    {
        session_write_close();
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $head) {
            session_write_close();
            return strlen($head);
        });
    }

    function proxy_docker_com(int $page = 1)
    {
        session_write_close();

        $url = 'https://www.proxydocker.com/';
        $scheme = parse_url($url, PHP_URL_SCHEME);
        $host = parse_url($url, PHP_URL_HOST);
        $port = parse_URL($url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host . '.');
        $resolveHost = implode(':', array($host, $port, $ipv4));

        curl_setopt($this->ch, CURLOPT_URL, 'https://www.proxydocker.com/id/proxylist/search?need=Google&type=https&anonymity=ELITE&port=all&country=Indonesia&city=&state=all');
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_COOKIEJAR, BASE_DIR . 'cookies/proxy-docker.txt');
        curl_setopt($this->ch, CURLOPT_REFERER, 'https://www.proxydocker.com/id/proxylist/platform/google');
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'accept: */*',
            'host: www.proxydocker.com',
            'origin: https://www.proxydocker.com',
        ));

        session_write_close();
        $response = curl_exec($this->ch);
        $err = curl_error($this->ch);

        if (!$err) {
            $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
            $meta = $dom->find('meta[name="_token"]');
            if (!empty($meta)) {
                curl_setopt($this->ch, CURLOPT_URL, 'https://www.proxydocker.com/id/api/proxylist/');
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($this->ch, CURLOPT_POSTFIELDS, 'token=' . $meta[0]->content . '&country=Indonesia&city=&state=all&port=all&type=https&anonymity=ELITE&need=Google&page=' . $page);
                curl_setopt($this->ch, CURLOPT_REFERER, 'https://www.proxydocker.com/id/proxylist/search?need=Google&type=https&anonymity=ELITE&port=all&country=Indonesia&city=&state=all');
                curl_setopt($this->ch, CURLOPT_COOKIEFILE, BASE_DIR . 'cookies/proxy-docker.txt');

                session_write_close();
                $response = curl_exec($this->ch);
                $err = curl_error($this->ch);

                if (!$err) {
                    $arr = json_decode($response, TRUE);
                    $result = [];
                    foreach ($arr['proxies'] as $pr) {
                        array_push($result, $pr['ip'] . ':' . $pr['port']);
                    }
                    return $result;
                }
            }
        }
        return [];
    }

    function free_proxy_list_net()
    {
        session_write_close();

        $url = 'https://free-proxy-list.net';
        $scheme = parse_url($url, PHP_URL_SCHEME);
        $host = parse_url($url, PHP_URL_HOST);
        $port = parse_URL($url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host . '.');
        $resolveHost = implode(':', array($host, $port, $ipv4));

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'host: free-proxy-list.net',
            'origin: http://free-proxy-list.net',
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $url);

        session_write_close();
        $response = curl_exec($this->ch);
        $err = curl_error($this->ch);

        if (!$err) {
            $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
            $raw = $dom->find('#raw', 0);
            if (!empty($raw)) {
                $textarea = $raw->find('textarea', 0)->plaintext;
                $list = explode('UTC.', $textarea);
                $list = trim(end($list));
                if (!empty($list)) {
                    $result = [];
                    $array = explode(" ", $list);
                    $array = count($array) > 1 ? $array : explode("\r\n", $list);
                    foreach ($array as $proxy) {
                        array_push($result, $proxy);
                    }
                    return $result;
                }
            }
        }
        return [];
    }

    function free_proxy_cz(string $type = 'https')
    {
        session_write_close();

        $url = 'http://free-proxy.cz/en/proxylist/country/all/' . $type . '/ping/level1';
        $scheme = parse_url($url, PHP_URL_SCHEME);
        $host = parse_url($url, PHP_URL_HOST);
        $port = parse_URL($url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host . '.');
        $resolveHost = implode(':', array($host, $port, $ipv4));

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'host: free-proxy.cz',
            'origin: http://free-proxy.cz',
        ));
        curl_setopt($this->ch, CURLOPT_REFERER, $url);

        session_write_close();
        $response = curl_exec($this->ch);
        $err = curl_error($this->ch);

        if (!$err) {
            if (preg_match_all('/Base64.decode\("([^"]+)"/', $response, $ip) && !empty($ip[1])) {
                $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
                $ports = $dom->find('span.fport');
                $result = [];
                foreach ($ip[1] as $i => $ip) {
                    if (isset($ports[$i])) {
                        array_push($result, base64_decode($ip) . ':' . trim($ports[$i]->plaintext) . ',' . $type);
                    }
                }
                return $result;
            }
        }
        return [];
    }

    function autoupdateProxy()
    {
        session_write_close();

        $disableFree = get_option('free_proxy');
        $disableProxy = get_option('disable_proxy');
        if (!empty($disableFree) && !empty($disableProxy)) {
            if (!filter_var($disableFree, FILTER_VALIDATE_BOOLEAN) && !filter_var($disableProxy, FILTER_VALIDATE_BOOLEAN)) {
                $list = proxy_list();
                $clist = count($list);
                if ($clist <= 1) {
                    $pd = $this->proxy_docker_com();
                    $fpl = $this->free_proxy_list_net();
                    $fp = $this->free_proxy_cz();
                    $prx = array_merge($list, $fpl, $fp, $pd);
                    $prx = array_filter($prx);
                    $prx = array_unique($prx, SORT_REGULAR);
                    if (!empty($prx)) {
                        set_option('proxy_list', implode("\n", $prx));
                        return $prx;
                    }
                }
            }
        }
        return FALSE;
    }

    function proxy_checker(array $proxyList = [])
    {
        session_write_close();

        if (!empty($proxyList)) {
            $deleteUnused = get_option('delete_unused_proxy');
            $proxyType = ['socks4', 'socks4a', 'socks5', 'http', 'https'];
            $proxyReplace = [CURLPROXY_SOCKS4, CURLPROXY_SOCKS4A, CURLPROXY_SOCKS5, CURLPROXY_HTTP, CURLPROXY_HTTPS];
            $realProxyType = CURLPROXY_HTTP;

            session_write_close();
            $mh = curl_multi_init();
            $ch = [];

            $url = 'https://docs.google.com/u/4/get_video_info?docid=1KPtwrGMyjC9c_ZpOID4eLaSDErrA5x3O';
            $scheme = parse_url($url, PHP_URL_SCHEME);
            $host = parse_url($url, PHP_URL_HOST);
            $port = parse_URL($url, PHP_URL_PORT);
            if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
            $ipv4 = gethostbyname($host . '.');
            $resolveHost = implode(':', array($host, $port, $ipv4));

            foreach ($proxyList as $i => $proxy) {
                $pr = explode(',', trim($proxy));
                $ch[$i] = curl_init($url);
                curl_setopt($ch[$i], CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch[$i], CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch[$i], CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch[$i], CURLOPT_MAXREDIRS, 10);
                curl_setopt($ch[$i], CURLOPT_ENCODING, '');
                curl_setopt($ch[$i], CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                curl_setopt($ch[$i], CURLOPT_RESOLVE, array($resolveHost));
                curl_setopt($ch[$i], CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
                curl_setopt($ch[$i], CURLOPT_DNS_SERVERS, dns_servers());
                curl_setopt($ch[$i], CURLOPT_DNS_CACHE_TIMEOUT, 300);
                curl_setopt($ch[$i], CURLOPT_TCP_KEEPALIVE, true);
                curl_setopt($ch[$i], CURLOPT_TCP_NODELAY, true);
                curl_setopt($ch[$i], CURLOPT_FORBID_REUSE, true);
                curl_setopt($ch[$i], CURLOPT_FAILONERROR, true);
                curl_setopt($ch[$i], CURLOPT_CONNECTTIMEOUT, 10);
                curl_setopt($ch[$i], CURLOPT_NOSIGNAL, true);
                curl_setopt($ch[$i], CURLOPT_HEADER, 1);
                curl_setopt($ch[$i], CURLOPT_NOBODY, 1);
                curl_setopt($ch[$i], CURLOPT_USERAGENT, USER_AGENT);
                if (!empty($pr)) {
                    curl_setopt($ch[$i], CURLOPT_PROXY, $pr[0]);
                    if (!empty($pr[1])) {
                        if (in_array(strtolower($pr[1]), $proxyType)) {
                            $key = array_search(strtolower($pr[1]), $proxyType);
                            $realProxyType = $proxyReplace[$key];
                            curl_setopt($ch[$i], CURLOPT_PROXYTYPE, $realProxyType);
                        } else {
                            $usrpwd = strpos($pr[1], '@') !== FALSE ? strtr($pr[1], ['@' => ':']) : $pr[1];
                            curl_setopt($ch[$i], CURLOPT_PROXYUSERPWD, $usrpwd);
                        }
                    }
                    if (!empty($pr[2])) {
                        $key = array_search(strtolower($pr[2]), $proxyType);
                        curl_setopt($ch[$i], CURLOPT_PROXYTYPE, $proxyReplace[$key]);
                    }
                }
                curl_setopt($ch[$i], CURLOPT_HEADERFUNCTION, function ($ch, $head) {
                    session_write_close();
                    return strlen($head);
                });
                curl_multi_add_handle($mh, $ch[$i]);
            }

            $active = null;
            do {
                $mrc = curl_multi_exec($mh, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);

            while ($active && $mrc == CURLM_OK) {
                if (curl_multi_select($mh) == -1) {
                    usleep(10);
                }
                do {
                    $mrc = curl_multi_exec($mh, $active);
                } while ($mrc == CURLM_CALL_MULTI_PERFORM);
            }

            $prx = [];
            foreach ($proxyList as $i => $proxy) {
                $response = curl_multi_getcontent($ch[$i]);
                $status = curl_getinfo($ch[$i], CURLINFO_HTTP_CODE);
                $err = curl_error($ch[$i]);
                if ($status >= 200 && $status < 400 && strpos($response, 'recaptcha') === FALSE) {
                    error_log("Proxy $proxy work => $status: $err");
                    $prx[] = $proxy;
                } else {
                    error_log("Proxy $proxy doesn't work => $status: $err");
                    if (!empty($deleteUnused) && !filter_var($deleteUnused, FILTER_VALIDATE_BOOLEAN)) {
                        $prx[] = $proxy;
                    }
                }
                curl_multi_remove_handle($mh, $ch[$i]);
            }
            curl_multi_close($mh);

            return $prx;
        }
        return FALSE;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
