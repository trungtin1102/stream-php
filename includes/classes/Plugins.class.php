<?php
class Plugins extends Model
{
    protected $table = 'tb_plugins';
    protected $fields = ['id', 'key', 'value', 'updated'];
    protected $primaryKey = 'id';
    private $plugins = false;

    function __construct()
    {
        session_write_close();
        parent::__construct();
        $this->plugins = $this->list();
    }

    function load()
    {
        session_write_close();
        if ($this->plugins) {
            foreach ($this->plugins as $dir) {
                if (file_exists($dir . '/functions.php')) {
                    require $dir . '/functions.php';
                }
            }
        }
    }

    function list()
    {
        session_write_close();
        try {
            if (is_dir(BASE_DIR . 'plugins/')) {
                $result = [];
                open_resources_handler();
                $list = new \DirectoryIterator(BASE_DIR . 'plugins/');
                foreach ($list as $file) {
                    if (!$file->isDot() && $file->isDir()) {
                        $result[] = BASE_DIR . 'plugins/' . $file->getFilename();
                    }
                }
                unset($list);
                return $result;
            }
        } catch (\Exception $e) {
            error_log('Plugins.class.php list => ' . $e->getMessage());
        }
        return FALSE;
    }

    function getAdminCSS(bool $html = false)
    {
        session_write_close();
        try {
            if ($this->plugins) {
                if ($html) {
                    $result = '';
                    foreach ($this->plugins as $dir) {
                        if (is_dir($dir . '/admin/assets/css/')) {
                            open_resources_handler();
                            $files = new \DirectoryIterator($dir . '/admin/assets/css/');
                            $url = strtr($dir, [BASE_DIR => BASE_URL]);
                            foreach ($files as $file) {
                                if (!$file->isDot() && $file->isFile()) {
                                    $result .= '<link rel="preload" href="' . $url . '/admin/assets/css/' . $file->getFilename() . '" as="style"><link rel="stylesheet" type="text/css" href="' . $url . '/admin/assets/css/' . $file->getFilename() . '">';
                                }
                            }
                            unset($files);
                        }
                    }
                    return $result;
                } else {
                    $result = [];
                    foreach ($this->plugins as $dir) {
                        if (is_dir($dir . '/admin/assets/css/')) {
                            open_resources_handler();
                            $files = new \DirectoryIterator($dir . '/admin/assets/css/');
                            $url = strtr($dir, [BASE_DIR => BASE_URL]);
                            foreach ($files as $file) {
                                if (!$file->isDot() && $file->isFile()) {
                                    $result[] = $url . '/admin/assets/css/' . $file->getFilename();
                                }
                            }
                            unset($files);
                        }
                    }
                    return $result;
                }
            }
        } catch (\Exception $e) {
            error_log('Plugins.class.php getAdminCSS => ' . $e->getMessage());
        }
        return FALSE;
    }

    function getPublicCSS(bool $html = false)
    {
        session_write_close();
        try {
            if ($this->plugins) {
                if ($html) {
                    $result = '';
                    foreach ($this->plugins as $dir) {
                        if (is_dir($dir . '/public/assets/css/')) {
                            open_resources_handler();
                            $files = new \DirectoryIterator($dir . '/public/assets/css/');
                            $url = strtr($dir, [BASE_DIR => BASE_URL]);
                            foreach ($files as $file) {
                                if (!$file->isDot() && $file->isFile()) {
                                    $result .= '<link rel="preload" href="' . $url . '/public/assets/css/' . $file->getFilename() . '" as="style"><link rel="stylesheet" type="text/css" href="' . $url . '/public/assets/css/' . $file->getFilename() . '">';
                                }
                            }
                            unset($files);
                        }
                    }
                    return $result;
                } else {
                    $result = [];
                    foreach ($this->plugins as $dir) {
                        if (is_dir($dir . '/public/assets/css/')) {
                            open_resources_handler();
                            $files = new \DirectoryIterator($dir . '/public/assets/css/');
                            $url = strtr($dir, [BASE_DIR => BASE_URL]);
                            foreach ($files as $file) {
                                if (!$file->isDot() && $file->isFile()) {
                                    $result[] = $url . '/public/assets/css/' . $file->getFilename();
                                }
                            }
                            unset($files);
                        }
                    }
                    return $result;
                }
            }
        } catch (\Exception $e) {
            error_log('Plugins.class.php getPublicCSS => ' . $e->getMessage());
        }
        return FALSE;
    }

    function getAdminJS(bool $html = false)
    {
        session_write_close();
        try {
            if ($this->plugins) {
                if ($html) {
                    $result = '';
                    foreach ($this->plugins as $dir) {
                        if (is_dir($dir . '/admin/assets/js/')) {
                            open_resources_handler();
                            $files = new \DirectoryIterator($dir . '/admin/assets/js/');
                            $url = strtr($dir, [BASE_DIR => BASE_URL]);
                            foreach ($files as $file) {
                                if (!$file->isDot() && $file->isFile()) {
                                    $result .= '<script src="' . $url . '/admin/assets/js/' . $file->getFilename() . '" defer></script>';
                                }
                            }
                            unset($files);
                        }
                    }
                    return $result;
                } else {
                    $result = [];
                    foreach ($this->plugins as $dir) {
                        if (is_dir($dir . '/admin/assets/js/')) {
                            open_resources_handler();
                            $files = new \DirectoryIterator($dir . '/admin/assets/js/');
                            $url = strtr($dir, [BASE_DIR => BASE_URL]);
                            foreach ($files as $file) {
                                if (!$file->isDot() && $file->isFile()) {
                                    $result[] = $url . '/admin/assets/js/' . $file->getFilename();
                                }
                            }
                            unset($files);
                        }
                    }
                    return $result;
                }
            }
        } catch (\Exception $e) {
            error_log('Plugins.class.php getAdminJS => ' . $e->getMessage());
        }
        return FALSE;
    }

    function getPublicJS(bool $html = false)
    {
        session_write_close();
        try {
            if ($this->plugins) {
                if ($html) {
                    $result = '';
                    foreach ($this->plugins as $dir) {
                        if (is_dir($dir . '/public/assets/js/')) {
                            open_resources_handler();
                            $files = new \DirectoryIterator($dir . '/public/assets/js/');
                            $url = strtr($dir, [BASE_DIR => BASE_URL]);
                            foreach ($files as $file) {
                                if (!$file->isDot() && $file->isFile()) {
                                    $result .= '<script src="' . $url . '/public/assets/js/' . $file->getFilename() . '" defer></script>';
                                }
                            }
                            unset($files);
                        }
                    }
                    return $result;
                } else {
                    $result = [];
                    foreach ($this->plugins as $dir) {
                        if (is_dir($dir . '/public/assets/js/')) {
                            open_resources_handler();
                            $files = new \DirectoryIterator($dir . '/public/assets/js/');
                            $url = strtr($dir, [BASE_DIR => BASE_URL]);
                            foreach ($files as $file) {
                                if (!$file->isDot() && $file->isFile()) {
                                    $result[] = $url . '/public/assets/js/' . $file->getFilename();
                                }
                            }
                            unset($files);
                        }
                    }
                    return $result;
                }
            }
        } catch (\Exception $e) {
            error_log('Plugins.class.php getPublicJS => ' . $e->getMessage());
        }
        return FALSE;
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
