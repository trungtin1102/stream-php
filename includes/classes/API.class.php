<?php
class API
{
    private $qry = [];
    private $token = '';
    private $is_admin = false;
    private $thisSite = '';
    private $mainSite = '';
    private $titleBlacklisted = false;
    private $videoNotFound = false;
    private $serverError = false;
    private $accessDenied = false;
    private $invalidParameters = false;

    function __construct()
    {
        session_write_close();

        create_dir(BASE_DIR . 'tmp/hosts');
        create_dir(BASE_DIR . 'cache/playlist');
        create_dir(BASE_DIR . 'cache/streaming');
        create_dir(BASE_DIR . 'cache/logs/process');

        $replace = ['https:' => '', 'http:' => '', 'www.' => ''];
        $this->mainSite = rtrim(strtr(get_option('main_site'), $replace), '/') . '/';
        $this->thisSite = rtrim(strtr(BASE_URL, $replace), '/') . '/';
        $ip = $this->get_ip();
        $this->qry = $this->get_query();
        if (!empty($this->qry)) {
            if (isset($_SERVER['HTTP_USER_AGENT']) && is_smartTV($_SERVER['HTTP_USER_AGENT'])) {
                $class = new \WhichBrowser\Parser($_SERVER['HTTP_USER_AGENT']);
                $this->token = hash('SHA256', $class->device->manufacturer . '~' . (isset($class->device->series) ? $class->device->series . '~' : '') . (isset($class->os->name) ? $class->os->name . '~' : '') . $class->engine->name);
                $this->is_admin = true;
            } else {
                $username = '';
                $password = '';
                if (!empty($this->qry['token'])) {
                    session_write_close();
                    $this->token = $this->qry['token'];
                    $token = decode($this->qry['token']);
                    if (filter_var($token, FILTER_VALIDATE_IP)) {
                        $this->is_admin = $ip === '::1' || $ip === '127.0.0.1' || getUserIPASN($token) === getUserIPASN($ip);
                    } else {
                        list($mainSite, $time) = array_pad(explode('~', $token), 2, '');
                        $expires = strtotime('+5 minutes');
                        $this->is_admin = (parse_url($mainSite, PHP_URL_HOST) === parse_url($this->mainSite, PHP_URL_HOST) && $time <= $expires) || adminTokenValidation($this->token);
                    }
                } elseif (!empty($_COOKIE['adv_token'])) {
                    session_write_close();
                    $this->token = $_COOKIE['adv_token'];
                    $this->is_admin = adminTokenValidation($_COOKIE['adv_token']);
                } else {
                    session_write_close();
                    if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
                        $username = $_SERVER['PHP_AUTH_USER'];
                        $password = $_SERVER['PHP_AUTH_PW'];
                    } elseif (!empty($_SERVER['HTTP_AUTHORIZATION'])) {
                        list($username, $password) = array_pad(explode(':', base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6))), 2, '');
                    }
                    // user validation
                    $user = userValidation($username);
                    if ($user) {
                        $this->token = encode($ip);
                        // cek status user
                        if (password_verify($password, $user['password'])) {
                            $this->is_admin = intval($user['role']) === 0 || intval($user['role']) === 3;
                        }
                    }
                }
            }
        } else {
            $this->invalidParameters = true;
        }
    }

    function get_server_time()
    {
        session_write_close();
        return date(\DateTime::ISO8601, time());
    }

    private function sources()
    {
        session_write_close();
        $download = isset($this->qry['download']) ? filter_var($this->qry['download'], FILTER_VALIDATE_BOOLEAN) : false;
        $shortlink = isset($this->qry['shortlink']) ? filter_var($this->qry['shortlink'], FILTER_VALIDATE_BOOLEAN) : false;

        $class = new \Hosting();
        if (!empty($this->qry['id']) && validate_url($this->qry['id'])) {
            $class->setURL($this->qry['id']);
            $this->qry['host'] = $class->getHost();
            $this->qry['id'] = $class->getID();
        }
        if (!empty($this->qry['aid']) && validate_url($this->qry['aid'])) {
            $class->setURL($this->qry['aid']);
            $this->qry['ahost'] = $class->getHost();
            $this->qry['aid'] = $class->getID();
        }
        if (isset($this->qry['alt'])) {
            if (intval($this->qry['alt']) === 0 && !empty($this->qry['aid'])) {
                $this->qry['id'] = $this->qry['aid'];
                $this->qry['host'] = $this->qry['ahost'];
                $this->qry['aid'] = '';
                $this->qry['ahost'] = '';
            }
        }
        $class = new \Parse_Sources($this->qry);
        $config = $class->get_config($download, $this->token);
        if ($config) {
            $title = rawurldecode(htmlspecialchars_decode($config['title']));
            if (is_title_blacklisted($title)) {
                $this->titleBlacklisted = true;
            } else {
                $sources = [];
                $tracks = [];

                if (!empty($config['sources'])) {
                    if ($shortlink) {
                        foreach ($config['sources'] as $dt) {
                            $sources[] = [
                                'file' => download_link($dt['file']),
                                'type' => $dt['type'],
                                'label' => $dt['label']
                            ];
                        }
                    } else {
                        $sources = $config['sources'];
                    }
                }

                if ($download && filter_var(get_option('hide_sub_download'), FILTER_VALIDATE_BOOLEAN)) {
                    $tracks = [];
                } else {
                    $tracks = $config['tracks'];
                }

                unset($this->qry['email']);
                unset($this->qry['origin']);
                unset($this->qry['token']);
                unset($this->qry['uid']);
                unset($this->qry['ip']);

                $scheme = isSSL() ? 'https:' : 'http:';
                $query = http_build_query($this->qry);
                $slug = $class->get_short_key();
                $slug .= isset($this->qry['alt']) ? '/?alt=' . $this->qry['alt'] : '';
                if (!empty($slug)) {
                    $embedLink = $scheme . $this->mainSite . 'embed/' . $slug;
                    $downloadLink = $scheme . $this->mainSite . 'download/' . $slug;
                } else {
                    $key = encode($query);
                    $embedLink = $scheme . $this->mainSite . 'embed/?' . $key;
                    $downloadLink = $scheme . $this->mainSite . 'download/?' . $key;
                }

                return json_encode([
                    'status' => 'ok',
                    'server_time' => $this->get_server_time(),
                    'query' => $this->qry,
                    'embed_link' => $embedLink,
                    'download_link' => $downloadLink,
                    'request_link' => $this->mainSite . 'embed2/?' . $query,
                    'title' => $title,
                    'poster' => $config['poster'],
                    'sources' => $sources,
                    'tracks' => $tracks
                ]);
            }
        } else {
            $this->videoNotFound = true;
            error_log('API get_sources config => video not found');
        }
    }

    function get_sources()
    {
        session_write_close();
        if (accessValidation()) {
            if ($this->is_admin && !empty($this->token)) {
                $class = new \LoadBalancers();
                $class->setCriteria('status', 1);
                if ($this->thisSite === $this->mainSite && $class->getNumRows() > 0) {
                    // get sources from load balancer site
                    session_write_close();
                    $hosts = [];
                    $this->qry['token'] = encode($this->mainSite . '~' . time());

                    if (isset($this->qry['source'])) {
                        $class = new \Videos();
                        $class->setCriteria('id', $this->qry['id']);
                        $data = $class->getOne(['host']);
                        if ($data) {
                            if (isset($this->qry['alt']) && intval($this->qry['alt']) > 0) {
                                $class = new \VideosAlternatives();
                                $class->setCriteria('vid', $this->qry['id']);
                                $class->setCriteria('host', $this->qry['alt'], '=', 'AND');
                                $data = $class->getOne(['host']);
                                if ($data) {
                                    $hosts[] = $data['host'];
                                }
                            } else {
                                $hosts[] = $data['host'];
                            }
                        } else {
                            $this->videoNotFound = true;
                            error_log('API get_sources => database video not found');
                            return false;
                        }
                    } else {
                        if (isset($this->qry['alt']) && intval($this->qry['alt']) > 0) {
                            $hosts[] = $this->qry['ahost'];
                        } elseif (isset($this->qry['host'])) {
                            $hosts[] = $this->qry['host'];
                        }
                    }
                    $url = get_load_balancer_rand($hosts);
                    $url = strtr($url, ['www.' => '']);
                    if ($url !== $this->mainSite) {
                        $this->qry['saved'] = false;
                        $url .= 'api/?' . encode(http_build_query($this->qry));
                        return $url;
                    } else {
                        return $this->sources();
                    }
                } else {
                    return $this->sources();
                }
            } else {
                $this->accessDenied = true;
            }
        } else {
            $this->accessDenied = true;
        }
        return false;
    }

    function get_token()
    {
        session_write_close();
        return $this->token;
    }

    function get_status()
    {
        session_write_close();
        if ($this->invalidParameters) {
            return json_encode(array(
                'status' => 'fail',
                'message' => 'Invalid parameters.'
            ), true);
        } elseif ($this->titleBlacklisted) {
            return json_encode(array(
                'status' => 'fail',
                'message' => 'Sorry this video was blacklisted for DMCA reasons.'
            ), true);
        } elseif ($this->accessDenied) {
            return json_encode(array(
                'status' => 'fail',
                'message' => 'Access denied!'
            ), true);
        } elseif ($this->serverError) {
            return json_encode(array(
                'status' => 'fail',
                'message' => 'Sorry, an error has occurred on the server! Please contact Admin.'
            ), true);
        } elseif ($this->videoNotFound) {
            return json_encode(array(
                'status' => 'fail',
                'message' => 'Sorry this video is unavailable.'
            ), true);
        }
        return json_encode(array(
            'status' => 'fail',
            'message' => 'Sorry this video is unavailable.'
        ), true);
    }

    function get_query()
    {
        session_write_close();
        $qry = [];
        // otentikasi
        if (strtolower($_SERVER['REQUEST_METHOD']) === 'get') {
            session_write_close();
            if (!empty($_GET['id']) || !empty($_GET['host']) || !empty($_GET['file_code'])) {
                // ambil data dari get params
                parse_str($_SERVER['QUERY_STRING'], $qry);
            } else {
                // ambil data dari load balancer
                $input = @decode($_SERVER['QUERY_STRING']);
                if ($input) {
                    parse_str($input, $qry);
                }
            }
        } elseif (strtolower($_SERVER['REQUEST_METHOD']) === 'post') {
            session_write_close();
            if (!empty($_POST)) {
                // ambil data dari post params
                $qry = $_POST;
            } else {
                // ambil data dari post json params
                open_resources_handler();
                $fp = @fopen('php://input', 'r');
                if ($fp) {
                    stream_set_blocking($fp, false);
                    $content = stream_get_contents($fp);
                    fclose($fp);

                    $qry = json_decode($content, true);
                }
            }
        }
        return $qry;
    }

    function get_referer()
    {
        session_write_close();
        if (!empty($_SERVER['HTTP_REFERER'])) {
            return $_SERVER['HTTP_REFERER'];
        } elseif (!empty($_SERVER['HTTP_ORIGIN'])) {
            return $_SERVER['HTTP_ORIGIN'];
        }
        return '';
    }

    function get_ip()
    {
        session_write_close();
        return getUserIP();
    }

    function get_ua()
    {
        session_write_close();
        return !empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    }

    function __destruct()
    {
        session_write_close();
        $class = new \InstanceCache();
        $checkChrome = $class->get('check-chrome-version');
        $checkServer = $class->get('check-server');
        if (!$checkChrome || !$checkServer) {
            execInBackground(strtr(BASE_DIR . 'includes/server-info.php', ['\/' => DIRECTORY_SEPARATOR, '\\' => DIRECTORY_SEPARATOR]));
        }
    }
}
