<?php
class Subscene
{
    private $baseURL = 'https://subscene.icu';
    private $subtitleURL = '';
    private $zipURL = '';
    private $zipFile = '';
    private $fileInfo = [];
    private $downloadDir = '';
    private $outputBaseURL = '';
    private $ch;

    function __construct()
    {
        session_write_close();

        $this->downloadDir = BASE_DIR . 'uploads/subtitles/';
        $this->outputBaseURL = BASE_URL . 'uploads/subtitles/';
    }

    function set_url(string $url = '')
    {
        session_write_close();

        $this->subtitleURL = !empty($url) ? $url : '';
        $sourceHost = parse_url($this->subtitleURL, PHP_URL_HOST);
        $subsceneHost = parse_url($this->baseURL, PHP_URL_HOST);
        $this->subtitleURL = strtr($this->subtitleURL, [$sourceHost => $subsceneHost]);
        $this->fileInfo = [
            'source_url' => $this->subtitleURL,
            'name' => '',
            'path' => '',
            'url' => ''
        ];
        $this->zipURL = '';
        $this->zipFile = '';
    }

    private function curl_initialize()
    {
        session_write_close();
        $scheme = parse_url($this->subtitleURL, PHP_URL_SCHEME);
        $host = parse_url($this->subtitleURL, PHP_URL_HOST);
        $port = parse_url($this->subtitleURL, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host . '.');
        $resolveHost = implode(':', array($host, $port, $ipv4));

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_REFERER, $this->baseURL);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
    }

    function get_download_url()
    {
        session_write_close();

        $this->curl_initialize();
        curl_setopt($this->ch, CURLOPT_URL, $this->subtitleURL);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 0);

        $response = curl_exec($this->ch);
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $err = curl_error($this->ch);
        curl_close($this->ch);

        if ($status === 200) {
            $dom = \KubAT\PhpSimple\HtmlDomParser::str_get_html($response);
            $subtitle = $dom->find('#content', 0)->find('.subtitle', 0);
            if (!empty($subtitle)) {
                $ul = $subtitle->find('.box', 0)->find('.top', 1)->find('.header', 0)->find('ul', 0);
                $result = '';
                foreach ($ul->find('li') as $li) {
                    $btnDL = $li->find('#downloadButton', 0);
                    if (!empty($btnDL)) {
                        $result = $this->baseURL . $btnDL->href;
                        break;
                    }
                }
                return $result;
            }
        } else {
            error_log("Subscene get_download_url => $status: $err");
        }
        return '';
    }

    function download()
    {
        session_write_close();

        $this->zipURL = $this->get_download_url();
        if (!empty($this->zipURL)) {
            list($host, $output_zip) = explode('/subtitles/', $this->subtitleURL, 2);
            $name = strtr($output_zip, ['/' => '-']);
            $fileName = 'subscene-' . $name . '.zip';
            $output_zip = $this->downloadDir . $fileName;
            $i = 0;
            while (file_exists($output_zip)) {
                $i++;
                $name = $name . '-' . $i;
                $newFileName = $name . '.zip';
                $output_zip = strtr($output_zip, [$fileName => $newFileName]);
                $fileName = $newFileName;
            }

            open_resources_handler();
            $fp = @fopen($output_zip, 'w+');
            $this->curl_initialize();
            curl_setopt($this->ch, CURLOPT_URL, $this->zipURL);
            curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 0);
            curl_setopt($this->ch, CURLOPT_FILE, $fp);
            curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);
            curl_close($this->ch);
            if ($fp) fclose($fp);

            if ($status === 200 || $status === 206) {
                $this->zipFile = $output_zip;
                return $output_zip;
            } else {
                error_log("Subscene download => $status: $err");
            }
        } else {
            error_log("Subscene download => downlad url does not exist");
        }
        return FALSE;
    }

    /**
     * @return array|false
     */
    function file()
    {
        session_write_close();

        $this->zipFile = !empty($this->zipFile) ? $this->zipFile : $this->download();
        if (file_exists($this->zipFile)) {
            $path = rtrim(pathinfo($this->zipFile, PATHINFO_DIRNAME), '/') . '/';
            $fileName = pathinfo($this->zipFile, PATHINFO_FILENAME);

            $class = new \ZipArchive;
            $fp = $class->open($this->zipFile);
            if ($fp) {
                $zipPath = $path . $fileName . '/';
                $class->extractTo($zipPath);
                $class->close();

                $allowExt = ['srt', 'vtt', 'ass', 'sub', 'stl', 'dfxp', 'ttml', 'sbv', 'txt'];
                open_resources_handler();
                $list = new \DirectoryIterator($zipPath);
                foreach ($list as $dt) {
                    if (!$dt->isDot() && $dt->isFile()) {
                        $ext = $dt->getExtension();
                        if (in_array($ext, $allowExt)) {
                            $basename = $fileName . '.' . $ext;
                            $newBaseName = $basename;
                            $file = $path . $basename;
                            $i = 0;
                            while (file_exists($file)) {
                                $i++;
                                $newBaseName = $fileName . '-' . $i . '.' . $ext;
                                $file = strtr($file, [$basename => $newBaseName]);
                            }
                            $rename = @rename($dt->getPathname(), $file);
                            if ($rename) {
                                $this->fileInfo['name'] = $newBaseName;
                                $this->fileInfo['path'] = $file;
                                $this->fileInfo['url'] = $this->outputBaseURL . $newBaseName;
                                @unlink($this->zipFile);
                                deleteDir($zipPath);
                                break;
                            }
                        }
                    }
                }
                return $this->fileInfo;
            } else {
                error_log('Subscene file => unzip failed');
            }
        } else {
            error_log('Subscene file => file does not exist');
        }
        return FALSE;
    }

    /**
     * get subtitle file information: source_url, file name, path, url
     * @param string $key
     * @return array|string
     */
    function get_file_info(string $key = '')
    {
        session_write_close();
        if (!empty($key)) {
            return $this->fileInfo[$key];
        }
        return $this->fileInfo;
    }
}
