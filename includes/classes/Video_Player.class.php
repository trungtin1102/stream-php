<?php
class Video_Player extends Core
{
    private $qry = [];
    private $videoHost  = '';
    private $videoId    = '';
    private $videoURL   = '';
    private $videoRes   = '';
    private $videoEmail = '';
    private $videoTitle = '';
    private $header     = [];
    private $allowResponseHeader = ['accept-ranges', 'content-length', 'content-range', 'content-type', 'content-transfer-encoding', 'etag'];
    private $withoutReferer = ['anonfile', 'bayfiles', 'racaty', 'uptobox'];
    private $withoutOrigin = ['uqload', 'vidoza', 'fembed', 'archive', 'pcloud', 'blogger', 'amazon', 'facebook', 'hexupload', 'hxfile', 'mp4upload', 'streamable', 'solidfiles', 'rumble', 'tiktok', 'userscloud', 'uploadsmobi', 'yourupload', 'zippyshare', 'anonfile', 'bayfiles', 'racaty', 'vupto', 'uptobox', 'gofile'];
    private $ch;
    private $cacheExpires;
    private $cacheCreated;

    function __construct(array $qry = [])
    {
        session_write_close();

        parent::__construct();

        $this->qry = $qry;
        $this->videoHost = isset($qry['host']) ? $qry['host'] : '';
        $this->videoId = isset($qry['id']) ? rawurldecode($qry['id']) : '';
        if ($this->videoHost === 'direct') $this->videoId = strtr($this->videoId, [' ' => '%20']);
        $this->videoRes = isset($qry['res']) ? $qry['res'] : '';
        $this->videoURL = isset($qry['url']) ? $qry['url'] : '';
        $this->videoEmail = isset($qry['email']) ? $qry['email'] : '';

        $now = time();
        $this->cacheExpires = $now;
        $this->cacheCreated = $now;

        $host = parse_url(BASE_URL, PHP_URL_HOST);
        $iCache = new \InstanceCache();
        $iCache->setKey('lb_get_id_byhost~' . $host);
        $cache = $iCache->get();
        if ($cache) {
            $sid = $cache;
        } else {
            $class = new \LoadBalancers();
            $class->setCriteria('link', "%//{$host}%", 'LIKE');
            $data = $class->getOne(['id']);
            $sid = $data ? $data['id'] : 0;
            $iCache->save($sid, 10);
        }

        $this->set_server_id($sid);
        if (in_array($this->videoHost, $this->host_using_MP4HLS())) {
            $cache = $this->getCache($this->videoHost, $this->videoId, true);
        } else {
            $cache = $this->getCache($this->videoHost, $this->videoId);
        }
        if ($cache) {
            $data = json_decode($cache['data'], true);

            $this->videoEmail = $data['email'];
            $this->cacheExpires = $cache['expired'];
            $this->cacheCreated = $cache['created'];
            $this->videoTitle = !empty($data['title']) ? htmlspecialchars_decode($data['title']) : '';
            if (!empty($this->videoTitle)) {
                $ext = pathinfo($this->videoTitle, PATHINFO_EXTENSION);
                if (empty($ext)) $this->videoTitle .= '.mp4';
                header('Content-Disposition: attachment; filename="' . $this->videoTitle . '"');
            } else {
                header('Content-Disposition: attachment');
            }

            $type = '';
            $key = array_search($this->videoRes, array_column($data['sources'], 'label'));
            if (isset($data['sources'][$key])) {
                $type = $data['sources'][$key];
                $this->videoURL = html_entity_decode($data['sources'][$key]['file']);
                $this->videoURL = strtr($this->videoURL, [' ' => '%20']);
            } else {
                $type = $data['sources'][0];
                $this->videoURL = html_entity_decode($data['sources'][0]['file']);
                $this->videoURL = strtr($this->videoURL, [' ' => '%20']);
            }
            if ($type !== 'hls' && $type !== 'mpd' && validate_url($this->videoURL)) {
                if (!empty($this->videoURL)) {
                    $scheme = parse_url($this->videoURL, PHP_URL_SCHEME);
                    $host = parse_url($this->videoURL, PHP_URL_HOST);
                    $port = parse_url($this->videoURL, PHP_URL_PORT);
                    $ipv4 = gethostbyname($host);
                    if (empty($port)) {
                        $port = $scheme === 'https' ? 443 : 80;
                    }
                    $resolveHost = array(implode(':', array($host, $port, $ipv4)));

                    $host .= $port !== 80 && $port !== 443 ? ':' . $port : '';
                    $this->header = array_merge($this->header, $this->defaultHeaders($this->videoHost));
                    $this->header[] = 'Host: ' . $host;

                    if ($this->videoHost === 'gdrive') {
                        $auth = $this->gdrive_auth();
                        if ($auth) $this->header[] = $auth;
                    }

                    if (!empty($data['referer'])) {
                        // get origin
                        if (!in_array($this->videoHost, $this->withoutOrigin)) {
                            $this->header[] = 'Origin: ' . parse_url($data['referer'], PHP_URL_SCHEME) . '://' . parse_url($data['referer'], PHP_URL_HOST);
                        }
                        // get referer
                        if (
                            !in_array($this->videoHost, $this->withoutReferer) &&
                            ($this->videoHost !== 'direct' || strpos($this->videoURL, 'b-cdn.net') !== FALSE || strpos($this->videoURL, 'bitball.se') !== FALSE)
                        ) {
                            $this->header[] = 'Referer: ' . $data['referer'];
                        }
                    } elseif (!in_array($this->videoHost, $this->withoutOrigin)) {
                        $this->header[] = 'Origin: ' . $scheme . '://' . $host;
                    }

                    session_write_close();
                    $this->ch = curl_init();
                    curl_setopt($this->ch, CURLOPT_URL, $this->videoURL);
                    curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, false);
                    curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($this->ch, CURLOPT_ENCODING, '');
                    curl_setopt($this->ch, CURLOPT_MAXREDIRS, 10);
                    curl_setopt($this->ch, CURLOPT_TIMEOUT, 0);
                    curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
                    curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
                    curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                    curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
                    curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
                    curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
                    curl_setopt($this->ch, CURLOPT_BUFFERSIZE, 10485760);
                    curl_setopt($this->ch, CURLOPT_HEADER, false);
                    curl_setopt($this->ch, CURLOPT_NOBODY, false);
                    curl_setopt($this->ch, CURLOPT_FAILONERROR, true);

                    if (!filter_var($host, FILTER_VALIDATE_IP)) {
                        curl_setopt($this->ch, CURLOPT_RESOLVE, $resolveHost);
                        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
                    }

                    if (!empty($data['cookies'])) {
                        curl_setopt($this->ch, CURLOPT_COOKIE, trim(implode(';', $data['cookies'])));
                    }

                    if (!filter_var(get_option('production_mode'), FILTER_VALIDATE_BOOLEAN) && count(get_resources()) < 900) {
                        curl_setopt($this->ch, CURLOPT_VERBOSE, true);
                        curl_setopt($this->ch, CURLOPT_STDERR, fopen(BASE_DIR . 'cache/streaming/curl~' . $this->videoHost . '~' . substr(keyFilter($this->videoId), 0, 200) . '.txt', 'w+'));
                    }
                } else {
                    error_log("Video_Playback {$this->videoHost} {$this->videoId} => {$this->videoURL} => url not found");
                }
            } else {
                error_log("Video_Playback {$this->videoHost} {$this->videoId} => $type video not supported");
            }
        } else {
            error_log("Video_Playback {$this->videoHost} {$this->videoId} => {$this->videoURL} => sources not found");
            $this->errorResponse(404);
        }
    }

    private function gdrive_auth()
    {
        session_write_close();
        if ($this->videoHost === 'gdrive' && strpos($this->videoURL, 'googleapis.com') !== FALSE) {
            session_write_close();
            $class = new \GDrive_Auth();
            if (!empty($this->videoEmail)) {
                session_write_close();
                $token = $class->get_access_token($this->videoEmail);
                if ($token) {
                    return 'authorization: ' . $token['token_type'] . ' ' . $token['access_token'];
                } else {
                    $tokens = $class->get_access_tokens();
                    if ($tokens) {
                        $key = array_rand($tokens);
                        $token = $tokens[$key];
                        return 'authorization: ' . $token['token_type'] . ' ' . $token['access_token'];
                    } else {
                        $req = explode('/', trim(strtr($_SERVER['REQUEST_URI'], ['\/' => '/']), '/'));
                        $qry = !empty($_SERVER['QUERY_STRING']) ? '?' . $_SERVER['QUERY_STRING'] : '';
                        // remove videplayback
                        unset($req[0]);
                        // rearrange array
                        $req = array_values($req);
                        $newUrl = BASE_URL . 'videoplayback/' . $req[0] . '/' . $req[1] . '/original/v.mp4' . $qry;
                        header('location: ' . $newUrl);
                        exit();
                    }
                }
            } else {
                session_write_close();
                $tokens = $class->get_access_tokens();
                if ($tokens) {
                    $key = array_rand($tokens);
                    $token = $tokens[$key];
                    return 'authorization: ' . $token['token_type'] . ' ' . $token['access_token'];
                } else {
                    $req = explode('/', trim(strtr($_SERVER['REQUEST_URI'], ['\/' => '/']), '/'));
                    $qry = !empty($_SERVER['QUERY_STRING']) ? '?' . $_SERVER['QUERY_STRING'] : '';
                    // remove videplayback
                    unset($req[0]);
                    // rearrange array
                    $req = array_values($req);
                    $newUrl = BASE_URL . 'videoplayback/' . $req[0] . '/' . $req[1] . '/original/v.mp4' . $qry;
                    header('location: ' . $newUrl);
                    exit();
                }
            }
        }
        return FALSE;
    }

    private function errorResponse(int $statusCode = 404)
    {
        session_write_close();
        header_remove('Cache-Control');
        header_remove('Last-Modified');
        header_remove('Expires');
        $now = time();
        header('Cache-Control: no-store');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', $now));
        header('Expires: ' . gmdate('D, d M Y H:i:s T', $now));
        http_response_code($statusCode);
        exit();
    }

    function stream(string $range = '')
    {
        session_write_close();
        if ($this->ch) {
            if ($this->videoHost === 'uqload') {
                sleep(rand(1, 5));
            } elseif (in_array($this->videoHost, $this->bad_hosts())) {
                sleep(rand(1, 5));
                $proxy = rate_limit_ips();
                if ($proxy) {
                    $this->header[] = 'X-Originating-IP: ' . $proxy;
                    $this->header[] = 'X-Forwarded-For: ' . $proxy;
                    $this->header[] = 'X-Remote-IP: ' . $proxy;
                    $this->header[] = 'X-Remote-Addr: ' . $proxy;
                    $this->header[] = 'X-Client-IP: ' . $proxy;
                    $this->header[] = 'X-Host: ' . $proxy;
                    $this->header[] = 'X-Forwared-Host: ' . $proxy;
                }
            }

            if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
                $this->header[] = 'If-Modified-Since: ' . $_SERVER['HTTP_IF_MODIFIED_SINCE'];
            }

            if (isset($_SERVER['HTTP_IF_NONE_MATCH'])) {
                $this->header[] = 'If-None-Match: ' . $_SERVER['HTTP_IF_NONE_MATCH'];
            }

            if (isset($_SERVER['HTTP_IF_RANGE'])) {
                $this->header[] = 'If-Range: ' . $_SERVER['HTTP_IF_RANGE'];
            }

            if (!empty($range)) {
                $this->header[] = 'range: ' . $range;
                http_response_code(206);
            } else {
                $this->header[] = 'range: bytes=0-';
                http_response_code(200);
            }

            if ($this->videoHost === 'streamlare') {
                $key = array_search('content-type', $this->allowResponseHeader);
                unset($this->allowResponseHeader[$key]);
                header('content-type: application/octet-stream');
            }

            header('Cache-Control: private');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', $this->cacheCreated));
            header('Expires: ' . gmdate('D, d M Y H:i:s T', $this->cacheExpires));

            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->header);
            curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($curl, $header) {
                session_write_close();
                $header = strtolower($header);
                list($key, $val) = array_pad(explode(':', $header), 2, '');
                if (in_array($key, $this->allowResponseHeader)) header($header);
                return strlen($header);
            });
            curl_setopt($this->ch, CURLOPT_WRITEFUNCTION, function ($curl, $body) {
                session_write_close();
                echo $body;
                ob_flush();
                flush();
                return strlen($body);
            });
            curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_RESPONSE_CODE);
            $err = curl_error($this->ch);
            curl_close($this->ch);

            if ($status === 401 || $status === 429 || $status >= 500) {
                $class = new \Video_Player($this->qry);
                $class->stream($range);
                exit();
            } elseif ($status < 200 || $status >= 400) {
                error_log("Video_Playback {$this->videoHost} {$this->videoId} => {$this->videoURL} => $status: $err");
                $this->errorResponse($status);
            }
        } else {
            error_log("Video_Playback {$this->videoHost} {$this->videoId} => {$this->videoURL} => curl instance not found");
            $this->errorResponse(404);
        }
    }

    function __destruct()
    {
        session_write_close();
    }
}
