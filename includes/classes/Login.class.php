<?php
class Login
{
	private $user;
	private $session;
	private $isLoggedIn = false;

	function __construct()
	{
		session_write_close();
		$this->user = new \Users();
		$this->session = new \Sessions();
		$this->isSSL = isSSL();
	}

	function cek_login()
	{
		session_write_close();
		$iCache = new \InstanceCache();
		if (isset($_COOKIE['adv_token'])) {
			$iCache->setKey('session_' . $_COOKIE['adv_token']);
			$cache = $iCache->get();
			if ($cache) {
				if ($cache['useragent'] === $_SERVER['HTTP_USER_AGENT']) {
					$this->user->setCriteria('user', $cache['username']);
					$this->user->setCriteria('email', $cache['username'], '=', 'OR');
					$user = $this->user->getOne();
					if ($user) {
						$this->isLoggedIn = true;
						$_SESSION['user'] = $user;
						return $user;
					}
				}
			} else {
				$this->session->setCriteria('token', $_COOKIE['adv_token']);
				$this->session->setCriteria('expired', time(), '>', 'AND');
				$this->session->setCriteria('stat', 0, '=', 'AND');
				$this->session->setCriteria('useragent', $_SERVER['HTTP_USER_AGENT'], '=', 'AND');
				$data = $this->session->getOne();
				if ($data) {
					$iCache->save($data, 5, 'sessions');

					$this->user->setCriteria('user', $data['username']);
					$this->user->setCriteria('email', $data['username'], '=', 'OR');
					$user = $this->user->getOne();
					if ($user) {
						$this->isLoggedIn = true;
						$_SESSION['user'] = $user;
						return $user;
					}
				}
			}
		}
		return false;
	}

	function salah_login_action(string $username = '')
	{
		session_write_close();

		$ip = getUserIP();
		$created = time();
		$saved = $this->session->insert(array(
			'ip' => $ip,
			'useragent' => $_SERVER['HTTP_USER_AGENT'],
			'created' => $created,
			'username' => $username
		));
		return $saved;
	}

	function cek_salah_login(int $limit = 5)
	{
		session_write_close();

		$ip = getUserIP();
		$this->session->setCriteria('stat', 0);
		$this->session->setCriteria('ip', $ip);
		$this->session->setOrderBy('created', 'DESC');
		$this->session->setLimit($limit);
		$rows = $this->session->get();
		$data = $this->session->getNumRows();
		return $data >= $limit && ($rows && (time() - $rows[0]['created']) < 300);
	}

	function true_login(string $username = '', string $expiredAt = '')
	{
		session_write_close();

		$ip = getUserIP();
		$created = time();
		if (!empty($expiredAt)) {
			$expireddb = strtotime($expiredAt);
		} else {
			$expireddb = strtotime("+6 hours");
		}
		$token = sha1($ip . $expireddb . "kbDx-120_MzWkl" . microtime());

		// buat session baru
		$data = array(
			'ip' => $ip,
			'useragent' => $_SERVER['HTTP_USER_AGENT'],
			'created' => $created,
			'username' => $username,
			'expired' => $expireddb,
			'token' => $token,
			'stat' => 0
		);
		$insert = $this->session->insert($data);

		if ($insert) {
			//kembalikan data user yg sedang login,, siapa tahu nanti ingin diolah
			$this->user->setCriteria('user', $username);
			$this->user->setCriteria('email', $username, '=', 'OR');
			$data = $this->user->getOne();
			if ($data) {
				if (PHP_SESSION_NONE === session_status()) session_start();
				$_SESSION['user'] = $data;
				$this->isLoggedIn = true;
				//simpan token ke cookie
				$expr = !empty($expiredAt) ? strtotime($expiredAt) : 0;
				setcookie('adv_token', $token, $expr, '/');
				return TRUE;
			}
		}
		return FALSE;
	}

	function logout()
	{
		session_write_close();

		$this->isLoggedIn = false;
		if (isset($_COOKIE['adv_token'])) {
			$expr = time();

			// nonaktifkan sesi sebelumnya
			$this->session->setCriteria('token', $_COOKIE['adv_token']);
			$this->session->update(array(
				'expired' => $expr,
				'stat' => 9
			));

			if (isset($_SESSION['user'])) unset($_SESSION['user']);
			setcookie('adv_token', null, $expr, '/');
			unset($_COOKIE['adv_token']);
		}
		return TRUE;
	}

	function login_redir()
	{
		session_write_close();

		if (!$this->isLoggedIn) {
			header("location: " . BASE_URL . admin_dir() . '/login/');
			exit();
		}
	}
}
