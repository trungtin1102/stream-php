<?php
class Hosting
{
    private $url = '';
    private $host = 'direct';
    private $id = '';
    private $urlIDHost = ['vidio', 'viu', 'blogger', 'fembed', 'zippyshare', 'pcloud', 'soundcloud', 'filesfm', 'direct', 'neonime', 'vlive'];
    private $pathIDHost = ['facebook', 'tiktok'];
    private $zeroIDHost = ['vidoza', 'mp4upload', 'bayfiles', 'anonfile', 'filerio', 'fireload', 'uploadbuzz', 'filesim', 'hexupload', 'hxfile', 'indishare', 'pandafiles', 'racaty'];
    private $oneIDHost = ['mediafire', 'dropbox', 'streamtape', 'ulozto', 'mixdropto', 'okru'];
    private $pathQryHost = ['desustream'];
    private $qryIDHost = ['youtube', 'blogger', 'onedrive'];

    function __construct(string $url = '')
    {
        session_write_close();
        $this->url = trim($url);
        $this->extractor();
    }

    function setURL(string $url = '')
    {
        session_write_close();
        $this->url = trim($url);
        $this->host = '';
        $this->id = '';
        $this->extractor();
    }

    private function extractor()
    {
        session_write_close();
        $this->url = strtr(rawurldecode($this->url), [' ' => '%20', 'www.' => '', 'slwatch.co' => 'streamlare.com', 'slmaxed.com' => 'streamlare.com', 'streamadblockplus.com' => 'streamtape.com', 'odnoklassniki.ru' => 'ok.ru', 'video.sibnet.ru' => 'sibnet.ru', 'sbplay2' => 'streamsb', 'sbplay' => 'streamsb', 'watchsb' => 'streamsb', 'sbfast' => 'streamsb', 'sbfull' => 'streamsb', 'embedsb' => 'streamsb', 'sbembed2' => 'streamsb', 'sbembed' => 'streamsb', 'viewsb' => 'streamsb', 'femax20.com' => 'fembed.com', 'mrdhan.com' => 'fembed.com', 'dutrag.com' => 'fembed.com', 'feurl.com' => 'fembed.com', 'diasfem.com' => 'fembed.com', 'mixdrop.co' => 'mixdrop.to', 'mixdrop.sx' => 'mixdrop.to', 'dl.indishare.cc' => 'indishare.org', 'streamtape.xyz' => 'streamtape.com', 'strtape.cloud' => 'streamtape.com', 'strcloud.link' => 'streamtape.com', 'streamta.pe' => 'streamtape.com', 'strtpe.link' => 'streamtape.com', 'uptostream.com' => 'uptobox.com', 'vupload.com' => 'vup.to', 'disk.yandex.com' => 'yadi.sk', 'disk.yandex.ru' => 'yadi.sk', 'anonfiles.com' => 'anonfile.com', 'googleapis.com' => 'drive.google.com']);
        $this->hostExtractor();
        $this->idExtractor();
    }

    static function supportedSites()
    {
        session_write_close();
        $list = [
            'cloudvideo' => 'CloudVideo|Additional Host',
            'embedgram' => 'Embedgram|Additional Host',
            'streamhub' => 'StreamHub|Additional Host',
            'ulozto' => 'Uloz.to|Additional Host',
            'vlive' => 'V LIVE|Additional Host',
            'vtube' => 'vTube|Additional Host',
            'desustream' => 'Desustream Otakudesu|Additional Host',
            'gomunime'  => 'Gomunime|Additional Host',
            'kuronime' => 'Kuronime|Additional Host',
            'neonime' => 'Neonime|Additional Host',
            'uservideo' => 'Uservideo|Additional Host',
            'direct' => 'Direct Link',
            'gdrive' => 'Google Drive',
            'fembed' => 'Fembed',
            'blogger' => 'Blogger',
            'googlephotos' => 'Google Photos',
            'youtube' => 'Youtube',
            'amazon' => 'Amazon Drive',
            'anonfile' => 'AnonFile',
            'archive' => 'Archive',
            'bayfiles' => 'BayFiles',
            'dailymotion' => 'Dailymotion',
            'dropbox' => 'Dropbox',
            'facebook' => 'Facebook',
            'filecm' => 'File.cm',
            'filerio' => 'Filerio',
            'filesfm' => 'Files.fm',
            'filesim' => 'Files.im',
            'fireload' => 'Fireload',
            'gofile' => 'Gofile',
            'hexupload' => 'Hexupload',
            'hxfile' => 'HxFile',
            'indishare' => 'Indishare',
            'mediafire' => 'MediaFire',
            'mixdropto' => 'MixDrop',
            'mp4upload' => 'mp4upload',
            'mymailru' => 'Mail.Ru Video',
            'okru' => 'OK.ru',
            'onedrive' => 'OneDrive',
            'pandafiles' => 'PandaFiles',
            'pcloud' => 'pCloud',
            'racaty' => 'Racaty',
            'rumble' => 'Rumble',
            'sendvid' => 'Sendvid',
            'sibnet' => 'Sibnet.Ru Video',
            'solidfiles' => 'Solidfiles',
            'soundcloud' => 'Soundcloud',
            'streamable' => 'Streamable',
            'streamff' => 'Streamff',
            'streamlare' => 'Streamlare',
            'streamsb' => 'StreamSB',
            'streamtape' => 'Streamtape',
            'supervideo' => 'SuperVideo',
            'tiktok' => 'Tiktok',
            'uploadbuzz' => 'UploadBuzz',
            'uploadsmobi' => 'Uploads.mobi',
            'upstream' => 'UpStream',
            'uptobox' => 'Uptobox',
            'uqload' => 'Uqload',
            'userscloud' => 'Userscloud',
            'videobin' => 'Videobin',
            'vidio' => 'Vidio',
            'vidoza' => 'Vidoza',
            'vimeo' => 'Vimeo',
            'viu' => 'VIU',
            'voe' => 'VOE',
            'vudeo' => 'Vudeo',
            'vupto' => 'Vupload',
            'yadisk' => 'Yandex Disk',
            'yourupload' => 'YourUpload',
            'zippyshare' => 'Zippyshare',
            'zplayer' => 'zPlayer.live',
        ];
        return $list;
    }

    static function linkExample(string $host = '')
    {
        session_write_close();
        $list = array(
            'uploadbuzz' => [
                'https://uploadbuzz.cc/mp0oj82tpac5'
            ],
            'vlive' => [
                'https://www.vlive.tv/post/0-18382971',
                'https://www.vlive.tv/video/118697',
                'https://www.vlive.tv/embed/118697'
            ],
            'embedgram' => [
                'https://embedgram.com/v/61np157orps72',
                'https://embedgram.com/f/61np157orps72'
            ],
            'streamlare' => [
                'https://streamlare.com/v/7AkQpz9LyKjDRdN6',
                'https://streamlare.com/e/7AkQpz9LyKjDRdN6'
            ],
            'sibnet' => [
                'https://video.sibnet.ru/video1934093-007__Spectre_Official_Trailer___007__Spektr__Perevod__Wizzar63_'
            ],
            'sendvid' => [
                'https://sendvid.com/pd8en8bj',
                'https://sendvid.com/embed/pd8en8bj'
            ],
            'mymailru' => [
                'https://my.mail.ru/video/embed/84530788950872688'
            ],
            'voe' => [
                'https://voe.sx/8tql9at1dice',
                'https://voe.sx/e/8tql9at1dice'
            ],
            'supervideo' => [
                'https://supervideo.tv/jyvt4tqy6351',
                'https://supervideo.tv/e/jyvt4tqy6351'
            ],
            'ulozto' => [
                'https://ulozto.net/file/Z9wKxVoRiqEl',
                'https://ulozto.net/file/Z9wKxVoRiqEl/007-mp4'
            ],
            'filecm' => [
                'https://file.cm/pb2vx6b97tpj'
            ],
            'cloudvideo' => [
                'https://cloudvideo.tv/6ixdmrfrfalm',
                'https://cloudvideo.tv/embed-6ixdmrfrfalm.html'
            ],
            'fireload' => [
                'https://www.fireload.com/873944eb0a45b5ac',
                'https://www.fireload.com/873944eb0a45b5ac/007.mp4'
            ],
            'pandafiles' => [
                'https://pandafiles.com/oce0i01feylq',
                'https://pandafiles.com/oce0i01feylq/Ujicoba_video_tampilkan_Gunung_Sinabung.mp4'
            ],
            'streamff' => [
                'https://streamff.com/v/1a2769'
            ],
            'streamhub' => [
                'https://streamhub.to/a9dzoiiuyl8v',
                'https://streamhub.to/embed-a9dzoiiuyl8v.html'
            ],
            'vtube' => [
                'https://vtube.to/klhci40kha5z.html',
                'https://vtube.to/embed-klhci40kha5z.html'
            ],
            'vudeo' => [
                'https://vudeo.io/fh74pouftjwe.html',
                'https://vudeo.io/embed-fh74pouftjwe.html'
            ],
            'direct' => [
                BASE_URL . '007.mp4',
                'https://bitmovin-a.akamaihd.net/content/MI20192708/master.m3u8',
                'https://bitmovin-a.akamaihd.net/content/MI20192708/stream.mpd'
            ],
            'gdrive' => [
                'https://drive.google.com/file/d/1KPtwrGMyjC9c_ZpOID4eLaSDErrA5x3O/view',
                'https://drive.google.com/open?id=1KPtwrGMyjC9c_ZpOID4eLaSDErrA5x3O'
            ],
            'fembed' => [
                'https://www.fembed.com/v/qgl1-fee-e-nrry',
                'https://www.fembed.com/f/qgl1-fee-e-nrry'
            ],
            'blogger' => [
                'https://TvHaytop.blogspot.com/2021/10/testing.html',
                'https://www.blogger.com/video.g?token=AD6v5dyn8RZEpGJxtUCAqX4HtPuhgoH3oht5gFtEj-djFdO5cJllMrQUttdNUJKpdcc_WghS07NWEr5gZZF9pEz2OjDpDpNgKqN89WNVYeUGjfzuqqdGLZiZUlDLbdofcAQVHLVEo9w'
            ],
            'youtube' => [
                'https://www.youtube.com/watch?v=7GqClqvlObY',
                'https://youtu.be/7GqClqvlObY'
            ],
            'googlephotos' => [
                'https://photos.app.goo.gl/a9SuP8tvujExReBMA',
                'https://photos.google.com/share/AF1QipPzJ2h_qMn2HpXoNquhC7YH78FBRzt3jOAq3hu_ha-_F0VnNZYp877WNeUPBHkiFA?key=OWVtLUpkWWVpbnd1cUxvQmVXU3l1RUYxZ0xKRDN3'
            ],
            'amazon' => [
                'https://www.amazon.com/clouddrive/share/ljr2Tl6Fv3UdezkJbUMBO4KweJYh5ESF700b9kyZbH7'
            ],
            'anonfile' => [
                'https://anonfiles.com/d5EeOaBfo6',
                'https://anonfiles.com/d5EeOaBfo6/SPECTRE_-_Official_Trailer_mp4'
            ],
            'archive' => [
                'https://archive.org/details/007_20211018'
            ],
            'bayfiles' => [
                'https://bayfiles.com/x8EbOcB5o7',
                'https://bayfiles.com/x8EbOcB5o7/SPECTRE_-_Official_Trailer_mp4'
            ],
            'dailymotion' => [
                'https://www.dailymotion.com/video/k2I7DtiFJm5WNmxinOX',
                'https://www.dailymotion.com/embed/video/k2I7DtiFJm5WNmxinOX'
            ],
            'dropbox' => [
                'https://www.dropbox.com/s/seap0x345nexifw/007.mp4'
            ],
            'facebook' => [
                'https://www.facebook.com/AttackOnTitan/videos/349559842677536'
            ],
            'filerio' => [
                'https://filerio.in/j2zv3d8nv2kh',
                'https://filerio.in/j2zv3d8nv2kh/007.mp4',
                'https://filerio.in/embed-j2zv3d8nv2kh.html'
            ],
            'filesfm' => [
                'https://files.fm/f/9nt87xpfm'
            ],
            'filesim' => [
                'https://files.im/9jn8dm8xxy34',
                'https://files.im/embed-9jn8dm8xxy34.html'
            ],
            'gofile' => [
                'https://gofile.io/d/nnn2rO'
            ],
            'hexupload' => [
                'https://hexupload.net/xmxfq161l46z',
                'https://hexupload.net/embed-xmxfq161l46z.html'
            ],
            'hxfile' => [
                'https://hxfile.co/q1fyhvuti0ot',
                'https://hxfile.co/embed-q1fyhvuti0ot.html'
            ],
            'indishare' => [
                'https://www.indishare.org/57mb1qyzf46r',
                'https://dl.indishare.cc/57mb1qyzf46r'
            ],
            'mediafire' => [
                'https://www.mediafire.com/file/8kov3shiy05ao7k/',
                'https://www.mediafire.com/file/8kov3shiy05ao7k/007.mp4/file'
            ],
            'mixdropto' => [
                'https://mixdrop.co/f/knnndwnjipeqm8',
                'https://mixdrop.co/e/knnndwnjipeqm8'
            ],
            'mp4upload' => [
                'https://www.mp4upload.com/kfqv40px28yi',
                'https://www.mp4upload.com/embed-kfqv40px28yi.html',
                'https://www.mp4upload.com/kfqv40px28yi/007.mp4'
            ],
            'okru' => [
                'https://ok.ru/video/1726154213914',
                'https://ok.ru/videoembed/1726154213914'
            ],
            'onedrive' => [
                'https://onedrive.live.com/embed?resid=2D375281D3105D45%2123209',
                'https://onedrive.live.com/?cid=2D375281D3105D45&id=2D375281D3105D45%2123209&parId=2D375281D3105D45%21105&o=OneUp'
            ],
            'pcloud' => [
                'https://e.pcloud.link/publink/show?code=XZavK5Zgbl8Ab9XXbppK2cOwScLqV0YFY1V'
            ],
            'racaty' => [
                'https://racaty.net/73urj8yd817s',
                'https://racaty.net/embed-73urj8yd817s.html'
            ],
            'rumble' => [
                'https://rumble.com/v2znie-bond-is-back-with-a-trailer-for-spectre.html',
                'https://rumble.com/embed/vb83k/'
            ],
            'solidfiles' => [
                'https://www.solidfiles.com/v/Q4D5D2X2AB4GR',
                'https://www.solidfiles.com/e/Q4D5D2X2AB4GR'
            ],
            'soundcloud' => [
                'https://soundcloud.com/dengerin-musik-797697733/habib-syech-bin-abdul-qodir-assegaf-padang-wulan'
            ],
            'streamable' => [
                'https://streamable.com/nqfrzj',
                'https://streamable.com/e/nqfrzj'
            ],
            'streamsb' => [
                'https://streamsb.net/vgr45s4harga',
                'https://streamsb.net/play/vgr45s4harga',
                'https://streamsb.net/embed-vgr45s4harga.html'
            ],
            'streamtape' => [
                'https://streamtape.com/v/9bj93zpV3jca4Zy',
                'https://strcloud.link/e/9bj93zpV3jca4Zy'
            ],
            'tiktok' => [
                'https://www.tiktok.com/@lyothecat/video/6766975139570633990'
            ],
            'uploadsmobi' => [
                'https://uploads.mobi/n3h60rulhwdd',
                'https://uploads.mobi/embed-n3h60rulhwdd.html'
            ],
            'upstream' => [
                'https://upstream.to/da8n1o2pblj8.html',
                'https://upstream.to/embed-da8n1o2pblj8.html'
            ],
            'uptobox' => [
                'https://uptobox.com/3hwqnzqh9061',
                'https://uptostream.com/3hwqnzqh9061',
                'https://uptostream.com/iframe/3hwqnzqh9061'
            ],
            'uqload' => [
                'https://uqload.com/tlf4hore8oag.html',
                'https://uqload.com/embed-tlf4hore8oag.html'
            ],
            'userscloud' => [
                'https://userscloud.com/neijebdgw9km',
                'https://userscloud.com/embed-neijebdgw9km.html'
            ],
            'videobin' => [
                'https://videobin.co/ybqxhzbg0nil',
                'https://videobin.co/embed-ybqxhzbg0nil.html'
            ],
            'vidoza' => [
                'https://vidoza.net/q5yla9g4ztxk.html',
                'https://vidoza.net/q5yla9g4ztxk/spectre_official_trailer',
                'https://vidoza.net/embed-q5yla9g4ztxk.html'
            ],
            'vimeo' => [
                'https://vimeo.com/259411563',
                'https://player.vimeo.com/video/259411563'
            ],
            'vupto' => [
                'https://vupload.com/f2polvplm1jm',
                'https://vupload.com/v/f2polvplm1jm',
                'https://vupload.com/embed-f2polvplm1jm.html'
            ],
            'yadisk' => [
                'https://disk.yandex.ru/i/jUCaMeoCKepLUw',
                'https://yadi.sk/i/jUCaMeoCKepLUw'
            ],
            'yourupload' => [
                'https://www.yourupload.com/watch/SFy6T2PbbGWi',
                'https://www.yourupload.com/embed/SFy6T2PbbGWi'
            ],
            'zippyshare' => [
                'https://www70.zippyshare.com/v/gZ55XaiU/file.html'
            ],
            'zplayer' => [
                'https://v2.zplayer.live/video/6gqrvznt7t8p',
                'https://v2.zplayer.live/embed/6gqrvznt7t8p'
            ],
        );
        if (!empty($host)) {
            if (isset($list[$host])) return $list[$host];
            return [];
        } else {
            return $list;
        }
    }

    static function linkFormatExample()
    {
        session_write_close();
        $html = '';
        $list = self::supportedSites();
        foreach ($list as $key => $val) {
            $links = self::linkExample($key);
            if (!empty($links) && (strpos($val, 'Additional') === false || (strpos($val, 'Additional') !== false && class_exists($key)))) {
                $link = '';
                foreach ($links as $dt) {
                    $link .= '<input type="url" readonly onfocus="this.select()" class="form-control form-control-sm mb-1" value="' . $dt . '" aria-label="Example Link">';
                }
                list($val, $badge) = array_pad(explode('|', $val), 2, '');
                $html .= '<tr data-host="' . $key . '">
                        <td><img alt="' . $val . '" title="' . $val . '" width="16" height="' . ($key === 'hxfile' ? '14' : '16') . '" src="' . BASE_URL . 'assets/img/logo/' . $key . '.png"></td>
                        <td>' . $val . (!empty($badge) ? ' <span class="badge badge-success">' . $badge . '</span>' : '') . '</td>
                        <td>' . get_host_status($key, TRUE) . '</td>
                        <td>' . $link . '</td>
                    </tr>';
            }
        }
        return $html;
    }

    private function hostExtractor()
    {
        session_write_close();
        $host = parse_url($this->url, PHP_URL_HOST);
        if (strpos($host, 'zippyshare.com') !== FALSE) {
            $this->host = 'zippyshare';
            $this->id = $this->url;
        } elseif (strpos($host, 'vidio.com') !== FALSE) {
            $this->host = 'vidio';
            $this->id = $this->url;
        } elseif (strpos($host, 'sharepoint.com') !== FALSE || ($host === 'onedrive.live.com' && strpos($this->url, 'authkey=') !== FALSE)) {
            $this->host = 'onedrive';
            $this->id = $this->url;
        } elseif ($host === 'ulozto.net') {
            $this->host = 'ulozto';
        } elseif ($host === 'file.cm') {
            $this->host = 'filecm';
        } elseif ($host === 'my.mail.ru') {
            $this->host = 'mymailru';
        } elseif ($host === 'drive.google.com') {
            $this->host = 'gdrive';
        } elseif (substr($host, 0, 6) === 'photos') {
            $this->host = 'googlephotos';
        } elseif ($host === 'onedrive.live.com') {
            $this->host = 'onedrive';
        } elseif (preg_match('/([^.]+)\.[^.]+$/', $host, $dt) && isset($dt[1])) {
            $withTLD = ['files', 'mixdrop', 'ok', 'uploads', 'vup', 'yadi', 'youtu'];
            if (in_array($dt[1], $withTLD)) {
                $this->host = strtr($host, ['.' => '']);
            } else {
                preg_match('/solidfiles|streamtape|streamff|streamlare|mail.ru|embedgram|.m3u|.mpd/', $this->url, $data);
                if ((strpos($this->url, '/f/') !== FALSE || strpos($this->url, '/v/') !== FALSE)) {
                    if (!empty($data)) {
                        list($this->host, $trash) = array_pad(explode('.', $host), 2, '');
                    } else {
                        $this->host = 'fembed';
                        $this->id = $this->url;
                    }
                } elseif (strpos($this->url, 'blogspot') !== FALSE) {
                    $this->host = 'blogger';
                    $this->id = $this->url;
                } else {
                    $host = $dt[1];
                    $core = new \Core();
                    $hosts = array_merge($core->bypass_host(), $core->direct_host());
                    if (!in_array($host, $hosts)) {
                        $this->host = 'direct';
                    } else {
                        $this->host = $host;
                    }
                }
            }
        }
    }

    function setHost(string $host = '')
    {
        session_write_close();
        $this->host = $host;
    }

    function getHost()
    {
        session_write_close();
        return $this->host;
    }

    private function idExtractor()
    {
        session_write_close();
        if (empty($this->id)) {
            $query = parse_url($this->url, PHP_URL_QUERY);
            parse_str($query, $qry);
            if ($this->host === 'gdrive') {
                $this->id = getDriveId($this->url);
            } elseif ($this->host === 'googlephotos') {
                if (isset($qry['key'])) {
                    $ex = explode('/share/', $this->url, 2);
                } else {
                    $ex = explode('/', trim($this->url, '/'));
                }
                $this->id = end($ex);
            } elseif (in_array($this->host, $this->urlIDHost)) {
                if (strpos($this->host, 'blogger') !== false && isset($qry['token'])) {
                    $this->id = $qry['token'];
                } else {
                    $this->id = $this->url;
                }
            } elseif (in_array($this->host, $this->qryIDHost) && !empty($qry)) {
                if ($this->host === 'youtube') {
                    $this->id = $qry['v'];
                } elseif ($this->host === 'blogger') {
                    $this->id = $qry['token'];
                } elseif ($this->host === 'onedrive') {
                    $this->id = http_build_query($qry);
                }
            } else {
                $path = trim(parse_url($this->url, PHP_URL_PATH), '/');
                $exPath = explode('/', $path);
                if (in_array($this->host, $this->pathIDHost)) {
                    $this->id = $path;
                } elseif (in_array($this->host, $this->zeroIDHost) && isset($exPath[0])) {
                    $this->id = strtr($exPath[0], ['embed-' => '', '.html' => '']);
                } elseif (in_array($this->host, $this->oneIDHost) && isset($exPath[1])) {
                    $this->id = strtr($exPath[1], ['embed-' => '', '.html' => '']);
                } elseif (in_array($this->host, $this->pathQryHost)) {
                    $this->id = $path . '?' . $query;
                } else {
                    $this->id = strtr(end($exPath), ['embed-' => '', '.html' => '']);
                }
            }
        }
    }

    function setID($id)
    {
        session_write_close();
        $this->id = $id;
    }

    function getID()
    {
        session_write_close();
        return $this->id;
    }

    function getDownloadLink()
    {
        session_write_close();
        if (!validate_url($this->id)) {
            if ($this->host === 'uploadbuzz') {
                return 'https://uploadbuzz.cc/' . $this->id;
            } elseif ($this->host === 'embedgram') {
                return 'https://embedgram.com/v/' . $this->id;
            } elseif ($this->host === 'streamlare') {
                return 'https://streamlare.com/v/' . $this->id;
            } elseif ($this->host === 'voe') {
                return 'https://voe.sx/' . $this->id;
            } elseif ($this->host === 'supervideo') {
                return 'https://supervideo.tv/' . $this->id;
            } elseif ($this->host === 'uqload') {
                return 'https://uqload.com/' . $this->id . '.html';
            } elseif ($this->host === 'dailymotion') {
                return 'https://www.dailymotion.com/video/' . $this->id;
            } elseif ($this->host === 'pcloud') {
                return 'https://u.pcloud.link/publink/show?code=' . $this->id;
            } elseif ($this->host === 'archive') {
                return 'https://archive.org/details/' . $this->id;
            } elseif ($this->host === 'tiktok') {
                return 'https://www.tiktok.com/' . strtr($this->id, ['~' => '/']);
            } elseif ($this->host === 'uptobox') {
                return 'https://uptobox.com/' . $this->id;
            } elseif ($this->host === 'hexupload') {
                return 'https://hexupload.net/' . $this->id;
            } elseif ($this->host === 'amazon') {
                return 'https://www.amazon.com/clouddrive/share/' . $this->id;
            } elseif ($this->host === 'vimeo') {
                return 'https://vimeo.com/' . $this->id;
            } elseif ($this->host === 'zplayer') {
                return 'https://v2.zplayer.live/video/' . $this->id;
            } elseif ($this->host === 'mediafire') {
                return 'https://www.mediafire.com/file/' . $this->id;
            } elseif ($this->host === 'yourupload') {
                return 'https://yourupload.com/watch/' . $this->id;
            } elseif ($this->host === 'streamsb') {
                return 'https://streamsb.net/' . $this->id . '.html';
            } elseif ($this->host === 'yadisk') {
                return 'https://yadi.sk/i/' . $this->id;
            } elseif ($this->host === 'facebook') {
                return 'https://www.facebook.com/' . $this->id;
            } elseif ($this->host === 'dropbox') {
                return 'https://www.dropbox.com/s/' . $this->id;
            } elseif ($this->host === 'gdrive') {
                return 'https://drive.google.com/file/d/' . $this->id . '/view';
            } elseif ($this->host === 'googlephotos') {
                if (strlen($this->id) > 17) {
                    return 'https://photos.google.com/share/' . $this->id;
                } else {
                    return 'https://photos.app.goo.gl/' . $this->id;
                }
            } elseif ($this->host === 'youtube') {
                return 'https://youtube.com/watch?v=' . $this->id;
            } elseif ($this->host === 'blogger') {
                if (validate_url($this->id)) {
                    return $this->id;
                } else {
                    return 'https://www.blogger.com/video.g?token=' . $this->id;
                }
            } elseif ($this->host === 'anonfile') {
                return 'https://anonfiles.com/' . $this->id;
            } elseif ($this->host === 'bayfiles') {
                return 'https://bayfiles.com/' . $this->id;
            } elseif ($this->host === 'filerio') {
                return 'https://filerio.in/' . $this->id . '.html';
            } elseif ($this->host === 'filesim') {
                return 'https://files.im/' . $this->id;
            } elseif ($this->host === 'gofile') {
                return 'https://gofile.io/d/' . $this->id;
            } elseif ($this->host === 'hxfile') {
                return 'https://hxfile.co/' . $this->id;
            } elseif ($this->host === 'indishare') {
                return 'https://www.indishare.org/' . $this->id;
            } elseif ($this->host === 'mixdropto') {
                return 'https://mixdrop.sx/f/' . $this->id;
            } elseif ($this->host === 'mp4upload') {
                return 'https://mp4upload.com/' . $this->id . '.html';
            } elseif ($this->host === 'onedrive') {
                if (validate_url($this->id)) {
                    return $this->id;
                } else {
                    $this->id = strpos($this->id, 'resid') !== FALSE ? $this->id : 'resid=' . $this->id;
                    return 'https://onedrive.live.com/embed?' . $this->id;
                }
            } elseif ($this->host === 'racaty') {
                return 'https://racaty.net/' . $this->id;
            } elseif ($this->host === 'solidfiles') {
                return 'https://solidfiles.com/v/' . $this->id;
            } elseif ($this->host === 'uploadsmobi') {
                return 'https://uploads.mobi/' . $this->id;
            } elseif ($this->host === 'userscloud') {
                return 'https://userscloud.com/' . $this->id;
            } elseif ($this->host === 'vidoza') {
                return 'https://vidoza.net/' . $this->id . '.html';
            } elseif ($this->host === 'vupto') {
                return 'https://vupload.com/' . $this->id . '.html';
            } elseif ($this->host === 'streamable') {
                return 'https://streamable.com/' . $this->id;
            } elseif ($this->host === 'okru') {
                return 'https://ok.ru/video/' . $this->id;
            } elseif ($this->host === 'videobin') {
                return 'https://videobin.co/' . $this->id;
            } elseif ($this->host === 'vidlox') {
                return 'https://vidlox.me/' . $this->id . '.html';
            } elseif ($this->host === 'streamtape') {
                return 'https://streamadblockplus.com/e/' . $this->id;
            } elseif ($this->host === 'upstream') {
                return 'https://upstream.to/' . $this->id . '.html';
            } elseif ($this->host === 'rumble') {
                return 'https://rumble.com/' . $this->id;
            } elseif ($this->host === 'cloudvideo') {
                // additional host
                return 'https://cloudvideo.tv/' . $this->id;
            } elseif ($this->host === 'filecm') {
                // additional host
                return 'https://file.cm/' . $this->id;
            } elseif ($this->host === 'fireload') {
                // additional host
                return 'https://www.fireload.com/' . $this->id;
            } elseif ($this->host === 'pandafiles') {
                // additional host
                return 'https://pandafiles.com/' . $this->id;
            } elseif ($this->host === 'ulozto') {
                // additional host
                return 'https://ulozto.net/file/' . $this->id;
            } elseif ($this->host === 'streamff') {
                // additional host
                return 'https://streamff.com/v/' . $this->id;
            } elseif ($this->host === 'streamhub') {
                // additional host
                return 'https://streamhub.to/' . $this->id;
            } elseif ($this->host === 'vtube') {
                // additional host
                return 'https://vtube.to/' . $this->id . '.html';
            } elseif ($this->host === 'vudeo') {
                // additional host
                return 'https://vudeo.io/' . $this->id;
            } elseif ($this->host === 'mymailru') {
                // additional host
                return 'https://my.mail.ru/video/embed/' . $this->id;
            } elseif ($this->host === 'sendvid') {
                // additional host
                return 'https://sendvid.com/' . $this->id;
            } elseif ($this->host === 'sibnet') {
                // additional host
                return 'https://video.sibnet.ru/' . $this->id;
            } elseif ($this->host === 'desustream') {
                // additional host
                return 'https://desustream.me/' . $this->id;
            } elseif ($this->host === 'gomunime') {
                // additional host
                return 'https://gomunime.one/' . $this->id;
            } elseif ($this->host === 'kuronime') {
                // additional host
                return 'https://kuronime.tv/' . $this->id;
            } elseif ($this->host === 'uservideo') {
                // additional host
                return 'https://uservideo.xyz/file/' . $this->id . '?embed=true';
            }
        }
        return $this->id;
    }

    function __destruct()
    {
        session_write_close();
    }
}
