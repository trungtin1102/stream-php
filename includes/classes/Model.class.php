<?php
class Model
{
    protected $db;
    protected $table = '';
    protected $fields = [];
    protected $primaryKey = 'id';
    protected $params = [];
    protected $keyParams = [];
    protected $whereQuery = '';
    protected $orderByQuery = '';
    protected $limitQuery = '';
    protected $groupByQuery = '';
    protected $joinCriteria = '';
    protected $joinQuery = '';
    protected $sqlQuery = '';
    protected $errors = [];
    protected $iCache;
    protected $cacheTimeout = 10;
    protected $cacheKey = '';

    function __construct()
    {
        session_write_close();
        global $db;
        $this->db = $db;
        $this->iCache = new \InstanceCache();
    }

    function setTable(string $table = '')
    {
        session_write_close();
        $this->table = $table;
    }

    function setPrimaryKey(string $key = '')
    {
        session_write_close();
        $this->primaryKey = $key;
    }

    function setFields(array $fields = [])
    {
        session_write_close();
        $this->fields = $fields;
    }

    function setLimit(int $start = 0, int $length = 0)
    {
        session_write_close();
        if ($length > 0) {
            $this->limitQuery = sprintf(' LIMIT %d, %d ', $start, $length);
        }
    }

    function setOrderBy(string $field = 'id', string $sort = 'ASC')
    {
        session_write_close();
        if (!empty($field) && in_array($field, $this->fields)) {
            $this->orderByQuery = sprintf(' ORDER BY `%s` %s', $field, $sort);
        }
    }

    function setGroupBy(array $fields = [])
    {
        session_write_close();
        if (!empty($fields)) {
            $fields = implode('`,`', $fields);
            $this->groupByQuery = " GROUP BY `$fields` ";
        }
    }

    function setCriteria(string $key = '', $val = null, string $opr = '=', string $sep = '')
    {
        session_write_close();
        if (!empty($key)) {
            $this->keyParams[] = $key;
            $this->params[] = $val;

            $keyParamCount = count($this->keyParams);
            $keyIndex = $keyParamCount - 1;

            if ($keyIndex > 0 && empty($sep)) $opr = 'AND';

            $this->whereQuery .= " $sep `$key` $opr :$key$keyIndex";
        }
    }

    function serializeData(array $data = [])
    {
        session_write_close();
        $result = [];
        if (!empty($data)) {
            foreach ($data as $key => $val) {
                $result[":$key"] = is_array($val) ? json_encode($val) : $val;
            }
        } elseif (!empty($this->keyParams) && !empty($this->params)) {
            foreach ($this->keyParams as $i => $key) {
                if (isset($this->params[$i])) {
                    if (is_array($this->params[$i])) $result[":$key$i"] = json_encode($this->params[$i]);
                    elseif (is_null($this->params[$i])) $result[":$key$i"] = '';
                    else $result[":$key$i"] = $this->params[$i];
                }
            }
        }
        return $result;
    }

    function resetVar()
    {
        session_write_close();
        $this->params = [];
        $this->keyParams = [];
        $this->whereQuery = '';
        $this->orderByQuery = '';
        $this->limitQuery = '';
        $this->groupByQuery = '';
        $this->joinCriteria = '';
        $this->joinQuery = '';
        $this->sqlQuery = '';
        $this->errors = [];
    }

    function joinCriteria(string $columnLeft = '', string $columnRight = '', string $opr = '=', string $sep = '')
    {
        session_write_close();
        if (!empty($columnLeft) && !empty($columnRight)) {
            if (!empty($this->joinCriteria)) {
                $sep = !empty($sep) ? $sep : 'AND';
                $this->joinCriteria .= " $sep $columnLeft $opr $columnRight ";
            } else {
                $this->joinCriteria = " $columnLeft $opr $columnRight ";
            }
        }
    }

    function join(string $table = '', string $type = 'LEFT')
    {
        session_write_close();
        if (!empty($table) && !empty($this->joinCriteria)) {
            $this->joinQuery = " $type JOIN $table ON ({$this->joinCriteria}) ";
        }
    }

    private function getCache(string $prefix = '')
    {
        session_write_close();
        $this->cacheKey = keyFilter(strtolower($prefix . '_' . $this->sqlQuery . '_' . implode('-', array_values($this->params))));
        $this->iCache->setKey($this->cacheKey);
        return substr(trim(strtolower($this->sqlQuery)), 0, 6) === 'select' && strpos($this->cacheKey, 'cache_instance') === false ? $this->iCache->get() : false;
    }

    private function clearCache()
    {
        session_write_close();
        $this->iCache->deleteItemsByTags(['db_rawFetch', 'db_rawFetchAll', 'db_get', 'db_getOne', 'db_getTotalRows', 'db_getNumRows']);
    }

    function rawFetch(string $sql = '', array $params = [])
    {
        session_write_close();
        try {
            if (!empty($sql)) {
                $this->sqlQuery = $sql;
                $this->params = $params;

                $cache = $this->getCache('rawFetch');
                if ($cache) {
                    return $cache;
                } else {
                    $qry = $this->db->prepare($this->sqlQuery);
                    $qry->execute($this->params);
                    $result = $qry->fetch(\PDO::FETCH_ASSOC);

                    $this->iCache->save($result, $this->cacheTimeout, 'db_rawFetch');

                    $qry = null;
                    unset($qry);
                    $this->resetVar();

                    return $result;
                }
            } else {
                $this->errors[] = 'Insert the sql query first';
            }
        } catch (\PDOException | \Exception $e) {
            $this->errors[] = $e->getMessage();
        }
        return false;
    }

    function rawFetchAll(string $sql = '', array $params = [])
    {
        session_write_close();
        try {
            if (!empty($sql)) {
                $this->sqlQuery = $sql;
                $this->params = $params;

                $cache = $this->getCache('rawFetchAll');
                if ($cache) {
                    return $cache;
                } else {
                    $qry = $this->db->prepare($sql);
                    $qry->execute($params);
                    $result = $qry->fetchAll(\PDO::FETCH_ASSOC);

                    $this->iCache->save($result, $this->cacheTimeout, 'db_rawFetchAll');

                    $qry = null;
                    unset($qry);
                    $this->resetVar();

                    return $result;
                }
            } else {
                $this->errors[] = 'Insert the sql query first';
            }
        } catch (\PDOException | \Exception $e) {
            $this->errors[] = $e->getMessage();
        }
        return false;
    }

    function rawQuery(string $sql = '')
    {
        session_write_close();
        try {
            if (!empty($sql)) {
                $this->sqlQuery = $sql;
                $qry = $this->db->query($sql);
                $this->resetVar();
                return $qry;
            } else {
                $this->errors[] = 'Insert the sql query first';
            }
        } catch (\PDOException | \Exception $e) {
            $this->errors[] = $e->getMessage();
        }
        return false;
    }

    function get(array $fields = [])
    {
        session_write_close();
        try {
            if (!empty($this->table)) {
                if (!empty($fields)) {
                    $fieldsQuery =  '`' . implode('`,`', $fields) . '`';
                } elseif (!empty($this->fields)) {
                    $fieldsQuery =  '`' . implode('`,`', $this->fields) . '`';
                } else {
                    $fieldsQuery = '*';
                }

                $params = $this->serializeData();
                $this->params = $params;

                $whereQuery = !empty($this->whereQuery) ? 'WHERE' : '';
                $whereQuery .= $this->whereQuery;

                $this->sqlQuery = "SELECT $fieldsQuery FROM `{$this->table}` {$this->joinQuery} {$whereQuery} {$this->groupByQuery} {$this->orderByQuery} {$this->limitQuery}";

                $cache = $this->getCache('get');
                if ($cache) {
                    return $cache;
                } else {
                    $qry = $this->db->prepare($this->sqlQuery);
                    $qry->execute($params);
                    $result = $qry->fetchAll(\PDO::FETCH_ASSOC);

                    if (strpos($this->cacheKey, 'cache_instance') === false) {
                        $this->iCache->save($result, $this->cacheTimeout, 'db_get');
                    }

                    $qry = null;
                    unset($qry);
                    $this->resetVar();

                    return $result;
                }
            } else {
                $this->errors[] = 'Set the table first';
            }
        } catch (\PDOException | \Exception $e) {
            $this->errors[] = $e->getMessage();
        }
        return false;
    }

    function getOne(array $fields = [])
    {
        session_write_close();
        try {
            if (!empty($this->table)) {
                if (!empty($fields)) {
                    $fieldsQuery =  '`' . implode('`,`', $fields) . '`';
                } elseif (!empty($this->fields)) {
                    $fieldsQuery =  '`' . implode('`,`', $this->fields) . '`';
                } else {
                    $fieldsQuery = '*';
                }

                $params = $this->serializeData();
                $this->params = $params;

                $whereQuery = !empty($this->whereQuery) ? 'WHERE' : '';
                $whereQuery .= $this->whereQuery;

                $this->sqlQuery = "SELECT $fieldsQuery FROM `{$this->table}` {$this->joinQuery} {$whereQuery}";

                $cache = $this->getCache('getOne');
                if ($cache) {
                    return $cache;
                } else {
                    $qry = $this->db->prepare($this->sqlQuery);
                    $qry->execute($params);
                    $result = $qry->fetch(\PDO::FETCH_ASSOC);

                    if (strpos($this->cacheKey, 'cache_instance') === false) {
                        $this->iCache->save($result, $this->cacheTimeout, 'db_getOne');
                    }

                    $qry = null;
                    unset($qry);
                    $this->resetVar();

                    return $result;
                }
            } else {
                $this->errors[] = 'Set the table first';
            }
        } catch (\PDOException | \Exception $e) {
            $this->errors[] = $e->getMessage();
        }
        return false;
    }

    function getTotalRows()
    {
        session_write_close();
        try {
            if (!empty($this->table)) {
                $this->sqlQuery = "SELECT COUNT(`{$this->primaryKey}`) FROM `{$this->table}` {$this->joinQuery} {$this->groupByQuery}";

                $cache = $this->getCache('getTotalRows');
                if ($cache) {
                    return (int) $cache;
                } else {
                    $qry = $this->db->prepare($this->sqlQuery);
                    $qry->execute();
                    $result = $qry->fetchColumn();

                    $this->iCache->save($result, $this->cacheTimeout, 'db_getTotalRows');

                    $qry = null;
                    unset($qry);
                    $this->resetVar();

                    if ($result) return (int) $result;
                }
            } else {
                $this->errors[] = 'Set the table first';
            }
        } catch (\PDOException | \Exception $e) {
            $this->errors[] = $e->getMessage();
        }
        return 0;
    }

    function getNumRows()
    {
        session_write_close();
        try {
            if (!empty($this->table)) {
                $params = $this->serializeData();
                $this->params = $params;

                $whereQuery = !empty($this->whereQuery) ? 'WHERE' : '';
                $whereQuery .= $this->whereQuery;

                $this->sqlQuery = "SELECT COUNT(`{$this->primaryKey}`) FROM `{$this->table}` {$this->joinQuery} {$whereQuery} {$this->groupByQuery}";

                $cache = $this->getCache('getNumRows');
                if ($cache) {
                    return (int) $cache;
                } else {
                    $qry = $this->db->prepare($this->sqlQuery);
                    $qry->execute($params);
                    $result = $qry->fetchColumn();

                    $this->iCache->save($result, $this->cacheTimeout, 'db_getNumRows');

                    $qry = null;
                    unset($qry);
                    $this->resetVar();

                    if ($result) return (int) $result;
                }
            } else {
                $this->errors[] = 'Set the table first';
            }
        } catch (\PDOException | \Exception $e) {
            $this->errors[] = $e->getMessage();
        }
        return 0;
    }

    function insert(array $data = [])
    {
        session_write_close();
        try {
            if (!empty($this->table)) {
                if (!empty($data)) {
                    $fieldsQuery =  '(`' . implode('`,`', array_keys($data)) . '`)';
                    $valuesQuery = '(:' . implode(',:', array_keys($data)) . ')';

                    $params = $this->serializeData($data);
                    $this->params = $params;

                    $this->sqlQuery = "INSERT INTO `{$this->table}` $fieldsQuery VALUES $valuesQuery";
                    $qry = $this->db->prepare($this->sqlQuery);
                    $exec = $qry->execute($params);
                    if ($exec) {
                        $result = $this->db->lastInsertId($this->primaryKey);
                        $this->clearCache();
                    } else {
                        $result = false;
                    }

                    $qry = null;
                    unset($qry);
                    $this->resetVar();

                    return $result;
                } else {
                    $this->errors[] = 'Insert the data you want to save first';
                }
            } else {
                $this->errors[] = 'Set the table first';
            }
        } catch (\PDOException | \Exception $e) {
            $this->errors[] = $e->getMessage();
        }
        return false;
    }

    function update(array $data = [])
    {
        session_write_close();
        try {
            if (!empty($this->table)) {
                if (!empty($this->keyParams)) {
                    $params = $this->serializeData();
                    $params = array_merge($params, $this->serializeData($data));

                    $keys = array_keys($data);
                    $fieldsQuery = '';
                    foreach ($keys as $key) {
                        $fieldsQuery .= "`$key` = :$key, ";
                    }
                    $fieldsQuery = trim($fieldsQuery, ', ');

                    $whereQuery = !empty($this->whereQuery) ? 'WHERE' : '';
                    $whereQuery .= $this->whereQuery;

                    $this->sqlQuery = "UPDATE `{$this->table}` SET $fieldsQuery {$whereQuery} {$this->limitQuery}";

                    $qry = $this->db->prepare($this->sqlQuery);
                    $updated = $qry->execute($params);

                    $this->clearCache();

                    $qry = null;
                    unset($qry);
                    $this->resetVar();

                    return $updated;
                } else {
                    $this->errors[] = 'First set the criteria for the data you want to update';
                }
            } else {
                $this->errors[] = 'Set the table first';
            }
        } catch (\PDOException | \Exception $e) {
            $this->errors[] = $e->getMessage();
        }
        return false;
    }

    function delete()
    {
        session_write_close();
        try {
            if (!empty($this->table)) {
                if (!empty($this->keyParams)) {
                    $params = $this->serializeData();

                    $whereQuery = !empty($this->whereQuery) ? 'WHERE' : '';
                    $whereQuery .= $this->whereQuery;

                    $this->sqlQuery = "DELETE FROM `{$this->table}` {$whereQuery}";

                    $qry = $this->db->prepare($this->sqlQuery);
                    $deleted = $qry->execute($params);
                    $qry = null;
                    unset($qry);

                    $qry = $this->db->query("ALTER TABLE `{$this->table}` AUTO_INCREMENT=1");

                    $this->clearCache();
                    
                    $qry = null;
                    unset($qry);
                    $this->resetVar();

                    return $deleted;
                } else {
                    $this->errors[] = 'First set the criteria for the data you want to delete';
                }
            } else {
                $this->errors[] = 'Set the table first';
            }
        } catch (\PDOException | \Exception $e) {
            $this->errors[] = $e->getMessage();
        }
        return false;
    }

    function getErrors()
    {
        session_write_close();
        return $this->errors;
    }

    function getLastError()
    {
        session_write_close();
        return end($this->errors);
    }

    function __destruct()
    {
        session_write_close();
        $this->resetVar();
    }
}
