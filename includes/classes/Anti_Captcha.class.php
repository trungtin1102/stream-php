<?php
class Anti_Captcha
{
    private $apiKey = '';
    private $websiteURL = '';
    private $websiteKey = '';
    private $proxy = [];
    private $apiBaseUrl = 'https://api.anti-captcha.com/';
    private $ch;

    function __construct()
    {
        session_write_close();

        $scheme = parse_url($this->apiBaseUrl, PHP_URL_SCHEME);
        $host = parse_url($this->apiBaseUrl, PHP_URL_HOST);
        $port = parse_url($this->apiBaseUrl, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host . '.');
        $resolveHost = array(implode(':', array($host, $port, $ipv4)));

        $this->apiKey = get_option('anti_captcha');

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_RESOLVE, $resolveHost);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($this->ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($this->ch, CURLOPT_TCP_KEEPALIVE, true);
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $line) {
            session_write_close();
            return strlen($line);
        });
    }

    function set_websiteURL(string $url = '')
    {
        session_write_close();
        $this->websiteURL = $url;
    }

    function set_websiteKey(string $gcaptcha = '')
    {
        session_write_close();
        $this->websiteKey = $gcaptcha;
    }

    function set_proxy(string $ip = '', int $port = 80, string $user = '', string $pass = '', string $type = 'http')
    {
        session_write_close();
        $this->proxy = array(
            "proxyType" => $type,
            "proxyAddress" => $ip,
            "proxyPort" => $port,
            "proxyLogin" => $user,
            "proxyPassword" => $pass,
            "userAgent" => USER_AGENT
        );
    }

    function createTask(string $type = 'RecaptchaV2TaskProxyless')
    {
        session_write_close();
        if (!empty($this->apiKey)) {
            $post = array(
                "clientKey" => $this->apiKey,
                "task" => array(
                    "type" => $type,
                    "websiteURL" => $this->websiteURL,
                    "websiteKey" => $this->websiteKey
                )
            );
            if (!empty($this->proxy) && strpos($type, 'Proxyless') === FALSE) {
                $post = array_merge($post, $this->proxy);
            }
            curl_setopt($this->ch, CURLOPT_URL, $this->apiBaseUrl . 'createTask');
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($post));
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json'
            ));

            $response = curl_exec($this->ch);
            $err = curl_error($this->ch);

            if (!$err) {
                return json_decode($response, true);
            }
        }
        return FALSE;
    }

    function getTaskResult(int $taskId = 0)
    {
        session_write_close();
        if (!empty($this->apiKey)) {
            $post = json_encode(array(
                "clientKey" => $this->apiKey,
                "taskId" => $taskId
            ));
            curl_setopt($this->ch, CURLOPT_URL, $this->apiBaseUrl . 'getTaskResult');
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json'
            ));

            $response = curl_exec($this->ch);
            $err = curl_error($this->ch);

            if (!$err) {
                return json_decode($response, true);
            }
        }
        return FALSE;
    }

    function __destruct()
    {
        session_write_close();
        curl_close($this->ch);
    }
}
