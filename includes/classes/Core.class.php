<?php
class Core
{
    private $bypass = ['streamlare', 'onedrive', 'uploadbuzz', 'vudeo', 'blogger', 'viu', 'vidio', 'supervideo', 'voe', 'uqload', 'gofile', 'bayfiles', 'yadisk', 'anonfile', 'uploadsmobi', 'filesim', 'zippyshare', 'dropbox', 'fembed', 'filerio', 'gdrive', 'mixdropto', 'vidoza', 'yourupload', 'upstream', 'okru', 'mp4upload', 'mediafire', 'streamsb', 'tiktok', 'streamtape', 'dailymotion', 'pcloud', 'sibnet', 'sendvid', 'mymailru', 'gomunime', 'filecm', 'facebook', 'uptobox', 'racaty', 'solidfiles'];
    private $direct = ['vupto', 'yodbox', 'vlive', 'hxfile', 'streamff', 'ulozto', 'streamhub', 'cloudvideo', 'vtube', 'userscloud', 'fireload', 'soundcloud', 'pandafiles', 'amazon', 'hexupload', 'rumble', 'direct', 'googlephotos', 'vimeo', 'filesfm', 'zplayer', 'videobin', 'indishare', 'streamable', 'archive', 'youtube', 'desustream', 'kuronime', 'neonime', 'uservideo'];
    private $usingMP4HLS = ['dropbox', 'okru', 'videobin', 'vimeo', 'yadisk', 'zplayer'];
    private $badHosts = ['supervideo', 'vidoza', 'solidfiles', 'mixdropto', 'mp4upload', 'uploadsmobi', 'racaty', 'anonfile', 'bayfiles', 'vupto', 'userscloud', 'uptobox', 'gofile', 'sendvid', 'uqload', 'onedrive', 'upstream']; // Bad hosts mean they have the traffic control enabled
    private $query = [];
    private $email = '';
    private $download = false;
    private $server_id = 0;

    function __construct(array $query = [])
    {
        session_write_close();

        $this->query = $query;

        if (!empty($query['email'])) {
            $this->email = filter_var(rawurldecode($query['email']), FILTER_SANITIZE_EMAIL);
        }

        $bypass = get_option('bypass_host');
        if (!empty($bypass)) {
            $bypass = is_array($bypass) ? $bypass : json_decode($bypass, TRUE);
            $hosts = array_values(array_unique(array_merge($this->bypass, $this->direct, $bypass)));
            $direct = [];
            foreach ($hosts as $v) {
                if (!in_array($v, $bypass)) {
                    $direct[] = $v;
                }
            }
            $this->bypass = $bypass;
            $this->direct = $direct;
        } else {
            $this->bypass = [];
        }

        $host = parse_url(BASE_URL, PHP_URL_HOST);
        $class = new \LoadBalancers();
        $class->setCriteria('link', "%//$host%", 'LIKE');
        $row = $class->getOne(['id']);
        if ($row) $this->server_id = intval($row['id']);
    }

    function bad_hosts()
    {
        session_write_close();
        return $this->badHosts;
    }

    function set_server_id(int $id = 0)
    {
        session_write_close();
        $this->server_id = $id;
    }

    function set_download(bool $dl = true)
    {
        session_write_close();
        $this->download = $dl;
    }

    function set_query(array $qry = [])
    {
        session_write_close();
        $this->query = $qry;
    }

    function result()
    {
        session_write_close();
        if (!empty($this->query['host']) && !empty($this->query['id'])) {
            $disabled_hosts = get_option('disable_host');
            if (!empty($disabled_hosts)) {
                $disabled_hosts = is_array($disabled_hosts) ? $disabled_hosts : json_decode($disabled_hosts, TRUE);
            } else {
                $disabled_hosts = [];
            }
            if (!in_array($this->query['host'], $disabled_hosts)) {
                $cache = $this->getCache($this->query['host'], $this->query['id'], $this->download);
                if ($cache) {
                    return json_decode($cache['data'], TRUE);
                } else {
                    // cek class host
                    $className = strtr(strtolower($this->query['host']), ['amazondrive' => 'amazon']);
                    // cek class
                    if (class_exists($className)) {
                        if ($this->query['host'] === 'gdrive') {
                            $class = new $className($this->query['id'], $this->email);
                        } else {
                            $class = new $className($this->query['id']);
                        }

                        // jika cache tidak ditemukan
                        // ambil data video dari hosting
                        $sources = $class->get_sources($this->download);
                        if (!empty($sources)) {
                            $title = htmlspecialchars($class->get_title());
                            $image = $class->get_image();
                            $referer = $class->get_referer();
                            $email = method_exists($class, 'get_email') ? $class->get_email() : '';
                            $tracks = method_exists($class, 'get_tracks') ? $class->get_tracks() : [];
                            $cookies = method_exists($class, 'get_cookies') ? $class->get_cookies() : [];
                            $logURL = method_exists($class, 'get_logURL') ? $class->get_logURL() : '';

                            $data = array(
                                'sources'   => $sources,
                                'tracks'    => $tracks,
                                'referer'   => $referer,
                                'title'     => $title,
                                'email'     => $email,
                                'image'     => $image,
                                'cookies'   => $cookies,
                                'log_url'   => $logURL
                            );

                            $this->updateCache($data, $this->query['host'], $this->query['id'], $this->download);

                            return $data;
                        }
                    }
                }
            }
        }
        return [
            'tracks'    => [],
            'sources'   => [],
            'title'     => '',
            'image'     => '',
            'referer'   => '',
            'email'     => '',
            'cookies'   => []
        ];
    }

    function bypass_host()
    {
        session_write_close();
        return $this->bypass;
    }

    function direct_host()
    {
        session_write_close();
        return $this->direct;
    }

    function host_using_MP4HLS()
    {
        session_write_close();
        return $this->usingMP4HLS;
    }

    function timeout(string $host = 'gdrive')
    {
        session_write_close();
        switch ($host) {
            case 'amazon':
            case 'googlephotos':
                return 6500;

            case 'vimeo':
            case 'onedrive':
                return 3300;
                break;

            case 'vlive':
            case 'zplayer':
                return 41800;
                break;

            case 'yodbox':
            case 'pcloud':
            case 'okru':
            case 'archive':
            case 'blogger':
            case 'youtube':
                return 20798;
                break;

            case 'uploadbuzz':
            case 'viu':
            case 'tiktok':
            case 'streamable':
            case 'rumble':
            case 'filesfm':
            case 'dropbox':
                return 82800;
                break;

            case 'streamsb':
                return 7200;
                break;

            default:
                return 13200;
                break;
        }
    }

    function create_video_hash(string $host = '', string $id = '', string $email = '')
    {
        session_write_close();
        if (!empty($host) && !empty($id)) {
            $hash_host = md5(strrev($host));
            $hash_id = md5(strrev($id));
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);

            $result = array(
                'host' => $host,
                'host_id' => $id,
                'gdrive_email' => $email,
                'hash_host' => $hash_host,
                'hash_id' => $hash_id
            );

            $class = new \VideosHash();
            $class->setCriteria('host', $host, '=');
            $class->setCriteria('host_id', $id, '=', 'AND');
            if ($class->getNumRows() === 0) {
                $class->insert($result);
                return $result;
            } else {
                $class->setCriteria('host', $host, '=');
                $class->setCriteria('host_id', $id, '=', 'AND');
                $class->update([
                    'hash_host' => $hash_host,
                    'hash_id' => $hash_id
                ]);
                return $result;
            }
        }
        return FALSE;
    }

    function getCache(string $host = '', string $id = '', bool $dl = false)
    {
        session_write_close();
        if (!empty($host) && !empty($id)) {
            $dl = $dl && in_array($host, $this->usingMP4HLS) ? 1 : 0;
            $now = time();
            $iCache = new \InstanceCache();
            $iCache->setKey('video_sources~' . $host . '~' . keyFilter($id) . '~' . $dl);
            $cache = $iCache->get();
            if ($cache) {
                return $cache;
            } else {
                $class = new \VideoSources();
                $class->setCriteria('host', $host, '=');
                $class->setCriteria('host_id', $id, '=', 'AND');
                $class->setCriteria('dl', $dl, '=', 'AND');
                $class->setCriteria('expired', $now, '>', 'AND');
                $class->setCriteria('sid', $this->server_id, '=', 'AND');
                $data = $class->getOne(['data', 'created', 'expired', 'sid']);
                if ($data) {
                    $iCache->save($data, 10, 'video_sources');
                    return $data;
                }
            }
        }
        return FALSE;
    }

    private function updateCache(array $data = [], string $host = '', string $id = '', bool $dl = false)
    {
        session_write_close();
        if (!empty($host) && !empty($id)) {
            $dl = $dl && in_array($host, $this->usingMP4HLS) ? 1 : 0;
            $now = time();
            $exp = $now + $this->timeout($host);

            $class = new \VideoSources();
            $class->setCriteria('host', $host, '=');
            $class->setCriteria('host_id', $id, '=', 'AND');
            $class->setCriteria('dl', $dl, '=', 'AND');
            $class->setCriteria('sid', $this->server_id, '=', 'AND');
            $row = $class->getOne(['data']);
            if ($row) {
                $class->setCriteria('host', $host, '=');
                $class->setCriteria('host_id', $id, '=', 'AND');
                $class->setCriteria('dl', $dl, '=', 'AND');
                $class->setCriteria('sid', $this->server_id, '=', 'AND');
                $updated = $class->update(array(
                    'data' => $data,
                    'expired' => $exp
                ));
                return $updated;
            } else {
                $insert = $class->insert(array(
                    'host' => $host,
                    'host_id' => $id,
                    'data' => $data,
                    'dl' => $dl,
                    'sid' => $this->server_id,
                    'expired' => $exp,
                    'created' => $now
                ));
                return $insert;
            }
        }
        return FALSE;
    }

    function defaultHeaders(string $host = 'gdrive')
    {
        session_write_close();
        if ($host === 'streamsb') {
            $class = new \InstanceCache();
            $ua = $class->get('chrome_ua');
            if (!$ua) {
                $ua = getHeadlessChromeUA();
                $ua = $ua ? $ua : USER_AGENT;
            }
        } else {
            $ua = USER_AGENT;
        }
        return [
            'accept: */*',
            'accept-language: id,en,q=0.9,id-ID,q=0.8',
            'connection: keep-alive',
            'user-agent: ' . $ua
        ];
    }

    function __destruct()
    {
        session_write_close();
        $class = new \InstanceCache();
        $checkChrome = $class->get('check-chrome-version');
        $checkServer = $class->get('check-server');
        if (!$checkChrome || !$checkServer) {
            execInBackground(strtr(BASE_DIR . 'includes/server-info.php', ['\/' => DIRECTORY_SEPARATOR, '\\' => DIRECTORY_SEPARATOR]));
        }
    }
}
