<?php
class SimpleImage
{
    private $image;
    private $image_type;

    function load(string $filename = '')
    {
        session_write_close();

        $image_info = getimagesize($filename);
        $this->image_type = (int) $image_info[2];
        if ($this->image_type === IMAGETYPE_JPEG && function_exists('imagecreatefromjpeg')) {
            $this->image = imagecreatefromjpeg($filename);
        } elseif ($this->image_type === IMAGETYPE_GIF && function_exists('imagecreatefromgif')) {
            $this->image = imagecreatefromgif($filename);
        } elseif ($this->image_type === IMAGETYPE_PNG && function_exists('imagecreatefrompng')) {
            $this->image = imagecreatefrompng($filename);
        }
    }

    function save(string $filename = '', int $image_type = IMAGETYPE_JPEG, int $quality = 80, $permissions = null)
    {
        session_write_close();

        if ($image_type === IMAGETYPE_JPEG) {
            imagejpeg($this->image, $filename, $quality);
        } elseif ($image_type === IMAGETYPE_GIF) {
            imagegif($this->image, $filename);
        } elseif ($image_type === IMAGETYPE_PNG) {
            imagepng($this->image, $filename, 0, PNG_NO_FILTER);
        }
        if (!is_null($permissions)) {
            chmod($filename, $permissions);
        }
    }

    function output(int $image_type = IMAGETYPE_JPEG)
    {
        session_write_close();

        if ($image_type === IMAGETYPE_JPEG) {
            imagejpeg($this->image);
        } elseif ($image_type === IMAGETYPE_GIF) {
            imagegif($this->image);
        } elseif ($image_type === IMAGETYPE_PNG) {
            imagepng($this->image, null, 0);
        }
    }

    function getWidth()
    {
        session_write_close();

        return imagesx($this->image);
    }

    function getHeight()
    {
        session_write_close();

        return imagesy($this->image);
    }

    function resizeToHeight(int $height = 16)
    {
        session_write_close();

        $ratio = $height / $this->getHeight();
        $width = $this->getWidth() * $ratio;
        $this->resize($width, $height);
    }

    function resizeToWidth(int $width = 16)
    {
        session_write_close();

        $ratio = $width / $this->getWidth();
        $height = $this->getheight() * $ratio;
        $this->resize($width, $height);
    }

    function scale(int $scale = 1)
    {
        session_write_close();

        $width = $this->getWidth() * $scale / 100;
        $height = $this->getheight() * $scale / 100;
        $this->resize($width, $height);
    }

    function resize(int $width = 16, int $height = 16)
    {
        session_write_close();

        $new_image = imagecreatetruecolor($width, $height);
        if ($this->image_type === IMAGETYPE_PNG || $this->image_type === IMAGETYPE_ICO) {
            imagealphablending($new_image, false);
            imagesavealpha($new_image, true);
            $transparentindex = imagecolorallocatealpha($new_image, 255, 255, 255, 127);
            imagefill($new_image, 0, 0, $transparentindex);
        }
        imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->image = $new_image;
    }
}
