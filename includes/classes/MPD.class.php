<?php
class MPD extends Core
{
    private $videoHost = '';
    private $videoId = '';
    private $key = '';
    private $ch;
    private $allowResponseHeader = ['accept-ranges', 'content-range', 'content-type', 'content-transfer-encoding', 'etag'];
    private $url = '';
    private $host = '';
    private $origin = '';
    private $referer = '';
    private $header = [];
    private $cacheMaxAge = 0;
    private $cacheExpires = 0;
    private $token = '';
    private $live = false;

    function __construct(array $qry = [])
    {
        session_write_close();

        parent::__construct();

        $this->qry = $qry;
        $url = isset($qry['url']) ? rawurldecode($qry['url']) : '';
        $ref = isset($qry['ref']) ? rawurldecode($qry['ref']) : '';
        $this->key = isset($qry['data']) ? $qry['data'] : '';
        $this->token = isset($qry['token']) ? $qry['token'] : '';

        if (!empty($url)) {
            $scheme = parse_url($url, PHP_URL_SCHEME);
            $this->host = parse_url($url, PHP_URL_HOST);
            $port = parse_URL($url, PHP_URL_PORT);
            $this->url = $url;
        } else {
            $scheme = parse_url($ref, PHP_URL_SCHEME);
            $this->host = parse_url($ref, PHP_URL_HOST);
            $port = parse_URL($ref, PHP_URL_PORT);
        }

        $ipv4 = gethostbyname($this->host . '.');
        if (!empty($port) && $port !== 80 && $port !== 443) {
            $resolveHost = array(implode(':', array($this->host, $port, $ipv4)));
        } else {
            $port = $scheme === 'https' ? 443 : 80;
            $resolveHost = array(implode(':', array($this->host, $port, $ipv4)));
        }

        if (strpos($_SERVER['REQUEST_URI'], '/segments/') !== FALSE) {
            $this->live = isset($qry['live']) ? boolval($qry['live']) : false;
            parse_str($_SERVER['QUERY_STRING'], $qry);
            unset($qry['url']);
            unset($qry['ref']);
            unset($qry['data']);
            unset($qry['token']);
            unset($qry['live']);

            $newQueryString = !empty($qry) ? '?' . http_build_query($qry) : '';

            $fragment = parse_url($_SERVER['REQUEST_URI'], PHP_URL_FRAGMENT);
            $newFragment = !empty($fragment) ? "#$fragment" : '';

            $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $path = explode('/segments/', $path);
            $path = end($path);

            $this->url = rtrim($ref, '/') . '/' . $path . $newQueryString . $newFragment;
        }

        list($this->videoHost, $this->videoId) = array_pad(explode('~', $this->key), 2, '');
        $this->cacheMaxAge = $this->timeout($this->videoHost);

        $host = parse_url(BASE_URL, PHP_URL_HOST);
        $iCache = new \InstanceCache();
        $iCache->setKey('lb_get_id_byhost~' . $host);
        $cache = $iCache->get();
        if ($cache) {
            $sid = $cache;
        } else {
            $class = new \LoadBalancers();
            $class->setCriteria('link', "%//{$host}%", 'LIKE');
            $data = $class->getOne(['id']);
            $sid = $data ? $data['id'] : 0;
            $iCache->save($sid, 10);
        }

        $this->set_server_id($sid);
        $cache = $this->getCache($this->videoHost, $this->videoId, 0);
        if ($cache) {
            $data = json_decode($cache['data'], true);
            $this->cacheCreated = $cache['created'];
            $this->cacheExpires = $cache['expired'];
            $cookies = $data['cookies'];
            if (!empty($data['referer'])) {
                $this->referer = $data['referer'];
                $urlParser = parse_url($data['referer']);
                $this->origin = $urlParser['scheme'] . '://' . $urlParser['host'];
            }
        }

        if (strpos($this->url, BASE_URL . 'tmp/') === FALSE && validate_url($this->url)) {
            if (empty($this->origin) && empty($this->referer)) {
                $this->getReferer($this->url, $this->videoHost, $this->videoId);
            }

            $host = $port !== 80 && $port !== 443 ? $this->host . ':' . $port : $this->host;
            $this->header = $this->defaultHeaders($this->videoHost);
            $this->header[] = 'Host: ' . $this->host;
            $this->header[] = 'Origin: ' . $this->origin;
            $this->header[] = 'Referer: ' . $this->referer;
            $this->header[] = 'X-Requested-With: XMLHttpRequest';

            session_write_close();
            $this->ch = curl_init();
            curl_setopt($this->ch, CURLOPT_URL, $this->url);
            curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($this->ch, CURLOPT_ENCODING, '');
            curl_setopt($this->ch, CURLOPT_MAXREDIRS, 10);
            curl_setopt($this->ch, CURLOPT_TIMEOUT, 0);
            curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($this->ch, CURLOPT_TIMEOUT, 0);
            curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
            curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
            curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
            curl_setopt($this->ch, CURLOPT_FAILONERROR, true);

            if (!filter_var($this->host, FILTER_VALIDATE_IP)) {
                curl_setopt($this->ch, CURLOPT_RESOLVE, $resolveHost);
                curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
            }

            if (!empty($cookies)) {
                curl_setopt($this->ch, CURLOPT_COOKIE, trim(implode(';', $cookies)));
            }

            if (!filter_var(get_option('production_mode'), FILTER_VALIDATE_BOOLEAN) && count(get_resources()) < 900) {
                curl_setopt($this->ch, CURLOPT_VERBOSE, true);
                curl_setopt($this->ch, CURLOPT_STDERR, fopen(BASE_DIR . 'cache/streaming/curl~' . $this->videoHost . '~' . substr(keyFilter($this->videoId), 0, 200) . '.txt', 'w+'));
            }
        }
    }

    private function getReferer(string $url = '', string $host = '', string $id = '')
    {
        session_write_close();
        if ($host === 'direct') {
            $host = parse_url($url, PHP_URL_HOST);
            if (strpos($this->host, 'gcpvuclip') !== FALSE) {
                $this->origin = 'https://www.viu.com';
                $this->referer = 'https://www.viu.com/';
            } elseif (strpos($this->host, 'tvmalaysia') !== FALSE) {
                $this->origin = 'https://www.tvmalaysia.online';
                $this->referer = 'https://www.tvmalaysia.online/';
            } elseif (
                strpos($url, 'cloudfront.net/RCTI') !== FALSE ||
                strpos($url, 'cloudfront.net/MNCTV') !== FALSE ||
                strpos($url, 'cloudfront.net/GTV') !== FALSE ||
                strpos($url, 'cloudfront.net/INEWS') !== FALSE
            ) {
                $this->origin = 'https://www.rctiplus.com';
                $this->referer = 'https://www.rctiplus.com/';
            } elseif (strpos($host, 'rctiplus') !== FALSE) {
                $this->origin = 'https://www.rctiplus.com';
                $this->referer = 'https://www.rctiplus.com/';
            } elseif (strpos($this->url, 'googlevideo.com') !== FALSE || strpos($this->url, 'googleusercontent.com') !== FALSE) {
                $this->origin = 'https://youtube.googleapis.com';
                $this->referer = 'https://youtube.googleapis.com/';
            } elseif (
                strpos($this->url, 'production-premium-vidio') !== FALSE ||
                strpos($this->url, 'vidio-com.akamaized.net') !== FALSE ||
                strpos($this->url, 'vidio.com') !== FALSE ||
                $this->host === 'live-production.secureswiftcontent.com'
            ) {
                $this->origin = 'https://www.vidio.com';
                $this->referer = 'https://www.vidio.com/';
            } elseif (strpos($this->host, 'ttvnw.net') !== FALSE) {
                $this->origin = 'https://www.twitch.tv';
                $this->referer = 'https://www.twitch.tv/';
            } else {
                $this->referer = $url;
            }
        } else {
            $class = new \VideoSources();
            $class->setCriteria('host', $host, '=');
            $class->setCriteria('host_id', $id, '=', 'AND');
            $cache = $class->getOne(['data']);
            if ($cache) {
                $data = json_decode($cache['data'], true);
                $this->origin = rtrim($data['referer'], '/');
                $this->referer = $data['referer'];
            }
        }
    }

    private function errorResponse(int $statusCode = 404)
    {
        session_write_close();
        $now = time();
        header('Cache-Control: no-store');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', $now));
        header('Expires: ' . gmdate('D, d M Y H:i:s T', $now));
        http_response_code($statusCode);
        exit();
    }

    private function parseURL(string $url = '', string $ref = '', string $key = '')
    {
        session_write_close();
        $ref = '&amp;ref=' . encode($ref);
        $xToken = '&amp;token=' . rawurlencode($this->token);
        $xLive = '&amp;live=' . ($this->live ? 'true' : 'false');

        $parse = parse_url($url);
        $port = !empty($parse['port']) ? ':' . $parse['port'] : '';
        if (!empty($parse['host'])) {
            if (!empty($parse['scheme'])) {
                $scheme = $parse['scheme'];
            } else {
                $scheme = isSSL() ? 'https' : 'http';
            }
            $host = $scheme . '://' . $parse['host'] . $port;
        } else {
            $host = '';
        }
        $path = !empty($parse['path']) ? $parse['path'] : '';
        $query = !empty($parse['query']) ? '?' . $parse['query'] . '&amp;' : '?';
        $query .= 'data=' . $key . $ref . $xLive . $xToken;
        $fragment = !empty($parse['fragment']) ? '#' . $parse['fragment'] : '';

        return $host . strtr($path, ['tmp/hosts/facebook//' => '']) . $query . $fragment;
    }

    private function getHost(string $url = '')
    {
        session_write_close();
        $url = parse_url($url);
        $user = !empty($url['user']) ? $url['user'] : '';
        $pass = !empty($url['pass']) ? $url['pass'] : '';
        $cred = $user . ':' . $pass;
        if ($cred !== ':') $host = $cred . '@' . $url['host'];
        else $host = $url['host'];
        $port = !empty($url['port']) ? ':' . $url['port'] : '';
        return $url['scheme'] . '://' . $host . $port . '/';
    }

    private function parse(string $mpdContent = '', string $masterUrl = '', string $key = '')
    {
        session_write_close();
        if (validate_url($masterUrl) && !empty($mpdContent)) {
            $url = parse_url($masterUrl);
            $scheme = $url['scheme'];
            $path = !empty($url['path']) ? trim($url['path'], '/') : '';

            $baseName = basename($path);
            $hostURL = $this->getHost($masterUrl);
            $baseURL = $hostURL . rtrim($path, $baseName);

            $segmentURL = strtr($baseURL, [$hostURL => BASE_URL . 'segments/']);
            $contentURL = '';
            $init = [];
            $media = [];
            $cbase = 0;
            $cinit = 0;
            $cmedia = 0;

            $mpdContent = @mb_convert_encoding($mpdContent, 'ISO-8859-1');
            $xml = @simplexml_load_string($mpdContent);
            if ($xml) {
                $json = json_encode($xml, true);
                $json = json_decode($json, true);
                if (!empty($json['BaseURL'])) {
                    if (validate_url($json['BaseURL'])) {
                        $hostURL = $this->getHost($json['BaseURL']);
                        $contentURL = strtr($json['BaseURL'], [$hostURL => BASE_URL . 'segments/']);
                    } elseif (!is_array($json['BaseURL'])) {
                        $contentURL = rtrim($segmentURL, '/') . '/' . ltrim($json['BaseURL'], '/');
                    }
                }
            }

            if (preg_match_all('/<BaseURL[^>]*>([^"]+)<\/BaseURL>/', $mpdContent, $BaseURL) && !empty($BaseURL[1])) {
                $cbase = count($BaseURL[1]);
                foreach ($BaseURL[1] as $val) {
                    if (substr($val, 0, 2) === '//') {
                        $hostURL = $this->getHost($scheme . ':' . $val);
                        $newURL = strtr($val, [$hostURL => BASE_URL . 'segments/']);
                    } elseif (validate_url($val)) {
                        $hostURL = $this->getHost($val);
                        $newURL = strtr($val, [$hostURL => BASE_URL . 'segments/']);
                    } else {
                        if (!empty($contentURL)) {
                            if (strpos($contentURL, $val) !== FALSE) {
                                $newURL = $segmentURL . '/' . ltrim($val, '/');
                            } else {
                                $newURL = $val;
                            }
                        } else {
                            $newURL = $segmentURL . '/' . ltrim($val, '/');
                        }
                    }
                    $newBaseURL = $this->parseURL($newURL, $hostURL, $key);
                    $mpdContent = strtr($mpdContent, ['>' . $val . '</BaseURL>' => '>' . $newBaseURL . '</BaseURL>']);
                }
            }
            if (preg_match_all('/<Location[^>]*>([^"]+)<\/Location>/', $mpdContent, $Location) && !empty($Location[1])) {
                foreach ($Location[1] as $val) {
                    if (substr($val, 0, 2) === '//') {
                        $hostURL = $this->getHost($scheme . ':' . $val);
                        $newURL = strtr($val, [$hostURL => BASE_URL . 'segments/']);
                    } elseif (validate_url($val)) {
                        $hostURL = $this->getHost($val);
                        $newURL = strtr($val, [$hostURL => BASE_URL . 'segments/']);
                    } else {
                        if (!empty($contentURL)) {
                            if (strpos($contentURL, $val) !== FALSE) {
                                $newURL = $segmentURL . '/' . ltrim($val, '/');
                            } else {
                                $newURL = $val;
                            }
                        } else {
                            $newURL = $segmentURL . '/' . ltrim($val, '/');
                        }
                    }
                    $locationURL = $this->parseURL($newURL, $hostURL, $key);
                    $mpdContent = strtr($mpdContent, ['>' . $val . '</Location>' => '>' . $locationURL . '</Location>']);
                }
            }
            if (preg_match_all('/initialization="([^"]+)"/', $mpdContent, $init) && !empty($init[1])) {
                $cinit = count($init[1]);
                foreach ($init[1] as $val) {
                    if (substr($val, 0, 2) === '//') {
                        $hostURL = $this->getHost($scheme . ':' . $val);
                        $newURL = strtr($val, [$hostURL => BASE_URL . 'segments/']);
                    } elseif (validate_url($val)) {
                        $hostURL = $this->getHost($val);
                        $newURL = strtr($val, [$hostURL => BASE_URL . 'segments/']);
                    } else {
                        if (!empty($contentURL) && (!empty($newBaseURL) || $cinit === $cbase)) {
                            $newURL = $val;
                        } else {
                            $newURL = $segmentURL . '/' . ltrim($val, '/');
                        }
                    }
                    $initURL = $this->parseURL($newURL, $hostURL, $key);
                    $mpdContent = strtr($mpdContent, ['initialization="' . $val . '"' => 'initialization="' . $initURL . '"']);
                }
            }
            if (preg_match_all('/media="([^"]+)"/', $mpdContent, $media) && !empty($media[1])) {
                $cmedia = count($media[1]);
                foreach ($media[1] as $val) {
                    if (substr($val, 0, 2) === '//') {
                        $hostURL = $this->getHost($scheme . ':' . $val);
                        $newURL = strtr($val, [$hostURL => BASE_URL . 'segments/']);
                    } elseif (validate_url($val)) {
                        $hostURL = $this->getHost($val);
                        $newURL = strtr($val, [$hostURL => BASE_URL . 'segments/']);
                    } else {
                        if (!empty($contentURL) && (!empty($newBaseURL) || $cmedia === $cbase)) {
                            $newURL = $val;
                        } else {
                            $newURL = $segmentURL . '/' . ltrim($val, '/');
                        }
                    }
                    $mediaURL = $this->parseURL($newURL, $hostURL, $key);
                    $mpdContent = strtr($mpdContent, ['media="' . $val . '"' => 'media="' . $mediaURL . '"']);
                }
            }
            if (preg_match_all('/sourceURL="([^"]+)"/', $mpdContent, $source) && !empty($source[1])) {
                $csource = count($source[1]);
                foreach ($source[1] as $val) {
                    if (substr($val, 0, 2) === '//') {
                        $hostURL = $this->getHost($scheme . ':' . $val);
                        $newURL = strtr($val, [$hostURL => BASE_URL . 'segments/']);
                    } elseif (validate_url($val)) {
                        $hostURL = $this->getHost($val);
                        $newURL = strtr($val, [$hostURL => BASE_URL . 'segments/']);
                    } else {
                        if (!empty($contentURL) &&  (!empty($newBaseURL) || $csource === $cbase)) {
                            $newURL = $val;
                        } else {
                            $newURL = $segmentURL . '/' . ltrim($val, '/');
                        }
                    }
                    $sourceURL = $this->parseURL($newURL, $hostURL, $key);
                    $mpdContent = strtr($mpdContent, ['sourceURL="' . $val . '"' => 'sourceURL="' . $sourceURL . '"']);
                }
            }

            $dom = new \DOMDocument('1.0');
            $dom->preserveWhiteSpace = true;
            $dom->formatOutput = true;
            $dom->loadXML(trim($mpdContent, '?'));
            $mpdContent = $dom->saveXML();
        }
        return $mpdContent;
    }

    private function parseCache(string $mpdContent = '')
    {
        session_write_close();
        if (!empty($mpdContent)) {
            $scheme = isSSL() ? 'https:' : 'http:';
            $xToken = '&amp;token=';
            $xToken .= !empty($this->token) ? rawurlencode($this->token) : encode(getUserIP());
            if (preg_match_all('/<BaseURL[^>]*>([^"]+)<\/BaseURL>/', $mpdContent, $base) && !empty($base[1])) {
                foreach ($base[1] as $val) {
                    $baseURL = strtr($val, ['https:' => $scheme, 'http:' => $scheme]) . $xToken;
                    $mpdContent = strtr($mpdContent, ['>' . $val . '</BaseURL>' => '>' . $baseURL . '</BaseURL>']);
                }
            }
            if (preg_match_all('/<Location[^>]*>([^"]+)<\/Location>/', $mpdContent, $location) && !empty($location[1])) {
                foreach ($location[1] as $val) {
                    $locationURL = strtr($val, ['https:' => $scheme, 'http:' => $scheme]) . $xToken;
                    $mpdContent = strtr($mpdContent, ['>' . $val . '</BaseURL>' => '>' . $locationURL . '</BaseURL>']);
                }
            }
            if (preg_match_all('/initialization="([^"]+)"/', $mpdContent, $init) && !empty($init[1])) {
                foreach ($init[1] as $val) {
                    $initURL = strtr($val, ['https:' => $scheme, 'http:' => $scheme]) . $xToken;
                    $mpdContent = strtr($mpdContent, ['initialization="' . $val . '"' => 'initialization="' . $initURL . '"']);
                }
            }
            if (preg_match_all('/media="([^"]+)"/', $mpdContent, $media) && !empty($media[1])) {
                foreach ($media[1] as $val) {
                    $mediaURL = strtr($val, ['https:' => $scheme, 'http:' => $scheme]) . $xToken;
                    $mpdContent = strtr($mpdContent, ['media="' . $val . '"' => 'media="' . $mediaURL . '"']);
                }
            }
            if (preg_match_all('/sourceURL="([^"]+)"/', $mpdContent, $media) && !empty($media[1])) {
                foreach ($media[1] as $val) {
                    $sourceURL = strtr($val, ['https:' => $scheme, 'http:' => $scheme]) . $xToken;
                    $mpdContent = strtr($mpdContent, ['sourceURL="' . $val . '"' => 'sourceURL="' . $sourceURL . '"']);
                }
            }
            return trim($mpdContent, '?');
        }
        return $mpdContent;
    }

    private function setCacheHeaders(bool $useCache = false, bool $playlist = true)
    {
        session_write_close();
        if ($useCache && $this->videoHost !== 'facebook') {
            header('Cache-Control: private');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', $this->cacheCreated));
            header('Expires: ' . gmdate('D, d M Y H:i:s T', $this->cacheExpires));
        } else {
            if ($playlist) {
                header('Cache-Control: no-store');
            } else {
                $cacheExpires = $this->cacheCreated + 300;
                header('Cache-Control: max-age=300');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', $this->cacheCreated));
                header('Expires: ' . gmdate('D, d M Y H:i:s T', $cacheExpires));
            }
        }
    }

    function playlistStream()
    {
        session_write_close();

        header('Accept-Ranges: bytes');
        header('Content-Type: application/dash+xml');
        $this->setCacheHeaders(false, true);

        $prefix = $this->videoHost . '~' . keyFilter($this->videoId);
        $pLen = strlen($prefix);

        $fileName = keyFilter($this->url);
        $fLen = strlen($fileName);

        $wLen = $pLen + $fLen;
        if ($wLen <= 250) {
            $offset = 0;
            $length = $fLen;
        } else {
            $offset = $fLen - $pLen;
            $length = 250 - $pLen;
        }

        $fileName = substr(rtrim($fileName, 'mpd'), $offset, $length);
        $cache = BASE_DIR . 'cache/playlist/' . $prefix . '~' . $fileName . '.mpd';
        if (file_exists($cache)) {
            $timeIntv = time() - filemtime($cache);
            if ($timeIntv < $this->cacheMaxAge) {
                open_resources_handler();
                $fp = @fopen($cache, 'r');
                if ($fp) {
                    stream_set_blocking($fp, false);
                    $content = stream_get_contents($fp);
                    fclose($fp);

                    $this->live = false;
                    echo $this->parseCache($content);
                    ob_flush();
                    flush();
                    exit();
                }
            }
        }

        @unlink($cache);
        $keyEncode = encode($this->key);
        if (strpos($this->url, BASE_URL . 'tmp/') !== FALSE) {
            open_resources_handler();
            $fp = @fopen(strtr($this->url, [BASE_URL => BASE_DIR]), 'r');
            if ($fp) {
                stream_set_blocking($fp, false);
                $response = stream_get_contents($fp);
                fclose($fp);

                preg_match('/type="([^"]+)"/', $response, $type);
                if (!empty($type[1]) && strtolower($type[1]) === 'static') {
                    $this->live = false;
                    $content = $this->parse($response, $this->url, $keyEncode);
                    $saveContent = strtr($content, ['&amp;token=' . $this->token => '', '&amp;token=' . rawurlencode($this->token) => '']);
                    create_file($cache, $saveContent);
                } else {
                    $this->live = true;
                    $content = $this->parse($response, $this->url, $keyEncode);
                }
                echo $content;
                ob_flush();
                flush();
            } else {
                $this->errorResponse(404);
            }
        } else {
            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->header);
            curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $line) {
                session_write_close();
                return strlen($line);
            });

            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);
            curl_close($this->ch);

            if ($status >= 200 && $status < 400) {
                if (strpos($response, '<MPD') !== FALSE) {
                    preg_match('/type="([^"]+)"/', $response, $type);
                    if (!empty($type[1]) && strtolower($type[1]) === 'static') {
                        $this->live = false;
                        $content = $this->parse($response, $this->url, $keyEncode);
                        $saveContent = strtr($content, ['&amp;token=' . $this->token => '', '&amp;token=' . rawurlencode($this->token) => '']);
                        create_file($cache, $saveContent);
                    } else {
                        $this->live = true;
                        $content = $this->parse($response, $this->url, $keyEncode);
                    }
                    echo $content;
                    ob_flush();
                    flush();
                } else {
                    $this->errorResponse(415);
                }
            } else {
                error_log("MPD Playlist Error {$this->videoHost} {$this->videoId} => {$this->url} => $status: $err");
                $this->errorResponse($status);
            }
        }
    }

    function stream(string $range = '')
    {
        session_write_close();
        if ($this->ch) {
            if (in_array($this->videoHost, $this->bad_hosts())) {
                sleep(rand(1, 5));
                $proxy = rate_limit_ips();
                if ($proxy) {
                    $this->header[] = 'X-Originating-IP: ' . $proxy;
                    $this->header[] = 'X-Forwarded-For: ' . $proxy;
                    $this->header[] = 'X-Remote-IP: ' . $proxy;
                    $this->header[] = 'X-Remote-Addr: ' . $proxy;
                    $this->header[] = 'X-Client-IP: ' . $proxy;
                    $this->header[] = 'X-Host: ' . $proxy;
                    $this->header[] = 'X-Forwared-Host: ' . $proxy;
                }
            }
            
            if (isset($_SERVER['HTTP_IF_RANGE'])) {
                $this->header[] = 'if-range: ' . $_SERVER['HTTP_IF_RANGE'];
            }

            if (!empty($range)) {
                $this->header[] = 'range: ' . $range;
                $fmp4 = true;
                http_response_code(206);
            } else {
                $fmp4 = false;
                http_response_code(200);
            }
            
            $this->live = isset($_GET['live']) && filter_var($_GET['live'], FILTER_VALIDATE_BOOLEAN);
            if ($this->live) {
                $this->setCacheHeaders(false, true);
            } else {
                $this->setCacheHeaders(true, false);
            }

            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, false);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->header);
            curl_setopt($this->ch, CURLOPT_BUFFERSIZE, 10485760);
            curl_setopt($this->ch, CURLOPT_HEADER, false);
            curl_setopt($this->ch, CURLOPT_NOBODY, false);
            curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($curl, $header) use ($fmp4) {
                session_write_close();
                list($key, $val) = array_pad(explode(':', strtolower($header)), 2, '');
                if (in_array($key, $this->allowResponseHeader) || ($fmp4 && $key === 'content-length')) header($header);
                return strlen($header);
            });
            curl_setopt($this->ch, CURLOPT_WRITEFUNCTION, function ($curl, $body) {
                session_write_close();
                echo $body;
                ob_flush();
                flush();
                return strlen($body);
            });

            curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);
            curl_close($this->ch);

            if ($status === 401 || $status === 429 || $status >= 500 || ($status === 403 && in_array($this->videoHost, $this->bad_hosts()))) {
                $class = new \MPD($this->qry);
                $class->stream($range);
                exit();
            } elseif ($status < 200 || $status >= 400) {
                error_log("MPD Stream Error {$this->videoHost} {$this->videoId} => {$this->url} => $status: $err");
                $this->errorResponse($status);
            }
        } else {
            error_log("MPD Stream Error {$this->videoHost} {$this->videoId} => {$this->url} => curl instance not found");
        }
    }

    function __destruct()
    {
        session_write_close();
    }
}
