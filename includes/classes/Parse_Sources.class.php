<?php
class Parse_Sources extends Core
{
    private $originalQuery = [];
    private $qry    = [];
    private $title  = '';
    private $poster = '';
    private $tracks = [];
    private $sources = [];
    private $userID = 1;
    private $saved = false;
    private $useTitleAsSlug = false;
    private $shortKey = '';
    private $mainSite = '';

    function __construct(array $qry = [])
    {
        session_write_close();

        parent::__construct();

        $this->qry = $qry;
        $this->originalQuery = $qry;

        $mainSite = get_option('main_site');
        $this->mainSite = !empty($mainSite) ? $mainSite : BASE_URL;

        if (isset($qry['saved'])) {
            $this->saved = filter_var($qry['saved'], FILTER_VALIDATE_BOOLEAN);
        } else {
            $this->saved = filter_var(get_option('save_public_video'), FILTER_VALIDATE_BOOLEAN);
        }

        if (isset($qry['useTitleAsSlug'])) {
            $this->useTitleAsSlug = filter_var($qry['useTitleAsSlug'], FILTER_VALIDATE_BOOLEAN);
        }

        if (!empty($this->qry['source']) && !empty($this->qry['id'])) {
            $class = new \Videos();
            $class->setCriteria('id', $this->qry['id']);
            $data = $class->getOne(['uid', 'title', 'poster']);
            if ($data) {
                $this->userID = $data['uid'];

                $this->title = $data['title'];

                if (!empty($data['poster'])) {
                    if (validate_url($data['poster'])) {
                        $this->poster = $data['poster'];
                    } else {
                        $this->poster = $mainSite . 'uploads/images/' . $data['poster'];
                    }
                }

                $class = new \VideoShort();
                $class->setCriteria('vid', $this->qry['id']);
                $data = $class->getOne(['key']);
                if ($data) $this->shortKey = $data['key'];

                $class = new \Subtitles();
                $class->setCriteria('vid', $this->qry['id']);
                $class->setOrderBy('order', 'ASC');
                $list = $class->get(['language', 'link']);
                if ($list) {
                    foreach ($list as $dt) {
                        $this->tracks[] = [
                            'file' => $dt['link'],
                            'label' => $dt['language']
                        ];
                    }
                    $tracks = array_intersect_key($this->tracks, array_unique(array_column($this->tracks, 'label')));
                    $this->tracks = $tracks;
                }
            }
        } else {
            if (!empty($qry['uid'])) {
                $this->userID = (int) $qry['uid'];
            } else {
                $user = current_user();
                if ($user) {
                    $this->userID = (int) $user['id'];
                } else {
                    $uid = (int) get_option('public_video_user');
                    $this->userID = !empty($uid) ? $uid : 1;
                }
            }

            if (!empty($qry['poster'])) {
                $this->poster = validate_url($qry['poster']) ? $qry['poster'] : $mainSite . 'uploads/images/' . $qry['poster'];
            } else {
                $this->poster = get_option('poster');
            }

            if (!empty($qry['sub'])) {
                $subs = [];
                if (is_array($qry['sub'])) {
                    $subs = $qry['sub'];
                } else {
                    $subs = array_values(array_filter(explode('~', trim($qry['sub'], '~'))));
                }

                $langs = [];
                if (is_array($qry['lang'])) {
                    $langs = $qry['lang'];
                } else {
                    $langs = array_values(array_filter(explode('~', trim($qry['lang'], '~'))));
                }

                $class = new \SubtitleManager();
                foreach ($subs as $i => $dt) {
                    if (!empty($dt)) {
                        if (is_numeric($dt)) {
                            $class->setCriteria('id', $dt);
                            $data = $class->getOne(['host', 'file_name', 'language']);
                            if ($data) {
                                $this->tracks[] = [
                                    'file' => rtrim($data['host'], '/') . '/uploads/subtitles/' . $data['file_name'],
                                    'label' => $data['language']
                                ];
                            }
                        } elseif (validate_url($dt)) {
                            $this->tracks[] = [
                                'file' => $dt,
                                'label' => $langs[$i]
                            ];
                        }
                    }
                }

                $class = new \Subtitles();
                foreach ($subs as $dt) {
                    if (is_numeric($dt)) {
                        $class->setCriteria('id', $dt);
                        $data = $class->getOne(['link', 'language']);
                        if ($data) {
                            $this->tracks[] = [
                                'file' => $data['link'],
                                'label' => $data['language']
                            ];
                        }
                    }
                }

                $tracks = array_intersect_key($this->tracks, array_unique(array_column($this->tracks, 'label')));
                $this->tracks = $tracks;
            }
        }
    }

    function set_userID(int $uid = 1)
    {
        session_write_close();
        $this->userID = $uid;
    }

    function set_title(string $title = '')
    {
        session_write_close();
        $this->title = $title;
    }

    function parseSources(string $key = '', array $sources = [], string $ref = '', string $email = '', string $baseURL = '', string $token = '')
    {
        session_write_close();
        $token = !empty($token) ? $token : encode(getUserIP());
        if (!empty($key) && strpos($key, '~') !== FALSE && !empty($sources)) {
            list($host, $id) = explode('~', $key, 2);
            $hash_host = md5(strrev($host));
            $hash_id = md5(strrev($id));
            $hashed = $this->create_video_hash($host, $id, $email);
            if ($hashed) {
                $baseURL = !empty($baseURL) ? $baseURL : BASE_URL;
                $baseURL = strtr($baseURL, ['https:' => '', 'http:' => '']);
                $bypass = $this->bypass_host();
                if (strpos($ref, 'animeku') !== FALSE) {
                    array_push($bypass, 'kuronime');
                }
                if (in_array($host, $bypass)) {
                    $result = [];
                    if ($sources[0]['type'] === 'hls') {
                        $result[] = [
                            'file' => $baseURL . 'playlist/' . $hash_host . '/' . $hash_id . '/master.m3u8?token=' . $token,
                            'type' => 'hls',
                            'label' => 'Original',
                            'default' => true
                        ];
                    } elseif ($sources[0]['type'] === 'mpd') {
                        $result[] = [
                            'file' => $baseURL . 'dash/' . $hash_host . '/' . $hash_id . '/master.mpd?token=' . $token,
                            'type' => 'mpd',
                            'label' => 'Original',
                            'default' => true
                        ];
                    } else {
                        $defaultRes = (int) get_option('default_resolution');
                        preg_match('/audio|mp3|aac/', $sources[0]['type'], $mimeType);
                        $ext = !empty($mimeType[0]) ? 'mp3' : 'mp4';
                        $title = !empty($this->title) ? slugify(strtr($this->title, ['.' . $ext => '']), ' ') : 'v';
                        $title .= '.' . $ext;
                        foreach ($sources as $data) {
                            $dt = [];
                            $label = (int) preg_replace('/[^0-9]/', '', $data['label']);
                            if ($label >= 100 && $label < 200) $label = 100;
                            elseif ($label >= 200 && $label < 300) $label = 200;
                            elseif ($label >= 300 && $label < 400) $label = 300;
                            elseif ($label >= 400 && $label < 500) $label = 400;
                            elseif ($label >= 500 && $label < 600) $label = 500;
                            elseif ($label >= 600 && $label < 700) $label = 600;
                            elseif ($label >= 700 && $label < 800) $label = 700;
                            elseif ($label >= 800 && $label < 900) $label = 800;
                            elseif ($label >= 900 && $label < 1000) $label = 900;
                            elseif ($label >= 1000) $label = 1000;
                            $dt['file'] = $baseURL . 'videoplayback/' . $hash_host . '/' . $hash_id . '/' . strtolower($data['label']) . '/?token=' . $token;
                            $dt['type'] = $data['type'];
                            $dt['label'] = $data['label'];
                            if ($defaultRes === $label) $dt['default'] = true;
                            $result[] = $dt;
                        }
                    }
                    return $result;
                } else {
                    if (strpos($sources[0]['file'], 'tmp/hosts/') !== FALSE) {
                        $sources[0]['default'] = true;
                        if ($sources[0]['type'] === 'hls') {
                            $sources[0]['file'] = $baseURL . 'hls-parser/' . $hash_host . '/' . $hash_id . '/master.m3u8?token=' . $token;
                        } elseif ($sources[0]['type'] === 'mpd') {
                            $sources[0]['file'] = $baseURL . 'dash-parser/' . $hash_host . '/' . $hash_id . '/master.mpd?token=' . $token;
                        }
                    }
                    return $sources;
                }
            }
        }
        return $sources;
    }

    private function save_video_shortkey(int $vid = 0, string $key = '')
    {
        if (!empty($vid)) {
            $class = new \VideoShort();
            $class->setCriteria('vid', $vid);
            $short = $class->getOne(['key']);
            if ($short === false) {
                if (!empty($key)) {
                    $this->shortKey = $key;
                    $newKey = $class->insert(array(
                        'vid' => $vid,
                        'key' => $key
                    ));
                    if ($newKey) {
                        return $newKey;
                    } else {
                        $uuid = new \UUID();
                        $key = $uuid->v4();
                        $this->shortKey = $key;
                        return $class->insert(array(
                            'vid' => $vid,
                            'key' => $key
                        ));
                    }
                } else {
                    $uuid = new \UUID();
                    $key = $uuid->v4();
                    $this->shortKey = $key;
                    $newKey = $class->insert(array(
                        'vid' => $vid,
                        'key' => $key
                    ));
                    if ($newKey) {
                        return $newKey;
                    } else {
                        $key = $uuid->v4();
                        $this->shortKey = $key;
                        return $class->insert(array(
                            'vid' => $vid,
                            'key' => $key
                        ));
                    }
                }
            } else {
                $this->shortKey = $short['key'];
            }
        }
        return false;
    }

    private function save_public_video()
    {
        session_write_close();
        if (!isset($this->originalQuery['alt']) || (isset($this->originalQuery['alt']) && intval($this->originalQuery['alt']) < 1)) {
            if ($this->saved && !empty($this->userID) && !empty($this->qry['host']) && !empty($this->qry['id'])) {
                $time = time();

                if (strpos($this->poster, 'poster/?url=') !== FALSE) {
                    $pqry = parse_url($this->poster, PHP_URL_QUERY);
                    parse_str($pqry, $pqry);
                    $poster = decode($pqry['url']);
                    $poster = rawurldecode($poster);
                    if (strpos($poster, '?url=') !== FALSE) {
                        $pqry = parse_url($poster, PHP_URL_QUERY);
                        parse_str($pqry, $pqry);
                        $pqurl = strtr($pqry['url'], [' ' => '+']);
                        $epqurl = rawurlencode($pqurl);
                        $poster = strtr($poster, [$pqurl => $epqurl]);
                    }
                } else {
                    $poster = $this->poster;
                }

                if (isset($this->originalQuery['source'])) {
                    $class = new \Videos();
                    $class->setCriteria('id', $this->originalQuery['id']);
                    $data = $class->getOne(['id', 'title', 'ahost', 'ahost_id']);
                } else {
                    $class = new \Videos();
                    $class->setCriteria('uid', $this->userID, '=');
                    $class->setCriteria('host', $this->qry['host'], '=', 'AND');
                    $class->setCriteria('host_id', $this->qry['id'], '=', 'AND');
                    if (!empty($this->qry['ahost']) && !empty($this->qry['aid'])) {
                        $class->setCriteria('ahost', $this->qry['ahost'], '=', 'AND');
                        $class->setCriteria('ahost_id', $this->qry['aid'], '=', 'AND');
                    }
                    $data = $class->getOne(['id', 'title', 'ahost', 'ahost_id']);
                }
                if ($data) {
                    // update
                    $id = $data['id'];

                    if (!empty($this->qry['ahost']) && !empty($this->qry['aid'])) {
                        $class = new \VideosAlternatives();
                        $class->setCriteria('vid', $id, '=');
                        $class->setCriteria('host', $this->qry['ahost'], '=', 'AND');
                        $class->setCriteria('host_id', $this->qry['aid'], '=', 'AND');
                        if ($class->getNumRows() === 0) {
                            $class->insert(array(
                                'vid' => $id,
                                'host' => $this->qry['ahost'],
                                'host_id' => $this->qry['aid']
                            ));
                        }
                    } elseif (!empty($data['ahost']) && !empty($data['ahost_id'])) {
                        $class = new \VideosAlternatives();
                        $class->insert(array(
                            'vid' => $id,
                            'host' => $data['ahost'],
                            'host_id' => $data['ahost_id']
                        ));
                    }

                    $class = new \Videos();
                    $title = !empty($data['title']) ? $data['title'] : trim(substr($this->title, 0, 100));
                    $data = [
                        'title' => htmlspecialchars($title),
                        'host' => $this->qry['host'],
                        'host_id' => $this->qry['id'],
                        'poster' => $poster,
                        'ahost' => '',
                        'ahost_id' => ''
                    ];
                    $class->setCriteria('id', $id, '=');
                    $class->setCriteria('uid', $this->userID, '=', 'AND');
                    $class->update($data);

                    if (!empty($this->tracks)) {
                        $subs = array_column($this->tracks, 'file');
                        $langs = array_column($this->tracks, 'label');
                        $class = new \Subtitles();
                        foreach ($subs as $i => $dt) {
                            list($trash, $url) = array_pad(explode('.vtt?url=', $dt), 2, '');
                            $url = rawurldecode($url);
                            if (!empty($url)) {
                                $lang = isset($langs[$i]) ? $langs[$i] : 'Default';

                                $class->setCriteria('vid', $id, '=');
                                $class->setCriteria('language', $lang, '=', 'AND');
                                $class->setCriteria('link', $url, '=', 'AND');
                                if ($class->getNumRows() === 0) {
                                    $class->insert(array(
                                        'vid' => $id,
                                        'language' => $lang,
                                        'link' => $url,
                                        'added' => $time,
                                        'uid' => $this->userID
                                    ));
                                }
                            }
                        }
                    }

                    if ($this->useTitleAsSlug) {
                        if (!empty($data['title'])) $this->save_video_shortkey($id, slugify($data['title']));
                        elseif (!empty($title)) $this->save_video_shortkey($id, slugify($title));
                        else $this->save_video_shortkey($id);
                    } else {
                        $this->save_video_shortkey($id);
                    }
                } else {
                    $title = trim(substr($this->title, 0, 100));
                    $class = new \Videos();
                    $id = $class->insert(array(
                        'title' => htmlspecialchars($title),
                        'host' => $this->qry['host'],
                        'host_id' => $this->qry['id'],
                        'poster' => $poster,
                        'added' => $time,
                        'updated' => $time,
                        'uid' => $this->userID,
                        'ahost' => '',
                        'ahost_id' => ''
                    ));
                    if ($id) {
                        if (!empty($this->qry['ahost']) && !empty($this->qry['aid'])) {
                            $class = new \VideosAlternatives();
                            $class->insert(array(
                                'vid' => $id,
                                'host' => $this->qry['ahost'],
                                'host_id' => $this->qry['aid']
                            ));
                        }

                        if (!empty($this->tracks)) {
                            $subs = array_column($this->tracks, 'file');
                            $langs = array_column($this->tracks, 'label');
                            $class = new \Subtitles();
                            foreach ($subs as $i => $dt) {
                                list($trash, $url) = array_pad(explode('.vtt?url=', $dt), 2, '');
                                $url = rawurldecode($url);
                                if (!empty($url)) {
                                    $lang = isset($langs[$i]) ? $langs[$i] : 'Default';

                                    $class->setCriteria('vid', $id, '=');
                                    $class->setCriteria('language', $lang, '=', 'AND');
                                    $class->setCriteria('link', $url, '=', 'AND');
                                    if ($class->getNumRows() === 0) {
                                        $class->insert(array(
                                            'vid' => $id,
                                            'language' => $lang,
                                            'link' => $url,
                                            'added' => $time,
                                            'uid' => $this->userID
                                        ));
                                    }
                                }
                            }
                        }

                        if ($this->useTitleAsSlug) {
                            if (!empty($data['title'])) $this->save_video_shortkey($id, slugify($data['title']));
                            elseif (!empty($title)) $this->save_video_shortkey($id, slugify($title));
                            else $this->save_video_shortkey($id);
                        } else {
                            $this->save_video_shortkey($id);
                        }
                    } else {
                        error_log('Parse_Sources save_public_video => ' . $class->getLastError());
                    }
                }
            }
        }
    }

    function reorderSources(array $sources = [])
    {
        session_write_close();
        if (!empty($sources)) {
            $key = array_search('Default', array_column($sources, 'label'));
            if ($key) {
                $default = $sources[$key];
                unset($sources[$key]);
                array_unshift($sources, $default);
            }
        }
        return $sources;
    }

    function filterSources(array $sources = [])
    {
        session_write_close();
        $count = count($sources);
        if ($count > 1) {
            $disableRes = get_option('disable_resolution');
            if (!empty($disableRes)) {
                $disableRes = is_array($disableRes) ? $disableRes : json_decode($disableRes, true);
            } else {
                $disableRes = [];
            }
            if (!empty($disableRes)) {
                $result = [];
                foreach ($sources as $file) {
                    if (isset($file['label'])) {
                        $label = rtrim($file['label'], 'p');
                        if (is_numeric($label)) {
                            if ($label >= 100 && $label < 200) $label = 100;
                            elseif ($label >= 200 && $label < 300) $label = 200;
                            elseif ($label >= 300 && $label < 400) $label = 300;
                            elseif ($label >= 400 && $label < 500) $label = 400;
                            elseif ($label >= 500 && $label < 600) $label = 500;
                            elseif ($label >= 600 && $label < 700) $label = 600;
                            elseif ($label >= 700 && $label < 800) $label = 700;
                            elseif ($label >= 800 && $label < 900) $label = 800;
                            elseif ($label >= 900 && $label < 1000) $label = 900;
                            elseif ($label >= 1000) $label = 1000;
                        }
                        if (!in_array($label, $disableRes)) $result[] = $file;
                    } else {
                        $result[] = $file;
                    }
                }
                $result = array_map('unserialize', array_unique(array_map('serialize', $result)));
                return $result;
            }
        }
        return $sources;
    }

    function get_config(bool $download = false, string $token = '')
    {
        session_write_close();
        if (isset($this->qry['source'])) {
            if (isset($this->originalQuery['alt'])) {
                if (intval($this->originalQuery['alt']) > 0) {
                    $class = new \VideosAlternatives();
                    $class->setCriteria('id', $this->originalQuery['alt']);
                    $class->setCriteria('vid', $this->qry['id'], '=', 'AND');
                    $data = $class->getOne(['host', 'host_id']);
                    if ($data) {
                        $this->qry = [
                            'host' => $data['host'],
                            'id' => $data['host_id']
                        ];
                    } elseif (!empty($data['ahost']) && !empty($data['ahost_id'])) {
                        $this->qry = [
                            'host' => $data['ahost'],
                            'id' => $data['ahost_id']
                        ];
                    }
                } elseif (intval($this->originalQuery['alt']) === 0) {
                    $class = new \Videos();
                    $class->setCriteria('id', $this->qry['id']);
                    $data = $class->getOne(['ahost', 'ahost_id']);
                    if ($data) {
                        $this->qry = [
                            'host' => $data['ahost'],
                            'id' => $data['ahost_id']
                        ];
                    }
                } else {
                    $class = new \Videos();
                    $class->setCriteria('id', $this->qry['id']);
                    $data = $class->getOne(['host', 'host_id']);
                    if ($data) {
                        $this->qry = [
                            'host' => $data['host'],
                            'id' => $data['host_id']
                        ];
                    }
                }
            } else {
                $class = new \Videos();
                $class->setCriteria('id', $this->qry['id']);
                $data = $class->getOne(['host', 'host_id']);
                if ($data) {
                    $this->qry = [
                        'host' => $data['host'],
                        'id' => $data['host_id']
                    ];
                }
            }
        }

        if (!empty($this->qry['host']) && !empty($this->qry['id'])) {
            $this->cacheExpires = $this->timeout($this->qry['host']) - 60;

            $this->set_download($download);
            $this->set_query($this->qry);
            $data = $this->result();

            if (!empty($data['sources'])) {
                if (empty($this->title)) {
                    $this->title = $data['title'];
                }

                if (!empty($this->poster)) {
                    $this->poster = $this->mainSite . 'poster/?url=' . encode($this->poster);
                } elseif (!empty($data['image'])) {
                    $this->poster = $this->mainSite . 'poster/?url=' . encode($data['image']);
                }
                
                $data['sources'] = $this->parseSources($this->qry['host'] . '~' . $this->qry['id'], $data['sources'], $data['referer'], $data['email'], BASE_URL, $token);
                $data['sources'] = $this->filterSources($data['sources']);
                $this->sources = $this->reorderSources($data['sources']);

                $this->tracks = array_merge($this->tracks, $data['tracks']);
                $tracks = [];
                foreach ($this->tracks as $data) {
                    $dt = [];
                    $dt['file'] = $this->mainSite . 'subtitle.vtt?url=' . rawurlencode($data['file']);
                    $dt['label'] = $data['label'];
                    $tracks[] = $dt;
                }
                $newTracks = array_intersect_key($tracks, array_unique(array_column($tracks, 'label')));
                $this->tracks = $newTracks;

                if (!empty($this->tracks)) {
                    $key = array_search(get_option('default_subtitle'), array_column($this->tracks, 'label'));
                    if ($key) $this->tracks[$key]['default'] = true;
                    else $this->tracks[0]['default'] = true;
                }

                $this->save_public_video();

                return array(
                    'title' => $this->title,
                    'poster' => $this->poster,
                    'sources' => $this->sources,
                    'tracks' => $this->tracks
                );
            }
        }
        return FALSE;
    }

    function get_short_key()
    {
        session_write_close();
        return $this->shortKey;
    }

    function get_query()
    {
        session_write_close();
        return $this->qry;
    }

    function get_title()
    {
        session_write_close();
        return $this->title;
    }

    function get_poster()
    {
        session_write_close();
        return $this->poster;
    }

    function get_tracks()
    {
        session_write_close();
        return $this->tracks;
    }

    function get_sources()
    {
        session_write_close();
        return $this->sources;
    }

    function __destruct()
    {
        session_write_close();
        $class = new \InstanceCache();
        $checkChrome = $class->get('check-chrome-version');
        $checkServer = $class->get('check-server');
        if (!$checkChrome || !$checkServer) {
            execInBackground(strtr(BASE_DIR . 'includes/server-info.php', ['\/' => DIRECTORY_SEPARATOR, '\\' => DIRECTORY_SEPARATOR]));
        }
    }
}
