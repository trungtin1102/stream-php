<?php
class FileStream
{
    private $videoId = '';
    private $file = '';
    private $fileSize = '';
    private $start = 0;
    private $cacheExpires = 0;
    private $notFound = true;

    function __construct(string $file = '')
    {
        session_write_close();
        if (file_exists($file)) {
            $this->notFound = false;
            $this->file = $file;
            $this->fileSize = fileSize($file);
            $fileType = mime_content_type($file);
            $fileName = basename($file);
            $this->videoId = strtr($file, [BASE_DIR => BASE_URL]);

            header('Content-Type: ' . $fileType);
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
            $this->get_cache_info();
        }
    }

    private function get_cache_info()
    {
        session_write_close();
        if (empty($this->cacheExpires)) {
            $host = parse_url(BASE_URL, PHP_URL_HOST);

            $class = new \LoadBalancers();
            $class->setCriteria('link', "%//{$host}%", 'LIKE');
            $row = $class->getOne(['id']);
            $sid = $row ? $row['id'] : 0;

            $class = new \VideoSources();
            $class->setCriteria('host', 'direct', '=');
            $class->setCriteria('host_id', $this->videoId, '=', 'AND');
            $class->setCriteria('sid', $sid, '=', 'AND');
            $class->setCriteria('dl', 0, '=', 'AND');
            $row = $class->getOne(['created', 'expired']);
            if ($row) {
                $this->cacheMaxAge = intval($row['expired']) - intval($row['created']);
                $this->cacheExpires = $row['expired'];
            } else {
                $this->cacheMaxAge = 0;
                $this->cacheExpires = time();
            }
        }
        header('Cache-Control: private');
        header('Expires: ' . gmdate('D, d M Y H:i:s T', $this->cacheExpires));
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', @filemtime($this->file)));
    }

    function stream(string $range = '')
    {
        session_write_close();
        if ($this->notFound) {
            echo 'Video not found!';
            http_response_code(404);
            exit();
        } else {
            if (!empty($range)) {
                // Parse field value
                list($specifier, $value) = array_pad(explode('=', $range), 2, '');

                // Can only handle bytes range specifier
                if ($specifier !== 'bytes') {
                    $this->stream('bytes=0-');
                    exit();
                } else {
                    // Set start/finish bytes
                    list($from, $to) = array_pad(explode('-', $value), 2, '');
                    $this->start = $from;
                    if (empty($to)) $to = $this->fileSize - 1;
                    $length = $this->fileSize - $from;

                    http_response_code(206);
                    header('Content-Range: ' . sprintf('bytes %d-%d/%d', $from, $to, $this->fileSize));
                    header('Content-Length: ' . $length);
                }
            } else {
                $to = $this->fileSize - 1;

                http_response_code(200);
                header('Content-Range: ' . sprintf('bytes %d-%d/%d', 0, $to, $this->fileSize));
                header('Content-Length: ' . $this->fileSize);
            }
            open_resources_handler();
            $fp = @fopen($this->file, 'rb');
            if ($fp) {
                stream_set_blocking($fp, false);
                fseek($fp, $this->start);
                while (!feof($fp)) {
                    echo fread($fp, 1048576);
                    ob_flush();
                    flush();
                }
                fclose($fp);
            }
        }
    }
}
