<?php
class HLS extends Core
{
    private $videoHost = '';
    private $videoId = '';
    private $key = '';
    private $ch;
    private $allowResponseHeader = ['accept-ranges', 'content-range', 'content-type', 'content-transfer-encoding', 'etag'];
    private $url = '';
    private $host = '';
    private $origin = '';
    private $referer = '';
    private $token = '';
    private $header = [];
    private $cacheMaxAge = 0;
    private $cacheExpires = 0;
    private $live = false;

    function __construct(array $qry = [])
    {
        session_write_close();

        parent::__construct();

        $this->qry = $qry;
        $this->url = isset($qry['url']) ? $qry['url'] : '';
        $this->key = isset($qry['data']) ? $qry['data'] : '';
        $this->token = isset($qry['token']) ? $qry['token'] : '';
        unset($qry['url']);
        unset($qry['data']);
        unset($qry['token']);

        $host = parse_url(BASE_URL, PHP_URL_HOST);
        $iCache = new \InstanceCache();
        $iCache->setKey('lb_get_id_byhost~' . $host);
        $cache = $iCache->get();
        if ($cache) {
            $sid = $cache;
        } else {
            $class = new \LoadBalancers();
            $class->setCriteria('link', "%//{$host}%", 'LIKE');
            $data = $class->getOne(['id']);
            $sid = $data ? $data['id'] : 0;
            $iCache->save($sid, 10);
        }

        if (!empty($this->url)) {
            $scheme = parse_url($this->url, PHP_URL_SCHEME);
            $this->host = parse_url($this->url, PHP_URL_HOST);
            $port = parse_URL($this->url, PHP_URL_PORT);
            $ipv4 = gethostbyname($this->host . '.');
            if (!empty($port) && $port !== 80 && $port !== 443) {
                $resolveHost = array(implode(':', array($this->host, $port, $ipv4)));
            } else {
                $port = $scheme === 'https' ? 443 : 80;
                $resolveHost = array(implode(':', array($this->host, $port, $ipv4)));
            }

            list($this->videoHost, $this->videoId) = array_pad(explode('~', $this->key), 2, '');
            $this->cacheMaxAge = $this->timeout($this->videoHost);

            $now = time();
            $this->cacheExpires = $now;
            $this->cacheCreated = $now;
            $cookies = [];

            $this->set_server_id($sid);
            $cache = $this->getCache($this->videoHost, $this->videoId, 0);
            if ($cache) {
                $data = json_decode($cache['data'], true);
                $this->cacheCreated = $cache['created'];
                $this->cacheExpires = $cache['expired'];
                $cookies = $data['cookies'];
                if (!empty($data['referer'])) {
                    $this->referer = $data['referer'];
                    $urlParser = parse_url($data['referer']);
                    $this->origin = $urlParser['scheme'] . '://' . $urlParser['host'];
                }
            }

            if (strpos($this->url, BASE_URL . 'tmp/') === FALSE && validate_url($this->url)) {
                if (empty($this->origin) && empty($this->referer)) {
                    $this->getReferer($this->url, $this->videoHost, $this->videoId);
                }

                $host = $port !== 80 && $port !== 443 ? $this->host . ':' . $port : $this->host;
                $this->header = $this->defaultHeaders($this->videoHost);
                $this->header[] = 'Host: ' . $host;
                $this->header[] = 'Origin: ' . $this->origin;
                $this->header[] = 'Referer: ' . $this->referer;
                $this->header[] = 'X-Requested-With: XMLHttpRequest';

                session_write_close();
                $this->ch = curl_init();
                curl_setopt($this->ch, CURLOPT_URL, $this->url);
                curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($this->ch, CURLOPT_ENCODING, '');
                curl_setopt($this->ch, CURLOPT_MAXREDIRS, 10);
                curl_setopt($this->ch, CURLOPT_TIMEOUT, 0);
                curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
                curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                curl_setopt($this->ch, CURLOPT_TIMEOUT, 0);
                curl_setopt($this->ch, CURLOPT_NOSIGNAL, true);
                curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
                curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
                curl_setopt($this->ch, CURLOPT_FAILONERROR, true);

                if (!filter_var($this->host, FILTER_VALIDATE_IP)) {
                    curl_setopt($this->ch, CURLOPT_RESOLVE, $resolveHost);
                    curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
                }

                if (!empty($cookies)) {
                    curl_setopt($this->ch, CURLOPT_COOKIE, trim(implode(';', $cookies)));
                }

                if (!filter_var(get_option('production_mode'), FILTER_VALIDATE_BOOLEAN) && count(get_resources()) < 900) {
                    curl_setopt($this->ch, CURLOPT_VERBOSE, true);
                    curl_setopt($this->ch, CURLOPT_STDERR, fopen(BASE_DIR . 'cache/streaming/curl~' . $this->videoHost . '~' . substr(keyFilter($this->videoId), 0, 200) . '.txt', 'w+'));
                }
            }
        } else {
            error_log("HLS Error {$this->videoHost} {$this->videoId} => {$this->url} => url is empty");
            $this->errorResponse(404);
        }
    }

    private function errorResponse(int $statusCode = 404)
    {
        session_write_close();
        $now = time();
        header('Cache-Control: no-store');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', $now));
        header('Expires: ' . gmdate('D, d M Y H:i:s T', $now));
        http_response_code($statusCode);
        exit();
    }

    private function getReferer(string $url = '', string $host = '', string $id = '')
    {
        session_write_close();
        if ($host === 'direct') {
            $host = parse_url($url, PHP_URL_HOST);
            if (strpos($this->host, 'gcpvuclip') !== FALSE) {
                $this->origin = 'https://www.viu.com';
                $this->referer = 'https://www.viu.com/';
            } elseif (strpos($this->host, 'tvmalaysia') !== FALSE) {
                $this->origin = 'https://www.tvmalaysia.online';
                $this->referer = 'https://www.tvmalaysia.online/';
            } elseif (
                strpos($url, 'cloudfront.net/RCTI') !== FALSE ||
                strpos($url, 'cloudfront.net/MNCTV') !== FALSE ||
                strpos($url, 'cloudfront.net/GTV') !== FALSE ||
                strpos($url, 'cloudfront.net/INEWS') !== FALSE
            ) {
                $this->origin = 'https://www.rctiplus.com';
                $this->referer = 'https://www.rctiplus.com/';
            } elseif (strpos($host, 'rctiplus') !== FALSE) {
                $this->origin = 'https://www.rctiplus.com';
                $this->referer = 'https://www.rctiplus.com/';
            } elseif (strpos($this->url, 'googlevideo.com') !== FALSE || strpos($this->url, 'googleusercontent.com') !== FALSE) {
                $this->origin = 'https://youtube.googleapis.com';
                $this->referer = 'https://youtube.googleapis.com/';
            } elseif (
                strpos($this->url, 'production-premium-vidio') !== FALSE ||
                strpos($this->url, 'vidio-com.akamaized.net') !== FALSE ||
                strpos($this->url, 'vidio.com') !== FALSE ||
                $this->host === 'live-production.secureswiftcontent.com'
            ) {
                $this->origin = 'https://www.vidio.com';
                $this->referer = 'https://www.vidio.com/';
            } elseif (strpos($this->host, 'ttvnw.net') !== FALSE) {
                $this->origin = 'https://www.twitch.tv';
                $this->referer = 'https://www.twitch.tv/';
            } else {
                $this->referer = $url;
            }
        } else {
            $class = new \VideoSources();
            $class->setCriteria('host', $host, '=');
            $class->setCriteria('host_id', $id, '=', 'AND');
            $data = $class->getOne(['data']);
            if ($data) {
                $data = json_decode($data['data'], true);
                $this->origin = rtrim($data['referer'], '/');
                $this->referer = $data['referer'];
            }
        }
    }

    private function parse(string $content = '', string $ref = '', string $key = '')
    {
        session_write_close();
        $scheme = isSSL() ? 'https:' : 'http:';
        $baseURL = strtr(BASE_URL, ['https:' => $scheme, 'http:' => $scheme]);
        $fileName = basename($ref);
        $ref = rtrim($ref, $fileName);
        $parser = parse_url($ref);
        $scheme = $parser['scheme'];
        $host = $parser['host'];
        $port = isset($parser['port']) ? ':' . $parser['port'] : '';
        $host = $scheme . '://' . $host . $port;
        if (strpos($ref, 'dailymotion.com') !== FALSE) {
            $ref = parse_url($ref, PHP_URL_SCHEME) . '://' . parse_url($ref, PHP_URL_HOST);
        } elseif (strpos($ref, 'tvmalaysia.cyou') !== false) {
            $ref = strtr($ref, ['/live/tvmalaysia/online' => '']);
        }
        if (!empty($content)) {
            $ex = explode("\n", strtr($content, ["\r\n" => "\n"]));
            $ex = array_filter($ex, function ($v) {
                return !empty($v);
            });
            $result = [];
            $xToken = !empty($this->token) ? '&token=' . rawurlencode($this->token) : '';
            $xLive = '&live=' . ($this->live ? 'true' : 'false');
            foreach ($ex as $val) {
                if (
                    strpos($val, '.m3u') !== FALSE || strpos($val, '.m3u8') !== FALSE ||
                    strpos($val, '.txt') !== FALSE || strpos($val, 'QualityLevels') !== FALSE ||
                    (strpos($val, 'dailymotion.com') !== FALSE && strpos($val, '/video') !== FALSE) ||
                    ((strpos($val, 'mycdn.me') !== FALSE || strpos($val, 'vkuser.net') !== FALSE) && strpos($val, '/video') !== FALSE) ||
                    strpos($val, 'TYPE=SUBTITLES') !== FALSE || strpos($val, 'TYPE=AUDIO') !== FALSE
                ) {
                    if (preg_match('/URI="([^"]+)"/', $val, $uri)) {
                        if (validate_url($uri[1])) {
                            $newUrl = $baseURL . 'playlist.m3u8?data=' . $key . '&url=' . encode($uri[1]) . $xLive . $xToken;
                        } else {
                            $newUrl = $baseURL . 'playlist.m3u8?data=' . $key . '&url=' . encode(trim($ref, '/') . '/' . trim($uri[1], '/')) . $xLive . $xToken;
                        }
                        $result[] = strtr($val, [$uri[1] => $newUrl]);
                    } elseif (validate_url($val) && substr(trim($val), 0, 4) !== '#EXT') {
                        $result[] = $baseURL . 'playlist.m3u8?data=' . $key . '&url=' . encode(trim($val, '/')) . $xLive . $xToken;
                    } else {
                        $result[] = $baseURL . 'playlist.m3u8?data=' . $key . '&url=' . encode(trim($ref, '/') . '/' . trim($val, '/')) . $xLive . $xToken;
                    }
                } else {
                    if (substr(trim($val), 0, 4) !== '#EXT' || strpos($val, 'URI=') !== FALSE) {
                        if (preg_match('/URI="([^"]+)"/', $val, $uri)) {
                            if (validate_url($uri[1])) {
                                $newUrl = $baseURL . 'hls.ts?data=' . $key . '&url=' . encode($uri[1]) . $xLive . $xToken;
                                $result[] = strtr($val, [$uri[1] => $newUrl]);
                            } else {
                                if (substr($uri[1], 0, 1) === '/') {
                                    $newUrl = $baseURL . 'hls.ts?data=' . $key . '&url=' . encode(trim($host) . '/' . trim($uri[1], '/')) . $xLive . $xToken;
                                    $result[] = strtr($val, [$uri[1] => $newUrl]);
                                } else {
                                    $newUrl = $baseURL . 'hls.ts?data=' . $key . '&url=' . encode(trim($ref, '/') . '/' . trim($uri[1], '/')) . $xLive . $xToken;
                                    $result[] = strtr($val, [$uri[1] => $newUrl]);
                                }
                            }
                        } elseif (validate_url($val)) {
                            $result[] = $baseURL . 'hls.ts?data=' . $key . '&url=' . encode(trim($val, '/')) . $xLive . $xToken;
                        } elseif (!empty($ref)) {
                            if (substr($val, 0, 1) === '/') {
                                $result[] = $baseURL . 'hls.ts?data=' . $key . '&url=' . encode(trim($host) . '/' . trim($val, '/')) . $xLive . $xToken;
                            } else {
                                $result[] = $baseURL . 'hls.ts?data=' . $key . '&url=' . encode(trim($ref, '/') . '/' . trim($val, '/')) . $xLive . $xToken;
                            }
                        } else {
                            $result[] = $val;
                        }
                    } else {
                        $result[] = $val;
                    }
                }
            }
            return trim(implode("\n", $result), '1');
        }
        return trim($content, '1');
    }

    private function parseCache(string $content = '')
    {
        session_write_close();
        if (!empty($content)) {
            $result = [];
            $scheme = isSSL() ? 'https:' : 'http:';
            $ex = explode("\n", strtr($content, ["\r\n" => "\n"]));
            $xToken = '&token=';
            $xToken .= !empty($this->token) ? rawurlencode($this->token) : encode(getUserIP());
            foreach ($ex as $i => $val) {
                if (!empty($val)) {
                    if (strpos($val, '/playlist') !== FALSE) {
                        if (preg_match('/URI="([^"]+)"/', $val, $uri) && validate_url($uri[1])) {
                            $newUrl = strtr($uri[1], ['https:' => $scheme, 'http:' => $scheme]) . $xToken;
                            $result[] = strtr($val, [$uri[1] => $newUrl]);
                        } elseif (validate_url($val)) {
                            $newUrl = strtr($val, ['https:' => $scheme, 'http:' => $scheme]) . $xToken;
                            $result[] = strtr($val, [$val => $newUrl]);
                        } else {
                            $result[] = $val;
                        }
                    } elseif (strpos($val, '/hls') !== FALSE) {
                        if (preg_match('/URI="([^"]+)"/', $val, $uri) && validate_url($uri[1])) {
                            $newUrl = strtr($uri[1], ['https:' => $scheme, 'http:' => $scheme]) . $xToken;
                            $result[] = strtr($val, [$uri[1] => $newUrl]);
                        } elseif (validate_url($val)) {
                            $newUrl = strtr($val, ['https:' => $scheme, 'http:' => $scheme]) . $xToken;
                            $result[] = strtr($val, [$val => $newUrl]);
                        } else {
                            $result[] = $val;
                        }
                    } else {
                        $result[] = $val;
                    }
                }
            }
            return trim(implode("\n", $result));
        }
        return trim($content);
    }

    private function setCacheHeaders(bool $useCache = false, bool $playlist = true)
    {
        session_write_close();
        if ($useCache) {
            header('Cache-Control: private');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', $this->cacheCreated));
            header('Expires: ' . gmdate('D, d M Y H:i:s T', $this->cacheExpires));
        } else {
            if ($playlist) {
                header('Cache-Control: no-store');
            } else {
                $cacheExpires = $this->cacheCreated + 300;
                header('Cache-Control: max-age=300');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', $this->cacheCreated));
                header('Expires: ' . gmdate('D, d M Y H:i:s T', $cacheExpires));
            }
        }
    }

    function playlistStream()
    {
        session_write_close();

        header('Accept-Ranges: bytes');
        header('Content-Type: application/vnd.apple.mpegurl');
        $this->setCacheHeaders(false, true);

        $prefix = $this->videoHost . '~' . keyFilter($this->videoId);
        $pLen = strlen($prefix);

        $fileName = keyFilter($this->url);
        $fLen = strlen($fileName);

        $wLen = $pLen + $fLen;
        if ($wLen <= 250) {
            $offset = 0;
            $length = $fLen;
        } else {
            $offset = $fLen - $pLen;
            $length = 250 - $pLen;
        }

        $fileName = substr(rtrim($fileName, 'm3u8'), $offset, $length);
        $cache = BASE_DIR . 'cache/playlist/' . $prefix . '~' . $fileName . '.m3u8';
        if (file_exists($cache)) {
            $timeIntv = time() - filemtime($cache);
            if ($timeIntv < $this->cacheMaxAge) {
                open_resources_handler();
                $fp = @fopen($cache, 'r');
                if ($fp) {
                    stream_set_blocking($fp, false);
                    $content = stream_get_contents($fp);
                    fclose($fp);

                    $this->live = false;

                    echo $this->parseCache($content);
                    ob_flush();
                    flush();
                    exit();
                }
            }
        }
        @unlink($cache);
        $keyEncode = encode($this->key);
        if (strpos($this->url, BASE_URL . 'tmp/') !== FALSE) {
            open_resources_handler();
            $fp = @fopen(strtr($this->url, [BASE_URL => BASE_DIR]), 'r');
            if ($fp) {
                stream_set_blocking($fp, false);
                $content = stream_get_contents($fp);
                fclose($fp);

                if (strpos($content, '#EXT-X-ENDLIST') !== FALSE) {
                    $this->live = false;
                    $content = $this->parse($content, $this->url, $keyEncode);
                    $saveContent = strtr($content, ['&token=' . $this->token => '', '&token=' . rawurlencode($this->token) => '']);
                    create_file($cache, $saveContent);
                } else {
                    $this->live = true;
                    $content = $this->parse($content, $this->url, $keyEncode);
                }
                echo $content;
                ob_flush();
                flush();
            } else {
                error_log("Playlist HLS Error {$this->videoHost} {$this->videoId} => {$this->url} => file Not found");
                $this->errorResponse(404);
            }
        } elseif ($this->ch) {
            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->header);
            curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($ch, $line) {
                session_write_close();
                return strlen($line);
            });

            $response = curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);
            curl_close($this->ch);

            if ($status >= 200 && $status < 400) {
                if (strpos($response, '#EXTM3U') !== FALSE) {
                    if (strpos($response, '#EXTINF:') !== FALSE) {
                        if (strpos($response, '#EXT-X-ENDLIST') !== FALSE) {
                            $this->live = false;
                            $content = $this->parse($response, $this->url, $keyEncode);
                            $saveContent = strtr($content, ['&token=' . $this->token => '', '&token=' . rawurlencode($this->token) => '']);
                            create_file($cache, $saveContent);
                        } else {
                            $this->live = true;
                            $content = $this->parse($response, $this->url, $keyEncode);
                        }
                    } else {
                        $this->live = false;
                        $content = $this->parse($response, $this->url, $keyEncode);
                        $saveContent = strtr($content, ['&token=' . $this->token => '', '&token=' . rawurlencode($this->token) => '']);
                        create_file($cache, $saveContent);
                    }
                    echo $content;
                    ob_flush();
                    flush();
                } else {
                    $this->errorResponse(415);
                }
            } else {
                error_log("HLS Playlist Error {$this->videoHost} {$this->videoId} => {$this->url} => $status: $err");
                $this->errorResponse($status);
            }
        } else {
            error_log("HLS Playlist Error {$this->videoHost} {$this->videoId} => {$this->url} => curl instance not found");
        }
    }

    function stream(string $range = '')
    {
        session_write_close();
        if ($this->ch) {
            if (in_array($this->videoHost, $this->bad_hosts())) {
                sleep(rand(1, 5));
                $proxy = rate_limit_ips();
                if ($proxy) {
                    $this->header[] = 'X-Originating-IP: ' . $proxy;
                    $this->header[] = 'X-Forwarded-For: ' . $proxy;
                    $this->header[] = 'X-Remote-IP: ' . $proxy;
                    $this->header[] = 'X-Remote-Addr: ' . $proxy;
                    $this->header[] = 'X-Client-IP: ' . $proxy;
                    $this->header[] = 'X-Host: ' . $proxy;
                    $this->header[] = 'X-Forwared-Host: ' . $proxy;
                }
            }

            if (isset($_SERVER['HTTP_IF_RANGE'])) {
                $this->header[] = 'if-range: ' . $_SERVER['HTTP_IF_RANGE'];
            }

            if (!empty($range)) {
                $this->header[] = 'range: ' . $range;
                $fmp4 = true;
                http_response_code(206);
            } else {
                $fmp4 = false;
                http_response_code(200);
            }

            $this->live = isset($_GET['live']) && filter_var($_GET['live'], FILTER_VALIDATE_BOOLEAN);
            if ($this->live) {
                $this->setCacheHeaders(false, true);
            } else {
                $this->setCacheHeaders(true, false);
            }

            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, false);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->header);
            curl_setopt($this->ch, CURLOPT_BUFFERSIZE, 10485760);
            curl_setopt($this->ch, CURLOPT_HEADER, false);
            curl_setopt($this->ch, CURLOPT_NOBODY, false);
            curl_setopt($this->ch, CURLOPT_HEADERFUNCTION, function ($curl, $header) use ($fmp4) {
                session_write_close();
                list($key, $val) = array_pad(explode(':', strtolower($header)), 2, '');
                if (in_array($key, $this->allowResponseHeader) || ($fmp4 && $key === 'content-length')) header($header);
                return strlen($header);
            });
            curl_setopt($this->ch, CURLOPT_WRITEFUNCTION, function ($curl, $body) {
                session_write_close();
                echo $body;
                ob_flush();
                flush();
                return strlen($body);
            });

            curl_exec($this->ch);
            $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            $err = curl_error($this->ch);
            curl_close($this->ch);

            if ($status === 401 || $status === 429 || $status >= 500 || ($status === 403 && in_array($this->videoHost, $this->bad_hosts()))) {
                $class = new \HLS($this->qry);
                $class->stream($range);
                exit();
            } elseif ($status < 200 || $status >= 400) {
                error_log("HLS Error {$this->videoHost} {$this->videoId} => {$this->url} => $status: $err");
                $this->errorResponse($status);
            }
        } else {
            error_log("HLS Stream Error {$this->videoHost} {$this->videoId} => {$this->url} => curl instance not found");
        }
    }

    function __destruct()
    {
        session_write_close();
    }
}
