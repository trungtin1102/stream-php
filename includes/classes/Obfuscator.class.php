<?php
class Obfuscator extends Minify
{
    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function cryptoJsAesDecrypt($passphrase, $jsonString)
    {
        session_write_close();
        $jsondata = json_decode($jsonString, true);
        $salt = hex2bin($jsondata["s"]);
        $ct = base64_decode($jsondata["ct"]);
        $iv  = hex2bin($jsondata["iv"]);
        $concatedPassphrase = $passphrase . $salt;
        $md5 = array();
        $md5[0] = md5($concatedPassphrase, true);
        $result = $md5[0];
        for ($i = 1; $i < 3; $i++) {
            $md5[$i] = md5($md5[$i - 1] . $concatedPassphrase, true);
            $result .= $md5[$i];
        }
        $key = substr($result, 0, 32);
        $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
        return json_decode($data, true);
    }

    function cryptoJsAesEncrypt($passphrase, $value)
    {
        session_write_close();
        $salt = openssl_random_pseudo_bytes(8);
        $salted = '';
        $dx = '';
        while (strlen($salted) < 48) {
            $dx = md5($dx . $passphrase . $salt, true);
            $salted .= $dx;
        }
        $key = substr($salted, 0, 32);
        $iv  = substr($salted, 32, 16);
        $encrypted_data = openssl_encrypt(json_encode($value, true), 'aes-256-cbc', $key, true, $iv);
        $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
        return json_encode($data, true);
    }

    function jsObfustator(string $script = '')
    {
        session_write_close();
        $script = trim($this->minify_js($script));
        $script = $this->cryptoJsAesEncrypt("TvHay-JASm(8234_)9312HJASi23lakka", $script);
        $script = "eval(_decx('$script'));";
        return $script;
    }
}
