<?php
class GDrive_Auth
{
    protected $id = '';
    protected $title = '';
    protected $email = '';
    protected $parents = [];
    protected $nextPageToken = '';
    protected $userLimit = [];
    protected $baseUrl = 'https://www.googleapis.com';
    protected $encTitle = TRUE;
    protected $mirror;
    protected $db;
    protected $ch;
    protected $iCache;
    protected $videoHash;

    function __construct(string $id = '', string $email = '')
    {
        session_write_close();

        $this->iCache = new \InstanceCache();
        $this->db = new \GDriveAuth();
        $this->mirror = new \GDriveMirrors();
        $this->videoHash = new \VideosHash();

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($this->ch, CURLOPT_ENCODING, '');
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($this->ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($this->ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_USERAGENT, USER_AGENT);
        if (!filter_var(get_option('production_mode'), FILTER_VALIDATE_BOOLEAN) && count(get_resources()) < 900) {
            create_dir(BASE_DIR . 'cache');
            create_dir(BASE_DIR . 'cache/streaming');
            curl_setopt($this->ch, CURLOPT_VERBOSE, true);
            curl_setopt($this->ch, CURLOPT_STDERR, fopen(BASE_DIR . 'cache/streaming/curl~gdrive_auth.txt', 'w+'));
        }

        $this->set_id($id);
        $this->set_email($email);
        $this->get_access_tokens();
    }

    /**
     * Buat folder pada google drive
     * @param string $name Nama folder
     * @param array $parents Lokasi folder, masukkan daftar array id folder dimana folder baru tersebut akan disimpan
     */
    function create_folder(string $name = '', array $parents = ['root'], string $email = '')
    {
        session_write_close();
        $email = !empty($email) ? $email : $this->email;
        if (!empty($name) && !empty($email)) {
            $token = $this->get_access_token($email);
            if ($token) {
                curl_setopt($this->ch, CURLOPT_URL, $this->baseUrl . '/drive/v3/files');
                curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($this->ch, CURLOPT_NOBODY, 0);
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode(array(
                    'name' => $name,
                    'parents' => $parents,
                    'mimeType' => 'application/vnd.google-apps.folder'
                )));
                curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                    'accept: application/json',
                    'content-type: application/json',
                    'authorization: ' . $token['token_type'] . ' ' . $token['access_token']
                ));

                $response = curl_exec($this->ch);
                $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                $err = curl_error($this->ch);

                if ($status >= 200 && $status < 400) {
                    return json_decode($response, TRUE);
                } else {
                    error_log("gdrive_auth create_folder => $status: $err");
                }
            }
        }
        return false;
    }

    /**
     * Enkripsi judul ke md5 yang berguna untuk melewati inspeksi gdrive dengan harapan akan membuat akun gdrive tidak mudah diblokir
     */
    function encrypt_title(bool $encrypt = TRUE)
    {
        session_write_close();
        $this->encTitle = $encrypt;
    }

    /**
     * Atur id file gdrive
     * @param string $id masukkan string/url
     */
    function set_id(string $id = '')
    {
        session_write_close();
        if (validate_url($id)) {
            $this->id = getDriveId($id);
        } else {
            $this->id = $id;
        }
    }

    /**
     * Ambil id file gdrive yang telah diatur sebelumnya
     * @return string
     */
    function get_id()
    {
        session_write_close();
        return $this->id;
    }

    /**
     * Atur title file gdrive
     * @param string $title
     */
    function set_title(string $title = '')
    {
        session_write_close();
        $this->title = $title;
    }

    /**
     * Atur parent file gdrive
     * @param array $parent masukkan parent file gdrive misalnya ['root']
     */
    function set_parent(array $parent = [])
    {
        session_write_close();
        $this->parents = $parent;
    }

    /**
     * Atur email owner dari file gdrive, ini sangat penting untuk mengambil detail file nantinya
     * @param string $email
     */
    function set_email(string $email = '')
    {
        session_write_close();
        if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) $this->email = $email;
    }

    /**
     * Ambil email owner dari file gdrive
     * @return string
     */
    function get_email()
    {
        session_write_close();
        return $this->email;
    }

    /**
     * Ambil next page token yang diatur sebelumnya, ini akan terisi otomatis saat pengambilan daftar file gdrive
     * @return string
     */
    function get_nextPageToken()
    {
        session_write_close();
        return $this->nextPageToken;
    }

    /**
     * Ambil data semua akun grive yang ada di database
     * @return array|false
     */
    function get_accounts()
    {
        session_write_close();
        $this->iCache->setKey('gdrive_accounts');
        $cache = $this->iCache->get();
        if ($cache) {
            return $cache;
        } else {
            $this->db->setCriteria('status', 1, '=');
            $this->db->setOrderBy('email');
            $list = $this->db->get();
            if ($list) {
                $this->iCache->save($list, 5);
            }
            return $list;
        }
    }

    /**
     * Ambil data akun gdrive yang ada di database berdasarkan email yang telah diatur sebelumnya
     * @param string $email parameter ini boleh dikosongkan jika email telah diatur sebelumnya, misal $this->set_email('email@gmail.com')
     * @return array|false
     */
    function get_account($email)
    {
        session_write_close();
        if (!empty($email)) {
            $this->iCache->setKey('gdrive_account_' . md5($email));
            $cache = $this->iCache->get();
            if ($cache) {
                return $cache;
            } else {
                $this->db->setCriteria('email', $email, '=');
                $data = $this->db->getOne(['email', 'api_key', 'client_id', 'client_secret', 'refresh_token']);
                if ($data) {
                    $this->iCache->save($data, 5, 'gdrive_account');
                }
                return $data;
            }
        }
        return false;
    }

    /**
     * Ambil access token dari semua akun gdrive dan simpan hasilnya ke cache
     * @return array|false
     */
    function get_access_tokens()
    {
        session_write_close();
        try {
            $accounts = $this->get_accounts();
            if ($accounts) {
                $tokens = [];
                $notInCache = [];
                foreach ($accounts as $userData) {
                    $this->iCache->setKey('gdrive_access_token~' . md5($userData['email']));
                    $cache = $this->iCache->get();
                    if ($cache) {
                        $tokens[] = $cache;
                    } else {
                        $notInCache[] = $userData;
                    }
                }
                if (!empty($notInCache)) {
                    $mh = curl_multi_init();
                    $ch = [];

                    foreach ($notInCache as $i => $user) {
                        $ch[$i] = curl_init($this->baseUrl . '/oauth2/v4/token');
                        curl_setopt($ch[$i], CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch[$i], CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch[$i], CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt($ch[$i], CURLOPT_MAXREDIRS, 5);
                        curl_setopt($ch[$i], CURLOPT_ENCODING, '');
                        curl_setopt($ch[$i], CURLOPT_TIMEOUT, 30);
                        curl_setopt($ch[$i], CURLOPT_CUSTOMREQUEST, 'POST');
                        curl_setopt($ch[$i], CURLOPT_POSTFIELDS, http_build_query([
                            'client_id' => $user['client_id'],
                            'client_secret' => $user['client_secret'],
                            'refresh_token' => $user['refresh_token'],
                            'grant_type' => 'refresh_token'
                        ]));
                        curl_setopt($ch[$i], CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                        curl_setopt($ch[$i], CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
                        curl_setopt($ch[$i], CURLOPT_DNS_SERVERS, dns_servers());
                        curl_setopt($ch[$i], CURLOPT_TCP_NODELAY, true);
                        curl_setopt($ch[$i], CURLOPT_FORBID_REUSE, true);
                        curl_setopt($ch[$i], CURLOPT_FAILONERROR, true);
                        curl_setopt($ch[$i], CURLOPT_USERAGENT, USER_AGENT);
                        curl_setopt($ch[$i], CURLOPT_HEADERFUNCTION, function ($ch, $line) {
                            session_write_close();
                            return strlen($line);
                        });
                        curl_multi_add_handle($mh, $ch[$i]);
                    }

                    $active = null;
                    do {
                        $mrc = curl_multi_exec($mh, $active);
                    } while ($mrc == CURLM_CALL_MULTI_PERFORM);

                    while ($active && $mrc == CURLM_OK) {
                        if (curl_multi_select($mh) == -1) {
                            usleep(10);
                        }
                        do {
                            $mrc = curl_multi_exec($mh, $active);
                        } while ($mrc == CURLM_CALL_MULTI_PERFORM);
                    }

                    foreach ($notInCache as $i => $user) {
                        $response = curl_multi_getcontent($ch[$i]);
                        $status = curl_getinfo($ch[$i], CURLINFO_HTTP_CODE);
                        if ($status >= 200 && $status < 400) {
                            $result = json_decode($response, TRUE);
                            if (!empty($result['access_token']) && !empty($result['token_type'])) {
                                $this->iCache->setKey('gdrive_access_token~' . md5($user['email']));
                                $this->iCache->save($result, 3500, 'gdrive_access_token');
                                $tokens[] = $result;
                            } else {
                                $this->db->setCriteria('email', $user['email'], '=');
                                $this->db->update(array(
                                    'status' => 0,
                                    'modified' => time()
                                ));
                            }
                        }
                        curl_multi_remove_handle($mh, $ch[$i]);
                    }
                    curl_multi_close($mh);
                }
                return $tokens;
            }
        } catch (\PDOException | \Exception $e) {
            error_log('get_access_tokens => ' . $e->getMessage());
        }
        return FALSE;
    }

    /**
     * Ambil access token dari akun gdrive yang telah ditentukan sebelumnya dan simpan hasilnya ke cache
     * @param string $email parameter ini boleh dikosongkan jika email telah diatur sebelumnya, misal $this->set_email('email@gmail.com')
     * @return array|false
     */
    function get_access_token(string $email = '')
    {
        session_write_close();
        try {
            $email = !empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL) ? $email : $this->email;
            if (!empty($email)) {
                $user = $this->get_account($email);
                if ($user) {
                    $this->iCache->setKey('gdrive_access_token~' . md5($email));
                    $cache = $this->iCache->get();
                    if ($cache && !empty($cache['token_type']) && !empty($cache['access_token'])) {
                        return $cache;
                    } else {
                        curl_setopt($this->ch, CURLOPT_URL, $this->baseUrl . '/oauth2/v4/token');
                        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($this->ch, CURLOPT_NOBODY, 0);
                        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
                        curl_setopt($this->ch, CURLOPT_POSTFIELDS, http_build_query([
                            'client_id' => $user['client_id'],
                            'client_secret' => $user['client_secret'],
                            'refresh_token' => $user['refresh_token'],
                            'grant_type' => 'refresh_token'
                        ]));

                        $response = curl_exec($this->ch);
                        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                        $err = curl_error($this->ch);

                        if ($status >= 200 && $status < 400) {
                            $token = json_decode($response, TRUE);
                            if (!empty($token['token_type']) && !empty($token['access_token'])) {
                                // simpan access token ke dalam cache
                                $this->iCache->save($token, 3500, 'gdrive_access_token');
                                return $token;
                            } else {
                                // nonaktifkan akun jika tidak bisa membuat access token
                                $this->db->setCriteria('email', $user['email'], '=');
                                $this->db->update(array(
                                    'status' => 0,
                                    'modified' => time()
                                ));
                            }
                        } else {
                            error_log("get_access_token $email => $status: $err => $response");
                        }
                    }
                }
            } else {
                $tokens = $this->get_access_tokens();
                if ($tokens) {
                    $key = array_rand($tokens);
                    return $tokens[$key];
                }
            }
        } catch (\Exception $e) {
            error_log('get_access_token => ' . $e->getMessage());
        }
        return FALSE;
    }

    /**
     * Ambil info about semua akun gdrive yang ada di database dan simpan hasilnya ke cache
     * @return array|false
     */
    function get_accounts_info()
    {
        session_write_close();
        try {
            $users = $this->get_accounts();
            if ($users) {
                $tokens = $this->get_access_tokens();
                if ($tokens) {
                    $info = [];
                    $notInCache = [];
                    $notInCacheTokens = [];

                    foreach ($users as $i => $user) {
                        $this->iCache->setKey('gdrive_about~' . md5($user['email']));
                        $cache = $this->iCache->get();
                        if ($cache) {
                            $info[$user['email']] = $cache;
                        } else {
                            $notInCache[] = $user;
                            $notInCacheTokens[] = $tokens[$i];
                        }
                    }

                    if (!empty($notInCache)) {
                        $mh = curl_multi_init();
                        $ch = [];

                        foreach ($notInCache as $i => $user) {
                            $ch[$i] = curl_init($this->baseUrl . '/drive/v2/about?includeSubscribed=false');
                            curl_setopt($ch[$i], CURLOPT_SSL_VERIFYHOST, 0);
                            curl_setopt($ch[$i], CURLOPT_SSL_VERIFYPEER, 0);
                            curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch[$i], CURLOPT_FOLLOWLOCATION, 1);
                            curl_setopt($ch[$i], CURLOPT_MAXREDIRS, 5);
                            curl_setopt($ch[$i], CURLOPT_ENCODING, '');
                            curl_setopt($ch[$i], CURLOPT_TIMEOUT, 30);
                            curl_setopt($ch[$i], CURLOPT_CUSTOMREQUEST, 'GET');
                            curl_setopt($ch[$i], CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                            curl_setopt($ch[$i], CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
                            curl_setopt($ch[$i], CURLOPT_DNS_SERVERS, dns_servers());
                            curl_setopt($ch[$i], CURLOPT_TCP_NODELAY, true);
                            curl_setopt($ch[$i], CURLOPT_FORBID_REUSE, true);
                            curl_setopt($ch[$i], CURLOPT_FAILONERROR, true);
                            curl_setopt($ch[$i], CURLOPT_USERAGENT, USER_AGENT);
                            curl_setopt($ch[$i], CURLOPT_HTTPHEADER, array(
                                'authorization: ' . $notInCacheTokens[$i]['token_type'] . ' ' . $notInCacheTokens[$i]['access_token'],
                            ));
                            curl_setopt($ch[$i], CURLOPT_HEADERFUNCTION, function ($ch, $line) {
                                session_write_close();
                                return strlen($line);
                            });
                            curl_multi_add_handle($mh, $ch[$i]);
                        }

                        $active = null;
                        do {
                            $mrc = curl_multi_exec($mh, $active);
                        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

                        while ($active && $mrc == CURLM_OK) {
                            if (curl_multi_select($mh) == -1) {
                                usleep(10);
                            }
                            do {
                                $mrc = curl_multi_exec($mh, $active);
                            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
                        }

                        foreach ($notInCache as $i => $user) {
                            $response = curl_multi_getcontent($ch[$i]);
                            $status = curl_getinfo($ch[$i], CURLINFO_HTTP_CODE);

                            if ($status >= 200 && $status < 400) {
                                $result = json_decode($response, TRUE);
                                if (isset($result['error'])) {
                                    $info[$user['email']] = [];
                                    if (isset($result['error']['message'])) error_log('get_accounts_info ' . $user['email'] . ' => ' . $result['error']['message']);
                                    else error_log('get_accounts_info ' . $user['email'] . ' => ' . $result['error']);
                                } else {
                                    $this->iCache->save($result, 2592000, 'gdrive_about');
                                    $info[$user['email']] = $result;
                                }
                            } else {
                                $info[$user['email']] = [];
                            }
                            curl_multi_remove_handle($mh, $ch[$i]);
                        }
                        curl_multi_close($mh);
                    }
                    return $info;
                }
            }
        } catch (\Exception $e) {
            error_log('get_accounts_info => ' . $e->getMessage());
        }
        return FALSE;
    }

    /**
     * Ambil info about akun gdrive yang telah ditentukan sebelumnya dan simpan hasilnya ke cache
     * @param string $email parameter ini boleh dikosongkan jika email telah diatur sebelumnya, misal $this->set_email('email@gmail.com')
     * @return array|false
     */
    function get_account_info(string $email = '')
    {
        session_write_close();
        try {
            $token = $this->get_access_token($email);
            if ($token) {
                $this->iCache->setKey('gdrive_about~' . md5($email));
                $cache = $this->iCache->get();
                if ($cache) {
                    return $cache;
                } else {
                    curl_setopt($this->ch, CURLOPT_URL, $this->baseUrl . '/drive/v2/about?includeSubscribed=false');
                    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt($this->ch, CURLOPT_NOBODY, 0);
                    curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
                    curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                        'authorization: ' . $token['token_type'] . ' ' . $token['access_token'],
                    ));

                    $response = curl_exec($this->ch);
                    $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                    $err = curl_error($this->ch);

                    if ($status >= 200 && $status < 400) {
                        $result = json_decode($response, TRUE);
                        if (!isset($result['error'])) {
                            $this->iCache->save($result, 2592000, 'gdrive_about');
                            return $result;
                        }
                    } else {
                        error_log("get_account_info => $status: $err => $response");
                    }
                }
            }
        } catch (\Exception $e) {
            error_log('get_account_info => ' . $e->getMessage());
        }
        return FALSE;
    }

    /**
     * Ambil info file gdrive
     * @param string $gdrive_id atur id file gdrive, parameter ini boleh dikosongkan jika id file telah diatur sebelumnya, misal $this->set_id('1KPtwrGMyjC9c_ZpOID4eLaSDErrA5x3O')
     * @param string $email atur email owner/bukan untuk mengambil token, parameter ini boleh dikosongkan jika email telah diatur sebelumnya, misal $this->set_email('email@gmail.com')
     * @return array|false
     */
    function get_file_info(string $gdrive_id = '', string $email = '')
    {
        session_write_close();
        try {
            $id = !empty($gdrive_id) ? getDriveId($gdrive_id) : $this->id;
            if (!empty($id)) {
                $this->videoHash->setCriteria('host', 'gdrive', '=');
                $this->videoHash->setCriteria('host_id', $id, '=', 'AND');
                $data = $this->videoHash->getOne(['data', 'gdrive_email']);
                if ($data && !empty($data['data'])) {
                    return json_decode($data['data'], true);
                } else {
                    $token = false;
                    $email = !empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL) ? $email : $this->email;
                    if (!empty($email)) {
                        $token = $this->get_access_token($email);
                    } else {
                        $tokens = $this->get_access_tokens();
                        if ($tokens) {
                            $key = array_rand($tokens);
                            $token = $tokens[$key];
                        }
                    }
                    if ($token) {
                        curl_setopt($this->ch, CURLOPT_URL, $this->baseUrl . '/drive/v2/files/' . $id . '?alt=json&supportsAllDrives=true');
                        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt($this->ch, CURLOPT_HEADER, false);
                        curl_setopt($this->ch, CURLOPT_NOBODY, false);
                        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
                        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                            'authorization: ' . $token['token_type'] . ' ' . $token['access_token'],
                        ));
                        $response = curl_exec($this->ch);
                        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                        $err = curl_error($this->ch);

                        if ($status >= 200 && $status < 400) {
                            $file = json_decode($response, TRUE);
                            if (!empty($file['id'])) {
                                if (!empty($file['owners'][0]['emailAddress'])) {
                                    $this->videoHash->setCriteria('host', 'gdrive', '=');
                                    $this->videoHash->setCriteria('host_id', $file['id'], '=', 'AND');
                                    if ($this->videoHash->getNumRows() === 0) {
                                        $hash_host = md5(strrev('gdrive'));
                                        $hash_id = md5(strrev($file['id']));

                                        $this->videoHash->insert(array(
                                            'host' => 'gdrive',
                                            'host_id' => $file['id'],
                                            'gdrive_email' => $file['owners'][0]['emailAddress'],
                                            'hash_host' => $hash_host,
                                            'hash_id' => $hash_id,
                                            'data' => $response
                                        ));
                                    } else {
                                        $this->videoHash->setCriteria('host', 'gdrive');
                                        $this->videoHash->setCriteria('host_id', $file['id'], '=', 'AND');
                                        $this->videoHash->update(array(
                                            'gdrive_email' => $file['owners'][0]['emailAddress'],
                                            'data' => $response
                                        ));
                                    }
                                }
                                return $file;
                            } else {
                                $this->mirror->setCriteria('gdrive_id', $id, '=');
                                $this->mirror->setCriteria('mirror_id', $id, '=', 'OR');
                                $this->mirror->setLimit(0, 10);
                                $data = $this->mirror->get(['gdrive_id', 'mirror_id', 'mirror_email']);
                                if ($data) {
                                    $result = [];
                                    foreach ($data as $dt) {
                                        if ($dt['gdrive_id'] !== $id) {
                                            $result[] = $dt['gdrive_id'];
                                        } elseif ($dt['mirror_id'] !== $id) {
                                            $result[] = $dt['mirror_id'];
                                        }
                                    }
                                    if (isset($result[0])) return $this->get_file_info($result[0]['mirror_id'], $result[0]['mirror_email']);
                                    else error_log("get_file_info $id => $response");
                                } else {
                                    error_log("get_file_info $id => $response");
                                }
                            }
                        } else {
                            error_log("get_file_info $id => $status: $err => $response");
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            error_log('get_file_info => ' . $e->getMessage());
        }
        return FALSE;
    }

    /**
     * Ambil daftar file dari akun gdrive tapi pertama kali harus mengatur email, misal $this->set_email('email@gmail.com')
     * @param string $title atur ini jika mau mengambil file dengan judul tertentu atau kosongkan jika tidak
     * @param int $maxResult atur ini untuk mengambil sejumlah file yang diinginkan
     * @param string $orderBy atur ini untuk mensortir file yang ada, misal: 'title desc'
     * @param string $pageToken atur ini untuk pergi ke halaman berikutnya
     * @param bool $private true untuk mengambil file private saja
     * @return array|false
     */
    function get_files(string $title = '', int $maxResult = 25, string $orderBy = 'modifiedDate desc', string $pageToken = '', bool $private = FALSE, string $folder_id = '', bool $onlyFolder = false)
    {
        session_write_close();
        try {
            $token = $this->get_access_token();
            if (!empty($token['token_type']) && !empty($token['access_token'])) {
                $query = [
                    'corpora' => 'allDrives',
                    'includeItemsFromAllDrives' => 'true',
                    'supportsAllDrives' => 'true',
                    'maxResults' => $maxResult,
                    'orderBy' => $orderBy,
                ];

                if (!empty($title)) {
                    $title = addslashes(htmlspecialchars_decode($title));
                    $titleEncode = htmlspecialchars($title);
                    if ($this->encTitle) {
                        $encTitle = md5($title);
                        $query['q'] = "(title='$title' or title='$titleEncode' or title='$encTitle' or title contains '$title' or title contains '$titleEncode' or title contains '$encTitle' or fullText contains '$title' or fullText contains '$titleEncode')";
                    } else {
                        $query['q'] = "(title='$title' or title='$titleEncode' or title contains '$title' or title contains '$titleEncode')";
                    }
                } else {
                    $query['q'] = "(mimeType contains 'video/' or mimeType contains 'audio/' or mimeType contains '/octet-stream' or mimeType='application/vnd.google-apps.folder')";
                }

                if ($onlyFolder) {
                    $query['q'] .= " and mimeType='application/vnd.google-apps.folder'";
                } else {
                    $query['q'] .= " and (mimeType contains 'video/' or mimeType contains 'audio/' or mimeType contains '/octet-stream' or mimeType='application/vnd.google-apps.folder')";
                }

                if ($private) {
                    $query['q'] .= " and (visibility = 'limited')";
                }

                if (!empty($pageToken)) {
                    $query['pageToken'] = $pageToken;
                }

                if (!empty($folder_id)) {
                    $query['q'] .= " and ('{$folder_id}' in parents or sharedWithMe)";
                }

                curl_setopt($this->ch, CURLOPT_URL, $this->baseUrl . '/drive/v2/files?' . http_build_query($query));
                curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($this->ch, CURLOPT_NOBODY, 0);
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                    'authorization: ' . $token['token_type'] . ' ' . $token['access_token'],
                ));

                $response = curl_exec($this->ch);
                $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                $err = curl_error($this->ch);

                if ($status >= 200 && $status < 400) {
                    $arr = json_decode($response, TRUE);
                    if (isset($arr['items'])) {
                        $this->nextPageToken = !empty($arr['nextPageToken']) ? $arr['nextPageToken'] : '';
                        if (!empty($arr['items'])) {
                            $result = [];
                            $hash_host = md5(strrev('gdrive'));
                            foreach ($arr['items'] as $dt) {
                                $result[] = $dt;
                                $this->videoHash->setCriteria('host', 'gdrive', '=');
                                $this->videoHash->setCriteria('host_id', $dt['id'], '=', 'AND');
                                $row = $this->videoHash->getOne(['data']);
                                if (!$row) {
                                    $hash_id = md5(strrev($dt['id']));
                                    $this->videoHash->insert(array(
                                        'host' => 'gdrive',
                                        'host_id' => $dt['id'],
                                        'gdrive_email' => $this->email,
                                        'hash_host' => $hash_host,
                                        'hash_id' => $hash_id,
                                        'data' => json_encode($dt, true)
                                    ));
                                }
                            }
                            return $result;
                        }
                    } else {
                        error_log('get_files ' . $this->email . ' => ' . $response);
                    }
                } else {
                    error_log("get_files {$this->email} => $status: $err => $response");
                }
            } else {
                error_log('get_files token error => ' . json_encode($token));
            }
        } catch (\Exception $e) {
            error_log('get_files => ' . $e->getMessage());
        }
        return FALSE;
    }

    /**
     * Ambil daftar file dari semua akun gdrive
     * @param string $title atur ini jika mau mengambil file dengan judul tertentu atau kosongkan jika tidak
     * @param int $maxResult atur ini untuk mengambil sejumlah file yang diinginkan
     * @param string $orderBy atur ini untuk mensortir file yang ada, misal: 'title desc'
     * @param string $pageToken atur ini untuk pergi ke halaman berikutnya
     * @param bool $private true untuk mengambil file private saja
     * @return array|false multidimesional array|false
     */
    function get_files_all_accounts(string $title = '', int $maxResult = 5, string $orderBy = 'modifiedDate desc', string $pageToken = '', bool $private = FALSE)
    {
        session_write_close();
        $tokens = $this->get_access_tokens();
        if ($tokens) {
            $query = [
                'corpora' => 'allDrives',
                'includeItemsFromAllDrives' => 'true',
                'supportsAllDrives' => 'true',
                'maxResults' => $maxResult,
                'orderBy' => $orderBy
            ];

            if (!empty($title)) {
                $title = addslashes(htmlspecialchars_decode($title));
                $titleEncode = htmlspecialchars($title);
                if ($this->encTitle) {
                    $encTitle = md5($title);
                    $query['q'] = "(title='$title' or title='$titleEncode' or title='$encTitle' or title contains '$title' or title contains '$titleEncode' or title contains '$encTitle' or fullText contains '$title' or fullText contains '$titleEncode') and (mimeType contains 'video/' or mimeType contains 'audio/' or mimeType contains '/octet-stream')";
                } else {
                    $query['q'] = "(title='$title' or title='$titleEncode' or title contains '$title' or title contains '$titleEncode') and (mimeType contains 'video/' or mimeType contains 'audio/' or mimeType contains '/octet-stream')";
                }
            } else {
                $query['q'] = "(mimeType contains 'video/' or mimeType contains 'audio/' or mimeType contains '/octet-stream')";
            }

            if ($private) {
                $query['q'] .= " and (visibility = 'limited')";
            }

            if (!empty($pageToken)) {
                $query['pageToken'] = $pageToken;
            }

            $url = $this->baseUrl . '/drive/v2/files?' . http_build_query($query);

            $mh = curl_multi_init();
            $ch = [];
            foreach ($tokens as $i => $token) {
                $ch[$i] = curl_init($url);
                curl_setopt($ch[$i], CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch[$i], CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch[$i], CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch[$i], CURLOPT_MAXREDIRS, 5);
                curl_setopt($ch[$i], CURLOPT_ENCODING, '');
                curl_setopt($ch[$i], CURLOPT_TIMEOUT, 30);
                curl_setopt($ch[$i], CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($ch[$i], CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                curl_setopt($ch[$i], CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
                curl_setopt($ch[$i], CURLOPT_DNS_SERVERS, dns_servers());
                curl_setopt($ch[$i], CURLOPT_TCP_NODELAY, true);
                curl_setopt($ch[$i], CURLOPT_FORBID_REUSE, true);
                curl_setopt($ch[$i], CURLOPT_FAILONERROR, true);
                curl_setopt($ch[$i], CURLOPT_USERAGENT, USER_AGENT);
                curl_setopt($ch[$i], CURLOPT_HTTPHEADER, array(
                    'authorization: ' . $token['token_type'] . ' ' . $token['access_token'],
                ));
                curl_setopt($ch[$i], CURLOPT_HEADERFUNCTION, function ($ch, $line) {
                    session_write_close();
                    return strlen($line);
                });
                curl_multi_add_handle($mh, $ch[$i]);
            }

            $active = null;
            do {
                $mrc = curl_multi_exec($mh, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);

            while ($active && $mrc == CURLM_OK) {
                if (curl_multi_select($mh) == -1) {
                    usleep(10);
                }
                do {
                    $mrc = curl_multi_exec($mh, $active);
                } while ($mrc == CURLM_CALL_MULTI_PERFORM);
            }
            $result = [];
            foreach ($tokens as $i => $token) {
                $response = curl_multi_getcontent($ch[$i]);
                $status = curl_getinfo($ch[$i], CURLINFO_HTTP_CODE);

                if ($status >= 200 && $status < 400) {
                    $arr = json_decode($response, TRUE);
                    if (!empty($arr['items'])) {
                        $result[] = $arr['items'];
                    }
                }
                curl_multi_remove_handle($mh, $ch[$i]);
            }
            curl_multi_close($mh);
            if (!empty($result)) return $result;
        }
        return FALSE;
    }

    /**
     * Perbarui permissions file gdrive
     * @return array|false
     */
    function insert_permissions(string $id = '', string $email = '', array $permission = [])
    {
        session_write_close();
        if (!empty($id) && !empty($email)) {
            $token = $this->get_access_token($email);
            if ($token) {
                if (empty($permission)) {
                    $permission = [
                        'role' => 'reader',
                        'type' => 'anyone'
                    ];
                }

                curl_setopt($this->ch, CURLOPT_URL, $this->baseUrl . '/drive/v2/files/' . $id . '/permissions?moveToNewOwnersRoot=true&supportsAllDrives=true');
                curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($this->ch, CURLOPT_NOBODY, 0);
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($permission));
                curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                    'authorization: ' . $token['token_type'] . ' ' . $token['access_token'],
                    'accept: application/json',
                    'content-type: application/json',
                ));

                $response = curl_exec($this->ch);
                $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                $err = curl_error($this->ch);

                if ($status >= 200 && $status < 400) {
                    $result = json_decode($response, TRUE);
                    if (isset($result['error'])) {
                        error_log("insert_permissions $id => $response");
                    } else {
                        return $result;
                    }
                } else {
                    error_log("insert_permissions $id => $status: $err => $response");
                }
            }
        }
        return FALSE;
    }

    function delete_permissions(string $id = '', string $email = '', string $permission_id = 'anyone')
    {
        session_write_close();
        if (!empty($id) && !empty($email)) {
            $permission_id = !empty($permission_id) ? $permission_id : 'anyone';
            $token = $this->get_access_token($email);
            if ($token) {
                curl_setopt($this->ch, CURLOPT_URL, $this->baseUrl . '/drive/v2/files/' . $id . '/permissions/' . $permission_id);
                curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($this->ch, CURLOPT_NOBODY, 0);
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                    'authorization: ' . $token['token_type'] . ' ' . $token['access_token'],
                ));

                $response = curl_exec($this->ch);
                $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                $err = curl_error($this->ch);

                if ($status >= 200 && $status < 400) {
                    $result = json_decode($response, TRUE);
                    if (isset($result['error'])) {
                        error_log("delete_permissions $id => " . $result['error']['message']);
                    } else {
                        return $result;
                    }
                } else {
                    error_log("delete_permissions $id => $status: $err => $response");
                }
            }
        }
        return FALSE;
    }

    function copy_file_3rd_party()
    {
        session_write_close();

        $url = 'https://jnckmedia.com/gdrive/bypassNL';
        $scheme = parse_url($url, PHP_URL_SCHEME);
        $host = parse_url($url, PHP_URL_HOST);
        $port = parse_URL($url, PHP_URL_PORT);
        if (empty($port)) $port = $scheme == 'https' ? 443 : 80;
        $ipv4 = gethostbyname($host . '.');
        $resolveHost = implode(':', array($host, $port, $ipv4));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_RESOLVE, array($resolveHost));
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'link=' . rawurlencode('https://drive.google.com/uc?id=' . $this->id . '&export=download'));
        curl_setopt($ch, CURLOPT_DNS_SERVERS, dns_servers());
        curl_setopt($ch, CURLOPT_TCP_NODELAY, true);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'cache-control: no-cache',
            'content-type: application/x-www-form-urlencoded; charset=UTF-8',
            'origin: https://jnckmedia.com',
            'pragma: no-cache',
            'referer: https://jnckmedia.com/gdrive/nologin',
            'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36 OPR/77.0.4054.203',
            'x-requested-with: XMLHttpRequest'
        ));
        curl_setopt($ch, CURLOPT_HEADERFUNCTION, function ($ch, $line) {
            session_write_close();
            return strlen($line);
        });
        // cek penggunaan proxy
        $proxy = proxy_rotator();
        if ($proxy) {
            curl_setopt($ch, CURLOPT_PROXY, $proxy['proxy']);
            curl_setopt($ch, CURLOPT_PROXYTYPE, $proxy['type']);
            curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxy['usrpwd']);
        }

        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $err = curl_error($ch);
        curl_close($ch);

        if ($status >= 200 && $status < 400) {
            if (preg_match_all('/href="([^"]+)"/', strtr($response, ["'" => '"']), $href) && !empty($href[1])) {
                return $this->get_file_info($href[1][0]);
            }
        } elseif (strpos($response, 'File not found') === false) {
            error_log("copy_file_3rd_party {$this->id} => proxy {$proxy['format']} doesn't work => $status: $err");
            $proxyList = proxy_list();
            $key = array_search($proxy['format'], $proxyList);
            if ($key) {
                $brokenProxy = $proxyList[$key];
                unset($proxyList[$key]);
                $proxyList = array_values($proxyList);
                if (count($proxyList) > 0) {
                    if (!filter_var(get_option('delete_unused_proxy'), FILTER_VALIDATE_BOOLEAN)) $proxyList[] = $brokenProxy;
                    $proxyList = trim(implode("\n", $proxyList));
                    set_option('proxy_list', $proxyList);
                    return $this->copy_file_3rd_party();
                } else {
                    set_option('proxy_list', []);
                }
            }
        }
        return FALSE;
    }

    /**
     * Copy file gdrive
     * @return array|false multidimensional array|array|false
     */
    function copy_file_now()
    {
        session_write_close();
        $info = $this->get_file_info();
        if ($info) {
            $user = isset($info['owners'][0]['emailAddress']) ? $this->get_account($info['owners'][0]['emailAddress']) : false;
            if ($user) {
                return $info;
            } else {
                $token = FALSE;
                if (!empty($this->email)) {
                    $token = $this->get_access_token($this->email);
                } else {
                    $tokens = $this->get_access_tokens();
                    if ($tokens) {
                        $key = array_rand($tokens);
                        $token = $tokens[$key];
                    }
                }
                if ($token) {
                    $data = array(
                        'parents' => ['root']
                    );
                    if (!empty($this->title)) {
                        $data['title'] = $this->encTitle ? md5($this->title) . '.' . $info['fileExtension'] : $this->title;
                        $data['description'] = $this->encTitle ? $this->title : 'copy by TvHay.top tool';
                    } else {
                        $data['title'] = $this->encTitle ? md5($info['title']) . '.' . $info['fileExtension'] : $info['title'];
                        $data['description'] = $this->encTitle ? $info['title'] : 'copy by TvHay.top tool';
                    }
                    curl_setopt($this->ch, CURLOPT_URL, $this->baseUrl . '/drive/v2/files/' . $info['id'] . '/copy?supportsAllDrives=true');
                    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt($this->ch, CURLOPT_NOBODY, 0);
                    curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($data));
                    curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                        'authorization: ' . $token['token_type'] . ' ' . $token['access_token'],
                        'accept: application/json',
                        'content-type: application/json',
                    ));

                    $response = curl_exec($this->ch);
                    $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);

                    if ($status >= 200 && $status < 400) {
                        $file = json_decode($response, TRUE);
                        if (!empty($file['id']) && !empty($file['owners'][0]['emailAddress'])) {
                            $updated = $this->insert_permissions($file['id'], $file['owners'][0]['emailAddress']);
                            if ($updated) {
                                $saved = false;
                                $this->mirror->setCriteria('mirror_id', $file['id']);
                                if ($this->mirror->getNumRows() === 0) {
                                    $saved = $this->mirror->insert(array(
                                        'gdrive_id' => $info['id'],
                                        'mirror_id' => $file['id'],
                                        'mirror_email' => $file['owners'][0]['emailAddress'],
                                        'added' => time()
                                    ));
                                } else {
                                    $saved = true;
                                }
                                if ($saved) return $file;
                            } else {
                                error_log("GDrive_Auth copy_file_now {$file['id']} => {$file['owners'][0]['emailAddress']} => cannot insert permissions");
                            }
                        } else {
                            return $this->copy_file_3rd_party();
                        }
                    }
                }
            }
        }
        return FALSE;
    }

    function copy_file_to_all()
    {
        session_write_close();
        $info = $this->get_file_info();
        if ($info) {
            $tokens = $this->get_access_tokens();
            if ($tokens) {
                $data = array(
                    'parents' => ['root']
                );
                if (!empty($this->title)) {
                    $data['title'] = $this->encTitle ? md5($this->title) . '.' . $info['fileExtension'] : $this->title;
                    $data['description'] = $this->encTitle ? $this->title : 'copy by TvHay.top tool';
                } else {
                    $data['title'] = $this->encTitle ? md5($info['title']) . '.' . $info['fileExtension'] : $info['title'];
                    $data['description'] = $this->encTitle ? $info['title'] : 'copy by TvHay.top tool';
                }

                $mh = curl_multi_init();
                $ch = [];
                foreach ($tokens as $i => $token) {
                    $ch[$i] = curl_init($this->baseUrl . '/drive/v2/files/' . $info['id'] . '/copy?supportsAllDrives=true');
                    curl_setopt($ch[$i], CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch[$i], CURLOPT_SSL_VERIFYPEER, 0);
                    curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch[$i], CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt($ch[$i], CURLOPT_MAXREDIRS, 5);
                    curl_setopt($ch[$i], CURLOPT_ENCODING, '');
                    curl_setopt($ch[$i], CURLOPT_TIMEOUT, 30);
                    curl_setopt($ch[$i], CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($ch[$i], CURLOPT_POSTFIELDS, json_encode($data));
                    curl_setopt($ch[$i], CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                    curl_setopt($ch[$i], CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
                    curl_setopt($ch[$i], CURLOPT_DNS_SERVERS, dns_servers());
                    curl_setopt($ch[$i], CURLOPT_TCP_NODELAY, true);
                    curl_setopt($ch[$i], CURLOPT_FORBID_REUSE, true);
                    curl_setopt($ch[$i], CURLOPT_FAILONERROR, true);
                    curl_setopt($ch[$i], CURLOPT_HTTPHEADER, array(
                        'authorization: ' . $token['token_type'] . ' ' . $token['access_token'],
                        'accept: application/json',
                        'content-type: application/json',
                    ));
                    curl_setopt($ch[$i], CURLOPT_USERAGENT, USER_AGENT);
                    curl_setopt($ch[$i], CURLOPT_HEADERFUNCTION, function ($ch, $line) {
                        session_write_close();
                        return strlen($line);
                    });
                    curl_multi_add_handle($mh, $ch[$i]);
                }

                $active = null;
                do {
                    $mrc = curl_multi_exec($mh, $active);
                } while ($mrc == CURLM_CALL_MULTI_PERFORM);

                while ($active && $mrc == CURLM_OK) {
                    if (curl_multi_select($mh) == -1) {
                        usleep(10);
                    }
                    do {
                        $mrc = curl_multi_exec($mh, $active);
                    } while ($mrc == CURLM_CALL_MULTI_PERFORM);
                }

                $result = [];
                foreach ($tokens as $i => $token) {
                    $response = curl_multi_getcontent($ch[$i]);
                    $status = curl_getinfo($ch[$i], CURLINFO_HTTP_CODE);
                    $err = curl_error($ch[$i]);
                    if ($status >= 200 && $status < 400) {
                        $file = json_decode($response, TRUE);
                        if (!empty($file['id'])) {
                            $updated = $this->insert_permissions($file['id'], $file['owners'][0]['emailAddress']);
                            if ($updated) {
                                $this->mirror->setCriteria('mirror_id', $file['id']);
                                if ($this->mirror->getNumRows() === 0) {
                                    $this->mirror->insert(array(
                                        'gdrive_id' => $info['id'],
                                        'mirror_id' => $file['id'],
                                        'mirror_email' => $file['owners'][0]['emailAddress'],
                                        'added' => time()
                                    ));
                                    $result[] = $file;
                                } else {
                                    $result[] = $file;
                                }
                            } else {
                                error_log("GDRive_Auth copy_file_to_all {$this->id} => {$file['id']} => cannot update permissions");
                            }
                        } else {
                            error_log("GDRive_Auth copy_file_to_all {$this->id} => {$file['id']} => id is empty");
                        }
                    } else {
                        error_log("GDRive_Auth copy_file_to_all {$this->id} => $status: $err");
                    }
                    curl_multi_remove_handle($mh, $ch[$i]);
                }
                curl_multi_close($mh);
                return $result;
            }
        }
        return false;
    }

    /**
     * Cek apakah file sudah limit atau belum
     * @param string $id atur id file gdrive, parameter ini boleh dikosongkan jika id file telah diatur sebelumnya, misal $this->set_id('1KPtwrGMyjC9c_ZpOID4eLaSDErrA5x3O')
     * @return bool
     */
    function download_checker(string $id = '')
    {
        session_write_close();
        try {
            $id = !empty($id) ? getDriveId($id) : $this->id;
            if (!empty($id)) {
                $token = false;
                if (!empty($this->email)) {
                    $token = $this->get_access_token($this->email);
                } else {
                    $users = $this->get_accounts();
                    if ($users) {
                        $key = array_rand($users);
                        $token = $this->get_access_token($users[$key]['email']);
                    }
                }
                if ($token) {
                    curl_setopt($this->ch, CURLOPT_URL, $this->baseUrl . '/drive/v2/files/' . $id . '?alt=media&source=downloadUrl');
                    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 0);
                    curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt($this->ch, CURLOPT_NOBODY, 1);
                    curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
                    curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                        'authorization: ' . $token['token_type'] . ' ' . $token['access_token']
                    ));

                    curl_exec($this->ch);
                    $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                    $type = curl_getinfo($this->ch, CURLINFO_CONTENT_TYPE);

                    return $status >= 200 && $status < 400 && preg_match('/video|audio/', $type, $mimeType) && !empty($mimeType);
                }
            }
        } catch (\Exception $e) {
            error_log('download_checker => ' . $e->getMessage());
        }
        return FALSE;
    }

    /**
     * Untuk mencari file mirror yang ada di akun gdrive menggunakan title
     * @param string $id atur id file yg ingin dicari file mirror nya
     * @param bool $single atur true jika mau mengambil banyak file, false jika mau mengambil satu saja
     */
    function get_mirrors(bool $single = true, string $id = '')
    {
        session_write_close();
        $result = [];
        $id = !empty($id) ? getDriveId($id) : $this->id;
        $this->mirror->setCriteria('gdrive_id', $id, '=');
        $this->mirror->setCriteria('mirror_id', $id, '=', 'OR');
        $this->mirror->setLimit(0, 10);
        $rows = $this->mirror->get(['gdrive_id', 'mirror_id']);
        if ($rows) {
            // periksa file mirror di database
            foreach ($rows as $row) {
                if (!empty($row['mirror_id']) && $row['mirror_id'] !== $id) {
                    $item = $this->get_file_info($row['mirror_id']);
                    $result[] = $item;
                } elseif (!empty($row['gdrive_id']) && $row['gdrive_id'] !== $id) {
                    $item = $this->get_file_info($row['gdrive_id']);
                    $result[] = $item;
                }
            }
        } else {
            // periksa file mirror di official
            $info = $this->get_file_info($id);
            if (!empty($info['title'])) {
                if (!empty($info['description'])) {
                    $useDesc = [];
                    $onDesc = ['copy by', 'downloaded', 'uploaded', 'upload', 'download', 'mirror'];
                    foreach ($onDesc as $find) {
                        $useDesc[] = strpos(strtolower($info['description']), $find);
                    }
                    $useDesc = array_unique($useDesc);
                    $useDesc = array_values($useDesc);
                    $ctitle = count($useDesc);
                    if ($ctitle === 1 && $useDesc[0] === false) {
                        $this->title =  $info['description'];
                    } else {
                        $this->title =  $info['title'];
                    }
                } else {
                    $this->title = $info['title'];
                }
                if (!empty($this->title)) {
                    $list = $this->get_files_all_accounts($this->title, 10);
                    if ($list) {
                        $now = time();
                        foreach ($list as $items) {
                            foreach ($items as $dt) {
                                $result[] = $dt;
                                $this->mirror->setCriteria('gdrive_id', $dt['id'], '=');
                                $this->mirror->setCriteria('mirror_id', $dt['id'], '=', 'OR');
                                if ($this->mirror->getNumRows() === 0 && $id !== $dt['id']) {
                                    $this->mirror->insert(array(
                                        'gdrive_id' => $id,
                                        'mirror_id' => $dt['id'],
                                        'mirror_email' => $dt['owners'][0]['emailAddress'],
                                        'added' => $now
                                    ));
                                }
                            }
                        }
                    }
                }
            }
        }
        // ditemukan file
        if (!empty($result)) {
            if ($single) {
                $key = array_rand($result);
                return $result[$key];
            } else {
                return $result;
            }
        }
        return FALSE;
    }

    /**
     * Hapus sebuah file gdrive. Fungsi ini memerlukan email owner-nya, untuk itu ambil email owner-nya dengan $this->get_file_info() terlebih dahulu atau langsung atur email ownernya, misal $this->set_email('owner@gmail.com')
     * @param string $gdrive_id, parameter ini boleh dikosongkan jika id file telah diatur sebelumnya, misal $this->set_id('1KPtwrGMyjC9c_ZpOID4eLaSDErrA5x3O')
     * @return bool
     */
    function delete_file(string $gdrive_id = '')
    {
        session_write_close();
        try {
            $id = !empty($gdrive_id) ? $gdrive_id : $this->id;
            if (!empty($id)) {
                $token = $this->get_access_token();
                if ($token) {
                    curl_setopt($this->ch, CURLOPT_URL, $this->baseUrl . '/drive/v2/files/' . $id . '?supportsAllDrives=true');
                    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt($this->ch, CURLOPT_NOBODY, 0);
                    curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                    curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                        'authorization: ' . $token['token_type'] . ' ' . $token['access_token'],
                    ));

                    $response = curl_exec($this->ch);
                    $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
                    $err = curl_error($this->ch);

                    if ($status >= 200 && $status < 400) {
                        if (empty($response)) {
                            $this->mirror->setCriteria('gdrive_id', $id, '=');
                            $this->mirror->setCriteria('mirror_id', $id, '=', 'OR');
                            return $this->mirror->delete();
                        } else {
                            error_log('delete_file ' . $id . ' => ' . $response);
                        }
                    } else {
                        error_log("delete_file $id => $status: $err => $response");
                    }
                }
            }
        } catch (\PDOException | \Exception $e) {
            error_log('delete_file => ' . $e->getMessage());
        }
        return FALSE;
    }

    /**
     * Hapus beberapa file gdrive. Fungsi ini memerlukan email owner-nya, untuk itu ambil email owner-nya dengan $this->get_file_info() terlebih dahulu atau langsung atur email ownernya, misal $this->set_email('owner@gmail.com')
     * @param string $gdrive_ids, parameter ini harus diisi
     * @return array|bool multidimesional array|array|false
     */
    function delete_files(array $gdrive_ids = [])
    {
        session_write_close();
        try {
            if (!empty($gdrive_ids)) {
                $token = $this->get_access_token();
                if ($token) {
                    $mh = curl_multi_init();
                    $ch = [];

                    foreach ($gdrive_ids as $i => $id) {
                        $ch[$i] = curl_init($this->baseUrl . '/drive/v2/files/' . $id . '?supportsAllDrives=true');
                        curl_setopt($ch[$i], CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch[$i], CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch[$i], CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt($ch[$i], CURLOPT_MAXREDIRS, 5);
                        curl_setopt($ch[$i], CURLOPT_ENCODING, '');
                        curl_setopt($ch[$i], CURLOPT_TIMEOUT, 30);
                        curl_setopt($ch[$i], CURLOPT_CUSTOMREQUEST, 'DELETE');
                        curl_setopt($ch[$i], CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                        curl_setopt($ch[$i], CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
                        curl_setopt($ch[$i], CURLOPT_DNS_SERVERS, dns_servers());
                        curl_setopt($ch[$i], CURLOPT_TCP_NODELAY, true);
                        curl_setopt($ch[$i], CURLOPT_FORBID_REUSE, true);
                        curl_setopt($ch[$i], CURLOPT_FAILONERROR, true);
                        curl_setopt($ch[$i], CURLOPT_USERAGENT, USER_AGENT);
                        curl_setopt($ch[$i], CURLOPT_HTTPHEADER, array(
                            'authorization: ' . $token['token_type'] . ' ' . $token['access_token'],
                        ));
                        curl_setopt($ch[$i], CURLOPT_HEADERFUNCTION, function ($ch, $line) {
                            session_write_close();
                            return strlen($line);
                        });
                        curl_multi_add_handle($mh, $ch[$i]);
                    }

                    $active = null;
                    do {
                        $mrc = curl_multi_exec($mh, $active);
                    } while ($mrc == CURLM_CALL_MULTI_PERFORM);

                    while ($active && $mrc == CURLM_OK) {
                        if (curl_multi_select($mh) == -1) {
                            usleep(10);
                        }
                        do {
                            $mrc = curl_multi_exec($mh, $active);
                        } while ($mrc == CURLM_CALL_MULTI_PERFORM);
                    }

                    $deleted = [];
                    foreach ($gdrive_ids as $i => $id) {
                        $response = curl_multi_getcontent($ch[$i]);
                        $status = curl_getinfo($ch[$i], CURLINFO_HTTP_CODE);

                        if ($status >= 200 && $status < 400) {
                            $result = json_decode($response, TRUE);
                            if (isset($result['error'])) {
                                $deleted[] = [
                                    'id' => $id,
                                    'status' => 'fail',
                                    'error' => $result['error']
                                ];
                                error_log('delete_files ' . $id . ' => ' . $response);
                            } else {
                                $this->mirror->setCriteria('gdrive_id', $id, '=');
                                $this->mirror->setCriteria('mirror_id', $id, '=', 'OR');
                                $wasDeleted = $this->mirror->delete();
                                if ($wasDeleted) {
                                    $deleted[] = [
                                        'id' => $id,
                                        'status' => 'ok'
                                    ];
                                } else {
                                    $deleted[] = [
                                        'id' => $id,
                                        'status' => 'fail'
                                    ];
                                }
                            }
                        } else {
                            $deleted[] = [
                                'id' => $id,
                                'status' => 'fail'
                            ];
                        }
                        curl_multi_remove_handle($mh, $ch[$i]);
                    }
                    curl_multi_close($mh);
                    return $deleted;
                }
            }
        } catch (\PDOException | \Exception $e) {
            error_log('delete_files => ' . $e->getMessage());
        }
        return FALSE;
    }

    function __destruct()
    {
        session_write_close();
        $this->db = null;
        unset($this->db);
        curl_close($this->ch);
    }
}
