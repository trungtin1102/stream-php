<?php
class InstanceCache
{
    private $iCache;
    private $key = '';
    private $prefix = '';

    function __construct()
    {
        session_write_close();
        global $InstanceCache;
        $this->iCache = $InstanceCache;
        $this->prefix = keyFilter(BASE_URL);
    }

    function setKey(string $key = '')
    {
        session_write_close();
        $this->key = !empty($key) ? $this->prefix . '_' . keyFilter($key) : '';
    }

    function getKey()
    {
        session_write_close();
        return $this->key;
    }

    function get(string $key = '')
    {
        session_write_close();
        $key = !empty($key) ? $this->prefix . '_' . keyFilter($key) : $this->key;
        $cache = $this->iCache->getItem($key);
        if ($cache->isHit()) return $cache->get();
        else return false;
    }

    function save($data = null, $expiresAfter = null, $tags = null)
    {
        session_write_close();
        if (!empty($this->key) && !is_null($data)) {
            $cache = $this->iCache->getItem($this->key);
            $cache->set($data)->expiresAfter($expiresAfter);
            if (!empty($tags)) {
                if (is_array($tags)) {
                    $addTags = [];
                    foreach ($tags as $dt) {
                        $addTags[] = $this->prefix . '_' . $dt;
                    }
                    $cache->addTags($addTags);
                } else {
                    $cache->addTag($this->prefix . '_' . $tags);
                }
            }
            return $this->iCache->save($cache);
        }
        return false;
    }

    function delete(string $key = '')
    {
        session_write_close();
        $key = (!empty($key) ? $this->prefix . '_' . keyFilter($key) : $this->key);
        return $this->iCache->deleteItem($key);
    }

    function deleteItemsByTag(string $tag = '', int $strategy = 1)
    {
        session_write_close();
        if (!empty($tag)) return $this->iCache->deleteItemsByTag($this->prefix . '_' . $tag, $strategy);
        else return false;
    }

    function deleteItemsByTags(array $tags = [], int $strategy = 1)
    {
        session_write_close();
        if (!empty($tags)) {
            $deleteTags = [];
            foreach ($tags as $dt) {
                $deleteTags[] = $this->prefix . '_' . $dt;
            }
            return $this->iCache->deleteItemsByTags($deleteTags, $strategy);
        }
        return false;
    }

    function clear()
    {
        session_write_close();
        $deleted[] = $this->iCache->clear();
        $CacheInstance = get_option('cache_instance');
        if ($CacheInstance === 'redis') {
            if (function_exists('exec')) {
                exec('redis-cli FLUSHALL > /dev/null &', $output);
                $output = array_values(array_filter($output));
                $deleted[] = in_array('OK', $output);
            }
        }
        return in_array(true, $deleted);
    }
}
