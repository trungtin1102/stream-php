<?php
class Player extends Core
{
    private $thisSite = '';
    private $mainSite = '';
    private $referer = '';
    private $ipAddress = '';
    private $lang = 'en';
    private $policy = '';
    private $siteName = '';
    private $title = '';
    private $poster = '';
    private $ga = '';
    private $gtm_head = '';
    private $gtm_body = '';
    private $histats = '';
    private $popupads = '';
    private $share = '';
    private $embedOnly = false;
    private $themeColor = '673AB7';
    private $newQuery = [];
    private $query = [];
    private $queryString = '';
    private $isDenied = false;
    private $isBlocked = false;
    private $isNotFound = false;
    private $isLoadBalancer = false;
    private $isInvalid = false;
    private $loadBalancerLink = '';
    private $token = '';
    private $disableShortenerLink = false;
    private $altLinks = [];
    private $altSelected = -1;
    private $supportedSites = [];
    private $hashKey = '';
    private $iCache;
    private $randomHost = false;

    function __construct()
    {
        session_write_close();

        $this->iCache = new \InstanceCache();

        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $this->lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        }
        $this->policy = isSSL() ? '<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">' : '';
        $this->siteName = sitename();

        $replace = ['https:' => '', 'http:' => '', 'www.' => ''];
        $this->thisSite = (isSSL() ? 'https:' : 'http:') . strtr(BASE_URL, $replace);

        $mainSite = get_option('main_site');
        $this->mainSite = (isSSL() ? 'https:' : 'http:') . strtr($mainSite, $replace);

        $this->randomHost = filter_var(get_option('load_balancer_rand'), FILTER_VALIDATE_BOOLEAN) ? 'true' : 'false';
        $this->isLoadBalancer = is_load_balancer();
        $this->loadBalancerLink = $this->thisSite;

        if (!empty($_SERVER['HTTP_REFERER'])) {
            $this->referer = $_SERVER['HTTP_REFERER'];
        } elseif (!empty($_SERVER['HTTP_ORIGIN'])) {
            $this->referer = $_SERVER['HTTP_ORIGIN'];
        }

        $this->ipAddress = getUserIP();
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
            if (strpos($ua, 'censysinspect') !== false || strpos($ua, 'censys.io') !== false || strpos($ua, 'headlesschrome') !== false) {
                $this->isDenied = true;
            } elseif (is_smartTV()) {
                $class = new \WhichBrowser\Parser($_SERVER['HTTP_USER_AGENT']);
                $this->token = hash('SHA256', $class->device->manufacturer . '~' . (isset($class->device->series) ? $class->device->series . '~' : '') . (isset($class->os->name) ? $class->os->name . '~' : '') . $class->engine->name);
            } else {
                $this->token = encode($this->ipAddress);
            }
        } else {
            $this->token = encode($this->ipAddress);
        }

        $this->embedOnly = filter_var(get_option('embed_only'), FILTER_VALIDATE_BOOLEAN);
        if (accessValidation()) {
            $this->get_video();
        } else {
            $this->isDenied = true;
        }
    }

    private function language(string $key = '')
    {
        session_write_close();
        $list = [
            '{text_rewind}' => get_option('text_rewind') ?? 'Rewind 10 Seconds',
            '{text_forward}' => get_option('text_forward') ?? 'Forward 10 Seconds',
            '{text_download}' => get_option('text_download') ?? 'Download {title}',
            '{text_title}' => get_option('text_title') ?? 'Watch {title} - {siteName}',
            '{text_resume}' => get_option('text_resume') ?? 'Welcome back! You left off at <span id="timez"></span>. Would you like to resume watching?',
            '{text_resume_no}' => get_option('text_resume_no') ?? 'No, Thanks',
            '{text_resume_yes}' => get_option('text_resume_yes') ?? 'Yes, Please',
            '{text_close_ads}' => get_option('text_close_ads') ?? 'Close Ads',
            '{text_loading}' => get_option('text_loading') ?? 'Please wait...',
        ];
        if (!empty($key) && isset($list[$key])) {
            return $list[$key];
        } else {
            return $list;
        }
    }

    private function get_video()
    {
        session_write_close();
        if (isset($_GET['alt'])) {
            $this->altSelected = (int) $_GET['alt'];
        }

        $class = new \Hosting();
        $this->supportedSites = $class->supportedSites();

        $this->ga = trim(include('includes/ga.php'));
        $this->gtm_head = trim(include('includes/gtm_head.php'));
        $this->gtm_body = trim(include('includes/gtm_body.php'));
        $this->histats = trim(include('includes/histats.php'));
        $this->popupads = filter_var(get_option('disable_popup_ads'), FILTER_VALIDATE_BOOLEAN) ? '' : strtr(trim(include('includes/popupads.php')), [')}' => ');}']);
        $this->share = trim(include('includes/share.php'));

        $themeColor = get_option('pwa_themecolor');
        if (!empty($themeColor)) {
            $this->themeColor = $themeColor;
        }

        $this->disableShortenerLink = filter_var(get_option('disable_shortener_link'), FILTER_VALIDATE_BOOLEAN);

        // url path parser
        $basePath = trim(parse_url($this->thisSite, PHP_URL_PATH), '/');
        $qry = '?' . parse_url($this->thisSite, PHP_URL_QUERY);
        $uri = trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
        $uri = ltrim($uri, $basePath);
        $hash = array_values(array_filter(explode('/', strtr($uri, [$qry => '']))));

        // url query parser
        if (!empty($_SERVER['QUERY_STRING']) && !isset($_GET['alt'])) {
            $this->queryString = decode($_SERVER['QUERY_STRING']);
            if ($this->queryString) {
                parse_str($this->queryString, $this->query);
            }
        }
        
        if (isset($this->query['source']) && !empty($this->query['id'])) {
            $class = new \Videos();
            $class->setCriteria('id', $this->query['id']);
            $data = $class->getOne(['id', 'title', 'poster', 'host', 'host_id', 'ahost', 'ahost_id']);
            if ($data) {
                $this->title = strip_tags(htmlspecialchars_decode($data['title']));
                $this->isBlocked = is_title_blacklisted($this->title);
                if (!$this->isBlocked) {
                    if (empty($this->poster) && !empty($data['poster'])) {
                        if (validate_url($data['poster'])) {
                            $this->poster = $this->mainSite . 'poster/?url=' . $this->get_poster_cache($data['poster']);
                        } else {
                            $this->poster = $this->mainSite . 'uploads/images/' . $data['poster'];
                        }
                    }
                    $this->newQuery = [
                        'host' => $data['host'],
                        'id' => $data['host_id'],
                        'ahost' => $data['ahost'],
                        'aid' => $data['ahost_id']
                    ];
                    if (isset($this->supportedSites[$data['host']])) {
                        $this->altLinks[] = [
                            'alt' => -1,
                            'name' => $this->supportedSites[$data['host']],
                            'host' => $data['host']
                        ];
                    }
                    if (!empty($data['ahost']) && !empty($data['ahost_id']) && isset($this->supportedSites[$data['ahost']])) {
                        $this->altLinks[] = [
                            'alt' => 0,
                            'name' => $this->supportedSites[$data['ahost']],
                            'host' => $data['ahost']
                        ];
                    }

                    // get multiple alternatives
                    $class = new \VideosAlternatives();
                    $class->setCriteria('vid', $this->query['id']);
                    if ($class->getNumRows() > 0) {
                        $class->setCriteria('vid', $this->query['id']);
                        $class->setOrderBy('order');
                        $list = $class->get(['id', 'host']);
                        if ($list) {
                            foreach ($list as $dt) {
                                if (isset($this->supportedSites[$dt['host']])) {
                                    $this->altLinks[] = [
                                        'alt' => $dt['id'],
                                        'name' => $this->supportedSites[$dt['host']],
                                        'host' => $dt['host']
                                    ];
                                }
                            }
                        }
                    }
                }
            } else {
                $this->isNotFound = true;
            }
        } elseif (!empty($this->query['host']) && !empty($this->query['id'])) {
            $this->poster = !empty($this->query['poster']) ? $this->mainSite . 'poster/?url=' . $this->get_poster_cache(rawurldecode($this->query['poster'])) : '';
            $this->newQuery = $this->query;
            if (isset($this->supportedSites[$this->query['host']])) {
                $this->altLinks[] = [
                    'alt' => -1,
                    'name' => $this->supportedSites[$this->query['host']],
                    'host' => $this->query['host']
                ];
            }
            $class = new \Videos();
            $class->setCriteria('host', $this->query['host']);
            $class->setCriteria('host_id', $this->query['id'], '=', 'AND');
            if (!empty($this->query['ahost']) && !empty($this->query['aid'])) {
                $class->setCriteria('ahost', $this->query['ahost'], '=', 'AND');
                $class->setCriteria('ahost_id', $this->query['aid'], '=', 'AND');
                if (isset($this->supportedSites[$this->query['ahost']])) {
                    $this->altLinks[] = [
                        'alt' => 0,
                        'name' => $this->supportedSites[$this->query['ahost']],
                        'host' => $this->query['ahost']
                    ];
                }
            }
            $data = $class->getOne(['id', 'title', 'poster']);
            if ($data) {
                $this->title = strip_tags(htmlspecialchars_decode($data['title']));
                $this->isBlocked = is_title_blacklisted($this->title);
                if (!$this->isBlocked) {
                    if (empty($this->poster) && !empty($data['poster'])) {
                        if (validate_url($data['poster'])) {
                            $this->poster = $this->mainSite . 'poster/?url=' . $this->get_poster_cache($data['poster']);
                        } else {
                            $this->poster = $this->mainSite . 'uploads/images/' . $data['poster'];
                        }
                    }
                }
            }
        } elseif (!empty($hash[1])) {
            $this->hashKey = rawurldecode($hash[1]);
            $class = new \VideoShort();
            $class->setCriteria('key', $this->hashKey);
            $data = $class->getOne(['vid']);
            if ($data) {
                $class = new \Videos();
                $class->setCriteria('id', $data['vid']);
                $data = $class->getOne(['id', 'title', 'poster', 'host', 'host_id', 'ahost', 'ahost_id', 'uid']);
                if ($data) {
                    $this->title = strip_tags(htmlspecialchars_decode($data['title']));
                    $this->isBlocked = is_title_blacklisted($this->title);
                    if (!$this->isBlocked) {
                        if (empty($this->poster) && !empty($data['poster'])) {
                            if (validate_url($data['poster'])) {
                                $this->poster = $this->thisSite . 'poster/?url=' . $this->get_poster_cache($data['poster']);
                            } else {
                                $this->poster = $this->mainSite . 'uploads/images/' . $data['poster'];
                            }
                        }
                        $this->query = [
                            'source' => 'db',
                            'id' => $data['id']
                        ];
                        $this->newQuery = [
                            'host' => $data['host'],
                            'id' => $data['host_id'],
                            'ahost' => $data['ahost'],
                            'aid' => $data['ahost_id']
                        ];
                        if (isset($this->supportedSites[$data['host']])) {
                            $this->altLinks[] = [
                                'alt' => -1,
                                'name' => $this->supportedSites[$data['host']],
                                'host' => $data['host']
                            ];
                        }
                        if (!empty($data['ahost']) && !empty($data['ahost_id']) && isset($this->supportedSites[$data['ahost']])) {
                            $this->altLinks[] = [
                                'alt' => 0,
                                'name' => $this->supportedSites[$data['ahost']],
                                'host' => $data['ahost']
                            ];
                        }

                        // get multiple alternatives
                        $class = new \VideosAlternatives();
                        $class->setCriteria('vid', $this->query['id']);
                        $class->setOrderBy('order');
                        $list = $class->get(['id', 'host']);
                        if ($list) {
                            foreach ($list as $dt) {
                                if (isset($this->supportedSites[$dt['host']])) {
                                    $this->altLinks[] = [
                                        'alt' => $dt['id'],
                                        'name' => $this->supportedSites[$dt['host']],
                                        'host' => $dt['host']
                                    ];
                                }
                            }
                        }
                    }
                } else {
                    $this->isNotFound = true;
                }
            } else {
                $this->isNotFound = true;
            }
        } else {
            $this->isNotFound = true;
        }
    }

    private function get_poster_cache(string $url = '')
    {
        session_write_close();
        if (!empty($url)) {
            $this->iCache->setKey('poster_' . $url);
            $cache = $this->iCache->get();
            if ($cache) {
                return $cache;
            } else {
                $url = encode($url);
                $this->iCache->save($url, 2592000, 'poster');
                return $url;
            }
        }
        return '';
    }

    function embed_response_invalid(array $data = [])
    {
        session_write_close();

        $content = '';
        open_resources_handler();
        $fp = @fopen(BASE_DIR . 'includes/templates/invalid.html', 'r');
        if ($fp) {
            stream_set_blocking($fp, false);
            $content = stream_get_contents($fp);
            fclose($fp);

            $min = new \Minify();
            $content = $min->minify_html($content);
        }

        $params = [];
        if (!empty($data)) {
            $params = [
                '{themeColor}' => '#' . $this->themeColor,
                '{lang}' => $this->lang,
                '{thisSite}' => $this->thisSite,
                '{siteName}' => $this->siteName,
                '{poster}' => stripslashes($this->poster),
                '{ContentPolicy}' => $this->policy,
                '{GA}' => $this->ga,
                '{GTM_Head}' => $this->gtm_head,
                '{GTM_Body}' => $this->gtm_body,
                '{Histats}' => $this->histats,
                '{PopupAds}' => $this->popupads,
                '{title}' => stripslashes($data['title']),
                '{message}' => $data['message']
            ];
        }

        $result = strtr($content, $params);
        return $result;
    }

    function render_embed_html()
    {
        session_write_close();

        if ($this->embedOnly && empty($this->referer)) {
            return $this->embed_response_invalid([
                'title' => 'Access denied',
                'message' => 'Please access the embed page using an iframe!',
            ]);
        } elseif ($this->isDenied) {
            return $this->embed_response_invalid([
                'title' => 'Access denied',
                'message' => 'Access denied!',
            ]);
        } elseif ($this->isBlocked) {
            return $this->embed_response_invalid([
                'title' => 'Unavailable For Legal Reasons',
                'message' => 'Sorry this video is unavailable for legal reasons.'
            ]);
        } elseif ($this->isNotFound) {
            return $this->embed_response_invalid([
                'title' => 'Video is Unavailable',
                'message' => 'Sorry this video is unavailable.',
            ]);
        } elseif ($this->isInvalid) {
            return $this->embed_response_invalid([
                'title' => 'Invalid Parameter',
                'message' => 'Invalid Parameter!',
            ]);
        } else {
            $player = get_option('player') ?? 'jwplayer';
            $playerFile = BASE_DIR . "includes/templates/$player.html";
            $playerJSFile = BASE_DIR . "includes/templates/$player.js";
            if (file_exists($playerFile) && file_exists($playerJSFile)) {
                $message = $this->language('{text_loading}');
                $queryString = http_build_query($this->query);
                $localKey = md5($queryString);

                $this->query['origin'] = strtr(parse_url($this->thisSite, PHP_URL_HOST), ['www.' => '']);
                $this->query['token'] = $this->token;
                $this->query['alt'] = $this->altSelected;
                $queryString = http_build_query($this->query);
                $originQuery = encode($queryString);

                $playerColor = get_option('player_color') ?? '673AB7';
                $playerSkin = strtr(get_option('player_skin'), ['_latest' => '']) ?? 'default';
                $rgbColor = hex2RGB($playerColor, true, ',');
                $rgbColor = $rgbColor ? $rgbColor : '103, 58, 183';

                $blockAdBlocker = get_option('block_adblocker') === 'true' ? 'true' : 'false';
                $vastAds = get_vast();
                $preload = get_option('preload') ?? 'auto';
                $resumePlayback = get_option('continue_watching') === 'true' ? 'true' : 'false';
                $productionMode = get_option('production_mode') === 'true' ? 'true' : 'false';
                $displayTitle = get_option('display_title') === 'true' ? 'true' : 'false';
                $smallLogoFile = get_option('small_logo_file');
                $smallLogoURL = get_option('small_logo_link');
                $logoLink = get_option('logo_open_link');
                $logoImage = get_option('logo_file');
                $logoPosition = get_option('logo_position');
                $logoMargin = intval(get_option('logo_margin'));
                $logoHide = get_option('logo_hide') === 'true' ? 'true' : 'false';
                $stretching = get_option('stretching') ?? 'uniform';
                $showDownloadButton = get_option('enable_download_button') === 'true' ? 'true' : 'false';
                $enableDownloadPage = get_option('enable_download_page') === 'true' ? 'true' : 'false';
                if (strpos($_SERVER['REQUEST_URI'], 'embed/') !== FALSE) {
                    $ex = array_pad(explode('embed/', trim($_SERVER['REQUEST_URI'], '/')), 2, '');
                    $downloadLink = $this->mainSite . 'download/' . end($ex);
                } else {
                    $ex = array_pad(explode('embed.php', trim($_SERVER['REQUEST_URI'], '/')), 2, '');
                    $downloadLink = $this->mainSite . 'download/' . end($ex);
                }
                $captionsColor = get_option('subtitle_color') ?? 'ffff00';
                $p2p = filter_var(get_option('p2p'), FILTER_VALIDATE_BOOLEAN);
                $p2pScripts = '';
                $jsDepScripts = '';
                if ($p2p) {
                    if ($player === 'jwplayer_latest') {
                        $p2pScripts = '<script src="' . $this->thisSite . 'assets/vendor/p2p-media-loader/p2p-media-loader-core.min.js"></script>
                        <script src="' . $this->thisSite . 'assets/vendor/p2p-media-loader/p2p-media-loader-hlsjs.min.js"></script>
                        <script src="' . $this->thisSite . 'assets/vendor/hls.js/0.14.17/hls.min.js"></script>';

                        $jsDepScripts = '<script src="https://content.jwplatform.com/libraries/KB5zFt7A.js"></script>
                        <script src="' . $this->thisSite . 'assets/vendor/jwplayer-hlsjs/jwplayer.hlsjs.min.js"></script>';
                    } elseif ($player === 'jwplayer') {
                        $p2pScripts = '<script src="' . $this->thisSite . 'assets/vendor/p2p-media-loader/p2p-media-loader-core.min.js"></script>
                        <script src="' . $this->thisSite . 'assets/vendor/p2p-media-loader/p2p-media-loader-hlsjs.min.js"></script>
                        <script src="' . $this->thisSite . 'assets/vendor/hls.js/0.14.17/hls.min.js"></script>';

                        $jsDepScripts = '<script src="https://ssl.p.jwpcdn.com/player/v/8.9.5/jwplayer.js"></script>
                        <script src="' . $this->thisSite . 'assets/vendor/jwplayer-hlsjs/jwplayer.hlsjs.min.js"></script>';
                    } elseif ($player === 'plyr') {
                        $jsDepScripts = '<script src="' . $this->thisSite . 'assets/vendor/hls.js/0.14.17/hls.min.js"></script>
                        <script src="' . $this->thisSite . 'assets/vendor/mux.js/mux.min.js"></script>
                        <script src="' . $this->thisSite . 'assets/vendor/shaka-player/2.5.23/shaka-player.compiled.js"></script>';

                        $p2pScripts = '<script src="' . $this->thisSite . 'assets/vendor/p2p-media-loader/p2p-media-loader-core.min.js"></script>
                        <script src="' . $this->thisSite . 'assets/vendor/p2p-media-loader/p2p-media-loader-hlsjs.min.js"></script>
                        <script src="' . $this->thisSite . 'assets/vendor/p2p-media-loader/p2p-media-loader-shaka.min.js"></script>';
                    }
                } else {
                    if ($player === 'jwplayer_latest') {
                        $jsDepScripts = '<script src="https://content.jwplatform.com/libraries/KB5zFt7A.js"></script>';
                    } elseif ($player === 'jwplayer') {
                        $jsDepScripts = '<script src="https://ssl.p.jwpcdn.com/player/v/8.9.5/jwplayer.js"></script>';
                    } elseif ($player === 'plyr') {
                        $jsDepScripts = '<script src="' . $this->thisSite . 'assets/vendor/hls.js/latest/hls.min.js"></script>
                        <script src="' . $this->thisSite . 'assets/vendor/mux.js/mux.min.js"></script>
                        <script src="' . $this->thisSite . 'assets/vendor/shaka-player/latest/shaka-player.compiled.js"></script>';
                    }
                }

                $altLinks = '';
                $cAlt = count($this->altLinks);
                if ($cAlt > 1) {
                    if (!empty($this->hashKey)) {
                        $embedURL = $this->mainSite . 'embed/' . $this->hashKey . '/';
                    } elseif (!empty($_SERVER['QUERY_STRING'])) {
                        list($baseQS, $trash) = array_pad(explode('&', $_SERVER['QUERY_STRING']), 2, '');
                        $embedURL = $this->mainSite . 'embed/?' . $baseQS;
                    }

                    $altLinks .= '<div id="server-list" style="display:none"><button type="button" id="btnServer" onclick="$(\'#servers\').toggle()"><img src="' . $this->mainSite . 'assets/img/menu.png" alt="Servers"></button><ul id="servers" style="display:none">';

                    $alt = strpos($embedURL, '?') !== FALSE ? '&alt=-1' : '?alt=-1';
                    $active = $this->altSelected === -1 ? 'active' : '';
                    $altLinks .= '<li><a href="' . $embedURL . $alt . '" class="' . $active . '" onclick="xStorage.removeItem(\'retry_multi~' . $localKey . '\')"><img src="' . $this->mainSite . 'assets/img/logo/' . $this->altLinks[0]['host'] . '.png" width="16" height="16"> ' . $this->altLinks[0]['name'] . '</a></li>';

                    unset($this->altLinks[0]);
                    $this->altLinks = array_values($this->altLinks);
                    foreach ($this->altLinks as $dt) {
                        $alt = strpos($embedURL, '?') !== FALSE ? '&alt=' . $dt['alt'] : '?alt=' . $dt['alt'];
                        $active = $this->altSelected === intval($dt['alt']) ? 'active' : '';
                        $altLinks .= '<li><a href="' . $embedURL . $alt . '" class="' . $active . '" onclick="xStorage.removeItem(\'retry_multi~' . $localKey . '\')"><img src="' . $this->mainSite . 'assets/img/logo/' . $dt['host'] . '.png" width="16" height="16"> ' . $dt['name'] . '</a></li>';
                    }
                    $altLinks .= '</ul></div>';
                }

                $class = new \Core();
                $direct = $class->direct_host();
                if ($this->altSelected > -1) {
                    $hkey = array_search($this->altSelected, array_column($this->altLinks, 'alt'));
                    $host = $this->altLinks[$hkey]['host'];
                } else {
                    $host = $this->newQuery['host'];
                }
                if (in_array($host, $direct)) {
                    $metaReferrer = '<meta name="referrer" content="no-referrer">';
                } else {
                    $metaReferrer = '';
                }

                if ($this->isLoadBalancer === false) {
                    $class = new \VideoSources();
                    $class->setCriteria('host', $this->newQuery['host'], '=');
                    $class->setCriteria('host_id', $this->newQuery['id'], '=', 'AND');
                    $data = $class->getOne(['sid']);
                    if ($data) {
                        if (intval($data['sid']) > 0) {
                            $class = new \LoadBalancers();
                            $class->setCriteria('id', $data['sid']);
                            $data = $class->getOne(['link']);
                            if ($data) {
                                $this->loadBalancerLink = rtrim($data['link'], '/') . '/';
                            } else {
                                $this->loadBalancerLink = get_load_balancer_rand([$host]);
                            }
                        } else {
                            $this->loadBalancerLink = BASE_URL;
                        }
                    } else {
                        $this->loadBalancerLink = get_load_balancer_rand([$host]);
                    }
                }

                $params = [
                    '{productionMode}' => $productionMode,
                    '{small_logo_file}' => $smallLogoFile,
                    '{small_logo_url}' => $smallLogoURL,
                    '{jsDepScripts}' => $jsDepScripts,
                    '{randomHost}' => $this->randomHost,
                    '{preload}' => $preload,
                    '{referrer}' => $metaReferrer,
                    '{lang}' => $this->lang,
                    '{mainSite}' => $this->mainSite,
                    '{thisSite}' => $this->thisSite,
                    '{siteName}' => $this->siteName,
                    '{title}' => stripslashes($this->title),
                    '{message}' => $message,
                    '{poster}' => stripslashes($this->poster),
                    '{themeColor}' => '#' . $this->themeColor,
                    '{player}' => $player,
                    '{playerColor}' => "#$playerColor",
                    '{rgbColor}' => "$rgbColor",
                    '{resumePlayback}' => $resumePlayback,
                    '{displayTitle}' => $displayTitle,
                    '{logoLink}' => $logoLink,
                    '{logoImage}' => $logoImage,
                    '{logoPosition}' => $logoPosition,
                    '{logoMargin}' => $logoMargin,
                    '{logoHide}' => $logoHide,
                    '{stretching}' => $stretching,
                    '{playerSkin}' => $playerSkin,
                    '{showDownloadButton}' => $showDownloadButton,
                    '{enableDownloadPage}' => $enableDownloadPage,
                    '{GA}' => $this->ga,
                    '{GTM_Head}' => $this->gtm_head,
                    '{GTM_Body}' => $this->gtm_body,
                    '{Histats}' => $this->histats,
                    '{PopupAds}' => $this->popupads,
                    '{ContentPolicy}' => $this->policy,
                    '{downloadLink}' => $downloadLink,
                    '{captionsColor}' => "#$captionsColor",
                    '{p2pScripts}' => $p2pScripts,
                    '{altLinks}' => strtr($altLinks, ['|New' => '', '|Additional Host' => ''])
                ];

                $content = '';
                open_resources_handler();
                $fp = @fopen($playerFile, 'r');
                if ($fp) {
                    stream_set_blocking($fp, false);
                    $content = stream_get_contents($fp);
                    fclose($fp);
                }

                $jsCode = '';
                open_resources_handler();
                $fp = @fopen($playerJSFile, 'r');
                if ($fp) {
                    stream_set_blocking($fp, false);
                    $jsCode = stream_get_contents($fp);
                    fclose($fp);
                }

                if (filter_var($productionMode, FILTER_VALIDATE_BOOLEAN)) {
                    $min = new \Minify();
                    $content = $min->minify_html($content);
                    $jsCode = $min->minify_js($jsCode);
                }

                $torrentList = get_option('torrent_tracker');
                if (!empty($torrentList)) {
                    $torrentList = trim(strtr($torrentList, ["\r\n" => "\n"]));
                    $torrentList = array_values(array_unique(array_filter(explode("\n", $torrentList))));
                } else {
                    $torrentList = ['wss://tracker.openwebtorrent.com', 'wss://spacetradersapi-chatbox.herokuapp.com:443/announce', 'wss://tracker.btorrent.xyz/'];
                }
                $visitAdsOnplay = get_option('visitads_onplay') === 'true' ? 'true' : 'false';
                $directAdsLink = get_option('direct_ads_link') ?? '#';
                $directAdsLink = !filter_var(get_option('disable_direct_ads'), FILTER_VALIDATE_BOOLEAN) ? $directAdsLink : '#';
                $autoplay = get_option('autoplay') === 'true' ? 'true' : 'false';
                $repeat = get_option('repeat') === 'true' ? 'true' : 'false';
                $mute = get_option('mute') === 'true' ? 'true' : 'false';
                $displayRateControls = get_option('playback_rate') === 'true' ? 'true' : 'false';
                $enableP2P = $p2p ? 'true' : 'false';
                $enableShare = get_option('enable_share_button') === 'true' ? 'true' : 'false';
                $defaultRes = get_option('default_resolution');
                $defaultRes = !empty($defaultRes) ? $defaultRes : 360;

                $jsParams = [
                    '{default_resolution}' => $defaultRes,
                    '{hosts}' => json_encode(array_column($this->altLinks, 'host'), true),
                    '{block_adblocker}' => $blockAdBlocker,
                    '{preload}' => $preload,
                    '{preload}' => $preload,
                    '{player}' => $player,
                    '{lbSite}' => $this->loadBalancerLink,
                    '{mainSite}' => $this->mainSite,
                    '{thisSite}' => $this->thisSite,
                    '{siteName}' => $this->siteName,
                    '{title}' => stripslashes($this->title),
                    '{poster}' => stripslashes($this->poster),
                    '{autoplay}' => $autoplay,
                    '{repeat}' => $repeat,
                    '{mute}' => $mute,
                    '{displayTitle}' => $displayTitle,
                    '{displayRateControls}' => $displayRateControls,
                    '{stretching}' => $stretching,
                    '{embedLink}' => 'http:' . $this->mainSite . ltrim($_SERVER['REQUEST_URI'], '/'),
                    '{captionsColor}' => "#$captionsColor",
                    '{localKey}' => $localKey,
                    '{cacheKey}' => $originQuery,
                    '{torrentList}' => json_encode($torrentList, true),
                    '{enableDownloadPage}' => $enableDownloadPage,
                    '{visitAdsOnplay}' => $visitAdsOnplay,
                    '{directAdsLink}' => $directAdsLink,
                    '{enableP2P}' => $enableP2P,
                    '{enableShare}' => $enableShare,
                    '{logoLink}' => $logoLink,
                    '{logoImage}' => $logoImage,
                    '{logoPosition}' => $logoPosition,
                    '{logoMargin}' => $logoMargin,
                    '{logoHide}' => $logoHide,
                    '{playerColor}' => "#$playerColor",
                    '{rgbColor}' => "$rgbColor",
                    '{playerSkin}' => $playerSkin,
                    '{showDownloadButton}' => $showDownloadButton,
                    '{downloadLink}' => $downloadLink,
                    '{vastAds}' => json_encode($vastAds, true),
                    '{originQry}' => $originQuery,
                    '{visit_counter_runtime}' => intval(get_option('visit_counter_runtime'))
                ];

                $jsCode = strtr($jsCode, $jsParams);
                if (filter_var($productionMode, FILTER_VALIDATE_BOOLEAN)) {
                    $obf = new \Obfuscator();
                    $jsCode = $obf->jsObfustator($jsCode);
                }
                $params['{jsCode}'] = $jsCode;

                $content = strtr($content, $this->language());
                $content = strtr($content, $params);
                $content = strtr($content, $this->language());

                return $content;
            } else {
                return $this->embed_response_invalid([
                    'title' => 'Video Player is Unavailable',
                    'message' => 'Video player is Unavailable. Please contact admin.',
                ]);
            }
        }
        return $this->embed_response_invalid([
            'title' => 'Video is Unavailable',
            'message' => 'Sorry this video is unavailable.',
        ]);
    }

    function download_response_invalid(array $data = [])
    {
        session_write_close();

        $content = '';
        open_resources_handler();
        $fp = @fopen(BASE_DIR . 'includes/templates/invalid-download.html', 'r');
        if ($fp) {
            stream_set_blocking($fp, false);
            $content = stream_get_contents($fp);
            fclose($fp);

            $min = new \Minify();
            $content = $min->minify_html($content);
        }

        $disableAds = filter_var(get_option('disable_banner_ads'), FILTER_VALIDATE_BOOLEAN);

        $params = [];
        if (!empty($data)) {
            $params = [
                '{themeColor}' => '#' . $this->themeColor,
                '{lang}' => $this->lang,
                '{thisSite}' => $this->thisSite,
                '{siteName}' => $this->siteName,
                '{poster}' => stripslashes($this->poster),
                '{ContentPolicy}' => $this->policy,
                '{GA}' => $this->ga,
                '{GTM_Head}' => $this->gtm_head,
                '{GTM_Body}' => $this->gtm_body,
                '{Histats}' => $this->histats,
                '{PopupAds}' => $this->popupads,
                '{title}' => stripslashes($data['title']),
                '{message}' => $data['message'],
                '{topBanner}' => !$disableAds ? htmlspecialchars_decode(get_option('dl_banner_top')) : '',
                '{bottomBanner}' => !$disableAds ? htmlspecialchars_decode(get_option('dl_banner_bottom')) : ''
            ];
        }

        $result = strtr($content, $params);
        return $result;
    }

    function render_download_html()
    {
        session_write_close();

        if ($this->isDenied) {
            return $this->download_response_invalid([
                'title' => 'Access denied',
                'message' => 'Access denied!',
            ]);
        } elseif ($this->isBlocked) {
            return $this->download_response_invalid([
                'title' => 'Unavailable For Legal Reasons',
                'message' => 'Sorry this video is unavailable for legal reasons.'
            ]);
        } elseif ($this->isNotFound) {
            return $this->download_response_invalid([
                'title' => 'Video is Unavailable',
                'message' => 'Sorry this video is unavailable.',
            ]);
        } elseif ($this->isInvalid) {
            return $this->download_response_invalid([
                'title' => 'Invalid Parameter',
                'message' => 'Invalid Parameter!',
            ]);
        } else {
            $downloadFile = BASE_DIR . 'includes/templates/download.html';
            $downloadJSFile = BASE_DIR . 'includes/templates/download.js';
            if (file_exists($downloadFile) && file_exists($downloadJSFile)) {
                $queryString = http_build_query($this->query);
                $localKey = md5($queryString);

                $this->query['alt'] = $this->altSelected;
                $this->query['origin'] = strtr(parse_url($this->thisSite, PHP_URL_HOST), ['www.' => '']);
                $this->query['token'] = $this->token;
                $this->query['download'] = true;
                $this->query['shortlink'] = $this->disableShortenerLink === false;

                $queryString = http_build_query($this->query);
                $originQuery = encode($queryString);

                if ($this->isLoadBalancer === false) {
                    $hkey = array_search($this->altSelected, array_column($this->altLinks, 'alt'));
                    $host = $this->altLinks[$hkey]['host'];
                    $class = new \VideoSources();
                    $class->setCriteria('host', $this->newQuery['host'], '=');
                    $class->setCriteria('host_id', $this->newQuery['id'], '=', 'AND');
                    $data = $class->getOne(['sid']);
                    if ($data) {
                        if (intval($data['sid']) > 0) {
                            $class = new \LoadBalancers();
                            $class->setCriteria('id', $data['sid']);
                            $data = $class->getOne(['link']);
                            if ($data) {
                                $this->loadBalancerLink = rtrim($data['link'], '/') . '/';
                            } else {
                                $this->loadBalancerLink = get_load_balancer_rand([$host]);
                            }
                        } else {
                            $this->loadBalancerLink = BASE_URL;
                        }
                    } else {
                        $this->loadBalancerLink = get_load_balancer_rand([$host]);
                    }
                }

                $altLinks = '';
                $cAlt = count($this->altLinks);
                if ($cAlt > 1) {
                    if (!empty($this->hashKey)) {
                        $downloadURL = $this->mainSite . 'download/' . $this->hashKey . '/';
                    } elseif (!empty($_SERVER['QUERY_STRING'])) {
                        list($baseQS, $trash) = array_pad(explode('&', $_SERVER['QUERY_STRING']), 2, '');
                        $downloadURL = $this->mainSite . 'download/?' . $baseQS;
                    }

                    $altLinks .= '<div id="server-list" style="display:none"><button type="button" id="btnServer" onclick="$(\'#servers\').toggle()"><img src="' . $this->mainSite . 'assets/img/menu.png" alt="Servers"></button><ul id="servers">';

                    $alt = strpos($downloadURL, '?') !== FALSE ? '&alt=-1' : '?alt=-1';
                    $active = $this->altSelected === -1 ? 'active' : '';
                    $altLinks .= '<li><a href="' . $downloadURL . $alt . '" class="' . $active . '"><img src="' . $this->mainSite . 'assets/img/logo/' . $this->altLinks[0]['host'] . '.png" width="16" height="16"> ' . $this->altLinks[0]['name'] . '</a></li>';

                    unset($this->altLinks[0]);
                    $this->altLinks = array_values($this->altLinks);
                    foreach ($this->altLinks as $dt) {
                        $alt = strpos($downloadURL, '?') !== FALSE ? '&alt=' . $dt['alt'] : '?alt=' . $dt['alt'];
                        $active = $this->altSelected === intval($dt['alt']) ? 'active' : '';
                        $altLinks .= '<li><a href="' . $downloadURL . $alt . '" class="' . $active . '"><img src="' . $this->mainSite . 'assets/img/logo/' . $dt['host'] . '.png" width="16" height="16"> ' . $dt['name'] . '</a></li>';
                    }
                    $altLinks .= '</ul></div>';
                }

                $productionMode = get_option('production_mode') === 'true' ? 'true' : 'false';
                $embedLink = $this->mainSite . strtr(ltrim($_SERVER['REQUEST_URI'], '/'), ['download/' => 'embed/', 'download.php?' => 'embed.php?']);

                $directAdsLink = get_option('direct_ads_link');
                $disableAds = filter_var(get_option('disable_banner_ads'), FILTER_VALIDATE_BOOLEAN);

                $params = [
                    '{productionMode}' => $productionMode,
                    '{altLinks}' => strtr($altLinks, ['|New' => '', '|Additional Host' => '']),
                    '{lang}' => $this->lang,
                    '{ContentPolicy}' => $this->policy,
                    '{thisSite}' => $this->thisSite,
                    '{siteName}' => $this->siteName,
                    '{title}' => stripslashes($this->title),
                    '{poster}' => stripslashes($this->poster),
                    '{themeColor}' => '#' . $this->themeColor,
                    '{GA}' => $this->ga,
                    '{GTM_Head}' => $this->gtm_head,
                    '{GTM_Body}' => $this->gtm_body,
                    '{Histats}' => $this->histats,
                    '{PopupAds}' => $this->popupads,
                    '{embedLink}' => $embedLink,
                    '{topBanner}' => !$disableAds ? htmlspecialchars_decode(get_option('dl_banner_top')) : '',
                    '{bottomBanner}' => !$disableAds ? htmlspecialchars_decode(get_option('dl_banner_bottom')) : '',
                    '{share}' => $this->share,
                    '{directAdsLink}' => $directAdsLink
                ];

                $content = '';
                open_resources_handler();
                $fp = @fopen($downloadFile, 'r');
                if ($fp) {
                    stream_set_blocking($fp, false);
                    $content = stream_get_contents($fp);
                    fclose($fp);
                }

                $jsCode = '';
                open_resources_handler();
                $fp = @fopen($downloadJSFile, 'r');
                if ($fp) {
                    stream_set_blocking($fp, false);
                    $jsCode = stream_get_contents($fp);
                    fclose($fp);
                }

                if (filter_var($productionMode, FILTER_VALIDATE_BOOLEAN)) {
                    $min = new \Minify();
                    $content = $min->minify_html($content);
                    $jsCode = $min->minify_js($jsCode);
                }

                $jsParams = [
                    '{localKey}' => $localKey,
                    '{originQry}' => $originQuery,
                    '{lbSite}' => $this->loadBalancerLink,
                    '{thisSite}' => $this->thisSite,
                    '{siteName}' => $this->siteName,
                    '{title}' => stripslashes($this->title),
                    '{directAdsLink}' => $directAdsLink
                ];

                $jsCode = strtr($jsCode, $jsParams);
                if (filter_var($productionMode, FILTER_VALIDATE_BOOLEAN)) {
                    $obf = new \Obfuscator();
                    $jsCode = $obf->jsObfustator($jsCode);
                }

                $params['{jsCode}'] = $jsCode;

                $content = strtr($content, $params);

                return $content;
            } else {
                return $this->embed_response_invalid([
                    'title' => 'Download page is Unavailable',
                    'message' => 'Download page is Unavailable. Please contact admin.',
                ]);
            }
        }
        return $this->download_response_invalid([
            'title' => 'Video is Unavailable',
            'message' => 'Sorry this video is unavailable.',
        ]);
    }

    function render_smartTV_html()
    {
        session_write_close();

        if ($this->embedOnly && empty($this->referer)) {
            return $this->embed_response_invalid([
                'title' => 'Access denied',
                'message' => 'Please access the embed page using an iframe!',
            ]);
        } elseif ($this->isDenied) {
            return $this->embed_response_invalid([
                'title' => 'Access denied',
                'message' => 'Access denied!',
            ]);
        } elseif ($this->isBlocked) {
            return $this->embed_response_invalid([
                'title' => 'Unavailable For Legal Reasons',
                'message' => 'Sorry this video is unavailable for legal reasons.'
            ]);
        } elseif ($this->isNotFound) {
            return $this->embed_response_invalid([
                'title' => 'Video is Unavailable',
                'message' => 'Sorry this video is unavailable.',
            ]);
        } elseif ($this->isInvalid) {
            return $this->embed_response_invalid([
                'title' => 'Invalid Parameter',
                'message' => 'Invalid Parameter!',
            ]);
        } else {
            $playerFile = BASE_DIR . 'includes/templates/smartTV.html';
            $playerJSFile = BASE_DIR . 'includes/templates/smartTV.js';
            if (file_exists($playerFile) && file_exists($playerJSFile)) {
                $message = $this->language('{text_loading}');
                $queryString = http_build_query($this->query);
                $localKey = md5($queryString);

                $this->query['origin'] = strtr(parse_url($this->thisSite, PHP_URL_HOST), ['www.' => '']);
                $this->query['token'] = $this->token;
                $this->query['alt'] = $this->altSelected;
                $queryString = http_build_query($this->query);
                $originQuery = encode($queryString);

                $playerColor = get_option('player_color') ?? '673AB7';
                $playerSkin = strtr(get_option('player_skin'), ['_latest' => '']) ?? 'default';
                $rgbColor = hex2RGB($playerColor, true, ',');
                $rgbColor = $rgbColor ? $rgbColor : '103, 58, 183';

                $preload = get_option('preload') ?? 'auto';
                $stretching = get_option('stretching') ?? 'uniform';
                $jsDepScripts = '<script src="' . $this->thisSite . 'assets/vendor/hls.js/0.14.17/hls.min.js"></script>
                <script src="' . $this->thisSite . 'assets/vendor/mux.js/mux.min.js"></script>
                <script src="' . $this->thisSite . 'assets/vendor/shaka-player/2.5.23/shaka-player.compiled.js"></script>';

                $altLinks = '';
                $cAlt = count($this->altLinks);
                if ($cAlt > 1) {
                    if (!empty($this->hashKey)) {
                        $embedURL = $this->mainSite . 'embed/' . $this->hashKey . '/';
                    } elseif (!empty($_SERVER['QUERY_STRING'])) {
                        list($baseQS, $trash) = array_pad(explode('&', $_SERVER['QUERY_STRING']), 2, '');
                        $embedURL = $this->mainSite . 'embed/?' . $baseQS;
                    }

                    $altLinks .= '<div id="server-list" style="display:none"><button type="button" id="btnServer" onclick="$(\'#servers\').toggle()"><img src="' . $this->mainSite . 'assets/img/menu.png" alt="Servers"></button><ul id="servers" style="display:none">';

                    $alt = strpos($embedURL, '?') !== FALSE ? '&alt=-1' : '?alt=-1';
                    $active = $this->altSelected === -1 ? 'active' : '';
                    $altLinks .= '<li><a href="' . $embedURL . $alt . '" class="' . $active . '" onclick="xStorage.removeItem(\'retry_multi~' . $localKey . '\')"><img src="' . $this->mainSite . 'assets/img/logo/' . $this->altLinks[0]['host'] . '.png" width="16" height="16"> ' . $this->altLinks[0]['name'] . '</a></li>';

                    unset($this->altLinks[0]);
                    $this->altLinks = array_values($this->altLinks);
                    foreach ($this->altLinks as $dt) {
                        $alt = strpos($embedURL, '?') !== FALSE ? '&alt=' . $dt['alt'] : '?alt=' . $dt['alt'];
                        $active = $this->altSelected === intval($dt['alt']) ? 'active' : '';
                        $altLinks .= '<li><a href="' . $embedURL . $alt . '" class="' . $active . '" onclick="xStorage.removeItem(\'retry_multi~' . $localKey . '\')"><img src="' . $this->mainSite . 'assets/img/logo/' . $dt['host'] . '.png" width="16" height="16"> ' . $dt['name'] . '</a></li>';
                    }
                    $altLinks .= '</ul></div>';
                }

                $class = new \Core();
                $direct = $class->direct_host();
                if ($this->altSelected > -1) {
                    $hkey = array_search($this->altSelected, array_column($this->altLinks, 'alt'));
                    $host = $this->altLinks[$hkey]['host'];
                } else {
                    $host = $this->newQuery['host'];
                }
                if (in_array($host, $direct)) {
                    $metaReferrer = '<meta name="referrer" content="no-referrer">';
                } else {
                    $metaReferrer = '';
                }

                if ($this->isLoadBalancer === false) {
                    $class = new \VideoSources();
                    $class->setCriteria('host', $this->newQuery['host'], '=');
                    $class->setCriteria('host_id', $this->newQuery['id'], '=', 'AND');
                    $data = $class->getOne(['sid']);
                    if ($data) {
                        if (intval($data['sid']) > 0) {
                            $class = new \LoadBalancers();
                            $class->setCriteria('id', $data['sid']);
                            $data = $class->getOne(['link']);
                            if ($data) {
                                $this->loadBalancerLink = rtrim($data['link'], '/') . '/';
                            } else {
                                $this->loadBalancerLink = get_load_balancer_rand([$host]);
                            }
                        } else {
                            $this->loadBalancerLink = BASE_URL;
                        }
                    } else {
                        $this->loadBalancerLink = get_load_balancer_rand([$host]);
                    }
                }

                $params = [
                    '{jsDepScripts}' => $jsDepScripts,
                    '{randomHost}' => $this->randomHost,
                    '{preload}' => $preload,
                    '{referrer}' => $metaReferrer,
                    '{lang}' => $this->lang,
                    '{mainSite}' => $this->mainSite,
                    '{thisSite}' => $this->thisSite,
                    '{siteName}' => $this->siteName,
                    '{title}' => stripslashes($this->title),
                    '{message}' => $message,
                    '{poster}' => stripslashes($this->poster),
                    '{themeColor}' => '#' . $this->themeColor,
                    '{playerColor}' => "#$playerColor",
                    '{rgbColor}' => "$rgbColor",
                    '{stretching}' => $stretching,
                    '{playerSkin}' => $playerSkin,
                    '{GA}' => $this->ga,
                    '{GTM_Head}' => $this->gtm_head,
                    '{GTM_Body}' => $this->gtm_body,
                    '{Histats}' => $this->histats,
                    '{PopupAds}' => $this->popupads,
                    '{ContentPolicy}' => $this->policy,
                    '{altLinks}' => strtr($altLinks, ['|New' => '', '|Additional Host' => ''])
                ];

                $content = '';
                open_resources_handler();
                $fp = @fopen($playerFile, 'r');
                if ($fp) {
                    stream_set_blocking($fp, false);
                    $content = stream_get_contents($fp);
                    fclose($fp);
                }

                $jsCode = '';
                open_resources_handler();
                $fp = @fopen($playerJSFile, 'r');
                if ($fp) {
                    stream_set_blocking($fp, false);
                    $jsCode = stream_get_contents($fp);
                    fclose($fp);
                }

                $autoplay = get_option('autoplay') === 'true' ? 'true' : 'false';
                $repeat = get_option('repeat') === 'true' ? 'true' : 'false';
                $mute = get_option('mute') === 'true' ? 'true' : 'false';

                $jsParams = [
                    '{hosts}' => json_encode(array_column($this->altLinks, 'host'), true),
                    '{preload}' => $preload,
                    '{preload}' => $preload,
                    '{lbSite}' => $this->loadBalancerLink,
                    '{mainSite}' => $this->mainSite,
                    '{thisSite}' => $this->thisSite,
                    '{siteName}' => $this->siteName,
                    '{title}' => stripslashes($this->title),
                    '{poster}' => stripslashes($this->poster),
                    '{autoplay}' => $autoplay,
                    '{repeat}' => $repeat,
                    '{mute}' => $mute,
                    '{stretching}' => $stretching,
                    '{embedLink}' => 'http:' . $this->mainSite . ltrim($_SERVER['REQUEST_URI'], '/'),
                    '{localKey}' => $localKey,
                    '{cacheKey}' => $originQuery,
                    '{playerColor}' => "#$playerColor",
                    '{rgbColor}' => "$rgbColor",
                    '{playerSkin}' => $playerSkin,
                    '{originQry}' => $originQuery,
                    '{visit_counter_runtime}' => intval(get_option('visit_counter_runtime'))
                ];

                $jsCode = strtr($jsCode, $jsParams);
                $params['{jsCode}'] = $jsCode;

                $content = strtr($content, $this->language());
                $content = strtr($content, $params);
                $content = strtr($content, $this->language());

                return $content;
            } else {
                return $this->embed_response_invalid([
                    'title' => 'Video Player is Unavailable',
                    'message' => 'Video player is Unavailable. Please contact admin.',
                ]);
            }
        }
        return $this->embed_response_invalid([
            'title' => 'Video is Unavailable',
            'message' => 'Sorry this video is unavailable.',
        ]);
    }

    function __destruct()
    {
        session_write_close();
        $class = new \InstanceCache();
        $checkChrome = $class->get('check-chrome-version');
        $checkServer = $class->get('check-server');
        if (!$checkChrome || !$checkServer) {
            execInBackground(strtr(BASE_DIR . 'includes/server-info.php', ['\/' => DIRECTORY_SEPARATOR, '\\' => DIRECTORY_SEPARATOR]));
        }
    }
}
