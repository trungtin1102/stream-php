<?php
class ServerInfo
{
    function getMemoryUsage(bool $getPercentage = true)
    {
        session_write_close();
        try {
            $memoryTotal = null;
            $memoryFree = null;

            if (PHP_OS_FAMILY === 'Windows') {
                if (function_exists('exec')) {
                    // Get total physical memory (this is in bytes)
                    $cmd = "wmic ComputerSystem get TotalPhysicalMemory";
                    exec($cmd, $outputTotalPhysicalMemory);

                    // Get free physical memory (this is in kibibytes!)
                    $cmd = "wmic OS get FreePhysicalMemory";
                    exec($cmd, $outputFreePhysicalMemory);

                    if ($outputTotalPhysicalMemory && $outputFreePhysicalMemory) {
                        // Find total value
                        foreach ($outputTotalPhysicalMemory as $line) {
                            if ($line && preg_match("/^[0-9]+\$/", $line)) {
                                $memoryTotal = $line;
                                break;
                            }
                        }

                        // Find free value
                        foreach ($outputFreePhysicalMemory as $line) {
                            if ($line && preg_match("/^[0-9]+\$/", $line)) {
                                $memoryFree = $line;
                                $memoryFree *= 1024;  // convert from kibibytes to bytes
                                break;
                            }
                        }
                    }
                }
            } else {
                $memInfo = @is_readable('/proc/meminfo');
                if ($memInfo) {
                    open_resources_handler();
                    $fp = @fopen('/proc/meminfo', 'rb');
                    if ($fp) {
                        stream_set_blocking($fp, false);
                        $stats = stream_get_contents($fp);
                        fclose($fp);

                        if ($stats) {
                            $stats = strtr($stats, array("\r\n" => "\n", "\n\r" => "\n", "\r" => "\n"));
                            $stats = explode("\n", $stats);
                            foreach ($stats as $i => $line) {
                                if ($i < 2) {
                                    list($key, $val) = array_pad(explode(':', trim($line)), 2, '');
                                    list($val, $kb) = array_pad(explode(' ', trim($val)), 2, '');
                                    if ($key === 'MemTotal') {
                                        $memoryTotal = intval($val) * 1024;
                                    } elseif($key === 'MemFree') {
                                        $memoryFree = intval($val) * 1024;
                                    }
                                } else {
                                    break;
                                }
                            }
                        }
                    }
                } elseif (function_exists('exec')) {
                    exec('cat /proc/meminfo', $stats);
                    if (!empty($stats)) {
                        $stats = implode("\n", $stats);
                        $stats = strtr($stats, array("\r\n" => "\n", "\n\r" => "\n", "\r" => "\n"));
                        $stats = explode("\n", $stats);
                        foreach ($stats as $i => $line) {
                            if ($i < 2) {
                                list($key, $val) = array_pad(explode(':', trim($line)), 2, '');
                                list($val, $kb) = array_pad(explode(' ', trim($val)), 2, '');
                                if ($key === 'MemTotal') {
                                    $memoryTotal = intval($val) * 1024;
                                } elseif($key === 'MemFree') {
                                    $memoryFree = intval($val) * 1024;
                                }
                            } else {
                                break;
                            }
                        }
                    }
                }
            }

            if (!is_null($memoryTotal) && !is_null($memoryFree)) {
                if ($getPercentage) {
                    return intval(100 - ($memoryFree * 100 / $memoryTotal));
                } else {
                    return array(
                        "total" => $memoryTotal,
                        "free" => $memoryFree,
                    );
                }
            }
        } catch (\Exception $e) {
            error_log('getMemoryUsage => ' . $e->getMessage());
        }
        return FALSE;
    }

    private function cpuUsageLinux()
    {
        session_write_close();
        try {
            $stat = @is_readable('/proc/stat');
            if ($stat) {
                open_resources_handler();
                $fp = @fopen('/proc/stat', 'rb');
                if ($fp) {
                    stream_set_blocking($fp, false);
                    $stats = stream_get_contents($fp);
                    fclose($fp);

                    if ($stats) {
                        $stats = preg_replace("/[[:blank:]]+/", " ", $stats);
                        $stats = str_replace(array("\r\n", "\n\r", "\r"), "\n", $stats);
                        $stats = explode("\n", $stats);
                        foreach ($stats as $statLine) {
                            $statLineData = explode(" ", trim($statLine));
                            if ((count($statLineData) >= 5) && ($statLineData[0] == 'cpu')) {
                                return array(
                                    $statLineData[1],
                                    $statLineData[2],
                                    $statLineData[3],
                                    $statLineData[4],
                                );
                            }
                        }
                    }
                }
            } elseif (function_exists('exec')) {
                exec('cat /proc/stat', $stats);
                if (!empty($stats)) {
                    $stats = implode("\n", $stats);
                    $stats = preg_replace("/[[:blank:]]+/", " ", $stats);
                    $stats = explode("\n", $stats);
                    foreach ($stats as $statLine) {
                        $statLineData = explode(" ", trim($statLine));
                        if ((count($statLineData) >= 5) && ($statLineData[0] == 'cpu')) {
                            return array(
                                $statLineData[1],
                                $statLineData[2],
                                $statLineData[3],
                                $statLineData[4],
                            );
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            error_log('cpuUsageLinux => ' . $e->getMessage());
        }
        return FALSE;
    }

    function getCpuUsage()
    {
        session_write_close();
        if (PHP_OS_FAMILY === 'Windows') {
            if (class_exists('COM')) {
                $wmi = new \COM("Winmgmts://");
                $server = $wmi->execquery("SELECT LoadPercentage FROM Win32_Processor");

                $cpu_num = 0;
                $load_total = 0;

                foreach ($server as $cpu) {
                    $cpu_num++;
                    $load_total += $cpu->loadpercentage;
                }

                return intval($load_total / $cpu_num);
            }
        } else {
            $statData1 = $this->cpuUsageLinux();
            sleep(1);
            $statData2 = $this->cpuUsageLinux();

            if ($statData1 && $statData2) {
                // Get difference
                $statData2[0] -= $statData1[0];
                $statData2[1] -= $statData1[1];
                $statData2[2] -= $statData1[2];
                $statData2[3] -= $statData1[3];

                // Sum up the 4 values for User, Nice, System and Idle and calculate
                // the percentage of idle time (which is part of the 4 values!)
                $cpuTime = $statData2[0] + $statData2[1] + $statData2[2] + $statData2[3];

                // Invert percentage to get CPU time, not idle time
                $load = intval(100 - ($statData2[3] * 100 / $cpuTime));

                return $load;
            }
        }
        return FALSE;
    }

    function getCpuThreads()
    {
        session_write_close();
        if (PHP_OS_FAMILY === 'Windows') {
            return (int) (getenv("NUMBER_OF_PROCESSORS") + 0);
        } else {
            open_resources_handler();
            $fp = @fopen('/proc/cpuinfo', 'rb');
            if ($fp) {
                stream_set_blocking($fp, false);
                $stats = stream_get_contents($fp);
                fclose($fp);
                return (int) substr_count($stats, 'processor');
            }
        }
        return false;
    }

    function getNumVideos($uid = null, $status = null)
    {
        session_write_close();

        $class = new \Videos();
        if (!is_null($uid)) {
            $class->setCriteria('uid', $uid, '=');
            if (!is_null($status)) $class->setCriteria('status', $status, '=', 'AND');
        } elseif (!is_null($status)) {
            $class->setCriteria('status', $status);
        }
        return (int) $class->getNumRows();
    }

    function getNumServers()
    {
        session_write_close();

        $class = new \LoadBalancers();
        return (int) $class->getTotalRows();
    }

    function getNumGDriveAccounts()
    {
        session_write_close();

        $class = new \GDriveAuth();
        return (int) $class->getTotalRows();
    }
}
