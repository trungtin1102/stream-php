<?php
if (!defined('BASE_DIR')) exit();

session_write_close();

$popup_link = get_option('popup_ads_link');
if (!empty($popup_link) && !filter_var(get_option('disable_popup_ads'), FILTER_VALIDATE_BOOLEAN)) {
    return '<script src="' . $popup_link . '" data-cfasync="false" async></script>';
} else {
    return get_option('popup_ads_code');
}
