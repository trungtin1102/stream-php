<?php
if (!defined('BASE_DIR')) exit();

session_write_close();

return '<div class="a2a_kit a2a_kit_size_32 a2a_floating_style shadow">
    <a class="a2a_button_facebook"></a>
    <a class="a2a_button_twitter"></a>
    <a class="a2a_button_whatsapp"></a>
    <a class="a2a_button_telegram"></a>
    <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
</div>
<script src="https://static.addtoany.com/menu/page.js" defer></script>';
