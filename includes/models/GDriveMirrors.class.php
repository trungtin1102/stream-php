<?php
class GDriveMirrors extends Model
{
    protected $table = 'tb_gdrive_mirrors';
    protected $fields = ['id', 'gdrive_id', 'mirror_id', 'mirror_email', 'added'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
