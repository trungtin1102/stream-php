<?php
class Subtitles extends Model
{
    protected $table = 'tb_subtitles';
    protected $fields = ['id', 'language', 'link', 'vid', 'added', 'uid', 'order'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
