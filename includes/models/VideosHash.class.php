<?php
class VideosHash extends Model
{
    protected $table = 'tb_videos_hash';
    protected $fields = ['id', 'host', 'host_id', 'gdrive_email', 'hash_host', 'hash_id', 'data'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
