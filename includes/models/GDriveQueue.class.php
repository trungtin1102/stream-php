<?php
class GDriveQueue extends Model
{
    protected $table = 'tb_gdrive_queue';
    protected $fields = ['id', 'gdrive_id'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
