<?php
class VideoSources extends Model
{
    protected $table = 'tb_videos_sources';
    protected $fields = ['id', 'host', 'host_id', 'data', 'dl', 'sid', 'created', 'espired'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
