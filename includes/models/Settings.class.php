<?php
class Settings extends Model
{
    protected $table = 'tb_settings';
    protected $fields = ['id', 'key', 'value', 'updated'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function reset()
    {
        session_write_close();
        $this->setCriteria('key', '', '<>');
        return $this->delete();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
