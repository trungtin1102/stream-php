<?php
class SubtitleManager extends Model
{
    protected $table = 'tb_subtitle_manager';
    protected $fields = ['id', 'file_name', 'file_size', 'file_type', 'language', 'added', 'uid', 'host'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
