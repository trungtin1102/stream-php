<?php
class VideoShort extends Model
{
    protected $table = 'tb_videos_short';
    protected $fields = ['id', 'key', 'vid'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
