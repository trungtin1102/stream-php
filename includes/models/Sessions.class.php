<?php
class Sessions extends Model
{
    protected $table = 'tb_sessions';
    protected $fields = ['id', 'ip', 'useragent', 'created', 'username', 'expired', 'token', 'stat'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
