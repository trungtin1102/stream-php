<?php
class Users extends Model
{
    protected $table = 'tb_users';
    protected $fields = ['id', 'user', 'email', 'password', 'name', 'status', 'added', 'updated', 'role'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
