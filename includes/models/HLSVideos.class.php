<?php
class HLSVideos extends Model
{
    protected $table = 'tb_hls_videos';
    protected $fields = ['id', 'title', 'folder', 'embed_code', 'views', 'embed_link', 'updated', 'poster', 'status'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
