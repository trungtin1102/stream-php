<?php
class VideosAlternatives extends Model
{
    protected $table = 'tb_videos_alternatives';
    protected $fields = ['id', 'vid', 'host', 'host_id', 'order'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
