<?php
class Videos extends Model
{
    protected $table = 'tb_videos';
    protected $fields = ['id', 'title', 'host', 'host_id', 'ahost', 'ahost_id', 'uid', 'added', 'updated', 'poster', 'status'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
