<?php
class LoadBalancers extends Model
{
    protected $table = 'tb_loadbalancers';
    protected $fields = ['id', 'name', 'link', 'status', 'public', 'added', 'updated', 'disallow_hosts'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
