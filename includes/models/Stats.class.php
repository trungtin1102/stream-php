<?php
class Stats extends Model
{
    protected $table = 'tb_stats';
    protected $fields = ['id', 'vid', 'ip', 'ua', 'created'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
