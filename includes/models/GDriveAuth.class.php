<?php
class GDriveAuth extends Model
{
    protected $table = 'tb_gdrive_auth';
    protected $fields = ['id', 'email', 'api_key', 'client_id', 'client_secret', 'refresh_token', 'created', 'modified', 'uid', 'status'];
    protected $primaryKey = 'id';

    function __construct()
    {
        session_write_close();
        parent::__construct();
    }

    function __destruct()
    {
        session_write_close();
        parent::__destruct();
    }
}
