<?php
if (!defined('BASE_DIR')) exit();

session_write_close();

$ga_id = get_option('google_analytics_id');
if (!empty($ga_id)) {
    return "<script defer src=\"https://www.googletagmanager.com/gtag/js?id=$ga_id\"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', '$ga_id');
    </script>";
} else {
    return '';
}
