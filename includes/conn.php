<?php
$hostname   = 'localhost';
$username   = 'testp';
$password   = 'testp';
$database   = 'testp';
$port       = 3306;

try {
    if (!empty($hostname) && !empty($database) && !empty($username) && empty($db)) {
        session_write_close();
        $db = new \PDO("mysql:host=$hostname;dbname=$database;port=$port;charset=utf8mb4", $username, $password, array(
            \PDO::ATTR_PERSISTENT   => true,
            \PDO::ATTR_ERRMODE      => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE   => \PDO::FETCH_ASSOC
        ));
    } else {
        session_write_close();
        $db = null;
        die('Set up the database first!');
    }
} catch (\PDOException | \Exception $e) {
    session_write_close();
    $db = null;
    error_log('Database disconnected => ' . $e->getMessage());
    die('Database disconnected!');
}