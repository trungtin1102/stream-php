<?php
if (!defined('BASE_DIR')) exit();

session_write_close();

$gtm_id = get_option('google_tag_manager_id');
if (!empty($gtm_id)) {
    return '<noscript><iframe src="https://www.googletagmanager.com/ns.html?id='. $gtm_id .'" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>';
} else {
    return '';
}
