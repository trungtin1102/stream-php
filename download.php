<?php
session_write_close();

require 'includes.php';

$playerColor = get_option('player_color') ?? '673AB7';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Download - <?php echo sitename(); ?></title>
    <style>
        body {
            padding: 0;
            margin: 0;
            overflow: hidden
        }

        .embed-container {
            text-align: center;
            width: 100%;
            height: 100%;
            padding: 0;
            margin: 0;
            background: #000;
            position: absolute;
            color: #fff;
            z-index: 1024
        }

        .embed-wrapper {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 90%
        }

        .lds-ellipsis {
            display: block;
            position: relative;
            width: 80px;
            height: 80px;
            margin: 0 auto
        }

        .lds-ellipsis div {
            position: absolute;
            top: 33px;
            width: 13px;
            height: 13px;
            border-radius: 50%;
            animation-timing-function: cubic-bezier(0, 1, 1, 0)
        }

        .lds-ellipsis div:nth-child(1) {
            left: 8px;
            animation: lds-ellipsis1 .6s infinite
        }

        .lds-ellipsis div:nth-child(2) {
            left: 8px;
            animation: lds-ellipsis2 .6s infinite
        }

        .lds-ellipsis div:nth-child(3) {
            left: 32px;
            animation: lds-ellipsis2 .6s infinite
        }

        .lds-ellipsis div:nth-child(4) {
            left: 56px;
            animation: lds-ellipsis3 .6s infinite
        }

        @keyframes lds-ellipsis1 {
            0% {
                transform: scale(0)
            }

            100% {
                transform: scale(1)
            }
        }

        @keyframes lds-ellipsis3 {
            0% {
                transform: scale(1)
            }

            100% {
                transform: scale(0)
            }
        }

        @keyframes lds-ellipsis2 {
            0% {
                transform: translate(0, 0)
            }

            100% {
                transform: translate(24px, 0)
            }
        }

        .lds-ellipsis div {
            background: #<?php echo $playerColor; ?>
        }
    </style>
</head>

<body>
    <div id="mContainer" class="embed-container">
        <div class="embed-wrapper">
            <div id="loading" class="lds-ellipsis">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
    <script>
        window.location.href = "<?php echo BASE_URL . 'download/?' . $_SERVER['QUERY_STRING']; ?>";
    </script>
</body>

</html>
